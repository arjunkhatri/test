public with sharing class AccountHandler {
	public static map<Id,List<Loan_Sponsor__c>> mpAccToLSpon;
	public void onBeforeUpdate(map<Id,Account> oldAccountMap,map<Id,Account> newAccountMap){
		this.updateLoan(oldAccountMap,newAccountMap);
	}
	
	public void onBeforeDelete(list<Account> accountList){
		this.updateLoanSponsor(accountList);
	}
	
	public void onAfterDelete(list<Account> accountList, map<Id,List<Loan_Sponsor__c>> mpAccToLSponRecs){
		this.mergeLoanSponsor(accountList,mpAccToLSponRecs);
	}	
	
	//Function used to merge loan sponser when Account get merged
	public void mergeLoanSponsor(list<Account> accountList, map<Id,List<Loan_Sponsor__c>> mpAccToLSponRec){
		list<Loan_Sponsor__c> updatedLSponList = new list<Loan_Sponsor__c>();
		for(Account accRec:accountList){
           if(!mpAccToLSponRec.isEmpty() && mpAccToLSponRec.containsKey(accRec.id)){
	           	if(String.isNotBlank(accRec.MasterRecordId) && accRec.MasterRecordId != null) {
	           		for(Loan_Sponsor__c lSpon:mpAccToLSponRec.get(accRec.id)){
	           			lSpon.Entity__c = accRec.MasterRecordId;
	           			updatedLSponList.add(lSpon);
	           		}
	           	}
	           	else{
	           		for(Loan_Sponsor__c lSpon:mpAccToLSponRec.get(accRec.id)){
	           			lSpon.Entity__c = null;
	           			updatedLSponList.add(lSpon);
	           		}
	           	}
           }
		}
		if(updatedLSponList.size() > 0)
			update updatedLSponList;		
	}	
	
	//Function used to update LoanSponsor record on delete of Account
	public void updateLoanSponsor(list<Account> accountList){
		set<Id> accountIdSet = new set<Id>();
		list<Loan_Sponsor__c> updatedLSList = new list<Loan_Sponsor__c>();
		mpAccToLSpon = new map<Id,List<Loan_Sponsor__c>>();
		for(Account objAcc : accountList){
			accountIdSet.add(objAcc.Id);
		}
		//query the Loan Sponsor records
		for(Loan_Sponsor__c objSponsor : [Select Id,Entity__c from Loan_Sponsor__c where
											Entity__c IN : accountIdSet]){	
			//set the entity to null as the record is deleted to fire the LS trigger
			if(objSponsor.Entity__c != null && !mpAccToLSpon.containsKey(objSponsor.Entity__c))
			{
				mpAccToLSpon.put(objSponsor.Entity__c,new List<Loan_Sponsor__c>{objSponsor});
			}
			else if(objSponsor.Entity__c != null && mpAccToLSpon.containsKey(objSponsor.Entity__c))
			{
				mpAccToLSpon.get(objSponsor.Entity__c).add(objSponsor);
			}			
		}
	}
	
	//Function to update the Loan Report Fields on if the Account Name is changed
	public void updateLoan(map<Id,Account> oldAccountMap,map<Id,Account> newAccountMap){
		//list<Loan_Sponsor__c> updatedLSList = new list<Loan_Sponsor__c>();
		set<Id> accIdSet = new set<Id>();
		set<Id> loanSet = new set<Id>();
		set<String> setRole = new set<String>();
		map<Id,sObject> mapUpdatedLoans = new map<Id,sObject>();
		
		map<String,set<String>> loanEntityMap = new map<String,set<String>>(); //map contains loanid as key and updated entities as values 
		set<String> setPrevEntityName = new set<String>();
		
		for(Account acc : newAccountMap.values()){
			//If the Account Name is updated add it to set
			if(acc.Name != oldAccountMap.get(acc.Id).Name){
				accIdSet.add(acc.Id);
				setPrevEntityName.add(oldAccountMap.get(acc.Id).Name);
			}	
		}
		
		set<Id> setUpdatedLs = new set<Id>();
		
		//query the Loan Sponsor records  //query name set Loan_EntityName 
		for(Loan_Sponsor__c objSponsor : [Select Id,
											     Role__c,
												 Loan__c,
												 Entity__c
										  from Loan_Sponsor__c 
										  where Entity__c IN : accIdSet]){	
			
			setRole.add(objSponsor.Role__c);
			loanSet.add(objSponsor.Loan__c);
			setUpdatedLs.add(objSponsor.Id);
			
			if(loanEntityMap.size() > 0 && loanEntityMap.containsKey(objSponsor.Loan__c) && objSponsor.Role__c != null){
				//put Loan , oldEntity+newEntity+Role
				loanEntityMap.get(objSponsor.Loan__c).add(oldAccountMap.get(objSponsor.Entity__c).Name+'_'+newAccountMap.get(objSponsor.Entity__c).Name+'_'+objSponsor.Role__c);
			}else if(objSponsor.Role__c != null && (loanEntityMap.size() == 0 || (loanEntityMap.size() > 0 && !loanEntityMap.containsKey(objSponsor.Loan__c)) )){
				loanEntityMap.put(objSponsor.Loan__c,new set<String>{oldAccountMap.get(objSponsor.Entity__c).Name+'_'+newAccountMap.get(objSponsor.Entity__c).Name+'_'+objSponsor.Role__c});	
			}
		}	
		
		set<String> setDuplicate = new set<String>();
		
		//Query LoanSponsor to check  for duplicates
		if(setPrevEntityName.size() > 0 && loanSet.size() > 0){
			for(Loan_Sponsor__c objSponsor : [Select Id,
												     Role__c,
													 Loan__c,
													 Entity__c,
													 Entity__r.Name,
													 Contact_Name_DDP__c
											  from Loan_Sponsor__c 
											  where (Entity__r.Name IN : setPrevEntityName or
											  		Contact_Name_DDP__c IN : setPrevEntityName)
											        and Id Not IN: setUpdatedLs
											        and Loan__c IN : loanSet]){	
				
				if(setPrevEntityName.size() > 0 && setPrevEntityName.contains(objSponsor.Entity__r.Name) && objSponsor.Role__c != null){
					setDuplicate.add(objSponsor.Loan__c+'_'+objSponsor.Entity__r.Name+'_'+objSponsor.Role__c);
				}else if(setPrevEntityName.size() > 0 && setPrevEntityName.contains(objSponsor.Contact_Name_DDP__c) && objSponsor.Role__c != null){
					setDuplicate.add(objSponsor.Loan__c+'_'+objSponsor.Contact_Name_DDP__c+'_'+objSponsor.Role__c);
				}								  	
			}
		}
		
		String loanFields ;
	    for(String role : setRole){
	        if(Report_LoanSponsor_Field__c.getInstance(role) != NULL)
	            loanFields = loanFields== NULL ? Report_LoanSponsor_Field__c.getInstance(role).Loan_Field__c :
	                                                loanFields+','+Report_LoanSponsor_Field__c.getInstance(role).Loan_Field__c;
	    }
	    
	    if(loanSet.size() > 0 && loanEntityMap.size() > 0){
			String query =  'Select '+loanFields+' from Loan__c where Id In: loanSet' ;
			
			LoanSponsorTriggerHandler LSHandler = new LoanSponsorTriggerHandler();
			//query the loans that are to be updated
			for(Loan__c objLoan : Database.query(query)){
				if(loanEntityMap.size() > 0 && loanEntityMap.containsKey(objLoan.Id)){
					for(String rprtField :loanEntityMap.get(objLoan.id)){
						
						String[] strArr  = rprtField.split('_');
						if(strArr[2] != null && Report_LoanSponsor_Field__c.getInstance(strArr[2]) != null){
							
							//Remove only if there are no other Loan Sponsor records with same name for that Loan 
							if(setDuplicate.size() == 0 || (setDuplicate.size() > 0 && !setDuplicate.contains(objLoan.Id+'_'+strArr[0]+'_'+strArr[2]))){
								//remove previous Account value
								objLoan = LSHandler.removeFromLoanField(objLoan,strArr[2],strArr[0],
																	(String)objLoan.get(Report_LoanSponsor_Field__c.getInstance(strArr[2]).Loan_Field__c));  
							}
							//insert new value
							objLoan = LSHandler.insertToLoanField(objLoan,strArr[2],strArr[1],
																 (String)objLoan.get(Report_LoanSponsor_Field__c.getInstance(strArr[2]).Loan_Field__c));
							
							mapUpdatedLoans.put(objLoan.Id,objLoan);
						}
					}
				}
			}
			if(!mapUpdatedLoans.isEmpty())
	        	update mapUpdatedLoans.values(); 
	    }
	}
	
	
	//Test Methods
	/* Test method to check if the Account record is deleted then update the Loan Sponsor to make Entity null
	 * and update the report field on Loan Sponsor.
	 */
	static testMethod void deleteAccTest(){
		
		//Create Custom setting record for test role
		Report_LoanSponsor_Field__c reportCS = new Report_LoanSponsor_Field__c(Name ='Test Role', 
																			   Loan_Field__c='Report_Key_Principal__c');
		insert reportCS;
		
		Loan__c objLoan = new Loan__c(Name = 'Test',Report_Key_Principal__c ='');
        insert objLoan;
        
		Account objAcc = new Account(Name='testAcc');
		insert objAcc;
		
		Contact objContact = new Contact(LastName = 'TestContact');
        insert objContact;
		
        Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Contact__c = objContact.id, 
        											Entity__c = objAcc.Id,
        											Sponsor_is_the_Individual__c = true,
        											Role__c = 'Test Role');
        insert objLS;
        
        test.startTest();
        	
        	 objLoan =[Select Report_Key_Principal__c from Loan__c where Id = : objLoan.Id ];
        	 
        	 delete objAcc;
        	 
        	 objLoan =[Select Report_Key_Principal__c from Loan__c where Id = : objLoan.Id ];
        	 system.assert(objLoan.Report_Key_Principal__c.Equals('TestContact'));
        test.stopTest();
	}
	
	//Test Method to check if the Account Name is updated the same should be updated in Loan report field
	static testMethod void updateAccNameTest(){
		//Create Custom setting record for test role
		Report_LoanSponsor_Field__c reportCS = new Report_LoanSponsor_Field__c(Name ='Test Role', 
																			   Loan_Field__c='Report_Key_Principal__c');
		insert reportCS;
		
		Loan__c objLoan = new Loan__c(Name = 'Test',Report_Key_Principal__c ='');
        insert objLoan;
        
		Account objAcc = new Account(Name='testAcc');
		insert objAcc;
		
		Contact objContact = new Contact(LastName = 'TestContact');
        insert objContact;
		
        Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Contact__c = objContact.id, 
        											Entity__c = objAcc.Id,
        											Sponsor_is_the_Individual__c = false,
        											Role__c = 'Test Role');
        insert objLS;
        
         test.startTest();
        	
        	 objLoan =[Select Report_Key_Principal__c from Loan__c where Id = : objLoan.Id ];
        	 
        	 system.assert(objLoan.Report_Key_Principal__c.Equals('testAcc'));
        	 
        	 objAcc.Name = 'Test Account';
        	 update objAcc;
        	 
        	 objLoan =[Select Report_Key_Principal__c from Loan__c where Id = : objLoan.Id ];
        	 system.assert(objLoan.Report_Key_Principal__c.Equals('Test Account'));
        test.stopTest();
	}  
}