public with sharing class M2M_LinkController {
	public static final String PARAM_LINKCONFIGNAME = 'lcn';
	public static final String PARAM_RETURL = 'returl';
	public static final String PARAM_FROMID = 'fid';
	public static final String PARAM_TOID = 'newid';
	public static final String PROPERTY = 'PropertyId__c';
	public static final String FROMLOAN = 'LoanId__c';
	public static final String linkLoanSponsor = 'LinkLoanSponsor';
	public static final String PARAM_ISNEW = 'isNew';
	
	private String linkConfigName;
	private String retUrl;
	public  String fromId{get; private set;}
	private  String toId;
	private Boolean isNewRec;
	private M2M_Config__c currLinkConfig;
	
	// properties
	public string replaceUrl{get;private set;}
	public boolean errorOccurred{get;private set;}

	public M2M_LinkController(){
		map<String, String> params = ApexPages.currentPage().getParameters(); 
		linkConfigName = params.get(PARAM_LINKCONFIGNAME);
		retUrl = params.get(PARAM_RETURL);
		fromId = params.get(PARAM_FROMID);
		toId = params.get(PARAM_TOID);
		isNewRec = false;
		if(params.containsKey(PARAM_ISNEW)){ 
			isNewRec = Boolean.valueOf(params.get(PARAM_ISNEW));
		}
		if (linkConfigName!=null && linkConfigName!='') currLinkConfig = M2M_Config__c.getInstance(linkConfigName);
	}
	
	public PageReference getRedirect(){
		errorOccurred = false;
		if (currLinkConfig.LinkObjectHasOtherData__c){
			// link object has more data for the user to fill in.  generate proper prefill url
			// Loan -> Contact
			// Contact -> Loan
			PageReference newPrefillPR = new PageReference('/'+currLinkConfig.LinkObjectPrefix__c+'/e');
			map<string,string> params = newPrefillPR.getParameters();
			params.put('CF'+currLinkConfig.LinkFromFieldId__c+'_lkid',fromId);
			params.put('CF'+currLinkConfig.LinkFromFieldId__c, getSObjectName(fromId, currLinkConfig.LinkFromObjectName__c));
			params.put('CF'+currLinkConfig.LinkToFieldId__c+'_lkid',toId);
			params.put('CF'+currLinkConfig.LinkToFieldId__c, getSObjectName(toId, currLinkConfig.LinkToObjectName__c));
			
			params.put('saveURL',retUrl);
			params.put('retURL',retUrl);
			if(currLinkConfig.Name == linkLoanSponsor){
				String name =ApexPages.currentPage().getParameters().get('entityName');
				String entityId =ApexPages.currentPage().getParameters().get('entity');
				
				String isIndividual =ApexPages.currentPage().getParameters().get('in');
				if(isIndividual != 'null' || isIndividual != NULL)
					params.put(System.Label.M2MLinkController_IsIndividualId,isIndividual);
				
				if(name != 'null' || entityId != 'null' ){
					params.put('CF'+System.Label.M2MLinkController_RelatedEntityId,name);
					params.put('CF'+System.Label.M2MLinkController_RelatedEntityId+'_lkid',+entityId);
				}
			}
			
			replaceUrl = newPrefillPR.getUrl();
		}
		else{ 
			// link object is just 2 links, create the object and redirect back to return url
			sObject linkRec = createManyToManyObject();
			linkRec.put(currLinkConfig.LinkToField__c, toId);
			linkRec.put(currLinkConfig.LinkFromField__c, fromId);
			System.debug('linkRec:'+linkRec);
			try{
				insert linkRec;
			}
			catch(DMLException e){
				if (e.getDmlType(0) == System.StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_Error_NoAccess));
					errorOccurred = true;
				}
				else{
					throw e;
				}
			}
			if(currLinkConfig.LinkToField__c == PROPERTY ){  
				//if Property is newly created redirect to Loan Property page
				if(isNewRec && toId != null && toId != ''){
					//if property created from loan
					if(currLinkConfig.LinkFromField__c == FROMLOAN){
						String loanProp = loanPropertyGet(toId);
						if(loanProp != null && loanProp != ''){
							 if(retUrl.contains('/'))
							 	retUrl = retUrl.replace('/', '');
							 replaceUrl = '/'+loanProp+'/e?retURL='+retUrl;
						}
					}else{
						replaceUrl = '/'+toId;
					}
				}else{  
					//if record selected from search redirect to edit page
					if(retUrl.contains('/'))
						retUrl = retUrl.replace('/', '');					
					replaceUrl = '/'+toId+'/e?retURL='+retUrl;
				}
			}else{ 
				replaceUrl = retUrl;
			}
		}
		return null;
	}
	//function to get Loan Property of newly created Property from Loan
	public String loanPropertyGet(String pId){
		String lId;
		Loan_Property__c lp = [Select id from Loan_Property__c where PropertyId__c=:pId limit 1];
		if(lp != null)
			lId = lp.id;
		return lId;
	}
	
	// if we could dynamically create a record, this would be totally modular.  instead we'll have to simply subclass this when needed
	public virtual sObject createManyToManyObject(){
		
		// for now lets parse the from and to id and pass by the appropriate object
		// Loan -> Property
		
		if ( (fromId.startsWith(Loan__c.SObjectType.getDescribe().getKeyPrefix())
			&&toId.startsWith(Property__c.SObjectType.getDescribe().getKeyPrefix()))
			  ||(fromId.startsWith(Property__c.SObjectType.getDescribe().getKeyPrefix())
			&&toId.startsWith(Loan__c.SObjectType.getDescribe().getKeyPrefix()))
		){
			return new Loan_Property__c();
		}
		// Property -> Contact
		// Contact -> Property 
		else if((fromId.startsWith(Contact.SObjectType.getDescribe().getKeyPrefix())
			&&toId.startsWith(Property__c.SObjectType.getDescribe().getKeyPrefix()))
			  ||(fromId.startsWith(Property__c.SObjectType.getDescribe().getKeyPrefix())
			&&toId.startsWith(Contact.SObjectType.getDescribe().getKeyPrefix()))
		){
			return new Property_Contact__c();
		}
		return null;
	}

	private string getSObjectName(string sObjectID, string sObjectName){
		list<sObject> sObjList;
		string retName = ''; 
		
		if (sObjectID.startsWith(Contact.SObjectType.getDescribe().getKeyPrefix())){
			sObjList = [select FirstName, LastName from Contact where id =:sObjectID];
			string firstName = (string)((sObject)sObjList.get(0)).get('FirstName');
			string lastName  = (string)((sObject)sObjList.get(0)).get('LastName');
			if (firstName!=null && firstName!='') retName = firstName + ' ' + lastName;
			else retName = lastName;
		}
		else{
			string soqlQuery = 'select Name from '+sObjectName+' where id =:sObjectID'; 
			sObjList = Database.query(soqlQuery);
			retName = (string)((sObject)sObjList.get(0)).get('Name');
		}
		return retName;
	}

	// Test methods ----------------------------------------------------
	public static testMethod void testThisClass(){
		M2M_Config__c testLinkConfig1 = new M2M_Config__c(Name='testLinkConfig1', 
			LinkObjectName__c='Loan_Property__c', 
			LinkFromField__c='LoanId__c', LinkToField__c='PropertyId__c');

		M2M_Config__c testLinkConfig2 = new M2M_Config__c(Name='testLinkConfig2', 
			LinkObjectName__c='Loan_Contact__c', LinkObjectPrefix__c='a0B', LinkObjectHasOtherData__c=true,
			LinkFromField__c='LoanId__c', LinkFromFieldId__c='00NC000000562aF', LinkFromObjectName__c='Loan__c',
			LinkToField__c='ContactId__c',  LinkToFieldId__c='00NC000000562a8', LinkToObjectName__c = 'Contact');

		insert new M2M_Config__c[]{testLinkConfig1, testLinkConfig2};
		

		Loan__c testLoan = new Loan__c(name='testLoan');
		insert testLoan;
		
		Property__c testProperty = new Property__c(name='testProperty1');		
		insert testProperty;
		
		Contact testContact = new Contact(lastname='testContact1');		
		insert testContact;

		// call the auto create portion of the code.....
		PageReference testPageReference1 = Page.M2M_Link;
		map<string, string> params = testPageReference1.getParameters();
		params.put(M2M_LinkController.PARAM_LINKCONFIGNAME, 'testLinkConfig1');
		params.put(M2M_LinkController.PARAM_FROMID, testLoan.Id);
		params.put(M2M_LinkController.PARAM_RETURL, '/'+testLoan.Id);
		params.put(M2M_LinkController.PARAM_TOID, testProperty.Id);
		params.put(M2M_LinkController.PARAM_ISNEW,'true');
		Test.setCurrentPage(testPageReference1);
		M2M_LinkController testController = new M2M_LinkController();
		
		// call the link operation
		testController.getRedirect();

		List<Loan_Property__c> resultList = [select id from Loan_Property__c 
			where PropertyId__c = :testProperty.id and LoanId__c = :testLoan.id];		

		System.assertEquals(1 ,resultList.size());
		//System.assertEquals('/'+testLoan.Id ,testController.replaceUrl);

		// call the auto prefill portion of the code.....
		PageReference testPageReference2 = Page.M2M_Link;
		params = testPageReference2.getParameters();
		params.put(M2M_LinkController.PARAM_LINKCONFIGNAME, 'testLinkConfig2');
		params.put(M2M_LinkController.PARAM_FROMID, testLoan.Id);
		params.put(M2M_LinkController.PARAM_RETURL, '/'+testLoan.Id);
		params.put(M2M_LinkController.PARAM_TOID, testContact.Id);
		Test.setCurrentPage(testPageReference2);
		testController = new M2M_LinkController();
		
		// call the link operation
		testController.getRedirect();
		System.assert(testController.replaceUrl.startsWith('/a0B/e'));

	}

}