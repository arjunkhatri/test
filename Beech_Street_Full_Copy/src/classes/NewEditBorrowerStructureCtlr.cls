/*
 *   Descprition : Controller to insert/update Loan_Sponsor__c records and also update
 *                 Contact/Related Account records.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            07/04/15          Initial Draft
 *   1.1            14/05/15          Pre-populate OFAC date and TAX id field on contact and 
 *                                    Account based on sponsor is individual checkbox.
 */
public with sharing class NewEditBorrowerStructureCtlr {
    public Loan_Sponsor__c bStructure       {get;set;}
    public Contact loanContact              {get;set;}
    public Account loanAccount              {get;set;}
    public Contact loanCon                  {get;set;} 
    public String conId;
    public String accId;
    public String sponIndvId                {get;set;}
    public String bStrucId;
    private static final String STR_REG_TO_CHECK_CONTACT = '[0-9]{3}-[0-9]{2}-[0-9]{4}';
    private static final String STR_REG_TO_CHECK_ACCOUNT = '[0-9]{2}-[0-9]{7}';
    private static final String STR_CONTACT     = 'CF00Na0000009r4Id_lkid';
    private static final String STR_ACCOUNT     = 'CF00Na0000009rELJ_lkid';
    /*from the LinkLoanSponsor page search results checked Sponsor_is_the_Individual__c
      is true or false */
    private static final String STR_SPON_INDV   = '00Na000000B1ib2';
    private static final String STR_BSTRUCTURE_ID   = 'id';
    public boolean isExc;
    public boolean isUncheck                {get;set;}
    ApexPages.StandardController m_controller;
    //Constructor Start
    public NewEditBorrowerStructureCtlr(ApexPages.StandardController stdController){
        m_controller = stdController;
        isExc = false;
        isUncheck = false;
        bStrucId      =  ApexPages.currentPage().getParameters().get(STR_BSTRUCTURE_ID);
        /*if id is present in url it is in Edit mode */
        if(String.isNotBlank(bStrucId)){
            getLSRecord();
        }else{
        	 this.bStructure = (Loan_Sponsor__c)m_controller.getRecord();
	         conId      =  ApexPages.currentPage().getParameters().get(STR_CONTACT);
	         accId      =  ApexPages.currentPage().getParameters().get(STR_ACCOUNT);
	         sponIndvId = ApexPages.currentPage().getParameters().get(STR_SPON_INDV);
        }
         if(String.isNotBlank(sponIndvId) && sponIndvId == '1'){
            bStructure.Sponsor_is_the_Individual__c = true;
            getConFields();
         }
         else{
            bStructure.Sponsor_is_the_Individual__c = false;
            getAccFields();
         }
    }//Contructor end
    /* Method to get all fields of Loan Sponsor from fieldSet using dynamic SOQL query */
    public pagereference getLSRecord(){
        SObject objLS ;
	       String query = 'SELECT ';
	           for(Schema.FieldSetMember objfldMem : SObjectType.Loan_Sponsor__c.FieldSets.NewEdit_Borrower_Structure.getFields()) {
	               query += objfldMem.getFieldPath() + ', ';
               }
	           for(Schema.FieldSetMember objfldSetMem : SObjectType.Loan_Sponsor__c.FieldSets.NewEdit_Borrower_Structure_FCRA_Info.getFields()) {
	               query += objfldSetMem.getFieldPath() + ', ';
	           }
	           query += 'Id, Name,Mailing_Zip_Postal_Code__c,Mailing_Street__c,Mailing_State__c,Mailing_Country__c, Mailing_City__c,Copy_Address_Shown__c FROM Loan_Sponsor__c WHERE Id  = \'' + bStrucId + '\'';
	           objLS = Database.query(query);
	           bStructure = (Loan_Sponsor__c)objLS;
		       if(bStructure != null){
		          conId = bStructure.Contact__c;
		          accId = bStructure.Entity__c;
		          if(bStructure.Sponsor_is_the_Individual__c)
		                    sponIndvId= '1';
		       }
            return null;
    }

    /* Method to get all fields of contact from fieldSet using dynamic SOQL query */
    public pagereference getConFields(){
    	conId = bStructure.Contact__c;
        SObject objCon ;
        Matcher MyMatcherCon ;
        /*when user check bStructure.Sponsor_is_the_Individual__c 
          make  bStructure.Entity__c = null*/
        bStructure.Entity__c = null;
        /*Pre-populate TAX id field on contact
          based on sponsor is individual checkbox.*/
        if(String.isNotBlank(conId)){
            loanContact = [ Select id, Social_Security_Number__c,OFAC_Date__c
                            From Contact where id = :conId limit 1];
            Pattern MyPatternCon = Pattern.compile(STR_REG_TO_CHECK_CONTACT);
            if(loanContact !=null && loanContact.Social_Security_Number__c != null){
                MyMatcherCon = MyPatternCon.matcher(loanContact.Social_Security_Number__c);
            }
            if(loanContact !=null && bStructure.Sponsor_is_the_Individual__c == true ){
                isUncheck = false;
                if(loanContact.Social_Security_Number__c != null){
	                if(MyMatcherCon.matches()){
	                    bStructure.Is_Tax_ID_Accurate__c = 'Yes';
	                }
	                else {
	                    bStructure.Is_Tax_ID_Accurate__c = 'No';
	                }
                }
                else{
                    bStructure.Is_Tax_ID_Accurate__c = 'No';
                }
                bStructure.Tax_ID_Number__c          =   loanContact.Social_Security_Number__c ;
            }
            else{
                isUncheck = true;
                bStructure.Tax_ID_Number__c          =   ' ';
                getAccFields(); 
            }
        }
        if(isExc){
	        if(bStructure.Mailing_Street__c != null || bStructure.Mailing_City__c != null ||
	           bStructure.Mailing_State__c  != null || bStructure.Mailing_Zip_Postal_Code__c != null 
	           || bStructure.Mailing_Country__c   != null ||  bStructure.Copy_Address_Shown__c == true){
	               bStructure.Mailing_Street__c            =   '' ;
	               bStructure.Mailing_City__c              =   '' ;
	               bStructure.Mailing_State__c             =   '' ;
	               bStructure.Mailing_Zip_Postal_Code__c   =   '' ;
	               bStructure.Mailing_Country__c           =   '' ;
	               bStructure.Copy_Address_Shown__c        = false;
	        }
        }
        isExc = true;
        if(String.isNotBlank(conId)){
             String query = 'SELECT ';
             for(Schema.FieldSetMember objfldMem : SObjectType.Contact.FieldSets.NewEdit_BStructure_ConAddress.getFields()) {
                query += objfldMem.getFieldPath() + ', ';
             }
             for(Schema.FieldSetMember objfldSetMem : SObjectType.Contact.FieldSets.NewEdit_BStructure_ConVerification_Info.getFields()) {
                query += objfldSetMem.getFieldPath() + ', ';
             }
             query += 'Id, Name,Social_Security_Number__c,OFAC_Date__c FROM Contact WHERE Id  = \'' + conId + '\'';
             objCon = Database.query(query);
             loanContact = (Contact)objCon;
        }
            return null;
    }

    /* Method to get all fields of Account from fieldSet using dynamic SOQL query*/
    public pagereference getAccFields(){
        SObject objAcc ;
        accId = bStructure.Entity__c;
        Matcher MyMatcherAcc ;
        /*when user select value in bStructure.Entity__c make 
          bStructure.Sponsor_is_the_Individual__c = false*/
        bStructure.Sponsor_is_the_Individual__c = false;
        /*Pre-populate TAX id field on Account
          based on sponsor is individual checkbox.*/
        if(String.isNotBlank(accId)){
            loanAccount = [ Select id, Tax_ID_Number__c,OFAC_Date__c
                            From Account where id = :accId limit 1];
            Pattern MyPatternAcc = Pattern.compile(STR_REG_TO_CHECK_ACCOUNT);
            if(loanAccount !=null && loanAccount.Tax_ID_Number__c != null){
                MyMatcherAcc = MyPatternAcc.matcher(loanAccount.Tax_ID_Number__c);
            }
            if(loanAccount !=null && bStructure.Entity__c != null && bStructure.Sponsor_is_the_Individual__c == false){
                if(loanAccount.Tax_ID_Number__c != null){
                    if(MyMatcherAcc.matches()){
                        bStructure.Is_Tax_ID_Accurate__c = 'Yes';
                    }
                    else {
                        bStructure.Is_Tax_ID_Accurate__c = 'No';
                    }
                }
                else{
                    bStructure.Is_Tax_ID_Accurate__c = 'No';
                }
                bStructure.Tax_ID_Number__c          =   loanAccount.Tax_ID_Number__c ;
            }
        }
        if(isExc){
	        if(bStructure.Mailing_Street__c != null || bStructure.Mailing_City__c != null ||
	           bStructure.Mailing_State__c != null || bStructure.Mailing_Zip_Postal_Code__c != null 
	           ||  bStructure.Mailing_Country__c   != null ||  bStructure.Copy_Address_Shown__c == true){
	                bStructure.Mailing_Street__c            =   '' ;
	                bStructure.Mailing_City__c              =   '' ;
	                bStructure.Mailing_State__c             =   '' ;
	                bStructure.Mailing_Zip_Postal_Code__c   =   '' ;
	                bStructure.Mailing_Country__c           =   '' ;
	                bStructure.Copy_Address_Shown__c        = false;
	        }
        }
        isExc = true;
        if(String.isNotBlank(accId)){
            String query = 'SELECT ';
            for(Schema.FieldSetMember objfldMem : SObjectType.Account.FieldSets.NewEdit_BStructure_AccAddress.getFields()) {
                query += objfldMem.getFieldPath() + ', ';
            }
            for(Schema.FieldSetMember objfldSetMem : SObjectType.Account.FieldSets.NewEdit_BStructure_AccVerification_Info.getFields()) {
                query += objfldSetMem.getFieldPath() + ', ';
            }
            query += 'Id,Name,Tax_ID_Number__c,OFAC_Date__c FROM Account WHERE Id  = \'' + accId + '\'';
            objAcc = Database.query(query);
            loanAccount = (Account)objAcc; 
        }
            return null;
    }
    /*Method to perform Save operation*/
    public ApexPages.pagereference save(){
        if(bStructure.Tax_ID_Number__c == null || bStructure.Tax_ID_Number__c ==''){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,System.label.Tax_ID_Number_Required_Error));
             return null;
        }
        /*validation in the Borrower Structure VF page for checking format of 
          Social Security Number and Tax ID Number */
        Pattern MyPatternCon = Pattern.compile(STR_REG_TO_CHECK_CONTACT);
        Matcher MyMatcherCon = MyPatternCon.matcher(bStructure.Tax_ID_Number__c);

        Pattern MyPatternAcc = Pattern.compile(STR_REG_TO_CHECK_ACCOUNT);
        Matcher MyMatcherAcc = MyPatternAcc.matcher(bStructure.Tax_ID_Number__c);

         if (bStructure.Sponsor_is_the_Individual__c == true ){
             if(!MyMatcherCon.matches()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,System.Label.Tax_ID_Number_Format_Contact));
                return null;
             }
         }
         else{
             if(!MyMatcherAcc.matches()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,System.Label.Tax_ID_Number_Format_Account));
                return null;
             }
         }
        /* If bStructure.Entity__c and bStructure.Sponsor_is_the_Individual__c
           fields are blank, and they try to Save, display Error Message. */
        if(bStructure.Entity__c == null && bStructure.Sponsor_is_the_Individual__c == false){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,System.Label.NewEdit_BStruc_cbox_or_acct_Unchecked));
            return null;
        }
        if(loanContact != null && bStructure.Sponsor_is_the_Individual__c == true){
                loanContact.OFAC_Date__c  =  bStructure.OFAC_Date2__c ;
            if(loanContact.Social_Security_Number__c  != bStructure.Tax_ID_Number__c)
                loanContact.Social_Security_Number__c  =  bStructure.Tax_ID_Number__c;
            update loanContact;
        }
        if(loanAccount != null && bStructure.Sponsor_is_the_Individual__c == false && bStructure.Entity__c != null){
                loanAccount.OFAC_Date__c  =  bStructure.OFAC_Date2__c ;
            if(loanAccount.Tax_ID_Number__c  != bStructure.Tax_ID_Number__c )
                loanAccount.Tax_ID_Number__c  =  bStructure.Tax_ID_Number__c;
            update loanAccount;
        }
        try{
	        if(String.isNotBlank(bStrucId)){
		        update bStructure;
	        }
	        else{
	             insert bStructure;
	        }
        }
        catch(Exception e) {
            return null;
        }
        return new PageReference ('/'+bStructure.Loan__c);
    }
    /*Method to perform cancel operation*/
    public pagereference cancel(){
        PageReference ret = m_controller.cancel();
        return ret;
    }
    /* Method to perform cloning  of Contact Address fields to Borrower Structure 
       Address field on change of  Copy_Address_Shown__c(checkbox)*/
    public PageReference copyConAddress(){
        if(loanContact != null && bStructure.Copy_Address_Shown__c == true) {
            bStructure.Mailing_Street__c            =   loanContact.OtherStreet ;
            bStructure.Mailing_City__c              =   loanContact.OtherCity ;
            bStructure.Mailing_State__c             =   loanContact.OtherState ;
            bStructure.Mailing_Zip_Postal_Code__c   =   loanContact.OtherPostalCode ;
            bStructure.Mailing_Country__c           =   loanContact.OtherCountry ;
        }
        else {
            bStructure.Mailing_Street__c            =   '' ;
            bStructure.Mailing_City__c              =   '' ;
            bStructure.Mailing_State__c             =   '' ;
            bStructure.Mailing_Zip_Postal_Code__c   =   '' ;
            bStructure.Mailing_Country__c           =   '' ;
        }
        return null;
    }
    /* Method to perform cloning of Account Address fields to Borrower Structure Address field on
       change of  Copy_Address_Shown__c(checkbox)*/
    public PageReference copyAccAddress(){
        if(loanAccount != null && bStructure.Copy_Address_Shown__c == true){
            bStructure.Mailing_Street__c            =   loanAccount.BillingStreet ;
            bStructure.Mailing_City__c              =   loanAccount.BillingCity ;
            bStructure.Mailing_State__c             =   loanAccount.BillingState ;
            bStructure.Mailing_Zip_Postal_Code__c   =   loanAccount.BillingPostalCode ;
            bStructure.Mailing_Country__c           =   loanAccount.BillingCountry ;
        }
        else {
            bStructure.Mailing_Street__c            =   '' ;
            bStructure.Mailing_City__c              =   '' ;
            bStructure.Mailing_State__c             =   '' ;
            bStructure.Mailing_Zip_Postal_Code__c   =   '' ;
            bStructure.Mailing_Country__c           =   '' ;
        }
        return null;
    }
    /* Method to make field empty for Verification section (for Related Account only) */
    public PageReference emptyVerSecField(){
        system.debug('testttttttttttttloanAccount.Verification_Not_Required_Reason__c '+loanAccount.Verification_Not_Required_Reason__c);
        if(loanAccount.Verification_Not_Required_Reason__c != ''){
            loanAccount.Documentary_Verification_Type__c            =   '' ;
            loanAccount.Date_Issued__c                              =   null ;
            loanAccount.Place_of_Issuance__c                        =   '' ;
            loanAccount.Verification_Comments__c                    =   '' ;
        }
        else if(loanAccount.Documentary_Verification_Type__c != ''){
            loanAccount.Verification_Not_Required_Reason__c = '';
        }
        return null;
    }
}