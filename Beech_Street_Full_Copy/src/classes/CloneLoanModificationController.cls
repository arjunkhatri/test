/*
 *   Descprition : An apex page controller that exposes the  "Generate Modification"  functionality
 *                 for cloning Loan object Records.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            04/23/2015          Initial Draft
 */
public without sharing class CloneLoanModificationController {
    public List<relatedObjects> objectChildren      { get; set; }
    public String               objectTypeName      { get; set; }
    public String               objectName          { get; set; }
    public List<String>         lstFields           { get; set; } 
    public List<String>         lstLookups          { get; set; } 
    public Map<String,String>   mpLPfields          { get; set; }
    public Map<String,String>   mpLCfields          { get; set; }
    public Map<String,String>   mpLTfields          { get; set; }
    public Map<String,String>   mpLSfields          { get; set; }
    public Map<String,String>   mpFeildtoVal;
    private static final String STR_LOAN_TERM = 'Loan_Term__c';
    private static final String STR_LOAN_PROPERTY = 'Loan_Property__c';
    private static final String STR_STATUS_FHA = 'Firm app underwriting';
    private static final String STR_LOAN_CONTACT = 'Loan_Contact__c';
    private static final String STR_LOAN_SPON = 'Loan_Sponsor__c';
    private static final String STR_SPECIFIC = 'Specific';
    private static final String STR_ASSUMP_FEE = 'Assumption_Fee__c';
    private static final String STR_LEGAL_FEE = 'Legal_Fee__c';
    private static final String STR_LOAN_PURPOSE = 'Loan_Purpose__c';
    private static final String STR_LOAN_MODIFIC = 'Loan Modification';
    private static final String STR_ASSUMPTION = 'Assumption';
    private static final String STR_RECORD_TYPE = 'RecordTypeId';
    private static final String STR_MODIFICATION = 'Modification';
    private static final String STR_IRR = 'IRR';
    private static final String STR_RATE_LOCK_EXETYPE = 'Rate_Lock_Execution_Type__c';
    private static final String STR_FINAL = 'Final__c';
    private static final String STR_FHA = 'FHA';
    private static final String STR_INVESTER_TYPE = 'Investor_Type__c';
    private static final String STR_LOAN_STATUS = 'Loan_Status__c';
    private static final String STR_LOAN_NUMBER = 'Loan_Number_Formula__c';
    private static final String STR_CLOSING_OFFICER = 'Closing_Officer__c';
    private static final String STR_LOAN_PROCESSOR = 'Loan_Processor__c';
    private static final String STR_SERVICE_ANALYST = 'Servicing_Analyst__c';
    private static final String STR_UNDER_LEAD = 'Underwriting_Lead__c';
    private static final String STR_CLOSING_COORDINATOR = 'Closing_Coordinator__c';
    private static final String STR_DEPUTY_CHIEF = 'Deputy_Chief_Underwriter__c';
    private static final String STR_LOAN_ID = 'LoanId__c';
    private static final String STR_ID = 'Id';
    private static final String STR_SUFFICIENT_ACCESS = 'sufficient Access';
    private static final String STR_BOOLEAN = 'BOOLEAN';
    private static final String STR_CHILD_OBJECT_TYPES = 'childobjecttypes';
    private static final String STR_NONE = '-None-';
    private static final String STR_NULL = 'null';
    private static final String STR_TRUE = 'true';
    private static final String STR_FALSE = 'false';
    private static final String STR_NAME = 'Name';
    
    private SObject headSObject,headClone;
    private Loan__c             objLoanOld;
    public Loan__c              objLoanNew          { get; set; }
    public String               objectID;
    public String               objectloanTermID;
    public Boolean              isError             { get; set; }
    public Map<String,SObject>   mpOldToClone       { get; set; }
    Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
    Map<String,String> keyPrefixMap = new Map<String,String>{};
    public RecordType objRecId                      { get; set; }
    public set<string> setFileds = new set<string>();
    public String selectedValue{get;set;}
    //Constructor Start
	public CloneLoanModificationController(){
    	 objRecId = [SELECT Name,Id FROM RecordType WHERE Name=: STR_SPECIFIC and SobjectType =: STR_LOAN_TERM Limit 1];
	}
	////Constructor End
    public void initialiseObjectsForCloning(){
        /* Here we generate a keyprefixmap using the global describe 
           Then compare that to our object to determine type.
       */
        isError = false;  
        lstFields = new List<String>();
        lstLookups = new List<String>();
        String strField = ''; 
        mpFeildtoVal = new Map<String,String>();
        //Read custom settings for loan object to get fields to be display
        List<LoanCloneDisplayFields__c> lcf = LoanCloneDisplayFields__c.getAll().Values();
        if(lcf != null && lcf.size() > 0){
            for(LoanCloneDisplayFields__c df:lcf){ 
                if(df.name == STR_ASSUMP_FEE  ){
                    df.name = STR_LEGAL_FEE;
                }
                if(df.name == STR_LOAN_PURPOSE && df.Value__c == STR_ASSUMPTION){
                     df.Value__c = STR_LOAN_MODIFIC;
                }
                strField = strField+','+df.name;
                mpFeildtoVal.put(df.name,df.Value__c);
                if(!df.isEdit__c){
                    lstFields.add(df.name);
                }else{
                    lstLookups.add(df.name);
                }
            }
        }
        for(String sObj : gd.keySet()){
            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
            keyPrefixMap.put(r.getKeyPrefix(), r.getName());
        }
        objectID = ApexPages.currentPage().getParameters().get(STR_ID);
        String objectTypeKey = objectId.subString(0,3);
        objectTypeName = keyPrefixMap.get(objectTypeKey);
        String query = 'SELECT Id, OwnerId, Name, Assumption__c, Loan_Number_Formula__c';
        if(String.isNotBlank(strField)){
            query = query +strField;
        }
        query = query+' FROM '+ objectTypeName+ ' WHERE Id = \''+ objectId+ '\'';
        headSObject = Database.query(query);
        objectName          = '' + headSObject.get(STR_NAME);
        populateObjectChildren();
        headClone = cloneObjects(new List<sObject>{headSObject}).get(0);
        objLoanOld = (Loan__c)headClone;
        selectedValue = objLoanOld.Rate_Lock_Execution_Type__c;
        objLoanNew = objLoanOld;
        if(objLoanOld.Loan_Number_Formula__c != null){ 
            objLoanNew.put(STR_LOAN_NUMBER,objLoanOld.Loan_Number_Formula__c+'M');
        }else{
            objLoanNew.put(STR_LOAN_NUMBER,'M');
        }
        //objLoanNew.put('Assumption__c',true);
        objLoanNew.put(STR_CLOSING_OFFICER,' ');
        objLoanNew.put(STR_LOAN_PROCESSOR,' ');
        objLoanNew.put(STR_SERVICE_ANALYST,' ');
        objLoanNew.put(STR_UNDER_LEAD,' ');
        objLoanNew.put(STR_CLOSING_COORDINATOR,' ');
        objLoanNew.put(STR_DEPUTY_CHIEF,' ');
        for(Schema.FieldSetMember f : SObjectType.Loan__c.FieldSets.Clone_Loan_Null_Fields_Modification.getFields()) { 
            if(f.getType() != null && String.valueOf(f.getType()) != STR_BOOLEAN)
                objLoanNew.put(String.valueOf(f.getFieldPath()),null);
            else
                objLoanNew.put(String.valueOf(f.getFieldPath()),false);
        }

        for(String fs:lstFields){
            if(!mpFeildtoVal.isEmpty() && mpFeildtoVal.containsKey(fs)){
                String str = mpFeildtoVal.get(fs);
                if(str == STR_NULL){
                    objLoanNew.put(fs,'');
                }else if(str == STR_TRUE){
                    objLoanNew.put(fs,true);
                }else if(str == STR_FALSE){
                    objLoanNew.put(fs,false);
                }else{
                    objLoanNew.put(fs,str);
                }
            }
        } 
        //for lookup type fields
        /*for(String lf:lstLookups){
            if(lf == 'AccountId__c'){
                objLoanNew.put(lf,'');
            }else{
                objLoanNew.put(lf,headSObject.get(lf));
            }
        }*/
        //if Execution Type of loan is 'FHA' set Loan Status to 'Firm app underwriting'
        if(objLoanNew.get(STR_INVESTER_TYPE) == STR_FHA){
            objLoanNew.put(STR_LOAN_STATUS,STR_STATUS_FHA);
        }
    }
    
    public List<SelectOption> getExeTypePicklist(){
        List<SelectOption> lstExeType = new List<SelectOption>();
        lstExeType.add(new SelectOption('',STR_NONE));
        lstExeType.add(new SelectOption(STR_IRR,STR_IRR));
        lstExeType.add(new SelectOption(STR_MODIFICATION,STR_MODIFICATION));
        return lstExeType;
    }
    // we want to allow users to clone.  
    public void populateObjectChildren()
    {
        objectChildren = new List<relatedObjects>{};
        Set<String> childObjectTypes = new Set<String>{};
        mpOldToClone = new Map<String,Sobject>();
        // read the object types from the page parameter.    
        childObjectTypes.addAll(ApexPages.currentPage().getParameters()
                                    .get(STR_CHILD_OBJECT_TYPES)
                                    .split(',')
        );
        /* Use the sobjecttype describe method to retrieve all 
           child relationships for the object to be cloned.
        */
        Schema.DescribeSObjectResult headDescribe = headsObject.getSObjectType().getDescribe();
        List<Schema.ChildRelationship> childRelationships = headDescribe.getChildRelationships(); 
        /* Iterate through each relationship, 
           and retrieve the related objects.
        */
        String relatedChildSObjectsquery = ''; 
        for (Schema.ChildRelationship childRelationship : childRelationships)
        {
            Schema.SObjectType childObjectType = childRelationship.getChildSObject();
            /* Only retrieve the objects if their type is 
               included in the page argument.
            */  
            if (childObjectTypes.contains(childObjectType.getDescribe().getName()))
            {
                List<relatedObjectRow> relatedObjects = new List<relatedObjectRow>{};
                Schema.SObjectField childObjectField = childRelationship.getField();
                if(childObjectType.getDescribe().getName() == STR_LOAN_PROPERTY){
                    mpLPfields = new Map<String,String>();
                    relatedChildSObjectsquery = 'SELECT ID,Name,  ';
                    for(Schema.FieldSetMember f : SObjectType.Loan_Property__c.FieldSets.CloneLoanProperty.getFields()){
                        relatedChildSObjectsquery += f.getFieldPath() + ',';
                        mpLPfields.put(String.valueOf(f.getFieldPath()),String.valueof(f.getLabel()));
                    }
                    relatedChildSObjectsquery = relatedChildSObjectsquery.subString(0,relatedChildSObjectsquery.length() - 1);
                    relatedChildSObjectsquery = relatedChildSObjectsquery+ ' FROM ' 
                         + childObjectType.getDescribe().getName()
                         + ' WHERE '
                         + childObjectField.getDescribe().getName()
                         + ' = \'' 
                         + headsObject.Id
                         + '\''; 
                        for (SObject childObject :Database.query(relatedChildSObjectsquery))
                        {
                            relatedObjects.add(new relatedObjectRow(childObject));
                        }
                }
                else if (childObjectType.getDescribe().getName() == STR_LOAN_TERM){
                    mpLTfields = new Map<String,String>();
                    relatedChildSObjectsquery = 'SELECT ID,Final__c, ';
                    for(Schema.FieldSetMember f : SObjectType.Loan_Term__c.FieldSets.CloneLoanTermModification.getFields()) {
					    setFileds.add(f.getFieldPath()); 
					    if(f.getFieldPath() != STR_FINAL){
                        	relatedChildSObjectsquery += f.getFieldPath() + ',';
                        	mpLTfields.put(String.valueOf(f.getFieldPath()),String.valueof(f.getLabel()));
					    }
                    }
                    /*22/05/15 : As we can not update record type 
                      so to avoid this set to null we are adding it to set
                    */ 
                        setFileds.add(STR_RECORD_TYPE);
                    relatedChildSObjectsquery = relatedChildSObjectsquery.subString(0,relatedChildSObjectsquery.length() - 1);
                    relatedChildSObjectsquery = relatedChildSObjectsquery+ ' FROM ' 
                         + childObjectType.getDescribe().getName()
                         + ' WHERE '
                         + childObjectField.getDescribe().getName()
                         + ' = \'' 
                         + headsObject.Id
                         + '\'';
                    List<SObject> objLoanTerms = Database.query(relatedChildSObjectsquery);
                    if(objLoanTerms != null && objLoanTerms.size() > 0){
                        Boolean isNoFinal = true;
                        for (SObject childObject :objLoanTerms)
                        {
                            if(Boolean.valueOf(childObject.get(STR_FINAL))){
                                isNoFinal = false;
                                relatedObjects.add(new relatedObjectRow(childObject));
                            }
                        }
                        if(isNoFinal){
                            isError = true;
                            ApexPages.Message loanTermError = new ApexPages.Message(ApexPages.Severity.Error, system.Label.Loan_Term_Validation_for_Modification);
                            ApexPages.addMessage(loanTermError);
                        }
                    }else{
                            isError = true;
                            ApexPages.Message loanTermError2 = new ApexPages.Message(ApexPages.Severity.Error, system.Label.Loan_Term_Validation_for_Modification);
                            ApexPages.addMessage(loanTermError2);
                    }
                    
                }
                else if(childObjectType.getDescribe().getName() == STR_LOAN_SPON){
                    mpLSfields = new Map<String,String>();
                    relatedChildSObjectsquery = 'SELECT ID,Name,  ';
                    for(Schema.FieldSetMember f : SObjectType.Loan_Sponsor__c.FieldSets.CloneLoanSponsor.getFields()) {
                        relatedChildSObjectsquery += f.getFieldPath() + ',';
                        mpLSfields.put(String.valueOf(f.getFieldPath()),String.valueof(f.getLabel()));
                    }
                    relatedChildSObjectsquery = relatedChildSObjectsquery.subString(0,relatedChildSObjectsquery.length() - 1);
                    relatedChildSObjectsquery = relatedChildSObjectsquery+ ' FROM ' 
                         + childObjectType.getDescribe().getName()
                         + ' WHERE '
                         + childObjectField.getDescribe().getName()
                         + ' = \'' 
                         + headsObject.Id
                         + '\'';
                    for (SObject childObject :Database.query(relatedChildSObjectsquery))
                    {
                        relatedObjects.add(new relatedObjectRow(childObject));
                    }
                }
                else if(childObjectType.getDescribe().getName() == STR_LOAN_CONTACT){
                    mpLCfields = new Map<String,String>();
                    Loan__c objLoan = (Loan__c)headsObject;
                    relatedChildSObjectsquery = 'SELECT ID,Name,  ';
                    for(Schema.FieldSetMember f : SObjectType.Loan_Contact__c.FieldSets.CloneLoanContact.getFields()) {
                        relatedChildSObjectsquery += f.getFieldPath() + ',';
                        mpLCfields.put(String.valueOf(f.getFieldPath()),String.valueof(f.getLabel()));
                    }
                    relatedChildSObjectsquery = relatedChildSObjectsquery.subString(0,relatedChildSObjectsquery.length() - 1);
                    relatedChildSObjectsquery = relatedChildSObjectsquery+ ' FROM ' 
                         + childObjectType.getDescribe().getName()
                         + ' WHERE '
                         + childObjectField.getDescribe().getName()
                         + ' = \'' 
                         + headsObject.Id
                         + '\'';
                    for (SObject childObject :Database.query(relatedChildSObjectsquery))
                    {
                        relatedObjects.add(new relatedObjectRow(childObject));
                    }
                }
                if (!relatedObjects.isEmpty())
                {
                    objectChildren.add(new relatedObjects(relatedObjects, 
                        childObjectType.getDescribe().getLabelPlural(), 
                        childObjectField.getDescribe().getName(),
                        String.valueOf(childObjectType.getDescribe().getSobjectType())));
                }
            }
        }
    }
  
    /* Perform the cloning process.
       First clone the parent, then all of the child objects. 
       Then redirect the user to the new object page.
    */
    public PageReference doClone()
    {
        if(selectedValue != null && selectedValue != ''){
            objLoanNew.put(STR_RATE_LOCK_EXETYPE,selectedValue);
        }
        try{
            insert objLoanNew;
        }
        catch(DMLException ex){
            if(!ex.getMessage().contains(STR_SUFFICIENT_ACCESS)){
                ApexPages.Message InsertionError = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage());
                ApexPages.addMessage(InsertionError);
            }
            return null;
        }
        Boolean check = cloneSelectedObjects();
        if(check == true){
            return null;
        }
        cloneThirdPartyReports();
        return new PageReference('/' + objLoanNew.Id);
    }
    //this function clone the Loan_Property__c reated third party report recods
    public void cloneThirdPartyReports(){
        List<Sobject> listReportClone = new List<Sobject>();
        List<Sobject> lstToClone = new List<Sobject>();
        List<Sobject> lstObj = new List<Sobject>();
        if(!mpOldToClone.isEmpty()){
            for(Loan_Property__c lp:[Select Id,
                                    (Select Id, Loan_Property__c 
                                    From Third_Party_Reports__r) From Loan_Property__c 
                                    where id =:mpOldToClone.keySet()])
            {
                for(Third_Party_Report__c tpr:lp.Third_Party_Reports__r){
                    tpr.OwnerId = userinfo.getUserId();
                    listReportClone.add((Sobject)tpr);
                }
            }
            if(listReportClone != null && listReportClone.size() > 0){
                lstToClone = cloneObjects(listReportClone);
                for(Sobject cObj:lstToClone){ 
                    cObj.put(STR_LOAN_PROPERTY,mpOldToClone.get(String.valueof(cObj.get(STR_LOAN_PROPERTY))).Id);
                    lstObj.add(cObj);
                }
                try{
                    insert lstObj;
                }catch(DMLException ex){
                    ApexPages.Message InsertionError1 = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage());
                    ApexPages.addMessage(InsertionError1);  
                    return;
                }
            }
        }
    }

    public PageReference Cancel()
    {
        return new PageReference('/' + objectID);
    }  
    /* Clone the selected child objects.
       Associate the cloned objects with the new cloned parent object.
    */
    public Boolean cloneSelectedObjects(){
        List<sObject> clonedObjectNew = new List<sObject>{};
        List<sObject> clonedObjects = new List<sObject>{};
        List<sObject> selectedRelatedObjects;

        for (relatedObjects relatedObject : objectChildren)
        {
            selectedRelatedObjects = new List<sObject>{};  
            clonedObjects = new List<sObject>{};  
            for (relatedObjectRow row : relatedObject.objectRows) 
            {
                if (row.selected)
                {
                    selectedRelatedObjects.add(row.obj);
                }
            }
            if (!selectedRelatedObjects.isEmpty())
            {
                clonedObjects = cloneObjects(selectedRelatedObjects);
                //map of parent loan property vs clone Loan property instance
                if(!mpOldToClone.isEmpty()){
                    for (sObject clone : mpOldToClone.values()){
                        clone.put(STR_LOAN_ID, headClone.Id);
                    }
                }
                for (sObject clone : clonedObjects)
                {
                    clone.put(relatedObject.relatedFieldName, headClone.Id);
                    clonedObjectNew.add(clone);
                }
            }
        }
        try{
            if(!mpOldToClone.isEmpty()){
                insert mpOldToClone.values();
            }
            if(clonedObjectNew != null && clonedObjectNew.size() > 0){
                insert clonedObjectNew;
            }
        }
        catch(DMLException ex){
            ApexPages.Message InsertionError = new ApexPages.Message(ApexPages.Severity.Error,  ex.getMessage());
            ApexPages.addMessage(InsertionError);
            if(headClone.Id != null){
                Loan__c objLoan = new Loan__c(Id = headClone.Id);
                delete objLoan;
            }
            return true;
        }
        return false;
    }
    /* Clone a list of objects to a particular object type
       Parameters 
       - List<sObject> sObjects - the list of objects to be cloned 
       The sObjects you pass in must include the ID field, 
       and the object must exist already in the database, 
       otherwise the method will not work.
    */
    public List<sObject> cloneObjects(List<sObject> sObjects){
        Schema.SObjectType objectType = sObjects.get(0).getSObjectType();
        // A list of IDs representing the objects to clone
        List<Id> sObjectIds = new List<Id>{};
        // A list of fields for the sObject being cloned
        List<String> sObjectFields = new List<String>{};
        // A list of new cloned sObjects
        List<sObject> clonedSObjects = new List<sObject>{};
        /* Get all the fields from the selected object type using 
           the get describe method on the object type.
        */
        Schema.DescribeSObjectResult objSchema = Loan_Term__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objSchema.fields.getMap();
        if(objectType != null)
        {
            for (Schema.SObjectField objField : objectType.getDescribe().fields.getMap().values())
            {
                Schema.DescribeFieldResult fieldDesc = objField.getDescribe();
                if(fieldDesc.getType() != DisplayType.LOCATION)
                {
                    sObjectFields.add(fieldDesc.Name);
                }
            }
        }
        /* If there are no objects sent into the method, 
           then return an empty list
        */
        if (sObjects != null || sObjects.isEmpty() || sObjectFields.isEmpty()){
            // Strip down the objects to just a list of Ids.
            for (sObject objectInstance: sObjects){
                sObjectIds.add(objectInstance.Id);
            }
            String allSObjectFieldsQuery = 'SELECT ' + sObjectFields.get(0);
            for (Integer i=1 ; i < sObjectFields.size() ; i++){
                allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
            }
            allSObjectFieldsQuery += ' FROM ' + 
                                   objectType.getDescribe().getName() + 
                                   ' WHERE ID IN (\'' + sObjectIds.get(0) + 
                                   '\'';
            for (Integer i=1 ; i < sObjectIds.size() ; i++){
                allSObjectFieldsQuery += ', \'' + sObjectIds.get(i) + '\'';
            }
            allSObjectFieldsQuery += ')';
            try{
                for (SObject sObjectFromDatabase:
                    Database.query(allSObjectFieldsQuery)){
                        String keyPre = String.valueOf(sObjectFromDatabase.get(STR_ID)).substring(0,3);
                        Sobject cloneSobj = sObjectFromDatabase.clone(false,true);
                        if(keyPrefixMap.get(keyPre) == STR_LOAN_PROPERTY){
                            mpOldToClone.put(String.valueOf(sObjectFromDatabase.get(STR_ID)),cloneSobj);
                        }
                        /* 30/04/15 : set the field Loan_Term__c.Final__c (field label is 'Select 
                           as Final') to False and set the cloned Loan Term RecordTypeName to 
                           be "Specific" and also blank out all of the fields under the 
                           'Final Loan Terms' and 'Pricing' sections of the page layout
                        */
                        else if(keyPrefixMap.get(keyPre) == STR_LOAN_TERM){
                            if(cloneSobj.get(STR_FINAL) != null){
                                  cloneSobj.put(STR_FINAL , false);
                            }
                            if(cloneSobj.get(STR_RECORD_TYPE) != null){
                                  cloneSobj.put(STR_RECORD_TYPE , objRecId.id);
                            }
                            /*for(Schema.FieldSetMember objfldMem : SObjectType.Loan_Term__c.FieldSets.Clone_Loan_Term_Null_Fields.getFields()) {
                                if(objfldMem.getType() != null && String.valueOf(objfldMem.getType()) != 'BOOLEAN'){
                                    cloneSobj.put(String.valueOf(objfldMem.getFieldPath()),null);
                                }
                                else{
                                    cloneSobj.put(String.valueOf(objfldMem.getFieldPath()),false);
                                } 
                            }*/
                            for (String strLTField : fieldMap.keySet()){
                                SObjectField sfield = fieldMap.get(strLTField);
                                schema.describefieldresult dfield = sfield.getDescribe();
                                if(dfield.isUpdateable()){
	                                if(!setFileds.contains(dfield.getName()) && dfield != null){
	                                    if(dfield.getType() != null && String.valueOf(dfield.getType()) == STR_BOOLEAN) 
	                                        cloneSobj.put(String.valueOf(dfield.getName()),false);
    	                                else
	                                       cloneSobj.put(String.valueOf(dfield.getName()),null);
	                                }
                                }
                            }
                            clonedSObjects.add(cloneSobj);
                        }
                        else{
                            clonedSObjects.add(cloneSobj);
                        }
                }
          } catch (exception e){
            system.debug('<<<<<<<<<<<eeeee '+e.getMessage());
          }
        }
        return clonedSObjects;
    }
    /* Related objects data construct - 
       used to store a collection of child objects connected to 
       the head object through the same relationship field.
    */
    public class relatedObjects
    {
        public List<relatedObjectRow> objectRows { get; set; }
        public String                 pluralLabel      { get; set; }
        public String                 relatedFieldName { get; set; }
        public String                 objApi           { get; set; }
        public relatedObjects(List<relatedObjectRow> objectRows, 
                              String pluralLabel, 
                              String relatedFieldName,String objApi) 
        {
          this.objectRows       = objectRows;
          this.pluralLabel      = pluralLabel;
          this.relatedFieldName = relatedFieldName;
          this.objApi           = objApi;
        }   
    }     
    /* An indidual child object row. 
       Each instance simply contains the object definition, 
       and a checkbox to select the row for cloning 
       on the clone plus page.
    */
    public class relatedObjectRow
    {
        public sObject obj      { get; set; }
        public Boolean selected { get; set; }
        public relatedObjectRow (Sobject obj)
        {
            this.obj = obj;
            String strObj = String.valueOf(obj.getSObjectType());
            if(strObj == STR_LOAN_PROPERTY || strObj == STR_LOAN_TERM || strObj == STR_LOAN_SPON){
                this.selected = true;
            }else{
                this.selected = false;
            }
        }
        public String getName(){
            try{
                return '' + obj.get(STR_NAME);
            } catch (Exception e){
                return '';
            }    
        }  
    }
}