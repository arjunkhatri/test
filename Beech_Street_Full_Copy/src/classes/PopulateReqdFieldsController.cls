/* 
 *   Descprition : Pop Up Window to Display Required Fields to move to the next status
 *   
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            31/08/2012         Initial Draft
 *   1.1            18/12/2012         Changed Logic to turn off the validation rules for certain profiles mentioned in Custom setting
 *   1.2            21/12/2012         Changes made to display Section of fields
 */
public class PopulateReqdFieldsController {
    //Constants
    public static final String PARAM_LOANID = 'Id';
    public static final String PARAM_LOAN_STATUS= 'status';
    public static final String PARAM_LOAN_PROGRAM = 'pgm';
    public static final String PARAM_LOAN_EXECTYPE = 'type';
    public static final String paramClass = 'Class';
    //public static final String SPONSOR = 'Sponsor';
    //public static final String BARROWERSTRUC = 'Borrower Structure';


    //Properties
    public map<String,LoanValidationConfig__c> loanValidationConfig {get;private set;}
    public map<String,list<String>> mapReqdFields {get; private set;} // map of record Id and missing fields
    public map<String,String> mapRecordName {get;private set;}   // map of record Id and name
    public map<String, Schema.SObjectType> schemaMap {get ; private set;} // map of sObject names of all objects
    public map<String, Integer> StatusToIndex {get ; private set;} // map of Loan status to index
    public Id loanId {get;private set;}
    public String loanStatus {get; private set;}
    public String loanProgram {get; private set;}
    public String LoanExecType {get; private set;}
    public String nextStatus {get; private set;}  // Holds next status of Loan Program
    public list<String> lstFields {get; private set;} // List to hold missing fields
    public ID loanSponser;
    // map of FieldSectionConfig custom setting records
    public static map<String,FieldSectionConfig__c> mapFieldSectionConfig = FieldSectionConfig__c.getAll(); 
    public map<String,String> mapObjectName {get;private set;}
    RequiredFieldsHelper objReturnHlp = new RequiredFieldsHelper();
    
    public map<Id,Set<String>> mapSection{get;private set;}
    public map<String,list<String>> mapSectionFields{get;private set;}
    public map<Id ,Boolean> mapSec{get;private set;} // used to render section part
    public map<Id ,Boolean> mapReFields{get;private set;} // used to render fields w/o section
    
    public map<Id,list<String>> mapFields {get;private set;} // map of fields not in CS
    
    //Constructor
    public PopulateReqdFieldsController(){}
    
    public void init(){
        mapSection = new map<id,Set<String>>(); //map of id and section
        mapSectionFields = new map<String,list<String>>(); //map of Id_section and fields
        mapFields = new map<Id,list<String>>();
        
        mapSec = new map<Id ,Boolean>();
        mapReFields = new map<Id ,Boolean>();
        
        mapRecordName = new map<String,String>();
        mapReqdFields = new map<String,list<String>>();
        mapObjectName = new map<String,String>();
        StatusToIndex = new map<String, Integer>();
        loanId = (Id)Apexpages.currentPage().getParameters().get(PARAM_LOANID);
        loanStatus = Apexpages.currentPage().getParameters().get(PARAM_LOAN_STATUS);
        loanProgram = Apexpages.currentPage().getParameters().get(PARAM_LOAN_PROGRAM);
        LoanExecType = Apexpages.currentPage().getParameters().get(PARAM_LOAN_EXECTYPE);
        if(loanId != null){
        	Loan__c loan = [Select AccountId__c from Loan__c where id = :loanId];
        	if(loan != null && loan.AccountId__c != null){
        		loanSponser = loan.AccountId__c;
        	}
        }
        // Fetch all the records from custom setting in a map
        loanValidationConfig = LoanValidationConfig__c.getAll();
        
        //Returns map of sObject names of all objects
        schemaMap = Schema.getGlobalDescribe();
        ValidateFieldsHelper objHelper = new ValidateFieldsHelper();
        
        //Code Modified :To bypass validation logic for certain profiles mentioned in CS
        if(checkUserProfile()){
        	List<String> listStatus = new List<String>();
        	Loan_Status_Config__c loanStatusCS = Loan_Status_Config__c.getInstance(loanProgram);
        		if(loanStatusCS != null && loanStatusCS.Status__c.contains(',')){
        			listStatus = loanStatusCS.Status__c.split(',');
        		}else if(loanStatusCS != null){ 
        			listStatus.add(loanStatusCS.Status__c);
        		}
        	Integer index = 0;
        	String tempLoanStatus = loanStatus;
        	for(String lnStatus : listStatus){
        		StatusToIndex.put(lnStatus,index);
        		index++;
        	}
	        for(Integer i = 0; i <= StatusToIndex.get(tempLoanStatus); i++){
	        	loanStatus = listStatus[i];	
	            for(LoanValidationConfig__c obj : loanValidationConfig.values()){
	                
	                //Code Modified : Execution Type Field added to Loan Validation Config Custom setting
	                Set<String> setExecutionType = new Set<String>();
	                
	                if(obj.Execution_Type__c != NULL){
	                    for(String execType : obj.Execution_Type__c.split(',')){
	                        setExecutionType.add(execType.trim().toLowerCase());
	                    }
	                }
	                
	                //Find match based on loan status , Execution Type and existing status
	                if(obj.Loan_Program__c != NULL && obj.Existing_Status__c != NULL && obj.New_Status__c != NULL &&
	                    obj.Object__c != NULL && obj.Required_Fields__c != NULL && obj.Execution_Type__c != NULL &&
	                    obj.Loan_Program__c.equalsIgnoreCase(loanProgram) && obj.Existing_Status__c.equalsIgnoreCase(loanStatus)
	                    && setExecutionType.contains(LoanExecType.toLowerCase()))
	                {
	                    objReturnHlp = objHelper.formQuery(null, obj.Object__c.trim(),obj.Required_Fields__c,loanId, paramClass,loanSponser);
	                    if(objReturnHlp != NULL)
	                    { 
	                        if(objReturnHlp.mapObject != null && objReturnHlp.mapObject.size() > 0)
	                            mapObjectName.putAll(objReturnHlp.mapObject);
	                        if(objReturnHlp.mapRecordName != null && objReturnHlp.mapRecordName.size() > 0)
	                            mapRecordName.putAll(objReturnHlp.mapRecordName);
	                        if(objReturnHlp.mapReqdFields != null && objReturnHlp.mapReqdFields.size() > 0)
	                            mapReqdFields.putAll(objReturnHlp.mapReqdFields);
	                    }
	                    if(mapReqdFields != NULL && mapReqdFields.size() != 0){
	                        nextStatus = obj.New_Status__c;
	                    }
	                }
	            }
        	}   
            
            map<String,Id> mapObjectField = new map<String,Id>();
            for( Id id : mapReqdFields.keySet()){
                for(String strFields : mapReqdFields.get(id)){
                    mapObjectField.put(mapObjectName.get(id)+'_'+strFields+'_'+id , id);
                }
            }

            for(String objField : mapObjectField.KeySet()){
                
                String[] objFieldId  = objField.split('_');
                
                String obj = objFieldId [0];
                String field = objFieldId[1];
                String Id = objFieldId[2]; 
                
                Boolean checkCSRecord = false;
                
                for(FieldSectionConfig__c objFieldConfig : mapFieldSectionConfig.values()){
                    if(objFieldConfig.Object__c.toLowerCase() == obj.toLowerCase() && 
                       objFieldConfig.Field__c.toLowerCase() == field.toLowerCase() && objFieldConfig.Section__c != NULL ){
                        
	                        checkCSRecord = true;
	                        if(!mapSection.containsKey(Id ))
	                            mapSection.put(id,new Set<String>{objFieldConfig.Section__c});
	                        else if (mapSection.size() != NULL && mapSection.containsKey(id)){
	                            mapSection.get(id).add(objFieldConfig.Section__c);
	                        }
	                        
	                        if(!mapSectionFields.containsKey(id+'_'+objFieldConfig.Section__c)){
			                    /*if(field == SPONSOR){
			                        field = BARROWERSTRUC;
			                    }*/
	                            mapSectionFields.put(id+'_'+objFieldConfig.Section__c ,new List<String>{field});
	                            }
	                        else if(mapSectionFields.size() != NULL  && mapSectionFields.containsKey(id+'_'+objFieldConfig.Section__c)){
                                /*if(field == SPONSOR){
                                    field = BARROWERSTRUC;
                                }*/
	                            mapSectionFields.get(id+'_'+objFieldConfig.Section__c).add(field);
	                        }
                        }
                    }
                    
                    if(checkCSRecord == false){
                    	/* map of fields that dont have section */
                    	if(!mapFields.containsKey(id))
                        	mapFields.put(id,new List<String>{field});
                        else if(mapFields.size() != NULL  && mapFields.containsKey(id)){
                        	mapFields.get(id).add(field);
                    	}
                    
                	}
                
	                if(!mapSection.containsKey(id)){
	                    mapSec.put(id,false);
	                }else{
	                    mapSec.put(id,true);
	                }
	                
	                if(!mapFields.containsKey(id)){
	                    mapReFields.put(id,false);
	                }else{
	                    mapReFields.put(id,true);
	                }
            }     
        }
    }
    
    // Function to check if the custom setting contains the profile then bypass validation logic
    public Boolean checkUserProfile(){
        LoanValidationBypassConfig__c objLVC= 
                                    LoanValidationBypassConfig__c.getInstance([Select Profile.Name 
                                                                               from User 
                                                                               where id = :Userinfo.getUserId()].Profile.Name);
        return objLVC != NULL ? false : true;
    }//End
    
    // Test Methods
    static testMethod void PopulateRedfFieldsControllerTest(){
        Profile objProfile = [Select Id, Name From Profile Where Name='Loan Originator' limit 1];
        UserRole objUserRole = [Select Name, Id From UserRole where Name='Underwriter' limit 1];
        
        User objTestUser = new User();
        objTestUser.ProfileId = objProfile.Id; 
        objTestUser.IsActive = true;
        objTestUser.FirstName = 'testName';
        objTestUser.Username = 'testName@yahoo.com';
        objTestUser.LastName = 'TestLast';
        objTestUser.Email = 'testName@gmail.com';
        objTestUser.Alias = 'test';
        objTestUser.CommunityNickname = 'testNickName123@';
        objTestUser.TimeZoneSidKey = 'America/New_York';
        objTestUser.LocaleSidKey = 'en_US';
        objTestUser.EmailEncodingKey = 'ISO-8859-1';   
        objTestUser.LanguageLocaleKey = 'en_US';
        objTestUser.UserRoleId = objUserRole.Id;
        insert objTestUser;
        
        system.runAs(objTestUser)
        {
            LoanValidationBypassConfig__c valiConfig = new LoanValidationBypassConfig__c(Name='Test User');
            insert valiConfig;
        
            Loan_Status_Config__c statusConfig = new Loan_Status_Config__c(Name='test pgm',Status__c=System.Label.LoanReqdFields_LoanStatus);
            insert statusConfig;
        
            LoanValidationConfig__c validationConfig = new LoanValidationConfig__c(Name='Test',Existing_Status__c='Prospecting',
                            Loan_Program__c='test pgm',New_Status__c='Pre-screen (Origination)',Object__c='Loan__c',
                            Required_Fields__c ='Name,Comments__c',Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig;
        
            LoanValidationConfig__c validationConfig1 = new LoanValidationConfig__c(Name='Test1',Existing_Status__c='Prospecting',
                            Loan_Program__c='test pgm',New_Status__c='Pre-screen (Origination)',Object__c='Loan__c',
                            Required_Fields__c ='DSCR__c,Comments__c',Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig1;
        
            Loan__c testLoan = new Loan__c(Name='test',Loan_Program__c ='test pgm', Loan_Status__c = 'Prospecting',
                                            Investor_Type__c = 'Freddie');
            insert testLoan;
            
            Pagereference populateFields = Page.PopulateReqdFields;
            map<String, String> mapParams=populateFields.getParameters();
            
            mapParams.put(PopulateReqdFieldsController.PARAM_LOANID,testLoan.Id);
            mapParams.put(PopulateReqdFieldsController.PARAM_LOAN_STATUS,testLoan.Loan_Status__c);
            mapParams.put(PopulateReqdFieldsController.PARAM_LOAN_PROGRAM ,testLoan.Loan_Program__c);
            mapParams.put(PopulateReqdFieldsController.PARAM_LOAN_EXECTYPE ,testLoan.Investor_Type__c);
            test.setCurrentPage(populateFields);
            
            test.startTest();
            PopulateReqdFieldsController populateFieldsCntl = new PopulateReqdFieldsController();
    
            populateFieldsCntl.init();
            system.assertEquals(testLoan.Id, populateFieldsCntl.loanId);
            system.assertEquals(testLoan.Loan_Program__c, populateFieldsCntl.loanProgram);
            system.assertEquals(testLoan.Loan_Status__c,populateFieldsCntl.loanStatus);
            
            PopulateReqdFieldsController objController = new PopulateReqdFieldsController();
            List<String> missingFields = populateFieldsCntl.mapReqdFields.get(populateFieldsCntl.loanId);
            system.assertEquals(missingFields,populateFieldsCntl.mapReqdFields.get(testLoan.id));
            
            test.stopTest();
        }
    }  
}