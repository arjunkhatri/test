/*
 *   Description - Trigger handler for COntactTrigger
 *    Version          Date          Description
 *     1.0           24/04/2013     On delete of Contact update delete Loan Sponsor records to fire
 * 									Loan Sponsor trigger 
 *
 */

public with sharing class ContactTriggerHandler {
	public static map<Id,List<Loan_Sponsor__c>> mpConToLSpon;
	public static final String frmDelete = 'Delete';
	public static final String frmUpdate = 'Update';
	
	public void onBeforeDelete(list<Contact> lstContact){
		this.updateLoanSponsor(lstContact);		
	}
	
	public void onBeforeUpdate(list<Contact> lstContact,map<Id,Contact> oldConMap){
		deleteUpdateLoanSponsor(lstContact,frmUpdate,oldConMap);		
	}
	public void onAfterDelete(list<Contact> conttList, map<Id,List<Loan_Sponsor__c>> mpContToLSponRec){
		this.mergeLoanSponsor(conttList,mpContToLSponRec);
	}	
	
	//Function used to update LoanSponsor record on delete of Contact
	public void updateLoanSponsor(list<Contact> ContactList){
		set<Id> contactIdSet = new set<Id>();
		list<Loan_Sponsor__c> updatedLSList = new list<Loan_Sponsor__c>();
		mpConToLSpon = new map<Id,List<Loan_Sponsor__c>>();
		for(Contact objCon : ContactList){
			contactIdSet.add(objCon.Id);
		}
		//query the Loan Sponsor records
		for(Loan_Sponsor__c objSponsor : [Select Id,Contact__c,isDeleted from Loan_Sponsor__c where
											Contact__c IN : contactIdSet]){	
			//set the entity to null as the record is deleted to fire the LS trigger
			if(objSponsor.Contact__c != null && !mpConToLSpon.containsKey(objSponsor.Contact__c))
			{
				mpConToLSpon.put(objSponsor.Contact__c,new List<Loan_Sponsor__c>{objSponsor});
			}
			else if(objSponsor.Contact__c != null && mpConToLSpon.containsKey(objSponsor.Contact__c))
			{
				mpConToLSpon.get(objSponsor.Contact__c).add(objSponsor);
			}			
		}
	}	
	
	//Function used to merge loan sponser when Contact get merged
	public void mergeLoanSponsor(list<Contact> contList, map<Id,List<Loan_Sponsor__c>> mpContToLSponRec){
		list<Loan_Sponsor__c> updatedLSponList = new list<Loan_Sponsor__c>();
		list<Loan_Sponsor__c> delLSponList = new list<Loan_Sponsor__c>();
		for(Contact conRec:contList){
           if(!mpContToLSponRec.isEmpty() && mpContToLSponRec.containsKey(conRec.id)){
	           	if(String.isNotBlank(conRec.MasterRecordId) && conRec.MasterRecordId != null) {
	           		for(Loan_Sponsor__c lSpon:mpContToLSponRec.get(conRec.id)){
	           			lSpon.Contact__c = conRec.MasterRecordId;
	           			updatedLSponList.add(lSpon);
	           		}
	           	}
	           	else{
	           		for(Loan_Sponsor__c lSpon:mpContToLSponRec.get(conRec.id)){
	           			//lSpon.Contact__c = null;
	           			if(!lSpon.isDeleted)
	           			    delLSponList.add(lSpon);
	           		}
	           	}
           }
		}
		if(updatedLSponList.size() > 0)
			update updatedLSponList;	
	}

	public void deleteUpdateLoanSponsor(list<Contact> lstContact,String frmOp,map<Id,Contact> oldCMap){
		set<Id> setContactId = new set<Id>(); 
		list<Loan_Sponsor__c> lstLSToBeUpdated = new list<Loan_Sponsor__c>();
		for(Contact objContact : lstContact){
			if(oldCMap != null && oldCMap.containsKey(objContact.Id)){ 
				if((objContact.LastName != String.valueOf(oldCMap.get(objContact.Id).LastName)) || 
					(objContact.FirstName != String.valueOf(oldCMap.get(objContact.Id).FirstName))){
					setContactId.add(objContact.Id);
				}
			}
		}
		//query the Loan Sponsor records
		for(Loan_Sponsor__c objSponsor : [Select Id,Contact__c from Loan_Sponsor__c where
											Contact__c IN : setContactId]){

			if(frmOp.equalsIgnoreCase(frmUpdate)){
				//objSponsor.Contact__c = objSponsor.Contact__c;
				lstLSToBeUpdated.add(objSponsor);		
			}								
		}

		if(frmOp.equalsIgnoreCase(frmUpdate) && !lstLSToBeUpdated.isEmpty()){
			Database.SaveResult[] srList = Database.update(lstLSToBeUpdated,false);
			// Iterate through each returned result
			for (Database.SaveResult sr : srList) {  
			    if (!sr.isSuccess()){
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('Error' + ': ' + err.getMessage());
			        }
			    }
			}	
		}
	}
	//Test Method
	static testMethod void testDeleteLoanSponsor(){
        //Create Custom setting record for test role
        Report_LoanSponsor_Field__c reportCS = new Report_LoanSponsor_Field__c(Name ='TestRole', 
                                                                               Loan_Field__c='Report_Key_Principal__c');
        insert reportCS;

		Loan__c objLoan = new Loan__c(Name = 'Test Loan',Report_Key_Principal__c = '',Report_Principal__c = '',
										Loan_Program__c = 'Test',Loan_Status__c = 'status3');
	 	insert objLoan;
        
        Account objAcc = new Account(Name='testAcc');
        insert objAcc;

	 	Contact objContact = new Contact(Description = 'Test Contact',LastName ='Name');
	 	insert objContact;

	 	Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'Principal',Contact__c =objContact.Id, Entity__c = objAcc.Id,
	 	                                            Sponsor_is_the_Individual__c = true );
	 	insert objLS;

	 	Loan_Sponsor__c objLS1 = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'Key Principal',Contact__c =objContact.Id, Entity__c = objAcc.Id,
	 	                                             Sponsor_is_the_Individual__c = true );
	 	insert objLS1;

		test.startTest();
			objLoan = [Select Report_Key_Principal__c,Report_Principal__c from Loan__c where Id = : objLoan.Id];
			system.assertEquals(objLoan.Report_Key_Principal__c,objContact.Name);
			system.assertEquals(objLoan.Report_Principal__c,objContact.Name);
			delete objContact;
			
			objLoan = [Select Report_Key_Principal__c,Report_Principal__c from Loan__c where Id = : objLoan.Id];
			
			system.assertEquals(objLoan.Report_Key_Principal__c,NULL);
			system.assertEquals(objLoan.Report_Principal__c,NULL);
		test.stopTest();
		
	}
	
	static testMethod void testUpdateLoanSponsor(){

		//Create Custom setting record for test role
		Report_LoanSponsor_Field__c reportCS = new Report_LoanSponsor_Field__c(Name ='TestRole', 
																			   Loan_Field__c='Report_Key_Principal__c');
		insert reportCS;

		Loan__c objLoan = new Loan__c(Name = 'Test Loan',Report_Key_Principal__c = '',Report_Principal__c = '',
										Loan_Program__c = 'Test',Loan_Status__c = 'status3');
	 	insert objLoan;

        Account objAcc = new Account(Name='testAcc');
        insert objAcc;

	 	Contact objContact = new Contact(Description = 'Test Contact',LastName ='Name'); 
	 	insert objContact;

        Contact objContact1 = new Contact(Description = 'Test Contact1',LastName ='Name1'); 
        insert objContact1;

	 	Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'TestRole',Contact__c = objContact.Id , Entity__c = objAcc.Id,
	 	                                            Sponsor_is_the_Individual__c = true);
	 	insert objLS;

		test.startTest();
			objLoan = [Select Report_Key_Principal__c from Loan__c where Id = : objLoan.Id];
			system.assertEquals(objLoan.Report_Key_Principal__c,'Name');

			objContact.LastName = 'Test Con1';
			update objContact;
			objLoan = [Select Report_Key_Principal__c,Report_Principal__c from Loan__c where Id = : objLoan.Id];

			system.assertEquals(objLoan.Report_Key_Principal__c,'Test Con1');

		test.stopTest();
	} 
}