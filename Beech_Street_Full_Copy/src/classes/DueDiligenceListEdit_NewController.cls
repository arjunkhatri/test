public with sharing class DueDiligenceListEdit_NewController {
    public static final String PARAM_DDLID = 'Id'; 
    
    public string DDLName{get;private set;}
    public List<DDL_Item__c> dueDiligenceList{get;private set;}
    public boolean queryPerformed{get; private set;}
    public boolean userCanEditItems{get; set;}
    public string actionIds{get;set;}
     
    private ApexPages.StandardSetController controller;
    public ID DDLId{get;private set;}
    private Generic_Config__c genConfig;
        
    // Constructors
    public DueDiligenceListEdit_NewController(ApexPages.StandardSetController controller){
        genConfig = Generic_Config__c.getInstance();
        
        // must assume this is always coming from the DDL related list
        DDLId = ApexPages.currentPage().getParameters().get(PARAM_DDLID);
        DDL__c DDL = [select Name from DDL__c where id=:DDLId];
        DDLName = DDL.Name;
        queryPerformed = false;
        userCanEditItems = true;
    }
    
    // Methods
    public PageReference refreshList(){
        string soqlQuery = 'select id,'+genConfig.DueDiligenceEdit_QueryFields__c+' from DDL_Item__c where Due_Diligence_List__c = :DDLId '
            +'order by sort_order__c ASC NULLS LAST';
        dueDiligenceList = Database.query(soqlQuery);
        
        controller = new ApexPages.StandardSetController(dueDiligenceList);
        queryPerformed = true;
        
        // code to make sure that the item is editable
		try{
	        if (dueDiligenceList.size()>0){
				update dueDiligenceList.get(0);
	        }
	        else{
	        	DDL_Item__c item = new DDL_Item__c(Due_Diligence_List__c=DDLId); 
	        	insert item;
	        	delete item;
	        }
		}
    	catch(DMLException e){
			if (e.getDmlType(0) == System.StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY ||
				e.getDmlType(0) == System.StatusCode.INSUFFICIENT_ACCESS_OR_READONLY){
				userCanEditItems = false;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.DueDiligenceEdit_NoAccess));
			}
			else{
				throw e;
			}
			// otherwise squash this error.
    	}
        
        return null;
    }
    public PageReference newItem(){
        PageReference newItemPR = Page.DueDiligenceListNew_New;
        newItemPR.getParameters().put(DueDiligenceListNew_NewController.PARAM_DDLID, DDLId);
        controller.save();
        return newItemPR;
    }
    public PageReference save(){
        return controller.save();       
    }
    public PageReference cancel(){
        return controller.cancel();     
    }

    public PageReference remove(){
        controller.save();
        if (actionIds!=null && actionIds!=''){
            list<ID> removeIdList = actionIds.split(',');
            Database.Delete(removeIdList);
            actionIds='';
        }
        else{
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, System.Label.DueDiligenceEdit_NoSelectedItems));
        }
        refreshList();
        return null;
    }
    public PageReference setSentDate(){
        controller.save();
        if (actionIds!=null && actionIds!=''){
            list<ID> setList = actionIds.split(',');
            list<DDL_Item__c> updateList = new list<DDL_Item__c>(); 
            for(ID currId: setList){
                DDL_Item__c currDD = new DDL_Item__c(id=currId, Date_Sent__c=Date.Today());
                updateList.add(currDD);
            }
            update updateList;
            actionIds='';
        }
        else{
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, System.Label.DueDiligenceEdit_NoSelectedItems));
        }
        refreshList();
        return null;
    }

    
    // test methods
    public static testMethod void testThisClass(){
        Generic_Config__c genConfig = new Generic_Config__c(DueDiligenceEdit_QueryFields__c = 'Item_Desc__c,Comment__c,Date_Sent__c,Date_Received__c,Date_Approved__c');
        insert genConfig; 

        Loan__c testLoan = new Loan__c(Name='Test Loan', Loan_Program__c='Freddie Mac', Loan_status__c='Prospect',
        								Investor_Type__c = 'Freddie');
        insert testLoan;
        
        DDL__c testDDL = new DDL__c(Type__c='Borrower and Property',Loan__c=testLoan.Id);
        insert testDDL;
        
        PageReference testPr = Page.DueDiligenceListEdit_New;
        testPr.getParameters().put(DueDiligenceListEdit_NewController.PARAM_DDLID, testDDL.Id);
        
        Test.setCurrentPageReference(testPr);
        ApexPages.StandardSetController testSetController = new ApexPages.StandardSetController(new List<DDL_Item__c>());
        DueDiligenceListEdit_NewController testController = new DueDiligenceListEdit_NewController(testSetController);
        
        // check initial status
        System.assertEquals(false, testController.queryPerformed);
        //System.assertEquals('', testController.DDLName);
        
        // start with a loan with no DD items
        testController.refreshList();
        System.assertEquals(true, testController.queryPerformed);
        System.assertEquals(0, testController.dueDiligenceList.size());
        
        // lets pretend we added a DD item
        PageReference testNewItemRef = testController.newItem();
        System.assert(testNewItemRef.getUrl().startsWith(Page.DueDiligenceListNew_New.getUrl()));
        
        DDL_Item__c testDDListRecord1 = new DDL_Item__c(Item_Desc__c = 'Test Item1', Due_Diligence_List__c = testDDL.Id, Sort_Order__c=1);
        DDL_Item__c testDDListRecord2 = new DDL_Item__c(Item_Desc__c = 'Test Item2', Due_Diligence_List__c = testDDL.Id, Sort_Order__c=2);
        insert new DDL_Item__c[]{testDDListRecord1, testDDListRecord2};
        
        Test.setCurrentPageReference(testPr);
        testController.refreshList();
        System.assertEquals(2, testController.dueDiligenceList.size());
        
        // try saving
        testDDListRecord1 = (DDL_Item__c)testController.dueDiligenceList.get(0);
        testDDListRecord1.Comment__c = 'Test Comment';
        PageReference testSaveRef = testController.save();
        
        System.Debug('testSaveRef.getUrl():'+testSaveRef.getUrl());
        System.assert(testSaveRef.getUrl().startsWith('/'+((String)testDDL.Id).substring(0,15)));

        Test.setCurrentPageReference(testPr);
        testController.refreshList();
        System.assertEquals('Test Comment', ((DDL_Item__c)testController.dueDiligenceList.get(0)).Comment__c );
    
        // try setting sent date
        testController.actionIds = testDDListRecord1.Id+','+testDDListRecord2.Id;
        testController.setSentDate();
        List<DDL_Item__c> ddlQuery = [select id, Date_Sent__c from DDL_Item__c 
            where Date_Sent__c=:System.today() and Due_Diligence_List__c =: testDDL.Id];
        System.assertEquals(2, ddlQuery.size());
        
        // try removing multiple
        testController.actionIds = testDDListRecord1.Id+','+testDDListRecord2.Id;
        testController.remove();
        System.assertEquals(0, testController.dueDiligenceList.size());
        
        // try cancel
        PageReference testCancelRef = testController.cancel();
        System.assert(testCancelRef.getUrl().startsWith('/'+((String)testDDL.Id).substring(0,15)));
    }   
}