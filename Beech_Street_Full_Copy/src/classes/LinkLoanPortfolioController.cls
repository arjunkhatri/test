/*
 * Description   - Controller for LinkLoanPortfolio page . 
 *    
 *    Version          Date          Description
 *     1.0           26/04/2013      Search and New Portfolio from Loan	
 *   
 */
public with sharing class LinkLoanPortfolioController {
	public static final String PARAM_LOANID = 'LoanId'; 
	public static final String PARAM_FROMNAME = 'fn';
	
	//Properties
	public String fromName{get; private set;}
	public ID loanId{get;private set;}
	public String searchTerm{get;set;}
	public list<Portfolio__c> results{get;set;}
	public boolean searchPerformed{get;private set;}
	
	//Constructor
	public LinkLoanPortfolioController(){
		 // must assume this is always coming from the Load Button
		 loanId = ApexPages.currentPage().getParameters().get(PARAM_LOANID);
		 fromName = ApexPages.currentPage().getParameters().get(PARAM_FROMNAME);
		 searchTerm = '';	
		 searchPerformed= false;
		 results = new list<Portfolio__c>();
	}
	
	public PageReference goSearch(){
		searchPerformed = false;
		if (searchTerm!=null && searchTerm.trim()!=''){
			if (searchTerm.trim().length()<2){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_SearchTooShort));
			}else
			{
				Boolean assignAsterick = false ; 
				if(!searchTerm.contains('*')){
					searchTerm += '*';
					assignAsterick = true;
				}
				try
				{				
					results = (List<Portfolio__c>)[FIND :searchTerm RETURNING Portfolio__c(Name,Id,OwnerId) limit 100][0];
					if(assignAsterick)
					searchTerm= searchTerm.removeEnd('*');
				}catch(Exception e){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
				}
				searchPerformed = true;
				return null;
			}
		}else{
			results = new List<Portfolio__c>();
		}
		return null;
	}
	
	//Function to Link the Portfolio record to Loan when user clicks on record link
	public PageReference linkPortfolio(){
		try{
			Id portfolioId = Apexpages.currentPage().getParameters().get('resultID');
			Loan__c objLoan =new Loan__c (Id = loanId, Portfolio__c = portFolioId);
			update objLoan;
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return NULL;
		}
		return new PageReference('/'+loanId);
	} 
	
	//Function used to redirect to new page to create new Portfolio record
	public PageReference newItem(){
        PageReference newItem =Page.NewPortfolio;
        newItem.getParameters().put(PARAM_LOANID,loanId);
        return newItem;
    }
    
    //Test method
    public static testMethod void testLinkLoanPortfolio(){
    	Loan__c testLoan = new Loan__c(name='testLoan');
		insert testLoan;
		
		PageReference testPageReference = Page.LinkLoanPortfolio; 
		map<string, string> params = testPageReference.getParameters();
		params.put(LinkLoanPortfolioController.PARAM_LOANID,testLoan.Id );
		params.put(LinkLoanPortfolioController.PARAM_FROMNAME, testLoan.Name);
		
		Test.setCurrentPage(testPageReference);
		LinkLoanPortfolioController testController = new LinkLoanPortfolioController();
		
		// check defaults
		System.assertEquals('' ,testController.searchTerm);
		System.assertEquals(0 ,testController.results.size());
		System.assertEquals(false ,testController.searchPerformed);
		System.assertEquals('testLoan',testController.fromName);
		
		String link = 'apex/LinkLoanPortfolio?'+
			
		LinkLoanPortfolioController.PARAM_LOANID+'='+testLoan.Id+'&'+
		LinkLoanPortfolioController.PARAM_FROMNAME+'='+testLoan.Name;
	
		// setup a search
		Portfolio__c testPortfolio1 = new Portfolio__c(name='testPortfolio1');		
		Portfolio__c testPortfolio2 = new Portfolio__c(name='testPortfolio2');
		insert new Portfolio__c[]{testPortfolio1, testPortfolio2};
		
		// setup a bad search
	 	testController.searchTerm = '';
	 	testController.goSearch();
		
	 	System.assertEquals(0 ,testController.results.size());
	 	System.assertEquals(false ,testController.searchPerformed);
		
		// setup a too short search
		testController.searchTerm = 'a';
		testController.goSearch();
		
		System.assertEquals(0 ,testController.results.size());
		System.assertEquals(false ,testController.searchPerformed);
		
		//setup a search for testPortfollio 
		testController.searchTerm = 'testPor';
		Test.setFixedSearchResults(new Id[]{testPortfolio1.Id, testPortfolio2.Id});
		testController.goSearch();
		
		System.assertEquals(2 ,testController.results.size());
		System.assertEquals(true ,testController.searchPerformed);
		//check if testPortfolio2 is assigned to loan
		ApexPages.currentPage().getParameters().put('resultId',testPortfolio2.Id );
		testController.linkPortfolio();
		
		testLoan = [Select Portfolio__c from Loan__c where Id =: testLoan.Id];
		System.assertEquals(testPortfolio2.Id ,testLoan.Portfolio__c);
		
		// check the new link
		PageReference newRecordPR = testController.newItem();
		map<string,string> newRecordParams = newRecordPR.getParameters();
		
		String newLink = '/apex/newportfolio?'+
			NewPortfolioController.PARAM_LOANID+'='+testLoan.Id;
			
		System.assertEquals(testLoan.Id ,newRecordParams.get('LoanId'));
    }
    
    //Test if loan Id is Null ,exception is catch
     public static testMethod void testNegativeCase(){
     	PageReference testPageReference = Page.LinkLoanPortfolio; 
		map<string, string> params = testPageReference.getParameters();
		params.put(LinkLoanPortfolioController.PARAM_LOANID,NULL);
		
		Test.setCurrentPage(testPageReference);
		LinkLoanPortfolioController testController = new LinkLoanPortfolioController();  
		
    	testController.linkPortfolio();
    	List<Apexpages.Message> msgs = ApexPages.getMessages();
    	system.assert(msgs[0].getDetail().contains('Update failed'));
	}
}