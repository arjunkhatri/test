@isTest
public with sharing class EmailRequiredFieldsBatchTest {
	 static testMethod void testEmailRequiredFieldsBatch(){
        Profile objProfile = [Select Id, Name From Profile Where Name='Loan Originator' limit 1];
        UserRole objUserRole = [Select Name, Id From UserRole where Name='Underwriter' limit 1];
        
        User objTestUser = new User();
        objTestUser.ProfileId = objProfile.Id; 
        objTestUser.IsActive = true;
        objTestUser.FirstName = 'testName';
        objTestUser.Username = 'testName@yahoo.com';
        objTestUser.LastName = 'TestLast';
        objTestUser.Email = 'testName@gmail.com';
        objTestUser.Alias = 'test';
        objTestUser.CommunityNickname = 'testNickName123@';
        objTestUser.TimeZoneSidKey = 'America/New_York';
        objTestUser.LocaleSidKey = 'en_US';
        objTestUser.EmailEncodingKey = 'ISO-8859-1';   
        objTestUser.LanguageLocaleKey = 'en_US';
        objTestUser.UserRoleId = objUserRole.Id;
        insert objTestUser;	 	
        system.runAs(objTestUser)
        {	 	
	 	PriorDaysConfig__c pdConf = new PriorDaysConfig__c(Name = '3_Estimated_Closing_Date__c',
	 							PriorToDateField__c = 'Estimated_Closing_Date__c',Days__c = 3,Object__c = 'Loan__c');
	 	insert pdConf;
	 	
	 	MailDetailConfig__c mdc = new MailDetailConfig__c(Name = 'Addresses',
	 	ToAddress__c = 'Underwriter', Subject__c = 'Missing Fields {!Loan__c.Name}',
	 	Mail_Text__c = 'fields for {!Loan__c.Name} {!Loan__c.Status} {!Loan__c.Estimated_Closing_Date__c} {!Loan__c.Link} {!Loan__c.Name} {!Loan__c.RequiredFields}', 
	 	CCAddress__c='Originator',Mail_Text1__c ='test');
	 	insert mdc;


            Loan_Status_Config__c statusConfig = new Loan_Status_Config__c(Name='test pgm',Status__c=System.Label.LoanReqdFields_LoanStatus);
            insert statusConfig;
        
            LoanValidationConfig__c validationConfig = new LoanValidationConfig__c(Name='Test',Existing_Status__c='Prospecting',
                            Loan_Program__c='test pgm',New_Status__c='Pre-screen (Origination)',Object__c='Loan__c',
                            Required_Fields__c ='Name,Comments__c',Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig;
        
            LoanValidationConfig__c validationConfig1 = new LoanValidationConfig__c(Name='Test1',Existing_Status__c='Prospecting',
                            Loan_Program__c='test pgm',New_Status__c='Pre-screen (Origination)',Object__c='Loan__c',
                            Required_Fields__c ='DSCR__c,Comments__c',Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig1;
        	
        	Loan_Relationship_Config__c lrc1 = new Loan_Relationship_Config__c(Name = 'Contact',Loan_Field__c = 'LoanId__c,Loan__c',
        							 Related_Objects__c = 'Loan_Contact__c,Loan_Sponsor__c', Relationship_Field__c= 'ContactId__c,Contact__c',
        							 Type__c= 'Many-to-Many');
        	insert lrc1;

        	Loan_Relationship_Config__c lrc2 = new Loan_Relationship_Config__c(Name = 'Property__c',Loan_Field__c = 'LoanId__c',
        							 Related_Objects__c = 'Loan_Property__c', Relationship_Field__c= 'PropertyId__c',
        							 Type__c= 'Many-to-Many');
        	insert lrc2;
        	        	Loan_Relationship_Config__c lrc3 = new Loan_Relationship_Config__c(Name = 'Loan__c',Loan_Field__c = 'Id',
        							 Related_Objects__c = '', Relationship_Field__c= '',Type__c= 'Self');
        	insert lrc3;
        	        	Loan_Relationship_Config__c lrc4 = new Loan_Relationship_Config__c(Name = 'Loan_Term__c',Loan_Field__c = 'LoanId__c',
        							 Related_Objects__c = '', Relationship_Field__c= '',
        							 Type__c= 'One-to-Many');
        	insert lrc4;
        	        	
            Loan__c testLoan = new Loan__c(Name='test',Loan_Program__c ='test pgm', Loan_Status__c = 'Prospecting',
                                            Investor_Type__c = 'Freddie',Estimated_Closing_Date__c = date.today().addDays(3));
            insert testLoan;
            Deal_Team__c  dt1 = new Deal_Team__c (Role__c = 'Underwriter',LoanId__c = testLoan.id,UserId__c =objTestUser.id);
            	insert dt1;		 	
	 	
 		EmailRequiredFieldsBatch emailBatch = new EmailRequiredFieldsBatch();
 			database.executeBatch(emailBatch);
	 }
	 }

}