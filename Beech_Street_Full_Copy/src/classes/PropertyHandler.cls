/*
 *  Description - Trigger Handler for PropertyTrigger
 *  
 *    Version          Date          Description
 *     1.0           24/04/2013     -On update of Property update the Loan Report Fields
 *                                  -On delete of Property,delete the Loan Property related to 
 *                                   property to fire the LoanProperty trigger.
 *     1.2           27/05/15       -Added Logic for handling Case Sensitivity while populating 
 *                                   Loan's "Report Property" fields (Name, Street, and City )
 *                                   from Property.
 */

public with sharing class PropertyHandler {
    public set<String> setFields = new set<String>{'Report_Property_City__c','Report_Property_Name__c','Report_Property_Street__c'};
    //set to hold property fields
    public set<String> setpFields = new set<String>{'Name','City__c','Address__c'};
    //Function called on before Insert event
    public void onBeforeInsert(list<Property__c> lstProperty){
        populateCBSA(null,lstProperty);
    }
    
    //Function called on before update event
    public void onBeforeUpdate(map<Id,Property__c> mapOldProperty,list<Property__c> lstProperty){
        populateCBSA(mapOldProperty,lstProperty);
    }
    
    public void onBeforeDelete(list<Property__c> lstProperty){
        deleteLoanProperty(lstProperty);
    }
    
    public void onAfterUpdate(map<Id,Property__c> mapNewProperty,map<Id,Property__c> mapOldProperty){
        this.updateLoanReportFields(mapNewProperty, mapOldProperty);
        //this.updateFinalUnderwritten(mapNewProperty, mapOldProperty);
    }
    
    //Function used to populate CBSA field
    public void populateCBSA(map<Id,Property__c> mapOldProperty,list<Property__c> lstProperty){
        
        set<String> setPostalCode = new set<String>();
        map<String,Zip_Code__c> mapZipCodesCBSA = new map<String,Zip_Code__c>();
        
        for(Property__c objProperty : lstProperty){
            //If the postal code of the property is inserted/updated
            if(mapOldProperty == null || (mapOldProperty != NULL &&
                                          mapOldProperty.get(objProperty.Id).Postal_Code__c != objProperty.Postal_Code__c )){
                setPostalCode.add(objProperty.Postal_Code__c);
            }
        }
        
        if(!setPostalCode.isEmpty()){
            //Query the Zip Code object to get the CBSA__c where Zip Code Name = objProperty.Postal_Code__c
            for(Zip_Code__c zipCode : [Select Name, 
                                              CBSA__c,
                                              Zip_Code_County__c
                                       from Zip_Code__c 
                                       where Name IN: setPostalCode]){
            
                mapZipCodesCBSA.put(zipCode.Name,zipCode);      
            }
            //Query CBSA where NAme is Unknown
            list<CBSA__c> lstCBSA = [Select Id,
                                             Name
                                      from CBSA__c
                                      where Name =: 'UNKNOWN' LIMIT 1];
            
            //Now set the value of Property__c.CBSA__c field
            for(Property__c objProperty : lstProperty){
                
                if(mapOldProperty == null || (mapOldProperty != NULL &&
                                              mapOldProperty.get(objProperty.Id).Postal_Code__c != objProperty.Postal_Code__c )){
                    //also consider If its null
                    if(objProperty.Postal_Code__c != NULL &&
                       objProperty.Postal_Code__c != '' &&
                       !mapZipCodesCBSA.isEmpty() && mapZipCodesCBSA.containsKey(objProperty.Postal_Code__c))
                    {
                        if(mapZipCodesCBSA.get(objProperty.Postal_Code__c).CBSA__c != null )
                            objProperty.CBSA__c = mapZipCodesCBSA.get(objProperty.Postal_Code__c).CBSA__c;
                        else if(objProperty.CBSA__c != null && lstCBSA.size() > 0){
                            objProperty.CBSA__c = lstCBSA[0].Id; 
                        }
                        //if(mapZipCodesCBSA.get(objProperty.Postal_Code__c).Zip_Code_County__c != null )
                        objProperty.County__c = mapZipCodesCBSA.get(objProperty.Postal_Code__c).Zip_Code_County__c;
                    }else if(!lstCBSA.isEmpty()){
                        //If match is not found get the id; of the UNKNOWN Record
                        objProperty.CBSA__c = lstCBSA[0].Id; 
                        objProperty.County__c = null;
                    }else{
                        objProperty.CBSA__c = null;
                        objProperty.County__c = null;
                    }
                }
            }   
        }   
    }
    
    //Function used to delete Loan Property related to property on delete of property
    public void deleteLoanProperty(list<Property__c> lstProperty){
        list<Loan_Property__c> lstLPToBeDeleted = new list<Loan_Property__c>();
        set<Id> setPropertyId = new set<Id>();
        
        for(Property__c objProperty : lstProperty){
            setPropertyId.add(objProperty.Id);
        }
        //fetch all the LP and delete them  
        for(Loan_Property__c objLP :[Select LoanId__c,Id ,PropertyId__c
                                         From Loan_Property__c where PropertyId__c IN :setPropertyId]){
            lstLPToBeDeleted.add(objLP);                                
        }
        
        if(!lstLPToBeDeleted.isEmpty())
            delete lstLPToBeDeleted;
    }
    
    //Function used to update the Loan Fields on edit of Property
    public void updateLoanReportFields(map<Id,Property__c> mapNewProperty,map<Id,Property__c> mapOldProperty){
        set<Id> setPropertyId = new set<Id>();
        set<Id> setLoanId = new set<Id>();
        set<Id> setLPId = new set<Id>();
        LoanPropertyHandler objLPHandler = new LoanPropertyHandler();
        //map of property id and its loan id
        map<Id,set<Id>> mapOfPropertyLoan = new map<Id,set<Id>>();
        map<Id,Loan__c> mapUpdatedLoans = new map<Id,Loan__c>();
        
        //set of loan Id and cities that are in db related to that Loan
        map<Id,set<String>> mapLoanAndCity = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndCounty = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndName = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndState = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndAddress = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndPropertyType = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndUnitCount = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndCBSA = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndPostalCode = new map<Id,set<String>>();
        
        map<Id,set<String>> mapEditedLoanAndCity = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndCounty = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndName = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndState = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndAddress = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndPropertyType = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndUnitCount = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndCBSA = new map<Id,set<String>>();
        map<Id,set<String>> mapEditedLoanAndPostalCode = new map<Id,set<String>>();
        
        set<Id> setCBSA = new set<Id>();
        
        for(Property__c objProperty : mapNewProperty.values()){
            if(objProperty.CBSA__c != mapOldProperty.get(objProperty.Id).CBSA__c){
                setCBSA.add(objProperty.CBSA__c);
                setCBSA.add(mapOldProperty.get(objProperty.Id).CBSA__c);
            }
        }
        //Query the CBSA records to fetch the new and old CBSA names
        map<Id,CBSA__c> mapCBSA = new map<Id,CBSA__c>([Select Id,Name from CBSA__c where Id IN: setCBSA]);
        
        list<Property__c> listEditedProperties = new list<Property__c>();
        for(Property__c objProperty : mapNewProperty.values()){
            
            //If any of the values have changed
             if(checkCondition(objProperty.City__c,mapOldProperty.get(objProperty.Id).City__c,'City__c') ||
                checkCondition(objProperty.Postal_Code__c,mapOldProperty.get(objProperty.Id).Postal_Code__c,'Postal_Code__c') ||
                checkCondition(objProperty.County__c,mapOldProperty.get(objProperty.Id).County__c,'County__c') ||
                checkCondition(objProperty.Name,mapOldProperty.get(objProperty.Id).Name,'Name') ||
                checkCondition(objProperty.State__c,mapOldProperty.get(objProperty.Id).State__c,'State__c') ||
                checkCondition(objProperty.Address__c,mapOldProperty.get(objProperty.Id).Address__c,'Address__c') ||
                checkCondition(objProperty.Property_Type__c,mapOldProperty.get(objProperty.Id).Property_Type__c,'Property_Type__c') ||
                checkCondition(String.ValueOf(objProperty.Unit_Count__c),String.ValueOf(mapOldProperty.get(objProperty.Id).Unit_Count__c),'Unit_Count__c') ||
                (mapCBSA != null && mapCBSA.containsKey(objProperty.CBSA__c) && mapCBSA.containsKey(mapOldProperty.get(objProperty.Id).CBSA__c) &&
                    checkCondition(mapCBSA.get(objProperty.CBSA__c).Name,
                                    mapCBSA.get(mapOldProperty.get(objProperty.Id).CBSA__c).Name,'CBSA__r.Name') )){
                    setPropertyId.add(objProperty.Id);
                    listEditedProperties.add(objProperty);
            }            
        }
        
        if(!setPropertyId.isEmpty()){
            //query all the Lp where property Id is in setPropertyId
            for(Loan_Property__c objLP :[Select LoanId__c,Id ,PropertyId__c
                                         From Loan_Property__c where PropertyId__c IN :setPropertyId]){
                if(objLp.LoanId__c != NULL)
                    setLoanId.add(objLP.LoanId__c); 
                setLPId.add(objLP.Id);      
                
                if(mapOfPropertyLoan.isEmpty() || (!mapOfPropertyLoan.isEmpty() && !mapOfPropertyLoan.containsKey(objLP.PropertyId__c)))
                    mapOfPropertyLoan.put(objLP.PropertyId__c,new set<Id>{objLP.LoanId__c});
                else if(!mapOfPropertyLoan.isEmpty() && mapOfPropertyLoan.containsKey(objLP.PropertyId__c)){
                    mapOfPropertyLoan.get(objLP.PropertyId__c).add(objLP.LoanId__c);
                }
                
                //Populate these map to store new values so that they are not removed
                mapEditedLoanAndCity = objLPHandler.populateMap(mapEditedLoanAndCity, 
                                                                mapNewProperty.get(objLP.PropertyId__c).City__c,
                                                                objLP.LoanId__c);
                mapEditedLoanAndCounty = objLPHandler.populateMap(mapEditedLoanAndCounty,
                                                                  mapNewProperty.get(objLP.PropertyId__c).County__c,
                                                                  objLP.LoanId__c);
                mapEditedLoanAndName = objLPHandler.populateMap(mapEditedLoanAndName,
                                                                mapNewProperty.get(objLP.PropertyId__c).Name,
                                                                objLP.LoanId__c);
                mapEditedLoanAndState = objLPHandler.populateMap(mapEditedLoanAndState,
                                                                 mapNewProperty.get(objLP.PropertyId__c).State__c,
                                                                 objLP.LoanId__c);
                mapEditedLoanAndAddress = objLPHandler.populateMap(mapEditedLoanAndAddress,
                                                                   mapNewProperty.get(objLP.PropertyId__c).Address__c,
                                                                   objLP.LoanId__c);
                mapEditedLoanAndPropertyType = objLPHandler.populateMap(mapEditedLoanAndPropertyType, 
                                                                        mapNewProperty.get(objLP.PropertyId__c).Property_Type__c,
                                                                        objLP.LoanId__c);
                mapEditedLoanAndUnitCount   = objLPHandler.populateMap(mapEditedLoanAndUnitCount,
                                                                      String.ValueOf( mapNewProperty.get(objLP.PropertyId__c).Unit_Count__c),
                                                                      objLP.LoanId__c);
                                                                      
                mapEditedLoanAndPostalCode  = objLPHandler.populateMap(mapEditedLoanAndPostalCode,
                                                                       mapNewProperty.get(objLP.PropertyId__c).Postal_Code__c,
                                                                      objLP.LoanId__c);                                                                   
                                                                            
                if(mapCBSA != null && mapCBSA.containsKey(mapNewProperty.get(objLP.PropertyId__c).CBSA__c))
                    mapEditedLoanAndCBSA = objLPHandler.populateMap(mapEditedLoanAndCBSA, 
                                                                    mapCBSA.get(mapNewProperty.get(objLP.PropertyId__c).CBSA__c).Name,
                                                                    objLP.LoanId__c);                   
            }
            
            map<Id,Loan__c> mapLoan = objLPHandler.populateLoanMap(setLoanId);
            
            // query DB LP and populate map to check duplicates
             for(Loan_Property__c objLP : [Select Id,PropertyId__r.Unit_Count__c,
                                                  PropertyId__r.Property_Type__c, 
                                                  PropertyId__r.County__c, 
                                                  PropertyId__r.City__c, 
                                                  PropertyId__r.Address__c, 
                                                  PropertyId__r.Name,
                                                  PropertyId__r.State__c, 
                                                  PropertyId__r.CBSA__r.Name,
                                                  PropertyId__r.Postal_Code__c, 
                                                  PropertyId__c,LoanId__c From Loan_Property__c 
                                                where LoanId__c IN:setLoanId and Id NOT IN: setLPId ]){
                mapLoanAndCity = objLPHandler.populateMap(mapLoanAndCity, objLP.PropertyId__r.City__c,objLP.LoanId__c);
                mapLoanAndCounty = objLPHandler.populateMap(mapLoanAndCounty, objLP.PropertyId__r.County__c,objLP.LoanId__c);
                mapLoanAndName = objLPHandler.populateMap(mapLoanAndName, objLP.PropertyId__r.Name,objLP.LoanId__c);
                mapLoanAndState = objLPHandler.populateMap(mapLoanAndState, objLP.PropertyId__r.State__c,objLP.LoanId__c);
                mapLoanAndAddress = objLPHandler.populateMap(mapLoanAndAddress, objLP.PropertyId__r.Address__c,objLP.LoanId__c);
                mapLoanAndPropertyType = objLPHandler.populateMap(mapLoanAndPropertyType, objLP.PropertyId__r.Property_Type__c,objLP.LoanId__c);
                mapLoanAndUnitCount = objLPHandler.populateMap(mapLoanAndUnitCount,String.ValueOf(objLP.PropertyId__r.Unit_Count__c),objLP.LoanId__c);
                mapLoanAndCBSA = objLPHandler.populateMap(mapLoanAndCBSA, objLP.PropertyId__r.CBSA__r.Name,objLP.LoanId__c);
                mapLoanAndPostalCode = objLPHandler.populateMap(mapLoanAndPostalCode, objLP.PropertyId__r.Postal_Code__c,objLP.LoanId__c);
                                        
            }
            
            //now loop thr the property and remove previous value and add new
            for(Property__c objProperty : listEditedProperties){
             if(!mapOfPropertyLoan.isEmpty() && mapOfPropertyLoan.containsKey(objProperty.Id))
              {
                for(Id loanId: mapOfPropertyLoan.get(objProperty.Id))
                {
                    Loan__c objLoan = (!mapUpdatedLoans.isEmpty() && mapUpdatedLoans.containsKey(loanId)) ? mapUpdatedLoans.get(loanId) :
                                                                        mapLoan.get(loanId);

                    objLoan.Report_Property_City__c = addAndRemoveRprtFields(objProperty.City__c,
                                                                             mapOldProperty.get(objProperty.Id).City__c,
                                                                             objLoan.Report_Property_City__c,
                                                                             mapLoanAndCity,
                                                                             ',',objLoan.Id,mapEditedLoanAndCity,'Report_Property_City__c');
                    
                    objLoan.Report_Property_County__c = addAndRemoveRprtFields(objProperty.County__c,
                                                                               mapOldProperty.get(objProperty.Id).County__c,
                                                                               objLoan.Report_Property_County__c,
                                                                               mapLoanAndCounty,
                                                                               ',',objLoan.Id,mapEditedLoanAndCounty,'');
                    objLoan.Report_Property_Name__c = addAndRemoveRprtFields(objProperty.Name,
                                                                               mapOldProperty.get(objProperty.Id).Name,
                                                                               objLoan.Report_Property_Name__c,
                                                                               mapLoanAndName,
                                                                               ',',objLoan.Id,mapEditedLoanAndName,'Report_Property_Name__c');
                    objLoan.Report_Property_State__c = addAndRemoveRprtFields(objProperty.State__c,
                                                                               mapOldProperty.get(objProperty.Id).State__c,
                                                                               objLoan.Report_Property_State__c,
                                                                               mapLoanAndState,
                                                                               ',',objLoan.Id,mapEditedLoanAndState,'');   
                    objLoan.Report_Property_Street__c = addAndRemoveRprtFields(objProperty.Address__c,
                                                                               mapOldProperty.get(objProperty.Id).Address__c,
                                                                               objLoan.Report_Property_Street__c,
                                                                               mapLoanAndAddress,
                                                                               ';',objLoan.Id,mapEditedLoanAndAddress,'Report_Property_Street__c');     
                    objLoan.Report_Property_Type__c = addAndRemoveRprtFields(objProperty.Property_Type__c,
                                                                             mapOldProperty.get(objProperty.Id).Property_Type__c,
                                                                             objLoan.Report_Property_Type__c,
                                                                             mapLoanAndPropertyType,
                                                                             ',',objLoan.Id,mapEditedLoanAndPropertyType,'');
                    objLoan.Report_Property_Unit_Count__c = addAndRemoveRprtFields(String.ValueOf(objProperty.Unit_Count__c),
                                                                        String.ValueOf(mapOldProperty.get(objProperty.Id).Unit_Count__c),
                                                                             objLoan.Report_Property_Unit_Count__c,
                                                                             mapLoanAndUnitCount,
                                                                             ',',objLoan.Id,mapEditedLoanAndUnitCount,'');
                                                                             
                    objLoan.Report_Property_Postal_Code__c = addAndRemoveRprtFields(objProperty.Postal_Code__c,
                                                                        mapOldProperty.get(objProperty.Id).Postal_Code__c,
                                                                             objLoan.Report_Property_Postal_Code__c,
                                                                             mapLoanAndPostalCode,
                                                                             ',',objLoan.Id,mapEditedLoanAndPostalCode,'');
                                                                                                                                        
                    if(mapCBSA != null && mapCBSA.containsKey(objProperty.CBSA__c) && mapCBSA.containsKey(mapOldProperty.get(objProperty.Id).CBSA__c))
                        objLoan.Report_Property_CBSA__c = addAndRemoveRprtFields(mapCBSA.get(objProperty.CBSA__c).Name,
                                                                             mapCBSA.get(mapOldProperty.get(objProperty.Id).CBSA__c).Name,
                                                                             objLoan.Report_Property_CBSA__c,
                                                                             mapLoanAndCBSA,
                                                                             ';',objLoan.Id,mapEditedLoanAndCBSA,'');
                                                                                                                                                                                                                                                                                                                                
                    if(objLoan != NULL)
                        mapUpdatedLoans.put(objLoan.Id,objLoan);    
                }
                    
              }
            }
        }
        if(!mapUpdatedLoans.isEmpty()){
            try{
                update mapUpdatedLoans.values();
            }catch(DmlException ex){
                for(Property__c pro:listEditedProperties){
                    pro.addError(Label.PropertyHandler_PropertyUpdateError);
                }
            }           
        }   
    }

    //Function to check if the reportfields value is changed
    public boolean checkCondition(String prevValue,String newValue,String fldpApi){
	    if(fldpApi != '' && !setpFields.contains(fldpApi)){
		    return (prevValue == NULL || prevValue == '' ||  newValue == NULL || newValue == '' ||
		           (prevValue != NULL && newValue != NULL && prevValue != '' && newValue != '' &&
		                    !prevValue.equalsIgnoreCase(newValue))
		               );
	    }
	    else{
		    return (prevValue == NULL || prevValue == '' ||  newValue == NULL || newValue == '' ||
		           (prevValue != NULL && newValue != NULL && prevValue != '' && newValue != '' &&
		                    !prevValue.equals(newValue))
		               );
	    }
    }
    
    public String addAndRemoveRprtFields(String newPropValue,String prevPropValue,
                                         String pReportField,map<Id,set<String>> mapLoanReprtField,
                                         String pSeparator,Id pLoanId,map<Id,set<String>> mapEditedReprtField,String fldApi)
    {
        if(fldApi != '' && !setFields.contains(fldApi)){
            if((newPropValue == NULL || newPropValue =='') ||
               (prevPropValue == NULL || prevPropValue == '' ) ||
                (newPropValue != NULL && prevPropValue != NULL && 
                                        !newPropValue.equalsIgnoreCase(prevPropValue)))
            {
                LoanPropertyHandler objLPHandler = new LoanPropertyHandler();
                //Add modified value of Property field to loan report field 
                pReportField = objLPHandler.checkDuplicatesAndAdd(pReportField,
                                                                  newPropValue,pSeparator);
                //remove previous value only if it is not there in new values
                if(prevPropValue != NULL && 
                    (mapEditedReprtField.isEmpty() || (!mapEditedReprtField.isEmpty() && !mapEditedReprtField.containsKey(pLoanId)) ||
                                (!mapEditedReprtField.isEmpty() && mapEditedReprtField.containsKey(pLoanId) &&  
                                            !mapEditedReprtField.get(pLoanId).contains(prevPropValue.toLowerCase()) 
                                )
                    ))
                    pReportField = objLPHandler.checkDuplicatesAndRemove(pLoanId,mapLoanReprtField,
                                                                     pReportField,
                                                                     prevPropValue,pSeparator);     
                
            }
        }else{
            if((newPropValue == NULL || newPropValue =='') ||
               (prevPropValue == NULL || prevPropValue == '' ) ||
                (newPropValue != NULL && prevPropValue != NULL && 
                                        !newPropValue.equals(prevPropValue)))
            {
                LoanPropertyHandler objLPHandler = new LoanPropertyHandler();
                
                //Add modified value of Property field to loan report field 
                pReportField = objLPHandler.checkDuplicatesAndAddSelected(pReportField,
                                                                  newPropValue,pSeparator);
                //remove previous value only if it is not there in new values
                if(prevPropValue != NULL && 
                    (mapEditedReprtField.isEmpty() || (!mapEditedReprtField.isEmpty() && !mapEditedReprtField.containsKey(pLoanId)) ||
                                (!mapEditedReprtField.isEmpty() && mapEditedReprtField.containsKey(pLoanId) /*&&    
                                            !mapEditedReprtField.get(pLoanId).contains(prevPropValue)*/ 
                                )
                    ))
                    pReportField = objLPHandler.checkDuplicatesAndRemoveSelected(pLoanId,mapLoanReprtField,
                                                                     pReportField,
                                                                     prevPropValue,pSeparator);     
            }       
        }
        return pReportField;
    }
    
    static testMethod void testDeleteLoanProperty(){
        
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Program__c = 'Test',Loan_Status__c = 'status3');
        insert objLoan;
        
        Property__c objProperty = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
                                                  City__c = 'Test City',State__c='LA',Postal_Code__c= '1234',
                                                  County__c = 'Test County',Unit_Count__c = 1.2);
        insert objProperty;
        
        Loan_Property__c objLP = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProperty.Id );
        insert objLP;
        
        Loan_Property__c objLP1 = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProperty.Id );
        insert objLP1;
        
        test.startTest();
        
            objProperty= [Select County__c,City__c,Name,State__c,Address__c,Property_Type__c,
                                 Unit_Count__c from Property__c where Id =: objProperty.Id ];
                                  
            objLoan = [Select  Report_Property_City__c,Report_Property_County__c,
                                Report_Property_Name__c,Report_Property_State__c,
                                Report_Property_Street__c,
                                Report_Property_Type__c,
                                Report_Property_Unit_Count__c
                       from Loan__c where Id = : objLoan.Id];
            system.assertEquals(objLoan.Report_Property_City__c,objProperty.City__c);
            system.assertEquals(objLoan.Report_Property_County__c,objProperty.County__c);
            system.assertEquals(objLoan.Report_Property_Name__c,objProperty.Name);
            system.assertEquals(objLoan.Report_Property_State__c,objProperty.State__c);
            system.assertEquals(objLoan.Report_Property_Street__c,objProperty.Address__c);
            system.assertEquals(objLoan.Report_Property_Type__c,objProperty.Property_Type__c);
            system.assertEquals(objLoan.Report_Property_Unit_Count__c,String.valueOf(objProperty.Unit_Count__c));
            
            delete objProperty;
            objLoan = [Select  Report_Property_City__c,Report_Property_County__c,
                                Report_Property_Name__c,Report_Property_State__c,
                                Report_Property_Street__c,
                                Report_Property_Type__c,
                                Report_Property_Unit_Count__c
                       from Loan__c where Id = : objLoan.Id];
            
            system.assertEquals(objLoan.Report_Property_City__c,NULL);
            system.assertEquals(objLoan.Report_Property_County__c,NULL);
            system.assertEquals(objLoan.Report_Property_Name__c,NULL);
            system.assertEquals(objLoan.Report_Property_State__c,NULL);
            system.assertEquals(objLoan.Report_Property_Street__c,NULL);
            system.assertEquals(objLoan.Report_Property_Type__c,NULL);
            system.assertEquals(objLoan.Report_Property_Unit_Count__c,NULL);
        test.stopTest();
        
    }
    
    static testMethod void testUpdateProperty(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Program__c = 'Test',Loan_Status__c = 'status3');
        insert objLoan;
        
        Property__c objProperty = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
                                                  City__c = 'Test City',State__c='LA',Postal_Code__c= '1234',
                                                  County__c = 'Test County',Unit_Count__c = 1.2);
        insert objProperty;
        
        Property__c objProperty1 = new Property__c(Name ='Test Prop',Property_Type__c = 'Office',Address__c ='Street',
                                                  City__c = 'Test City',State__c='LA',Postal_Code__c= '5678',
                                                  County__c = 'County',Unit_Count__c = 1.0);
        insert objProperty1;
        
        Loan_Property__c objLP = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProperty.Id );
        insert objLP;
        
        test.startTest();
            
            objProperty= [Select County__c,City__c,Name,State__c,Address__c,Property_Type__c,Postal_Code__c,
                                 Unit_Count__c from Property__c where Id =: objProperty.Id ];
            
            objLoan = [Select  Report_Property_City__c,Report_Property_County__c,
                                Report_Property_Name__c,Report_Property_State__c,
                                Report_Property_Street__c,Report_Property_Postal_Code__c,
                                Report_Property_Type__c,
                                Report_Property_Unit_Count__c
                       from Loan__c where Id = : objLoan.Id];
            
            system.assertEquals(objLoan.Report_Property_City__c,objProperty.City__c);
            system.assertEquals(objLoan.Report_Property_County__c,objProperty.County__c);
            system.assertEquals(objLoan.Report_Property_Name__c,objProperty.Name);
            system.assertEquals(objLoan.Report_Property_State__c,objProperty.State__c);
            system.assertEquals(objLoan.Report_Property_Street__c,objProperty.Address__c);
            system.assertEquals(objLoan.Report_Property_Type__c,objProperty.Property_Type__c);
            system.assertEquals(objLoan.Report_Property_Unit_Count__c,String.valueOf(objProperty.Unit_Count__c));
            system.assertEquals(objLoan.Report_Property_Postal_Code__c,objProperty.Postal_Code__c);
            
            Loan_Property__c objLP1 = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProperty1.Id );
            insert objLP1;
        
            objProperty.City__c = 'Test City 1';
            objProperty.Property_Type__c = 'Retail';
            objProperty.County__c= 'Test County 2 ';
            objProperty.Name= 'Test Property 2';
            objProperty.State__c= 'NY';
            objProperty.Address__c= 'Test Street 2';
            objProperty.Unit_Count__c= 3.5;
            objProperty.Postal_Code__c = '1212';
            update objProperty;
            
            objLoan = [Select  Report_Property_City__c,Report_Property_County__c,
                                Report_Property_Name__c,Report_Property_State__c,
                                Report_Property_Street__c,Report_Property_Postal_Code__c,
                                Report_Property_Type__c,
                                Report_Property_Unit_Count__c
                       from Loan__c where Id = : objLoan.Id];
            
            system.assert(objLoan.Report_Property_City__c.contains(objProperty.City__c));
            if(objLoan.Report_Property_County__c != null){
                system.assert(objLoan.Report_Property_County__c.trim().contains(objProperty.County__c.trim()));
            }
            system.assert(objLoan.Report_Property_Name__c.contains(objProperty.Name));
            system.assert(objLoan.Report_Property_State__c.contains(objProperty.State__c));
            system.assert(objLoan.Report_Property_Street__c.contains(objProperty.Address__c));
            system.assert(objLoan.Report_Property_Type__c.contains(objProperty.Property_Type__c));
            system.assert(objLoan.Report_Property_Unit_Count__c.contains(String.valueOf(objProperty.Unit_Count__c)));
            system.assert(objLoan.Report_Property_Postal_Code__c.contains(objProperty.Postal_Code__c));
        test.stopTest();
    }
    
    static testMethod void testPopulateMSACounty(){
        //Create MSA
        CBSA__c cbsa = new CBSA__c(Name = 'Test CBSA');
        insert cbsa;
        
        CBSA__c cbsa1 = new CBSA__c(Name = 'Test CBSA1');
        insert cbsa1;
        
        // Create Zip Code
        Zip_Code__c zip = new Zip_Code__c(Name = 'Test Zip', CBSA__c = cbsa.Id ,Zip_Code_County__c = 'Test County');
        insert zip;
        
        Zip_Code__c zip1 = new Zip_Code__c(Name = 'Other Zip', CBSA__c = cbsa1.Id ,Zip_Code_County__c = 'Other County');
        insert zip1;
        
        Zip_Code__c zip2 = new Zip_Code__c(Name = 'Zip', CBSA__c = cbsa.Id ,Zip_Code_County__c = null);
        insert zip2;
        
        Loan__c objLoan = new Loan__c(Name='Test',Loan_Amount__c = 200 , Term__c= 2 ,Loan_Program__c = 'TBD',
                                    Loan_Status__c = 'Pre-Screen (Underwriting)',
                                    Investor__c = 'Test investor',Investor_type__c = 'Test type');
        insert objLoan; 
        
        Loan__c objLoan2 = new Loan__c(Name='Test2',Loan_Amount__c = 200 , Term__c= 2 ,Loan_Program__c = 'TBD',
                                    Loan_Status__c = 'Pre-Screen (Underwriting)',
                                    Investor__c = 'Test investor',Investor_type__c = 'Test type');
        insert objLoan2; 
    
        // Create Property
        Property__c objProperty = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
                                                  City__c = 'Test City',State__c='LA',Postal_Code__c= 'Test Zip',
                                                  County__c = 'County',Unit_Count__c = 1.2);
        insert objProperty;
        
        Loan_Property__c objLP = new Loan_Property__c(  PropertyId__c = objProperty.Id, LoanId__c = objLoan.Id) ;
        insert objLP;
        
        Loan_Property__c objLP2 = new Loan_Property__c( PropertyId__c = objProperty.Id, LoanId__c = objLoan2.Id) ;
        insert objLP2;
        
        //Check if the MSA lookup and County is filled
        objProperty = [Select CBSA__c,County__c from Property__c where Id = : objProperty.Id ];
        
        system.assert(objProperty.CBSA__c == cbsa.Id);
        system.assert(objProperty.County__c == zip.Zip_Code_County__c);
        
        //Check if the postal code is changed then thats zips'CBSA and County is copied
        objProperty.Postal_Code__c = 'Other Zip';
        update objProperty;
        
        objProperty = [Select CBSA__c,County__c from Property__c where Id = : objProperty.Id ];
        
        system.assert(objProperty.CBSA__c == cbsa1.Id);
        system.assert(objProperty.County__c == zip1.Zip_Code_County__c);
        
        objProperty.Postal_Code__c = 'Zip';
        objProperty.County__c = 'County';
        update objProperty;
        
        objProperty = [Select CBSA__c,County__c from Property__c where Id = : objProperty.Id ];
        
        system.assert(objProperty.CBSA__c == cbsa.Id);
        system.assert(objProperty.County__c == zip2.Zip_Code_County__c);

        update objProperty;
    }  
}