/*
	Batch class sending email to deal team members of loans whos Loan Property Third party Reports 
	Comment due date prior to 5 days
*/
global class ReportCommentsDueAlertBatch implements Database.Batchable<Third_Party_Report__c>,Schedulable{
	public map<Id,List<String>> mpLoanToDtMem;
	public map<Id,List<String>> mpLoanToDtMemCC;
	public MailDetailConfig__c mailConfig; 
	public List<PriorDaysConfig__c> pDays;
	public set<String> setToEmailAdd;
	public set<String> setCCEmailAdd;
	public static final String TPREPORT= 'Third_Party_Report__c';
	public static final String CONFIGNAME = 'ReportCommentsDue';
	public static final String EXETYPE = 'Fannie';		
	public ReportCommentsDueAlertBatch(){
		//read custom setting configurations
		mailConfig = MailDetailConfig__c.getInstance(CONFIGNAME);
		pDays = PriorDaysConfig__c.getall().values();
	}
	
    global Iterable<Third_Party_Report__c> start(Database.BatchableContext BC){
    	//collect all third party report whose loan Execution type is Fannie 
    	return ([Select Id,
    					Name,
    					Loan_Property__r.LoanId__c, 
    					Loan_Property__r.LoanId__r.Name,
    					Loan_Property__r.LoanId__r.Estimated_Closing_Date__c,
    					Loan_Property__c,
    					Report_Comments_Due__c 
    					From Third_Party_Report__c 
    					where Loan_Property__r.LoanId__r.Investor_Type__c = :EXETYPE]);	
    }
    
    global void execute(SchedulableContext sc) {
    	database.executebatch(new ReportCommentsDueAlertBatch());
    }
    
	global void execute(Database.BatchableContext BC, List<Third_Party_Report__c> scope){
		List<Third_Party_Report__c> listTPRRec = new List<Third_Party_Report__c>();
		mpLoanToDtMem = new map<Id,List<String>>();
		mpLoanToDtMemCC = new map<Id,List<String>>();
		List<Id> lstLoanIds = new List<Id>();
		Set<Integer> setDays = new Set<Integer>();
		for(PriorDaysConfig__c pdc:pDays){
			if(pdc.Object__c == TPREPORT)
				setDays.add(Integer.valueOf(pdc.Days__c));
		}
		for(Third_Party_Report__c tprRec:scope){
			//collect records whos prior to 5 days of field Report_Comments_Due__c
			if(tprRec.Report_Comments_Due__c != null){
				Integer days = Date.today().daysBetween(tprRec.Report_Comments_Due__c);
				if(setDays.contains(days)){
					listTPRRec.add(tprRec);
					lstLoanIds.add(tprRec.Loan_Property__r.LoanId__c);
				}
			}
		}
		if(lstLoanIds != null && lstLoanIds.size() > 0){
			getLoanAndDTeam(lstLoanIds);
		}
		//send email alert
		if(listTPRRec != null && listTPRRec.size() > 0)
			sendReportCommentsDueAlert(listTPRRec);
	}   
	
	//function to get loan vs deal team member ids
	private void getLoanAndDTeam(List<Id> loanIds){
		set<String> setRoles = new set<String>();
		set<String> setCCRoles = new set<String>();
		setCCEmailAdd = new set<String>();
		setToEmailAdd = new set<String>();
		if(mailConfig.ToAddress__c != null && mailConfig.ToAddress__c != ''){
			if(mailConfig.ToAddress__c.contains(',')){
				for(String strAd:mailConfig.ToAddress__c.split(',')){
					if(!strAd.trim().contains('@'))
						setRoles.add(strAd.trim());
					else
						setToEmailAdd.add(strAd.trim());						
				}
			}
			else{
				if(!mailConfig.ToAddress__c.contains('@'))
					setRoles.add(mailConfig.ToAddress__c.trim());	
				else
					setToEmailAdd.add(mailConfig.ToAddress__c.trim());	
			}
		}
		
		if(mailConfig.CCAddress__c != null && mailConfig.CCAddress__c != ''){
			if(mailConfig.CCAddress__c.contains(',')){
				for(String strAd:mailConfig.CCAddress__c.split(',')){
					if(!strAd.trim().contains('@'))
						setCCRoles.add(strAd.trim());
					else
						setCCEmailAdd.add(strAd.trim());	
				}
			}
			else{
				if(!mailConfig.CCAddress__c.contains('@'))
					setCCRoles.add(mailConfig.CCAddress__c.trim());	
				else 
					setCCEmailAdd.add(mailConfig.CCAddress__c.trim());	
			}
		}		
		for(Deal_Team__c dtRec:[Select UserId__c,
								Role__c,
								UserId__r.Email,
		                        LoanId__c From Deal_Team__c 
		                        where LoanId__c IN :loanIds])
		{
			//get map of loan vs to email addresses
		    if(!mpLoanToDtMem.containsKey(dtRec.LoanId__c) && setRoles.contains(dtRec.Role__c))
		    {
		    	mpLoanToDtMem.put(dtRec.LoanId__c,new List<String>{dtRec.UserId__r.Email});
		    }
		    else if(mpLoanToDtMem.containsKey(dtRec.LoanId__c) && setRoles.contains(dtRec.Role__c))
		    {
		    	mpLoanToDtMem.get(dtRec.LoanId__c).add(dtRec.UserId__r.Email);
		    } 
		    //get map of loan vs cc email addresses
		    if(!mpLoanToDtMemCC.containsKey(dtRec.LoanId__c) && setCCRoles.contains(dtRec.Role__c))
		    {
		    	mpLoanToDtMemCC.put(dtRec.LoanId__c,new List<String>{dtRec.UserId__r.Email});
		    }
		    else if(mpLoanToDtMemCC.containsKey(dtRec.LoanId__c) && setCCRoles.contains(dtRec.Role__c))
		    {
		    	mpLoanToDtMemCC.get(dtRec.LoanId__c).add(dtRec.UserId__r.Email);
		    } 		                       
		}
	}
	//function to get formated date
	private String formDate(Date dt){
		String strFormDate = '';
		if(dt != null){
			String strDate  = String.valueOf(dt);
			if(strDate.contains('-')){
				List<String> lstFormat = strDate.split('-');
				strFormDate = lstFormat[1]+'/'+lstFormat[2]+'/'+lstFormat[0];
			}	
		}
		return strFormDate;	
	}
	
	//send emails to all recepients
	private void sendReportCommentsDueAlert(List<Third_Party_Report__c> listTPR){
		List<Messaging.SingleEmailMessage> singleMails =  new List<Messaging.SingleEmailMessage>();
		String sfUrl ='https://'+URL.getSalesforceBaseUrl().getHost();
		
		for(Third_Party_Report__c tpr:listTPR){
			String mSubject = '';
			String mBody = '';
			List<String> listToAdd = new List<String>();
			List<String> listCCAdd = new List<String>();
			//get final all to email addresses
		    if(!mpLoanToDtMem.isEmpty() && mpLoanToDtMem.containsKey(tpr.Loan_Property__r.LoanId__c)){ 
			   	listToAdd.addAll(mpLoanToDtMem.get(tpr.Loan_Property__r.LoanId__c));
		    }	
		    if(setToEmailAdd != null && setToEmailAdd.size() > 0){
		    	listToAdd.addAll(setToEmailAdd);
		    }	
		    //get final all cc email addresses
		    if(!mpLoanToDtMemCC.isEmpty() && mpLoanToDtMemCC.containsKey(tpr.Loan_Property__r.LoanId__c)){ 
			   	listCCAdd.addAll(mpLoanToDtMemCC.get(tpr.Loan_Property__r.LoanId__c));
		    }	
		    if(setCCEmailAdd != null && setCCEmailAdd.size() > 0){
		    	listCCAdd.addAll(setCCEmailAdd);
		    }
		    		    	
			mSubject = mailConfig.Subject__c;
			mBody = mailConfig.Mail_Text__c;
			if(mailConfig.Mail_Text1__c != null && mailConfig.Mail_Text1__c!= ''){
				mBody += ' '+mailConfig.Mail_Text1__c;
			}

			if(mSubject.contains('{!Third_Party_Report__c.Report_Comments_Due__c}')){
				mSubject = mSubject.replace('{!Third_Party_Report__c.Report_Comments_Due__c}',formDate(tpr.Report_Comments_Due__c));
			}
			if(mSubject.contains('{!Third_Party_Report__c.Loan__c}')){
				mSubject = mSubject.replace('{!Third_Party_Report__c.Loan__c}', tpr.Loan_Property__r.LoanId__r.Name);
			}			
			if(mBody.contains('{!Third_Party_Report__c.Loan__c}')){
				mBody = mBody.replace('{!Third_Party_Report__c.Loan__c}', tpr.Loan_Property__r.LoanId__r.Name);
			}
			if(mBody.contains('{!Third_Party_Report__c.Report_Comments_Due__c}')){
				mBody = mBody.replace('{!Third_Party_Report__c.Report_Comments_Due__c}', formDate(tpr.Report_Comments_Due__c));
			}	
			if(mBody.contains('{!Loan__c.Estimated_Closing_Date__c}')){
				mBody = mBody.replace('{!Loan__c.Estimated_Closing_Date__c}', formDate(tpr.Loan_Property__r.LoanId__r.Estimated_Closing_Date__c));
			}			
			if(mBody.contains('{!Third_Party_Report__c.Link}')){
				mBody = mBody.replace('{!Third_Party_Report__c.Link}', sfUrl+'/'+tpr.id);
			}			
		    Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
				if(listToAdd != null && listToAdd.size() > 0){
					email.setToAddresses(listToAdd);
				}
				if(listCCAdd != null && listCCAdd.size() > 0){
					email.setCcAddresses(listCCAdd);
				}
			    email.setSaveAsActivity(false);
			    email.setSubject(mSubject);
			    email.setHtmlBody(mBody); 
			    singleMails.add(email);	
		}
		try{
		    if(singleMails != null && singleMails.size() > 0){
		    	Messaging.sendEmail(singleMails);	
		    }			
		}catch(Exception e){
			
		}			
	}
	global void finish(Database.BatchableContext BC){
	   	
	}	     
}