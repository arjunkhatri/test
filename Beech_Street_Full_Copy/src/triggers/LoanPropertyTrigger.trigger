trigger LoanPropertyTrigger on Loan_Property__c (before insert,after insert,after delete)
{
    LoanPropertyHandler objLoanProperty = new LoanPropertyHandler();
    if(trigger.isBefore && trigger.isInsert)
        objLoanProperty.OnBeforeInsert(Trigger.new);
    else if(trigger.isAfter){
        if(trigger.isInsert){
            objLoanProperty.OnAfterInsert(trigger.new);     
        }
        else if(trigger.isDelete){
            objLoanProperty.OnAfterDelete(trigger.old);
        }
    }
}