trigger DealTeamTrigger on Deal_Team__c (before delete,before update,before insert,after insert, after update, after delete)
{
    DealTeamHandler objDealTeam = new DealTeamHandler();  
    
    if(Trigger.isBefore)
    {
		if(Trigger.isUpdate){
		      objDealTeam.onBeforeUpdateEditErrorMsg(Trigger.new,Trigger.oldMap); 
		}    	
		
	    if(Trigger.isInsert || Trigger.isUpdate && Trigger.new.size() == 1)  
	    {
	        objDealTeam.determineUsersAccess(Trigger.new,Trigger.oldMap,true);
	    }
	    else if(Trigger.isDelete) 
	    //&& Trigger.old.size() == 1)
	    {
	        objDealTeam.determineUsersAccess(Trigger.old,null,false);
	    }
    }
    if(Trigger.isAfter){
    	if(Trigger.isInsert)
    		objDealTeam.onAfterInsert(Trigger.new);
    	
    	if(Trigger.isUpdate)
    		objDealTeam.onAfterUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
    	
    	if(Trigger.isDelete)
    		objDealTeam.onAfterDelete(Trigger.old, Trigger.oldMap);    	
    }
}