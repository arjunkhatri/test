trigger LoanTerm on Loan_Term__c (before update, before insert) { 
	
	LoanTermTriggerHandler objLTHandler = new LoanTermTriggerHandler();
    
   	if(Trigger.isBefore && Trigger.isInsert){
   		objLTHandler.OnBeforeInsert(Trigger.new);
   	}
   	else if(Trigger.isBefore && Trigger.isUpdate){
   		objLTHandler.OnBeforeUpdate(Trigger.new);
   	}
}