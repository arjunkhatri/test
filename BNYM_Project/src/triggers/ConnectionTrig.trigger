/*
  
     Authors :  David Brandenburg
     Created Date: 2014-04-30
     Last Modified: 2014-04-30
*/
trigger ConnectionTrig on Connections__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
  ConnectionTrigHand  handler = new ConnectionTrigHand  ();
  
  if (Trigger.isInsert)
  {
    if (Trigger.isAfter)
      handler.onAfterInsert(Trigger.New);
  }
  if (Trigger.isUpdate)
  {
    if (Trigger.isAfter)
      handler.onAfterUpdate(Trigger.New , Trigger.OldMap);
  }
  if (Trigger.isDelete)
  {
    if (Trigger.isAfter)
      handler.onAfterDelete(Trigger.Old);
  }
}