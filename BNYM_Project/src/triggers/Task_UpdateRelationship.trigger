trigger Task_UpdateRelationship on Task (before insert, before update) {

List<Task> completedTasks = new List<Task>();
Set<Id> accIds = new Set<Id>();
for(Task tsk: Trigger.new)
{
    //TO-DO Hardcoding to be replaced by quering the RecordType object for SFDC Id of Task -> Touch record type 
    if(tsk.RecordTypeId == '012e00000008YeVAAU' && tsk.Status == 'Completed')
    {
        System.debug('Task Account Id: '+tsk.WhatId);
        //Check for association to a Relationship/Account
        if(tsk.WhatId != null && String.valueOf(tsk.WhatId).startsWith('001'))
        {
            completedTasks.add(tsk);
            accIds.add(tsk.WhatId);
        }
    }
}
if(!completedTasks.isEmpty() && !accIds.isEmpty())
{
    Map<Id, Account> accMap = new Map<Id, Account>([select Id, Last_Touch_Activity_Date__c from Account where Id =: accIds]);
    Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
    for(Task tsk: completedTasks)
    {
        Account acc = accMap.get(tsk.WhatId);
        if(acc != null)
        {
            if(tsk.ActivityDate != null && (acc.Last_Touch_Activity_Date__c == null || tsk.ActivityDate > acc.Last_Touch_Activity_Date__c))
            {
                acc.Last_Touch_Activity_Date__c = tsk.ActivityDate;
                accountsToUpdate.put(acc.Id, acc);
            }
        }
    }
    
    if(!accountsToUpdate.isEmpty())
    {
        update accountsToUpdate.values();
    }
}

}