trigger Event_UpdateRelationship on Event (before insert, before update) {

List<Event> completedEvents = new List<Event>();
Set<Id> accIds = new Set<Id>();
for(Event evnt: Trigger.new)
{
    //Check for association to a Relationship/Account
    if(evnt.WhatId != null && String.valueOf(evnt.WhatId).startsWith('001'))
    {
        completedEvents.add(evnt);
        accIds.add(evnt.WhatId);
    }
}
if(!completedEvents.isEmpty() && !accIds.isEmpty())
{
    Map<Id, Account> accMap = new Map<Id, Account>([select Id, Last_Meeting_Date__c from Account where Id IN: accIds]);
    Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
    for(Event evnt: completedEvents)
    {
        Account acc = accMap.get(evnt.WhatId);
        if(acc != null)
        {
            if(evnt.EndDateTime != null && (acc.Last_Meeting_Date__c == null || evnt.EndDateTime > acc.Last_Meeting_Date__c))
            {
                acc.Last_Meeting_Date__c = evnt.EndDateTime;
                accountsToUpdate.put(acc.Id, acc);
            }
        }   
    }
    
    if(!accountsToUpdate.isEmpty())
    {
        update accountsToUpdate.values();
    }
}

}