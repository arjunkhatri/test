public with sharing class ConnectionMgmt_ContactConnections
{
  public string contactId {get;set;}
  public Connections__c ConnectionObj {get;set;}
  public List<Connections__c> ContactList {get;set;}
  public Connections__c ConnectionObjRel {get;set;}
  public Contact ContactObj {get;set;}
  public string hasError1 {get;set;}
  public string hasError2 {get;set;}
  public string FilterString {get;set;}
  public string SelectedId {get;set;}
  public string ContactTitle {get;set;}
  public List<fieldSet> FieldList {get;set;}
  private string recordTypeId ;
  public string ActionType {get;set;}

  private Map<String, SObjectField> accountFields ;
  private Map<String, SObjectField> contactFields ;
  private map<string, string> labelCache ;

  public ConnectionMgmt_ContactConnections   ()
  {
    if (contactId == null)
    {
      contactId =ApexPages.currentPage().getParameters().get('Id');
      ConnectionObj = new Connections__c();
      recordTypeId = [select id from RecordType where SobjectType = 'Connections__c' and Name = 'C:C' ].id;
      ConnectionObj.RecordTypeId = recordTypeId;
      ConnectionObjRel = new Connections__c();
      ConnectionObjRel.RecordTypeId = [select id from RecordType where SobjectType = 'Connections__c' and Name = 'R:C' ].id;
    }
    hasError1 = '0';
    hasError2 = '0';
    FilterString = 'Contact';
    ActionType = 'Relationship' ;
    labelCache = new map<string, string> ();
  }
  public void Init()
  {
    ContactTitle = [select name from Contact where Id = :contactId].Name;
    createContactList () ;
  }

  public string getFieldLabel ( string objType , string FieldName)
  {
    SObjectField fieldToken ;

    if (accountFields == null)
    {
      SObjectType objToken = Schema.getGlobalDescribe().get('Account');
       DescribeSObjectResult objDef = objToken.getDescribe();
      accountFields = objDef.fields.getMap();

    }
    if (contactFields == null)
    {
      SObjectType objTokenContacts = Schema.getGlobalDescribe().get('Contact');
       DescribeSObjectResult objDefContacts = objTokenContacts.getDescribe();
      contactFields= objDefContacts.fields.getMap();
    }

    if (objType=='Account')
      fieldToken = accountFields.get(FieldName);

    if (objType=='Contact')
      fieldToken = contactFields.get(FieldName);

    DescribeFieldResult selectedField = fieldToken.getDescribe();
     return selectedField.getLabel()  ;

  }
  public string getPanelVisibility ()
  {
    if (ActionType == 'Contact')
      return 'ContactPanel';
    if (ActionType == 'Relationship')
      return 'RelationshipPanel';


    //Default Value
      return 'ContactPanel';
  }
  public void ResetConnectionObj()
  {
    hasError1 = '0';
    hasError2 = '0';

      ConnectionObj = new Connections__c();
      ConnectionObjRel= new Connections__c();


  }

  private void createContactList ()
  {
    if (FilterString == 'Contact')
    {
      string sqlFields =  BuildFieldList ('ContactConnection' );
      ContactList = database.query('select id,Contact_C1__c , Contact_C2__r.Name,Relationship_R1__r.Name , Role_in_Relationship__c  ' + sqlFields
      + ' from Connections__c where Contact_C1__c = \'' + contactId + '\'');
      //+ ' from Connections__c where Contact_C1__c = \'' + contactId + '\' or Contact_C2__c = \'' + contactId + '\' ');
      //ContactList = [select id, Contact_C2__r.Name,Relationship2__r.Name,  Contact_C2__r.MailingCity, Contact_C2__r.Email, Contact_C2__r.MobilePhone , Role_in_Relationship__c
      //  from Connections__c where Contact_C1__c = :contactId and Contact_C2__c != null and RecordTypeId= :recordTypeId ];
    }
  }

  private string BuildFieldList(string filterType)
  {


    List<AddressConnectionMap__c> addressMap = AddressConnectionMap__c.getall().values();
    string sqlFields = '' ;
    list<fieldSet> fldSetList = new list<fieldSet>();
    string fieldLabel  ='';

    for ( AddressConnectionMap__c fieldMap : addressMap )
    {
      if (fieldMap.Type__c ==  filterType)
      {
        if (!labelCache.containskey(fieldMap.FieldName__c))
        {
          string [] fieldSplit = fieldMap.FieldName__c.split('\\.');
          if (fieldSplit.size() > 0 )
          {
            if (fieldSplit[0].contains('Relationship'))
              fieldLabel = getFieldLabel('Account' , fieldSplit[1] );
            if (fieldSplit[0].contains('Contact'))
              fieldLabel = getFieldLabel('Contact' , fieldSplit[1] );

            labelCache.put (fieldMap.FieldName__c ,  fieldLabel);
          }
        }
        else
          fieldLabel = labelCache.get(fieldMap.FieldName__c) ;

        fldSetList.add(new fieldSet(Integer.ValueOf(fieldMap.Sort_Order__c),fieldMap.FieldName__c , fieldLabel) );
        if (sqlFields.length() == 0)
          sqlFields = fieldMap.FieldName__c;
        else
          sqlFields += ',' + fieldMap.FieldName__c;
      }
    }

    if (fldSetList.size() > 0)
    {
      fldSetList.Sort();
      for (fieldSet f : fldSetList)
        FieldList  = fldSetList;
    }

    if (sqlFields.length() > 0)
      sqlFields = ',' + sqlFields ;
    return sqlFields  ;
  }
  public void SaveRoleChange ()
  {
    ContactObj = new Contact();

    for (Connections__c c : ContactList  )
    {
      if (c.id == SelectedId )
      {

        System.debug('**: ' + c);
        update c;
        break;
      }
    }
  }

  public void FilterList ()
  {
    createContactList ();
  }
  public PageReference SaveAndNew ()
  {
    SaveNewConnection ();
    return null;
  }

  public PageReference SaveAndClose ()
  {
    SaveNewConnection ();
    if (hasError1 == '1' || hasError2 == '1')
      return null;
    else
      return ReturnUrl();

  }
  public PageReference Cancel ()
  {
    return ReturnUrl();
  }
  private Pagereference ReturnUrl ()
  {
    PageReference pageRef = new PageReference('/' + contactId);
    pageRef.setRedirect(true);
    return pageRef;
  }
  private void SaveNewConnection ()
  {
    hasError1 = '0';
    hasError2 = '0';
    Connections__c connection= new Connections__c();

    if (ActionType == 'Contact')
    {
      if (ConnectionObj.Contact_C2__c == null  )
      {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact Name is a required field'));
        hasError1 = '1';
      }
      if (ConnectionObj.Role_in_Relationship__c == null )
      {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact Role is a required field'));
        hasError2 = '1';
      }
      if (hasError1 == '1' || hasError2 == '1')
        return;

      connection.Contact_C1__c = contactId;
      connection.Contact_C2__c = ConnectionObj.Contact_C2__c ;
      connection.Role_in_Relationship__c = ConnectionObj.Role_in_Relationship__c;
      connection.RecordTypeId = [select id from RecordType where sObjectType = 'Connections__c' and DeveloperName = 'C_C'  ].id;
      insert connection ;
    }

    if (ActionType =='Relationship')
    {
      if (ConnectionObjRel.Relationship_R1__c == null  )
      {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Relationship Name is a required field'));
        hasError1 = '1';
      }
      if (ConnectionObjRel.Role_in_Relationship__c == null )
      {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Reltaionship Role is a required field'));
        hasError2 = '1';
      }
      if (hasError1 == '1' || hasError2 == '1')
        return;

      connection.Relationship_R1__c = ConnectionObjRel.Relationship_R1__c;
      connection.Contact_C1__c = contactId ;
      connection.Role_in_Relationship__c = ConnectionObjRel.Role_in_Relationship__c;
      connection.RecordTypeId = [select id from RecordType where sObjectType = 'Connections__c' and DeveloperName = 'R_C'  ].id;
      insert connection ;

    }

    ConnectionObj = new Connections__c();
    ConnectionObjRel = new Connections__c();
    ResetConnectionObj();
    createContactList ();

  }



  public class fieldSet implements Comparable
  {
    public integer SortNumber {get;set;}
    public string Field {get;set;}
    public string fieldLabel {get;set;}

    public fieldSet (integer s , string f , string fldLabel)
    {
      SortNumber = s;
      Field = f;
      fieldLabel = fldLabel;

    }

    public Integer compareTo(Object compareTo)
    {
      fieldSet fs = (fieldSet)compareTo;
          if (SortNumber > fs.SortNumber )return 1;
          if (SortNumber == fs.SortNumber)return 0;
          return -1;


        }
  }
}