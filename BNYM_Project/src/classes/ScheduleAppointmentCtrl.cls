/*
 *   Description - An apex page controller display confirmation to schedule an appointment
 *                 on ipad BNYM Community Home page.
 *    Version          Date          Description
 *    1.0              11/05/2015    Initial Draft 
 *
 */
public without sharing class ScheduleAppointmentCtrl {
	public String strDate{get;set;}
	public String strUserId{get;set;}
	public Event userEvent {get;set;}
	public User usr{get;set;}
	public String strSelectedDt{get;set;}
	public String strLocation{get;set;}
	public String strSubject{get;set;}
	public String strSelectedTime{get;set;} 
	ApexPages.StandardController c;
	public ScheduleAppointmentCtrl(){ 
		strDate = ApexPages.currentPage().getParameters().get('sDate');
		strUserId = ApexPages.currentPage().getParameters().get('uId');
		userEvent = new Event();
		if(strDate != null && strDate != ''){  
			//year-month-day
			String strDay = strDate.split(' ')[0];
			String year = strDay.split('-')[2];
			String month = strDay.split('-')[0];
			String day =  strDay.split('-')[1];	
			String strTime = strDate.split(' ')[1];		
			String finalDt = year+'-'+month+'-'+day +' '+strTime;
			Datetime dt = Datetime.valueOf(finalDt);
			userEvent.StartDateTime = dt;
			userEvent.EndDateTime = dt.addHours(1);
			if(strDate.contains(' ')){
				strSelectedDt = strDate.split(' ')[0].replaceAll('-','/');
				strSelectedDt += ' at ';
				String strHr = strDate.split(' ')[1];
				if(strHr.contains(':')){
					String hr = strHr.split(':')[0];
					strSelectedDt += hr;
					if(hr == '9' || hr == '10' || hr == '11' )
						strSelectedDt += 'AM';
					else if(hr == '12' || hr == '1' || hr == '2' || hr == '3'|| hr == '4')
						strSelectedDt += 'PM';						 
				}
					
			}
		}
		if(strUserId != null && strUserId != '')	
			userEvent.OwnerId = strUserId;
		userEvent.IsVisibleInSelfService = true;	
		userEvent.WhoId = [Select ContactId From User where id = :userinfo.getUserId()].ContactId;
		usr = [Select Name, 
					  Id, 
					  FullPhotoUrl 
					  From User where id = :strUserId limit 1];
	}
	//function to cancel scheduling appointment and redirect to home page
	public pagereference schCancel(){
		return new pagereference('/BNYMWM/apex/Home3By2');
	}	
	//function to go back on calendar page
	public pagereference schGoBack(){
		return new pagereference('/BNYMWM/apex/Calender?uId='+strUserId);
	}	
	//function to insert new event
	public pagereference schConfirm(){ 
		userEvent.Location = strLocation;
		userEvent.Subject = strSubject;
		insert userEvent;
		return null;
	}
}