/*
 *   Descprition : Controller to hold Pie chart Data which is breaking out all the
 *                 Financial Accounts associated to the running user.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            29/04/15          Initial Draft
 */
public without sharing class MyFinancialAccountCtrl {
    public Decimal percentInv = 0.00;
    public Decimal percentBanking = 0.00;
    //constructor starts 
	public MyFinancialAccountCtrl(){
		if(ApexPages.currentPage().getParameters().get('inv') != null && ApexPages.currentPage().getParameters().get('inv') != '')
		      percentInv = decimal.valueof(ApexPages.currentPage().getParameters().get('inv'));
		if(ApexPages.currentPage().getParameters().get('bank') != null && ApexPages.currentPage().getParameters().get('bank') != '')
		      percentBanking = decimal.valueof(ApexPages.currentPage().getParameters().get('bank'));
	}//constructor ends

   //Method to display pecentage in pie chart 
   public List<PieWedgeData> getPieData() {
        List<PieWedgeData> data = new List<PieWedgeData>();
        data.add(new PieWedgeData('Investment', PercentInv));
        data.add(new PieWedgeData('Banking', PercentBanking));
        return data;
    }
    // Wrapper class for pie chart
    public class PieWedgeData {
        public String name { get; set; }
        public Decimal data { get; set; }
        public PieWedgeData(String name, Decimal data) {
            this.name = name;
            this.data = data;
        }
    }

}