public with sharing class GoogleMapAddresses {
	private Id accId;
	
	private List<Contact> cList = new List<Contact>();
    public List<Contact> contactList {get{return cList;}set{cList = value;}}
	
	public GoogleMapAddresses(ApexPages.StandardController controller) {  
    	if(controller.getId()!=null){
        	try	{
            	AccId = controller.getId();
        	}
        	catch(Exception e){
             	ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.WARNING,'No Records to  display');
             	ApexPages.addMessage(myMsg1);
        	}
        getcontactAddresses();
        }
    }
	
	public void getcontactAddresses(){
		cList = [select id, Name, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry from Contact 
		where AccountId =: AccId limit 20];
	}
}