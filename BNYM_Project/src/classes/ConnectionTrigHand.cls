/*
  
     Authors :  David Brandenburg, Don Koppel
     Created Date: 2014-04-30
     Last Modified: 2014-07-23
     
     Purpose:  Trigger handler for the trigger called ConnectionTrig

*/

public with sharing class ConnectionTrigHand 
{
    public void onBeforeInsert(list<Connections__c> newList)
    {
        CheckForSingleSpouse (newList);
    }
    
    public void onAfterInsert(list<Connections__c> newList)
    {
        insertInverseRecord(newList);
        UpdateContactEmployer (newList) ;
    }
    public void onAfterUpdate (list<Connections__c> newList , map<id,Connections__c> oldMap)
    {
        updateInverseRecord (newList ,  oldMap);
        CheckForSingleSpouse (newList);
        UpdateContactEmployer (newList) ;
    }
  
    public void onAfterDelete (list<Connections__c> oldList)
    {
        deleteInverseRecord(oldList);
        DeleteContactEmployer (oldList);
    }
  
  //*** method handles the inserted records ***/
  private void insertInverseRecord (list<Connections__c> newList)
  {
    if (ConnectionTrigHandHelper.hasAlreadyCreatedInverse())
      return;
      
    list<Connections__c> newConnectionsList = new list<Connections__c>() ;
    
    //Get mapping from custom setting and break into 2 maps
    map<string, map<string, string>> inverseMap = getInverseMapping();
    map<string, string> inverseMapRelationships = inverseMap.get('Relationship');
    map<string, string> inverseMapContacts = inverseMap.get('Contact');
        
    map<string, RecordType> rtMap = new map<string, RecordType>([select id, Name from RecordType where SobjectType = 'Connections__c' ]);
    for (Connections__c c : newList)
    {
      //Only process records which have not been already inserted.
      
        //Check to see if the Role is in the Relationship mapping object.  If found create a new record
        if (inverseMapRelationships.containsKey(c.Role_in_Relationship__c) &&  rtMap.get(c.RecordTypeId).Name == 'R:R' )
        {
          Connections__c newConnection = new Connections__c();
          newConnection.RecordTypeId = c.RecordTypeId;
          newConnection.Relationship_R1__c = c.Relationship_R2__c;
          newConnection.Relationship_R2__c  = c.Relationship_R1__c;
          newConnection.Role_in_Relationship__c = inverseMapRelationships.get(c.Role_in_Relationship__c);
          newConnectionsList.add(newConnection);
        }
        //Check to see if the Role is in the Contacts mapping object.  If found create a new record
        if (inverseMapContacts.containsKey(c.Role_in_Relationship__c) && rtMap.get(c.RecordTypeId).Name == 'C:C')
        {
          Connections__c newConnection = new Connections__c();
          newConnection.RecordTypeId = c.RecordTypeId;
          newConnection.Contact_C1__c = c.Contact_C2__c;
          newConnection.Contact_C2__c  = c.Contact_C1__c;
          newConnection.Role_in_Relationship__c = inverseMapContacts.get(c.Role_in_Relationship__c);
          newConnectionsList.add(newConnection);
        }
      
    }
    
    //If the list contains records insert into database
    if (newConnectionsList.size() > 0)
    {
      //Set the recursive flag to true.  Prevents records from being processed a second time
      ConnectionTrigHandHelper.setAlreadyCreatedInverse();
      insert newConnectionsList;
    }
  }
  private void updateInverseRecord (list<Connections__c> newList,  map<id,Connections__c> oldMap)
  {
    if (ConnectionTrigHandHelper.hasAlreadyCreatedInverse())
      return;
      
    list<Connections__c> updateConnectionsList = new list<Connections__c>() ;
    //Get mapping from custom setting and break into 2 maps
    map<string, map<string, string>> inverseMap = getInverseMapping();
    map<string, string> inverseMapRelationships = inverseMap.get('Relationship');
    map<string, string> inverseMapContacts = inverseMap.get('Contact');
    
    map<string, RecordType> rtMap = new map<string, RecordType>([select id, Name from RecordType where SobjectType = 'Connections__c' ]);
    set<string> affectedSet = new set<string>();
    map<string, string> changedConnection = new map<string, string>();
    
    //get a map of the inverse Connections and the affected inverse Connectsions
    for (Connections__c c : newList)
    {
      //Only process record where the role changed
      if (c.Role_in_Relationship__c != oldMap.get(c.id).Role_in_Relationship__c )
      {  
        if (rtMap.get(c.RecordTypeId).Name== 'R:R' )
        {
          affectedSet.add(c.Relationship_R2__c);
          changedConnection.put(c.Relationship_R1__c , c.Role_in_Relationship__c);
        }
        
        
        if (rtMap.get(c.RecordTypeId).Name== 'C:C' )
        {
          affectedSet.add(c.Contact_C2__c);
          changedConnection.put(c.Contact_C1__c , c.Role_in_Relationship__c);
        }
      }
    }
    
    //Query the inverse Connections
    List<Connections__c> inverseConnections = [select id, RecordTypeId, Relationship_R1__c , Relationship_R2__c ,Contact_C1__c , Contact_C2__c , Role_in_Relationship__c
    from Connections__c where Contact_C1__c in : affectedSet or Relationship_R1__c = :affectedSet ];
    
    //Loop through the inverse records update the role based on the mapping
    for (Connections__c c : inverseConnections )
    {
      if (inverseMapRelationships.containskey(changedConnection.get(c.Relationship_R2__c) ) &&  rtMap.get(c.RecordTypeId).Name== 'R:R' )
      {
        c.Role_in_Relationship__c = inverseMapRelationships.get(changedConnection.get(c.Relationship_R2__c));
        updateConnectionsList.add(c);
      }
      
      if (inverseMapContacts.containskey(changedConnection.get(c.Contact_C2__c) ) &&  rtMap.get(c.RecordTypeId).Name== 'C:C' )
      {
        c.Role_in_Relationship__c = inverseMapContacts.get(changedConnection.get(c.Contact_C2__c));
        updateConnectionsList.add(c);
      }
    }
    
    //If the list contains records update the records
    if (updateConnectionsList.size() > 0)
    {
      //Set the recursive flag to true.  Prevents records from being processed a second time
      ConnectionTrigHandHelper.setAlreadyCreatedInverse();
      update updateConnectionsList;
    }
  }
  
  //*** Process the delete reords ***/
  private void deleteInverseRecord (list<Connections__c> oldList)
  {
    if (ConnectionTrigHandHelper.hasAlreadyCreatedInverse())
      return;

    map<string, RecordType> rtMap = new map<string, RecordType>([select id, Name from RecordType where SobjectType = 'Connections__c' ]);
    set<string> affectedSet = new set<string>();
    map<string, string> relationshipMapping = new map<string, string> ();
    
    // Get a set of the inverse Connections
    for (Connections__c c : oldList)
    {
        if (rtMap.get(c.RecordTypeId).Name== 'R:R' )
        {
          affectedSet.add(c.Relationship_R2__c);
          relationshipMapping.put(c.Relationship_R2__c , c.Relationship_R1__c) ;
        }
        if (rtMap.get(c.RecordTypeId).Name== 'C:C' )
        {
          affectedSet.add(c.Contact_C2__c);
          relationshipMapping.put(c.Contact_C2__c , c.Contact_C1__c) ;
        }

    }
    
    //Query the inverse Connections
    List<Connections__c> deleteConnectionsList = [select id, RecordTypeId, Relationship_R1__c , Relationship_R2__c ,Contact_C1__c , Contact_C2__c , Role_in_Relationship__c
    from Connections__c where Contact_C1__c in : affectedSet or Relationship_R1__c = :affectedSet ];
    
    //If the list contains records deletethe records
    List<Connections__c> deleteRecords = new List<Connections__c> ();
    if (deleteConnectionsList.size() > 0)
    {
      for (Connections__c c : deleteConnectionsList)
      {
        if (rtMap.get(c.RecordTypeId).Name== 'C:C' )
        {
          if (relationshipMapping.get(c.Contact_C1__c ) == c.Contact_C2__c )
            deleteRecords.add(c);
        }
        if (rtMap.get(c.RecordTypeId).Name== 'R:R' )
        {
          if (relationshipMapping.get(c.Relationship_R1__c ) == c.Relationship_R2__c )
            deleteRecords.add(c);
        }
      }
      //Set the recursive flag to true.  Prevents records from being processed a second time
      ConnectionTrigHandHelper.setAlreadyCreatedInverse();
      delete deleteRecords;
    }
  }
  
  //**  Method calls the custom setting called Inverse_Connection_Role__c and retunrs 2 maps by RecordType  **/
  private map<string, map<string, string>> getInverseMapping()
  {
    //Get mapping from custom setting and break into 2 maps
    map<string, Inverse_Connection_Role__c> inverseMap = Inverse_Connection_Role__c.getAll();
    map<string, string> inverseMapRelationships = new map<string, string>();
    map<string, string> inverseMapContacts = new map<string, string>();
    
    //Loop through custom setting map and population Relationship and Contact maps
    for ( string key : inverseMap.keySet())
    {
      if (inverseMap.get(key).Connection_Record_Type__c == 'R:R')
        inverseMapRelationships.put(inverseMap.get(key).Role__c, inverseMap.get(key).Inverse_Role__c);
      
      if (inverseMap.get(key).Connection_Record_Type__c == 'C:C')
        inverseMapContacts.put(inverseMap.get(key).Role__c, inverseMap.get(key).Inverse_Role__c);
    }
    //Create a return object and return the 2 maps
    map<string, map<string, string>> retVal = new map<string, map<string, string>>();
    retVal.put('Relationship', inverseMapRelationships);
    retVal.put('Contact', inverseMapContacts);
    return retVal;
  }
  private void CheckForSingleSpouse (List<Connections__c> newList)
    {
        set<string> connectionSet = new set<string> ();
        
        string rt = [select id from RecordType where sObjectType = 'Connections__c' and Name = 'C:C'].id;
        
        for (Connections__c connection : newList) 
        {
            if (connection.RecordTypeID == rt && connection.Role_in_Relationship__c == 'Spouse')
                connectionSet.add(connection.Contact_C1__c);
        }
        System.Debug(connectionSet);
        List<Connections__c> existingConnections = [select Contact_C1__c from Connections__c 
        where Contact_C1__c in :connectionSet and Role_in_Relationship__c = 'Spouse'];
        
        set<string> existingSet = new set<string> ();
        for (Connections__c connection : existingConnections) 
            existingSet.add(connection.Contact_C1__c);
        
        for (Connections__c connection : newList) 
        {
            if (existingSet.Contains(connection.Contact_C1__c))
                connection.addError('A existing spouse already exists for the contact');
        }
            
    }
    /*** Update the contact record with the employer account from the connection Object ***/
    private void UpdateContactEmployer (List<Connections__c> newList)
    {
        map<string, string> connectionMap = new map<string, string> ();
        
        string rt = [select id from RecordType where sObjectType = 'Connections__c' and Name = 'R:C'].id;
        
        for (Connections__c connection : newList) 
        {
            if (connection.RecordTypeID == rt && connection.Role_in_Relationship__c == 'Employee')
                connectionMap.put(connection.Contact_C2__c , connection.Relationship_R1__c);
        }
        
        List<Contact> ctList = [select id, Employer__c from Contact where id in :connectionMap.keyset()];
        System.Debug('ctList: ' + connectionMap) ;
        for (Contact c : ctList)
        {
            c.Employer__c = connectionMap.get(c.id);
        }
        update ctList;
        
    }
    /*** Delete the contact record with the employer account from the connection Object ***/
    private void DeleteContactEmployer (List<Connections__c> oldList)
    {
        set<string >connectionSet = new set<string> ();
        
        string rt = [select id from RecordType where sObjectType = 'Connections__c' and Name = 'R:C'].id;
        
        for (Connections__c connection : oldList) 
        {
            if (connection.RecordTypeID == rt && connection.Role_in_Relationship__c == 'Employee')
                connectionSet.add(connection.Contact_C2__c );
        }
        
        List<Contact> ctList = [select id, Employer__c from Contact where id in :connectionSet];
        for (Contact c : ctList)
        {
            c.Employer__c = null;
        }
        update ctList;
        
    }
    
}