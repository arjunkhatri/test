public with sharing class BNYM_Community_Home_controller {


private final Contact cntct;

  public contact getCustomer() {
    return this.cntct;
  }
  
  public user getAdvisor() {
    User advisor = [Select u.Username, u.Title, u.Street, u.State, u.SmallPhotoUrl, u.PostalCode, u.Phone, u.Name, u.MobilePhone, u.Id, u.FullPhotoUrl, u.FirstName, u.Fax, u.Extension, u.EmployeeNumber, u.Email, u.Country, u.ContactId, u.City, u.AboutMe From User u where u.id=:cntct.OwnerId];
      return advisor;
  }
}