@isTest
private class ConnectionMgmt_RelationConnTest {
	static Account testAccount;
	static Contact testContact;
	static Contact testContact2;

	static void setup(){
		testAccount = new Account ( Name = 'testAccount');
		insert testAccount;

		testContact = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		testContact2 = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		insert new List<Contact>{ testContact, testContact2 };

		List<AddressConnectionMap__c> connAdds  = new List<AddressConnectionMap__c>();
		AddressConnectionMap__c conn = new AddressConnectionMap__c();
		conn.Name = 'testConn';
		conn.Sort_Order__c = 1;
		conn.FieldName__c = 'Contact_C2__r.MailingCity';
		conn.Type__c = 'ContactConnection';
		connAdds.add(conn);

		conn = new AddressConnectionMap__c();
		conn.Name = 'testConn1';
		conn.Sort_Order__c = 1;
		conn.FieldName__c = 'Relationship_R2__r.Type';
		conn.Type__c = 'AllContacts';
		connAdds.add(conn); 

		conn = new AddressConnectionMap__c();
		conn.Name = 'testConn2';
		conn.Sort_Order__c = 1;
		conn.FieldName__c = 'Relationship_R2__r.Type';
		conn.Type__c = 'AllRelationships';
		connAdds.add(conn); 

		conn = new AddressConnectionMap__c();
		conn.Name = 'testConn3';
		conn.Sort_Order__c = 1;
		conn.FieldName__c = 'Contact_C1__r.MailingCity';
		conn.Type__c = 'Business';
		connAdds.add(conn);

		conn = new AddressConnectionMap__c();
		conn.Name = 'testConn4';
		conn.Sort_Order__c = 1;
		conn.FieldName__c = 'Contact_C1__r.MailingCity';
		conn.Type__c = 'Trusts';
		connAdds.add(conn);  

		insert connAdds;

		PageReference pageRef = Page.ConnectionMgmt_RelationshipConnections;
		pageRef.getParameters().put( 'accountid', testAccount.Id );

		Test.setCurrentPageReference( pageRef );
	}

	@isTest static void testInit() {
		setup();
		ApexPages.Standardcontroller std = new ApexPages.Standardcontroller( new Connections__c() );
		ConnectionMgmt_RelationshipConnections ctrl = new ConnectionMgmt_RelationshipConnections( std );
		ctrl.Init();
		
		ctrl.ActionType = 'Contact';
		ctrl.getPanelVisibility();
		ctrl.ActionType = 'Relationship';
		ctrl.getPanelVisibility();
		ctrl.ActionType = 'NewContact';
		ctrl.getPanelVisibility();
		
		ctrl.ResetConnectionObj();
		ctrl.SaveRoleChange();
		
		ctrl.FilterString = 'Contact';
		ctrl.FilterList();

		ctrl.FilterString = 'Relationship';
		ctrl.FilterList();
		
		ctrl.FilterString = 'Business';
		ctrl.FilterList();
		
		ctrl.FilterString = 'Trusts';
		ctrl.FilterList();
		
		ctrl.SaveAndNew();
		
		ctrl.ActionType = 'Contact';
		ctrl.ConnectionObj.Contact_C1__c = testContact2.Id;
		ctrl.ConnectionObj.Role_in_Relationship__c = 'testRole';
		ctrl.SaveAndNew();
		
		ctrl.ActionType = 'Relationship';
		ctrl.SaveAndNew();
		ctrl.ConnectionObj.Relationship_R2__c = testAccount.Id;
		ctrl.ConnectionObj.Role_in_Relationship__c = 'testRole';
		ctrl.SaveAndNew();
		
		ctrl.SaveAndClose();
		ctrl.Cancel();
	}
	
}