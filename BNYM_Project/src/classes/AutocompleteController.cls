public with sharing class AutocompleteController 
{

    public String detail {get; set;}
    public String linkUrl {get; set;}
    public String linkTitle {get; set;}
    public Id oppId {get; set;}
    public Id groupId {get;set;}

    public PageReference doPost1() 
    {
        FeedItem fitem=new FeedItem();
        fItem.parentId=groupId;
        fItem.linkUrl=linkUrl;
        fItem.title=linkTitle;
        fItem.body=detail + ' Opportunity: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + oppId;

        insert fItem;

        return null;
    }
}