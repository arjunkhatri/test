/*
 *   Descprition : Controller to fetch three most recent Open Cases.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            29/04/15          Initial Draft
 */
public without sharing class OpenSupportCasesCtrl {
	public List < Case > lstcase {get;set;}
	public String strUserCont { get; set; }
    private static final String STR_CLOSED = 'Closed';
    //Constructor Start
    public OpenSupportCasesCtrl(){
        User uRec = [Select ContactId,AccountId,SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId;
        getlstcase();
    }//Contructor end

  	//Method to fetch the list of Case
  	public void getlstcase() {
      	if(String.isNotBlank(strUserCont)){
	      	lstcase = [SELECT Id ,Subject ,Status,ContactId 
	                                FROM Case 
	                                WHERE Status !=: STR_CLOSED
	                                AND ContactId= :strUserCont ORDER 
	                                BY CreatedDate DESC LIMIT 3];
      	}
  	}
}