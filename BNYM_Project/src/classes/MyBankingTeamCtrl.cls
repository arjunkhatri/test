/*
 *   Descprition : Controller to fetch Account team member users of logged in user
                   and thier related events.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            27/04/15          Initial Draft
 */
public without sharing class MyBankingTeamCtrl {
    public String strUserAcct { get; set; } 
    public String strUserCont { get; set; }
    /* variables declaration for my banking section 
    */
    public List<AccTeamWrap> lstWrapAccTeam{get;set;}
    public List <AccountTeamMember> lstAccTeam {get;set;}
    public List<Event> lstEve{get;set;}
    public String uId{get;set;}
    public set<String> setUserId{get;set;}
    public Map<Id,Event> mapIdEvent{get;set;}
    public String eventTime{get;set;}
    public String strEvents{get;set;}
    public String eventTimes{get;set;}
    public String eventOwnerId{get;set;}
    public String rmPhoto{get;set;}
    public String pbPhoto{get;set;}
    private static final String STR_REL_MANAGER = 'Relationship Manager';
    private static final String STR_PRI_BANKER = 'Private Banker';
	public MyBankingTeamCtrl(){
        User uRec = [Select ContactId,AccountId,SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId; 
        if(uRec != null && uRec.AccountId != null)
            strUserAcct = uRec.AccountId;
        getlstAccountTeam();
	}

    /* Method For getting account team member user of logged in user 
    *  and thier related events in my banking section
    */
    public void getlstAccountTeam() {
	    mapIdEvent = new Map<Id,Event>();
	    setUserId = new set<String>(); 
	    lstWrapAccTeam = new List<AccTeamWrap>();
	          if(String.isNotBlank(strUserAcct)){
	               lstAccTeam  = [Select 
	                                     UserId, TeamMemberRole, AccountId,User.Name,User.FullPhotoUrl,User.Phone
	                              From 
	                                     AccountTeamMember
	                              Where
	                                     AccountId = :strUserAcct and
	                                     (TeamMemberRole =: STR_REL_MANAGER or TeamMemberRole =: STR_PRI_BANKER ) ]; 
	                for (AccountTeamMember accTeam: lstAccTeam) {
	                       setUserId.add(accTeam.UserId);
	                       if(accTeam.TeamMemberRole == STR_REL_MANAGER )
	                           rmPhoto = accTeam.User.FullPhotoUrl;
	                       if(accTeam.TeamMemberRole == STR_PRI_BANKER )
	                            pbPhoto = accTeam.User.FullPhotoUrl;
	                }
	          }
        lstEve = [Select
                         id,WhoId, Subject, ActivityDate, StartDateTime, CreatedDate, OwnerId
                  From
                         Event
                  where
                         OwnerId IN: setUserId and 
                         WhoId = :strUserCont and 
                         StartDateTime >= TODAY
                  order by 
                         ActivityDate NULLS LAST];
        for(Event event : lstEve){
            if(!mapIdEvent.containsKey(event.OwnerId)){
                mapIdEvent.put(event.OwnerId , event);
            }
        }
        for (AccountTeamMember accTeams: lstAccTeam){
            Event eve = mapIdEvent.get(accTeams.UserId);
            if(accTeams != null ){
                if(eve != null && eve.ActivityDate != null && eve.Subject != null && eve.Subject != ''){
                    strEvents = eve.ActivityDate.month()+'/'+ eve.ActivityDate.day();
                    eventTimes = ' at ' + eve.StartDateTime.format('hh:mm a');
                }
                    lstWrapAccTeam.add(new accTeamWrap(accTeams,eve,strEvents,eventTimes));
            }
            strEvents = '';
            eventTimes = '';
        }
     }
     public class accTeamWrap {
	     public string userName {get; set;} 
	     public string teamMemberRole {get; set;}
	     public string userUrl {get; set;}
	     public string phone {get; set;}
	     public Event objEve{get;set;}
	     public String eventTime{get;set;}
	     public String strEvent{get;set;}
	     public String strUserid{get;set;}
        //Wrapper Class Controller
         public AccTeamWrap(AccountTeamMember objATeam , Event objEvent ,String strEvents ,String eventTimes ) {
            this.userName = objATeam.User.Name;
            this.teamMemberRole = objATeam.TeamMemberRole;
            this.strUserid = objATeam.UserId;
            this.userUrl = objATeam.User.FullPhotoUrl;
            this.phone = objATeam.User.Phone;
            this.objEve = objEvent;
            this.strEvent = strEvents;
            this.eventTime = eventTimes;
        }
     }
}