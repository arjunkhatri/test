public without sharing class MyMessagesCtlr {
public 	List<UserFeed> lstFeeds{get;set;}
public string photoUrl{get;set;}
public String loginUser{get;set;}
	public MyMessagesCtlr(){
		loginUser = userinfo.getUserId();
		getChatterData();
	}
	
  	//code to get chatter data and display in messages
  	public void getChatterData(){
  		String uName = userinfo.getName();
  		lstFeeds = new List<UserFeed>();
		lstFeeds = [Select id,Body,ParentId,CreatedDate,Type from UserFeed where ParentId = :loginUser order by CreatedDate DESC limit 3];
  		User usr = [Select Id, SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
  		if(usr != null && usr.SmallPhotoUrl != null)
  			photoUrl = String.valueOf(usr.SmallPhotoUrl);
  	}
}