/*
    
     Authors :  David Brandenburg
     Created Date: 2014-04-28
     Last Modified: 2014-04-28
     
     Purpose: Controller for ConnectionMgmt_RelationshipConnections page
*/
public with sharing class ConnectionMgmt_RelationshipConnections 
{
    public Account AccountObj {get;set;}
    public Connections__c ConnectionObj {get;set;}
    public Connections__c ConnectionObjRel {get;set;}
    public List<Connections__c> ContactList {get;set;}
    public List<fieldSet> FieldList {get;set;}
    public Contact ContactObj {get;set;}
    public string SelectedId {get;set;}
    public string ActionType {get;set;}
    public string  AccountId {get;set;} 
    public string  NewContactRole {get;set;} 
    public string hasError1 {get;set;}
    public string hasError2 {get;set;}
    public boolean hasDuplicates {get;set;}
    //public List<GlobalContactSearch.ContactResultSet> dupList {get;set;}
    public string AccountTitle {get;set;}
    transient public string FilterString {get;set;}
    
    private Map<String, SObjectField> accountFields ;
    private Map<String, SObjectField> contactFields ;
    private map<string, string> labelCache ;

    public ConnectionMgmt_RelationshipConnections  ()
    {
    }
    
    public ConnectionMgmt_RelationshipConnections  (ApexPages.Standardcontroller controller)
    {
        if (controller.getId() == null)
            AccountId = ApexPages.currentPage().getParameters().get('accountid');
        else
        {
            Connections__c connection = [select id, RecordType.Name , Relationship_R1__c, Contact_C1__c from Connections__c where id =:controller.getId() ];
            if (connection.RecordType.Name == 'R:R')
                AccountId = connection.Relationship_R1__c ;
            if (connection.RecordType.Name == 'R:C')
                AccountId = connection.Relationship_R1__c ;
            if (connection.RecordType.Name == 'C:C')
                AccountId = connection.Contact_C1__c;
            
        }
        
        ContactObj = new Contact();
            
    }
    

    public string getPanelVisibility ()
    {
        if (ActionType == 'Contact')
            return 'ContactPanel';
        if (ActionType == 'Relationship')
            return 'RelationshipPanel';
        if (ActionType == 'NewContact')
            return 'NewContactPanel';
            
        //Default Value
            return 'ContactPanel';
    }
    public PageReference Init() 
    {
        
        if (accountId.substring(0,3) == '003')
        {
            PageReference pageRef = Page.ConnectionMgmt_ContactConnections ;
            pageRef.getParameters().put('id', accountid);
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        ActionType = 'NewContact';
        ConnectionObj = new Connections__c();
        ConnectionObj.RecordTypeId = [select id from RecordType where SobjectType = 'Connections__c' and Name = 'R:C' ].id;
        ConnectionObjRel = new Connections__c();
        ConnectionObjRel.RecordTypeId = [select id from RecordType where SobjectType = 'Connections__c' and Name = 'R:R' ].id;
        
        hasError1 = '0';
        hasError2 = '0';
        FilterString = 'All';
        labelCache = new map<string, string> ();
        
        //AccountObj = [select Name, Owner.Name,Senior_Client_Advisor__c,Market_Value__c ,Family_Group__c , Bessemer_Office__c ,  Type,
        //Estimated_Annual_Fees__c from Account where ID = :AccountId];
        AccountObj = [select Name, Owner.Name,Senior_Client_Advisor__c,Market_Value__c , Bessemer_Office__c ,  Type
        from Account where ID = :AccountId];
        AccountTitle = [select name from Account where Id = :AccountId].Name;
        createContactList () ;
        return null;
    }
    
    private Pagereference ReturnUrl ()
    {
        PageReference pageRef = new PageReference('/' + AccountId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void FilterList ()
    {
        createContactList ();
    }
    
    private void createContactList ()
    {
        FieldList = new list<fieldSet>();
        List<Connections__c> tmpConnectionList ; 
        
        if (FilterString == 'All')
        {
            string sqlFields =  BuildFieldList ('Default' );
            tmpConnectionList = database.query('select id,RecordType.Name, Contact_C1__r.Name ,Relationship_R1__c, Relationship_R1__r.Name, Relationship_R2__r.Name, Role_in_Relationship__c  ' + sqlFields + ' from Connections__c where Relationship_R1__c = \'' + AccountId + '\' ') ;
            //ContactList = [select id, Contact_C1__r.Name,Relationship_R2__r.Name, Relationship_R2__r.BillingCity, Contact_C1__r.MailingCity, Contact_C1__r.Email, Contact_C1__r.MobilePhone , Role_in_Relationship__c
            //  from Connections__c where Relationship_R1__c = :AccountId ];
                
        }
        if (FilterString == 'Contact')
        {
            string sqlFields =  BuildFieldList ('AllContacts' );
            tmpConnectionList = database.query('select id, RecordType.Name, Contact_C1__r.Name ,Relationship_R2__r.Name, Role_in_Relationship__c  ' + sqlFields + ' from Connections__c where Relationship_R1__c = \'' + AccountId + '\' and Contact_C1__c != null ') ;
            
        }
        if (FilterString == 'Relationship')
        {
            string sqlFields =  BuildFieldList ('AllRelationships' );
            tmpConnectionList = database.query('select id,RecordType.Name, Contact_C1__r.Name ,Relationship_R2__r.Name, Role_in_Relationship__c  ' + sqlFields + ' from Connections__c where Relationship_R1__c = \'' + AccountId + '\' and Relationship_R2__c != null ') ; 
        }
        if (FilterString == 'Business')
        {
            string sqlFields =  BuildFieldList ('Business' );
            tmpConnectionList = database.query('select id,RecordType.Name, Contact_C1__r.Name ,Relationship_R2__r.Name, Role_in_Relationship__c  ' + sqlFields 
            + ' from Connections__c where Relationship_R1__c = \'' + AccountId + '\' and Relationship_R2__c != null and Relationship_R2__r.RecordTypeId = \'' + getRecordTypeId('Account' , 'Business') + '\' ') ;  
        }
        if (FilterString == 'NonClientContact')
        {
            string sqlFields =  BuildFieldList ('NonClientContact' );
            tmpConnectionList = database.query('select id,RecordType.Name, Contact_C1__r.Name ,Relationship_R2__r.Name, Role_in_Relationship__c  ' + sqlFields 
            + ' from Connections__c where Relationship_R1__c = \'' + AccountId + '\' and Relationship_R2__c = null and Contact_C1__c != null and Contact_C1__r.RecordTypeId = \'' + getRecordTypeId('Contact' , 'Non-Client Contact') + '\' ') ;  
        }
        if (FilterString == 'Trusts')
        {
            string sqlFields =  BuildFieldList ('Trusts' );
            tmpConnectionList = database.query('select id,RecordType.Name, Contact_C1__r.Name ,Relationship_R2__r.Name, Role_in_Relationship__c  ' + sqlFields 
            + ' from Connections__c where Relationship_R1__c = \'' + AccountId + '\' and Relationship_R2__c != null and Relationship_R2__r.RecordTypeId = \'' + getRecordTypeId('Account' , 'Trust/LLC') + '\'') ;  
        }
        
        ContactList = new List<Connections__c> ();
        for (Connections__c connection : tmpConnectionList)
        {
            if (connection.RecordType.Name == 'R:R' && connection.Relationship_R2__r.Name != null )
                ContactList.add(connection) ;
            if (connection.RecordType.Name == 'R:C' && connection.Contact_C1__c != null )
                ContactList.add(connection) ;
            if (connection.RecordType.Name == 'C:C' && connection.Contact_C2__c != null )
                ContactList.add(connection) ;
            
        }
        
    }
    
    private string getRecordTypeId (string objName , string typeName)
    {
        list<RecordType> rt = database.query('select id from RecordType where SobjectType = \'' + objName + '\' and Name = \'' + typeName + '\''); 
        string retVal = '';
        if (!rt.isEmpty())
            retVal = rt[0].id;
        
        return retVal;
    }
    
    private string BuildFieldList(string filterType)
    {
        
        
        List<AddressConnectionMap__c> addressMap = AddressConnectionMap__c.getall().values();
        string sqlFields = '' ;
        list<fieldSet> fldSetList = new list<fieldSet>();
        string fieldLabel  ='';
        
        for ( AddressConnectionMap__c fieldMap : addressMap )
        {
            if (fieldMap.Type__c ==  filterType)
            {
                if (!labelCache.containskey(fieldMap.FieldName__c))
                {
                    string [] fieldSplit = fieldMap.FieldName__c.split('\\.');
                    if (fieldSplit.size() > 0 )
                    {
                        if (fieldSplit[0].contains('Relationship'))
                            fieldLabel = getFieldLabel('Account' , fieldSplit[1] );
                        if (fieldSplit[0].contains('Contact'))
                            fieldLabel = getFieldLabel('Contact' , fieldSplit[1] );
                        
                        labelCache.put (fieldMap.FieldName__c ,  fieldLabel);
                    }
                }
                else
                    fieldLabel = labelCache.get(fieldMap.FieldName__c) ;
            
                fldSetList.add(new fieldSet(Integer.ValueOf(fieldMap.Sort_Order__c),fieldMap.FieldName__c , fieldLabel) );
                if (sqlFields.length() == 0)
                    sqlFields = fieldMap.FieldName__c;
                else
                    sqlFields += ',' + fieldMap.FieldName__c;
            }
        }
        
        if (fldSetList.size() > 0)
        {
            fldSetList.Sort();
            for (fieldSet f : fldSetList)
                FieldList  = fldSetList;
        }
        
        if (sqlFields.length() > 0) 
            sqlFields = ',' + sqlFields ;
        return sqlFields    ;
    }
    
    public string getFieldLabel ( string objType , string FieldName)
    {
        SObjectField fieldToken ; 
        
        if (accountFields == null)
        {
            SObjectType objToken = Schema.getGlobalDescribe().get('Account');
            DescribeSObjectResult objDef = objToken.getDescribe();
            accountFields = objDef.fields.getMap(); 
            
        }
        if (contactFields == null)
        {
            SObjectType objTokenContacts = Schema.getGlobalDescribe().get('Contact');
            DescribeSObjectResult objDefContacts = objTokenContacts.getDescribe();
            contactFields= objDefContacts.fields.getMap(); 
        }
        
        if (objType=='Account')
            fieldToken = accountFields.get(FieldName);
        
        if (objType=='Contact')
            fieldToken = contactFields.get(FieldName); 
        
        DescribeFieldResult selectedField = fieldToken.getDescribe();
        return selectedField.getLabel()  ;  
    
    }
    public void SaveRoleChange ()
    {
        //string selId = System.currentPagereference().getParameters().get('perfId');
        
        
        System.debug('***: ' + SelectedId);
        ContactObj = new Contact();
        
        for (Connections__c c : ContactList  )
        {
            if (c.id == SelectedId )
            {
                
                System.debug('**: ' + c);
                update c;
                break;
            }
        }
        
    }
    public void ResetConnectionObj()
    {
        hasError1 = '0';
        hasError2 = '0';
        if (ActionType == 'NewContact')
            ContactObj = new Contact();
        else
        {
            ConnectionObj = new Connections__c();
            ConnectionObjRel= new Connections__c();
        }
        
        System.Debug('***: ' + ContactObj);
    }
    public PageReference SaveAndNew ()
    {
        SaveNewConnection ();
        return null;
    }
    
    public PageReference SaveAndClose ()
    {
        SaveNewConnection ();
        if (hasError1 == '1' || hasError2 == '1' || hasDuplicates)
            return null;
        else
            return ReturnUrl();
    }
    
    
    
    public PageReference Cancel ()
    {
        return ReturnUrl();
    }
    
    //***************   private methods  ***********************************
    
    private void SaveNewConnection ()
    {
        hasError1 = '0';
        hasError2 = '0';
        hasDuplicates = false;
        Connections__c connection= new Connections__c();
        if (ActionType == 'Contact')
        {
            if (ConnectionObj.Contact_C1__c == null  )
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact Name is a required field'));
                hasError1 = '1';
            }
            if (ConnectionObj.Role_in_Relationship__c == null )
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact Role is a required field'));
                hasError2 = '1';
            }
            if (hasError1 == '1' || hasError2 == '1')
                return;
                
            connection.Relationship_R1__c = accountId;
            connection.Contact_C1__c = ConnectionObj.Contact_C1__c ;
            connection.Role_in_Relationship__c = ConnectionObj.Role_in_Relationship__c;
            connection.RecordTypeId = [select id from RecordType where sObjectType = 'Connections__c' and DeveloperName = 'R_C'  ].id;
            insert connection ;
            
        }
        if (ActionType =='Relationship')
        {
            if (ConnectionObjRel.Relationship_R2__c == null  )
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Relationship Name is arequired field'));
                hasError1 = '1';
                return;
            }
            if (ConnectionObjRel.Role_in_Relationship__c == null )
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Relationship Role is a required field'));
                hasError2 = '1';
            }
            if (hasError1 == '1' || hasError2 == '1')
                return;
            connection.Relationship_R1__c = accountId;
            connection.Relationship_R2__c = ConnectionObjRel.Relationship_R2__c ;
            connection.Role_in_Relationship__c = ConnectionObjRel.Role_in_Relationship__c;
            connection.RecordTypeId = [select id from RecordType where sObjectType = 'Connections__c' and DeveloperName = 'R_R'  ].id;
            insert connection ;
            
        }
        if (ActionType =='NewContact')
        {
           /* //Check For Duplicates
            GlobalContactSearch searchObj = new GlobalContactSearch ();
            dupList = searchObj.CheckForDuplicates(ContactObj);
            
            if (dupList.size() > 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'A duplicate contact(s) exists in the system.'));
                hasDuplicates = true;
                return;
            }
            */
            if (ContactObj.LastName == null || ContactObj.LastName.length() == 0)
            {    
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact LastName is a required field'));
                hasError1 = '1';
                return;
            }
            ContactObj.AccountId = accountId; 
            insert ContactObj;
            connection.Relationship_R1__c = accountId;
            connection.Contact_C1__c = ContactObj.id;
            connection.Role_in_Relationship__c = NewContactRole ;
            connection.RecordTypeId = [select id from RecordType where sObjectType = 'Connections__c' and DeveloperName = 'R_C'  ].id;
            insert connection ;
            
        }
        ResetConnectionObj();
        createContactList () ;
    }
    
    
    
    public class fieldSet implements Comparable 
    {
        public integer SortNumber {get;set;}
        public string Field {get;set;}
        public string fieldLabel {get;set;}
        
        public fieldSet (integer s , string f , string fldLabel)
        {
            SortNumber = s;
            Field = f;
            fieldLabel = fldLabel;
            
        }
        
        public Integer compareTo(Object compareTo) 
        {
            fieldSet fs = (fieldSet)compareTo;      
            if (SortNumber > fs.SortNumber )return 1;
            if (SortNumber == fs.SortNumber)return 0;
            return -1;
            
            
        }
    }
}