@isTest
global class testMethod_MockCalloutLaserApp  implements HttpCalloutMock 
{

	 global HTTPResponse respond(HTTPRequest req) 
	 {
        
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        //res.setHeader('Content-Type', 'application/json');
        res.setBody('{redurl: "http://abc.com", error: "0" }');
        res.setStatusCode(200);
       
        return res;
    }

}