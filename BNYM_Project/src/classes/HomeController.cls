/*
 *   Description   -   An visualforce page controller that is use for
 *                     an ipad BNYM Community Home page.
 *
 *    Version          Date          Description
 *    1.0              03/04/2015    Initial Draft 
 *
 */
public without sharing class HomeController {
    public  List<FeedItem> lstFeeds{get;set;}
    public String strUserCont { get; set; }
    public String strUserAcct { get; set; }
    public String loginUser{get;set;}
    public String loginUserName{get;set;}
    public String selectedPost{get;set;}
    public String strCmntBody{get;set;}
    public Decimal percentInv {get;set;}
    public Decimal percentBanking {get;set;} 
    public Integer totalCurrentValue {get;set;}
    public Decimal totalInvsCurrValue {get;set;}
    public Decimal totalBankCurrValue {get;set;}
    public Integer totalassetCurrentValueSum {get;set;}
    public Integer netWorthFinancial {get;set;}
    public Integer netHouseholdWorth {get;set;}
    public Integer netWorthFinancialAcct {get;set;}
    public Integer netWorth {get;set;}
    public String strEvents{get;set;}
    public String eventTimes{get;set;}
    public List<AggregateResult> lstaFinancial   = new List<AggregateResult>();
    public List<AggregateResult> lstAssetLiblity = new List<AggregateResult>();
    /* variables declaration for my banking section 
    */
    public List<AccTeamWrap> lstWrapAccTeam{get;set;}
    public List <AccountTeamMember> lstAccTeam {get;set;}
    public List<Event> lstEve{get;set;}
    public String uId{get;set;}
    public set<String> setUserId{get;set;}
    public Map<Id,Event> mapIdEvent{get;set;}
    public String eventTime{get;set;}
    public String eventOwnerId{get;set;}
    public String strChartUrl{get;set;} 
    public List < Case > lstcase {get;set;}
    public List < Campwrap > lstwrapCamp {get;set;}
    public map<String,String> mpUserToPhoto {get;set;}
    public map<String,List<FeedComment>> mpIdTofComment {get;set;}
    public map<String,String> mpIdToBody {get;set;}
    public set<String> setFeedItem;
    
    private static final String STR_REL_MANAGER = 'Relationship Manager';
    private static final String STR_PRI_BANKER = 'Private Banker';
    private static final String STR_INVESTMENT = 'Investment';
    private static final String STR_BANKING = 'Banking';
    private static final String STR_LOAN = 'Loan';
    private static final String STR_MORTAGE = 'Mortgage';
    private static final String STR_ASSET = 'Asset';
    private static final String STR_LIABILITY = 'Liability';
    private static final String STR_PRO_TYPE = 'Product_Type__c';
    private static final String STR_ACC_TYPE = 'Account_Type__c';
    private static final String STR_TYPE = 'Type__c';
    private static final String STR_CLOSED = 'Closed';
    //constructor starts  
    public HomeController(){
        percentInv = 0.00;
        percentBanking = 0.00;
        totalCurrentValue = 0;
        totalInvsCurrValue = 0;
        totalBankCurrValue = 0;
        totalassetCurrentValueSum = 0;
        netWorthFinancial = 0;
        netHouseholdWorth = 0;
        netWorthFinancialAcct = 0;
        netWorth = 0;
    }//constructor ends
    public pagereference init(){
        PageReference pageRef; 
        /*
        if(checkPlatform() == 'desktop'){
             pageRef = new PageReference('/BNYMWM/apex/Community_Customer_Home');
             return pageRef;
        }*/
        loginUser = userinfo.getUserId();
        loginUserName = userinfo.getName();
        User uRec = [Select ContactId,AccountId,SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId;
        if(uRec != null && uRec.AccountId != null)
            strUserAcct = uRec.AccountId;
        lstaFinancial = [Select
                               Product_Type__c, Account_Type__c, Name, Id , Sum(Current_Value__c) 
                         From
                               Financial_Account__c
                         where
                               Relationship_Entity__c =: strUserAcct
                         GROUP BY
                               Product_Type__c ,Id, Account_Type__c,Name];

       lstAssetLiblity = [Select
                                Id,Type__c ,Sum(Current_Value__c)
                          From
                                Assets_and_Liabilities__c
                          where
                                Account__c =: strUserAcct
                          GROUP BY
                                Id,Type__c ];
        getChatterData();
        getFinancialAccounts();
        getNetWorth();
        getlstcamp();
        getlstcase();
        getlstAccountTeam();
        return null;
    }
    //code to get chatter data and display in messages
    public pagereference getChatterData(){
        mpUserToPhoto = new map<String,String>();
        mpIdTofComment = new map<String,List<FeedComment>>();
        mpIdToBody = new map<String,String>();
        set<String> setUserReplId = new set<String>();
        	setUserReplId.add(loginUser);
        setFeedItem = new set<String>();
        String uName = userinfo.getName();
        lstFeeds = new List<FeedItem>();
        String searchquery ='FIND \''+uName+'* OR '+loginUser+'\'IN ALL FIELDS RETURNING FeedItem(id,Body,ParentId,CreatedDate,CreatedById order by CreatedDate DESC limit 3)'; 
        List<List<SObject>>searchList = search.query(searchquery);
        for(List<SObject> sobjOur: searchList){
            for(SObject sObj:sobjOur){
                FeedItem fItm = (FeedItem)sObj;
                lstFeeds.add(fItm);
                if(fItm.Body.contains('@'+uName))
                    mpIdToBody.put(fItm.id,fItm.Body.replace('@'+uName,''));
                else
                    mpIdToBody.put(fItm.id,fItm.Body);
                setUserReplId.add(fItm.CreatedById);
                setFeedItem.add(fItm.Id);
            }
        }
        for(FeedComment fComment:[Select Id,
                                         FeedItemId,
                                         CreatedBy.Id,
                                         CreatedById,
                                         CommentBody
                                         From FeedComment where FeedItemId in :setFeedItem order by CreatedDate DESC])
        {
            setUserReplId.add(fComment.CreatedById);
            if(!mpIdTofComment.containsKey(fComment.FeedItemId)){
                mpIdTofComment.put(fComment.FeedItemId,new List<FeedComment>{fComment});
            }
            else if(mpIdTofComment.containsKey(fComment.FeedItemId)){
                if(mpIdTofComment.get(fComment.FeedItemId).size() < 2)
                    mpIdTofComment.get(fComment.FeedItemId).add(fComment);
            }
            if(fComment.CommentBody.contains('@'+uName))
                mpIdToBody.put(fComment.id,fComment.CommentBody.replace('@'+uName,''));
            else
                mpIdToBody.put(fComment.id,fComment.CommentBody);
        }
        for(String strFeed:setFeedItem){
            
            if(!mpIdTofComment.containsKey(strFeed)){
                FeedComment fc = new FeedComment();
                mpIdTofComment.put(strFeed,new List<FeedComment>{fc});
            }
        }
        for(User usr:[Select Id, SmallPhotoUrl From User where id = :setUserReplId])
        {
            mpUserToPhoto.put(usr.id,usr.SmallPhotoUrl);
        }
        return null;
    }
    public pagereference getPostId(){
        return null;
    }
    //code to insert new feed comment
    public pagereference addfeedComment(){
        if(String.isNotBlank(selectedPost) && String.isNotBlank(strCmntBody)){
        FeedComment fc = new FeedComment();
            fc.FeedItemId = selectedPost;
            fc.CommentBody = strCmntBody;
            insert fc;
            strCmntBody = '';
            getChatterData();
        }
        return null;
    }
    //code to insert new feedItem
    public pagereference addfeedItem(){
        if(String.isNotBlank(strCmntBody)){
        FeedItem fItem = new FeedItem();
            fItem.Body = strCmntBody;
            fItem.ParentId = loginUser;
            if(Network.getNetworkId() != null){
                fItem.NetworkScope=Network.getNetworkId();
            }           
            insert fItem;
            strCmntBody = '';
            if(fItem.id != null){
            	setFeedItem.add(fItem.id);
            }
            String uName = userinfo.getName();
            if(lstFeeds.size() > 0)
            	lstFeeds.clear();
           for(FeedItem fItm:[Select id,Body,ParentId,CreatedDate,CreatedById 
           					  from FeedItem where id =:setFeedItem 
           					  order by CreatedDate DESC limit 3])
           {
            	lstFeeds.add(fItm);
	            if(!mpIdToBody.containsKey(fItm.Id)){
	            	if(fItm.Body.contains('@'+uName))
	                    mpIdToBody.put(fItm.id,fItm.Body.replace('@'+uName,''));
	                else
	                    mpIdToBody.put(fItm.id,fItm.Body);
                	FeedComment fc = new FeedComment();
                	mpIdTofComment.put(fItm.Id,new List<FeedComment>{fc});
	            }
           }
        }
        return null;
    }
    //Method to fetch the list of Case
    public void getlstcase() {
        if(String.isNotBlank(strUserCont)){
		      lstcase = [SELECT Id ,Subject ,Status,ContactId 
		                                FROM Case 
		                                WHERE Status !=: STR_CLOSED
		                                AND ContactId= :strUserCont ORDER 
		                                BY CreatedDate DESC LIMIT 3];
        }
    }
    //class and method for event section
    //Subclass : Wrapper Class for camp
    public class Campwrap {
    //Static Variables
    public string id {get; set;} 
    public string Name {get; set;}
    public String StartDate {get; set;}
    public String strStatus {get; set;}
        //Wrapper Class Controller
        Campwrap(String id,String Name,String sDate,String sStatus) {
            this.id = id;
            this.Name = Name;
            this.StartDate = sDate;
            this.strStatus = sStatus;
        }
    }
	//Method to fetch next 3 Campaigns that the Running User is a Campaign Member of.
	public void getlstcamp() {
      lstwrapCamp = new List < Campwrap > ();
          if(String.isNotBlank(strUserCont)){
                List <CampaignMember> lstCampaign  = [Select Id, ContactId, Status, Campaign.Name, 
                                                             Campaign.Id, Campaign.IsActive,
                                                             Campaign.StartDate, CampaignId 
                                                      From   CampaignMember 
                                                      Where  ContactId = :strUserCont and
                                                             Campaign.IsActive = true
                                                      ORDER BY 
                                                             Campaign.StartDate LIMIT 3]; 
                for (CampaignMember c: lstCampaign) {
                    Date sDate = c.Campaign.StartDate;
                    String strDate = sDate.month()+'/'+sDate.day()+'/'+sDate.year();
                    lstwrapCamp.add(new Campwrap(c.Campaign.id,c.Campaign.Name,strDate,c.Status));
                }
          }
    }
    //get object Name from Custom setting
    /*public String StringUrl {get;set;}
    public List<Objectwrapper> lstwrapper = new List<Objectwrapper>();
    map<string,string> mapobjnamePrefix = new map<string,string>();
    public class Objectwrapper{
        public String namestr{get; set;}
        public String urlstr {get; set;}
        public Objectwrapper(String name,string str)
        {
            namestr = name;
            urlstr = str;
        }
    }
    public List<Objectwrapper> getLstwrapper() {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType stype : gd.values())
        {
            Schema.DescribeSObjectResult r = stype.getDescribe();
            mapobjnamePrefix.put(r.getName(),r.getKeyPrefix());
        }
        list<NavigationMenu__c> lstDynamicObj = [ SELECT Name,Object_Name_API__c FROM NavigationMenu__c ORDER BY Object_Name_API__c];
        for(NavigationMenu__c objcs : lstDynamicObj){
              if(mapobjnamePrefix.get(objcs.Object_Name_API__c) != null){
                string lpstr = '/'+ mapobjnamePrefix.get(objcs.Object_Name_API__c);
                system.debug('>>>>>lpstr'+lpstr);
                if(objcs.Object_Name_API__c == 'Account')
                    lpstr = '/'+strUserCont;
                lstwrapper.add(new Objectwrapper(objcs.Name,lpstr));
              }
              else if (objcs.Object_Name_API__c == 'Chatter')
                lstwrapper.add(new Objectwrapper(objcs.Name,'/'+loginUser));
        }
        return lstwrapper; 
    }*/
    //Method for calculate percentage of Investment and Banking product types for chart section
    public void getFinancialAccounts(){
        Decimal investmentCount = 0;
        Decimal bankingCount = 0;
        Integer currentValueSum = 0;
        Integer investSum = 0;
        Integer bankSum = 0;
        if(String.isNotBlank(strUserAcct)){
            for(AggregateResult agrR : lstaFinancial){
                if(agrR != null ){
                    if(agrR.get('expr0') != null && agrR.get(STR_PRO_TYPE) !=null && agrR.get(STR_PRO_TYPE) != ''){
                        currentValueSum  = currentValueSum + Integer.valueOf(agrR.get('expr0'));
                        if(agrR.get(STR_PRO_TYPE).equals(STR_INVESTMENT)){
                            investmentCount++;
                            investSum = investSum + Integer.valueOf(agrR.get('expr0'));
                        }
                        else if(agrR.get(STR_PRO_TYPE).equals(STR_BANKING)){
                            bankingCount++;
                            bankSum = bankSum + Integer.valueOf(agrR.get('expr0'));
                        }
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size()>0){
                 totalCurrentValue = currentValueSum;
                 totalInvsCurrValue = investSum;
                 totalBankCurrValue = bankSum;
                 percentInv = ((totalInvsCurrValue / totalCurrentValue )*100).setScale(2);
                 percentBanking = ((totalBankCurrValue / totalCurrentValue)*100).setScale(2);
            }
        }
      String strInv = string.valueof(percentInv);
      String strBank = string.valueof(percentBanking);
      strChartUrl = '/BNYMWM/apex/MyFinancialAccount?inv='+strInv+'&bank='+strBank;
    }
    //Method for calculate net worth for net worth section
    public void getNetWorth(){
        Integer loanSum = 0;
        Integer mortageSum = 0;
        Integer assetSum = 0;
        Integer liabilitySum = 0;
        Integer assetAccConSum = 0;
        Integer liabilityAccConSum = 0;
        Integer loanNetSum = 0;
        Integer mortageNetSum = 0;
        Integer assetCurrentValueSum = 0;
        if(String.isNotBlank(strUserAcct) && String.isNotBlank(strUserCont)){
            for(AggregateResult agrRFin : lstaFinancial){
                if(agrRFin != null ){
                    if(agrRFin.get('expr0') != null && agrRFin.get(STR_ACC_TYPE) !=null && agrRFin.get(STR_ACC_TYPE) != ''){
                        if(agrRFin.get(STR_ACC_TYPE).equals(STR_LOAN)){
                            loanSum = loanSum + Integer.valueOf(agrRFin.get('expr0'));
                        }
                        if(agrRFin.get(STR_ACC_TYPE).equals(STR_MORTAGE)){
                            mortageSum = mortageSum + Integer.valueOf(agrRFin.get('expr0'));
                        }
                    }
                }
            }
            for(AggregateResult assetAgrR : lstAssetLiblity){
                if(assetAgrR != null ){
                    if(assetAgrR.get(STR_TYPE) !=null && assetAgrR.get(STR_TYPE) != '' && assetAgrR.get('expr0') != null){
                        if(assetAgrR.get(STR_TYPE).equals(STR_ASSET)){
                            assetAccConSum = assetAccConSum + Integer.valueOf(assetAgrR.get('expr0'));
                        } 
                        if(assetAgrR.get(STR_TYPE).equals(STR_LIABILITY)){
                            liabilityAccConSum = liabilityAccConSum + Integer.valueOf(assetAgrR.get('expr0'));
                        }
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size()>0 && lstAssetLiblity != null && lstAssetLiblity.size() > 0){
                netWorthFinancial = totalCurrentValue - (loanSum + mortageSum);
                netHouseholdWorth = (netWorthFinancial + assetAccConSum ) - liabilityAccConSum;
            }
            List<AggregateResult> lstFinancialAcct = [Select SUM(Financial_Account__r.Current_Value__c), 
                                                        Financial_Account__r.Account_Type__c, 
                                                        Financial_Account__r.Id, 
                                                        Contact__c 
                                                    From 
                                                        Financial_Account_Connection__c 
                                                    where 
                                                        Contact__c =: strUserCont
                                                    GROUP BY 
                                                          Financial_Account__r.Account_Type__c, 
                                                          Financial_Account__r.Id,Contact__c];
            for(AggregateResult agrR1 : lstFinancialAcct){
                if(agrR1 != null ){
                    if(agrR1.get('expr0') != null && agrR1.get(STR_ACC_TYPE) !=null && agrR1.get(STR_ACC_TYPE) != ''){
                        assetCurrentValueSum  = assetCurrentValueSum + Integer.valueOf(agrR1.get('expr0'));
                        if(agrR1.get(STR_ACC_TYPE).equals(STR_LOAN)){
                            loanNetSum = loanNetSum + Integer.valueOf(agrR1.get('expr0'));
                        }
                        else if(agrR1.get(STR_ACC_TYPE).equals(STR_MORTAGE)){
                            mortageNetSum = mortageNetSum + Integer.valueOf(agrR1.get('expr0'));
                        }
                    }
                }
            }
            List<AggregateResult> lstAssetAcctCont = [Select 
                                                        Assets_and_Liabilities__r.Account__c,
                                                        Contact__c, Assets_and_Liabilities__r.Type__c, 
                                                        SUM(Assets_and_Liabilities__r.Current_Value__c),
                                                        Assets_and_Liabilities__r.Id
                                                  From
                                                        Contact_Assets_and_Liabilities__c
                                                  where
                                                        Contact__c =: strUserCont
                                                  GROUP BY
                                                        Assets_and_Liabilities__r.Type__c,
                                                        Assets_and_Liabilities__r.Id,
                                                        Contact__c,
                                                        Assets_and_Liabilities__r.Account__c];
            for(AggregateResult objAssAcc : lstAssetAcctCont){
                if(objAssAcc != null ){
                    if(objAssAcc.get(STR_TYPE) !=null && objAssAcc.get(STR_TYPE) != '' && objAssAcc.get('expr0') != null){
                        if(objAssAcc.get(STR_TYPE).equals(STR_ASSET)){
                            assetSum = assetSum + Integer.valueOf(objAssAcc.get('expr0'));
                        }
                        else if(objAssAcc.get(STR_TYPE).equals(STR_LIABILITY)){
                            liabilitySum = liabilitySum + Integer.valueOf(objAssAcc.get('expr0'));
                        }
                    }
                }
            }
            if(lstFinancialAcct != null && lstFinancialAcct.size()>0 && lstAssetAcctCont != null && lstAssetAcctCont.size() > 0){
                totalassetCurrentValueSum = assetCurrentValueSum ;
                netWorthFinancialAcct = totalassetCurrentValueSum - (loanNetSum + mortageNetSum);
                netWorth = (netWorthFinancialAcct + assetSum ) - liabilitySum;
            }
        }
     }
     //for checking on which platform page is working
     public String checkPlatform(){
        String retVal = '';
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_OPERAMINI_PHONE_UA');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_SKYFIRE_PHONE');

        //& replace with custom setting - using (?i) case insensitive mode.
        String deviceReg = '(?i)(iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce)';//iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce
        String ipadReg = '(?i)(ipad)';
        String iphoneReg = '(?i)(iphone)';
        String mobileReg = '(?i)(mobile)';
        String androidReg = '(?i)(android)';
        String desktopReg = '(?i)(windows|linux|os\\s+[x9]|solaris|bsd)';
        String botReg = '(?i)(spider|crawl|slurp|bot)';
        Matcher ipadM = Pattern.compile( ipadReg ).matcher( userAgent );
        Matcher iphoneM = Pattern.compile( iphoneReg ).matcher( userAgent );
        Matcher mobileM = Pattern.compile( mobileReg ).matcher( userAgent );
        Matcher androidM = Pattern.compile( androidReg ).matcher( userAgent );

        Matcher m = Pattern.compile(deviceReg).matcher(userAgent);
        if( m.find(0) )
        {
            if( ipadM.find(0) || ( !mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = 'Tablet';
                system.debug('This is for tablet');
            }
            else if( iphoneM.find(0) || ( mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = 'Phone';
                system.debug('This is for Phone');
            }
        }
        else
        {
            m = Pattern.compile(desktopReg).matcher(userAgent);
            if( m.find(0) )
                retVal = 'desktop';
            m = Pattern.compile(botReg).matcher(userAgent);
            if( m.find(0) )
                retVal = 'desktop';
                system.debug('This is for desktop');
        }
        return retVal;
    }
    /* Method For getting account team member user of logged in user 
    *  and thier related events in my banking section
    */
    public void getlstAccountTeam() {
	    mapIdEvent = new Map<Id,Event>();
	    setUserId = new set<String>(); 
	    lstWrapAccTeam = new List<AccTeamWrap>();
        if(String.isNotBlank(strUserAcct)){
               lstAccTeam  = [Select 
                                     UserId, TeamMemberRole, AccountId,User.Name,User.FullPhotoUrl,User.Phone
                              From 
                                     AccountTeamMember
                              Where
                                     AccountId = :strUserAcct and
                                     (TeamMemberRole =: STR_REL_MANAGER or TeamMemberRole =: STR_PRI_BANKER ) ]; 
                for (AccountTeamMember accTeam: lstAccTeam) {
                       setUserId.add(accTeam.UserId);
                }
          }
          lstEve = [Select
                         id,WhoId, Subject, ActivityDate, StartDateTime, CreatedDate, OwnerId
                    From
                         Event
                    where
                         OwnerId IN: setUserId and 
                         WhoId = :strUserCont and
                         StartDateTime >= TODAY
                    order by 
                         ActivityDate NULLS LAST];
           for(Event event : lstEve){
	            if(!mapIdEvent.containsKey(event.OwnerId)){
	                mapIdEvent.put(event.OwnerId , event);
	            }
           }
           for (AccountTeamMember accTeams: lstAccTeam){
	            Event eve = mapIdEvent.get(accTeams.UserId);
	            if(accTeams != null ){
	                if(eve != null && eve.ActivityDate != null && eve.Subject != null && eve.Subject != ''){
	                    strEvents = eve.ActivityDate.month()+'/'+ eve.ActivityDate.day();
	                    eventTimes = ' at ' + eve.StartDateTime.format('hh:mm a');
	                }
	                    lstWrapAccTeam.add(new accTeamWrap(accTeams,eve,strEvents,eventTimes));
	            }
	            strEvents = '';
	            eventTimes = '';
            }
     }
     public class accTeamWrap {
	    //Static Variables
	    public string userName {get; set;} 
	    public string teamMemberRole {get; set;}
	    public string userUrl {get; set;}
	    public string phone {get; set;}
	    public Event objEve{get;set;}
	    public String eventTime{get;set;}
	    public String strEvent{get;set;}
	    public String strUserid{get;set;}
        //Wrapper Class Controller
        public AccTeamWrap(AccountTeamMember objATeam , Event objEvent ,String strEvents ,String eventTimes ) { 
            this.userName = objATeam.User.Name;
            this.teamMemberRole = objATeam.TeamMemberRole;
            this.strUserid = objATeam.UserId;
            this.userUrl = objATeam.User.FullPhotoUrl;
            this.phone = objATeam.User.Phone;
            this.objEve = objEvent;
        	this.strEvent = strEvents;
        	this.eventTime = eventTimes;
        }
    }
}