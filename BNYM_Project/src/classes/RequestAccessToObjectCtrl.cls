global without sharing class RequestAccessToObjectCtrl 
{
	@RemoteAction
	global static string SendRequestEmail  (string RequestAccessId , boolean RequestOwnership , string RequestMessage)
	{
		return RequestAccessHelper.SendRequestEmail (RequestAccessId , UserInfo.getUserId() ,RequestOwnership, RequestMessage) ;
		
	}
	@RemoteAction
    global static string getOwner(string objId) 
    {
    	string retval = '';
    	if (objId.substring(0,3) == '001' )
    	{
    		System.Debug(objId);
        	Account acct = [SELECT Owner.Name FROM Account WHERE id = :objId];
        	retval =  acct.Owner.Name;
    	}
    	if (objId.substring(0,3) == '003' )
    	{
        	Contact ct = [SELECT Owner.Name FROM Contact WHERE id = :objId];
        	retval =  ct.Owner.Name;
    	}
    	if (objId.substring(0,3) == '00Q')
		{
			Lead ld = [select Owner.Name from Lead where id = :objId];
			retval = ld.Owner.Name;
		}
    	return  retval;
    }
}