/*
 *   Description - An component controller that exposes the chatter(MyMessages) functionality
 *                 on ipad BNYM Community Home page.
 *    Version          Date          Description
 *    1.0              11/05/2015    Initial Draft 
 *
 */
public without sharing class MyMessagesCtrl {
    public  List<FeedItem> lstFeeds{get;set;}
    public String loginUser{get;set;}
    public String selectedPost{get;set;}
    public String strCmntBody{get;set;}
    public map<String,String> mpUserToPhoto {get;set;}
    public map<String,List<FeedComment>> mpIdTofComment {get;set;}
    public map<String,String> mpIdToBody {get;set;}
    public set<String> setFeedItem;
	//constructor starts  
    public MyMessagesCtrl(){
    	loginUser = userinfo.getUserId();
		getChatterData();
    }//constructor ends

    //code to get chatter data and display in messages
    public pagereference getChatterData(){  
        mpUserToPhoto = new map<String,String>();
        mpIdTofComment = new map<String,List<FeedComment>>();
        mpIdToBody = new map<String,String>();
        set<String> setUserReplId = new set<String>();
        	setUserReplId.add(loginUser);
        setFeedItem = new set<String>();
        String uName = userinfo.getName();
        lstFeeds = new List<FeedItem>();
        String searchquery ='FIND \''+uName+'* OR '+loginUser+'\'IN ALL FIELDS RETURNING FeedItem(id,Body,ParentId,CreatedDate,CreatedById order by CreatedDate DESC limit 3)'; 
        List<List<SObject>>searchList = search.query(searchquery);
        for(List<SObject> sobjOur: searchList){
            for(SObject sObj:sobjOur){
                FeedItem fItm = (FeedItem)sObj;
                lstFeeds.add(fItm);
                if(fItm.Body.contains('@'+uName))
                    mpIdToBody.put(fItm.id,fItm.Body.replace('@'+uName,''));      
                else
                    mpIdToBody.put(fItm.id,fItm.Body);      
                setUserReplId.add(fItm.CreatedById);
                setFeedItem.add(fItm.Id);
            }
        }
        for(FeedComment fComment:[Select Id, 
                                         FeedItemId, 
                                         CreatedBy.Id, 
                                         CreatedById,
                                         CommentBody 
                                         From FeedComment where 
                                         FeedItemId in :setFeedItem order by CreatedDate DESC])
        {
            setUserReplId.add(fComment.CreatedById);        
            if(!mpIdTofComment.containsKey(fComment.FeedItemId)){
                mpIdTofComment.put(fComment.FeedItemId,new List<FeedComment>{fComment});
            }
            else if(mpIdTofComment.containsKey(fComment.FeedItemId)){
                if(mpIdTofComment.get(fComment.FeedItemId).size() < 2)
                    mpIdTofComment.get(fComment.FeedItemId).add(fComment);
            }
            if(fComment.CommentBody.contains('@'+uName))
                mpIdToBody.put(fComment.id,fComment.CommentBody.replace('@'+uName,''));
            else
                mpIdToBody.put(fComment.id,fComment.CommentBody);                
        }
        for(String strFeed:setFeedItem){
            if(!mpIdTofComment.containsKey(strFeed)){
                FeedComment fc = new FeedComment();
                mpIdTofComment.put(strFeed,new List<FeedComment>{fc});
            }
        }
        for(User usr:[Select Id, SmallPhotoUrl From User where id = :setUserReplId])
        {
            mpUserToPhoto.put(usr.id,usr.SmallPhotoUrl);
        }
        return null;
    }

    public pagereference getPostId(){
        return null;
    }
    //code to insert new feed comment
    public pagereference addfeedComment(){
        if(selectedPost != null && selectedPost != '' && strCmntBody != null && strCmntBody != ''){
        	FeedComment fc = new FeedComment();
            fc.FeedItemId = selectedPost;
            fc.CommentBody = strCmntBody;
            insert fc;
            strCmntBody = '';
            getChatterData();
        }
        return null;
    }
    //code to insert new feedItem
    public pagereference addfeedItem(){
        if(strCmntBody != null && strCmntBody != ''){
        	FeedItem fItem = new FeedItem();
            fItem.Body = strCmntBody;
            fItem.ParentId = loginUser;
            if(Network.getNetworkId() != null){
                fItem.NetworkScope=Network.getNetworkId();
            }           
            insert fItem;
            strCmntBody = '';
            if(fItem.id != null){
            	setFeedItem.add(fItem.id);
            }
            String uName = userinfo.getName();
            if(lstFeeds.size() > 0)
            	lstFeeds.clear();
           for(FeedItem fItm:[Select id,Body,ParentId,CreatedDate,CreatedById 
           					  from FeedItem where id =:setFeedItem 
           					  order by CreatedDate DESC limit 3])
           {
            	lstFeeds.add(fItm);
	            if(!mpIdToBody.containsKey(fItm.Id)){
	            	if(fItm.Body.contains('@'+uName))
	                    mpIdToBody.put(fItm.id,fItm.Body.replace('@'+uName,''));      
	                else
	                    mpIdToBody.put(fItm.id,fItm.Body);
                	FeedComment fc = new FeedComment();
                	mpIdTofComment.put(fItm.Id,new List<FeedComment>{fc});
	            }
           }
        }
        return null;
    }   
}