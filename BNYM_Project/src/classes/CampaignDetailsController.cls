public without sharing class CampaignDetailsController {
public List < Campwrap > lstwrapCamp {get;set;}
public String strUserCont { get; set; }

public List <CampaignMember> lstCampaign {get;set;}

    public CampaignDetailsController(){
    	User uRec = [Select ContactId,AccountId From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId;
    }
  //class and method for event section
    //Subclass : Wrapper Class for camp
  public class Campwrap {
  //Static Variables
    public Campaign campMember {get; set;}
    public String StartDate {get; set;}
    //Wrapper Class Controller
    Campwrap(Campaign campMember,String sDate) {
    	if(campMember != null){
    		campMember = campMember;
    	}
        this.campMember = campMember;
        this.StartDate = sDate;
    }
  }
  //Method to bring the list of Campaign and Serialize Wrapper Object as JSON
  public void getlstcamp() {
      lstwrapCamp = new List < Campwrap > ();
      lstCampaign = new List <CampaignMember> ();
                lstCampaign  = [Select Id,
                                       ContactId, 
                                       Campaign.Name, 
                                       Campaign.Id,
                                       Campaign.IsActive,
                                       Campaign.Status, 
                                       Campaign.StartDate,
                                       CampaignId 
                                From   CampaignMember
                                Where  ContactId = :strUserCont ]; 
                system.debug('<<<<<<<<<<<lstCampaign  '+lstCampaign );
                for (CampaignMember c: lstCampaign) { 
                    Date sDate = c.Campaign.StartDate;
                    String strDate = sDate.month()+'/'+sDate.day()+'/'+sDate.year();
                    lstwrapCamp.add(new Campwrap(c.Campaign,strDate));
                }
  }
    public String checkPlatform()
    {
        String retVal = '';
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_OPERAMINI_PHONE_UA');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_SKYFIRE_PHONE');


        //& replace with custom setting - using (?i) case insensitive mode.
        String deviceReg = '(?i)(iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce)';//iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce
        String ipadReg = '(?i)(ipad)';
        String iphoneReg = '(?i)(iphone)';
        String mobileReg = '(?i)(mobile)';
        String androidReg = '(?i)(android)';
        String desktopReg = '(?i)(windows|linux|os\\s+[x9]|solaris|bsd)';
        String botReg = '(?i)(spider|crawl|slurp|bot)';
        Matcher ipadM = Pattern.compile( ipadReg ).matcher( userAgent );
        Matcher iphoneM = Pattern.compile( iphoneReg ).matcher( userAgent );
        Matcher mobileM = Pattern.compile( mobileReg ).matcher( userAgent );
        Matcher androidM = Pattern.compile( androidReg ).matcher( userAgent );

        Matcher m = Pattern.compile(deviceReg).matcher(userAgent);
        if( m.find(0) )
        {
            if( ipadM.find(0) || ( !mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = 'Tablet';
                system.debug('This is for tablet');
            }
            else if( iphoneM.find(0) || ( mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = 'Phone';
                system.debug('This is for Phone');
            }
        }
        else
        {
            m = Pattern.compile(desktopReg).matcher(userAgent);
            if( m.find(0) )
                retVal = 'desktop';
                system.debug('This is for desktop');
             
            m = Pattern.compile(botReg).matcher(userAgent);
            if( m.find(0) )
                retVal = 'desktop';
                system.debug('This is for desktop');
        }

        return retVal;
    }

}