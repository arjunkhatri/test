<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <customSettingsVisibility>Public</customSettingsVisibility>
    <description>This custom settings stores the field mappings between the goalgamiPro Financial Account object and an existing financial account object.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ASI_Is_Copied__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to store the is copied field API Name</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of Is Copied field from subscriber financial account</inlineHelpText>
        <label>Is Copied</label>
        <length>200</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_isPersonAccountMapping__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field is used to indicate if the mapping is for Person/Business Account</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this checkbox if the mapping is for Person Accounts</inlineHelpText>
        <label>isPersonAccountMapping</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Account_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the mapping for the account name field</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the API name of the respective Account Name field used</inlineHelpText>
        <label>Account Name</label>
        <length>250</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Number__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Account Number</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Account Number</inlineHelpText>
        <label>Account Number</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cash_Balance__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Cash Balance</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Cash Balance</inlineHelpText>
        <label>Cash Balance</label>
        <length>250</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Comments</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Comments</inlineHelpText>
        <label>Comments</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Custom_Financial_Account_Object_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the financial account object name</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the API name of the custom Financial Account object</inlineHelpText>
        <label>Custom Financial Account Object Name</label>
        <length>250</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Financial_Account_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the mapping for the field used to capture the name of custom financial account</description>
        <externalId>false</externalId>
        <inlineHelpText>Please enter the name of the field used to capture the name of custom financial account</inlineHelpText>
        <label>Financial Account Name</label>
        <length>250</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Investment_HoldingsValue__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Investment Holdings Value</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Investment Holdings Value</inlineHelpText>
        <label>Investment HoldingsValue</label>
        <length>250</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Long_Term_Gain_Loss__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Long Term Gain/Loss</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Long Term Gain/Loss</inlineHelpText>
        <label>Long Term Gain/Loss</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Primary_Owner__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Primary Owner</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Primary Owner</inlineHelpText>
        <label>Primary Owner</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Short_Term_Gain_Loss__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Short Term Gain/Loss</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Short Term Gain/Loss</inlineHelpText>
        <label>Short Term Gain/Loss</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the API name of the field equivalent to Type</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter API name of the field to be used for Type</inlineHelpText>
        <label>Type</label>
        <length>250</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User_to_notify_on_Batch__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to capture the username of the user whom to notify in Batch errors</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter username for the user to notify on Batch Report</inlineHelpText>
        <label>User to notify on Batch</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Financial Account Object Mapping</label>
</CustomObject>
