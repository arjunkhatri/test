public with sharing class TwilioCloudCommunicationClass {
    // Public Properties
    public String SelectedMobileNumber{get;set;}
    public String OtherMobileNumber{get;set;}
    public String textMessage{get;set;}

    // Default construtor
    public TwilioCloudCommunicationClass(){
        SelectedMobileNumber  = '' ;
        OtherMobileNumber = '' ;
    }

    public List<SelectOption> getPersonList(){
        try{
            List<SelectOption> localList = new List<SelectOption>();
            localList.add(new SelectOption('' , '--Select--'));
            for(contact cont : [select Name,MobilePhone from contact where Is_RegisteredTwilioUser__c = true ])
            {
                localList.add(new SelectOption(cont.MobilePhone , cont.Name));          
            }      
            localList.add(new SelectOption('other' , 'Other'));
            return localList ;
        }
        catch(Exception e)
        {
            ApexPages.addMessages(e);      
            return null;
        }
    }

    public void SendSMS()
    {
        try{      
            SelectedMobileNumber = (SelectedMobileNumber == '')? OtherMobileNumber:SelectedMobileNumber ;
            if(SelectedMobileNumber != '')
            {
                List<TwilioConfig__c> AdminInfo = TwilioConfig__c.getall().values();
                String ACCOUNT_SID = '';
                String AUTH_TOKEN  = '' ;            
                String SenderMobileNumber = '' ;
                // Informaton getting from custom setting
                if(AdminInfo.size()>0)
                {          
                    ACCOUNT_SID             = AdminInfo[0].AccountSid__c;
                    AUTH_TOKEN              = AdminInfo[0].AuthToken__c;                
                    SenderMobileNumber      = AdminInfo[0].Admin_Mobile_Number__c;    
                }            
                TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
               
                Map<String,String> properties = new Map<String,String> {
                            'To'   => SelectedMobileNumber ,
                            'From' => SenderMobileNumber,
                            'Body' => textMessage
                    };
                TwilioSMS message = client.getAccount().getSmsMessages().create(properties);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Message has been sent'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Pelase provide valid Mobile Number '));
            }
        }catch(Exception e )
        {
            ApexPages.addMessages(e);      
            return ;
        }  
    }

}