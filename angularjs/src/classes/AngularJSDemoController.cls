public with sharing class AngularJSDemoController{
public String AccountList { get; set; }
	//Subclass : Wrapper Class
	public class Accountwrap {
	//Static Variables
		public string id;
		public string name;
		public string Phone;
		//Wrapper Class Controller
		Accountwrap() {
		Phone = '';
		}
	}

    // Instance fields
    public String searchTerm {get; set;}
    public String selectedMovie {get; set;}
    
    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<Account> searchMovie(String searchTerm) {
        System.debug('Movie Name is: '+searchTerm );
        List<Account> movies = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return movies;
    }





//Method to bring the list of Account and Serialize Wrapper Object as JSON
	public static String getlstAccount() {
			List < Accountwrap > lstwrap = new List < Accountwrap > ();
			List < account > lstacc = [SELECT 
			                                 Id,
			                                 Name,
			                                 Phone
			                           FROM
			                                 Account
			                           limit
			                                 100];
			for (Account a: lstacc) {
				Accountwrap awrap = new Accountwrap();
				awrap.id = a.id;
				awrap.name = a.name;
				if (a.Phone != null) {
				    awrap.Phone = a.Phone;
				}
				lstwrap.add(awrap);
			}
	       return JSON.serialize(lstwrap);
	}
}