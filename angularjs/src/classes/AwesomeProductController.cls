public class AwesomeProductController {
    @AuraEnabled
    public static List<AuraExample__Product__c> getProducts() {
        return [select id, name, photo__c, description__c, points__c from AuraExample__Product__c];
    }

    @AuraEnabled
    public static AuraExample__Product__c getProductByName(String name) {
        return [select id, name, photo__c, color__c,
                points__c, description__c
                //(select name from AuraExample__Product_Size__r order by name)
                from AuraExample__Product__c where name = :name];
    }
}