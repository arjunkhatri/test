public without sharing class MyFinancialAccountCtrl {
    //public Decimal percentInv = 0.00;
    //public Decimal percentBanking = 0.00;
    //public Decimal percentOther = 0.00;

    public MyFinancialAccountCtrl(){
        /*if(ApexPages.currentPage().getParameters().get('inv') != null && ApexPages.currentPage().getParameters().get('inv') != '')
              percentInv = decimal.valueof(ApexPages.currentPage().getParameters().get('inv'));
        if(ApexPages.currentPage().getParameters().get('bank') != null && ApexPages.currentPage().getParameters().get('bank') != '')
              percentBanking = decimal.valueof(ApexPages.currentPage().getParameters().get('bank'));*/
        //if(ApexPages.currentPage().getParameters().get('oth') != null && ApexPages.currentPage().getParameters().get('oth') != '')
              //percentOther = decimal.valueof(ApexPages.currentPage().getParameters().get('oth'));
    }

 //for pie chart 
   public List<PieWedgeData> getPieData() {
    system.debug('<<<<in pie method');
        List<PieWedgeData> data = new List<PieWedgeData>();
        data.add(new PieWedgeData('Investment', 45));
        data.add(new PieWedgeData('Banking', 55));
        //data.add(new PieWedgeData('Other', PercentOther));
        system.debug('<<<<in pie method list '+data);
        return data;
    }
    // Wrapper class for pie chart
    public class PieWedgeData {

        public String name { get; set; }
        public Decimal data { get; set; }

        public PieWedgeData(String name, Decimal data) {
            this.name = name;
            this.data = data;
        }
    }

}