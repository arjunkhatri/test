public without sharing class HeaderController {
    public String strUserCont { get; set; }
    public String StringUrl {get;set;}
    public List<Objectwrapper> lstwrapper = new List<Objectwrapper>();
    map<string,string> mapobjnamePrefix = new map<string,string>();
    public String loginUser{get;set;}
    public string photoUrl{get;set;}
    public String loginUserName{get;set;}

    public HeaderController(){
        loginUser = userinfo.getUserId();
        loginUserName = userinfo.getName();
        User uRec = [Select ContactId,AccountId,SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId;
        if(uRec != null && uRec.SmallPhotoUrl != null)
            photoUrl = String.valueOf(uRec.SmallPhotoUrl);
    }

    public class Objectwrapper{
        public String namestr{get; set;}
        public String urlstr {get; set;}
        public Objectwrapper(String name,string str)
        {
            namestr = name;
            urlstr = str;
        }
    }
 
    public List<Objectwrapper> getLstwrapper() {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType stype : gd.values())
        {
            Schema.DescribeSObjectResult r = stype.getDescribe();
            mapobjnamePrefix.put(r.getName(),r.getKeyPrefix());
        }
        list<NavigationMenu__c> lstDynamicObj = [ SELECT Name,Object_Name_API__c FROM NavigationMenu__c ORDER BY Object_Name_API__c];
        for(NavigationMenu__c objcs : lstDynamicObj){   
              if(mapobjnamePrefix.get(objcs.Object_Name_API__c) != null){
                string lpstr = '/'+ mapobjnamePrefix.get(objcs.Object_Name_API__c);
                system.debug('>>>>>lpstr'+lpstr);
                if(objcs.Object_Name_API__c == 'Account')
                    lpstr = '/'+strUserCont;
                lstwrapper.add(new Objectwrapper(objcs.Name,lpstr));
              }
              else if (objcs.Object_Name_API__c == 'Chatter')
                lstwrapper.add(new Objectwrapper(objcs.Name,'/'+loginUser));
        }
        return lstwrapper; 
    }

}