public class TwilioSMSAsync {
  @future (callout=true)
  public static void sendSMSCaseTeamList(Set<Id> Ids) {

    String caseNumber = '';
    String caseContactName = '';
    String caseContactAccountName = '';
    String caseTeamMemberContactId = '';
    String contactName = '';
    String toNumber = '';
    String messageBody = '';
    String SFDC_hostnameUrl = URL.getSalesforceBaseUrl().toExternalForm();

    // Get Case Info
    for (Case c: [SELECT Id, CaseNumber, AccountId, ContactId, Priority FROM Case WHERE id IN :Ids]) {
       // Only send SMS to case team members if the Priority of the case is set to ‘High’
        if (c.Priority == 'High') {
            caseNumber = c.CaseNumber;
            for (Contact cc: [SELECT Name FROM Contact WHERE id = :c.ContactId]) {
                caseContactName = cc.Name;
            }
            for (Account cca: [SELECT Name FROM Account WHERE id = :c.AccountId]) {
                caseContactAccountName = cca.Name;
            }
	// Create the SMS message body
            messageBody = 'SFDC High Priority Case ' + caseNumber + ': ' + 'For ' + caseContactName + '@' + caseContactAccountName + ' '  + SFDC_hostnameUrl + '/' + c.Id ;

            // The TwilioAPI helper class looks up your Twilio AccountSid and AuthToken from your current organization, in the TwilioConfig custom setting.
            // You can configure TwilioConfig by going to Settings->Develop->Custom Settings->Twilio_Config, and your AccountSid and AuthToken
            // can be found on the Twilio account dashboard
            TwilioRestClient SMSclient = TwilioAPI.getDefaultClient();

            // Iterate through all the Case Team Members and send them a SMS notification
            for (CaseTeamMember ct : [SELECT MemberId FROM CaseTeamMember WHERE ParentId = :c.Id]) {
                caseTeamMemberContactId = ct.MemberId;
                for (Contact cc: [SELECT name, MobilePhone FROM Contact WHERE id = :caseTeamMemberContactId]) {
                    contactName = cc.name;
                    toNumber = cc.MobilePhone;
                    // Format (+<Country Code><Number>) the toNumber
                    toNumber = '+' + toNumber.replaceAll('\\D', '');

                    // Setup the params for SMS message
                    Map<String,String> params = new Map<String,String> {
                        'To'   => toNumber,
                        'From' => '+12055066926',
                        'Body' => messageBody
                    };
                    //Send SMS out via Twilio
                    TwilioSMS sms = SMSclient.getAccount().getSmsMessages().create(params);
                }
            }
        }
    }
  }
}