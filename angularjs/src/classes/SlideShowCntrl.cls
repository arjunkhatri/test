public with sharing class SlideShowCntrl {
  public List<AggregateResult> lstaFinancial = new List<AggregateResult>();
  public List<AggregateResult> lstAssetLiblity = new List<AggregateResult>();
    public SlideShowCntrl(){
    lstaFinancial = [Select Type,Name ,Id,Rating, Sum(AnnualRevenue)
                                               From Account
                                               where OwnerId =: UserInfo.getUserId()
                                               GROUP BY Type ,Id, Name,Rating];
    lstAssetLiblity = [Select Name ,Id,Type__c, Sum(Revenue__c)
                                               From Member__c
                                               where OwnerId =: UserInfo.getUserId()
                                               GROUP BY Id, Name,Type__c];
       chartFinancial();
       netWorth();
    }
public String CaseList { get; set; }
  //Subclass : Wrapper Class
  public class Casewrap {
  //Static Variables
    public string id;
    public string Subject;
    public string Status;
    //Wrapper Class Controller
    Casewrap() {
    Subject = '';
    }
  }
//Method to bring the list of Case and Serialize Wrapper Object as JSON
  public static String getlstcase() {
      List < Casewrap > lstwrap = new List < Casewrap > ();
      List < Case > lstcase = [SELECT Id ,Subject ,Status FROM Case WHERE IsClosed = false ORDER BY CreatedDate DESC LIMIT 3];
      system.debug('<<<<<<<<<<<lstcase  '+lstcase );
      for (Case c: lstcase) {
        Casewrap cwrap = new Casewrap();
        cwrap.id = c.id;
        cwrap.Status = c.Status;
        if (c.Subject != null) {
            cwrap.Subject = c.Subject;
        }
        lstwrap.add(cwrap);
      }
         return JSON.serialize(lstwrap);
  }
  
  //class and method for event section
    //Subclass : Wrapper Class for camp
  public class Campwrap {
  //Static Variables
    public string id;
    public string Name;
    public Date StartDate;
    //Wrapper Class Controller
    Campwrap() {
    Name = '';
    }
  }
  //Method to bring the list of Campaign and Serialize Wrapper Object as JSON
  
  public static String getlstcamp() {
      List < Campwrap > lstwrap1 = new List < Campwrap > ();
      //List < Campaign > lstCampaign = [SELECT Id ,Name ,Status,StartDate  FROM Campaign where Status = 'Planned' ORDER BY CreatedDate LIMIT 3];
      //select CampaignId  From CampaignMember  where ContactId IN (select id From Contact where OwnerId =:UserInfo.getUserId()
     List < Campaign > lstCampaign = [Select StartDate, Name, Id, 
                                        (Select CampaignId From CampaignMembers where ContactId IN (select id From Contact where OwnerId =:UserInfo.getUserId())) 
                                      From Campaign 
                                      where 
                                              IsActive = true and
                                              Status = 'Planned'
                                       ORDER BY StartDate LIMIT 3]; 
      /*List < CampaignMember > lstCampaign = [Select Id,
                                              Campaign.Name,
                                              Campaign.Status,
                                              Campaign.StartDate,
                                              CampaignId 
                                      From 
                                              CampaignMember
                                      where 
                                              Campaign.IsActive = true and
                                              Campaign.Status = 'Planned' and 
                                              ContactId IN (select id From Contact where OwnerId =:UserInfo.getUserId())
                                      ORDER BY 
                                              Campaign.StartDate LIMIT 3];*/
      system.debug('<<<<<<<<<<<lstCampaign  '+lstCampaign );
      for (Campaign c: lstCampaign) {
        Campwrap cwrap = new Campwrap();
        cwrap.id = c.id;
        cwrap.StartDate = c.StartDate;
        if (c.Name != null) {
            cwrap.Name = c.Name;
        }
        lstwrap1.add(cwrap);
      }
         return JSON.serialize(lstwrap1);
  } 
//get object Name from Custom setting
   /* public String StringUrl {get;set;}
    public List<Objectwrapper> lstwrapper = new List<Objectwrapper>();

    public List<Objectwrapper> getLstwrapper() {
        StringUrl = 'https://ap2.salesforce.com/';
          list<DynamicObject__c> lstDynamicObj = [ SELECT Object_Name__c ,LinkPrefix__c FROM DynamicObject__c ORDER BY Object_Name__c];
          for(DynamicObject__c objcs : lstDynamicObj){
              lstwrapper.add(new Objectwrapper(objcs.Object_Name__c,objcs.LinkPrefix__c));
          }
          return lstwrapper; 
    }
*/
    public class Objectwrapper{
        public String namestr{get; set;}
        public String urlstr {get; set;}
        public Objectwrapper(String name,string str)
        {
            namestr = name;
            urlstr = str;
        }
    }


    
    public String StringUrl {get;set;}
    public List<Objectwrapper> lstwrapper = new List<Objectwrapper>();
    public List<Objectwrapper> getLstwrapper() {
           Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
    map<string,string> mapobjnamePrefix = new map<string,string>();
    System.debug('***********map********' + gd);
    for(Schema.SObjectType stype : gd.values())
    {
        Schema.DescribeSObjectResult r = stype.getDescribe();
        mapobjnamePrefix.put(r.getName(),r.getKeyPrefix());
    }
    StringUrl = 'https://ap2.salesforce.com/';
    list<DynamicObject__c> lstDynamicObj = [ SELECT Object_Name__c ,LinkPrefix__c FROM DynamicObject__c ORDER BY Object_Name__c];
    for(DynamicObject__c objcs : lstDynamicObj){
      if(mapobjnamePrefix.get(objcs.Object_Name__c) != null){
        string lpstr = StringUrl + mapobjnamePrefix.get(objcs.Object_Name__c);
    lstwrapper.add(new Objectwrapper(objcs.Object_Name__c,lpstr));
    }
    }
    return lstwrapper; 
 }
 
 
 
 
 
 
 
 //for pie chart 
    public List<PieWedgeData> getPieData() {
        List<PieWedgeData> data = new List<PieWedgeData>();
        data.add(new PieWedgeData('Investment', 50));
        data.add(new PieWedgeData('Banking', 30));
        data.add(new PieWedgeData('Other', 20));
        return data;
    }

    // Wrapper class
    public class PieWedgeData {

        public String name { get; set; }
        public Integer data { get; set; }

        public PieWedgeData(String name, Integer data) {
            this.name = name;
            this.data = data;
        }
    }
//for chart
 /*public void chartFinancial(){
 public double single = 0;
 public double doublee = 0;
      User u = [Select AccountId From User where id =: UserInfo.getUserId() limit 1][0];
        if(u != null && u.AccountId != null){
        List<Financial_Account__c> lstaFinancial = [Select Product_Type__c, Name, Id From Financial_Account__c where Relationship_Entity__c =: u.AccountId];
        system.debug('<<<<<<<<<<<lstaFinancial   '+lstaFinancial );
            for(Financial_Account__c f : lstaFinancial){
                //Finanlcialwrap cwrap = new Finanlcialwrap();
                    if(f.Product_Type__c.equals('Investment'))
                    single++;
                    else if(f.Product_Type__c.equals('Banking'))
                    doublee++;

            }
            System.debug('single Percentage : ' +(single / lstaFinancial.size() )*100);
                System.debug('doublee Percentage : ' + (doublee / lstaFinancial.size())*100);
      }
 }*/
  //Method for calculate percentage for chart section
 /*public double PercentInv {get;set;} 
 public void chartFinancial(){
    system.debug('<<<<<<<<<<<in method chart   ');
  double single = 0;
  double doublee = 0;
      //User u = [Select ContactId From User where id =: UserInfo.getUserId() limit 1];
      User u = [Select AccountId From User where id =: UserInfo.getUserId() limit 1];
      system.debug('<<<<<<<<<<<u   '+u.AccountId );
        if(u != null && u.AccountId != null){
        system.debug('<<<<<<<<<<<u1   '+u );
        List<Financial_Account__c> lstaFinancial = [Select Product_Type__c, Name, Id From Financial_Account__c where Relationship_Entity__c =: u.AccountId];
        system.debug('<<<<<<<<<<<lstaFinancial   '+lstaFinancial );
            for(Financial_Account__c f : lstaFinancial){
                //Finanlcialwrap cwrap = new Finanlcialwrap();
                    if(f.Product_Type__c.equals('Investment')){
                        single++;
                        PercentInv = (single / lstaFinancial.size() )*100;
                    }
                    else if(f.Product_Type__c.equals('Banking')){
                        doublee++;
                        double PercentBanking = (doublee / lstaFinancial.size())*100;
                    }
 
            }
            System.debug('single Percentage : ' +(single / lstaFinancial.size() )*100);
                System.debug('doublee Percentage : ' + (doublee / lstaFinancial.size())*100);
      }
 }*/
  //Method for calculate percentage for chart section
 public Decimal PercentInv {get;set;}
 public Decimal PercentBanking {get;set;} 
 public Decimal PercentOther {get;set;}
 public Integer TotalCurrentValue {get;set;}
 public Integer TotalInvsCurrValue {get;set;}
 public Integer TotalBankCurrValue {get;set;}
 public void chartFinancial(){
    Decimal InvestmentCount = 0;
    Decimal BankingCount = 0;
    Integer CurrentValueSum = 0;
    Integer InvestSum = 0;
    Integer BankSum = 0;
      //User u = [Select ContactId From User where id =: UserInfo.getUserId() limit 1];
     // User u = [Select AccountId From User where id =: UserInfo.getUserId() limit 1];
     // system.debug('<<<<<<<<<<<u   '+u.AccountId );
       // if(u != null && u.AccountId != null){
      //  system.debug('<<<<<<<<<<<u1   '+u );
        
            for(AggregateResult f : lstaFinancial){
                if(f.get('expr0') != null){
                   CurrentValueSum  = CurrentValueSum + Integer.valueOf(f.get('expr0'));
                   System.debug('Currf.gSum ' + f.get('expr0'));
                   System.debug('CurrentValueSum ' + CurrentValueSum);
                }
                //Finanlcialwrap cwrap = new Finanlcialwrap();
                if(f.get('Type') !=null && f.get('Type') != '' && f.get('expr0') != null){
                    if(f.get('Type').equals('Prospect') && f.get('expr0') != null){
                        InvestmentCount++;
                        InvestSum = InvestSum + Integer.valueOf(f.get('expr0'));
                    }
                    else if(f.get('Type').equals('Other') && f.get('expr0') != null){
                        BankingCount++;
                        BankSum = BankSum + Integer.valueOf(f.get('expr0'));
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size() > 0){
             TotalCurrentValue = CurrentValueSum;
             TotalInvsCurrValue = InvestSum;
             TotalBankCurrValue = BankSum;
             System.debug('AnnualRevenue11111 ' + TotalCurrentValue);
             PercentInv = ((InvestmentCount / lstaFinancial.size() )*100).setScale(2);
             PercentBanking = ((BankingCount / lstaFinancial.size())*100).setScale(2);
             PercentOther = (100 - (PercentInv + PercentBanking)).setScale(2);
            }
 }
 //Method for net worth section
 public Integer netWorthFinancial {get;set;}
 public Integer netWorthAsset {get;set;}
    public void netWorth(){
        Integer hotSum = 0;
        Integer warmSum = 0;
        Integer hotSum1 = 0;
        Integer warmSum1 = 0;
        Integer assetCurrentValueSum = 0;
            for(AggregateResult f : lstaFinancial){
                if(f.get('Rating') !=null && f.get('Rating') != '' && f.get('expr0') != null){
                    if(f.get('Rating').equals('Hot') && f.get('expr0') != null){
                        hotSum = hotSum + Integer.valueOf(f.get('expr0'));
                    }
                    else if(f.get('Rating').equals('Warm') && f.get('expr0') != null){
                        warmSum = warmSum + Integer.valueOf(f.get('expr0'));
                    }
                }
            }

            for(AggregateResult l : lstAssetLiblity){
            if(l.get('Type__c') !=null && l.get('Type__c') != '' && l.get('expr0') != null){
                    if(l.get('Type__c').equals('Hot') && l.get('expr0') != null){
                        hotSum1 = hotSum1 + Integer.valueOf(l.get('expr0'));
                    }
                    else if(l.get('Type__c').equals('Warm') && l.get('expr0') != null){
                        warmSum1 = warmSum1 + Integer.valueOf(l.get('expr0'));
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size() > 0 && lstAssetLiblity != null && lstAssetLiblity.size() > 0){
                netWorthFinancial = TotalCurrentValue - (hotSum + warmSum);
                System.debug('<<<<netWorthFinancial   ' + netWorthFinancial );
                netWorthAsset = (netWorthFinancial + hotSum1 ) - warmSum1;
            }
            /*if(lstAssetLiblity != null && lstAssetLiblity.size() > 0){
                netWorthAsset = (netWorthFinancial + hotSum1 ) - warmSum1;
            }*/
    }
 }