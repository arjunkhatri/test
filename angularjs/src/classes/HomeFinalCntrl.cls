public without sharing class HomeFinalCntrl {
   // public  List<UserFeed> lstFeeds{get;set;}
    public User userRec{get;set;}
    public string photoUrl{get;set;}
    public string ownerphotoUrl{get;set;}
    public String CaseList { get; set; }
    public String strUserCont { get; set; }
    public String strUserAcct { get; set; }
    public String strEvent{get;set;}
    public String strEventSub{get;set;}
    public String loginUser{get;set;}
    public String loginUserName{get;set;}
    public String eventId{get;set;}
    
     public Decimal PercentInv {get;set;}
     public Decimal PercentBanking {get;set;} 
     public Integer TotalCurrentValue {get;set;}
     public Decimal TotalInvsCurrValue {get;set;}
     public Decimal TotalBankCurrValue {get;set;}
     public Integer totalassetCurrentValueSum {get;set;}
     public Integer netWorthFinancial {get;set;}
     public Integer netHouseholdWorth {get;set;}
     public Integer netWorthFinancialAcct {get;set;}
     public Integer netWorth {get;set;}
    public List<AggregateResult> lstaFinancial   = new List<AggregateResult>();
    public List<AggregateResult> lstAssetLiblity = new List<AggregateResult>();

     public map<String,String> mpUserToPhoto {get;set;}
     public map<String,List<FeedComment>> mpIdTofComment {get;set;}
     public map<String,String> mpIdToBody {get;set;}
     public  List<FeedComment> lstFeedComments{get;set;}
     public  List<ChatterData> lstChatterData{get;set;}
     public  List<FeedItem> lstFeeds{get;set;}
     public String selectedPost{get;set;}
     public String strCmntBody{get;set;}

    public String strChartUrl{get;set;} 
    //public List < Casewrap > lstwrap {get;set;}
     public List < Case > lstcase {get;set;}
     public List < Campwrap > lstwrapCamp {get;set;}
//constructor starts 
    public HomeFinalCntrl(){
        system.debug('<<<<<in constructor');
        PercentInv = 0.00;
        PercentBanking = 0.00;
        TotalCurrentValue = 0;
        TotalInvsCurrValue = 0;
        TotalBankCurrValue = 0;
        totalassetCurrentValueSum = 0;
        netWorthFinancial = 0;
        netHouseholdWorth = 0;
        netWorthFinancialAcct = 0;
        netWorth = 0;
    }//constructor ends

    public pagereference init(){
       /* system.debug('>>>>>>>redirect'+checkPlatform());
       PageReference pageRef; */
        /*
        if(checkPlatform() == 'desktop'){
             pageRef = new PageReference('/BNYMWM/apex/Community_Customer_Home');
             return pageRef;
        }*/

      /*  loginUser = userinfo.getUserId();
        loginUserName = userinfo.getName();
        User uRec = [Select ContactId,AccountId From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId;
        if(uRec != null && uRec.AccountId != null)
            strUserAcct = uRec.AccountId;

        lstaFinancial = [Select Product_Type__c,
                                Account_Type__c,
                                Name,
                                Id ,
                                Sum(Current_Value__c) 
                         From 
                               Financial_Account__c
                         where 
                               Relationship_Entity__c =: strUserAcct
                         GROUP BY 
                               Product_Type__c ,Id, Account_Type__c,Name];


       lstAssetLiblity = [Select 
                                Id,Type__c ,Sum(Current_Value__c)
                          From 
                                Assets_and_Liabilities__c
                          where 
                                Account__c =: strUserAcct
                          GROUP BY
                                Id,Type__c ];
        getChatterData();
        getAdvisors();
        getFinancialAccounts();
        getNetWorth();
        getlstcamp();
        getlstcase();*/
        getChatterData();
        return null;
    }
    //Subclass : Wrapper Class
    /*
    public class Casewrap {
        //Static Variables
        public string id {get;set;}
        public string Subject{get;set;}
        public string Status{get;set;}
        //Wrapper Class Controller
        Casewrap(String id, String Sub,String Sts) {
            this.id = 
            this.Subject = '';
        }
    }*/
    
    public void getAdvisors(){
        /*User usr = [Select Contact.OwnerId From User where id = :userinfo.getUserId() limit 1];
        if(usr != null && usr.Contact.OwnerId != null) {
            userRec = [Select Phone, 
                              Name, 
                              FullPhotoUrl, 
                              Email 
                              From User where id = :usr.Contact.OwnerId];
        }
        system.debug('>>>>strUserCont'+strUserCont);
        List<Event> evnt = [Select id,WhoId,
                             Subject,
                             ActivityDate 
                             From Event 
                             where WhoId = :strUserCont
                             order by ActivityDate DESC NULLS LAST limit 1];

    
    system.debug('>>>>evnt'+evnt);                   
        if(evnt != null && evnt.size() > 0 && evnt[0].ActivityDate != null){        
            strEvent = evnt[0].ActivityDate.month()+'/'+ evnt[0].ActivityDate.day();
            strEventSub = evnt[0].Subject;
            eventId = evnt[0].id;
        } */           
    }
    
    //code to get chatter data and display in messages
    public pagereference getChatterData(){  
        mpUserToPhoto = new map<String,String>();
        mpIdTofComment = new map<String,List<FeedComment>>();
        mpIdToBody = new map<String,String>();
        set<String> setUserReplId = new set<String>();
        set<String> setFeedItem = new set<String>();
        String uName = userinfo.getName();
        lstFeeds = new List<FeedItem>();
        String searchquery ='FIND \''+uName+'* OR '+loginUser+'\'IN ALL FIELDS RETURNING FeedItem(id,Body,ParentId,CreatedDate,CreatedById order by CreatedDate DESC limit 3)'; 
        System.debug('searchquery>>>' + searchquery);
        List<List<SObject>>searchList = search.query(searchquery);
        System.debug('searchList>>>' + searchList);
        for(List<SObject> sobjOur: searchList){
            for(SObject sObj:sobjOur){
                System.debug('sobjOur>>>' + sObj);
                FeedItem fItm = (FeedItem)sObj;
                lstFeeds.add(fItm);
                if(fItm.Body.contains('@'+uName))
                    mpIdToBody.put(fItm.id,fItm.Body.replace('@'+uName,''));      
                else
                    mpIdToBody.put(fItm.id,fItm.Body);      
                setUserReplId.add(fItm.CreatedById);
                setFeedItem.add(fItm.Id);
            }
        }       
        
        for(FeedComment fComment:[Select Id, 
                                         FeedItemId, 
                                         CreatedBy.Id, 
                                         CreatedById,
                                         CommentBody 
                                         From FeedComment where FeedItemId in :setFeedItem order by CreatedDate DESC])
        {
            setUserReplId.add(fComment.CreatedById);        
            if(!mpIdTofComment.containsKey(fComment.FeedItemId)){
                mpIdTofComment.put(fComment.FeedItemId,new List<FeedComment>{fComment});
            }
            else if(mpIdTofComment.containsKey(fComment.FeedItemId)){
                if(mpIdTofComment.get(fComment.FeedItemId).size() < 2)
                    mpIdTofComment.get(fComment.FeedItemId).add(fComment);
            }
            if(fComment.CommentBody.contains('@'+uName))
                mpIdToBody.put(fComment.id,fComment.CommentBody.replace('@'+uName,''));
            else
                mpIdToBody.put(fComment.id,fComment.CommentBody);                
        }
        
        for(String strFeed:setFeedItem){
            
            if(!mpIdTofComment.containsKey(strFeed)){
                FeedComment fc = new FeedComment();
                mpIdTofComment.put(strFeed,new List<FeedComment>{fc});
            }
        }
        system.debug('mpIdTofComment>>>'+mpIdTofComment);
        
        for(User usr:[Select Id, SmallPhotoUrl From User where id = :setUserReplId])
        {
            mpUserToPhoto.put(usr.id,usr.SmallPhotoUrl);
        }
        return null;    
    
    }
    public pagereference getPostId(){
         system.debug('selectedPost>>>'+selectedPost);
        return null;
    }
    public pagereference addfeedComment(){
        system.debug('selectedPost>>>'+selectedPost);
        system.debug('strCmntBody>>>'+strCmntBody);
        if(selectedPost != null && selectedPost != '' && strCmntBody != null && strCmntBody != ''){
        FeedComment fc = new FeedComment();
            fc.FeedItemId = selectedPost;
            fc.CommentBody = strCmntBody;
            insert fc;
            getChatterData();
        }
        
        //return new pagereference('/BNYMWM/Home');
        return null;
    }
	public class ChatterData{
	    public string strBody{get;set;}
	    public string CreatedById{get;set;}
	    public FeedComment objFeedComment{get;set;}
	    public ChatterData(string sBody,string crId,FeedComment objFeedComment){
	            this.objFeedComment = objFeedComment;
	            this.strBody = sBody;
	            this.CreatedById = crId;
	    }
	} 

//Method to bring the list of Case and Serialize Wrapper Object as JSON
  public void getlstcase() {
      //lstwrap = new List < Casewrap > ();
      /*lstcase = [SELECT Id ,Subject ,Status,ContactId 
                                FROM Case 
                                WHERE Status != 'Closed'
                                AND ContactId= :strUserCont ORDER 
                                BY CreatedDate DESC LIMIT 3];*/
      system.debug('<<<<<<<<<<<lstcase  '+lstcase );
      /*
      for (Case c: lstcase) {
        Casewrap cwrap = new Casewrap();
        cwrap.id = c.id;
        cwrap.Status = c.Status;
        if (c.Subject != null) {
            cwrap.Subject = c.Subject;
        }
        lstwrap.add(cwrap); 
      }*/
  }
  
  //class and method for event section
    //Subclass : Wrapper Class for camp
    public class Campwrap {
    //Static Variables
    public string id {get; set;} 
    public string Name {get; set;}
    public String StartDate {get; set;}
    public String strStatus {get; set;}
        //Wrapper Class Controller
        /*Campwrap(String id,String Name,String sDate,String sStatus) {
            this.id = id;
            this.Name = Name;
            this.StartDate = sDate;
            this.strStatus = sStatus;
        }*/
  }
  //Method to bring the list of Campaign and Serialize Wrapper Object as JSON
  public void getlstcamp() {
      lstwrapCamp = new List < Campwrap > ();
         /* if(strUserCont != null && strUserCont != ''){
                List <CampaignMember> lstCampaign  = [Select Id,
                                                  ContactId, 
                                                  Campaign.Name, 
                                                  Campaign.Id,
                                                  Campaign.IsActive,
                                                  Campaign.Status, 
                                                  Campaign.StartDate,
                                                  CampaignId 
                                                  From CampaignMember 
                                                  Where ContactId = :strUserCont and
                                                  Campaign.IsActive = true
                                                  ORDER BY 
                                                  Campaign.StartDate LIMIT 3]; 
                system.debug('<<<<<<<<<<<lstCampaign  '+lstCampaign );
                for (CampaignMember c: lstCampaign) { 
                    Date sDate = c.Campaign.StartDate;
                    String strDate = sDate.month()+'/'+sDate.day()+'/'+sDate.year();
                    lstwrapCamp.add(new Campwrap(c.Campaign.id,c.Campaign.Name,strDate,c.Campaign.Status));
                }
          }*/
  }   
//get object Name from Custom setting

    public String StringUrl {get;set;}
    public List<Objectwrapper> lstwrapper = new List<Objectwrapper>();
    map<string,string> mapobjnamePrefix = new map<string,string>();
    public class Objectwrapper{
        public String namestr{get; set;}
        public String urlstr {get; set;}
        public Objectwrapper(String name,string str)
        {
            namestr = name;
            urlstr = str;
        }
    }
 
    public List<Objectwrapper> getLstwrapper() {
        /*Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType stype : gd.values())
        {
            Schema.DescribeSObjectResult r = stype.getDescribe();
            mapobjnamePrefix.put(r.getName(),r.getKeyPrefix());
        }
        list<NavigationMenu__c> lstDynamicObj = [ SELECT Name,Object_Name_API__c FROM NavigationMenu__c ORDER BY Object_Name_API__c];
        for(NavigationMenu__c objcs : lstDynamicObj){
              if(mapobjnamePrefix.get(objcs.Object_Name_API__c) != null){
                string lpstr = '/'+ mapobjnamePrefix.get(objcs.Object_Name_API__c);
                system.debug('>>>>>lpstr'+lpstr);
                if(objcs.Object_Name_API__c == 'Account')
                    lpstr = '/'+strUserCont;
                lstwrapper.add(new Objectwrapper(objcs.Name,lpstr));
              }
              else if (objcs.Object_Name_API__c == 'Chatter')
                lstwrapper.add(new Objectwrapper(objcs.Name,'/'+loginUser));
        }
        return lstwrapper; */
        return null;
    }

  //Method for calculate percentage for chart section

  public void getFinancialAccounts(){
    Decimal InvestmentCount = 0;
    Decimal BankingCount = 0;
    Integer CurrentValueSum = 0;
    Integer InvestSum = 0;
    Integer BankSum = 0;

     /* if(strUserAcct != null && strUserAcct != ''){
            for(AggregateResult agrR : lstaFinancial){
                if(agrR != null ){
                    if(agrR.get('expr0') != null && agrR.get('Product_Type__c') !=null && agrR.get('Product_Type__c') != ''){
                        CurrentValueSum  = CurrentValueSum + Integer.valueOf(agrR.get('expr0'));
                        if(agrR.get('Product_Type__c').equals('Investment') && agrR.get('expr0') != null){
                            InvestmentCount++;
                            InvestSum = InvestSum + Integer.valueOf(agrR.get('expr0'));
                        }
                        else if(agrR.get('Product_Type__c').equals('Banking') && agrR.get('expr0') != null){
                            BankingCount++;
                            BankSum = BankSum + Integer.valueOf(agrR.get('expr0'));
                        }
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size()>0){
                 TotalCurrentValue = CurrentValueSum;
                 TotalInvsCurrValue = InvestSum;
                 TotalBankCurrValue = BankSum;
                 PercentInv = ((TotalInvsCurrValue / TotalCurrentValue )*100).setScale(2);
                 PercentBanking = ((TotalBankCurrValue / TotalCurrentValue)*100).setScale(2);

                 system.debug('<<<<<TotalCurrentValue'+TotalCurrentValue);
                 system.debug('<<<<<TotalInvsCurrValue'+TotalInvsCurrValue);
                 system.debug('<<<<<TotalBankCurrValue'+TotalBankCurrValue);
                 system.debug('<<<<<PercentInv'+PercentInv);
                 system.debug('<<<<<PercentBanking'+PercentBanking);
            }
      }
      String strInv = string.valueof(PercentInv);
      String strBank = string.valueof(PercentBanking);
      strChartUrl = '/BNYMWM/apex/MyFinancialAccount?inv='+strInv+'&bank='+strBank;**/
 }
 
   //Method for net worth section
    public void getNetWorth(){
        Integer loanSum = 0;
        Integer mortageSum = 0;
        Integer assetSum = 0;
        Integer liabilitySum = 0;
        Integer assetAccConSum = 0;
        Integer liabilityAccConSum = 0;
        Integer loanNetSum = 0;
        Integer mortageNetSum = 0;
        Integer assetCurrentValueSum = 0;

        /*if(strUserAcct != null && strUserAcct != '' && strUserCont != null && strUserCont != ''){
                for(AggregateResult agrRFin : lstaFinancial){
                    if(agrRFin != null ){
                        if(agrRFin.get('expr0') != null && agrRFin.get('Account_Type__c') !=null && agrRFin.get('Account_Type__c') != ''){
                            //cValue = cValue + Integer.valueOf(agrRFin.get('expr0'));
                            if(agrRFin.get('Account_Type__c').equals('Loan') && agrRFin.get('expr0') != null){
                                loanSum = loanSum + Integer.valueOf(agrRFin.get('expr0'));
                            }
                            if(agrRFin.get('Account_Type__c').equals('Mortgage') && agrRFin.get('expr0') != null){
                                mortageSum = mortageSum + Integer.valueOf(agrRFin.get('expr0'));
                            }
                        }
                    }
                }
            for(AggregateResult assetAgrR : lstAssetLiblity){
                    if(assetAgrR != null ){
                        if(assetAgrR.get('Type__c') !=null && assetAgrR.get('Type__c') != '' && assetAgrR.get('expr0') != null){
                                if(assetAgrR.get('Type__c').equals('Asset') && assetAgrR.get('expr0') != null){
                                    assetAccConSum = assetAccConSum + Integer.valueOf(assetAgrR.get('expr0'));
                                } 
                                if(assetAgrR.get('Type__c').equals('Liability') && assetAgrR.get('expr0') != null){
                                    liabilityAccConSum = liabilityAccConSum + Integer.valueOf(assetAgrR.get('expr0'));
                                }
                         }
                    }
            }

                if(lstaFinancial != null && lstaFinancial.size()>0 && lstAssetLiblity != null && lstAssetLiblity.size() > 0){
                     netWorthFinancial = TotalCurrentValue - (loanSum + mortageSum);
                     netHouseholdWorth = (netWorthFinancial + assetAccConSum ) - liabilityAccConSum;
                }

          List<AggregateResult> lstFinancialAcct = [Select SUM(Financial_Account__r.Current_Value__c), 
                                                        Financial_Account__r.Account_Type__c, 
                                                        Financial_Account__r.Id, 
                                                        Contact__c 
                                                    From 
                                                        Financial_Account_Connection__c 
                                                    where 
                                                        Contact__c =: strUserCont
                                                    GROUP BY 
                                                          Financial_Account__r.Account_Type__c, 
                                                          Financial_Account__r.Id,Contact__c];
         for(AggregateResult agrR1 : lstFinancialAcct){
            if(agrR1 != null ){
                if(agrR1.get('expr0') != null && agrR1.get('Account_Type__c') !=null && agrR1.get('Account_Type__c') != ''){
                    assetCurrentValueSum  = assetCurrentValueSum + Integer.valueOf(agrR1.get('expr0'));
                    if(agrR1.get('Account_Type__c').equals('Loan') && agrR1.get('expr0') != null){
                        loanNetSum = loanNetSum + Integer.valueOf(agrR1.get('expr0'));
                    }
                    else if(agrR1.get('Account_Type__c').equals('Mortgage') && agrR1.get('expr0') != null){
                        mortageNetSum = mortageNetSum + Integer.valueOf(agrR1.get('expr0'));
                    }
                }
           }
         }
        List<AggregateResult> lstAssetAcctCont = [Select 
                                                        Assets_and_Liabilities__r.Account__c,
                                                        Contact__c, Assets_and_Liabilities__r.Type__c, 
                                                        SUM(Assets_and_Liabilities__r.Current_Value__c),
                                                        Assets_and_Liabilities__r.Id
                                                  From
                                                        Contact_Assets_and_Liabilities__c
                                                  where
                                                        Contact__c =: strUserCont
                                                  GROUP BY
                                                        Assets_and_Liabilities__r.Type__c,
                                                        Assets_and_Liabilities__r.Id,
                                                        Contact__c,
                                                        Assets_and_Liabilities__r.Account__c];
                for(AggregateResult l : lstAssetAcctCont){
                    if(l != null ){
                        if(l.get('Type__c') !=null && l.get('Type__c') != '' && l.get('expr0') != null){
                            if(l.get('Type__c').equals('Asset') && l.get('expr0') != null){
                                assetSum = assetSum + Integer.valueOf(l.get('expr0'));
                            }
                            else if(l.get('Type__c').equals('Liability') && l.get('expr0') != null){
                                liabilitySum = liabilitySum + Integer.valueOf(l.get('expr0'));
                            }
                        }
                    }
                }

                if(lstFinancialAcct != null && lstFinancialAcct.size()>0 && lstAssetAcctCont != null && lstAssetAcctCont.size() > 0){

                     totalassetCurrentValueSum = assetCurrentValueSum ;
                     netWorthFinancialAcct = totalassetCurrentValueSum - (loanNetSum + mortageNetSum);
                     netWorth = (netWorthFinancialAcct + assetSum ) - liabilitySum;
                }

        }*/
    }

 //for checking on which platform page is working
     public String checkPlatform()
    {
        String retVal = '';
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_OPERAMINI_PHONE_UA');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_SKYFIRE_PHONE');


        //& replace with custom setting - using (?i) case insensitive mode.
        String deviceReg = '(?i)(iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce)';//iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce
        String ipadReg = '(?i)(ipad)';
        String iphoneReg = '(?i)(iphone)';
        String mobileReg = '(?i)(mobile)';
        String androidReg = '(?i)(android)';
        String desktopReg = '(?i)(windows|linux|os\\s+[x9]|solaris|bsd)';
        String botReg = '(?i)(spider|crawl|slurp|bot)';
        Matcher ipadM = Pattern.compile( ipadReg ).matcher( userAgent );
        Matcher iphoneM = Pattern.compile( iphoneReg ).matcher( userAgent );
        Matcher mobileM = Pattern.compile( mobileReg ).matcher( userAgent );
        Matcher androidM = Pattern.compile( androidReg ).matcher( userAgent );

        Matcher m = Pattern.compile(deviceReg).matcher(userAgent);
        if( m.find(0) )
        {
            if( ipadM.find(0) || ( !mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = 'Tablet';
                system.debug('This is for tablet');
            }
            else if( iphoneM.find(0) || ( mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = 'Phone';
                system.debug('This is for Phone');
            }
        }
        else
        {
            m = Pattern.compile(desktopReg).matcher(userAgent);
            if( m.find(0) )
                retVal = 'desktop';
             
            m = Pattern.compile(botReg).matcher(userAgent);
            if( m.find(0) )
                retVal = 'desktop';
                system.debug('This is for desktop');
        }

        return retVal;
    }

  /* Method For getting account team member user of logged in user 
  *  and thier related events in my banking section
  */
  
 /* public void getlstAccountTeam() {
    mapIdEvent = new Map<Id,Event>();
    setUserId = new set<String>(); 
    lstWrapAccTeam = new List<AccTeamWrap>();
          if(strUserAcct != null && strUserAcct != ''){
               lstAccTeam  = [Select 
                                     UserId, TeamMemberRole, AccountId,User.Name,User.FullPhotoUrl,User.Phone
                              From 
                                     AccountTeamMember
                              Where
                                     AccountId = :strUserAcct and
                                     (TeamMemberRole = 'Relationship Manager' or TeamMemberRole = 'Private Banker' ) ]; 
                system.debug('<<<<<<<<<<<lstAccTeam  '+lstAccTeam );
                for (AccountTeamMember accTeam: lstAccTeam) {
                       setUserId.add(accTeam.UserId);
                    lstWrapAccTeam.add(new accTeamWrap(accTeam.User.Name,accTeam.TeamMemberRole,accTeam.User.FullPhotoUrl,accTeam.User.Phone));
                }
                system.debug('<<<<<<<<<<<lstWrapAccTeam  '+lstWrapAccTeam );
                system.debug('<<<<<<<<<<setUserId '+setUserId );
          }

        lstEve = [Select id,WhoId,
                         Subject,
                         ActivityDate,
                         StartDateTime,
                         CreatedDate,
                         OwnerId
                  From Event 
                  where OwnerId IN: setUserId
                  and WhoId = :strUserCont
                  order by ActivityDate NULLS LAST];
      system.debug('<<<<<<<<<<<lstEve  '+lstEve );
      system.debug('<<<<<<<<<<setUserId '+setUserId );

        for(Event event : lstEve){
          if(mapIdEvent.get(event.OwnerId) == null ){
               mapIdEvent.put(event.OwnerId , event);
               system.debug('<<<<<<<<<<mapIdEvent '+mapIdEvent );
          }
          else if(event.CreatedDate > mapIdEvent.get(event.OwnerId).CreatedDate){
                mapIdEvent.put(event.OwnerId , event);
                system.debug('<<<<<<<<<<mapIdEvent111111 '+mapIdEvent );
          }
        }
        for (AccountTeamMember accTeams: lstAccTeam){
            if(accTeams != null && accTeams.TeamMemberRole != null && accTeams.TeamMemberRole != '' && mapIdEvent.get(accTeams.UserId) != null){
                if(accTeams.TeamMemberRole == 'Relationship Manager' && mapIdEvent.get(accTeams.UserId).ActivityDate != null
                    && mapIdEvent.get(accTeams.UserId).Subject != null && mapIdEvent.get(accTeams.UserId).Subject != '' ){
                    strEvent = mapIdEvent.get(accTeams.UserId).ActivityDate.month()+'/'+ mapIdEvent.get(accTeams.UserId).ActivityDate.day();
                    strEventSub = mapIdEvent.get(accTeams.UserId).Subject;
                    eventId = mapIdEvent.get(accTeams.UserId).id;
                    eventTime = mapIdEvent.get(accTeams.UserId).StartDateTime.format('hh:mm a');
                    eventOwnerId = mapIdEvent.get(accTeams.UserId).OwnerId;
                }
                if(accTeams.TeamMemberRole == 'Private Banker' && mapIdEvent.get(accTeams.UserId).ActivityDate != null 
                    && mapIdEvent.get(accTeams.UserId).Subject != null && mapIdEvent.get(accTeams.UserId).Subject != '' && mapIdEvent.get(accTeams.UserId) != null){
                    strEventPB = mapIdEvent.get(accTeams.UserId).ActivityDate.month()+'/'+ mapIdEvent.get(accTeams.UserId).ActivityDate.day();
                    strEventSubPB = mapIdEvent.get(accTeams.UserId).Subject;
                    eventIdPB = mapIdEvent.get(accTeams.UserId).id;
                    eventTimePB = mapIdEvent.get(accTeams.UserId).StartDateTime.format('hh:mm a');
                    eventOwnerIdPB = mapIdEvent.get(accTeams.UserId).OwnerId;
                }
            }
        }
        system.debug('<<<<strEventPB '+strEventPB);
        system.debug('<<eventOwnerId '+eventOwnerId);
        system.debug('<<<<eventTime '+eventTime);
        }

    public class accTeamWrap {
    //Static Variables
    public string userName {get; set;} 
    public string teamMemberRole {get; set;}
    public string userUrl {get; set;}
    public string phone {get; set;}
        //Wrapper Class Controller
        AccTeamWrap(String userName,String teamMemberRole,String userUrl,string phone) {
            this.userName = userName;
            this.teamMemberRole = teamMemberRole;
            this.userUrl = userUrl;
            this.phone = phone;
        }
  }*/

}