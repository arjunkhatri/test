trigger trg_case_send_sms on Case (after insert, after update) {
    //Call the Apex class to Send out SMS messages
    TwilioSMSAsync.sendSMSCaseTeamList(Trigger.newMap.keySet());
}