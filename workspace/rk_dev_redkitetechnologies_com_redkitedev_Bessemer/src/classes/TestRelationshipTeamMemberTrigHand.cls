@isTest
public with sharing class TestRelationshipTeamMemberTrigHand {
	 static testMethod void RelationshipTeamMemberTrigHandTest(){
		Profile pf = [Select Id From Profile where Name= 'Bessemer Wealth Advisor'];
		Profile pf1 = [Select Id From Profile where Name= 'Bessemer Client Advisor'];
		User u = TestDataFactory.createTestUser(pf.id);
			insert u; 
			system.assert(u.id != null,'Created user record');
		User u1 = TestDataFactory.createTestUser(pf1.id);
			u1.Username = 'test1@domain.com.dev';
			insert u1;
			system.assert(u1.id != null,'Created user record');	
		GlobalSettings__c gs = new GlobalSettings__c();
			gs.Relationship_Number_Seq__c = 123;
			gs.Integration_User_Name__c = userinfo.getUserName();
			insert gs;			
		Account acc = TestDataFactory.createTestAccount();
		 	insert acc;
		 	system.assert(acc.id != null,'Created relationship record');
		Task tsk = TestDataFactory.createTask(acc.id);
			 tsk.Relationship_Team_List__c = '';
			 insert tsk;
		AccountTeamMember  accTM1 = TestDataFactory.createATM(acc.id,u.id,'Cash Manager');
			insert accTM1;
			system.assert(accTM1.id != null,'Created account team member record');			 	
		Relationship_Team_Member__c accTM = TestDataFactory.createRelTeamMem(acc.id,u.id,'Cash Manager');
			accTM.EDB_Entity_Id__c = 'test';
			insert accTM;
			system.assert(accTM.id != null,'Created account team member record');
		Relationship_Team_Member__c rtm = [Select Role__c,User__c from Relationship_Team_Member__c where id=:accTM.id];
			rtm.Role__c = 'Senior CA';
			rtm.User__c = u1.id;
			update rtm;
		Relationship_Team_Member__c rtmDel = [Select Id from Relationship_Team_Member__c where id=:rtm.id];
			delete rtmDel;			
							 
	 } 
}