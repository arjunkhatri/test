/*
    
     Authors :  David Brandenburg
     Created Date: 2014-06-23
     Last Modified: 2014-06-23
     
     Purpose: Test Class for LeadConversion page. 
*/
@isTest
private class TestLeadConversionCtrl
{
    static testMethod void LeadConversion_Test_NoTask() 
    {
        Lead leadRec = createTestData (true, false , null ) ;
        
        PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', leadRec.Id);
        Test.startTest();
        LeadConversionCtrl myCon = new LeadConversionCtrl ();
        myCon.createTask = false;
        myCon.createNew = true;
        list<selectOption> tmpOptions = myCon.getTimeFrames ();
        myCon.CompanyName = 'ABC TEST CORP';

        myCon.convert() ;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        System.assertEquals(0 , msgs.size() ) ;

        list<Account> acctCheck = [select id from Account where Name = :myCon.CompanyName];

        System.assertEquals(1, acctCheck.size());
        Test.stopTest();
    }
    static testMethod void LeadConversion_Test_WithTask() 
    {
        List<Account> acct = new List<Account> () ;
        Lead ld = createTestData (true, true , acct ) ;

        PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', ld.Id);
        Test.startTest();
        LeadConversionCtrl myCon = new LeadConversionCtrl ();
        myCon.createTask = true;

        myCon.convert() ;
        

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        System.assertEquals('The Task subject is required' , msgs[0].getDetail() ) ;
        myCon.TaskObj.Subject = 'Other';
        myCon.TaskObj.Priority = 'Normal';
        myCon.TaskObj.Status = 'In Progress';
        myCon.TaskObj.isReminderSet = true;
        myCon.TimeComponent = '01:00 PM';
        msgs.clear();
        myCon.convert() ;
        msgs = ApexPages.getMessages();
        System.assertEquals('The Reminder date is required' , msgs[1].getDetail() ) ;
        
        myCon.DateComponent.Birthdate = System.now().addDays(5).date();

       
        myCon.createNew = false;
        myCon.AccountObj.AccountId = acct[0].id;
        myCon.convert() ;
        list<Task> tsk = [select id from Task where Whatid = :acct[0].id ];
        msgs = ApexPages.getMessages();
        System.assertEquals(2 , msgs.size() ) ;
        System.assertEquals(1, tsk.size());
        list<Contact> ct = [select id from Contact where AccountId = :acct[0].id and LastName = :ld.LastName];
        System.assertEquals(1, ct.size());
        Test.stopTest();
    }

    static testMethod void LeadConversion_Test_Cancel() 
    {
        Lead leadRec = createTestData (true, false , null ) ;
        
        PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', leadRec.Id);
        Test.startTest();
        LeadConversionCtrl myCon = new LeadConversionCtrl ();
        PageReference pgRef = myCon.Cancel() ;
        System.assertEquals('/' + leadRec.id, pgRef.getUrl());
        Test.stopTest();
    }

     private static Lead createTestData (Boolean CreateLead , Boolean CreateAccount , List<Account> acctRec ) 
    {
        /********** Create test data *****************/
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        lead retval = new Lead();

        if (CreateLead)
        {
            Lead ld =  (Lead)TestDataCreator_Utils.createSObject('Lead' , true , 1);
            insert ld;
            retval = ld;

        }

        if (CreateAccount)
        {
            Account acct =  (Account)TestDataCreator_Utils.createSObject('Account' , true , 1 );
            insert acct;
            acctRec.add(acct);
        }
        return retVal;
        
        //**************************/
    }
}