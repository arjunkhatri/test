/*
    
     Authors :  David Brandenburg
     Created Date: 2014-09-01
     Last Modified: 2014-10-03
     
     Purpose:  Used to Contact Edit override pages.  Redirects to correct page based on recordtype
*/
public with sharing class ContactRedirectCtrl 
{
	private ApexPages.StandardController controller;
	public String retURL {get; set;}
	public String saveNewURL {get; set;}
	public String rType {get; set;}
	public String cancelURL {get; set;}
	public String ent {get; set;}
	public String confirmationToken {get; set;}
	public String accountID {get; set;}
	public String contactID {get; set;}
	public Contact contactRec {get;set;} 
	private boolean isNewRec ;
	private string isNewPage ;

	public ContactRedirectCtrl  (ApexPages.StandardController controller) {
	
		this.controller = controller;
	
		retURL = ApexPages.currentPage().getParameters().get('retURL');
		rType = ApexPages.currentPage().getParameters().get('RecordType');
		isNewPage = ApexPages.currentPage().getParameters().get('save_new_url');

		//if (isNewPage == null)
		//{
			if (controller.getId() == null)
				isNewPage = '1' ;
		//}
		/*
		cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
		ent = ApexPages.currentPage().getParameters().get('ent');
		confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
		saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
		accountID = ApexPages.currentPage().getParameters().get('def_account_id');
		contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
		*/
		isNewRec = false ;
	}
	public PageReference GotoPage ()
	{
		isNewRec = true ;
		return redirect();
		
	}
	public PageReference redirect() 
	{
		if (isNewPage == '1')
			return null;
		
		/*
		string recTypeName;
		if (rType == null)
			recTypeName = [select RecordType.Name from Contact where id = :controller.getId()].RecordType.Name ;
		else
			recTypeName = [select Name from RecordType where id = :rType].Name ;	

		
		PageReference returnURL;
	
		// Redirect if Record Type corresponds to custom VisualForce page
		map<string, ContactEditLayouts__c > layouts = ContactEditLayouts__c.getAll();
		if (!layouts.containskey(recTypeName) )
		{
			ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'The recordtype is missing from the Custom Setting Contact Edit Layouts.  Please contact your system administrator to fix.');
            ApexPages.addMessage(errorMessage);
            return null;
		}
		else
		{
			returnURL = new PageReference('/apex/' + layouts.get(recTypeName).PageNameAPI__c); 
		}
		*/
		PageReference returnURL = new PageReference('/apex/ContactEdit' ); 
		returnURL.getParameters().put('id', controller.getID());
		returnURL.getParameters().put('retURL', retURL);
		returnURL.getParameters().put('RecordType', rType);
		
		returnURL.getParameters().put('sfdc.override', '1');
		returnURL.getParameters().put('scontrolCaching','1');
		
		returnURL.setRedirect(true);
		return returnURL;
		
		return null;
	}
}