public with sharing class FormatDateTimeCtrl {
    public Date dateTimeValue { get; set; }
    public String getTimeZoneValue() {
        if( dateTimeValue != null ) {  
            String localeFormatDT = dateTimeValue.format();
            return localeFormatDT;
        }
        return null;
    }
}