@isTest
public with sharing class TestScheduleBatch {
	 public static String CRON_EXP = '0 0 * * * ?';
	 static testMethod void ScheduleBatchTest(){ 
	      // Schedule the test job
	      String jobId = System.schedule('SchedulebatchTest',CRON_EXP, new ScheduleBatch());
	      // Get the information from the CronTrigger API object
	      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
	      // Verify the expressions are the same
	      System.assertEquals(CRON_EXP, ct.CronExpression);
	      // Verify the job has not run
	      System.assertEquals(0, ct.TimesTriggered); 	 
	 }
}