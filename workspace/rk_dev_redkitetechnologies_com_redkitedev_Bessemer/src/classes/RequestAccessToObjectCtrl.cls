global without sharing class RequestAccessToObjectCtrl 
{
	@RemoteAction
	global static string SendRequestEmail  (string RequestAccessId , boolean RequestOwnership , string RequestMessage)
	{
		return RequestAccessHelper.SendRequestEmail (RequestAccessId , UserInfo.getUserId() ,RequestOwnership, RequestMessage) ;
		
	}
	@RemoteAction
    global static string getOwner(string objId) 
    {
    	string retval = '';
    	if (objId.substring(0,3) == '001' )
    	{
    		System.Debug(objId);
        	Account acct = [SELECT Owner.Name ,Name FROM Account WHERE id = :objId];
        	retval =  JSON.serialize (acct);
    	}
    	if (objId.substring(0,3) == '003' )
    	{
        	Contact ct = [SELECT Owner.Name, Account.Name, Name FROM Contact WHERE id = :objId];
        	retval =  JSON.serialize (ct);
    	}
    	if (objId.substring(0,3) == '00Q')
		{
			Lead ld = [select Owner.Name, Name from Lead where id = :objId];
			retval =  JSON.serialize (ld);
		}
    	return  retval;
    }
}