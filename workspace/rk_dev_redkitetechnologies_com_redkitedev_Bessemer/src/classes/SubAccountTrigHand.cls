/*
    
     Authors :  Don Koppel
     Created Date: 2014-07-20
     Last Modified: 2014-07-20
*/

public with sharing class SubAccountTrigHand {
    // template methods
    public void onAfterInsert(list<SubAccounts__c> newList) {
    	updateDataSyncLog(newList, 'I');
    }
    public void onAfterUpdate(list<SubAccounts__c> newList, map<id,SubAccounts__c> oldMap) {
    	updateDataSyncLog(newList, 'U');
    }
    public void onAfterDelete(list<SubAccounts__c> oldList){
    	updateDataSyncLog(oldList, 'D');
    }

// business logic

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs SubAccount EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.
    private void updateDataSyncLog (list<SubAccounts__c> recordList, String operation) {
    	
    	//records should only be added to the data sync log if the current user is the Bessemer Integration user
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
    	
	    	list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
	 		for (SubAccounts__c account : recordList){
	 			if(account.EDB_Entity_Id__c <> null){
		 			Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
		 			syncRecord.EDB_Entity_Id__c = account.EDB_Entity_Id__c;
		 			syncRecord.Entity__c = 'ACCNT';
		 			syncRecord.Action__c = Operation;
		 			syncList.Add(syncRecord);
	 			}
	 		}

			if(synclist.size() > 0)
				insert syncList;
    	}
    }       
}