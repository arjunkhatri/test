/*  
    
     Authors :  Don Koppel
     Created Date: 2014-10-08
     Bug Fix: Owner records not being updated.
     Last Modified: 2015-05-06
*/

public without sharing class RelationshipTeamMemberTrigHand {
    public static   final String WEALTHAD   = 'Wealth Advisor';
    // template methods
    public void onBeforeInsert(list<Relationship_Team_Member__c> newList) {

    }
    public void onAfterInsert(map<Id, Relationship_Team_Member__c> newMap) {
        updateDataSyncLog(newMap.values(), 'I');
        syncAccountTeamMember( newMap, null);   
        insertEnitySub(newMap.values());
    }
    public void onBeforeUpdate(list<Relationship_Team_Member__c> newList, map<id,Relationship_Team_Member__c> oldMap) {

    }
    public void onAfterUpdate(map<Id, Relationship_Team_Member__c> newMap, map<id,Relationship_Team_Member__c> oldMap) {
        updateDataSyncLog(newMap.values(), 'U');
        syncAccountTeamMember( newMap,oldMap );
    }
    public void onAfterDelete(map<Id, Relationship_Team_Member__c> oldMap,list<Relationship_Team_Member__c> lstOldRtm){
        updateDataSyncLog(oldMap.values(), 'D');
        deleteAccTeamMember(oldMap);
        updateAccount(lstOldRtm);
    }
    
    public void syncAccountTeamMember(map<Id, Relationship_Team_Member__c> newMap,
                                      map<id,Relationship_Team_Member__c> oldMap){
        
        set<String> userIdSet = new set<String>();
        set<String> oldUserIdSet = new set<String>();
        set<String> accIdSet = new set<String>();
        set<String> oldAccIdSet = new set<String>();
        map<String, Relationship_Team_Member__c> oldRTMMap = new map<String, Relationship_Team_Member__c>();
        
        //Map Contains UserId_AccountId as key and RekationshipTeamMemeber as value -used to check dup 
        map<String,Relationship_Team_Member__c> RTMMap = new  map<String,Relationship_Team_Member__c>();
        
        map<String, AccountTeamMember> accTeamMemberMap = new map<String, AccountTeamMember>();
        
        // Get the Account and Current user
        for( Relationship_Team_Member__c objRTM: newMap.values()){
            if(oldMap == null || 
                (oldMap != null && oldMap.containsKey(objRTM.Id) 
//              &&
//                  (oldMap.get(objRTM.Id).User__c != objRTM.User__c || oldMap.get(objRTM.Id).Role__c != objRTM.Role__c ||
//                   oldMap.get(objRTM.Id).Relationship__c != objRTM.Relationship__c
//                  ) 
                    ) ){
                System.Debug('----------------');
                System.Debug('Processing RTM Account = ' + objRTM.Relationship__c + '; User = ' + objRTM.User__c  );

                userIdSet.add(objRTM.User__c);  
                accIdSet.add(objRTM.Relationship__c);
                RTMMap.put(objRTM.User__c+'_'+objRTM.Relationship__c,objRTM);
                
                //If the User or Account is changed maintain a map of Old RTM
                if(oldMap != null && oldMap.containsKey(objRTM.Id) 
                    ){

                    System.Debug('RTM record is an update'  );

                    oldRTMMap.put(oldMap.get(objRTM.Id).User__c+'_'+oldMap.get(objRTM.Id).Relationship__c,
                                  oldMap.get(objRTM.Id)  );
                    if(oldMap.get(objRTM.Id).User__c != objRTM.User__c)
                        oldUserIdSet.add(oldMap.get(objRTM.Id).User__c);    
                    if(oldMap.get(objRTM.Id).Relationship__c != objRTM.Relationship__c)
                        oldAccIdSet.add(oldMap.get(objRTM.Id).Relationship__c );
                        
                }
                System.Debug('----------------');
            }
        }
        
        //Query the AccountTeamMember with matching UserId and AccountId
        System.Debug('----------------');
        System.Debug('Processing matching ATM records... ' );
        if(!RTMMap.isEmpty())

            for (AccountTeamMember accteamMember : [Select Id,
                                                       UserId,
                                                       AccountId,
                                                       TeamMemberRole
                                                       from AccountTeamMember where AccountId IN: accIdSet
                                                       or UserId IN: userIdSet or
                                                       AccountId IN: oldAccIdSet or
                                                       UserId IN: oldUserIdSet]){
                //Populate a map of account Team member with userId and AccountId as key
                System.Debug('Found ATM records Account = ' + accteamMember.AccountId + '; User = ' + accteamMember.UserId  );
                accTeamMemberMap.put(accteamMember.UserId+'_'+accteamMember.AccountId,accteamMember);
            }
        
        this.upsertAccTeamMember(RTMMap, accTeamMemberMap);
        System.Debug('----------------');
    } 
    
    /*Function called on delete of RelationshipMember to delete corresponding AccountTeamMember
     */
    public void deleteAccTeamMember(map<Id, Relationship_Team_Member__c> oldMap ){
        
        set<String> oldUserIdSet = new set<String>();
        set<String> oldAccIdSet = new set<String>();
        map<String, Relationship_Team_Member__c> oldRTMMap = new map<String, Relationship_Team_Member__c>();
        map<String, AccountTeamMember> accTeamMemberMap = new map<String, AccountTeamMember>();
        
        for( Relationship_Team_Member__c objRTM: oldMap.values()){
            oldUserIdSet.add(objRTM.User__c);   
            oldAccIdSet.add(objRTM.Relationship__c);
            oldRTMMap.put(objRTM.User__c+'_'+objRTM.Relationship__c,objRTM);    
        }
        
        for (AccountTeamMember accteamMember : [Select Id,
                                                   UserId,
                                                   AccountId,
                                                   TeamMemberRole
                                                   from AccountTeamMember where 
                                                   AccountId IN: oldAccIdSet or
                                                   UserId IN: oldUserIdSet]){
            //Populate a map of account Team member with userId and AccountId as key
            accTeamMemberMap.put(accteamMember.UserId+'_'+accteamMember.AccountId,accteamMember);
        }
        
        if(!accTeamMemberMap.isEmpty())
            this.removeAccTeamMember(oldMap.keySet(),oldUserIdSet, oldAccIdSet, oldRTMMap, null, accTeamMemberMap);
    }

    /* 22/04/15 : This method update values of Wealth Advisor, Office and Team 
       when Relationship_Team_Member__c user deleted with role wealth advisor
    */
    public void updateAccount(list<Relationship_Team_Member__c> lstOldRtm){
        Map<String,String> mpRoleToWadv = new Map<String,String>();
        set<String> oldAccIdSet = new set<String>();
        for(Relationship_Team_Member__c objrtm : lstOldRtm){
            oldAccIdSet.add(objrtm.Relationship__c);
        }
        for(Bessemer_Role_To_Team__c brt:Bessemer_Role_To_Team__c.getall().values()) {
            if(!String.isBlank(brt.Role_Name__c) && brt.Wealth_Advisor_Team__c != null)
                mpRoleToWadv.put(brt.Role_Name__c,brt.Wealth_Advisor_Team__c);  
        }

        List<Relationship_Team_Member__c> lstatm = [
            SELECT Id, User__c, Role__c, User__r.Name, User__r.Office__c, User__r.UserRole.Name, Relationship__c
            FROM   Relationship_Team_Member__c 
            WHERE  Role__c = :WEALTHAD and Id NOT IN: lstOldRtm and Relationship__c IN: oldAccIdSet
        ];

        if(!lstatm.isEmpty()) {
            Account accUpdate = new Account(Id = lstOldRtm[0].Relationship__c);
            if(lstatm[0].User__c != Null)
                accUpdate.OwnerId = lstatm[0].User__c;
            if(!String.isBlank(lstatm[0].User__r.Name))
                accUpdate.Wealth_Advisor__c = lstatm[0].User__r.Name;
            if(!String.isBlank(lstatm[0].User__r.Office__c))
                accUpdate.Bessemer_Office__c = lstatm[0].User__r.Office__c;
            if(!String.isBlank(lstatm[0].User__r.UserRole.Name))
                accUpdate.Bessemer_Team__c = mpRoleToWadv.get(lstatm[0].User__r.UserRole.Name);

            try{
                update accUpdate;
            } catch(DMLException ex) { 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        }
    }

    /* Function used to delete AccountTeamMember if all its corresponding RelationshipTeamMember 
     * are deleted, else update the role of AccountTeamMeber with the other RelationshipTeamMember role  
     *
     */
    public void removeAccTeamMember(set<Id> RtmId,
                                    set<String> oldUserIdSet,
                                    set<String> oldAccIdSet,
                                    map<String, Relationship_Team_Member__c> oldRTMMap,
                                    map<String, Relationship_Team_Member__c> RTMMap,
                                    map<String, AccountTeamMember> accTeamMemberMap){
        
        list <AccountTeamMember> deleteAccTeamMemberList = new list<AccountTeamMember>();
        list<AccountTeamMember> updateAccTeamMemberList = new list<AccountTeamMember>();
        RTMMap = new map<String, Relationship_Team_Member__c>();
        
        //Query the RelationshipTeamMember from Database where UserId and AccountId matches
        for (Relationship_Team_Member__c objRTM : [Select User__c,
                                                          Role__c,
                                                          Relationship__c,
                                                          Name, Id
                                                          From Relationship_Team_Member__c 
                                                          where (User__c IN: oldUserIdSet or
                                                                Relationship__c IN: oldAccIdSet)
                                                           and Id NOT IN: RtmId order by CreatedDate desc]){
            RTMMap.put(objRTM.User__c+'_'+objRTM.Relationship__c,objRTM);                                                       
        }
        for(String key :oldRTMMap.keySet()){
            //Check if no corresponding relationshpTeamMember exist, then delete the AccountTeamMember
            if( (RTMMap == null || 
                    (RTMMap != null && !RTMMap.containsKey(key))
                 ) && accTeamMemberMap.containsKey(key)
               ){
                deleteAccTeamMemberlist.add(accTeamMemberMap.get(key));     
            }
            //If there are any Relationship team member that are not deleted, then update the AccountTeamMber with role of
            // existing RelationshipTeammember
            else if(accTeamMemberMap.containsKey(key)){
                
                //Get the AccountTeam Member from Database and update it
                AccountTeamMember accTeamMember =  accTeamMemberMap.get(key);
                // update the old AccountTeamMember with the previous role 
                accTeamMember.TeamMemberRole = RTMMap.get(key).Role__c ; 
                updateAccTeamMemberList.add(accTeamMember); 
            }
        }   
        
        if(deleteAccTeamMemberlist.size() > 0)
            delete deleteAccTeamMemberlist; 
        if(updateAccTeamMemberList.size() > 0)
            update updateAccTeamMemberList;
    }
    
    /* Function used to check if corresponding AccountTeamMember exists for relationship Team Member,
     * if yes then update the old record else create a new record
     */ 
    public void upsertAccTeamMember(map<String,Relationship_Team_Member__c> RTMMap,
                                    map<String, AccountTeamMember> accTeamMemberMap){

        System.Debug('----------------');
        System.Debug('Upserting ATM and Account share records... ' );
        list<Id> lstAcct = new list<Id>();
        for (Relationship_Team_Member__c  rtm : RTMMap.values()){
            lstAcct.add(rtm.Relationship__c);
        }

        list<AccountShare> asInsert = new List<AccountShare>();
        list<AccountShare> asUpdate = new List<AccountShare>();
        list<AccountShare> asExisting = [SELECT AccountId, UserOrGroupId, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel, RowCause
                                        FROM AccountShare
                                        WHERE AccountId IN :lstAcct];               
        
        list<AccountTeamMember> accTeamMemList = new list<AccountTeamMember>();
        
        for(String key : RTMMap.keySet()){

            //update AccountShare records for affected users
            boolean bFound = false;
            boolean bOwner = false;
            for(AccountShare ash : asExisting){
                if(ash.AccountId == RTMMap.get(key).Relationship__c && ash.UserOrGroupId == RTMMap.get(key).User__c){
//                    if(ash.RowCause == 'Owner'){
//                        bOwner = true;
//                        system.debug('Found existing AccountShare owner record: ' + RTMMap.get(key).Relationship__c + ' ' + RTMMap.get(key).User__c);
//                    }
//                    else{
                        ash.AccountAccessLevel = 'Edit';
                        asUpdate.add(ash);
                        bFound = true;
                        system.debug('Found existing AccountShare record - set AccessLevel = Edit: ' + RTMMap.get(key).Relationship__c + ' ' + RTMMap.get(key).User__c);
//                    }
                }
            }

            system.debug('bFound = ' + bFound + '; bOwner = ' + bOwner);
            if (!bFound && !bOwner){
                system.debug('AccountShare existing: ' + RTMMap.get(key).Relationship__c + ' ' + RTMMap.get(key).User__c);
                asInsert.add(new AccountShare(AccountId = RTMMap.get(key).Relationship__c, UserOrGroupId = RTMMap.get(key).User__c, 
                                AccountAccessLevel = 'Edit', OpportunityAccessLevel = 'None', CaseAccessLevel = 'None'));
            }


            //Check if corresponding AccountTeamMember exists
            if(accTeamMemberMap.size() > 0 && accTeamMemberMap.containsKey(key)){
                
                //Get the AccountTeam Member from Database and update it
                AccountTeamMember accTeamMember =  accTeamMemberMap.get(key);
                accTeamMember.TeamMemberRole = RTMMap.get(key).Role__c ; 
                accTeamMemList.add(accTeamMember);  

            }else{
                //If no corresponding Account Team Member exists, create a new record
                accTeamMemList.add(new AccountTeamMember(TeamMemberRole = RTMMap.get(key).Role__c,
                                                         UserId = RTMMap.get(key).User__c,
                                                         AccountId = RTMMap.get(key).Relationship__c ));        
            }
        }

        if(accTeamMemList.size() > 0)
            upsert accTeamMemList;  
        
        if (asInsert.size() > 0){
            system.debug('AccountShare insert list: ' + asInsert);
            insert asInsert;
        }
        
        if (asUpdate.size() > 0){
            system.debug('AccountShare update list: ' + asUpdate);
            update asUpdate;
        }
    }
    
    //This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
    //completed successfully.  The Informatica job that syncs Relationship Team data EDB-->SFDC will use this data as a source to update
    //status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Relationship_Team_Member__c> recordList, String operation) {
        
        //records should only be added to the data sync log if the current user is the Bessemer Integration user
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
        
            list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
            for (Relationship_Team_Member__c rtm : recordList){
                Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
                syncRecord.EDB_Entity_Id__c = rtm.EDB_Entity_Id__c;
                syncRecord.Entity__c = 'EMP_RELTNSHP_OPTN';
                syncRecord.Action__c = operation;
                syncList.Add(syncRecord);
            }

            insert syncList;
        }
    }
    //Create a new EntitySubscription for the user added to the Relationship, following the Relationship record
    public void insertEnitySub (list<Relationship_Team_Member__c> recordList) {
        set<ID> setSubscriber = new  set<ID>();
        set<String> setSub = new  set<String>();
        for(Relationship_Team_Member__c rtMem:recordList ){
            setSubscriber.add(rtMem.User__c);
        }
        
        Map<Id,integer> mpUserToFollowers = new Map<Id,integer>();
        integer maxRecFollowed = 0;
        GlobalSettings__c gs = GlobalSettings__c.getInstance(); 
        if(gs != null && gs.Max_of_Records_followed__c != null) 
            maxRecFollowed = integer.valueOf(gs.Max_of_Records_followed__c);
        //get already followed records size
        for(User uRecords:[Select id, (Select Id, SubscriberId From FeedSubscriptions) 
                            From User where id IN :setSubscriber])
        {
            integer i = 0;
            for(EntitySubscription ent:uRecords.FeedSubscriptions){
                i++;
            }               
            mpUserToFollowers.put(uRecords.id,i);   
        }
                
        if(setSubscriber != null && setSubscriber.size() > 0){
             for(EntitySubscription eSub:[Select SubscriberId,
                                                          ParentId,
                                                          Id From EntitySubscription 
                                                          where SubscriberId =:setSubscriber])
             {
                setSub.add(eSub.SubscriberId+'-'+eSub.ParentId);
             }                                             
        }
            
        list<EntitySubscription> entitySubList = new list<EntitySubscription>();
        for (Relationship_Team_Member__c rtm : recordList){
            if(mpUserToFollowers.containsKey(rtm.User__c) && 
                            mpUserToFollowers.get(rtm.User__c) <= maxRecFollowed){
                if(!setSub.contains(rtm.User__c+'-'+rtm.Relationship__c)){
                    EntitySubscription entitySubRecord = new EntitySubscription();
                    if(rtm.User__c != null && rtm.Relationship__c != null ) {
                        entitySubRecord.ParentId = rtm.Relationship__c;
                        entitySubRecord.SubscriberId = rtm.User__c;
                        entitySubList.Add(entitySubRecord);
                    }
                }
            }
        }
        
        if(entitySubList != null && entitySubList.size() > 0)
            Database.SaveResult[] srList = Database.insert(entitySubList, false);
    }
}