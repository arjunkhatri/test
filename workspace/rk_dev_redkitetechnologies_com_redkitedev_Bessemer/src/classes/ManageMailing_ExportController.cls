public with sharing class ManageMailing_ExportController {
	public static final String PARAM_MAILINGS = 'mailings';
	public static final String PARAM_TYPE = 'type';
	public static final String PARAM_DEBUG = 'debug';


	public List<MailingsObject > mailingList{get;set;}

	//private List<Mailing__c> largeMailingList{get;set;}

	public Mailing__c selectedValueObj {get;set;}
	public Contact selectedOwnerObj {get;set;}
	public String fileName {get;set;}
	public Boolean isMarketingUser {get;set;}
	public String cityFilter {get;set;}
	public String stateFilter {get;set;}
	public String filterText {get;set;}
	
	public ManageMailing_ExportController(ApexPages.StandardController controller) 
	{

		mailingList = new List<MailingsObject>();
		fileName = 'MailingExport';
		selectedValueObj = new Mailing__c();
		selectedOwnerObj = new Contact();

		map<String, String> params = ApexPages.currentPage().getParameters();

		// TODO : Add logic to determine if use is a marketing user
		isMarketingUser = false;

		if (params.containsKey(PARAM_MAILINGS)) selectedValueObj.Mailings__c = params.get(PARAM_MAILINGS);
		if (params.containsKey(PARAM_TYPE)) selectedValueObj.Type__c = params.get(PARAM_TYPE);
		if (String.isEmpty(selectedValueObj.Type__c)==false) getMailingRecs();

		if (params.get(PARAM_DEBUG)=='1'){
			isMarketingUser = true;
		}
	}
	
	public PageReference getMailingRecs(){
		
		System.debug('Mailings:' + selectedValueObj.Mailings__c+' Type:'+selectedValueObj.Type__c);
		// this should be a field set
		String soql = 'select Id, Name, Mailings__c, Type__c, Mailing_Name__c, Address__c, Address__r.Street_Address__c, '+
						'Address__r.City__c, Address__r.State__c, Address__r.Postal_Code__c, Address__r.Country__c, Lead__r.FirstName, Lead__r.LastName,'+
						'Contact__r.FirstName, Contact__r.LastName, Salutation__c, Contact__r.Owner.Name, Contact__r.Occupation_Type__c ,Lead__r.Owner.Name ,Lead__r.Occupation_Type__c , Contact__c, Lead__c '+
						'from Mailing__c ';

		list<String> whereStrings = new list<String>();
		String mailingsString = selectedValueObj.Mailings__c;
		String typeString = selectedValueObj.Type__c;
		if (String.isEmpty(selectedValueObj.Mailings__c)==false) whereStrings.add('Mailings__c = :mailingsString'); 
		if (String.isEmpty(selectedValueObj.Type__c)==false) whereStrings.add('Type__c = :typeString'); 
		//if (String.isEmpty(cityFilter)==false) whereStrings.add('Address__r.City__c = :cityFilter');
		//if (String.isEmpty(stateFilter)==false) whereStrings.add('Address__r.State__c = :stateFilter');

		String whereString = '';
		if(whereStrings.size()>0) whereString = ' where '+ String.join(whereStrings, ' and ');

		String limitString = ' Order By Contact__r.LastName limit 500';

		soql += whereString + limitString;
		System.Debug('SOQL:'+soql);
		
		

		mailingList = CheckRecordVisibility ( Database.query(soql) );

		filterText = '';
		cityFilter = '';
		stateFilter = '';
		
		return null;
	}

	public PageReference getFilteredMailingRecs(){

		Set<Id> mailingids = new Set<Id>();

		if(mailingList.size()!=0)
		{
			
			
			for(MailingsObject m: mailingList)
				mailingids.add(m.Id);

		  String soql =  'select Id, Name, Mailings__c, Type__c, Mailing_Name__c, Address__c, Address__r.Street_Address__c, '+
							'Address__r.City__c, Address__r.State__c, Address__r.Postal_Code__c, Address__r.Country__c, '+
							'Contact__r.FirstName, Contact__r.LastName, Salutation__c, Contact__r.Owner.Name, Contact__r.Occupation_Type__c , Lead__r.FirstName, Lead__r.LastName, Lead__r.Owner.Name , Lead__r.Occupation_Type__c , Contact__c, Lead__c ' +
							'from Mailing__c ';	

		list<String> whereStrings = new list<String>();
		 whereStrings.add('Id IN: mailingids ');
		 if (String.isEmpty(filterText)==false) whereStrings.add('(Type__c like  \'%'+filterText+'%\' OR Address__r.City__c like \'%'+filterText+'%\' '+
		   'OR Address__r.State__c like  \'%'+filterText+'%\' or Address__r.Country__c like \'%'+filterText+'%\' or Contact__r.Owner.Name like \'%'+filterText+'%\' or  Contact__r.FirstName like \'%'+filterText+'%\' '+
		   'OR Address__r.Street_Address__c like \'%'+filterText+'%\' or Contact__r.LastName like \'%'+filterText+'%\' or Contact__r.Occupation_Type__c like \'%'+filterText+'%\' '+
		   'OR Mailing_Name__c like \'%'+filterText+'%\')'); 


		 	/*or Address__r.City__c like \'%'+filterText+'%\' or Address__r.State__c like \'%'+filterText+'%\'
							or Address__r.Country__c like \'%'+filterText+'%\' or Contact__r.Owner.Name like \'%'+filterText+'%\' or  Contact__r.FirstName like \'%'+filterText+'%\' 
							or Address__r.Street_Address__c like \'%'+filterText+'%\' or Contact__r.LastName like \'%'+filterText+'%\' or Contact__r.Occupation_Type__c like \'%'+filterText+'%\')');	*/					
		 if(String.isEmpty(cityFilter)==false) whereStrings.add('Address__r.City__c = :cityFilter');
		 if(String.isEmpty(stateFilter)==false) whereStrings.add('Address__r.State__c = :stateFilter');

		 String whereString = '';
		if(whereStrings.size()>0) whereString = ' where '+ String.join(whereStrings, ' and ');

		String limitString = ' Order By Contact__r.LastName limit 500';

		/*mailingList = [select Id, Name, Mailings__c, Type__c, Address__c, Address__r.Street_Address__c, 
							Address__r.City__c, Address__r.State__c, Address__r.Postal_Code__c, Address__r.Country__c, 
							Contact__r.FirstName, Contact__r.LastName, Salutation__c, Contact__r.Owner.Name, Contact__r.Occupation_Type__c 
							from Mailing__c where Id IN: mailingids and 
							(Type__c like :('%'+filterText+'%') or Address__r.City__c like :('%'+filterText+'%') or Address__r.State__c like :('%'+filterText+'%')
							or Address__r.Country__c like :('%'+filterText+'%') or Contact__r.Owner.Name like :('%'+filterText+'%') or  Contact__r.FirstName like :('%'+filterText+'%') 
							or Address__r.Street_Address__c like :('%'+filterText+'%') or Contact__r.LastName like :('%'+filterText+'%') or Contact__r.Occupation_Type__c like :('%'+filterText+'%'))
							];*/

		soql += whereString + limitString;
		System.Debug('SOQL:'+soql);
		//mailingList = Database.query(soql);					
		mailingList = CheckRecordVisibility ( Database.query(soql) );

		}

		return null;
	}

	/*public PageReference getFilteredMailingRecs(){

		Set<Id> mailingids = new Set<Id>();

		if(mailingList.size()!=0 && String.isEmpty(filterText)==false)
		{
			
			
			for(mailing__c m: mailingList)
				mailingids.add(m.Id);

			mailingList = [select Id, Name, Mailings__c, Type__c, Address__c, Address__r.Street_Address__c, 
							Address__r.City__c, Address__r.State__c, Address__r.Postal_Code__c, Address__r.Country__c, 
							Contact__r.FirstName, Contact__r.LastName, Salutation__c, Contact__r.Owner.Name, Contact__r.Occupation_Type__c 
							from Mailing__c where Id IN: mailingids and 
							(Type__c like :('%'+filterText+'%') or Address__r.City__c like :('%'+filterText+'%') or Address__r.State__c like :('%'+filterText+'%')
							or Address__r.Country__c like :('%'+filterText+'%') or Contact__r.Owner.Name like :('%'+filterText+'%') or  Contact__r.FirstName like :('%'+filterText+'%') 
							or Address__r.Street_Address__c like :('%'+filterText+'%') or Contact__r.LastName like :('%'+filterText+'%') or Contact__r.Occupation_Type__c like :('%'+filterText+'%'))
							];
		}

		return null;
	}*/
	
	public PageReference GenerateCSV()
	{
		System.debug('In generated  module');
		PageReference np = new PageReference('/apex/ManageMailing_ExportCSV');
		np.setRedirect(false);
		createTask();
		return np;
	}

	public void createTask()
	{
		List<Task> tobeCreated = new List<Task>();

		for(MailingsObject  mail: mailingList){
			
			Task tsk = new Task();
			tsk.OwnerId = System.UserInfo.getUserId();
			tsk.Subject = 'Exported by Mailing Export Utility';
			tsk.Status = 'Completed';
			tsk.ActivityDate = Date.today();
			if (mail.isLead)
				tsk.WhoId = mail.MailingsLeadId;
			else
				tsk.WhoId = mail.MailingsContactId;
			tobeCreated.add(tsk);
		}
		try{
			insert tobeCreated;
		}
		Catch(Exception e)
		{
			System.debug('Error inserted task: ' + e.getMessage());
		}
	}

	private List<MailingsObject> CheckRecordVisibility ( List<Mailing__c> MailList)
	{
		List<MailingsObject > returnedMailList = new List<MailingsObject > ();

		set<string> contactSet = new set<string>() ;
		set<string> leadSet = new set<string>() ;

		for (Mailing__c mailing : MailList)
		{
			if (mailing.Contact__c != null)
				contactSet.add(mailing.Contact__c) ;
			if (mailing.lead__c != null && mailing.Contact__c == null)
				leadSet.add(mailing.lead__c) ;
		}
		
		map<Id, Contact> contactMap = new map<id, Contact> ([select id , UserRecordAccess.HasEditAccess from Contact where id in :contactSet]) ;
		map<Id, Lead> leadMap = new map<id, Lead> ([select id, UserRecordAccess.HasEditAccess from Lead where id in :leadSet]) ;
		for (Mailing__c mailing : MailList)
		{
			if (contactMap.containsKey(mailing.Contact__c) || leadMap.containsKey(mailing.Lead__c) )
				returnedMailList.add(new MailingsObject  (mailing) ) ;
		}

		return returnedMailList ;
	}

	/*============================================================
		Sub Class to hold the Mailing Object
		============================================================*/

	public class MailingsObject 
	{
		public string Id {get;set;}
		public boolean isLead {get;set;}
		public string Mailings {get;set;}
		public string MailingsType {get;set;}
		public string MailingsName {get;set;}
		public string MailingsFirstName {get;set;}
		public string MailingsLastName {get;set;}
		public string MailingsSalutation {get;set;}
		public string MailingsStreetAddress {get;set;}
		public string MailingsCity {get;set;}
		public string MailingsState {get;set;}
		public string MailingsPostalCode {get;set;}
		public string MailingsCountry {get;set;}
		public string MailingsOccupationType {get;set;}
		public string MailingsOwnerName {get;set;}
		public string MailingsLeadId {get;set;}
		public string MailingsContactId {get;set;}

		public MailingsObject (Mailing__c m)
		{
			Id = m.Id;
			Mailings = m.Mailings__c;
			MailingsType = m.Type__c;
			MailingsName = m.Mailing_Name__c;
			if (m.Lead__c != null && m.Contact__c == null )
			{
				MailingsFirstName = m.Lead__r.FirstName;
				MailingsLastName = m.Lead__r.LastName;
				MailingsOccupationType = m.Lead__r.Occupation_Type__c;
				MailingsOwnerName = m.Lead__r.Owner.NAme;
				isLead = true;
				MailingsLeadId = m.Lead__c ;
			}
			else
			{
				MailingsFirstName = m.Contact__r.FirstName;
				MailingsLastName = m.Contact__r.LastName;
				MailingsOccupationType = m.Contact__r.Occupation_Type__c;
				MailingsOwnerName = m.Contact__r.Owner.NAme;
				MailingsContactId = m.Contact__c;
				isLead = false;

			}
			MailingsSalutation = m.Salutation__c;
			MailingsStreetAddress = m.Address__r.Street_Address__c;
			MailingsCity = m.Address__r.City__c;
			MailingsState = m.Address__r.State__c;
			MailingsPostalCode = m.Address__r.Postal_Code__c;
			MailingsCountry = m.Address__r.Country__c;


		}
	}

}