public with sharing class RelationshipTrigHandler {
    public static final string PROSPECTIVE = 'Prospective Relationship';
    public static final string CLIENT = 'Client Relationship';
    public static   final String SENIORCA   = 'Senior Client Advisor';
    public static   final String MESSAGE1   = 'is now a client';
    public static   final String MESSAGE2   = 'is now 80% vested';
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }   
    // template methods
    /*
    public void onBeforeInsert(list<Account> newList){    
    }
    */
    public void onAfterInsert(list<Account> newList){
        assignRelationshipNumber (newList);
        updateDataSyncLog(newList, 'I');
        handleSubscriptionAndPost(newList,null,'I');
    }
    /*
    public void onBeforeUpdate(list<Account> newList, map<id,Account> oldMap){
    }
    */
    public void onAfterUpdate(list<Account> newList, map<id,Account> oldMap){
        //updateAccountsContactOwner(newList,oldMap);   //Requirement to sync contact owner with accout has been dropped  
        updateDataSyncLog(newList, 'U');
        handleSubscriptionAndPost(newList,oldMap,'U');
    }
    
    public void onBeforeUpdate (list<Account> newList, map<id,Account> oldMap){
        restoreOldRecordTypeIds(newList, oldMap);
        updateFieldsOnRecTypeChange(newList,oldMap);
        updateLastActivityDates(newList);
    }
    
    public void onBeforeDelete(list<Account> oldList)
    {
        validateDeletions_2(oldList);
        GetOrphanedConnections(oldList);  
    }
    
    public void onAfterDelete(list<Account> oldList){
        updateDataSyncLog(oldList, 'D');
        RemoveOrphanedConnections();
    }
    /*
    public void onAfterUndelete(list<Account> newList){  
    }
    */

    private void handleSubscriptionAndPost(list<Account> newList,map<id,Account> oldMap,String operation){
        List<EntitySubscription> listEntsub = new List<EntitySubscription>();
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        Map<Id,integer> mpUserToFollowers = new Map<Id,integer>();
        integer maxRecFollowed = 0;
        set<Id> setOwners = new set<Id>();
        for(Account accRec:newList){
            setOwners.add(accRec.OwnerId);
        }
        
        GlobalSettings__c gs = GlobalSettings__c.getInstance(); 
        if(gs != null && gs.Max_of_Records_followed__c != null) 
            maxRecFollowed = integer.valueOf(gs.Max_of_Records_followed__c);
        //get already followed records size
        for(User uRecords:[Select id, (Select Id, SubscriberId From FeedSubscriptions) 
                            From User where id IN :setOwners])
        {
            integer i = 0;
            for(EntitySubscription ent:uRecords.FeedSubscriptions){
                        i++;
            }
            mpUserToFollowers.put(uRecords.id,i);                       
        }
        //if Relationship rec inserted create new EntitySubscription record
        if(operation == 'I'){
            for(Account acc:newList){
                //check if user already exceed max limit for record following
                if(mpUserToFollowers.containsKey(acc.OwnerId) && 
                            mpUserToFollowers.get(acc.OwnerId) <= maxRecFollowed)
                {
                    EntitySubscription eSub = new EntitySubscription();
                    eSub.ParentId = acc.id;
                    eSub.SubscriberId = acc.OwnerId;
                    listEntsub.add(eSub);
                }
            }
            if(listEntsub != null && listEntsub.size() > 0)
                Database.SaveResult[] srList = Database.insert(listEntsub, false);
        }
        else if(operation == 'U'){
            List<FeedItem> listFeed = new List<FeedItem>();
            for(Account acc:newList){
                //If the Relationship Owner has changed, create a new EntitySubscription for the new owner 
                //following the Relationship record
                //check if user already exceed max limit for record following
                if(mpUserToFollowers.containsKey(acc.OwnerId) && 
                            mpUserToFollowers.get(acc.OwnerId) <= maxRecFollowed)
                {               
                    if(oldMap.containsKey(acc.id) && acc.OwnerId != oldMap.get(acc.id).OwnerId){
                        EntitySubscription eSub = new EntitySubscription();
                        eSub.ParentId = acc.id;
                        eSub.SubscriberId = acc.OwnerId;
                        listEntsub.add(eSub);
                    }   
                }   
                //If the Relationship Type has been changed to Client Relationship, create a FeedItem record
                if(oldMap.containsKey(acc.id) && rtMapById.get(oldMap.get(acc.id).RecordTypeId).getName() == PROSPECTIVE 
                    && rtMapById.get(acc.RecordTypeId).getName() == CLIENT) {
                    FeedItem  fItm = new FeedItem ();
                        fItm.ParentId = acc.id;
                        fItm.Body = acc.Name +' '+MESSAGE1;
                        listFeed.add(fItm);
                }
                //If the Percent_of_Cash__c value is modified and is now >= 80%
                if(oldMap.containsKey(acc.id) && acc.Percent_of_Cash__c >= 80 &&  oldMap.get(acc.Id).Percent_of_Cash__c < 80){
                    FeedItem  fItm = new FeedItem ();
                        fItm.ParentId = acc.id;
                        fItm.Body = acc.Name +' '+MESSAGE2;
                        listFeed.add(fItm);             
                }
            }
            if(listEntsub != null && listEntsub.size() > 0 && !test.isRunningTest())
                Database.SaveResult[] srList = Database.insert(listEntsub, false);
            if(listFeed != null && listFeed.size() > 0  )   
                insert listFeed;
        }
    }
    
    // This method set values of Owner, Office and Team when Record Type change 
    //from 'Prospective Relationship' to 'Client Relationship'
    private void updateFieldsOnRecTypeChange(list<Account> newList, map<id,Account> oldMap){
        set<Id> setIds = new set<Id>();
        for(Account acc: newList){
            setIds.add(acc.id);
        }
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        Map<String,String> mpRoleToCadv = new Map<String,String>();
        Map<String,String> mpAccToUser = new Map<String,String>();
        Map<String,String> mpAccTourole = new Map<String,String>();
        Map<String,String> mpAccTouoffice = new Map<String,String>();
        for(Bessemer_Role_To_Team__c brt:Bessemer_Role_To_Team__c.getall().values())
        {
            if(brt.Role_Name__c != null && brt.Role_Name__c !='' && brt.Client_Advisor_Team__c != null)
                mpRoleToCadv.put(brt.Role_Name__c,brt.Client_Advisor_Team__c);
        }
        
        for(Relationship_Team_Member__c rTM : [Select User__r.Id, User__c, Role__c, User__r.UserRole.Name, User__r.Office__c,
                                                                Relationship__c, Id From Relationship_Team_Member__c
                                                                Where Relationship__c IN : setIds and Role__c =:SENIORCA])
        {
            if(rTM.User__r.Id != null)
                mpAccToUser.put(rTM.Relationship__c, rTM.User__r.Id);
            if(rTM.User__r.UserRole.Name != null && rTM.User__r.UserRole.Name != '')
                mpAccTourole.put(rTM.Relationship__c,rTM.User__r.UserRole.Name);
            if(rTM.User__r.Office__c != null && rTM.User__r.Office__c != '')    
                mpAccTouoffice.put(rTM.Relationship__c,rTM.User__r.Office__c);
        }       
        for(Account accRec:newList){
            if( oldMap.containsKey(accRec.id) && rtMapById.get(oldMap.get(accRec.id).RecordTypeId).getName() == PROSPECTIVE 
                    && rtMapById.get(accRec.RecordTypeId).getName() == CLIENT 
                    && (accRec.Senior_Client_Advisor__c != null || accRec.Senior_Client_Advisor__c != ''))
            {
                    if(!mpAccToUser.isEmpty() && mpAccToUser.containsKey(accRec.id))
                        accRec.OwnerId = mpAccToUser.get(accRec.id);
                    if(!mpAccTourole.isEmpty() && mpAccTourole.containsKey(accRec.id)){
                        String strURole = mpAccTourole.get(accRec.id);
                        if(strURole != null && strURole != '' && !mpRoleToCadv.isEmpty() 
                                            && mpRoleToCadv.containsKey(strURole))  
                            accRec.Bessemer_Team__c = mpRoleToCadv.get(strURole);
                        else
                            accRec.Bessemer_Team__c = '';                       
                    }
                    if(!mpAccTouoffice.isEmpty() && mpAccTouoffice.containsKey(accRec.id))
                        accRec.Bessemer_Office__c = mpAccTouoffice.get(accRec.id);  
            }
        }
    }
    
    // business logic
    //This method will assign a new relationship number to any relationship that are created within Salesforce.com.
    //It assumes that any relationships created by the integration user account were created via Informatica.
    private void assignRelationshipNumber (list<Account> newList)
    {
        set <Id> changedRelationship = new set <Id> ();
        
        for (Account a : newList) {
            if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
                    if(a.Relationship_Number__c == null) {
                        changedRelationship.add(a.id);
                }
            }
        }

        if (changedRelationship.size() > 0) {
            Id userId = UserInfo.getUserId();
            GlobalSettings__c settings = [SELECT Id, Relationship_Number_Seq__c FROM GlobalSettings__c];
            Decimal nextRelationshipNumber = settings.Relationship_Number_Seq__c;
            
            List<Account> relationshipUpdates = [select id, Relationship_Number__c from Account where id in :changedRelationship]; 
            
            for (Account a : relationshipUpdates) {
                nextRelationshipNumber = nextRelationshipNumber + 1;
                a.Relationship_Number__c = String.valueof(nextRelationshipNumber);
            }
    
            if(relationshipUpdates.size() > 0) {
                settings.Relationship_Number_Seq__c = nextRelationshipNumber;
                update settings;
                update relationshipUpdates;
            }
        }
    }

    //2014-08-17: Requirement to sync contact owner with relationship has been dropped
/*
    private void updateAccountsContactOwner(list<Account> listNew, map<id,Account> oldMap){           
        List<Account> listAccount = new List<Account>();
        List<Contact> listContact = new List<Contact>();
        List<String> listAccIds = new List<String>();
        Map<String,String> mapAcctoOwner = new Map<String,String>();    
        for(Account accNew:listNew){  
            if(accNew.get('Ownerid') != oldMap.get(accNew.id).get('Ownerid')){
                listAccIds.add(accNew.id);
                mapAcctoOwner.put(accNew.id,accNew.OwnerId);
            }
        }
        if(listAccIds != null && listAccIds.size() > 0){                
            listAccount = [Select OwnerId, Id, (Select Id, OwnerId From Contacts) From Account where id=:listAccIds];
            for(Account acc:listAccount){
                for(Contact con:acc.Contacts){
                    if(!mapAcctoOwner.isEmpty() && mapAcctoOwner.containsKey(acc.id)){
                        con.OwnerId = mapAcctoOwner.get(acc.id);
                        listContact.add(con);
                    }
                }
            }
        }
        if(listContact != null && listContact.size() > 0){  
            update listContact;
        }
    }
*/
    
    //This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
    //completed successfully.  The Informatica job that syncs Famility Group EDB-->SFDC will use this data as a source to update
    //status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Account> recordList, String operation) {
        
        //records should only be added to the data sync log if the current user is the Bessemer Integration user
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
        
            list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
            for (Account relationship : recordList){
                if(relationship.EDB_Entity_Id__c <> null){
                    Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
                    syncRecord.EDB_Entity_Id__c = relationship.EDB_Entity_Id__c;
                    syncRecord.Entity__c = 'RELTNSHP';
                    syncRecord.Action__c = Operation;
                    syncList.Add(syncRecord);
                }
            }

            if(synclist.size() > 0)
                insert syncList;
        }
    }
    private void GetOrphanedConnections (list<Account> oldList)
    {
        set<string> contactIds = new set<string>();
        for (Account ct : oldList)
            contactIds.add(ct.id);
        List<Connections__c> connectionList = [select id from Connections__c where Relationship_R1__c in :ContactIds or Relationship2__c in :ContactIds];
        set<string> connectionIds = new set<string>();
        for (Connections__c c : connectionList  )
            connectionIds.add( c.id) ;
            
        RecursiveFlagHandler.orphanedIds = connectionIds ;
    }
    private void RemoveOrphanedConnections ()
    {
        
        List<Connections__c> connectionList = [select id from Connections__c where id in :RecursiveFlagHandler.orphanedIds ];
        delete connectionList ;
    }

    private static final Set<String> communicationEventTypes = new Set<String>{'Call', 'Meeting', 'Call - Inbound', 'Call - Outbound', 'Email - Outbound', 'Correspondence'};
    private static final Set<String> communicationTaskTypes = new Set<String>{'ITR-Followup'};
    private static final Set<String> callEventTypes = new Set<String>{'Call', 'Meeting', 'Call - Inbound', 'Call - Outbound', 'Email - Outbound'};

    private void updateLastActivityDates(List<Account> relationships) {
        Map<Id, Account> mapFromAccountIdToAccount = new Map<Id, Account>();
        for(Account r : relationships) {
            if(r.Id != Null) {
                mapFromAccountIdToAccount.put(r.Id, r);
                r.Last_Communication_Date__c = Null;
                r.Last_Call_Date__c = Null;
            }
        }

        for(Event e : [
            SELECT Id, AccountId, Type, ActivityDate
            FROM Event
            WHERE AccountId IN :mapFromAccountIdToAccount.keySet() AND ActivityDate != Null AND ActivityDate <= TODAY
        ]) {
            Account r = mapFromAccountIdToAccount.get(e.AccountId);
            if(communicationEventTypes.contains(e.Type)) {
                if(r.Last_Communication_Date__c == Null || r.Last_Communication_Date__c < e.ActivityDate) {
                    r.Last_Communication_Date__c = e.ActivityDate;
                }
            }
            if(callEventTypes.contains(e.Type)) {
                if(r.Last_Call_Date__c == Null || r.Last_Call_Date__c < e.ActivityDate) {
                    r.Last_Call_Date__c = e.ActivityDate;
                }
            }
        }

        for(Task t : [
            SELECT Id, AccountId, Type, Completed_Date__c
            FROM Task
            WHERE AccountId IN :mapFromAccountIdToAccount.keySet() AND Status = 'Done' AND Completed_Date__c != Null
        ]) {
            Account r = mapFromAccountIdToAccount.get(t.AccountId);
            if(callEventTypes.contains(t.Type)) {
                if(r.Last_Communication_Date__c == Null || r.Last_Communication_Date__c < t.Completed_Date__c) {
                    r.Last_Communication_Date__c = t.Completed_Date__c;
                }
            }
            if(true) {
                if(r.Last_Call_Date__c == Null || r.Last_Call_Date__c < t.Completed_Date__c) {
                    r.Last_Call_Date__c = t.Completed_Date__c;
                }
            }
        }
    }
/*
    private void validateDeletions(List<Account> oldRelationships) {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c) {
            return;
        }

        Set<Id> relationshipIds = new Set<Id>(); 
        Set<Id> blockedRelationshipIds = new Set<Id>();
       
        for(Account relationship : oldRelationships) {
            relationshipIds.add(relationship.Id);
        }

        for(Connections__c connection : [
            SELECT Id, Relationship_R1__c
            FROM Connections__c
            WHERE RecordType.DeveloperName = 'R_C' AND Relationship_R1__c IN :relationshipIds
        ]) {
            blockedRelationshipIds.add(connection.Relationship_R1__c);
        }

        for(Account relationship : oldRelationships) {
            if(relationship.Status__c != 'Closed') {
                System.debug('Relationship ' + relationship.Id + ': the Relationship Status must be Closed to delete the Relationship');
                relationship.addError('The Relationship Status must be Closed to delete the Relationship');
            }
            if(blockedRelationshipIds.contains(relationship.Id)) {
                System.debug('Relationship ' + relationship.Id + ': this Relationship cannot be deleted since it has connections to contacts');
                relationship.addError('This Relationship cannot be deleted since it has connections to contacts');
            }
        }
    }
*/
    // --- restore account record types after lead conversion ---
    private static void restoreOldRecordTypeIds(list<Account> newList, map<id,Account> oldMap) {
        if(!LeadTrigHand.isLeadConversion)
            return;

        for(Account newAccount : newList) {
            Account oldAccount = oldMap.get(newAccount.Id);
            newAccount.RecordTypeId = oldAccount.RecordTypeId;
        }
    }

    // --- validate deletions ---

    private void validateDeletions_2(list<Account> recordList) {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c) {
            return;
        }

        Set<Id> contactIds = Contact_TriggerHandler.extractField(recordList, 'Id');
        Set<Id> contactOwnerIds = Contact_TriggerHandler.extractField(recordList, 'OwnerId');

        Set<Id> blockedRelationshipIds = filterRelationshipIdsWithConnections(contactIds);
        Set<Id> contactIdsWithAssignedRoleToContact = filterRelationshipIdsWithAssignedRoleToContact(contactIds);
        Set<Id> ownerIdsWithCurrentUserAsParentInRoleHierarchy = Contact_TriggerHandler.filterUserIdsWithCurrentUserAsParentInRoleHierarchy(contactOwnerIds);

        for(Account relationship : recordList) {
            if(relationship.Status__c != 'Closed') {
                System.debug('Relationship ' + relationship.Id + ': the Relationship Status must be Closed to delete the Relationship');
                relationship.addError('The Relationship Status must be Closed to delete the Relationship');
            }
            if(blockedRelationshipIds.contains(relationship.Id)) {
                System.debug('Relationship ' + relationship.Id + ': this Relationship cannot be deleted since it has connections to contacts');
                relationship.addError('This Relationship cannot be deleted since it has connections to contacts');
            }
            if(contactIdsWithAssignedRoleToContact.contains(relationship.Id)) {
                System.debug('Relationship ' + relationship.Id + ': you cannot delete Relationships that have a contact with an assigned role');
                relationship.addError('You cannot delete Relationships that have a contact with an assigned role');
            }
            if(!ownerIdsWithCurrentUserAsParentInRoleHierarchy.contains(relationship.OwnerId)) {
                System.debug('Relationship ' + relationship.Id + ': you cannot delete Relationships unless you are the owner or a parent of the owner in the role hierarchy');
                relationship.addError('You cannot delete Relationships unless you are the owner or a parent of the owner in the role hierarchy');
            }
        }
    }

    public Set<Id> filterRelationshipIdsWithConnections(Set<Id> relationshipIds) {
        Set<Id> resultIds = new Set<Id>();

        for(Connections__c connection : [
            SELECT Id, Relationship_R1__c
            FROM Connections__c
            WHERE RecordType.DeveloperName = 'R_C' AND Relationship_R1__c IN :relationshipIds
        ]) {
            resultIds.add(connection.Relationship_R1__c);
        }

        return resultIds;
    }

    public Set<Id> filterRelationshipIdsWithAssignedRoleToContact(Set<Id> relationshipIds) {
        Set<Id> resultIds = new Set<Id>();

        for(Account_Contact__c accountContact : [
            SELECT Id, Contact__c
            FROM   Account_Contact__c
            WHERE  Account__c IN :relationshipIds
        ]) {
            resultIds.add(accountContact.Contact__c);
        }

        return resultIds;
    }

}