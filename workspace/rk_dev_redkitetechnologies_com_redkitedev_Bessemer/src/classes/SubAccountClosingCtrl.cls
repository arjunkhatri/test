public with sharing class SubAccountClosingCtrl 
{
	public Pagereference init ()
	{
		Profile profile = [SELECT Name FROM Profile where id = :userinfo.getProfileId()];
		if (profile.Name == 'Bessemer Executive Admin' 
			|| profile.Name == 'Bessemer Client Advisor' 
			|| profile.Name == 'Bessemer Client Admin Services (CAS)' )
			//|| profile.Name == 'System Administrator' )
			{
				string recordId = ApexPages.currentPage().getParameters().get('id') ;
				string SubaccountNumber = [select Name, SubAccount_Number_Unique__c from SubAccounts__c where id =:recordId].Name;
				string accountId = ApexPages.currentPage().getParameters().get('accountId') ;
				string relationshipNumber = [select Relationship_Number__c from Account where id = :accountId].Relationship_Number__c;
				
				//Get the url from Global Settings
				string baseURL = GlobalSettings__c.getInstance().SubAccount_Closing_URL__c;
				if (baseURL == null || baseURL.length() == 0 )
				{
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'The endpoint is not configured in global settings.  Contact your system administrator')) ;
					return null;
				}
				
				PageReference pgRef = new PageReference(baseUrl);
				//subacct_no=<<SubAccount_1>>%2c<<SubAccount_2>>%2c<<SubAccount_3>>&rel_no=<<Relationship_No>>
				pgRef.getParameters().put('subacct_no' , SubaccountNumber);
				pgRef.getParameters().put('rel_no' , relationshipNumber );
				pgRef.setRedirect(true);
				return pgRef;
			}
			else
			{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'The profile is not authorized to perform the closing process')) ;
			}
			return null;
	}
}