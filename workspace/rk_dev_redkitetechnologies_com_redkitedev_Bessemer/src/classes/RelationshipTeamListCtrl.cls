/*  
 *  Descprition :This Class displying  Team Members on Relationship detail page and handling 
 *               Edit and Delete actions on Team Members.
 *  Revision History:
 *   Version          Date            Description
 *   1.0            11/08/2014      Initial Draft
 */
public with sharing class RelationshipTeamListCtrl {  
    public String accId {get;set;} 
    public String accName {get;set;} 
    public List<Relationship_Team_Member__c> lstAccMem {get;set;}
    public List<Relationship_Team_Member__c> lstRtm {get;set;}
    public Account acct { get;set; }
    public String editId {get;set;} 
    public Boolean          displayPopUp            {get;set;}
    public Boolean          isBIUser                {get;set;}
    public List<Profile>    lstProfile              {get;set;}
    public String delId {get;set;}
    public static   final String SENIORCA   = 'Senior Client Advisor';
    public static   final String WEALTHAD   = 'Wealth Advisor';
    //variables for pagination
    public Integer RecordSize { get{ return lstAccMem.size(); } } // Total number of the records.
    public Integer PageCount { get{ return Math.ceil((Double)RecordSize / (Double)PageSize).intValue(); } } 
    public Boolean HasPrevious { get{ return PageNumber != 1; } }
    public Boolean HasNext { get{ return PageNumber != PageCount; } }
    public Boolean fullPage {get; set;}
    public Boolean isReadOnly {get; set;}
    public Boolean showMore { get; set; }
    public Integer PageNumber // Current page's page number.
    { 
        get;
        set
        {
            PageNumber = value;
            if(value <= 0)
            {
                PageNumber = 1;
            }
            else if(value > PageCount)
            {
                PageNumber = PageCount;
            }
        }
    }   
    
    public Integer PageSize 
    { 
    	get{
    			if(ApexPages.currentPage().getParameters().get( 'showMore' ) == 'true' ){
    				return 50;
    			}
    			else{
    				return 5;
    			}
    	   }
    }
    
    public RelationshipTeamListCtrl(ApexPages.Standardcontroller sc){
        isReadOnly = false; 
        displayPopUp = false;
        isBIUser = false;
        this.acct = (Account)sc.getRecord();
        if(acct != null && acct.id != null){ 
            accId = acct.id;
            lstAccMem = new List<Relationship_Team_Member__c>();
            lstAccMem = [Select User__c,Role__c,User__r.Name, Id,Relationship__r.Name
                         From Relationship_Team_Member__c where Relationship__c = :acct.id];
        }
        if(lstAccMem != null && lstAccMem.size() > 0){
        	accName = lstAccMem[0].Relationship__r.Name;
        	if(ApexPages.currentPage().getParameters().get( 'showMore' ) == 'true' ){
        		showMore = true;
        	}
        	else
        	{
        		showMore = false;
        	}
            firstPage();
        }
        
        List<AccountShare> accShr = [Select AccountAccessLevel From AccountShare 
                                    where UserOrGroupId=:userinfo.getUserId() AND 
                                    AccountId = :acct.id limit 1];
        if(accShr != null && accShr.size() > 0){  
            if(accShr[0].AccountAccessLevel == 'Read')
                isReadOnly = true;
        }                           
        lstProfile = [Select Id From Profile Where Name = 'Bessemer Integration' Limit 1];
        if(lstProfile != null && lstProfile.size() > 0)
            if(lstProfile[0].id != userinfo.getProfileId())
                isBIUser = false;
            else
                isBIUser = true;
    } 
    public pagereference recAdd(){
        return new pagereference('/apex/RelationshipTeam'+'?aid='+accId);
    }
    public pagereference recEdit(){
        if(editId != null && editId != ''){
           if(!isBIUser){
               displayPopUp = true;
               return null;
           }
        }
        return new pagereference('/apex/RelationshipTeam'+'?eid='+editId);
    } 
    
    public pagereference recDelete(){
        if(delId != null && delId != ''){
            String memUserid;
            Relationship_Team_Member__c atm = [Select id,User__c from Relationship_Team_Member__c where id =:delId limit 1];
			if(atm != null ){
                memUserid = atm.User__c;
	            try{
	                updateAccount(memUserid);
	                updateTask(memUserid); 	            	
	                delete atm;
	            }catch(DMLException ex){
	                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage())); 
	                return null;
	            }
        	}
        	//check user having still access on relationship and user is login user
        	List<Relationship_Team_Member__c> lstRtm = [Select Id,
        													   Relationship__c,
        													   User__c 
        													   From Relationship_Team_Member__c where 
        													   Relationship__c =:accId and User__c = :memUserid];
        	if(lstRtm.size() == 0 && Userinfo.getUserId() == memUserid)	
        		 return new pagereference('/001/o');											   
        }
        return new pagereference('/'+accId);
    }
    
    public void updateAccount(String delMem){
        Account accUpdate = [Select Senior_Client_Advisor__c , Wealth_Advisor__c from Account where id=:accId limit 1];
        if(accUpdate != null){  
            List<Relationship_Team_Member__c> actm = new List<Relationship_Team_Member__c>();
            actm = [Select Role__c ,User__c,User__r.Name from Relationship_Team_Member__c where 
                    Relationship__c = :accId and (Role__c = :SENIORCA or Role__c = :WEALTHAD ) ]; 

            accUpdate.Senior_Client_Advisor__c = '';
            accUpdate.Wealth_Advisor__c = '';

            if(actm != null && actm.size() > 0)
            {
                for (Relationship_Team_Member__c tm : actm )
                {
                    if (tm.Role__c == SENIORCA && tm.User__c != delMem)
                        accUpdate.Senior_Client_Advisor__c = tm.User__r.Name;
                    if (tm.Role__c == WEALTHAD && tm.User__c != delMem)
                        accUpdate.Wealth_Advisor__c = tm.User__r.Name;
                }
            }
            try{
                update accUpdate;
            }catch(DMLException ex){ 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        }
    }

    public void updateTask(String memId){
    	boolean isDuplicateExist = false;
        if(memId.length() == 18 && memId != null && memId != '')
            memId = memId.substring(0,15);
		List<Relationship_Team_Member__c> lstMem = [Select 
													id from Relationship_Team_Member__c where 
                    								Relationship__c = :accId  and User__c=:memId];   
        if(lstMem != null && lstMem.size() > 1)      
        	isDuplicateExist = true;	      								         
        List<Task> listTask = new List<Task>();
        if(accId != null && accId != ''){
            for(Task tsk :[Select WhatId, 
                            Relationship_Team_List__c 
                            From Task where WhatId =: accId])
            {
                if(tsk.Relationship_Team_List__c != null && tsk.Relationship_Team_List__c != ''){
                    if(tsk.Relationship_Team_List__c.contains(memId) && !isDuplicateExist){
                        tsk.Relationship_Team_List__c = tsk.Relationship_Team_List__c.replace('#'+memId+'#','');
                        listTask.add(tsk);
                    }
                }
            }
        }
        if(listTask != null && listTask.size() > 0){
            try{
                update listTask;
            }catch(DMLException ex){ 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        }
    }
    
    /*method for popup to be close*/
    public void closePopup() {  
        displayPopUp = false;
    }
    /**
     * Goes to the previous page.
     */
    public PageReference previousPage()
    {
        --PageNumber;
        return goToPage();
    }
    
    /**
     * Goes to the next page.
     */
    public PageReference nextPage()
    {
        ++PageNumber;
        return goToPage();
    }
    
    /**
     * Goes to the first page.
     */
    public PageReference firstPage()
    {
        PageNumber = 1;
        return goToPage();
    }
    
    /**
     * Goes to the last page.
     */
    public PageReference lastPage()
    {
        PageNumber = PageCount; 
        return goToPage();
    }
    
    /**
     * Refreshes the page.
     */
    public PageReference goToPage()
    {
        pagination();
        return null;
    }
    /**
     * Pages the forms.
     */
    private void pagination()
    {  
        lstRtm = new List<Relationship_Team_Member__c>();
        if(RecordSize > 0)
        {
            for(Integer i = 0; i < PageSize; i++)
            {
                Integer j = (pageNumber - 1) * PageSize + i;
                if(j < RecordSize)
                {
                    lstRtm.add(lstAccMem[j]);
                }
            }
        }
    }   
        
}