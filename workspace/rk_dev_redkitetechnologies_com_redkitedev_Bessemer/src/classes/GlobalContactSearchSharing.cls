public with sharing class GlobalContactSearchSharing
{
	public static Set<Id> contactOwnersRecords( Set<Id> contactSet )
	{
		List<Contact> ct = [ SELECT Id FROM Contact WHERE Id IN :contactSet ];
		Set<Id> idSet = new Set<Id>();
		for( Contact c : ct )
			idSet.add(c.id);

		return idSet;
	}

	public static Set<Id> accountOwnersRecords( Set<Id> accountSet )
	{
		List<Account> accts = [ SELECT Id FROM Account WHERE Id IN :accountSet ];
		Set<Id> idSet = new Set<Id>();
		for( Account a : accts )
			idSet.add(a.id);

		return idSet;
	}

	public static Set<Id> leadOwnersRecords( Set<Id> leadSet )
	{
		List<Lead> leads = [ SELECT Id FROM Lead WHERE Id IN :leadSet ];
		Set<Id> idSet = new Set<Id>();
		for( Lead l : leads )
			idSet.add(l.id);

		return idSet;
	}
}