/*
    
     Authors :  Don Koppel
     Created Date: 2014-08-08
     Last Modified: 2014-08-08
*/



public with sharing class LeadTrigHand 
{
	 
    // template methods
	public void onBeforeInsert(list<Lead> newList)
	{
		RecursiveFlagHandler.initialCount = newList.size() ;
		
		SetOccupationType ( newList) ;
		assignContactSNumber (newList);
	}
    public void onAfterInsert(list<Lead> newList) 
    {
		if (RecursiveFlagHandler.initialCount == newList.size() )
		{
	    	updateDataSyncLog(newList, 'I');
	    	CheckExistingContacts (newList);
	    	addAddressAsPrimary(newList);
    	}
    	
    }
    public void onBeforeUpdate (list<Lead> newList, map<id,Lead> oldMap)
    {
    	SetOccupationType ( newList) ;
    }
    public void onAfterUpdate(list<Lead> newList, map<id,Lead> newMap , map<id,Lead> oldMap) {
    	updateDataSyncLog(newList, 'U');
    }
    public void onAfterDelete(list<Lead> oldList){
    	updateDataSyncLog(oldList, 'D');
    }

// business logic
	//This method will assign a new contact S number to any Leads that are created within Salesforce.com.
	//It assumes that any Leads created by the integration user account were created via Informatica.
	private void assignContactSNumber (list<Lead> newList)
	{
		if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
			return;

		GlobalSettings__c settings = [SELECT Id, Contact_S_Number_Seq__c FROM GlobalSettings__c];
		Decimal nextContactNumber = settings.Contact_S_Number_Seq__c;
		Boolean bFound = false ;
		for (Lead l : newList) 
		{
			if(l.Contact_S_Number__c == null) 
			{
				nextContactNumber = nextContactNumber + 1;
				l.Contact_S_Number__c = ConvertSeqToSNumber(nextContactNumber);
				bFound = true;
			}
			
		}

		if (bFound)
		{
			settings.Contact_S_Number_Seq__c = nextContactNumber;
			update settings;
		}

		
        /*
        set <Id> changedLead = new set <Id> ();
	 	
		for (Lead l : newList) {
	    	if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
					if(l.Contact_S_Number__c == null) {
			            changedLead.add(l.id);
				}
	    	}
		}
		
		if (changedLead.size() > 0) {
			//Id userId = UserInfo.getUserId();
		 	
		 	GlobalSettings__c settings = [SELECT Id, Contact_S_Number_Seq__c FROM GlobalSettings__c];
		 	Decimal nextContactNumber = settings.Contact_S_Number_Seq__c;
		 	
		 	List<Lead> LeadUpdates = [select id, Contact_S_Number__c from Lead where id in :changedLead]; 
		 	
			for (Lead l : newList) 
			{
				nextContactNumber = nextContactNumber + 1;
				l.Contact_S_Number__c = ConvertSeqToSNumber(nextContactNumber);
			}
	
			if(LeadUpdates.size() > 0) {
				settings.Contact_S_Number_Seq__c = nextContactNumber;
				update settings;
				//update LeadUpdates;
			}
		}
		*/
	}

	//This method converts a numeric value to a Contact S Number.  Salesforce will assign S Numbers
	//within the range SL0000 and go to SLZZZZ
	private String ConvertSeqToSNumber (Decimal nextContactNumber) {
		String sNumber = '';
		String digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		Integer digit6 = Math.mod(nextContactNumber.intValue(), 36);
		sNumber = digits.substring(digit6, digit6 + 1) + sNumber;
		nextContactNumber = (nextContactNumber - digit6) / 36;
		Integer digit5 = Math.mod(nextContactNumber.intValue(), 36);
		sNumber = digits.substring(digit5, digit5 + 1) + sNumber;
		nextContactNumber = (nextContactNumber - digit5) / 36;
		Integer digit4 = Math.mod(nextContactNumber.intValue(), 46656);
		sNumber = digits.substring(digit4, digit4 +1) + sNumber;
		nextContactNumber = (nextContactNumber - digit4) / 36;
		Integer digit3 = Math.mod(nextContactNumber.intValue(), 1679616);
		sNumber = 'SL' + digits.substring(digit3, digit3 + 1) + sNumber;
		
		return sNumber;
	}

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs Contacts EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.  Note: EDB does not have separate tables for Leads and Contacts;
	//Leads are synced to SFDC along with Contacts
    private void updateDataSyncLog (list<Lead> recordList, String operation) {
    	
    	//records should only be added to the data sync log if the current user is the Bessemer Integration user
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
    	
	    	list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
	 		for (Lead lead : recordList){
	 			Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
	 			syncRecord.EDB_Entity_Id__c = lead.EDB_Entity_Id__c;
	 			syncRecord.Entity__c = 'CUST';
	 			syncRecord.Action__c = Operation;
	 			syncList.Add(syncRecord);
	 		}

			insert syncList;
    	}
    }
    
    //This method copys the Lead address to to the Address object
	private void addAddressAsPrimary (list<Lead> newList)
	{
		if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
    		return;
    		
		List<Addresses__c> addressList = new List<Addresses__c> ();
		list<string> affectedIds = new list<string>();
		
		//Bypass the following logic if the contact is inserted or updated by the integration user; should not fire for data sync from EDB
		if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
			for (Lead ld : newList)
			{

				if (ld.Street != null || ld.City != null || ld.State != null  || ld.PostalCode != null )
				{
					if (!RecursiveFlagHandler.alreadyRunning(ld.id))
					{
						Addresses__c address = new Addresses__c();
						address.Lead__c = ld.id;
						address.Address_Type__c = 'Home';
						address.Primary_Address__c = true;
						address.Street_Address__c = ld.Street;
						address.City__c = ld.City ;
						address.State__c = ld.State;
						address.Postal_Code__c = ld.PostalCode;
						address.Country__c = ld.Country;
						addressList.add(address);
						
					}
				}

				
			}
		}
		
		if (addressList.size() > 0)
		{
			
			insert addressList ;
			//for (Addresses__c address : addressList)
			//	affectedIds.add(address.id);
			//RecursiveFlagHandler.setRunning(affectedIds);
			
		}
	}
	//Method is used to set Occcuptaion Type and Other Occupation
    private void SetOccupationType (list<Lead> leadList )   
    {
    	set<string> pkListValues = Utility_Common.GetObjectsPickListValues('Lead', 'Occupation_Type__c' ) ; 
    	for (Lead ld : leadList)
    	{
    		if (!pkListValues.contains(ld.Occupation_Type__c) && ld.Occupation_Type__c <> '' && ld.Occupation_Type__c <> null )
    		{
    			ld.Other_Occupation__c = ld.Occupation_Type__c ;
    			ld.Occupation_Type__c = 'Other';
    		}
    		if (ld.Occupation_Type__c != 'Other')
    			ld.Other_Occupation__c = null;
    		
    	}
    }  
     //Method checks to see if an existing Lead exists, if so delete the lead and move taks and notes, attachment to the contact
    public void CheckExistingContacts (list<Lead> leadList)
    {

    	System.debug('****LEADS: ' + leadList.size() );
    	System.debug('****LEADS: ' + leadList );
    	
    	set<string> sContractSet = new set<string>();

    	map<string, string> contactMapping = new map<string, string> ();
    	map<string, string> leadMapping = new map<string, string> ();
        List<string> leadIDList = new List<String>() ;
        List<string> leadIDList2 = new List<String>() ;
    	for (Lead ld : leadList)
    	{
    		if (!RecursiveFlagHandler.alreadyRunning(ld.Contact_S_Number__c) &&  ld.Contact_S_Number__c != null)
    		{
    			sContractSet.add(ld.Contact_S_Number__c) ;
    			leadMapping.put(ld.Contact_S_Number__c , ld.id) ;
    			leadIDList2.add(ld.Contact_S_Number__c);
    		}


    	}
    	RecursiveFlagHandler.setRunning(leadIDList2) ;
    	
    	
    	
    	List<Contact> contactList = new List<Contact> ();
    	List<Contact> tmpContactList  = [select id, Contact_S_Number__c From Contact where Contact_S_Number__c in :sContractSet ];
    	List<string> ldList = new List<String>() ;
    	for (Contact ct : tmpContactList )
		{
			if (!RecursiveFlagHandler.alreadyRunning(ct.id) )
			{
				contactList.add(ct);
				contactMapping.put(ct.id , ct.Contact_S_Number__c ) ;
				leadIDList.add(contactMapping.get(ct.Contact_S_Number__c) ) ;
			}
			ldList.add(ct.id);
		}

		RecursiveFlagHandler.setRunning(ldList); 
		//Store the Contacts to prevent the Address Copy
		RecursiveFlagHandler.setRunning(leadIDList); 	
		
    	//If leads exist move activities,notes, attachments to contact and delete lead
    	//set<string> leadSet = new set<string>();
    	
    	if(contactList.size() > 0 )
    	{
    		map<string, string> contactMap = new map<string, string> ();
    		
    		for (Contact ct : contactList )
    		{
    			contactMap.put(ct.Contact_S_Number__c,ct.id) ;
    			//leadSet.add(ld.id);
    			
    		}
    		
    		
    		//Get the Activity List
    		List<Task> taskList = [SELECT Id, WhoId, WhatId FROM Task where whoId in :contactMapping.KeySet() Order by WhoId ALL ROWS];
    		map<string, list<Task>> taskMap = new map<string, list<Task>> ();
    		string currentId ;
    		List<Task> tmpTask = new List<Task> ();
    		for(Task tsk : taskList)
    		{
    			if (currentId == null )
    				currentId  = tsk.WhoId ;
    				
    			if (currentId != tsk.WhoId)
    			{
    				taskMap.put(currentId, tmpTask);
    				currentId  = tsk.WhoId ;
    			 	tmpTask = new List<Task> ();
    			}
    			
    			tmpTask.add(tsk) ;
    			
    		}
    		//Put the last item into the list
    		taskMap.put(currentId, tmpTask);
    		
    		List<Task> taskUpdateList = new List<Task> ();
    		//Loop back through the Lead List and get tasks to move
    		for (Lead ld : leadList)
    		{
    			//Get the lead by the Contact_S_Number__c
    			if (ld.Contact_S_Number__c != null)
    			{
    				if (contactMap.containskey(ld.Contact_S_Number__c) )
    				{
	    				if( taskMap.containskey (contactMap.get(ld.Contact_S_Number__c)) )
	    				{
	    					
			    		 	List<Task> tasksList =  taskMap.get(contactMap.get(ld.Contact_S_Number__c)) ;
			    		 	for (Task tsk : tasksList)
			    		 	{
			    		 		tsk.WhoId = ld.id;
			    		 		tsk.WhatId = null;
			    		 		taskUpdateList.add(tsk);
			    		 	}
	    				}
    				}
    			}	
    		}
    		update taskUpdateList;
    		//delete leadList;
    	}

		//Update the Address__c Object 
		list<string> affectedIds = new list<string>();
		List<Addresses__c>   addList = [select Lead__c ,Contact__c ,Address_Type__c,  Primary_Address__c  from Addresses__c where Contact__c in :contactMapping.KeySet()  ];
		for (Addresses__c a : addList)
			affectedIds.add(a.id);
			
		RecursiveFlagHandler.setRunning(affectedIds);
		
		/*==========  Move the Addresses to the Lead Object  ==========*/
		for (Addresses__c ad : addList)
		{
			if (contactMapping.ContainsKey(ad.Contact__c) && leadMapping.Containskey(ContactMapping.get(ad.Contact__c)) )
			{
				ad.Lead__c = leadMapping.get(ContactMapping.get(ad.Contact__c)) ;
				ad.Contact__c  = null;
			}
		}
	
		update addList;
		
		/*==========  Move the Keywords to the Lead Object  ==========*/
		List<KeywordContacts__c> keywordList = [select Contact__c, Lead__c  from KeywordContacts__c where Contact__c in :contactMapping.keyset() ] ;
		for (KeywordContacts__c kw : keywordList)
		{
			if (contactMapping.ContainsKey(kw.Contact__c) && leadMapping.Containskey(ContactMapping.get(kw.Contact__c)) )
			{
				kw.Lead__c = leadMapping.get(ContactMapping.get(kw.Contact__c)) ;
				kw.Contact__c = null ;
			}
		}
		update keywordList;

		/*==========  Move the Mailings to the Lead Object  ==========*/

		
		/*==========================================================================================================================

		++++++  End of Need to Uncomment after the New Mailings is Deployed  ++++++++++++++
	
		List<Mailing__c> mailingsList = [select Contact__c, Lead__c  from Mailing__c where Contact__c in :contactMapping.keyset() ] ;
		for (Mailing__c ml : mailingsList)
		{
			if (contactMapping.ContainsKey(ml.Contact__c) && leadMapping.Containskey(ContactMapping.get(ml.Contact__c)) )
			{
				ml.Lead__c = leadMapping.get(ContactMapping.get(ml.Contact__c)) ;
				ml.Contact__c = null ;
			}
		}
		update mailingsList;
		==============================================================  */


		if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
		{
			
			try 
			{
    			Database.DeleteResult[] drList = DataBase.Delete(contactList, false); 
    			for(Database.DeleteResult dr : drList) 
    			{
				    if (dr.isSuccess()) 
				    {
				        // Operation was successful, so get the ID of the record that was processed
				        System.debug('Successfully deleted account with ID: ' + dr.getId());
				    }
				    else 
				    {
				        // Operation failed, so get all errors                
				        for(Database.Error err : dr.getErrors()) 
				        {
				            System.debug('The following error has occurred.');                    
				            System.debug(err.getStatusCode() + ': ' + err.getMessage());
				            System.debug('Lead fields that affected this error: ' + err.getFields());
				        }
    				}
				}
    		}
    		catch (Exception ex ) 
    		{
    			//Check with Don.  Do we need a error Object.
    			System.Debug (ex.getMessage()) ;

    		}
    		
			
		}

    }

    private void HandleCeaseCommunications (list<Lead> newList )
   	{
   		set<string> leadSet = new set<string> () ;
   		for (Lead ld : newList)
   		{
   			
   			if (ld.Cease_Communications__c == true )
   			{
   				leadSet.add(ld.id) ;
   			}
   		}

   		List<Mailing__c> mailingsList = [select id, Delivery_Method__c  from Mailing__c where Lead__c in :leadSet and Mailings__c != 'Mailer' and Mailings__c != 'AAMG' and ( Delivery_Method__c = 'Mail and Email' or Delivery_Method__c = 'Email' or Delivery_Method__c = 'Mail')];

   		for (Mailing__c m :mailingsList)
   			m.Delivery_Method__c = '';

   		if (!mailingsList.isEmpty())
   			update mailingsList;
   	}


}