@isTest
private class TestManageMailing_EditController {
	
        @isTest static void testEditMailing() {
                GlobalSettings__c setting = TestDataFactory.createGlobalSetting();
                setting.Relationship_Number_Seq__c = 1201;
                setting.Integration_User_Name__c = userinfo.getUserName();
                insert setting;

                Account acc = TestDataFactory.createTestAccount();
                insert acc;

                Contact con = TestDataFactory.createTestContact( acc );
                con.Contact_Preference__c = 'Mail;Do Not Mail';
                insert con;

                Mailing__c testMailing = new Mailing__c();
                testMailing.Contact__c = con.Id;
                insert testMailing;

                Mailing_Link__c testMailLink = new Mailing_Link__c();
                testMailLink.MailingId__c = testMailing.Id;
                insert testMailLink;

                Addresses__c testAdd = new Addresses__c();
                testAdd.Contact__c = con.Id;
                insert testAdd;

                Test.startTest();
                        ApexPages.StandardController std = new ApexPages.StandardController( testMailing );
                	ManageMailing_EditController ctrl = new ManageMailing_EditController( std );

                	List<SelectOption> types = ctrl.getMailType();
                	ctrl.changeContact();
                	ctrl.mailRec.Delivery_Method__c = 'None';
                	ctrl.Save();
                Test.stopTest();
        }


	@isTest static void testNewMailing(){

		GlobalSettings__c setting = TestDataFactory.createGlobalSetting();
		setting.Relationship_Number_Seq__c = 1201;
		setting.Integration_User_Name__c = userinfo.getUserName();
		insert setting;

		Account acc = TestDataFactory.createTestAccount();
                insert acc;
        
                Contact con = TestDataFactory.createTestContact( acc );
                insert con;

		Test.startTest();
			PageReference testPage = Page.ManageMailing_Edit;
			testPage.getParameters().put( ManageMailing_EditController.PARAM_CONTACTID, con.Id );
			testPage.getParameters().put( ManageMailing_EditController.PARAM_MAILINGS, 'mailings' );
			testPage.getParameters().put( ManageMailing_EditController.PARAM_TYPE, 'type' );
			Test.setCurrentPage( testPage );

        	ApexPages.StandardController std = new ApexPages.StandardController( new Mailing__c() );
        	ManageMailing_EditController ctrl = new ManageMailing_EditController( std );
        Test.stopTest();
	}
}