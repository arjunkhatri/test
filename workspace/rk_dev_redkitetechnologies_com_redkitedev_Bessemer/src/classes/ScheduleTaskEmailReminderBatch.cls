/*
 * Description : A schedule class which will fetch the new batch & schedule it per every day.
 */
global class ScheduleTaskEmailReminderBatch implements Schedulable{
    global void execute(SchedulableContext ctx) {
        this.start();
        id Batchid = database.executeBatch(new TaskEmailReminderBatch(),10);
    }
    
    // This method will add the  1 day to current time
    public void start()
    {
     	Datetime dtObj = System.now().addDays(1);
        String day = string.valueOf(dtObj.day());
        String month = string.valueOf(dtObj.month());
        String hour = string.valueOf(dtObj.hour());
        String minute = String.valueOf(dtObj.minute());
        String second = string.valueOf(dtObj.second());
        String year = string.valueOf(dtObj.year());
        String strJobName = 'TaskEmailReminderBatch-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        System.schedule(strJobName, strSchedule, new ScheduleBatch());
    }     
}