@isTest
public with sharing class TestTrigContact 
{
    static testMethod void testMethod_Trigger_Contact_test1() 
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;
        
        
        map<String, set<String>> ExcludeMap = new map<String, set<String>> ();
        ExcludeMap = TestDataCreator_Utils.ExcludedFields ;
        set<string> excludeFld = new set<string> ();
        excludeFld.add('Lead__c') ;
        //excludeFld.add('geopointe__Geocode__c') ;
        
        //There are values already in the map so need rebuild
        if (TestDataCreator_Utils.ExcludedFields.containskey('Account') )
        {
            set<string> tmpSet = TestDataCreator_Utils.ExcludedFields.get('Account') ;
            for (string key : tmpSet)
                excludeFld.add( key) ;
        }
        ExcludeMap.put('Addresses__c', excludeFld);
        TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;
        TestDataCreator_Utils.SingleRelatedRecord =  new set<string>() ;
        List<Addresses__c> addressList = TestDataCreator_Utils.createSObjectList('Addresses__c' , true , 25); 
        List<Contact> contactList = [select id from Contact ] ;
        for (integer c = 0 ; c<25 ;c++ )
        {
            addressList[c].Contact__c = contactList[c].id ;
        }
         insert addressList;

        //Try Deleting a record
        Contact ctDelete = [select id from Contact Limit 1] ;
        delete ctDelete;

       
         

    }
    static testMethod void testMethod_Trigger_Contact_test2() 
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        Account acct = new Account ();
        acct.RecordTypeId = [select id from RecordType where SobjectType = 'Account' and Name = 'Client Relationship'].id;
        acct.Name = 'Test Method';
        insert acct;

        Contact newContact = new Contact();
        newContact.RecordTypeId = [select id from RecordType where SobjectType = 'Contact' and Name = 'Client Contact'].id;
        newContact.AccountId = acct.id;
        newContact.FirstName = 'firtname';
        newContact.LastName =  'lastname';
        newContact.MailingStreet = '1 test ave';
        newContact.MailingCity = 'Test City' ;
        newContact.MailingState = 'TX';
        insert newContact;

    }
    static testMethod void testMethod_Trigger_Contact_test3() 
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        Account acct = new Account ();
        acct.RecordTypeId = [select id from RecordType where SobjectType = 'Account' and Name = 'Client Relationship'].id;
        acct.Name = 'Test Method';
        insert acct;

        //Test  addAddressAsPrimary 
        Contact newContact2 = new Contact();
        newContact2.AccountId = acct.id;
        newContact2.FirstName = 'firtname';
        newContact2.LastName =  'lastname';
        newContact2.MailingStreet = '1 test ave';
        newContact2.MailingCity = 'Test City' ;
        newContact2.MailingState = 'TX';
        insert newContact2;

        newContact2.Email = 'test@test12.com' ;
        newContact2.RecordTypeId = [select id from RecordType where SobjectType = 'Contact' and Name = 'Prospect'].id;
        update newContact2 ;

        newContact2.RecordTypeId = [select id from RecordType where SobjectType = 'Contact' and Name = 'Client Contact'].id;
        update newContact2 ;
    }

    static testMethod void testMethod_Trigger_Contact_test4() 
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;
        
        Lead ld =  (Lead)TestDataCreator_Utils.createSObject('Lead' , true , 1);
        insert ld;
        PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', ld.Id);
        Test.startTest();
        LeadConversionCtrl myCon = new LeadConversionCtrl ();
        myCon.createTask = true;
        myCon.createNew = true;
        myCon.CompanyName = 'ABC TEST CORP' ;
        Task TaskObj = new Task ();
        TaskObj.Subject = 'test' ;
        TaskObj.WhoId = ld.Id;
        insert TaskObj;

        myCon.convert() ;

       
    }

    private static void CreateAddresses ()
    {
        set<string> singleGrp = new set<string>() ;
        singleGrp.add('Account');

        map<String, set<String>> ExcludeMap = new map<String, set<String>> ();
        ExcludeMap = TestDataCreator_Utils.ExcludedFields ;
        set<string> excludeFld = new set<string> ();
        excludeFld.add('Lead__c') ;
        //excludeFld.add('geopointe__Geocode__c') ;
        
        //There are values already in the map so need rebuild
        if (TestDataCreator_Utils.ExcludedFields.containskey('Account') )
        {
            set<string> tmpSet = TestDataCreator_Utils.ExcludedFields.get('Account') ;
            for (string key : tmpSet)
                excludeFld.add( key) ;
        }
        ExcludeMap.put('Addresses__c', excludeFld);
        TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;
        TestDataCreator_Utils.SingleRelatedRecord =  singleGrp;
        List<Addresses__c> addressList = TestDataCreator_Utils.createSObjectList('Addresses__c' , false , 25); 
        
       
        
       
    }

}