@isTest
private class TestManageMailing_ContactController {
	
	@isTest static void testContactMailing() {

		GlobalSettings__c setting = TestDataFactory.createGlobalSetting();
		setting.Relationship_Number_Seq__c = 1201;
		setting.Integration_User_Name__c = userinfo.getUserName();
		insert setting;

		Account acc = TestDataFactory.createTestAccount();
        insert acc;
        
        Contact con = TestDataFactory.createTestContact( acc );
        insert con;

        Mailing__c testMailing = new Mailing__c();
        testMailing.Contact__c = con.Id;
        insert testMailing;

        Mailing_Link__c testMailLink = new Mailing_Link__c();
        testMailLink.MailingId__c = testMailing.Id;
        insert testMailLink;

        Addresses__c testAdd = new Addresses__c();
        testAdd.Contact__c = con.Id;
        insert testAdd;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController( con );
			ManageMailing_ContactController ctrl = new ManageMailing_ContactController( std );
			ctrl.getMailingRecs();
			ctrl.massEdit();
			ctrl.tableEntries[0].isSelected = true;
			ctrl.massEditSave();
		Test.stopTest();
	}	
}