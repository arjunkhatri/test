global class MailingTableEntry
{
        public Mailing__c mailingRec{get;set;}
        public List<Mailing_Link__c> familyGroups{get;set;}
        public List<Mailing_Link__c> otherContacts{get;set;}
        public List<Mailing_Link__c> otherEntities{get;set;}
        public Boolean isSelected{get;set;}
        
        public MailingTableEntry(Mailing__c rec){
            this.mailingRec = rec;
            familyGroups = new List<Mailing_Link__c>();
            otherContacts = new List<Mailing_Link__c>();
            otherEntities = new List<Mailing_Link__c>();
        }
        public void addLink(Mailing_Link__c inLink){
            if(inLink.RecordType.Name=='Family Group') familyGroups.add(inLink);
            else if(inLink.RecordType.Name=='Other Contact') otherContacts.add(inLink);
            else otherEntities.add(inLink);
        }
}