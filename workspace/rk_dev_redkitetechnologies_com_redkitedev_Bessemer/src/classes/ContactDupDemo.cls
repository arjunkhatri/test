public with sharing class ContactDupDemo 
{
	public String contactId {get; set;} //account id
	public Contact ct {get; set;}
	
	public string testResponse {get;set;}
	public ContactDupDemo (ApexPages.Standardcontroller sc) 
	{
		testResponse = 'page Opened' ;
		contactId = sc.getId();
		ct = (Contact)sc.getRecord() ;
	}
	
	public PageReference Save()
	{
		testResponse = 'Save clicked' ;
		return null;
	}
}