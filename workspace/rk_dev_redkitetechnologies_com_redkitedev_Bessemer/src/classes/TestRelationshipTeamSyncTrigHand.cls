@isTest
public with sharing class TestRelationshipTeamSyncTrigHand {
	static testMethod void RelationshipTeamSyncTrigHandtest(){
		Profile pf = [Select Id From Profile where Name= 'Bessemer Wealth Advisor'];
		Profile pf1 = [Select Id From Profile where Name= 'Bessemer Client Advisor'];
		
		User u = TestDataFactory.createTestUser(pf.id);
			insert u;
			system.assert(u.id != null,'Created user record');
		User u1 = TestDataFactory.createTestUser(pf1.id);
			u1.Username = 'test1@domain.com.dev';
			insert u1;
			system.assert(u1.id != null,'Created user record');				
		Account acc = TestDataFactory.createTestAccount();
			acc.Relationship_Number__c = '1234';
		 	insert acc;
		 	system.assert(acc.id != null,'Created relationship record');
		AccountTeamMember accTM = TestDataFactory.createATM(acc.id,u.id,'Cash Manager');
			insert accTM;
			system.assert(accTM.id != null,'Created account team member record');
		Task tsk = TestDataFactory.createTask(acc.id);
			tsk.Relationship_Team_List__c = '';
			insert tsk;
		Relationship_Team_Sync__c rTeamUpdate = new Relationship_Team_Sync__c(Name = 'test rel sync 1',
			AccountId__c = acc.id,UserId__c = u.id,Role__c = 'Other',EDB_Relationship_Team_Key__c = '123',
			Action__c = 'U');
			insert rTeamUpdate;
		String accId = String.valueOf(acc.id);
		if(accId.length() == 18)
			accId = accId.substring(0,15);
		Relationship_Team_Sync__c rTeamInsert = new Relationship_Team_Sync__c(Name = 'test rel sync 2',
			AccountId__c =id.valueOf(accId),UserId__c = u1.id,Role__c = 'Other',EDB_Relationship_Team_Key__c = '1234',
			Action__c = 'I');
			insert rTeamInsert;
			
		Relationship_Team_Sync__c rTeamDelete = new Relationship_Team_Sync__c(Name = 'test rel sync 1',
			AccountId__c = acc.id,UserId__c = u.id,Role__c = 'Other',EDB_Relationship_Team_Key__c = '123',
			Action__c = 'D');
			insert rTeamDelete;			
							 		
	}
}