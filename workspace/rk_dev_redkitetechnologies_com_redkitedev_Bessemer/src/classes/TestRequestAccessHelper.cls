@isTest (SeeAllData=True)
private class TestRequestAccessHelper
{
	 static testMethod void RequestAccessHelper_AccountTest ()
	 {
        Account acct =  (Account )TestDataCreator_Utils.createSObject('Account' , false , 1);
        insert acct ;
        Test.StartTest();
        RequestAccessHelper.SendRequestEmail (acct.id, userInfo.getUserId() , false , 'Test message') ;
        RequestAccessHelper.SendRequestEmail (acct.id, userInfo.getUserId() , true , 'Test message') ;
        Test.stopTest() ;
	 }
	 static testMethod void RequestAccessHelper_ContactTest ()
	 {
        Contact ct =  (Contact )TestDataCreator_Utils.createSObject('Contact' , false , 1);
        insert ct ;
        Test.StartTest();
        RequestAccessHelper.SendRequestEmail (ct.id, userInfo.getUserId() , false , 'Test message') ;
        RequestAccessHelper.SendRequestEmail (ct.id, userInfo.getUserId() , true , 'Test message') ;
        Test.stopTest() ;
	 } 
	 static testMethod void RequestAccessHelper_LeadTest ()
	 {
        Lead ld =  (Lead )TestDataCreator_Utils.createSObject('Lead' , false , 1);
        insert ld ;
        Test.StartTest();
        RequestAccessHelper.SendRequestEmail (ld.id, userInfo.getUserId() , false , 'Test message') ;
        RequestAccessHelper.SendRequestEmail (ld.id, userInfo.getUserId() , true , 'Test message') ;
        Test.stopTest() ;
	 } 
}