public with sharing class DuplicateContactCtrl 
{
	public List<Contact> dupes {get;set;}
	public boolean hasDuplicates {get;set;}
    public List<GlobalContactSearch.ContactResultSet> dupList {get;set;}
    
	public void Init()
	{
		string DupesList = ApexPages.currentPage().getParameters().get('dupes' );
		if ( DupesList == null || DupesList.trim().length() == 0 )
            return;
        List<String> dupePairs = DupesList.split( '\f' );
        Set<Id> dupeIds = new Set<Id>();
        // Add just the ID from the ID URL/name pairs and remove the leading / from the ID
        // URL to convert it to an ID and then add it to the list of IDs to query.
        for ( String pair : dupePairs )
            dupeIds.add( pair.split( '\b' )[0].subString( 1 ) );
      
      	 //Get Duplicate List
      	GlobalContactSearch searchObj = new GlobalContactSearch ();
        dupList = searchObj.ContactDataSetDupBlocker (dupeIds);
	}
}