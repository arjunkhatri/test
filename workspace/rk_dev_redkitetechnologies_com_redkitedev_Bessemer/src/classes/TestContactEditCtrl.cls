@isTest
private class TestContactEditCtrl
{
    static testMethod void ContactEditCtrl_Test1() 
    {
        Contact con = createTestData ();
        con.RecordTypeId = [select id from RecordType where sObjectType = 'Contact' and Name = 'Prospect' ].id;
        update con;

        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.ContactEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
        ApexPages.currentPage().getParameters().put('Id', con.Id);
        Test.startTest() ;
        ContactPageExt myCon = new ContactPageExt(sc);
        DupeBlockerListCtrl dupBlock = new DupeBlockerListCtrl ();
        myCon.setComponentController (dupBlock)  ;
        myCon.init();
        myCon.contactRec.email = 'test@test.com';
        myCon.saveRecord() ;

        Test.StopTest();

    }
    static testMethod void ContactEditCtrl_Test2() 
    {
        Contact con = createTestData ();
        con.RecordTypeId = [select id from RecordType where sObjectType = 'Contact' and Name = 'Prospect' ].id;
        update con;

        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.ContactEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
        ApexPages.currentPage().getParameters().put('Id', con.Id);
        Test.startTest() ;
        ContactPageExt myCon = new ContactPageExt(sc);
        DupeBlockerListCtrl dupBlock = new DupeBlockerListCtrl ();
        myCon.setComponentController (dupBlock)  ;
        myCon.init();
        myCon.contactRec.email = 'test@test.com';
        myCon.saveRecordOverride() ;

        Test.StopTest();

    }
     private static Contact createTestData ()
    {
        /********** Create test data *****************/
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        ContactFieldList__c ctFld = new ContactFieldList__c();
        ctFld.Name = 'DK_Com_LastName' ;
        ctFld.SectionName__c = 'Communication Details' ;
        ctFLd.SectionOrder__c = 1; 
        ctFld.SortOrder__c = 103;
        ctFld.Field__c = 'Email' ;
        ctFld.Platform__c = 'Desktop';
        ctFld.Field_Id__c = '1';
        ctFLd.PageName__c = 'Contact Prospect' ;
        insert ctFld ;
        ctFld = new ContactFieldList__c();
        ctFld.Name = 'DK_Com_FirstName' ;
        ctFld.SectionName__c = 'Communication Details' ;
        ctFLd.SectionOrder__c = 1; 
        ctFld.SortOrder__c = 104;
        ctFld.Field__c = 'Email' ;
        ctFld.Platform__c = 'Desktop';
        ctFld.Field_Id__c = '2';
        ctFLd.PageName__c = 'Contact Prospect' ;
        insert ctFld ;
        ctFld = new ContactFieldList__c();
        ctFld.Name = 'DK_Com_BLANK' ;
        ctFld.SectionName__c = 'Communication Details' ;
        ctFld.SectionOrder__c = 2; 
        ctFld.SortOrder__c = 303;
        ctFld.Field__c = 'BlankSectionItem' ;
        ctFld.Platform__c = 'Desktop';
        ctFld.Field_Id__c = '3';
        ctFLd.PageName__c = 'Contact Prospect' ;
        insert ctFld ;
        ctFld = new ContactFieldList__c();
        ctFld.Name = 'DK_Com_Email' ;
        ctFld.SectionName__c = 'Communication Details' ;
        ctFld.SectionOrder__c = 2; 
        ctFld.SortOrder__c = 303;
        ctFld.Field__c = 'Email' ;
        ctFld.Platform__c = 'Desktop';
        ctFld.Field_Id__c = '4';
        ctFLd.PageName__c = 'Contact Prospect' ;
        insert ctFld ;


        ContactEditLayouts__c  ctLayouts = new ContactEditLayouts__c ();
        ctLayouts.Name = 'Prospect' ;
        ctLayouts.PageName__c = 'Contact Prospect' ;
        insert ctLayouts;

        CRMfusionDBR101__Scenario__c scenario = new CRMfusionDBR101__Scenario__c ();
        scenario.Name = 'Contact Name'; 
        scenario.CRMfusionDBR101__Scenario_Type__c = 'Contact' ;
        scenario.CRMfusionDBR101__Allow_Block_Bypass__c = true ;
        scenario.CRMfusionDBR101__Deployed__c = true;
        insert scenario ;
        CRMfusionDBR101__Scenario_Rule__c rule = new CRMfusionDBR101__Scenario_Rule__c ();
        rule.CRMfusionDBR101__Field_Name__c  = 'Email' ; 
        rule.CRMfusionDBR101__Mapping_Type__c = 'Exact' ;
        rule.CRMfusionDBR101__Scenario__c = scenario.id;
        rule.CRMfusionDBR101__Scenario_Type__c = 'Contact' ; 
        insert rule;


        
        Account acc = TestDataFactory.createTestAccount();
        insert acc;
        system.assert(acc.id != null,'Account record created successfully');
        
               
        Contact con = TestDataFactory.createTestContact(acc);
        insert con;
        system.assert(con.id != null,'Contact record created successfully');  

        return con;
    }
}