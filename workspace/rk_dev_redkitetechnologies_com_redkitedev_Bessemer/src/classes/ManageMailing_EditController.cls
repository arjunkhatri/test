public with sharing class ManageMailing_EditController {
	public static final string PARAM_CONTACTID ='cid';
	public static final String PARAM_MAILINGS = 'mailings';
	public static final String PARAM_TYPE = 'type';

	public Mailing__c mailRec {get; set; }
	public List<Addresses__c> listAddresses;
	public List<Row> rows { get; set; }
	public Boolean showAdd {get; set;}
	public String salutation {get;set;}
	public Boolean newPage {get;set;}
	public Boolean editPage {get;set;}
	public list<SelectOption> options {get; set;}
	public Contact cont;
	public Lead ld;
	public Boolean isLead {get;set;}

	


	public ManageMailing_EditController(ApexPages.StandardController controller) {
		map<String,String> params = ApexPages.currentPage().getParameters();
		isLead = false ;

		//check if existing record, or a new one
		if(controller.getRecord().Id!=null){
			mailRec = (Mailing__c)controller.getRecord();
			mailRec = [Select Id, Name, Contact__c, Lead__c, Mailings__c, Type__c, Delivery_Method__c, Details__c, Address__c,  Salutation__c
						from Mailing__c where Id =: mailRec.Id];
			setAddresses();
			system.debug('record is not null');
			newPage = false;
			editPage = true;
			if (mailRec.Lead__c != null && mailRec.Contact__c == null)
				isLead = true;
		}
		else{
			mailRec = new Mailing__c();
			listAddresses = new List<Addresses__c>();
			rows = new List<Row>();
			showAdd = false;
			newPage = true;
			editPage = false;
			String contactId = params.get(PARAM_CONTACTID);
			if (String.isEmpty(contactId)==false && contactId.substring(0,3) == '003')
			{
				mailRec.Contact__c = contactId;
				setAddresses();
				isLead = false;
			}
			else
			{
				mailRec.Lead__c = contactId;
				setAddresses();
				isLead = true;
			}


			if (params.containsKey(PARAM_MAILINGS)) mailRec.Mailings__c = params.get(PARAM_MAILINGS);
			if (params.containsKey(PARAM_TYPE)) 
				mailRec.Type__c = params.get(PARAM_TYPE);
			
		}
	}

	public void Init () 
	{
		if (isLead == false )
		{
			/*==========  Commeneted OUT to change Suffix  ==========
			
			if (newPage == true)
			{
				string contactId = ApexPages.currentPage().getParameters().get('cid');
				cont = [select Salutation, FirstName, LastName, Suffix__c , Do_Not_Mail__c , HasOptedOutOfEmail from Contact where id = :contactId ];
			}
			Else
				cont = [select Salutation, FirstName, LastName, Suffix__c , Do_Not_Mail__c , HasOptedOutOfEmail from Contact where id = :mailRec.Contact__c ];

			==========*/
		}
		if (isLead == true )
		{
			/*==========  Commeneted OUT to change Suffix  ==========
			if (newPage == true)
			{
				string leadId = ApexPages.currentPage().getParameters().get('cid');
				ld = [select Salutation, FirstName, LastName, Suffix__c , Do_Not_Mail__c , HasOptedOutOfEmail from Lead where id = :leadId ];
			}
			else
				ld = [select Salutation, FirstName, LastName, Suffix__c , Do_Not_Mail__c , HasOptedOutOfEmail from Lead where id = :mailRec.Lead__c ];

			****/
		}
	}

	/*
	public List<SelectOption> getMailType()
	{
		
		System.debug('getMailType() ' + mailRec.contact__c);
		Schema.DescribeFieldResult fieldResult = Mailing__c.Delivery_Method__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		options = new List<SelectOption>();
		Set<String> contactPrefPV = new Set<String>();
		options.add(new SelectOption('None', 'None'));
	    
		if(mailRec.contact__c!=null){
			cont = [Select Id, Name, Contact_Preference__c from Contact where Id=: mailRec.contact__c];
     	
     	//obtain values for preffered contact, which is multiselect PV
	     	if(cont.Contact_Preference__c!=null && cont.Contact_Preference__c!=''){
	     		for(String pv: cont.Contact_Preference__c.split(';'))
	     		{
	     			System.debug('Adding PV value to set: ' + pv);
	     			if(!contactPrefPV.contains(pv))
	     				contactPrefPV.add(pv);
	     		}
	     	}
     	}

	        
	   for( Schema.PicklistEntry f : ple)
	   {
	   	  if(mailRec.contact__c!=null){

	   	  	System.debug('contactpregpv logic: ' +  contactPrefPV.contains('Do Not Mail') +  contactPrefPV.contains('Do Not Email'));
	   	  	
	   	  	if(!contactPrefPV.contains('Do Not Mail') && !contactPrefPV.contains('Do Not Email'))
	      		options.add(new SelectOption(f.getLabel(), f.getValue()));
	      	else
	      	{
	      		System.debug(f.getValue());
	      		System.debug('in Else part ' +  f.getValue().contains('Mail') + contactPrefPV.contains('Do Not Mail'));
	      		System.debug('in Else part 2 ' + f.getValue().contains('Email') + contactPrefPV.contains('Do Not Email'));
	      		if(f.getValue().contains('Mail') && contactPrefPV.contains('Do Not Mail'))
	      		{
	      			//do nothing
	      		}
	      		
	      		else if(f.getValue().contains('Email') && contactPrefPV.contains('Do Not Email'))
	      		{
	      				//do nothing
	      		}

	      		else
	      			options.add(new SelectOption(f.getLabel(), f.getValue()));


	      	}


	      }
	      else
	      		options.add(new SelectOption(f.getLabel(), f.getValue()));
	   }       
	   return options;
		

	}
	*/
	public void SalutationChange ()
	{
		/*	
		if (isLead == false)
		{
			
			if (mailrec.Salutation_Type__c == 'Formal')
				mailRec.Salutation__c = String.ValueOf(CheckForNull (cont.Salutation) + ' ' + CheckForNull (cont.FirstName) + ' ' + CheckForNull (cont.LastName) + ' ' + CheckForNull (cont.Suffix__c) );
			if (mailrec.Salutation_Type__c == 'Prefix + Last')
				mailRec.Salutation__c = String.ValueOf(CheckForNull (cont.Salutation) + ' ' + CheckForNull (cont.LastName)  );
			if (mailrec.Salutation_Type__c == 'First + Last')
				mailRec.SalutationText__c = String.ValueOf(CheckForNull (cont.FirstName) + ' ' + CheckForNull (cont.LastName)  );
			if (mailrec.Salutation_Type__c == 'Custom'  || mailrec.Salutation_Type__c == null)
				mailRec.SalutationText__c = '';
		}

		if (isLead == true)
		{
			
			if (mailrec.Salutation_Type__c == 'Formal')
				mailRec.SalutationText__c = String.ValueOf(CheckForNull (ld.Salutation) + ' ' + CheckForNull (ld.FirstName) + ' ' + CheckForNull (ld.LastName) + ' ' + CheckForNull (ld.Suffix__c) );
			if (mailrec.Salutation_Type__c == 'Prefix + Last')
				mailRec.SalutationText__c = String.ValueOf(CheckForNull (ld.Salutation) + ' ' + CheckForNull (ld.LastName)  );
			if (mailrec.Salutation_Type__c == 'First + Last')
				mailRec.SalutationText__c = String.ValueOf(CheckForNull (ld.FirstName) + ' ' + CheckForNull (ld.LastName)  );
			if (mailrec.Salutation_Type__c == 'Custom'  || mailrec.Salutation_Type__c == null)
				mailRec.SalutationText__c = '';
		}
		*/
	}

	private string CheckForNull (string strValue)
	{
		string retval = '';
		if (strValue != null)
			retVal = strValue;
		return retVal;
	}

	public List<SelectOption> getDeliveryMethods ()
	{
		List<SelectOption> retVal = new List<SelectOption>();
		retVal.add(new SelectOption('None', 'None'));
		
		if (isLead == false)
		{
			if (cont.HasOptedOutOfEmail  == false )
			{
				retVal.add(new SelectOption('Email', 'Email'));
				
				
			}
			else
			{
				if (mailRec.Delivery_Method__c == 'Email')
					mailRec.Delivery_Method__c = 'None';
			}
			if (cont.Do_Not_Mail__c == false )
			{
				retVal.add(new SelectOption('Mail', 'Mail'));
				
				
			}
			else
			{
				if (mailRec.Delivery_Method__c == 'Mail')
					mailRec.Delivery_Method__c = 'None';
			}

			if (cont.HasOptedOutOfEmail == false && cont.Do_Not_Mail__c == false )
			{
				retVal.add(new SelectOption('Mail and Email', 'Mail and Email'));
				
			}	
			else
			{
				if (mailRec.Delivery_Method__c == 'Mail and Email')
					mailRec.Delivery_Method__c = 'None';
			}
		}

		if (isLead == true)
		{
			if (ld.HasOptedOutOfEmail  == false )
			{
				retVal.add(new SelectOption('Email', 'Email'));
				
				
			}
			else
			{
				if (mailRec.Delivery_Method__c == 'Email')
					mailRec.Delivery_Method__c = 'None';
			}
			if (ld.Do_Not_Mail__c == false )
			{
				retVal.add(new SelectOption('Mail', 'Mail'));
				
				
			}
			else
			{
				if (mailRec.Delivery_Method__c == 'Mail')
					mailRec.Delivery_Method__c = 'None';
			}

			if (ld.HasOptedOutOfEmail == false && ld.Do_Not_Mail__c == false )
			{
				retVal.add(new SelectOption('Mail and Email', 'Mail and Email'));
				
			}	
			else
			{
				if (mailRec.Delivery_Method__c == 'Mail and Email')
					mailRec.Delivery_Method__c = 'None';
			}
		}



		return retVal;
	}
	public PageReference changeContact(){
		setAddresses();
		return null;
	}
	public void setAddresses(){
		
		System.debug('In setAddresses module');
		System.debug('Contact: ' + mailRec.Contact__c);
		//based on selected contact
		if(mailRec.Contact__c!=null)
		{
			listAddresses = [Select Id, Name, Address_Type__c, 	Street_Address__c, 	City__c, State__c, Postal_Code__c, Country__c, Address_Label_Block__c 
							 from Addresses__c where Contact__c =: mailRec.Contact__c];
		}
		else
		{
			listAddresses = [Select Id, Name, Address_Type__c, 	Street_Address__c, 	City__c, State__c, Postal_Code__c, Country__c, Address_Label_Block__c 
							 from Addresses__c where Lead__c =: mailRec.Lead__c];
		}
		
		//add a new row of radio button
		rows =  new List<Row>();
		for(Addresses__c add: listAddresses)
		{
			rows.add(new Row(add.Id, add.Address_Label_Block__c, add));
			System.debug('Adding ' + add.Id);
		}
		showAdd = true;
	}
	
	public PageReference Save()
	{
		Boolean hasValidationError = false;
		//Validate the Delivery Method
		if (isLead == false)
		{
			if (cont.HasOptedOutOfEmail == true && (mailRec.Delivery_Method__c == 'Email' || mailRec.Delivery_Method__c == 'Mail and Email' ) )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'This recipient has requested not to be contacted via email.  Please select a valid delivery method.' ); 
				ApexPages.addMessage(myMsg);
				hasValidationError = true;
			}

			if (cont.Do_Not_Mail__c == true && (mailRec.Delivery_Method__c == 'Mail' || mailRec.Delivery_Method__c == 'Mail and Email' ) )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'This recipient has requested not to be contacted via mail.  Please select a valid delivery method.' ); 
				ApexPages.addMessage(myMsg);
				hasValidationError = true;
			}
			if (cont.HasOptedOutOfEmail == true && cont.Do_Not_Mail__c == true && (mailRec.Delivery_Method__c == 'Email' || mailRec.Delivery_Method__c == 'Mail' || mailRec.Delivery_Method__c == 'Mail and Email' ) )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'This recipient has requested not to be contacted via mail or email.  Please select delivery type = blank (--None--).' ); 
				ApexPages.addMessage(myMsg);
				hasValidationError = true;
			}
		}

		if (isLead == true)
		{
			if (ld.HasOptedOutOfEmail == true && (mailRec.Delivery_Method__c == 'Email' || mailRec.Delivery_Method__c == 'Mail and Email' ) )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'This recipient has requested not to be contacted via email.  Please select a valid delivery method.' ); 
				ApexPages.addMessage(myMsg);
				hasValidationError = true;
			}

			if (ld.Do_Not_Mail__c == true && (mailRec.Delivery_Method__c == 'Mail' || mailRec.Delivery_Method__c == 'Mail and Email' ) )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'This recipient has requested not to be contacted via mail.  Please select a valid delivery method.' ); 
				ApexPages.addMessage(myMsg);
				hasValidationError = true;
			}
			if (ld.HasOptedOutOfEmail == true && ld.Do_Not_Mail__c == true && (mailRec.Delivery_Method__c == 'Email' || mailRec.Delivery_Method__c == 'Mail' || mailRec.Delivery_Method__c == 'Mail and Email' ) )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'This recipient has requested not to be contacted via mail or email.  Please select delivery type = blank (--None--).' ); 
				ApexPages.addMessage(myMsg);
				hasValidationError = true;
			}
		}


		if (hasValidationError)
			return null;

		//look for selected address

		try{
			System.debug('upsert Id ' + mailRec.id);
			if(mailRec.Delivery_Method__c=='None')
				mailRec.Delivery_Method__c = null;
			upsert mailRec;

			String targetUrl = '/' + mailRec.id;
			map<String, String> params = ApexPages.currentPage().getParameters();
			if (params.containsKey('saveURL')) targetUrl = params.get('saveURL');
			return new PageReference(targetUrl);
		}
		catch (Exception e)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  e.getMessage() ); 
			ApexPages.addMessage(myMsg);
		}
		
		
		return null;
	}

	//class representing radiobuttons
	public class Row {
		public String Value { get; set; }
		public String Label { get; set; }
		public Addresses__c add {get;set;}
		public Boolean isChecked { get; set; }
		
		public Row(String Value, String Label, Addresses__c address) {
			this.Value = Value;
			this.Label = Label;
			this.add = address;
		} 
	}

}