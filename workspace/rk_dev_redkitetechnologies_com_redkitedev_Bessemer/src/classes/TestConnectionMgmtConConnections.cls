@isTest
private class TestConnectionMgmtConConnections {
	
	static testMethod void  ConnectionMgmtContact_Test1() 
    {
        CreateTestData() ;
        Connections__c con = [Select id , Contact_C1__c from Connections__c Limit 1] ;
        //Contact ct = [select Id from Contact Limit 1]; 
        
        PageReference pageRef = Page.ConnectionMgmt_ContactConnections;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
        ApexPages.currentPage().getParameters().put('Id', con.Contact_C1__c);
        Test.startTest() ;
        ConnectionMgmt_ContactConnections  myCon = new ConnectionMgmt_ContactConnections ();         
        myCon.init();
        myCon.ActionType = 'Contact' ;
        myCon.getPanelVisibility () ;
        
        myCon.SaveAndClose () ;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        System.assertEquals(2 , msgs.size() ) ;
        TestDataCreator_Utils.Clear();
        List<Contact> ctList = TestDataCreator_Utils.createSObjectList('Contact' , true , 1); 
        insert ctList ;
        MyCon.ConnectionObjRel.Contact_C1__c = con.Contact_C1__c ;
        myCon.ConnectionObj.Contact_C2__c = ctList[0].id;
        myCon.ConnectionObj.Role_on_Relationship__c = 'Other' ;
        myCon.SaveAndClose () ;
        Test.stopTest();
  	}
  	static testMethod void  ConnectionMgmtContact_Test2() 
    {
        CreateTestData() ;
        Connections__c con = [Select id , Contact_C1__c from Connections__c Limit 1] ;
        Contact ct = [select Id from Contact Limit 1]; 
        
        PageReference pageRef = Page.ConnectionMgmt_ContactConnections;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
        ApexPages.currentPage().getParameters().put('Id', ct.Id);
        Test.startTest() ;
        ConnectionMgmt_ContactConnections  myCon = new ConnectionMgmt_ContactConnections ();         
        myCon.init();
        myCon.getPanelVisibility () ;
       // myCon.ContactList[0].Role_on_Relationship__c = 'Primary';
        //myCon.SaveRoleChange();
        myCon.SaveAndClose () ;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        System.assertEquals(2 , msgs.size() ) ;
        TestDataCreator_Utils.Clear();
        List<Account> acctList = TestDataCreator_Utils.createSObjectList('Account' , true , 1); 
        insert acctList ;
        MyCon.ConnectionObj.Contact_C1__c = con.Contact_C1__c ;
        myCon.ConnectionObj.Relationship_R1__c = acctList[0].id;
        myCon.ConnectionObj.Role_on_Relationship__c = 'Other' ;
        myCon.SaveAndNew () ;
        myCon.Cancel () ;
        Test.stopTest();
  	}
  	static testMethod void  ConnectionMgmtContact_Test3() 
    {
        CreateTestData() ;
        Connections__c con = [Select id , Contact_C1__c from Connections__c Limit 1] ;
        Contact ct = [select Id from Contact Limit 1]; 
        
        PageReference pageRef = Page.ConnectionMgmt_ContactConnections;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
        ApexPages.currentPage().getParameters().put('Id', ct.Id);
        Test.startTest() ;
        ConnectionMgmt_ContactConnections  myCon = new ConnectionMgmt_ContactConnections ();         
        myCon.init();
        myCon.FilterString = 'ClientContact' ;
        myCon.FilterList();
       // myCon.FilterString = 'NonClientContact' ;
       // myCon.FilterList();
        myCon.FilterString = 'ClientRelationships' ;
        myCon.FilterList();
        myCon.FilterString = 'NonClientRelationships' ;
        myCon.FilterList();
        myCon.FilterString = 'Entity' ;
        myCon.FilterList();
        Test.stopTest();
    }

  	private static void CreateTestData()
    {

 
	    GlobalSettings__c gs = new GlobalSettings__c();
	    gs.Contact_S_Number_Seq__c = 1111;
	    gs.Relationship_Number_Seq__c  = 1111;
	    gs.Address_Number_Seq__c = 2222;
	    insert gs;

	    ContactFieldList__c ctFld = new ContactFieldList__c();
	    ctFld.Name = 'DK_Com_LastName' ;
	    ctFld.SectionName__c = 'Communication Details' ;
	    ctFLd.SectionOrder__c = 1; 
	    ctFld.SortOrder__c = 103;
	    ctFld.Field__c = 'Email' ;
	    ctFld.Platform__c = 'Desktop';
	    ctFld.Field_Id__c = '1';
	    ctFLd.PageName__c = 'Contact Prospect' ;
	    insert ctFld ;
	    ctFld = new ContactFieldList__c();
	    ctFld.Name = 'DK_Com_FirstName' ;
	    ctFld.SectionName__c = 'Communication Details' ;
	    ctFLd.SectionOrder__c = 1; 
	    ctFld.SortOrder__c = 104;
	    ctFld.Field__c = 'Email' ;
	    ctFld.Platform__c = 'Desktop';
	    ctFld.Field_Id__c = '2';
	    ctFLd.PageName__c = 'Contact Prospect' ;
	    insert ctFld ;
	    ctFld = new ContactFieldList__c();
	    ctFld.Name = 'DK_Com_BLANK' ;
	    ctFld.SectionName__c = 'Communication Details' ;
	    ctFld.SectionOrder__c = 2; 
	    ctFld.SortOrder__c = 303;
	    ctFld.Field__c = 'BlankSectionItem' ;
	    ctFld.Platform__c = 'Desktop';
	    ctFld.Field_Id__c = '3';
	    ctFLd.PageName__c = 'Contact Prospect' ;
	    insert ctFld ;
	    ctFld = new ContactFieldList__c();
	    ctFld.Name = 'DK_Com_Email' ;
	    ctFld.SectionName__c = 'Communication Details' ;
	    ctFld.SectionOrder__c = 2; 
	    ctFld.SortOrder__c = 303;
	    ctFld.Field__c = 'Email' ;
	    ctFld.Platform__c = 'Desktop';
	    ctFld.Field_Id__c = '4';
	    ctFLd.PageName__c = 'Contact Prospect' ;
	    insert ctFld ;


	    ContactEditLayouts__c  ctLayouts = new ContactEditLayouts__c ();
	    ctLayouts.Name = 'Prospect' ;
	    ctLayouts.PageName__c = 'Contact Prospect' ;
	    insert ctLayouts;

	    List<AddressConnectionMap__c> acMapList = new List<AddressConnectionMap__c> ();
	    AddressConnectionMap__c acMap = new AddressConnectionMap__c ();
	    acMap.Name = 'AllContacts1' ;
	    acMap.FieldName__c = 'Relationship2__r.Relationship_Number__c'  ;
	    acMap.Sort_Order__c  = 1;
	    acMap.Type__c  = 'AllContacts' ;
	    acMapList.add(acMap);
	    acMap = new AddressConnectionMap__c ();
	    acMap.Name = 'AllRelationships1' ;
	    acMap.FieldName__c = 'Relationship2__r.Relationship_Number__c'  ;
	    acMap.Sort_Order__c  = 1;
	    acMap.Type__c  ='AllRelationships' ;
	    acMapList.add(acMap);
	    acMap = new AddressConnectionMap__c ();
	    acMap.Name = 'Business1' ;
	    acMap.FieldName__c = 'Contact_C1__r.MailingCity'  ;
	    acMap.Sort_Order__c  = 1;
	    acMap.Type__c  = 'Business' ;
	    acMapList.add(acMap);
	    acMap = new AddressConnectionMap__c ();
	    acMap.Name = 'ContactConnection1' ;
	    acMap.FieldName__c = 'Contact_C2__r.MailingCity'  ;
	    acMap.Sort_Order__c  = 1;
	    acMap.Type__c  = 'ContactConnection' ;
	    acMapList.add(acMap);
	    acMap = new AddressConnectionMap__c ();
	    acMap.Name = 'NonClientContact1' ;
	    acMap.FieldName__c = 'Contact_C1__r.Occupation__c'  ;
	    acMap.Sort_Order__c  = 1;
	    acMap.Type__c =  'NonClientContact' ;
	    acMapList.add(acMap);
	    acMap = new AddressConnectionMap__c ();
	    acMap.Name = 'Trusts1' ;
	    acMap.FieldName__c = 'Relationship2__r.Market_Value__c'  ;
	    acMap.Sort_Order__c  = 1;
	    acMap.Type__c = 'Trusts' ;
	    acMapList.add(acMap);
	    insert acMapList;
	    
	    map<String, set<String>> ExcludeMap = new map<String, set<String>> ();
	    ExcludeMap = TestDataCreator_Utils.ExcludedFields ;
	    set<string> excludeFld = new set<string> ();
	   
	    excludeFld.add('Relationship2__c') ;
	    excludeFld.add('Contact_C2__c') ;
	   
	    ExcludeMap.put('Connections__c', excludeFld);
	    
	    
	    TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;
	    
	    List<Connections__c> connectionList = TestDataCreator_Utils.createSObjectList('Connections__c' , True , 1); 
	    insert connectionList;

   

	}
	
}