/*
	
	 Authors :  David Brandenburg, Don Koppel
	 Created Date: 2014-04-30
	 Last Modified: 2014-07-23
	 
	 Purpose:  Trigger handler for the trigger called ConnectionTrig

*/
public with sharing class ConnectionTrigHand
{
	private Map<String, String> cachedRecordTypeByName;

	public void onBeforeInsert( List<Connections__c> newList )
	{
		checkNewPrimaryDecisionMaker( newList );
	}

	public void onAfterInsert( List<Connections__c> newList )
	{
		insertInverseRecord( newList );
		updateDataSyncLog( newList, 'I' );
		stageRecordsToSynctoEDB( newList, 'I' );
		updatePrimaryDecisionMaker( newList );
		setRecursiveCheck( newList );
		checkOtherRT( newList );
	}

	public void onAfterUpdate( List<Connections__c> newList, Map<Id, Connections__c> oldMap )
	{
		updateInverseRecord( newList, oldMap );
		updateDataSyncLog( newList, 'U' );
		updatePrimaryDecisionMaker( newList );
		setRecursiveCheck( newList );
	}

	public void onBeforeUpdate( List<Connections__c> newList, Map<Id, Connections__c> oldMap )
	{
		stageUpdatesToSynctoEDB( newList, oldMap );
		checkRoleChange( newList, oldMap );
	}

	public void onBeforeDelete( List<Connections__c> oldList )
	{
		checkDeletedRecords( oldList );
	}

	public void onAfterDelete( List<Connections__c> oldList )
	{
		deleteInverseRecord( oldList );
		updateDataSyncLog( oldList, 'D' );
		stageRecordsToSynctoEDB( oldList, 'D' );
		removeRelationbshipPDM( oldList );
	}

	//*** method handles the inserted records ***/
	private void insertInverseRecord( List<Connections__c> newList )
	{
		if( ConnectionTrigHandHelper.hasAlreadyCreatedInverse() )
			return;

		List<Connections__c> newConnectionsList = new List<Connections__c>();
		//Get mapping from custom setting and break into 2 maps
		Map<String, Map<String, String>> inverseMap = getInverseMapping();
		Map<String, String> inverseMapRelationships = inverseMap.get( 'Relationship' );
		Map<String, String> inverseMapContacts = inverseMap.get( 'Contact' );

		Map<String, RecordType> rtMap = new Map<String, RecordType>( [ SELECT Id, Name FROM RecordType WHERE SObjectType = 'Connections__c' ] );
		for( Connections__c c : newList )
		{
			//Only process records which have not been already inserted.
			//Check to see if the Role is in the Relationship mapping object. If found create a new record
			Connections__c newConnection = new Connections__c();
			newConnection.RecordTypeId = c.RecordTypeId;
			if( rtMap.get( c.RecordTypeId ).Name == 'R:R' )
			{
				newConnection.Relationship_R1__c = c.Relationship2__c;
				newConnection.Relationship2__c = c.Relationship_R1__c;
				if( inverseMapRelationships.containsKey( c.Role_on_Relationship__c ) )
					newConnection.Role_on_Relationship__c = inverseMapRelationships.get( c.Role_on_Relationship__c );
				else
					newConnection.Role_on_Relationship__c = c.Role_on_Relationship__c;

				newConnectionsList.add( newConnection );
			}
			else if( rtMap.get( c.RecordTypeId ).Name == 'C:C' )
			{
				newConnection.Contact_C1__c = c.Contact_C2__c;
				newConnection.Contact_C2__c  = c.Contact_C1__c;
				if( inverseMapContacts.containsKey( c.Role_on_Relationship__c ) )
					newConnection.Role_on_Relationship__c = inverseMapContacts.get( c.Role_on_Relationship__c );
				else
					newConnection.Role_on_Relationship__c = c.Role_on_Relationship__c;

				newConnectionsList.add( newConnection );
			}
		}

		//If the list contains records insert into database
		if( !newConnectionsList.isEmpty() )
		{
			//Set the recursive flag to true.  Prevents records from being processed a second time
			ConnectionTrigHandHelper.setAlreadyCreatedInverse();
			insert newConnectionsList;
		}
	}

	private void updateInverseRecord( List<Connections__c> newList,  Map<Id, Connections__c> oldMap )
	{
		if( ConnectionTrigHandHelper.hasAlreadyCreatedInverse() )
			return;

		List<Connections__c> updateConnectionsList = new List<Connections__c>() ;
		//Get mapping from custom setting and break into 2 maps
		Map<String, Map<String, String>> inverseMap = getInverseMapping();
		Map<String, String> inverseMapRelationships = inverseMap.get( 'Relationship' );
		Map<String, String> inverseMapContacts = inverseMap.get( 'Contact' );

		Map<String, RecordType> rtMap = new Map<String, RecordType>( [ SELECT Id, Name FROM RecordType WHERE SObjectType = 'Connections__c' ] );
		Set<String> affectedSet = new Set<String>();
		Map<String, String> changedConnection = new Map<String, String>();

		//get a map of the inverse Connections and the affected inverse Connectsions
		for( Connections__c c : newList )
		{
			//Only process record where the role changed
			if( c.Role_on_Relationship__c != oldMap.get( c.Id ).Role_on_Relationship__c )
			{
				if( rtMap.get( c.RecordTypeId ).Name == 'R:R' )
				{
					affectedSet.add( c.Relationship2__c );
					changedConnection.put( c.Relationship_R1__c, c.Role_on_Relationship__c );
				}
				if( rtMap.get( c.RecordTypeId ).Name == 'C:C' )
				{
					affectedSet.add( c.Contact_C2__c );
					changedConnection.put( c.Contact_C1__c, c.Role_on_Relationship__c );
				}
			}
		}

		//Query the inverse Connections
		List<Connections__c> inverseConnections = [ SELECT Id, RecordTypeId, Relationship_R1__c, Relationship2__c, Contact_C1__c, Contact_C2__c, Role_on_Relationship__c
													FROM Connections__c
													WHERE Contact_C1__c IN :affectedSet OR Relationship_R1__c = :affectedSet ];

		//Loop through the inverse records update the role based on the mapping
		for( Connections__c c : inverseConnections )
		{
			if( inverseMapRelationships.containsKey( changedConnection.get( c.Relationship2__c ) ) && rtMap.get( c.RecordTypeId ).Name == 'R:R' )
			{
				c.Role_on_Relationship__c = inverseMapRelationships.get( changedConnection.get( c.Relationship2__c ) );
				updateConnectionsList.add( c );
			}
			if( inverseMapContacts.containsKey( changedConnection.get( c.Contact_C2__c ) ) && rtMap.get( c.RecordTypeId ).Name == 'C:C' )
			{
				c.Role_on_Relationship__c = inverseMapContacts.get( changedConnection.get( c.Contact_C2__c ) );
				updateConnectionsList.add( c );
			}
		}
		//If the list contains records update the records
		if( updateConnectionsList.size() > 0 )
		{
			//Set the recursive flag to true.  Prevents records from being processed a second time
			ConnectionTrigHandHelper.setAlreadyCreatedInverse();
			update updateConnectionsList;
		}
	}

	//*** Process the delete reords ***/
	private void deleteInverseRecord( List<Connections__c> oldList )
	{
		if( ConnectionTrigHandHelper.hasAlreadyCreatedInverse() )
			return;

		Map<String, RecordType> rtMap = new Map<String, RecordType>( [ SELECT Id, Name FROM RecordType WHERE SObjectType = 'Connections__c' ] );
		Set<String> affectedSet = new Set<String>();
		Map<String, String> relationshipMapping = new Map<String, String>();

		// Get a set of the inverse Connections
		for( Connections__c c : oldList )
		{
			if( rtMap.get( c.RecordTypeId ).Name == 'R:R' )
			{
				affectedSet.add( c.Relationship2__c );
				relationshipMapping.put( c.Relationship2__c, c.Relationship_R1__c );
			}
			if( rtMap.get( c.RecordTypeId ).Name == 'C:C' )
			{
				affectedSet.add(c.Contact_C2__c);
				relationshipMapping.put( c.Contact_C2__c, c.Contact_C1__c );
			}
		}

		//Query the inverse Connections
		List<Connections__c> deleteConnectionsList = [ SELECT Id, RecordTypeId, Relationship_R1__c, Relationship2__c, Contact_C1__c, Contact_C2__c, Role_on_Relationship__c
													FROM Connections__c
													WHERE Contact_C1__c IN :affectedSet OR Relationship_R1__c = :affectedSet ];
		
		//If the list contains records deletethe records
		List<Connections__c> deleteRecords = new List<Connections__c> ();
		if( deleteConnectionsList.size() > 0 )
		{
			for( Connections__c c : deleteConnectionsList )
			{
				if( rtMap.get( c.RecordTypeId ).Name == 'C:C' )
				{
					if( relationshipMapping.get( c.Contact_C1__c ) == c.Contact_C2__c )
						deleteRecords.add( c );
				}
				if( rtMap.get( c.RecordTypeId ).Name == 'R:R' )
				{
					if( relationshipMapping.get( c.Relationship_R1__c ) == c.Relationship2__c )
						deleteRecords.add( c );
				}
			}
			//Set the recursive flag to true.  Prevents records from being processed a second time
			ConnectionTrigHandHelper.setAlreadyCreatedInverse();
			delete deleteRecords;
		}
	}

	//**  Method calls the custom setting called Inverse_Connection_Role__c and returns 2 maps by RecordType  **/
	private Map<String, Map<String, String>> getInverseMapping()
	{
		//Get mapping from custom setting and break into 2 maps
		Map<String, Inverse_Connection_Role__c> inverseMap = Inverse_Connection_Role__c.getAll();
		Map<String, String> inverseMapRelationships = new Map<String, String>();
		Map<String, String> inverseMapContacts = new Map<String, String>();

		//Loop through custom setting map and population Relationship and Contact maps
		for( String key : inverseMap.keySet() )
		{
			if( inverseMap.get(key).Connection_Record_Type__c == 'R:R' )
				inverseMapRelationships.put( inverseMap.get( key ).Role__c, inverseMap.get( key ).Inverse_Role__c );
			if( inverseMap.get(key).Connection_Record_Type__c == 'C:C' )
				inverseMapContacts.put( inverseMap.get( key ).Role__c, inverseMap.get( key ).Inverse_Role__c );
		}
		//Create a return object and return the 2 maps
		Map<String, Map<String, String>> retVal = new Map<String, Map<String, String>>();
		retVal.put( 'Relationship', inverseMapRelationships );
		retVal.put( 'Contact', inverseMapContacts );

		return retVal;
	}

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs Relationship Contacts EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.
	private void updateDataSyncLog( List<Connections__c> recordList, String operation )
	{
		//records should only be added to the data sync log if the current user is the Bessemer Integration user
		if( UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c )
		{
			List<Data_Sync_Log__c> syncList = new List<Data_Sync_Log__c>();
			for( Connections__c connection : recordList )
			{
				if( connection.EDB_Entity_Id__c <> null )
				{
					Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
					syncRecord.EDB_Entity_Id__c = connection.EDB_Entity_Id__c;
					syncRecord.Entity__c = 'CUST_RELTNSHP_OPTN';
					syncRecord.Action__c = Operation;
					syncList.Add( syncRecord );
				}
			}

			if( synclist.size() > 0 )
				insert syncList;
		}
	}

	//Method to prevent Changing the Role from "Primary Decision Maker"
	private void checkRoleChange( List<Connections__c> newList, Map<Id, Connections__c> oldMap )
	{
		//Do not process if the current user is the integration user
		if( UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c )
			return;

		for( Connections__c c : newList )
		{
			if( !RecursiveFlagHandler.alreadyRunning( c.Id ) && oldMap.get( c.Id ).Role_on_Relationship__c == 'Primary Decision Maker' && c.Override__c == false )
				c.addError( 'The Role of the Primary Decision Maker for the Relationship cannot be changed' );
		}

		for( Connections__c c : newList )
			c.Override__c = false;
	}

	//Method check to see if a Primary Decision Maker is already attached to the relationship
	private void checkNewPrimaryDecisionMaker( List<Connections__c> newList )
	{
		String recTypeId = [ SELECT Id FROM RecordType WHERE Name = 'R:C' AND SObjectType = 'Connections__c' ].Id;
		//Get Relationship ID for R:C recordTypes

		Set<String> accountSet = new Set<String>();
		for( Connections__c c : newList )
		{
			if( c.RecordTypeId == recTypeId && c.Role_on_Relationship__c == 'Primary Decision Maker' )
				accountSet.add( c.Relationship_R1__c );
		}

/*
		List<Account> accountList = [ SELECT Id, Primary_Decision_Maker__c FROM Account WHERE Id IN :accountSet AND Primary_Decision_Maker__c != null ];
		accountSet = new Set<String>();
		for( Account a: accountList )
			accountSet.add( a.Id );
*/
		List<Connections__c> connList = [ SELECT Id, Relationship_R1__c FROM Connections__c WHERE Relationship_R1__c IN :accountSet AND Role_on_Relationship__c = 'Primary Decision Maker' ];
		accountSet = new Set<String>();
		for( Connections__c conn: connList )
			accountSet.add( conn.Relationship_R1__c );
			
		for( Connections__c c : newList )
		{
			if( c.RecordTypeId == recTypeId && c.Role_on_Relationship__c == 'Primary Decision Maker' )
			{
				if( accountSet.contains( c.Relationship_R1__c ) )
					c.addError( 'The Role of the Primary Decision Maker alreay exists for this Relationship' );
			}
		}
	}

	//Method to Update Relationship Primary Decision Maker
	private void updatePrimaryDecisionMaker( List<Connections__c> newList )
	{
		//Check User Permissions 
		if( !Schema.SObjectType.Account.fields.Primary_Decision_Maker__c.isAccessible() )
		{
			for( Connections__c c : newList )
			{
				c.addError( 'User does not have permission to update Relationship record' );
			}
			return;
		}

		//Do not process if the current user is the integration user
		//if( UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
		//	return;
		String recTypeId = [ SELECT Id FROM RecordType WHERE Name = 'R:C' AND SObjectType = 'Connections__c' ].Id;
		//Get Relationship ID for R:C recordTypes
		Map<String, String> relationshipSet = new Map<String, String>();
		Set<String> contactSet = new Set<String>();
		for( Connections__c c : newList )
		{
			if( c.RecordTypeId == recTypeId && c.Role_on_Relationship__c == 'Primary Decision Maker' )
			{
				contactSet.add( c.Contact_C1__c );
				relationshipSet.put( c.Relationship_R1__c, c.Contact_C1__c );  
			}
		}

		List<Account> accountList = [ SELECT Id, Primary_Decision_Maker__c FROM Account WHERE Id IN :relationshipSet.keySet() ];
		for( Account a : accountList )
			a.Primary_Decision_Maker__c = relationshipSet.get( a.Id );

		update accountList;
	}

	//Method to prevent R:C recordtypes from being deleted if it is a primary relationship 
	private void checkDeletedRecords( List<Connections__c> oldList )
	{
		//Do not process if the current user is the integration user
		if( UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c )
			return;

		String recTypeId = [ SELECT Id FROM RecordType WHERE Name = 'R:C' AND SObjectType = 'Connections__c' ].Id;
		Set<String> recordSet = new Set<String>();
		for( Connections__c c : oldList)
		{
			if( c.RecordTypeId == recTypeId )
				recordSet.add( c.Id );
		}
		
		Map<String, Connections__c> connectionMap = new Map<String,Connections__c> ( [ SELECT Id, Relationship_R1__c, Contact_C1__r.AccountId FROM Connections__c WHERE Id IN :recordSet AND Role_On_Relationship__c IN ('Primary Decision Maker', 'Decision Maker') ] );
		for( Connections__c c : oldList )
		{
			if( connectionMap.containsKey( c.Id ) )
			{
				if( connectionMap.get( c.Id ).Relationship_R1__c == connectionMap.get( c.Id ).Contact_C1__r.AccountId )
					c.addError( 'You cannot remove the contact\'s connection for the primary relationship if the role is Primary Decision Maker or Decision Maker' );
			}
		}
	}

	private void removeRelationbshipPDM( List<Connections__c> oldList )
	{
		String recTypeId = [ SELECT Id FROM RecordType WHERE Name = 'R:C' AND SObjectType = 'Connections__c' ].Id;
		Set<String> recordSet = new Set<String>();
		for( Connections__c c : oldList )
		{
			if( c.RecordTypeId == recTypeId && c.Role_on_Relationship__c == 'Primary Decision Maker' )
				recordSet.add( c.Relationship_R1__c );
		}

		List<Account> accountList = [ SELECT Id, Primary_Decision_Maker__c FROM Account WHERE Id IN :recordSet ];
		for( Account a : accountList )
			a.Primary_Decision_Maker__c = null;

		update accountList;
	}

	private void setRecursiveCheck( List<Connections__c> newList )
	{
		List<String> idList = new List<String>();
		for( Connections__c c: newList )
			idList.add( c.Id );

		RecursiveFlagHandler.setRunning( idList );
	}

	private void checkOtherRT( List<Connections__c> newList )
	{
		Set<String> professionalRoles = new Set<String>();
		professionalRoles.add( 'Accountant' );
		professionalRoles.add( 'Lawyer' );
		professionalRoles.add( 'Tax Attorney' );

		//Need to Cache this Query
		String rtID = getRecordTypesByName( 'R:C' );
		Set<String> contactSet = new Set<String>();
		for( Connections__c c: newList )
		{
			if( c.RecordTypeId == rtID && professionalRoles.contains( c.Role_on_Relationship__c ) )
				contactSet.add( c.Contact_C1__c );
		}

		if( contactSet.size() > 0 )
		{
			List<RecordType> rtList = [ SELECT Id, Name FROM RecordType WHERE SObjectType = 'Contact' ];
			Map<String, String> contactRT = new Map<String, String>();
			for( RecordType rt :rtList )
				contactRT.put( rt.Name, rt.Id );

			List<Contact> ctList = [ SELECT RecordTypeId FROM Contact WHERE Id IN :contactSet AND RecordTypeId = :contactRT.get( 'Other' ) ];
			for( Contact c : ctList )
				c.RecordTypeId = contactRT.get( 'Professional/Intermediary' );
			update ctList;
		}
	}
	private String getRecordTypesByName( String rtName )
	{
		if( cachedRecordTypeByName == null )
		{
			cachedRecordTypeByName = new Map<String, String> ();
			List<RecordType> rtList = [ SELECT Id, Name FROM RecordType WHERE SObjectType = 'Connections__c' ];
			for( RecordType rt :rtList )
				cachedRecordTypeByName.put( rt.Name, rt.Id );
		}

		return cachedRecordTypeByName.get( rtName );
	}

	private void stageRecordsToSynctoEDB( List<Connections__c> conList, String action )
	{
		if( UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c )
		{
			String recTypeId = [ SELECT Id FROM RecordType WHERE Name = 'R:C' AND SObjectType = 'Connections__c'].Id;
			List<Connection_Sync__c> csList = new List<Connection_Sync__c>();
			for( Connections__c con : conList )
			{
				if( con.RecordTypeId == recTypeId )
				{
					Connection_Sync__c cs = new Connection_Sync__c();
					cs.Relationship_R1__c = con.Relationship_R1__c;
					cs.Contact_C1__c = con.Contact_C1__c;
					cs.Role_on_Relationship__c = con.Role_on_Relationship__c;
					cs.Action__c = action;

					csList.add( cs );
				}
			}

			if( csList.size() > 0 )
				insert csList;
		}
	}

	private void stageUpdatesToSynctoEDB( List<Connections__c> newList, Map<Id, Connections__c> oldMap )
	{
		if( UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c )
		{
			String recTypeId = [ SELECT Id FROM RecordType WHERE Name = 'R:C' AND SObjectType = 'Connections__c'].Id;
			List<Connection_Sync__c> csList = new List<Connection_Sync__c>();
			for( Connections__c con : newList )
			{
				if( con.RecordTypeId == recTypeId )
				{
					Connection_Sync__c cs = new Connection_Sync__c();
					cs.Relationship_R1__c = con.Relationship_R1__c;
					cs.Contact_C1__c = con.Contact_C1__c;
					cs.Role_on_Relationship__c = con.Role_on_Relationship__c;

					//system.debug('old role = ' + oldMap.get(con.Id).Role_on_Relationship__c + ' new role = ' + con.Role_on_Relationship__c)	;
					if( con.Role_on_Relationship__c != oldMap.get( con.Id ).Role_on_Relationship__c )
					{
						//updates should be recorded as Delete/Insert - remove
						Connection_Sync__c csDel = new Connection_Sync__c();
						csDel.Relationship_R1__c = con.Relationship_R1__c;
						csDel.Contact_C1__c = con.Contact_C1__c;
						csDel.Role_on_Relationship__c = oldMap.get( con.Id ).Role_on_Relationship__c;
						csDel.Action__c = 'D';
						//system.debug('old role = ' + oldMap.get(con.Id).Role_on_Relationship__c + ' new role = ' + con.Role_on_Relationship__c)	;
						csList.add( csDel );

						//Deletes should be created first, followed by adds
						cs.Action__c = 'I';
						csList.add( cs );
					}
				}
			}

			if( csList.size() > 0 )
				insert csList;
		}
	}
}