@isTest
public with sharing class TestFeedItemTrigHand {
	 static testMethod void FeedItemTrigHandtest(){
	 	GlobalSettings__c gs = TestDataFactory.createGlobalSetting();
	 		insert gs;
	 		system.assert(gs.id != null,'GlobalSettings record created');
		Chatter_Compliance_Field_Mapping__c ccfm = TestDataFactory.createComplianceFieldMapping();
	 		insert ccfm;
	 		system.assert(ccfm.id != null,'Chatter Compliance Field Mapping record created');	
	 	Account acc = TestDataFactory.createTestAccount();
	 		acc.Relationship_Number__c = '123456';
	 		insert acc;	
	 		system.assert(acc.id != null,'Account record created');	
	 	FeedItem fi = TestDataFactory.createFeedItem(acc.id);
	 		insert fi;
	 		system.assert(fi.id != null,'FeedItem record created');	
	 }
}