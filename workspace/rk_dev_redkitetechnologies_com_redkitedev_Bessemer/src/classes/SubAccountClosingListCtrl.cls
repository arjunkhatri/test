public with sharing class SubAccountClosingListCtrl 
{
	private list<sObject> selectedList;
	public string returnUrl {get;set;}
	public string returnBaseUrl {get;set;}
	public boolean hasError {get;set;}
	public string errorMsg {get;set;}
	public string returnPage {get;set;}

	
	public SubAccountClosingListCtrl (ApexPages.StandardSetController stdSetController)
	{
		selectedList  = stdSetController.getSelected();
		
	}
	public Pagereference init ()
	{
		hasError = false;
		returnUrl = ApexPages.currentPage().getParameters().get('retURL') ;
		returnBaseUrl  = String.ValueOf(URL.getSalesforceBaseUrl().toExternalForm()  );

		Profile profile = [SELECT Name FROM Profile where id = :userinfo.getProfileId()];
		if (profile.Name == 'Bessemer Executive Admin' 
			|| profile.Name == 'Bessemer Client Advisor' 
			|| profile.Name == 'Bessemer Client Admin Services (CAS)' //)
			|| profile.Name == 'System Administrator' )
			{
				
				System.debug(selectedList);
				
				set<string> subAccountList = new set<string> ();
				for (sObject sObj : selectedList)
					subAccountList.add(String.ValueOf(sObj.get('id')));
				
				if (subAccountList.size() == 0)
				{
					 hasError = true;
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'There were no subaccounts selected')) ;
					return null;
				}
					
				List<SubAccounts__c> SubaccountNumbers = [select Relationship__c ,Name, SubAccount_Number_Unique__c from SubAccounts__c where id in :subAccountList ];
				string subAccountNumberString ='';
				string accountId = '';
				for (SubAccounts__c subAcct :SubaccountNumbers)
				{
					accountId = subAcct.Relationship__c;
					if (subAccountNumberString.length() == 0)
						subAccountNumberString = subAcct.Name;
					else
						subAccountNumberString += ',' + subAcct.Name;
				}
				
				string relationshipNumber = [select Relationship_Number__c from Account where id = :accountId].Relationship_Number__c;
				
				//Get the url from Global Settings
				string baseURL = GlobalSettings__c.getInstance().SubAccount_Closing_URL__c;
				if (baseURL == null || baseURL.length() == 0 )
				{
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'The endpoint is not configured in global settings.  Contact your system administrator')) ;
					return null;
				}

				//Http h = new http();
				returnPage = baseURL + '?subacct_no=' + subAccountNumberString + '&rel_no=' + relationshipNumber ;
				/*
	    		HttpRequest req = new HttpRequest();
			    req.setEndpoint(ENDPOINT);
			    req.setMethod('GET');
			    HttpResponse res = h.send(req);

	    		if (res.getStatus() == '200' || res.getStatus() == '201')
	    		{
	    			PageReference pgRef = new PageReference('/' + returnUrl ) ;
	    			pgRef.setRedirect(true);
					return pgRef;
	    		}
	    		else
	    		{
	    			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'The endpoint returned an error. Error code: "' + res.getStatus()  +'". Contact your system administrator')) ;
					hasError = true;
					errorMsg = res.getBody() ;
					return null;
	    		}

				
				PageReference pgRef = new PageReference(baseURL);
				//subacct_no=<<SubAccount_1>>%2c<<SubAccount_2>>%2c<<SubAccount_3>>&rel_no=<<Relationship_No>>
				pgRef.getParameters().put('subacct_no' , subAccountNumberString);
				pgRef.getParameters().put('rel_no' , relationshipNumber );
				pgRef.setRedirect(true);
				return pgRef;
				*/
			}
			else
			{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'The profile is not authorized to perform the closing process')) ;
			}
			return null;
	}
}