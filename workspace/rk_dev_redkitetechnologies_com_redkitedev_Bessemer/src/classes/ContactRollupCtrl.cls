public with sharing class ContactRollupCtrl
{
	public Account accountObj { get; set; }
	public String accountId { get; set; }  
	public String selectedContact { get; set; }
	public String platForm { get; set; }
	public static final String ROLEOTHER = 'Other';
	public static final String ROLEEntity = 'Entity';
	private ApexPages.StandardSetController setContactList;
	public List<ContactRollup> returnList{ get; set; }
	public List<ContactRollup> rollupList{ get; set; }
    //variables for pagination
    public Integer RecordSize { get{ return returnList.size(); } } // Total number of the records.
    public Integer PageCount { get{ return Math.ceil((Double)RecordSize / (Double)PageSize).intValue(); } } 
    public Boolean HasPrevious { get{ return PageNumber != 1; } }
    public Boolean HasNext { get{ return PageNumber != PageCount; } }
    public Boolean fullPage {get; set;}
    public Boolean showMore { get; set; }	
    public Integer PageNumber // Current page's page number.
    { 
        get;
        set
        {
            PageNumber = value;
            if(value <= 0)
            {
                PageNumber = 1;
            }
            else if(value > PageCount)
            {
                PageNumber = PageCount;
            }
        }
    }

    public Integer PageSize 
    { 
    	get{
    			if(ApexPages.currentPage().getParameters().get( 'showMore' ) == 'true' ){
    				return 50;
    			}
    			else{
    				return 5;
    			}
    	   }
    }
        	
	public ContactRollupCtrl( ApexPages.Standardcontroller controller )
	{
		accountId = controller.getId();
		accountObj = (Account)controller.getRecord();
		if( ApexPages.currentPage().getParameters().get( 'showMore' ) == 'true' )
			showMore = true;
		else
			showMore = false;
		GlobalContactSearch gcs = new GlobalContactSearch();
		platForm = gcs.checkPlatform();
		init();
		firstPage();
	}
	
	public void init()
	{
		returnList = new List<ContactRollup>();
		List<Connections__c> connectionList = [ SELECT Contact_C1__c, Role_on_Relationship__c
												 FROM Connections__c
												 WHERE Relationship_R1__c = :accountId
													AND Contact_C1__c != null];

		Map<String, String> contactMap = new Map<String, String>();
		Map<String, String> contactAccRoleMap = new Map<String, String>();
		for( Connections__c c : connectionList )
		{
			if(c.Role_on_Relationship__c != null && c.Role_on_Relationship__c != '' 
										 && c.Role_on_Relationship__c != ROLEOTHER && c.Role_on_Relationship__c != ROLEEntity){
				if( contactMap.containskey( c.Contact_C1__c ) )
				{
					String tmpRole = contactMap.get( c.Contact_C1__c );
					    tmpRole += ';' + c.Role_on_Relationship__c;
					    contactMap.put( c.Contact_C1__c, tmpRole );
				}
				else
					contactMap.put( c.Contact_C1__c, c.Role_on_Relationship__c );
			}
		}

		List<Account_Contact__c>  accountContactList = [ SELECT Contact__c, Role__c
														 FROM Account_Contact__c
														 WHERE Account__c IN 
															( SELECT Id FROM Accounts__c WHERE Relationship__c = :accountId ) ];
		for( Account_Contact__c c : accountContactList )
		{
			if(c.Role__c != null && c.Role__c != ''){
				String strRole = c.Role__c;
				if(strRole.contains(ROLEOTHER) || strRole.contains(ROLEEntity)){
					strRole = removeRole(strRole);
				}
				if( contactAccRoleMap.containskey( c.Contact__c ) )
				{
					String tmpRole = contactAccRoleMap.get( c.Contact__c );
					tmpRole += ';' + strRole;
					contactAccRoleMap.put(c.Contact__c, tmpRole);
				}
				else
				{	
					if(strRole != '')
						contactAccRoleMap.put( c.Contact__c, strRole );
				}
			}
		}
		if(!contactMap.isEmpty())
			contactMap = scopeMap(contactMap);
		if(!contactAccRoleMap.isEmpty())
			contactAccRoleMap = scopeMap(contactAccRoleMap);
			
		for( Contact ct : [ SELECT Id, RecordType.Name,LastName,Name 
									FROM Contact 
									WHERE Id IN :contactMap.keyset() 
									or Id IN :contactAccRoleMap.keyset() 
									ORDER BY RecordType.Name])
		{  
			//system.debug('>>>>>ct.RecordType.Name'+ct.RecordType.Name);
			String strRecType = String.valueOf(ct.RecordType.Name);
			returnList.add( new ContactRollup( ct, contactMap.get( ct.Id ) , contactAccRoleMap.get( ct.Id ), strRecType));
		}
		if(returnList != null && returnList.size() > 0)
			returnList.sort();			
	}

	//function removing role 'Other
	public string removeRole(String strRole){
		String finalRoles = '';
		if(strRole.contains(';')){
			for(String strRl:strRole.split(';')){
				if(strRl != ROLEOTHER || strRl != ROLEEntity)
					finalRoles += strRl+';';
			}
			finalRoles = finalRoles.substring(0,finalRoles.length() - 1);
		}
		return finalRoles;
	}

	public void deleteContact()
	{
		if( selectedContact != null )
		{
			List<Contact> ctList = [ SELECT Id FROM Contact WHERE Id = :selectedContact ];
			try
			{
				delete ctList;
			}
			catch( DMLException dmlEx ) {}
			init();
			firstPage();
		}
	}

	public Map<String,String>  scopeMap( Map<String,String> MapRoles )
	{
		//Sort the roles by Contact
		String tmpNewString = '';
		List<string> tmpSort;
		for( String key : MapRoles.keyset() )
		{
			tmpSort = new List<String>();
			tmpSort = MapRoles.get(key).split( ';');
			Set<String> tmpUnique = new Set<String>();
			tmpSort.sort();
			tmpNewString = '';
			for( String str : tmpSort )
			{
				if( !tmpUnique.contains( str ) )
				{
					tmpUnique.add( str );
					if( tmpNewString.length() == 0 )
						tmpNewString = str;
					else
						tmpNewString +=  ', ' + str ;
				}
			}
			MapRoles.put( key, tmpNewString );
		}
		return MapRoles;
	}

    public PageReference previousPage()
    {
        --PageNumber;
        return goToPage();
    }
    
    /**
     * Goes to the next page.
     */
    public PageReference nextPage()
    {
        ++PageNumber;
        return goToPage();
    }
    
    /**
     * Goes to the first page.
     */
    public PageReference firstPage()
    {
        PageNumber = 1;
        return goToPage();
    }
    
    /**
     * Goes to the last page.
     */
    public PageReference lastPage()
    {
        PageNumber = PageCount; 
        return goToPage();
    }
    
    /**
     * Refreshes the page.
     */
    public PageReference goToPage()
    {
        pagination();
        return null;
    }
    /**
     * Pages the forms.
     */
    private void pagination()
    {  
        rollupList = new List<ContactRollup>();
        if(RecordSize > 0)
        {
            for(Integer i = 0; i < PageSize; i++)
            {
                Integer j = (pageNumber - 1) * PageSize + i;
                if(j < RecordSize)
                {
                    rollupList.add(returnList[j]);
                }
            }
        }
    } 

	public class ContactRollup implements Comparable
	{ 
		public Contact contactObj { get; set; }
		public String roles { get; set; }
		public String rolesAcc { get; set; }
		public String strRecType { get; set; }
		
		public ContactRollup( Contact ct, String role ,String accRole , String strRcType)
		{
			contactObj = ct;
			roles = role;
			rolesAcc = accRole;
			strRecType = strRcType;
		}
		
	    public Integer compareTo(Object compareTo) 
	    {
	        ContactRollup conWrapper = (ContactRollup) compareTo;
	        if (strRecType == conWrapper.strRecType && roles == conWrapper.roles){
	        	if(roles == conWrapper.roles && contactObj.LastName == conWrapper.contactObj.LastName)
	        	 return 0;	        	
	        	if(roles == conWrapper.roles && contactObj.LastName > conWrapper.contactObj.LastName)
	        	 return 1;
	        	if(roles == conWrapper.roles && contactObj.LastName < conWrapper.contactObj.LastName)
	        	 return -1;
	       	}
	        if (strRecType == conWrapper.strRecType && roles < conWrapper.roles) return 1;
	        if (strRecType == conWrapper.strRecType && roles > conWrapper.roles) return -1;
	        if (strRecType > conWrapper.strRecType) return 1;
	        return -1;        
	    }		
    }
}