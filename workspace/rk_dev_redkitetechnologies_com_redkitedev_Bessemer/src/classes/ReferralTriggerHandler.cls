/**
* ReferralTriggerHandler : Class that contains code for the Referral Trigger
*/
public with sharing class ReferralTriggerHandler {
	public static set<Id> inverseReferralsInProcess;
	public static final string REFERREDBY = 'has been referred by';
	public static final string REFERRED = 'has referred';
	public map<id,String> mpConToName;
	public map<id,String> mpRelToName;
	static{
		inverseReferralsInProcess = new set<Id>();
	}
	public static Boolean isAfterInsExe = false;
	public ReferralTriggerHandler(){
	}

	// Trigger Methods ----------------------------------------------------
	public void onBeforeInsert(list<Referral__c> listNew){
		lookupReferredByAccount_Before(listNew);
	}
	public void onAfterInsert(list<Referral__c> listNew){
		inverseOnInsert_After(listNew);
		if(!isAfterInsExe){
			createPost(listNew);
		}
		isAfterInsExe = true;
	}
	
	public void onBeforeUpdate(map<Id,Referral__c> oldMap, list<Referral__c> listNew){
		lookupReferredByAccount_Before(listNew);
		inverseOnUpdate(oldMap, listNew);
	}
//	public void onAfterUpdate(map<Id,Referral__c> oldMap, list<Referral__c> listNew){
//	}
	
	public void onBeforeDelete(map<Id,Referral__c> oldMap){
		inverseOnDelete(oldMap);
	}
//	public void onAfterDelete(map<Id,Referral__c> oldMap){
//	}
	
	public void onAfterUndelete(list<Referral__c> listNew){
		inverseOnInsert_After(listNew);
	}

	// -----------------------------------------------------------------------------------------------------------
	// Custom business logic
	// When a referral is created, post a Chatter record for each Relationship and Contact involved
	public void createPost(list<Referral__c> listNew){
		Set<Id> setCont = new Set<Id>();
		Set<Id> setRel = new Set<Id>(); 
		mpConToName  = new map<id,String>();
		mpRelToName  = new map<id,String>();
		for(Referral__c refRec:listNew){
			if(refRec.Contact__c != null)
				setCont.add(refRec.Contact__c);
			if(refRec.Referred_By_Contact__c != null)
				setCont.add(refRec.Referred_By_Contact__c);		
			if(refRec.Relationship__c != null)			
				setRel.add(refRec.Relationship__c);
			if(refRec.Referred_By_Relationship__c != null)			
				setRel.add(refRec.Referred_By_Relationship__c);				
		}
		
		if(setCont != null && setCont.size() > 0){
			for(Contact conRec:[Select id,Name from Contact where id In :setCont]){
				mpConToName.put(conRec.id,conRec.Name);
			}
		}

		if(setRel != null && setRel.size() > 0){
			for(Account accRec:[Select id,Name from Account where id In :setRel]){
				mpRelToName.put(accRec.id,accRec.Name);
			}
		}
		List<FeedItem> listFeed = new List<FeedItem>();
		for(Referral__c ref:listNew){
			if(ref.Contact__c != null){
				listFeed.add(getFeeditem(ref.Contact__c,ref.Contact__c,ref.Referred_By_Contact__c,ref.Relationship__c,ref.Referred_By_Relationship__c,'refBy'));	
			}	
			if(ref.Relationship__c != null){
				listFeed.add(getFeeditem(ref.Relationship__c,ref.Contact__c,ref.Referred_By_Contact__c,ref.Relationship__c,ref.Referred_By_Relationship__c,'refBy'));	
			}
			if(ref.Referred_By_Contact__c != null){
				listFeed.add(getFeeditem(ref.Referred_By_Contact__c,ref.Contact__c,ref.Referred_By_Contact__c,ref.Relationship__c,ref.Referred_By_Relationship__c,'ref'));				
			}			
			if(ref.Referred_By_Relationship__c != null){
				listFeed.add(getFeeditem(ref.Referred_By_Relationship__c,ref.Contact__c,ref.Referred_By_Contact__c,ref.Relationship__c,ref.Referred_By_Relationship__c,'ref'));				
			}						
		}
		if(listFeed != null && listFeed.size() > 0)
			insert listFeed;
		
	}
	
	public FeedItem getFeeditem(ID parent,ID con,ID refBycon,ID rel, ID refByrel,String sType){
		String feedBody = '';
		String refByconName = '';
		String conName = '';
		String relName = '';
		String refByrelName = '';
		if( refBycon != null && mpConToName.containsKey(refBycon))
			refByconName = mpConToName.get(refBycon);
		if(con != null && mpConToName.containsKey(con))
			conName = mpConToName.get(con);
		if(rel != null && mpRelToName.containsKey(rel))
			relName = mpRelToName.get(rel);
		if(refByrel != null && mpRelToName.containsKey(refByrel))
			refByrelName = mpRelToName.get(refByrel);	
											
		FeedItem  fItm = new FeedItem ();
			fItm.ParentId = parent;
		if(sType == 'refBy'){	
			if(refByconName != '')	
				feedBody = refByconName;
			if(refByrelName != '')
				feedBody += ' ('+ refByrelName +')';
			feedBody += ' '+REFERREDBY;
			if(conName != '')	 
				feedBody += ' '+conName;
			if(relName != '')	
				feedBody += ' ('+ relName +')';
		}
		if(sType == 'ref'){
			if(conName != '')	 
				feedBody += ' '+conName;
			if(relName != '')	
				feedBody += ' ('+ relName +')';	
			feedBody += ' '+REFERRED;
			if(refByconName != '')	
				feedBody += ' '+refByconName;	
			if(refByrelName != '')
				feedBody += ' ('+ refByrelName +')';													
		}
			fItm.Body = feedBody;
		return fItm;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// if a contact was added in the Referred By Contact - lookup their account and add it to the Referred By Account
	//	Method must be in a before trigger instance
	private void lookupReferredByAccount_Before(list<Referral__c> listNew){
		list<Referral__c> workingList = new list<Referral__c>();
		set<Id> contactIds = new set<Id>();
		
		for (Referral__c currReferral : listNew){
			if (currReferral.Referred_By_Contact__c!=null && currReferral.Referred_By_Relationship__c==null){
				workingList.add(currReferral);
				contactIds.add(currReferral.Referred_By_Contact__c);
			}
		}
		
		map<Id, Contact> contactMap = new map<Id, Contact>([select id, AccountId from Contact where id in :contactIds]); 
		
		for (Referral__c currReferral : workingList){
			Contact currContact = contactMap.get(currReferral.Referred_By_Contact__c);
			if (currContact!=null)
				currReferral.Referred_By_Relationship__c = currContact.AccountId;
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// generate the inverse referral
	//	Should come after the lookupReferredByAccount logic has run
	private void inverseOnInsert_After(list<Referral__c> listNew){
		map<id, Referral__c> referralMap = new map<Id, Referral__c>();
		list<Referral__c> newInvRefs = new list<Referral__c>();
		
		for(Referral__c currReferral : listNew){
			if (currReferral.Inverse_Referral__c == null){
				referralMap.put(currReferral.Id, currReferral);
				
				Referral__c newRef = new Referral__c();
				newInvRefs.add(newRef);
				
				mapReferralInverse(currReferral, newRef);
			}
		}
		
		if (newInvRefs.size()>0){
			insert newInvRefs;
		
			list<Referral__c> updateReferrals = new list<Referral__c>();
			 
			for(Referral__c currReferral : newInvRefs){
				Referral__c trigReferral = referralMap.get(currReferral.Inverse_Referral__c);
				if (trigReferral!=null){
					updateReferrals.add(new Referral__c(
						id = trigReferral.Id,
						Inverse_Referral__c = currReferral.Id
					));
				}
			}
			
			inverseReferralsInProcess.addAll(referralMap.keySet());
			update updateReferrals;
			inverseReferralsInProcess.removeAll(referralMap.keySet());
		}
	}
	private void mapReferralInverse(Referral__c trigReferral, Referral__c invReferral){
		String invRelationship = Utility_Common.inverseRelationshipMap.get(trigReferral.Type__c);
		
		invReferral.Referred_By_Relationship__c = trigReferral.Relationship__c;
		invReferral.Referred_By_Contact__c 		= trigReferral.Contact__c;
		invReferral.Relationship__c 			= trigReferral.Referred_By_Relationship__c;
		invReferral.Contact__c 					= trigReferral.Referred_By_Contact__c;
		invReferral.Type__c 					= invRelationship;
		invReferral.Inverse_Referral__c 		= trigReferral.Id;
		invReferral.Referral_Comments__c		= trigReferral.Referral_Comments__c;
	}
	private void inverseOnUpdate(map<Id,Referral__c> oldMap, list<Referral__c> listNew){
		list<Referral__c> workingList = new list<Referral__c>();
		for(Referral__c currReferral : listNew){
			if(inverseReferralsInProcess.contains(currReferral.Id)==false &&
				(currReferral.Type__c 						!= oldMap.get(currReferral.Id).Type__c 						||
				currReferral.Contact__c 					!= oldMap.get(currReferral.Id).Contact__c 					||
				currReferral.Relationship__c 				!= oldMap.get(currReferral.Id).Relationship__c 				||
				currReferral.Referred_By_Contact__c 		!= oldMap.get(currReferral.Id).Referred_By_Contact__c 		||
				currReferral.Referred_By_Relationship__c 	!= oldMap.get(currReferral.Id).Referred_By_Relationship__c)
			){
				workingList.add(currReferral);
			}
		}
		
		if (workingList.size()>0){
			map<Id,Referral__c> inverseReferralMap = new map<Id, Referral__c>([select id from Referral__c where Inverse_Referral__c in :workinglist]);
			
			for(Referral__c currReferral : workingList){
				mapReferralInverse(currReferral, inverseReferralMap.get(currReferral.Inverse_Referral__c));
			}
			
			inverseReferralsInProcess.addAll(inverseReferralMap.keySet());
			update inverseReferralMap.values();
			inverseReferralsInProcess.removeAll(inverseReferralMap.keySet());
		}
	}
	private void inverseOnDelete(map<Id,Referral__c> oldMap){
		set<id> deleteIds = new set<Id>();
		list<Referral__c> deleteList = new list<Referral__c>();
		
		for(Referral__c currReferral : oldMap.values()){
			if (currReferral.Inverse_Referral__c != null && 
				inverseReferralsInProcess.contains(currReferral.Id)==false){
					
				deleteIds.add(currReferral.Inverse_Referral__c);
				deleteList.add(new Referral__c(id = currReferral.Inverse_Referral__c));
			}
		}
		
		if(deleteIds.size()>0){
			inverseReferralsInProcess.addAll(deleteIds);
			delete deleteList;
			inverseReferralsInProcess.removeAll(deleteIds);
		}
	}
	
}