@isTest
private class testMethod_GlobalContactSearch {

    static testMethod void GlobalContactSearch_AccountSearch_Test() 
    {
    	List<Account> accountList = TestDataCreator_Utils.createSObjectList('Account' , false , 40);
    	Id [] fixedSearchResults= new Id[40];
    	for (integer i = 0 ; i< 40 ; i++)
    	{
			accountList[i].Name = 'TestAccount' ;
			accountList[i].Relationship_Number__c = 'A:' + i;
    		accountList[i].Estimated_Net_Worth__c = i * 500;
    	}
			
		insert accountList;
		for (integer i = 0 ; i< 40 ; i++)
			fixedSearchResults[i] = accountList[i].id;
			
			
		Test.setFixedSearchResults(fixedSearchResults);	
    	
    	Test.startTest();
    	
    	GlobalContactSearch myCon = new GlobalContactSearch();
    	myCon.searchTerm = 'TestAccount' ; 
    	myCon.performSearch ();
    	System.AssertEquals('40', myCon.accountResultsCount);
    	myCon.accountLastPage();
    	myCon.accountFirstPage() ;
    	myCon.accountNextPage();
    	myCon.accountPreviousPage();
    	
    	//Test Sorting
    	myCon.accountSortField  = 'Estimated_Net_Worth__c' ;
    	myCon.SortAccountList ();
    	
    	//Test the Account Filtering
    	myCon.accountRelationshipNumberFilter = 'A:1';
    	myCon.ApplyaccountFilter() ;
    	
    	List<GlobalContactSearch.AccountResultSet> resultSet = myCon.accountSearchResults;
    	System.AssertEquals('1', myCon.accountResultsCount);
    	
    	//Test Request Access
    	myCon.RequestAccessId = resultSet[0].Account.id;
    	myCon.RequestOwnership = false;
    	myCon.SendRequestEmail() ;
    	
    	Test.stopTest();
    }
    
    static testMethod void GlobalContactSearch_ContactSearch_Test() 
    {
    	List<Contact> ContactList = TestDataCreator_Utils.createSObjectList('Contact' , false , 40);
    	Id [] fixedSearchResults= new Id[40];
    	for (integer i = 0 ; i< 40 ; i++)
    	{
			ContactList[i].LastName = 'TestAccount' ;
			ContactList[i].Email = 'Test' + String.ValueOf(i) + '@test.com' ;
    	}
		insert contactList;
		for (integer i = 0 ; i< 40 ; i++)
			fixedSearchResults[i] = contactList[i].id;
			
			
		Test.setFixedSearchResults(fixedSearchResults);	
    	
    	Test.startTest();
    	
    	GlobalContactSearch myCon = new GlobalContactSearch();
    	myCon.searchTerm = 'TestAccount' ; 
    	myCon.performSearch ();
    	System.AssertEquals(20, myCon.contactSearchResults.size());
    	myCon.contactLastPage();
    	myCon.contactFirstPage() ;
    	myCon.contactNextPage();
    	myCon.contactPreviousPage();
    	
    	//Test Sorting
    	myCon.contactSortField = 'LastName';
    	myCon.SortContactList();
    	
    	myCon.contactSortField = 'RelationshipCount';
    	myCon.SortContactList();
    	
    	
    	List<GlobalContactSearch.ContactResultSet> resultSet = myCon.contactSearchResults;
    	
    	myCon.RequestAccessId = resultSet[0].Contact.id;
    	myCon.RequestOwnership = false;
    	myCon.SendRequestEmail() ;
    	
    	
    	Test.stopTest();
    }
    
    static testMethod void GlobalContactSearch_LeadSearch_Test() 
    {
    	List<Lead> leadList = TestDataCreator_Utils.createSObjectList('Lead' , false , 40);
    	Id [] fixedSearchResults= new Id[40];
    	for (integer i = 0 ; i< 40 ; i++)
    	{
			leadList[i].Company = 'TestAccount' ;
			leadList[i].Email = 'Test' + String.ValueOf(i) + '@test.com' ;
    	}
		insert leadList;
		for (integer i = 0 ; i< 40 ; i++)
			fixedSearchResults[i] = leadList[i].id;
			
			
		Test.setFixedSearchResults(fixedSearchResults);	
    	
    	Test.startTest();
    	
    	GlobalContactSearch myCon = new GlobalContactSearch();
    	myCon.searchTerm = 'TestAccount' ; 
    	myCon.performSearch ();
    	System.AssertEquals('40', myCon.leadResultsCount);
    	myCon.leadFirstPage() ;
    	myCon.leadNextPage();
    	myCon.leadPreviousPage();
    	
    	//test Sorting Fields 
    	myCon.leadSortField = 'Company';
    	myCon.SortLeadList();
    	
    	//Test sending request access
    	
    	List<GlobalContactSearch.LeadResultSet> resultSet = myCon.leadSearchResults;
    	
    	myCon.RequestAccessId = resultSet[2].Lead.id;
    	
    	myCon.RequestOwnership = false;
    	//myCon.SendRequestEmail() ;
    	
    	Test.stopTest();
    }
    
   
}