public with sharing class manageContactFieldListCtrl 
{
	public string PageName {get;set;}
	public string Platform {get;set;}
	public list<ContactFieldList__c > DataList {get;set;}
	public string selectedPageName {get;set;}
	public string selectedSectionName {get;set;}
	
	public manageContactFieldListCtrl  ()
	{
		PageName = null;
		Platform = null;
		selectedPageName = 'Contact Prospect' ;
		getRecords() ;
	}
	
	public void SaveRecs ()
	{
		list<ContactFieldList__c > fl = new list<ContactFieldList__c > ();
		for (ContactFieldList__c f : DataList)
		{
			if (f.Name != 'DK_tmp')
				fl.add(f) ;
		}
		upsert fl  ;
		getRecords() ;
	}
	
	public void Filter ()
	{
		getRecords() ;
	}
	public void getRecords() 
	{
		string sqlString = 'SELECT ' + Utility_Common.getFieldList( 'ContactFieldList__c' ) + ' FROM ContactFieldList__c '  ;
		sqlString += ' where PageName__c= \'' + selectedPageName + '\' ' ;
		if (selectedSectionName != null)
			sqlString += ' and SectionName__c = \'' + selectedSectionName + '\'';
		sqlString += 'Order By SectionOrder__c ,SortOrder__c';
		
		DataList = Database.query(sqlString );
		DataList.Add (new ContactFieldList__c (Name='DK_tmp') );
	}
	
	public List<String> getFields ()
	{
		SObjectType objToken = Schema.getGlobalDescribe().get( 'Contact' );
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap(); 
		List<String> tmpList = new List<String> ();
		for (string key : fields.keyset() )
		{
			tmpList.add(key) ;
		}
		
		return tmpList;
	}
	
	public List<SelectOption> getPageNameList ()
	{
		List<AggregateResult> ar = [select PageName__c FROM ContactFieldList__c  Group By PageName__c];
		List<SelectOption> options = new List<SelectOption>();
		for (AggregateResult a :ar)
		{
			if (a.get('PageName__c') != null)
				options.add(new SelectOption(String.ValueOf(a.get('PageName__c')), String.ValueOf(a.get('PageName__c'))  ));
		}
		
		return options;
		
	} 
	
	public List<SelectOption> getSectionName ()
	{
		List<AggregateResult> ar = [select SectionName__c FROM ContactFieldList__c where PageName__c = :selectedPageName Group By SectionName__c];
		List<SelectOption> options = new List<SelectOption>();
		for (AggregateResult a :ar)
		{
			if (a.get('SectionName__c') != null)
				options.add(new SelectOption(String.ValueOf(a.get('SectionName__c')), String.ValueOf(a.get('SectionName__c'))  ));
		}
		
		return options;
		
	} 
	public List<SelectOption> getFieldOptions() 
	{
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	SObjectType objToken = Schema.getGlobalDescribe().get( 'Contact' );
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap(); 
		List<String> tmpList = new List<String> ();
		map<string, string> mp = new map<string, string> () ;
		for (string key : fields.keyset() )
		{
			SObjectField fieldToken = fields.get( key );
			DescribeFieldResult selectedField = fieldToken.getDescribe();
			tmpList.add(selectedField.getName()) ;
			mp.put(selectedField.getName() , selectedField.getLabel());
			//options.add(new SelectOption(selectedField.getName(), selectedField.getLabel() ));
		}
		tmpList.Sort();
		options.add(new SelectOption('BlankSectionItem', 'BlankSectionItem' ));
		for (string s :tmpList)
			options.add(new SelectOption(s, mp.get(s)));
 	 	//options.add(new SelectOption('US','US'));
 	 	//options.add(new SelectOption('CANADA','Canada'));
 	 	//options.add(new SelectOption('MEXICO','Mexico'));
 	 	return options;
  	}
	
	
}