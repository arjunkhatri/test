/*  
 *  Descprition :This Class adding Team Members to Relationship and validation for 
 *  			only one Team Member of role 'Senior CA' is added.
 *  Revision History:
 *   Version          Date            Description
 *   1.0            11/08/2014      Initial Draft
 */

public without sharing class RelationshipTeamCtrl {
	public static 	String 		memId		{get;set;} 
	public List<Relationship_Team_Member__c > lstMem 	{get;set;}
	public List<AccountShare> 	lstAccShare {get;set;}
	public List<wrapper> 		lstWrap 	{get;set;}
	public String 	accId					{get;set;}
	public String 	accountName				{get;set;}
	public Boolean 	isEdit 					{get;set;}
	public Boolean 	displayPopup 			{get; set;}
	public String 	strError;
	public String 	editAccAccess 			{get;set;}
	public static 	final String READONLY 	= 'Read';
	public static 	final String PRVT 		= 'None';
	public static 	final String SENIORCA 	= 'Senior Client Advisor';
	public static 	final String WEALTHADV 	= 'Wealth Advisor';
	public static  final String PROSPECREL  = 'Prospective Relationship';
	public static  final String CLIENTREL  = 'Client Relationship';
	public List<Profile>    lstProfile      {get;set;}
	public set<String> setRoles;
	public map<String,String> mpOldRoleToUser;
	public map<String,String> mpNewRoleToUser;
	public List<roleAcctionWrap> 		lstMessageWrap 	{get;set;}
	public Boolean          isBIUser                {get;set;}
	public RelationshipTeamCtrl(){
		isBIUser = false;        
		//id of member to be edit
		memId = ApexPages.currentPage().getParameters().get('eid');
		lstMem = new List<Relationship_Team_Member__c >();
		lstAccShare = new List<AccountShare>();
		//read role for validation from custom settings
		setRoles = new set<String>();
		mpOldRoleToUser = new map<String,String>();
        lstProfile = [Select Id From Profile Where Name = 'Bessemer Integration' Limit 1];
        if(lstProfile != null && lstProfile.size() > 0){
            if(lstProfile[0].id == userinfo.getProfileId())
                isBIUser = true;		 
        }
		for(ValidateRelationshipTeamRoles__c strRole:ValidateRelationshipTeamRoles__c.getall().values()){
			setRoles.add(strRole.Name);
		}		 
		if(memId != null && memId != ''){      
			isEdit = true;
		 	initEdit();    
		}else{    
		 	isEdit = false;
		 	initNew();  
		}
		if(!isBIUser)  
			readOldRoles();
	}   
	
 	public List<SelectOption> getOptions(){
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	options.add(new SelectOption('Edit','Read/Write')); 
 	 	return options;
  	}
  		
	//load page with edit mode
	public void initEdit(){    
		//get account team member
		lstMem = [Select User__c, Role__c, Id, Relationship__c
				 From Relationship_Team_Member__c where id=:memId];
		if(lstMem != null && lstMem.size() > 0){    
			//get account share object 
			if(lstMem[0].Relationship__c != null && lstMem[0].User__c != null){ 
				accId = lstMem[0].Relationship__c;
				lstAccShare = [Select UserOrGroupId, OpportunityAccessLevel, CaseAccessLevel,
								AccountId, AccountAccessLevel From AccountShare 	where 
								UserOrGroupId = :lstMem[0].User__c and AccountId = :lstMem[0].Relationship__c
								limit 1];	
				if(lstAccShare != null && lstAccShare.size() > 0){
					editAccAccess = lstAccShare[0].AccountAccessLevel;
				}
			}
		}
	}
	
	//load page with add new team members mode
	public void initNew(){
		//display 5 rows to enter new team members
 		accId = ApexPages.currentPage().getParameters().get('aid');
 		if(accId != null && accId != ''){
	 		lstWrap = new List<wrapper>(); 
			for(Integer i=0;i<5;i++){ 
				Relationship_Team_Member__c   newTm = new Relationship_Team_Member__c ();
				newTm.Relationship__c = accId;
				AccountShare newShr = new AccountShare();
				newShr.AccountId = accId;
				lstWrap.add(new wrapper(newTm,newShr));
			}
 		}
	}
	
	//function call when add new members and Save 
	public pagereference newSave(){
		map<String,String> mpIdToUserName = new map<String,String>();
		map<String,List<String>> mpRoleTousers = new map<String,List<String>>();
		mpNewRoleToUser = new map<String,String>();
		lstMem = new List<Relationship_Team_Member__c>();
		lstAccShare = new List<AccountShare>();
		Boolean isNoRole = false; 
		Integer memCount = 0;
		String strOneRole = '';
		//process newly added team members
		for(wrapper wp:lstWrap){
			//check atleast 1 member selected to save
			if(wp.accTm.User__c != null){  
					memCount++;
			}
			if(wp.accTm.User__c != null && (wp.accTm.Role__c == null ||
				wp.accTm.Role__c =='')){
				//selected user but not role	
				isNoRole = true;
				break;
			}
			else if(wp.accTm.User__c != null  && wp.accTm.Role__c != null && wp.accTm.Role__c != ''){  
				if(wp.accShare.UserOrGroupId == null){  
					wp.accShare.UserOrGroupId = wp.accTm.User__c;
				}
				lstMem.add(wp.accTm);
				if(setRoles.contains(wp.accTm.Role__c) && !isBIUser){
					mpNewRoleToUser.put(wp.accTm.Role__c,wp.accTm.User__c);
					if(!mpRoleTousers.containsKey(wp.accTm.Role__c)){
						mpRoleTousers.put(wp.accTm.Role__c, new List<String> {wp.accTm.User__c});
					}
					else if(mpRoleTousers.containsKey(wp.accTm.Role__c)){
						mpRoleTousers.get(wp.accTm.Role__c).add(wp.accTm.User__c);
					}
				}
				//we cant change the sharing access of current owner user of record
				if(wp.accShare.UserOrGroupId != userinfo.getuserid()){
						wp.accShare.AccountAccessLevel = wp.straccShare; 
						lstAccShare.add(wp.accShare);
				}
			}
		}
		//validations
		if(memCount == 0){ 
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,System.Label.One_Team_Member));  
	        return null;			
		}
		if(isNoRole){  
			//not valid team members
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,System.Label.Team_Role_Required));  
	        return null;	
		}
		
		for(String mpRec:mpRoleTousers.KeySet()){
			if(mpRoleTousers.get(mpRec).size() > 1)
				strOneRole += mpRec+',';
		}
		if(strOneRole != ''){ 
			strOneRole = strOneRole.substring(0,strOneRole.length() - 1);
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,string.format(System.Label.One_Senior_CA, new string[] {strOneRole})));  
	        return null;
		}
		//validation for duplicate team members
		
		String strVal = validateDuplicateMem(lstMem);
		if(strVal != ''){  
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,strVal));  
	        return null;		
		}
		for(User uRec:[Select id,Name from User where id=:mpNewRoleToUser.values()]){
			mpIdToUserName.put(uRec.id,uRec.Name);
		}
		lstMessageWrap = new List<roleAcctionWrap>();
		if(!isBIUser){
			for(String strRole:setRoles){
				if(mpNewRoleToUser.containsKey(strRole) && mpOldRoleToUser.containsKey(strRole)){
					String uId = mpNewRoleToUser.get(strRole);
					String newUserName = mpIdToUserName.get(uId);
					String strValMsg = mpOldRoleToUser.get(strRole) +' is currently assigned to '+ accountName +' as the '+strRole+'. Do you wish to replace '+mpOldRoleToUser.get(strRole)+' with '+newUserName+'?';
					lstMessageWrap.add(new roleAcctionWrap(strRole,strValMsg,false));
					displayPopup = true;
				}
			}
		}
		if(lstMessageWrap != null && lstMessageWrap.size() > 0)
			return null;
		else
			newSaveOk();
		return new pagereference('/'+accId);
	}

	//read user name from id
	public String readUser(String uId){ 
		String uName;
		User u = [Select Name from User where id=:uId limit 1]; 
		if(u != null){
			uName = u.Name;
		}
		return uName;	
	}
	//function read existing Senior CA if exist
	public void readOldRoles(){    
		for(Relationship_Team_Member__c acm:[Select Id,User__r.Name,
													Relationship__r.Name,
													Relationship__r.Id,
													Role__c From 
													Relationship_Team_Member__c where Relationship__c=:accId]){
			accountName = String.valueOf(acm.Relationship__r.Name);
			if(setRoles != null && setRoles.size() > 0 && setRoles.contains(acm.Role__c)){
				mpOldRoleToUser.put(acm.Role__c,acm.User__r.Name);
			}
		}	
	}
	//common function performs insert ,upsert and delete and update as per requirement
	public pagereference newSaveOk(){
		strError = '';   
		set<String> setRoleDel = new set<String>();
		set<String> setRoleNoOverride = new set<String>();  
		List<Relationship_Team_Member__c> finalList = new List<Relationship_Team_Member__c>();
		if(lstMessageWrap != null && lstMessageWrap.size() > 0){
			for(roleAcctionWrap rWrap:lstMessageWrap){
				if(rWrap.isOverride){
					setRoleDel.add(rWrap.strRole);
				}else{
					setRoleNoOverride.add(rWrap.strRole);
				}
			}
		}
		for(Relationship_Team_Member__c RelMem:lstMem){
			if(!setRoleNoOverride.contains(RelMem.Role__c)){
				finalList.add(RelMem);
			}
		}
		
		//user press ok incase of override Senior CA
		List<Relationship_Team_Member__c> atm  = [Select id,
														 Role__c, 
														 Relationship__c from Relationship_Team_Member__c 
														 where Role__c =:setRoleDel and Relationship__c =:accId];
		//delete existing roles
 		if(atm != null && atm.size() > 0){
			try{  
				delete atm;
			}catch(DMLException ex){
		        strError = ex.getMessage();
			} 
 		}
		
		//insert account share record
		if(lstAccShare != null && lstAccShare.size() > 0){
			try{ 
				upsert lstAccShare;
			}catch(DMLException ex){
		        strError = ex.getMessage();
			}
		}
		//insert account member records
		if(finalList != null && finalList.size() > 0){
				try{
					upsert populateFields(finalList); 
					updateRelationshipTask();
				}catch(DMLException ex){
		        	strError = ex.getMessage();
				} 
		}
		system.debug('>>>>update acc call');
		//update Account Record
		updateAccount(); 
		pagereference pg = new pagereference('/'+accId);
		pg.setRedirect(true);
		return pg;
	}
	public List<Relationship_Team_Member__c> populateFields(List<Relationship_Team_Member__c> listNewMem){
		Account acc = [Select Relationship_Number__c From Account where id =:listNewMem[0].Relationship__c limit 1];
		List<Relationship_Team_Member__c> listNew = new List<Relationship_Team_Member__c>();
		Set<Id> setUser = new Set<Id>();
		for(Relationship_Team_Member__c mem:listNewMem){
			setUser.add(mem.User__c);
		}
		Map<ID,String> mpUser = new Map<ID,String>();
		for(User u:[Select Id, EmployeeNumber From User where id IN :setUser]){
			mpUser.put(u.id,u.EmployeeNumber);
		}
		for(Relationship_Team_Member__c rel:listNewMem){
			String edbKey = '';
			if(acc.Relationship_Number__c != null)
				edbKey = acc.Relationship_Number__c;			
			if(mpUser.containsKey(rel.User__c)){
				rel.Employee_Number__c = String.valueOf(mpUser.get(rel.User__c));
				edbKey += String.valueOf(mpUser.get(rel.User__c));
			}
				edbKey += rel.Role__c;
				rel.EDB_Relationship_Team_Key__c = edbKey;	
				listNew.add(rel);
		}	
		return listNew;
	}
	//function to validate duplicate Relationship team member
	public String validateDuplicateMem(List<Relationship_Team_Member__c> listNewMem){  
		String dupValError = '';
		for(Relationship_Team_Member__c rtm:[Select User__c,User__r.Name, Role__c,
							 				Relationship__c,Relationship__r.Name, Id 
							 				From Relationship_Team_Member__c 
							 				where Relationship__c =:listNewMem[0].Relationship__c])
		{
			for(Relationship_Team_Member__c rtmNew:listNewMem){
				if(rtm.Relationship__c == rtmNew.Relationship__c &&
					rtm.User__c == rtmNew.User__c && rtm.Role__c == rtmNew.Role__c)
					{
						dupValError += string.format(System.Label.Duplicate_Validation_Error, 
						new string[] {rtm.User__r.Name,rtm.Role__c,rtm.Relationship__r.Name});
					}
			}
		}
		return dupValError;
	}	
	//function updating Senior CA of Account
	public void updateAccount(){ 
		Map<String,String> mpRoleToWadv = new Map<String,String>();
		Map<String,String> mpRoleToCadv = new Map<String,String>();
   		for(Bessemer_Role_To_Team__c brt:Bessemer_Role_To_Team__c.getall().values())
   		{
   			if(brt.Role_Name__c != null && brt.Role_Name__c !='' && brt.Client_Advisor_Team__c != null)
   				mpRoleToCadv.put(brt.Role_Name__c,brt.Client_Advisor_Team__c);
   			if(brt.Role_Name__c != null && brt.Role_Name__c !='' && brt.Wealth_Advisor_Team__c != null)
   				mpRoleToWadv.put(brt.Role_Name__c,brt.Wealth_Advisor_Team__c);	
   		}

		Account accUpdate = [Select RecordType.Name,RecordType.Id ,Owner.Name , Bessemer_Team__c,Senior_Client_Advisor__c, Wealth_Advisor__c , Bessemer_Office__c from Account where id=:accId limit 1];
		if(accUpdate != null){  
			accUpdate.Senior_Client_Advisor__c = '';
			accUpdate.Wealth_Advisor__c = '';
			for(Relationship_Team_Member__c actm :[Select Role__c ,
													User__c,
													User__r.Office__c,
													User__r.Name,
													User__r.UserRole.Name
													from Relationship_Team_Member__c where 
													Relationship__c = :accId and 
													(Role__c = :SENIORCA or Role__c = :WEALTHADV)])
			{
				if(actm.Role__c == SENIORCA){
					accUpdate.Senior_Client_Advisor__c = actm.User__r.Name;
					if(accUpdate.RecordType.Name == CLIENTREL){
						accUpdate.OwnerId = actm.User__c;
						if(actm.User__r.Office__c != null)
							accUpdate.Bessemer_Office__c = actm.User__r.Office__c;
						if(actm.User__r.UserRole.Name != null && mpRoleToCadv.containsKey(actm.User__r.UserRole.Name)){
							accUpdate.Bessemer_Team__c = mpRoleToCadv.get(actm.User__r.UserRole.Name);
						}
						else{
							accUpdate.Bessemer_Team__c = '';
						}
					}
				}
				
				if(actm.Role__c == WEALTHADV){
					accUpdate.Wealth_Advisor__c = actm.User__r.Name;
					if(accUpdate.RecordType.Name == PROSPECREL){
						accUpdate.OwnerId = actm.User__c;
						if(actm.User__r.Office__c != null)
							accUpdate.Bessemer_Office__c = actm.User__r.Office__c;
						if(actm.User__r.UserRole.Name != null && mpRoleToWadv.containsKey(actm.User__r.UserRole.Name)){
							accUpdate.Bessemer_Team__c = mpRoleToWadv.get(actm.User__r.UserRole.Name);
						}
						else{
							accUpdate.Bessemer_Team__c = '';
						}
					}
				}
			}
			
			try{
				update accUpdate;
			}catch(DMLException ex){ 
		    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
			}
		}		
	}	
	//save after edit Member
	public pagereference eSave(){ 
		if(lstAccShare != null && lstAccShare.size() > 0) 
			lstAccShare[0].AccountAccessLevel = editAccAccess;
		if(lstMem[0].Role__c == null || lstMem[0].Role__c == ''){
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,System.Label.Team_Role_Required));   
	        return null;		
		}
		newSaveOk();
		if(strError != ''){   
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,strError));  
	        return null;				
		}		
		return new pagereference ('/'+accId);
	}
	
	public pagereference Cancel(){ 
		return new pagereference ('/'+accId);
	}	
	public void closePopup() {  
        displayPopup = false;    
    }
	//function to update task Relationship_Team_List__c filed according to relationship team member
	public void updateRelationshipTask(){ 
		List<Task> listTask = new List<Task>();
		List<Relationship_Team_Member__c> listMembers;
		if(accId != null && accId != ''){
			listMembers = [Select Id,User__c 
					  From Relationship_Team_Member__c where Relationship__c =: accId];
			if(listMembers != null && listMembers.size() > 0){
				for(Task tsk :[Select WhatId, 
							Relationship_Team_List__c 
							From Task where WhatId =: accId])
				{
					for(Relationship_Team_Member__c atm:listMembers){
						String atmUser = String.valueOf(atm.User__c);
						if(atmUser.length() == 18)
							atmUser = atmUser.substring(0,15);
						if(tsk.Relationship_Team_List__c != null && tsk.Relationship_Team_List__c != ''){
							if(!tsk.Relationship_Team_List__c.contains(atmUser)){
								tsk.Relationship_Team_List__c = tsk.Relationship_Team_List__c +
																	'#'+ atmUser + '#';
							}
						}
						else{
							tsk.Relationship_Team_List__c = '#'+ atmUser + '#';
						}
					} 
					listTask.add(tsk);
				}
			}
		}
		if(listTask != null && listTask.size() > 0){  
			try{
				update listTask;
			}catch(DMLException ex){     
		    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
			}			
		}
	}
	
	public class wrapper{
		public Relationship_Team_Member__c accTm{get;set;}
		public AccountShare accShare{get;set;}
		public String straccShare{get;set;}
		public wrapper(Relationship_Team_Member__c accTm,AccountShare accShare){  
			this.accTm = accTm;
			this.accShare = accShare;
			this.accShare.AccountAccessLevel = READONLY;
			this.accShare.OpportunityAccessLevel = PRVT;
			this.accShare.CaseAccessLevel = PRVT;
		}
	}
	
	public class roleAcctionWrap{
		public String strRole{get;set;}
		public String strMessage{get;set;}
		public Boolean isOverride{get;set;}
		public roleAcctionWrap(String strRole ,String strMessage,Boolean isOverride){  
			this.strRole = strRole;
			this.strMessage = strMessage;
			this.isOverride = isOverride;
		}
	}	
}