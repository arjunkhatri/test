/*
    
     Authors :  David Brandenburg
     Created Date: 2014-04-09
     Last Modified: 2014-04-09
*/

public with sharing class AddressTrigHand 
{
    // template methods
    public void onBeforeInsert(list<Addresses__c> newList)
    {
        setPrimaryAddress(newList);
        setContactData(newList);
        assignAddressNumber(newList);
    }
    public void onAfterInsert(list<Addresses__c> newList)
    {
        map<id , Addresses__c> updateList = new   map<id , Addresses__c>  () ;
        resetAddressPrimary (newList , updateList);
        updateParentObject (newList);
        resetLegalAddress (newList, updateList);
        if (updateList.size() > 0 )
        {
            update updateList.values();
        }

        updateDataSyncLog(newList, 'I');
    }
    public void onBeforeUpdate(list<Addresses__c> newList, map<id,Addresses__c> oldMap)
    {
        setPrimaryAddressUpdate(newList,oldMap);
        UnCheckPrimaryCheck(newList,oldMap);
        UnCheckLegalCheck(newList,oldMap);
    }
    public void onAfterUpdate(list<Addresses__c> newList, map<id,Addresses__c> oldMap)
    {
        
        updateParentObject (newList);
        map<id , Addresses__c> updateList = new   map<id , Addresses__c>  (); 
        resetAddressPrimary (newList ,  updateList ) ;
        resetLegalAddress (newList, updateList);
        if (updateList.size() > 0 )
        {
            update updateList.values();
        }
        
        updateDataSyncLog(newList, 'U');
        
    }
    public void onBeforeDelete(list<Addresses__c> oldList){
        DeletePrimaryCheck(oldList);
        DeleteMailingCheck(oldList);
    }
    public void onAfterDelete(list<Addresses__c> oldList){
        updateDataSyncLog(oldList, 'D');
    }
    public void onAfterUndelete(list<Addresses__c> newList){
    }

// business logic   
    private void setContactData (list<Addresses__c> newList)
    {
        set<string> contactSet = new set<string>();
        for (Addresses__c a : newList)
        {
            if (a.contact__c != null)
                contactSet.add(a.contact__c);
        }

        map<string, Contact > contactMap = new Map<string, Contact>( [select id, FirstName, LastName from Contact where id in :contactSet] );

        for (Addresses__c a : newList)
        {
            if (a.contact__c != null)
            {
                a.ContactFirstName__c = contactMap.get(a.Contact__c).FirstName;
                a.ContactLastName__c = contactMap.get(a.Contact__c).LastName;
            }
        }
    }
    private void setPrimaryAddress (list<Addresses__c> newList)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        set<Id> contactId = new set<Id> ();
        set<Id> leadId = new set<Id> ();
        for (Addresses__c address : newList)
        {
            if (address.Contact__c == null && address.Lead__c != null)
                leadId.add(address.Lead__c);
            else
            {
                contactId.add(address.Contact__c);
            }
        }
        
        integer cnt = 15;
        
        //Check the Lead Records    
        if (leadId.size() > 0 )
        {
	        List<AggregateResult> results = [select lead__c  , count(id) countOfId from Addresses__c where lead__c in :leadId Group BY Lead__c ];
	        map<string, integer> resultTotalMap = new map<string, integer> ();
	        for (AggregateResult result :results )
	            resultTotalMap.put((string)result.get('Lead__c'), (Integer)result.get('countOfId'));
	        
	        
	        
	        if (AddressCustomzationConfig__c.getinstance() != null)
	            cnt = Integer.ValueOf(AddressCustomzationConfig__c.getOrgDefaults().TotalAddressRecords__c) ;
	        
	        for (Addresses__c address : newList)
	        {
	            System.Debug(resultTotalMap);
	            if (resultTotalMap.ContainsKey(address.Lead__c)  && resultTotalMap.get(address.Lead__c) + 1  > cnt )
	                address.addError('There is a limit of ' + String.ValueOf(cnt) + ' address records');
	            else if (resultTotalMap.containskey(address.Lead__c) == false && address.Primary_Address__c == false )
	                address.Primary_Address__c = true;
	            
	            
	        }
        }
        //Check the Contact Records 
        if (contactId.Size() > 0 )
        {   
	        List<AggregateResult> results = [select Contact__c  , count(id) countOfId from Addresses__c where Contact__c in :contactId Group BY Contact__c ];
	        map<string, integer> resultTotalMap = new map<string, integer> ();
	        for (AggregateResult result :results )
	            resultTotalMap.put((string)result.get('Contact__c'), (Integer)result.get('countOfId'));
	        
	        
	        
	        if (AddressCustomzationConfig__c.getinstance() != null)
	            cnt = Integer.ValueOf(AddressCustomzationConfig__c.getOrgDefaults().TotalAddressRecords__c) ;
	        
	        for (Addresses__c address : newList)
	        {
	            System.Debug(resultTotalMap);
	            if (resultTotalMap.ContainsKey(address.Contact__c)  && resultTotalMap.get(address.Contact__c) + 1  > cnt )
	                address.addError('There is a limit of ' + String.ValueOf(cnt) + ' address records');
	            else if (resultTotalMap.containskey(address.Contact__c) == false && address.Primary_Address__c == false )
	                address.Primary_Address__c = true;
	            
	            
	        }
        }
        
    }
    
    private void setPrimaryAddressUpdate (list<Addresses__c> newList , map<id,Addresses__c> oldMap)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        set<Id> contactId = new set<Id> ();
        set<Id> leadId = new set<Id> ();
        
        //Contact Logic 
        for (Addresses__c address : newList)
        {
        	if (address.Contact__c != null) 
            	contactId.add(address.Contact__c);
            if (address.Lead__c != null) 
            	leadId.add(address.Lead__c);
        }
        
        if (contactId.size() > 0)
        {
	        List<AggregateResult> results = [select Contact__c  , count(id) countOfId from Addresses__c where Primary_Address__c = true and Contact__c in :contactId Group BY Contact__c , Primary_Address__c];
	        map<string, integer> resultPrimaryMap = new map<string, integer> ();
	        
	        for (AggregateResult result :results )
	            resultPrimaryMap.put((string)result.get('Contact__c'), (Integer)result.get('countOfId'));
	        results = [select Contact__c  , count(id) countOfId from Addresses__c where Contact__c in :contactId Group BY Contact__c ];
	        map<string, integer> resultTotalMap = new map<string, integer> ();
	        for (AggregateResult result :results )
	            resultTotalMap.put((string)result.get('Contact__c'), (Integer)result.get('countOfId'));
	        
	       
	        if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
	            for (Addresses__c address : newList)
	            {
	                if (resultTotalMap.get(address.Contact__c) == 1 && address.Primary_Address__c == false  && !RecursiveFlagHandler.alreadyRunning(address.id)  )
	                {
	                    address.addError('There is only one address for this contact and it must be set as the primary address.');
	                }
	            }
	        } 
        } 
        
        if (leadId.size() > 0)
        {
	        List<AggregateResult> results = [select lead__c  , count(id) countOfId from Addresses__c where Primary_Address__c = true and lead__c in :leadId Group BY lead__c , Primary_Address__c];
	        map<string, integer> resultPrimaryMap = new map<string, integer> ();
	        
	        for (AggregateResult result :results )
	            resultPrimaryMap.put((string)result.get('lead__c'), (Integer)result.get('countOfId'));
	        results = [select lead__c  , count(id) countOfId from Addresses__c where lead__c in :leadId Group BY lead__c ];
	        map<string, integer> resultTotalMap = new map<string, integer> ();
	        for (AggregateResult result :results )
	            resultTotalMap.put((string)result.get('lead__c'), (Integer)result.get('countOfId'));
	        
	       
	        if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
	            for (Addresses__c address : newList)
	            {
	                if (resultTotalMap.get(address.lead__c) == 1 && address.Primary_Address__c == false )
	                {
	                    address.addError('There is only one address for this Lead and it must be set as the primary address.');
	                }
	            }
	        } 
        }         
    }
    private void resetLegalAddress (list<Addresses__c> newList , map<id, Addresses__c> updateList)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        //Get all associated Contacts marked Primary
        set<Id> contactIdSet = new set<Id> ();
        set<Id> leadIdSet = new set<Id> ();
        set <Id> changedAddress = new set <Id> ();

        list<string> affectedIds = new list<string>();
        
        for (Addresses__c address : newList)
        {
            if (!RecursiveFlagHandler.alreadyRunning('LA:' + address.id))
            {
                
                if (address.Legal_Address__c  && address.Contact__c != null  )
                {
                    changedAddress.add(address.id);
                    contactIdSet.add(address.Contact__c);
                    affectedIds.add('LA:' + address.id);
                }
                if (address.Legal_Address__c  && address.Lead__c != null)
                {
                    changedAddress.add(address.id);
                    leadIdSet.add(address.Lead__c);
                    affectedIds.add('LA:' + address.id);
                }
            }
            
        }
        
        
        //Exit the method if there are no records to process.
        if ( changedAddress.size() == 0)
            return;

        RecursiveFlagHandler.setRunning(affectedIds);
        
        /*-----  Handle the contact Addresses  ------*/
                
        //Get List of All Primary Addresses and set to false
        if (contactIdset.size() > 0)
        {
            List<Addresses__c> addressList = [select id, Legal_Address__c from Addresses__c where Contact__c in :contactIdset and Legal_Address__c = true and id not in :changedAddress];
            affectedIds.clear() ;

            for (Addresses__c address : addressList)
            {
                if (updateList.containskey(address.id) )
                {
                    Addresses__c addressMap =  updateList.get(address.id) ;
                    addressMap.Legal_Address__c = false;
                    updateList.put(address.id, addressMap) ;
                }
                else
                {
                    address.Legal_Address__c = false;
                    updateList.put(address.id, address) ;
                }
                affectedIds.add('LA:' + address.id);    
            }
            
            
            RecursiveFlagHandler.setRunning(affectedIds);
                
        }

        /*-----  Handle the Lead Addresses  .  Resets the old record back to false  ------*/
                
        //Get List of All Primary Addresses and set to false
        if (leadIdset.size() > 0)
        {
            List<Addresses__c> addressList = [select id, Legal_Address__c from Addresses__c where Lead__c in :leadIdset and Legal_Address__c = true and id not in :changedAddress];

            affectedIds.clear() ;
            for (Addresses__c address : addressList)
            {
                    
                if (updateList.containskey(address.id) )
                {
                    Addresses__c addressMap =  updateList.get(address.id) ;
                    addressMap.Legal_Address__c = false;
                    updateList.put(address.id, addressMap) ;
                }
                else
                {
                    address.Legal_Address__c = false;
                    updateList.put(address.id, address) ;
                }
                affectedIds.add('LA:' + address.id);      
            }
            
            
            RecursiveFlagHandler.setRunning(affectedIds);
            
                
        
        }
        
        
    }
    
    //Method sets the old Primary Flag back to false if a new record has been marked as Primary
    private void resetAddressPrimary (list<Addresses__c> newList , map<id , Addresses__c> updateList)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        //Get all associated Contacts marked Primary
        set<Id> contactId = new set<Id> ();
        set<Id> leadId = new set<Id> ();
        set <Id> changedAddress = new set <Id> ();
        list<string> affectedIds = new list<string>();
        
        for (Addresses__c address : newList)
        {
            if (!RecursiveFlagHandler.alreadyRunning(address.id))
            {
                
                if (address.Primary_Address__c && address.Contact__c != null)
                {
                    changedAddress.add(address.id);
                    contactId.add(address.Contact__c);
                }
                 if (address.Primary_Address__c && address.Lead__c != null)
                {
                    changedAddress.add(address.id);
                    leadId.add(address.Lead__c);
                }
            }
            affectedIds.add(address.id);
        }
        
        //Exit the method if there are no records to process.
        if (contactId.size() == 0 && changedAddress.size() == 0)
            return;
        
        
        
        //Get List of All Primary Addresses and set to false
        List<Addresses__c> addressList = [select id, Primary_Address__c from Addresses__c where (Contact__c in :contactId or Lead__c in :leadId )
        and Primary_Address__c = true and id not in :changedAddress];
        
        for (Addresses__c address : addressList)
        {
                address.Primary_Address__c = false;
                affectedIds.add(address.id);
                updateList.put(address.id , address) ;
                    
        }
        
        
        RecursiveFlagHandler.setRunning(affectedIds);
            
        
    }
    private void updateParentObject (list<Addresses__c> newList)
    {
        map<id,Addresses__c> addressMap = new map<id,Addresses__c> ();
        for (Addresses__c address : newList) 
        {
            if (address.Primary_Address__c)
            {
            	if (address.Lead__c != null)
            		addressMap.put(address.Lead__c , address);
            	else
                	addressMap.put(address.Contact__c , address);
            }
            
        }
        
        //Handle the contact items
        List<Contact> contactList = [select id, MailingStreet, MailingCity, MailingState, MailingPostalCode , MailingCountry from Contact where id in :addressMap.Keyset()];
        list<string> affectedIds = new list<string>();
        List<Contact> changedContactList = new list<Contact>();
        
        //Loop through the Contact Object and Update the Mailing Address
        for (Contact contact  : contactList)
        {
            if (addressMap.containskey(contact.id) && !RecursiveFlagHandler.alreadyRunning(contact.id) )
            {
                Addresses__c address = addressMap.get(contact.id);

                contact.MailingStreet = address.Street_Address__c ;

                //if (address.Street_Address_Line_2__c != null && address.Street_Address_Line_2__c.length() > 0)
                 //   contact.MailingStreet += '\n' + address.Street_Address_Line_2__c ;
				if(String.isNotEmpty(address.City__c) && address.City__c.length() > 40)
                	contact.MailingCity = address.City__c.substring(0, 40);
                else
                	contact.MailingCity = address.City__c;
                contact.MailingState = address.State__c;
                contact.MailingPostalCode = address.Postal_Code__c;
                contact.MailingCountry = address.Country__c;
                changedContactList.add(contact);
                affectedIds.add(contact.id);
            }
        }
        
        if (changedContactList.size() > 0)
        {
            RecursiveFlagHandler.setRunning(affectedIds);
            update changedContactList;
           
        }
        
         //Handle the lead items
        List<Lead> LeadList = [select id,Street, City, State, PostalCode from lead where id in :addressMap.Keyset() and IsConverted  = false];
        affectedIds = new list<string>();
        List<Lead> changedLeadList = new list<Lead>();
        
        //Loop through the Lead Object and Update the Mailing Address
        for (Lead ld  : LeadList)
        {
            if (addressMap.containskey(ld.id) && !RecursiveFlagHandler.alreadyRunning(ld.id) )
            {
                Addresses__c address = addressMap.get(ld.id);

                ld.Street = address.Street_Address__c ;

                //if (address.Street_Address_Line_2__c != null && address.Street_Address_Line_2__c.length() > 0)
                 //   Lead.MailingStreet += '\n' + address.Street_Address_Line_2__c ;
				if(String.isNotEmpty(address.City__c) && address.City__c.length() > 40)
                	ld.City = address.City__c.substring(0, 40);
                else
                	ld.City = address.City__c;
                ld.State = address.State__c;
                ld.PostalCode = address.Postal_Code__c;
                ld.Country = address.Country__c;
                changedLeadList.add(ld);
                affectedIds.add(ld.id);
            }
        }
        
        if (changedLeadList.size() > 0)
        {
            RecursiveFlagHandler.setRunning(affectedIds);
            update changedLeadList;
           
        }
    }
    
    private void UnCheckPrimaryCheck (list<Addresses__c> newList  , map<id,Addresses__c> oldMap)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        //Check to make sure the Primary Address is not getting deleted
        for (Addresses__c address : newList) 
        {
            if (!RecursiveFlagHandler.alreadyRunning(address.id) && address.Primary_Address__c == false && oldMap.get(address.id).Primary_Address__c == true)
            {
                address.addError('The primary address can not be unchecked.  Select another address to be the primary Address.');
            }
        	
        }
    }

    private void UnCheckLegalCheck (list<Addresses__c> newList  , map<id,Addresses__c> oldMap)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        //Check to make sure the Primary Address is not getting deleted
        for (Addresses__c address : newList) 
        {
            if (!RecursiveFlagHandler.alreadyRunning('LA:'+ address.id) && address.Legal_Address__c == false && oldMap.get(address.id).Legal_Address__c == true)
            {
                address.addError('The legal address can not be unchecked.  Select another address to be the Legal Address.');
            }
            
        }
    }
    
    private void DeletePrimaryCheck (list<Addresses__c> oldList)
    {
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        //Check to make sure the Primary Address is not getting deleted
        for (Addresses__c address : oldList) 
        {
            if (address.Primary_Address__c == true)
                address.addError('The primary address can not be deleted.  Select another primary address then delete the address.');
        
        }
    }
    
    private void DeleteMailingCheck (list<Addresses__c> oldList)
    {
    	 if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
            return;
            
        //Get the Address List
        set<string> addressSet = new set<string>() ;
        for (Addresses__c address : oldList) 
        {
            addressSet.add(address.id);
        }
        List<Mailing__c> mailings = [SELECT Address__c FROM Mailing__c where id in :addressSet];
        set<string> mailSet = new set<string>() ;
        for (Mailing__c m : mailings)
        	mailSet.add(m.Address__c);
        	
        for (Addresses__c address : oldList) 
        {
            if(mailSet.contains(address.id))
                address.addError('The Address can not be deleted it is included in a mailing');
        }
        
    }

    //This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
    //completed successfully.  The Informatica job that syncs Addresses EDB-->SFDC will use this data as a source to update
    //status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Addresses__c> recordList, String operation) {
        
        //records should only be added to the data sync log if the current user is the Bessemer Integration user
        if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
        
            list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
            for (Addresses__c addr : recordList){
	 			if(addr.EDB_Entity_Id__c <> null){
	                Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
	                syncRecord.EDB_Entity_Id__c = addr.EDB_Entity_Id__c;
	                syncRecord.Entity__c = 'ADDR';
	                syncRecord.Action__c = Operation;
	                syncList.Add(syncRecord);
	 			}
            }   

			if(synclist.size() > 0)
				insert syncList;
        }
    }
    //This method will assign a new address number to any address that are created within Salesforce.com.
	//It assumes that any addresses created by the integration user account were created via Informatica.
	private void assignAddressNumber (list<Addresses__c> newList)
	{
        set <Id> changedRelationship = new set <Id> ();
	 	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c)
	 		return;
	 	
	 	GlobalSettings__c settings = [SELECT Id, Address_Number_Seq__c FROM GlobalSettings__c];
		Decimal nextRelationshipNumber = settings.Address_Number_Seq__c;
	 	boolean isUpdated = false;
	 	
		for (Addresses__c a : newList) 
		{
	    	
			if(a.Address_Number__c == null) 
			{
	            nextRelationshipNumber = nextRelationshipNumber + 1;
	            a.Address_Number__c  = String.valueof(nextRelationshipNumber);
	            isUpdated = true;
			}
	    
		}

	
		if(isUpdated) 
		{
			settings.Address_Number_Seq__c = nextRelationshipNumber;
			update settings;
		}
		
	}       

}