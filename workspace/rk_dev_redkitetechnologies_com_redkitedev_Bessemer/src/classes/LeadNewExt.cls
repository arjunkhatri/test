public with sharing class LeadNewExt extends PageControllerBase
{
	public Lead leadRec { get; set; }
	public String columnsToDisplay { get; set; }
	private Lead originalRecord;
	public Addresses__c AddressObj {get;set;}


	//** Dupeblocker Setup **/
	public DupeBlockerListCtrl myComponentController { get; set; }
	public override void setComponentController( ComponentControllerBase compController )
	{
		myComponentController = (DupeBlockerListCtrl)compController;
	}

	public override ComponentControllerBase getMyComponentController()
	{
		return myComponentController;
	}
	transient Boolean isOverride;
	String leadId;
	
	/**************  End DupBlocker Setup **************/
	public LeadNewExt( ApexPages.StandardController stdController )
	{
		leadId = stdController.getId();
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();
		if( platForm == 'Phone' )
			columnsToDisplay = '1';
		else
			columnsToDisplay = '2';
	}
	
	public void init()
	{
		if( ApexPages.currentPage().getParameters().get('id') ==  null )
			this.leadRec = new Lead();
		else
			this.leadRec = Database.query( 'SELECT ' + Utility_Common.getFieldList( 'Lead' ) + ' FROM Lead WHERE Id = \'' + leadId + '\'' );

		AddressObj = new Addresses__c();
		AddressObj.State__c = this.leadRec.State;
		if (this.leadRec.Country == null)
			AddressObj.Country__c = '';
		else
			AddressObj.Country__c = this.leadRec.Country;

		originalRecord = leadRec.clone();
	}

	public PageReference saveRecord()
	{
		this.leadRec.State = AddressObj.State__c  ;
		this.leadRec.Country = AddressObj.Country__c  ;
		//Reset the Dupblocker Bypass Field if the field values have changed.
		if( leadRec.Id != null )
		{
			List<CRMfusionDBR101__Scenario__c> dupblockerScenario = [ SELECT Name, CRMfusionDBR101__Scenario_Type__c, CRMfusionDBR101__Allow_Block_Bypass__c, CRMfusionDBR101__Deployed__c,
																		( SELECT CRMfusionDBR101__Field_Name__c FROM CRMfusionDBR101__Scenario_Rules__r )
																	  FROM CRMfusionDBR101__Scenario__c
																	  WHERE CRMfusionDBR101__Allow_Block_Bypass__c = true
																			AND CRMfusionDBR101__Deployed__c = true
																			AND CRMfusionDBR101__Scenario_Type__c = 'Lead' ];
			Set<String> fieldSet = new Set<String>();
			for( CRMfusionDBR101__Scenario__c scenario : dupblockerScenario )
			{
				List<CRMfusionDBR101__Scenario_Rule__c> fieldList = scenario.getSObjects( 'CRMfusionDBR101__Scenario_Rules__r' );
				for( CRMfusionDBR101__Scenario_Rule__c field : fieldList )
				{
					if( !fieldSet.contains( field.CRMfusionDBR101__Field_Name__c ) )
						fieldSet.add( field.CRMfusionDBR101__Field_Name__c );
				}
			}

			for( String key : fieldSet )
			{
				if( String.valueOf( originalRecord.get( key ) ) != String.valueOf( leadRec.get( key ) ) )
				{
					leadRec.Dupe_Blocker_Bypass__c = false;
					break;
				}
			}
		}

		Boolean hasDuplicates = false;
		hasDuplicates = myComponentController.checkLeadDups( leadRec );

		if( hasDuplicates )
			return null;

		try  {
			AddAddress ();
			upsert leadRec;


		}
		catch (Exception ex)
		{
			return null;
			//ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() )) ;
		}
		PageReference returnURL;
		returnURL = new PageReference( '/' + leadRec.Id );
		return returnURL;
	}

	private void AddAddress ()
	{//Add the Address Record
			List<Addresses__c> addList  = [SELECT Lead__c, Street_Address__c, 
				Country__c,   City__c, State__c, Postal_Code__c, Primary_Address__c FROM Addresses__c where Primary_Address__c = true and Lead__c = :leadRec.id];
			if (addList.isEmpty() )
			{
				if (leadRec.Street != null || leadRec.City != null || leadRec.State != null || leadRec.PostalCode != null)
				{
					Addresses__c add = new Addresses__c ();
					add.Lead__c = leadRec.id;
					add.Primary_Address__c = true ;
					add.Address_Type__c = 'Home';
					add.Street_Address__c = leadRec.Street;
					add.City__c = leadRec.City;
					add.State__c = leadRec.State;
					add.Postal_Code__c = leadRec.PostalCode;
					add.Country__c = leadRec.Country;
					insert add;
				}
			}
			else
			{
					Addresses__c add = addList[0];
					
					add.Street_Address__c = leadRec.Street;
					add.City__c = leadRec.City;
					add.State__c = leadRec.State;
					add.Postal_Code__c = leadRec.PostalCode;
					add.Country__c = leadRec.Country;
					update add;
					
			}
	}
	public PageReference saveRecordOverride()
	{
		//if (isOverride )
		//{
		myComponentController.overRideDupeBlocker();
		leadRec.Dupe_Blocker_Bypass__c = true;
		try  
		{
			AddAddress ();
			upsert leadRec;
			
		}
		catch (Exception ex)
		{
			return null;
			//ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() )) ;
		}

		PageReference returnURL;
		returnURL = new PageReference('/' + leadRec.Id);
		return returnURL;
		//}

//		return null;
	}
}