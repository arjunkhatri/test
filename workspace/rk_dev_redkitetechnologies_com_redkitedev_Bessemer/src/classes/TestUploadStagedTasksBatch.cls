@isTest
public with sharing class TestUploadStagedTasksBatch {

	static testMethod void uploadStagedTasksBatchTest(){
		Activity_Staging__c actStg = testdatafactory.createActivityStaging();
		insert actStg;
		
		Account accnt = testdatafactory.createTestAccount();
			accnt.Relationship_Number__c = 'Rel002';
		insert accnt;
		
		Accounts__c acc = testdatafactory.customAccount(accnt.id);
			insert acc;
			
		SubAccounts__c subAcc = testdatafactory.createSubAccount('SA001', accnt.id);
		subAcc.Account__c = acc.id;
		insert subAcc;
		
		Activity_Staging__c actStg1 = testdatafactory.createActivityStaging();
			actStg1.Activity_Id__c = '1235';
			actStg1.Relationship_Number__c = 'Rel002';
			actStg1.SubAccount_Number__c = 'SA001';
		insert actStg1;
				
	 	Test.StartTest();
	 	UploadStagedTasksBatch tskBatch = new UploadStagedTasksBatch();
	 	database.executeBatch(tskBatch);
	 	Test.stopTest(); 		
	}

}