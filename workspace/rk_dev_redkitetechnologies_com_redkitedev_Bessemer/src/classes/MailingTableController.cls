public class MailingTableController {
	
    public MailingTableEntry[] tableEntries{get;set;}
    public String tableType{get;set;}
    public Boolean massEditAddr {get; set; }
    public Boolean mainMode{get;set;}
    public Contact contact{get;set;}
  
    public MailingTableController(){}
    
}