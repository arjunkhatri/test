@isTest(SeeAllData=true) 
public with sharing class RelationshipTrigHandlerTest { 
   static testMethod void testRelationshipTrigHandler() 
    {
		Profile myProf = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
		system.assert(myProf.id != null,'Profile id fetched successfully');
		
		User u = TestDataFactory.createTestUser(myProf.id); 
		insert u;    	
		system.assert(u.id != null,'user record created successfully');
		    	
        Account acc = TestDataFactory.createTestAccount();
        	acc.Relationship_Number__c = 'Rel12344';
        insert acc;
        system.assert(acc.id != null,'Account record created successfully');
        
        Contact con = TestDataFactory.createTestContact(acc);
        insert con;
 		system.assert(con.id != null,'Contact record created successfully');   
    	List<Account> acnt = [Select Ownerid from Account where id=:acc.id limit 1];
    	acnt[0].Ownerid = u.id;
    	update acnt;
    }
}