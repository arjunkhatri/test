@isTest
private class TestGlobalContactSearch {

    static testMethod void GlobalContactSearch_AccountSearch_Test() 
    {
        createTestData () ; 

        List<Account> accountList = TestDataCreator_Utils.createSObjectList('Account' , false , 40);
        Id [] fixedSearchResults= new Id[40];
        for (integer i = 0 ; i< 40 ; i++)
        {
            accountList[i].Name = 'TestAccount' ;
            accountList[i].Relationship_Number__c = 'A:' + i;
            accountList[i].Estimated_Net_Worth__c = i * 500;
        }
            
        insert accountList;
        for (integer i = 0 ; i< 40 ; i++)
            fixedSearchResults[i] = accountList[i].id;
            
            
        Test.setFixedSearchResults(fixedSearchResults); 
        PageReference pageRef = Page.GlobalSearch;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
        Test.startTest();
        

        GlobalContactSearch myCon = new GlobalContactSearch();
        myCon.searchTerm = 'TestAccount' ; 
        myCon.performSearch ();
        System.AssertEquals('40', myCon.accountResultsCount);
        myCon.accountLastPage();
        myCon.accountFirstPage() ;
        myCon.accountNextPage();
        myCon.accountPreviousPage();
        
        //Test Sorting
        myCon.accountSortField  = 'Estimated_Net_Worth__c' ;
        myCon.SortAccountList ();
        
        //Test the Account Filtering
        myCon.accountRelationshipNumberFilter = 'A:1';
        myCon.ApplyaccountFilter() ;
        List<String> tmpList =  myCon.accountFilterStrings;
        System.AssertEquals(3, tmpList.size() );

        tmpList =  myCon.contactFilterStrings ;
        System.AssertEquals(2, tmpList.size() );

        tmpList =  myCon.leadFilterStrings ;
        System.AssertEquals(3, tmpList.size() );

        List<GlobalContactSearch.AccountResultSet> resultSet = myCon.accountSearchResults;
        System.AssertEquals('1', myCon.accountResultsCount);
        
        
        
        Test.stopTest();
    }
    
    static testMethod void GlobalContactSearch_ContactSearch_Test() 
    {
        createTestData () ;

        List<Contact> ContactList = TestDataCreator_Utils.createSObjectList('Contact' , false , 40);
        Id [] fixedSearchResults= new Id[40];
        for (integer i = 0 ; i< 40 ; i++)
        {
            ContactList[i].LastName = 'TestAccount' ;
            ContactList[i].Email = 'Test' + String.ValueOf(i) + '@test.com' ;
        }
        insert contactList;
        for (integer i = 0 ; i< 40 ; i++)
            fixedSearchResults[i] = contactList[i].id;
            
            
        Test.setFixedSearchResults(fixedSearchResults); 
        PageReference pageRef = Page.GlobalSearch;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');

        Test.startTest();
        
        GlobalContactSearch myCon = new GlobalContactSearch();
        myCon.searchTerm = 'TestAccount' ; 
        myCon.performSearch ();
        System.AssertEquals(20, myCon.contactSearchResults.size());
        myCon.contactLastPage();
        myCon.contactFirstPage() ;
        myCon.contactNextPage();
        myCon.contactPreviousPage();
        
        //Test Sorting
        myCon.contactSortField = 'LastName';
        myCon.SortContactList();
        
        myCon.contactSortField = 'RelationshipCount';
        myCon.SortContactList();
        
        
        List<GlobalContactSearch.ContactResultSet> resultSet = myCon.contactSearchResults;
        
        
        
        
        Test.stopTest();
    }
    
    static testMethod void GlobalContactSearch_LeadSearch_Test() 
    {
        createTestData () ;

        List<Lead> leadList = TestDataCreator_Utils.createSObjectList('Lead' , false , 40);
        Id [] fixedSearchResults= new Id[40];
        for (integer i = 0 ; i< 40 ; i++)
        {
            leadList[i].Company = 'TestAccount' ;
            leadList[i].Email = 'Test' + String.ValueOf(i) + '@test.com' ;
        }
        insert leadList;
        for (integer i = 0 ; i< 40 ; i++)
            fixedSearchResults[i] = leadList[i].id;
            
            
        Test.setFixedSearchResults(fixedSearchResults); 
        PageReference pageRef = Page.GlobalSearch;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');

        Test.startTest();
        
        GlobalContactSearch myCon = new GlobalContactSearch();
        myCon.searchTerm = 'TestAccount' ; 
        myCon.performSearch ();
        System.AssertEquals('40', myCon.leadResultsCount);
        myCon.leadFirstPage() ;
        myCon.leadNextPage();
        myCon.leadPreviousPage();
        
        //test Sorting Fields 
        myCon.leadSortField = 'Company';
        myCon.SortLeadList();
        
        //Test sending request access
        
        List<GlobalContactSearch.LeadResultSet> resultSet = myCon.leadSearchResults;
        
        
        
        Test.stopTest();
    }

    static testMethod void GlobalContactSearch_Test1() 
    {
        createTestData () ;

        List<Account> accountList = TestDataCreator_Utils.createSObjectList('Account' , false , 1);
        insert accountList ;
        
        Test.startTest();
        string strOwner = GlobalContactSearch.getOwner (accountList[0].id);    
        System.assertEquals(strOwner ,  userInfo.getName() ) ;
        Test.stopTest();
    }

    @isTest (SeeAllData=True)
    static void  GlobalContactSearch_TestDupBlockerContact() 
    {
        List<Contact> contactList = TestDataCreator_Utils.createSObjectList('Contact' , false , 2);
        contactList[0].email = 'testMthod99@testXYX.com';
        insert contactList[0] ;
        contactList[1].email = 'testMthod99@testXYX.com';
        Test.startTest();
        List<string> rs  = new List<string>  ();
        List<Boolean> Dups = new List<Boolean> ();
        GlobalContactSearch gcs = new GlobalContactSearch();
        gcs.CheckForDuplicates (contactList[1] , rs, dups)  ;
        System.assertEquals(1 ,  rs.size() ) ;
        Test.stopTest();
    }
     @isTest (SeeAllData=true)
    static void  GlobalContactSearch_TestDupBlockerLead() 
    {
        List<Lead> leadList = TestDataCreator_Utils.createSObjectList('Lead' , false , 2);
        leadList[0].email = 'testMthod99@testXYX.com';
        insert leadList[0] ;
        leadList[1].email = 'testMthod99@testXYX.com';
        Test.startTest();
        List<string> rs  = new List<string>  ();
        List<Boolean> Dups = new List<Boolean> ();
        GlobalContactSearch gcs = new GlobalContactSearch();
        gcs.checkForLeadDuplicates (leadList[1] , rs, dups)  ;
        System.assertEquals(0 ,  rs.size() ) ;
        Test.stopTest();
    }

    private static void createTestData ()
    {
        /********** Create test data *****************/
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        List<GlobalSearchFields__c> gsfList = new List<GlobalSearchFields__c>();
        
        /**** Account Fields *****************/
        GlobalSearchFields__c gsf = new GlobalSearchFields__c ();
        gsf.Name = 'AC1';
        gsf.Field__c = 'Name' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Account' ;
        gsf.SortOrder__c = 1;
        gsfList.add(gsf);

        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'AC2';
        gsf.Field__c = 'Website' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Account' ;
        gsf.SortOrder__c = 2;
        gsfList.add(gsf);

        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'AC3';
        gsf.Field__c = 'Estimated_Net_Worth__c' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Account' ;
        gsf.SortOrder__c = 3;
        gsfList.add(gsf);


        /************* Contact Fields *********/        
        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'Ct1';
        gsf.Field__c = 'FirstName' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Contact' ;
        gsf.SortOrder__c = 1;
        gsfList.add(gsf);

        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'Ct2';
        gsf.Field__c = 'LastName' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Contact' ;
        gsf.SortOrder__c = 2;
        gsfList.add(gsf);


        /****** Lead Fields*****/
        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'LD1';
        gsf.Field__c = 'FirstName' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Lead' ;
        gsf.SortOrder__c = 1;
        gsfList.add(gsf);

        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'LD2';
        gsf.Field__c = 'LastName' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Lead' ;
        gsf.SortOrder__c = 2;
        gsfList.add(gsf);

        gsf = new GlobalSearchFields__c ();
        gsf.Name = 'LD3';
        gsf.Field__c = 'Company' ;
        gsf.Platform__c = 'Desktop' ;
        gsf.Object__c = 'Lead' ;
        gsf.SortOrder__c = 3;
        gsfList.add(gsf);

        insert gsfList;


         List<GlobalSearchFilter__c> gsftList = new List<GlobalSearchFilter__c>();
        
        /**** Account Fields *****************/
        GlobalSearchFilter__c gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'AC1';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Account' ;
        gsft.SortOrder__c = 1;
        gsftList.add(gsft);

        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'AC2';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Account' ;
        gsft.SortOrder__c = 2;
        gsftList.add(gsft);

        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'AC3';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Account' ;
        gsft.SortOrder__c = 3;
        gsftList.add(gsft);

        /************* Contact Fields *********/        
        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'Ct1';
         gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Contact' ;
        gsft.SortOrder__c = 1;
        gsftList.add(gsft);

        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'Ct2';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Contact' ;
        gsft.SortOrder__c = 2;
        gsftList.add(gsft);

        /****** Lead Fields*****/
        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'LD1';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Lead' ;
        gsft.SortOrder__c = 1;
        gsftList.add(gsft);

        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'LD2';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Lead' ;
        gsft.SortOrder__c = 2;
        gsftList.add(gsft);

        gsft = new GlobalSearchFilter__c ();
        gsft.Name = 'LD3';
        gsft.Platform__c = 'Desktop' ;
        gsft.Object__c = 'Lead' ;
        gsft.SortOrder__c = 3;
        gsftList.add(gsft);

        insert gsftList;
    }
    
   
}