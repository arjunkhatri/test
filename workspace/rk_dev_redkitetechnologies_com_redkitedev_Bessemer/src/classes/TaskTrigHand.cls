public with sharing class TaskTrigHand {
    public static final string NOREMINDER = '--No Email Reminder Set--';
    public static final string ONDUEDATE = '--On Due Date--';
    public static final string NONE = '�None�';
    public string oldEmailReminder = '';

    public void onBeforeInsert(list<Task> newList) {
        getRelationshipTeamList(newList);
        setEmailReminderDate(newList);
        setCompletedDateOnInsert(newList);
    }

    public void onBeforeUpdate(list<Task> newList, map<id,Task> oldMap) {
        getUpdatedTask(newList,oldMap);
        updateEmailReminderDate(newList,oldMap);
        setCompletedDateOnUpdate(newList,oldMap);
    }
    public void onBeforeDelete(list<Task> oldList){
        
    }
    
    //If the Email Reminder field is populated with a value other than �None�or 
    //�No Email Reminder Set --, set Email Reminder Date to Due Date minus Email Reminder value
    //17/04/15 - If the Email Reminder field is populated with a value with �None�or 
    //�No Email Reminder Set --, set Email Reminder Date to null
    private void setEmailReminderDate(list<Task> newListTask){
        for(Task tsk:newListTask){
            if(tsk.Email_Reminder_Picklist__c != NOREMINDER && tsk.Email_Reminder_Picklist__c != NONE
                  && tsk.Email_Reminder_Picklist__c != '' && tsk.Email_Reminder_Picklist__c != null)
                {
                    integer picklistDay = getDays(tsk.Email_Reminder_Picklist__c);
                    if(picklistDay != 0 && tsk.ActivityDate != null)
                    {
                        tsk.Email_Reminder_Date__c = tsk.ActivityDate.addDays(-picklistDay);
                    }   
                }
             else if(tsk.Email_Reminder_Picklist__c == NOREMINDER || tsk.Email_Reminder_Picklist__c == NONE)
                 tsk.Email_Reminder_Date__c = null;
             if(tsk.Email_Reminder_Picklist__c == ONDUEDATE)   
                 tsk.Email_Reminder_Date__c = tsk.ActivityDate;
        }
    }

    private void updateEmailReminderDate(list<Task> newListTask,map<id,Task> oldMap){
        List<Task> lstTask = new List<Task>();
        for(Task tsk:newListTask){
            if(oldMap.get(tsk.id).Email_Reminder_Picklist__c != null && oldMap.get(tsk.id).Email_Reminder_Picklist__c != '')
               oldEmailReminder = oldMap.get(tsk.id).Email_Reminder_Picklist__c;
            if( oldMap.containsKey(tsk.id) && tsk.Email_Reminder_Picklist__c != oldMap.get(tsk.id).Email_Reminder_Picklist__c){
                if(oldEmailReminder != null && oldEmailReminder != '' && 
                    oldEmailReminder.equals(NOREMINDER) || oldEmailReminder.equals(NONE)){
                       tsk.Email_Reminder_Date__c = null;
                    }
                lstTask.add(tsk);
            }

        }
        if(lstTask != null && lstTask.size() > 0)
            setEmailReminderDate(lstTask);
    }

    public integer getDays(String strDays){
        if(strDays.contains('day') || strDays.contains('days') && strDays.contains(' ')) 
            return integer.valueOf(strDays.split(' ')[0]);
        else if (strDays.contains('weeks'))
            return 14;
        else if (strDays.contains('week'))
            return 7;
        return 0;   
    }
    
    private void getRelationshipTeamList(list<Task> newListTask){
        Set<String> listRelationship = new Set<String>();
        List<String> listAccount = new List<String>();
        //map of relationship vs team member
        Map<String,List<String>> mapRelToMem = new Map<String,List<String>>();
        Map<String,String> mapAccToRel = new Map<String,String>();
        for(Task tsk:newListTask){
            if(tsk.WhatId != null){
                String whtId = String.valueOf(tsk.WhatId);
                if(whtId.length() == 18)
                    whtId = whtId.substring(0, 15);
                if(whtId != null && whtId != '' && whtId.Startswith('001')){
                    listRelationship.add(whtId);
                }//check for whatid contains Account id
                else if (whtId != null && whtId != '' && 
                    String.valueOf(Id.valueOf(whtId).getSobjectType()) == 'Accounts__c'){
                    listAccount.add(whtId);
                }
            }
        }
        //get relationships from Accounts
        if(listAccount != null && listAccount.size() > 0){
            for(Accounts__c accRec:[Select id,
                                    Relationship__c 
                                    From Accounts__c where id=:listAccount])
            {
                listRelationship.add(accRec.Relationship__c);
                mapAccToRel.put(accRec.id,accRec.Relationship__c);
            }
        }
        if(listRelationship != null && listRelationship.size() > 0){
            for(AccountTeamMember atm:[Select UserId,
                                            AccountId 
                                            From AccountTeamMember 
                                            where AccountId = :listRelationship])
            {
                if(!mapRelToMem.containsKey(atm.AccountId)){
                    mapRelToMem.put(atm.AccountId ,new List<String>{atm.UserId});   
                }else{
                    mapRelToMem.get(atm.AccountId).add(atm.UserId);
                }                                                 
            }
        }
        for(Task newTsk:newListTask){
            String strTeamList = '';
            List<String> teamMem;
            if(newTsk.WhatId != null){
                if(mapRelToMem.containsKey(newTsk.WhatId)){
                    teamMem = mapRelToMem.get(newTsk.WhatId);
                }
                else if(String.valueOf(Id.valueOf(newTsk.WhatId).getSobjectType()) == 'Accounts__c'){
                    String relId = mapAccToRel.get(newTsk.WhatId);
                    if(relId != null && relId != ''){
                        teamMem = mapRelToMem.get(relId);
                    }
                }
                if(teamMem != null && teamMem.size() > 0){
                    for(String tMem:teamMem){
                        if(tMem.length() == 18)
                            tMem = tMem.substring(0, 15);
                        if(strTeamList.length() <= 238) //2014-10-31 DK: There is a max size of 255 chars on custom activity custom fields
                            strTeamList = strTeamList + '#' + tMem + '#';
                    }
                }
                if(strTeamList != null && strTeamList != ''){
                    newTsk.Relationship_Team_List__c = strTeamList;
                }
                else{
                    newTsk.Relationship_Team_List__c = '';
                }
            }
            else{
                newTsk.Relationship_Team_List__c = '';
            }
        }
    }
    public void getUpdatedTask(list<Task> newList, map<id,Task> oldMap){
        List<Task> listTask = new List<Task>();
        for(Task upTask:newList){
            if(upTask.AccountId != oldMap.get(upTask.id).AccountId || 
               upTask.WhatId != oldMap.get(upTask.id).WhatId){
                listTask.add(upTask);
            }
        }
        if(listTask != null && listTask.size() > 0){
            getRelationshipTeamList(listTask);
        }
    }

    private Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

    public void onAfterInsert(List<Task> newTasks) {
        addAccountsToUpdateList(newTasks);
        update accountsToUpdate.values();
    }

    public void onAfterUpdate(List<Task> newTasks, Map<Id, Task> oldTaskMap) {
        addAccountsToUpdateList(newTasks);
        addAccountsToUpdateList(oldTaskMap.values());
        update accountsToUpdate.values();
    }

    public void onAfterDelete(List<Task> oldTasks) {
        addAccountsToUpdateList(oldTasks);
        update accountsToUpdate.values();
        validateTaskDeletions(oldTasks);
    }

    public void onAfterUndelete(List< Task> newTasks) {
        addAccountsToUpdateList(newTasks);
        update accountsToUpdate.values();
    }

    private void addAccountsToUpdateList(List<Task> tasks) {
        for(Task t : tasks) {
            if(t.AccountId != Null) {
                accountsToUpdate.put(t.AccountId, new Account(Id = t.AccountId));
            }
        }
    }

    private void setCompletedDateOnInsert(List<Task> newTasks) {
        for(Task newTask : newTasks) {
            if(newTask.Status == 'Done') {
                newTask.Completed_Date_Stored__c = Date.today();
            }
        }
    }

    private void setCompletedDateOnUpdate(List<Task> newTasks, Map<Id, Task> oldTaskMap) {
        for(Task newTask : newTasks) {
            Task oldTask = oldTaskMap.get(newTask.Id);
            if(newTask.Status == 'Done' && oldTask.Status != 'Done') {
                newTask.Completed_Date_Stored__c = Date.today();
            }
        }
    }

    private void validateTaskDeletions(List<Task> oldTasks) {
        for(Task theTask : oldTasks) {
            if(theTask.Type == 'ITR-Followup') {
                System.debug('Task ' + theTask.Id + ': this Task cannot be deleted since it is of type ITR-Followup');
                theTask.addError('This Task cannot be deleted since it is of type ITR-Followup');
            }
        }
    }
}