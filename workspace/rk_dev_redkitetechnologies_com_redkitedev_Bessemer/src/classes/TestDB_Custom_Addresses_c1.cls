@isTest private without sharing class TestDB_Custom_Addresses_c1
{
    @isTest (SeeAllData=false)
    private static void testTrigger()
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        gs.Compliance_Email_Address__c = 'test@test.com' ;
        insert gs;
        
        CRMfusionDBR101.DB_Globals.triggersDisabled = true;
        //sObject testData = CRMfusionDBR101.DB_TriggerHandler.createTestData( Addresses__c.getSObjectType() );
        List<Addresses__c> testData =  TestDataCreator_Utils.createSObjectList('Addresses__c' , true , 1); 
        testData[0].Primary_Address__c = true;
        Test.startTest();
        insert testData;
        update testData;
        CRMfusionDBR101.DB_Globals.generateCustomTriggerException = true;
        update testData;
        //delete testData;
        Test.stopTest();
    }
}