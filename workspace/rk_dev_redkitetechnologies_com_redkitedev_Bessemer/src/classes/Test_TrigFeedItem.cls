@isTest
private class Test_TrigFeedItem {
    
    static testMethod void Test_TrigFeedItem_Test1() 
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        gs.Compliance_Email_Address__c = 'test@test.com' ;
        insert gs;

        Chatter_Compliance_Field_Mapping__c ccfm = new Chatter_Compliance_Field_Mapping__c ();
        ccfm.Name = 'Contact';
        ccfm.EDB_Id_Field__c = 'Contact_S_Number__c' ;
        ccfm.Object_API_Name__c = 'Contact' ;
        ccfm.Object_Id_Prefix__c = '003';
        insert ccfm;

        Contact ct =  (Contact )TestDataCreator_Utils.createSObject('Contact' , false , 1);
        insert ct;
        Test.startTest();
        FeedItem feedItm = new FeedItem();
        feedItm.ParentId = ct.id;
        feedItm.Body = 'Test Me' ;
        feedItm.Type ='TextPost';
        insert feedItm ;
        Test.stopTest();
    }
}