@isTest(seeAlldata = false)
public with sharing class TestRacSubmitCrtl 
{
     static testMethod void racSubmitCrtltest()
     {
        GlobalSettings__c gs = new GlobalSettings__c();
            gs.Contact_S_Number_Seq__c = 1111;
            gs.Relationship_Number_Seq__c  =1111;
            gs.Address_Number_Seq__c = 2222;
            gs.Compliance_Email_Address__c = 'test@test.com' ;
            gs.RAC_Tool_URL__c = 'test';
            insert gs;
            system.assert(gs.id != null,'Record GlobalSettings__c existing');
        RAC_Relationship_Required_Fields__c rrf = new RAC_Relationship_Required_Fields__c();
            rrf.Name = 'Category';
            rrf.FieldApi__c = 'Category__c';
            insert rrf;
            system.assert(rrf.id != null,'Record RAC_Relationship_Required_Fields__c existing');
        RAC_Contact_Required_Fields__c crf = new RAC_Contact_Required_Fields__c();
            crf.name = 'Email';
            crf.FieldApi__c = 'Email';
            insert crf;
        RAC_Contact_Required_Fields__c crf1 = new RAC_Contact_Required_Fields__c();
            crf1.name = 'LastName';
            crf1.FieldApi__c = 'LastName';
            insert crf1;
        RAC_Contact_Required_Fields__c crf2 = new RAC_Contact_Required_Fields__c();
            crf2.name = 'Role on Relationship(s)';
            crf2.FieldApi__c = 'Connection__r.Role_on_Relationship__c';
            insert crf2;
            system.assert(crf2.id != null,'Record RAC_Contact_Required_Fields__c existing');
        RAC_Entity_Required_Fields__c erf = new RAC_Entity_Required_Fields__c();
            erf.Name = 'Entity Name';
            erf.FieldApi__c = 'Entity_Name__c';
            insert erf;
        RAC_Entity_Required_Fields__c erf1 = new RAC_Entity_Required_Fields__c();
            erf1.Name = 'Country of Organization';
            erf1.FieldApi__c = 'Country_of_Organization__c';
            insert erf1;
            system.assert(erf1.id != null,'Record RAC_Contact_Required_Fields__c existing');
        RecordType rType = [Select Name, Id From RecordType where name='R:C' limit 1];
            system.assert(rType.id != null,'Record type existing');
        RecordType rType1 = [Select Name, Id From RecordType where DeveloperName ='Entity_Contact' limit 1];
            system.assert(rType1.id != null,'Record type existing');
        Account acc = TestDataFactory.createTestAccount();
            insert acc;
            system.assert(acc.id != null,'Created relationship record');
        Contact cont = TestDataFactory.createTestContact(acc);
            cont.RecordTypeId = rType1.Id;
            insert cont;
            system.assert(cont.id != null,'Created contact record');
        Connections__c con = TestDataFactory.createConnection(acc.id, cont.id);
            con.RecordTypeId = rType.id;
            con.Role_on_Relationship__c = 'Primary Decision Maker';
            insert con;
            system.assert(con.id != null,'Created Connection record');
         Test.startTest();
         ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(acc);
         RacSubmitCrtl rac = new RacSubmitCrtl(sc); 
         rac.init();
         Test.stopTest();
     }
}