@isTest(SeeAllData=true) 
public class TestManageMailingEdit_Controller {

    static testMethod void TestMethod1() {
    	
        Account rel = TestDataFactory.createTestAccount();
        insert rel;
        system.assert(rel.id != null,'Account record created successfully');
        
        Contact con = TestDataFactory.createTestContact(rel);
        insert con;
 		system.assert(con.id != null,'Contact record created successfully');   
    	//List<Account> acnt = [Select Ownerid from Account where id=:rel.id limit 1];
    	
        Accounts__c act = TestDataFactory.customAccount(rel.Id);
		insert act;
        
        Subaccounts__c subact = TestDataFactory.createSubAccount('12222',rel.Id);
        insert subact;
        
        Addresses__c add = new Addresses__c(Address_Type__c ='Home', 
/** ### DK - commented to allow field data type change        
        Street_Address__c ='1234 el camino', 
*/
        City__c='Edison', 
                                            State__c ='NJ', Postal_Code__c ='08901', Country__c ='United States', 
                                            Address_Label_Block__c = '1234 el camino Edison, NJ 08901', Contact__c = con.Id);
        insert add;
        
        
        Mailing__c mail = new Mailing__c(Contact__c = con.Id, Delivery_Method__c = 'Mail', 
                                         Mailings__c = 'Marketing' , Type__c = 'Annual Report', Address__c = add.Id);
        
        Mailing__c mailNew = new Mailing__c();
        
        PageReference pageRef = Page.ManageMailing_Edit;
        Test.setCurrentPage(pageRef);
        ApexPages.standardController controller = new ApexPages.standardController(mailNew);
        
        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('mailings','Marketing');
        ApexPages.currentPage().getParameters().put('type' , 'Annual Report');
        ManageMailing_EditController mEdit = new ManageMailing_EditController(controller);
        mEdit.mailRec.contact__c = con.Id;
        mEdit.mailRec.Delivery_Method__c = 'Mail';
        mEdit.changeContact();
        mEdit.getMailType();
        mEdit.Save();
        
        insert mail;
        
        controller = new ApexPages.standardController(mail);
        
        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('mailings','Marketing');
        ApexPages.currentPage().getParameters().put('type' , 'Annual Report');
        ManageMailing_EditController mEditTwo = new ManageMailing_EditController(controller);
        //mEdit.mailRec.contact__c = con.Id;
        mEditTwo.mailRec.Delivery_Method__c = 'Mail';
        mEditTwo.changeContact();
        mEditTwo.getMailType();
        mEditTwo.Save();
        
        
    }
}