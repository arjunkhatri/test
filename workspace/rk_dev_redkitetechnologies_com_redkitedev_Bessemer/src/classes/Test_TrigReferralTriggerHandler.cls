@isTest
public class Test_TrigReferralTriggerHandler
{
    static testMethod void testReferralInverses()
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  = 1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;


        list<Inverse_Relationship__c> retList = TestDataFactory.createTestInverseRelationships();
        insert retList;
        
        Account referredAcct = TestDataFactory.createTestAccount();
        referredAcct.Name = 'Referred Account';
        insert referredAcct;

        Contact referredCont = TestDataFactory.createTestContact(referredAcct);
        referredCont.FirstName = 'Referred';
        insert referredCont;

        /*
        Account referredByAcct = TestDataFactory.createTestAccount();
        referredByAcct.Name = 'Referred By Account';
        insert referredByAcct;

        Contact referredByCont = TestDataFactory.createTestContact(referredByAcct);
        referredByCont.FirstName = 'Referred By';
        insert referredByCont;
        */
        Contact referredByCont = (Contact)TestDataCreator_Utils.createSObject('Contact' , true , 1); 
        insert referredByCont;
        
        Referral__c testReferral = new Referral__c(Referred_By_Contact__c = referredByCont.Id, Relationship__c = referredByCont.AccountId, Type__c = 'Referred');
        insert testReferral;
        
        testReferral = [select id, Referred_By_Contact__c, Referred_By_Relationship__c, Type__c, Contact__c, Relationship__c, Inverse_Referral__c 
            from Referral__c where id = :testReferral.Id];
        Referral__c testInvReferral = [select id, Referred_By_Contact__c, Referred_By_Relationship__c, Type__c, Contact__c, Relationship__c, Inverse_Referral__c 
            from Referral__c where id = :testReferral.Inverse_Referral__c];
        
        // test Account fill in 
        System.assertEquals(referredByCont.AccountId, testReferral.Referred_By_Relationship__c);
        
        System.assertEquals('Referred By', testInvReferral.Type__c);
        System.assertEquals(referredByCont.Id, testInvReferral.Contact__c);
        System.assertEquals(referredByCont.AccountId, testInvReferral.Relationship__c);
        
        
        
        testReferral.Type__c = 'Referred By';
        update testReferral;
        
        testReferral = [select id, Referred_By_Contact__c, Referred_By_Relationship__c, Type__c, Contact__c, Relationship__c, Inverse_Referral__c 
            from Referral__c where id = :testReferral.Id];
        testInvReferral = [select id, Referred_By_Contact__c, Referred_By_Relationship__c, Type__c, Contact__c, Relationship__c, Inverse_Referral__c 
            from Referral__c where id = :testReferral.Inverse_Referral__c];
            
        System.assertEquals('Referred', testInvReferral.Type__c);
        
        
        delete testReferral;
        list<Referral__c> referralList = [select id from Referral__c where id =: testReferral.Id or id =: testInvReferral.Id ];
        System.assertEquals(0, referralList.size());
        
        
        undelete testReferral;
        testReferral = [select id, Referred_By_Contact__c, Referred_By_Relationship__c, Type__c, Contact__c, Relationship__c, Inverse_Referral__c 
            from Referral__c where id = :testReferral.Id];
        testInvReferral = [select id, Referred_By_Contact__c, Referred_By_Relationship__c, Type__c, Contact__c, Relationship__c, Inverse_Referral__c 
            from Referral__c where id = :testReferral.Inverse_Referral__c];
        
        System.assertEquals('Referred', testInvReferral.Type__c);
        System.assertEquals(referredByCont.Id, testInvReferral.Contact__c);
        System.assertEquals(referredByCont.AccountId, testInvReferral.Relationship__c);
        
    }

}