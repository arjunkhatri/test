@isTest(SeeAllData=false)
public with sharing class TestAssignIntermediaryRelationshipBatch 
{
	 static testMethod void AssignIntermediaryRelationshipBatchTest()
	 {
	 	GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        gs.Compliance_Email_Address__c = 'test@test.com' ;
        insert gs;

	 	RecordType accRecType = [Select Id From RecordType where Name = 'Professional/Intermediary' and SobjectType = 'Account'];
	 	RecordType conRecType = [Select Id From RecordType where Name = 'Professional/Intermediary' and SobjectType = 'Contact'];						 	
	 	
	 	/*
	 	Account acc = TestDataFactory.createTestAccount();
	 	acc.RecordTypeId = accRecType.id;
	 	insert acc;
	 	
	 	system.assert(acc.id != null,'Account Created successfully');	
	 	
	 	Contact con = TestDataFactory.createTestContact(acc);
	 	con.RecordTypeId = conRecType.id;
	 	insert con;	
	 	
	 	Contact testConts = [Select AccountId,Account.Name,Business_Name__c from Contact where id=:con.id];
	 	testConts.Business_Name__c = testConts.Account.Name;
	 	testConts.AccountId = null;
	 	update testConts;
	 	
	 	system.assert(testConts.AccountId == null,'Contact Account null ');	
	 	
	 	Contact con1 = TestDataFactory.createTestContact(Acc);
	 		con1.AccountId = null;
	 		con1.RecordTypeId = conRecType.id;
	 		con1.Business_Name__c = 'test acc 5';
	 		insert con1;

	 	system.assert(con1.id != null,'Contact Created successfully');	
		*/
		List<Account> testAcctList =  TestDataCreator_Utils.createSObjectList('Account' , false , 25); 
		for (Integer c = 0 ; c < 25; c++)
		{
			testAcctList[c].RecordTypeId = accRecType.id;
		}
		insert testAcctList;

		List<Contact> testDataList =  TestDataCreator_Utils.createSObjectList('Contact' , false , 25); 
		for (Integer c = 0 ; c < 25; c++)
		{
			testDataList[c].Company_Name__c = 'Company-' + String.ValueOf(c) ;
			testDataList[c].RecordTypeId = conRecType.id;

		}
		testDataList[24].AccountId = testAcctList[24].id;
		insert testDataList;

		Connections__c connect = [Select Role_On_Relationship__c from Connections__c where Contact_C1__c = :testDataList[23].id limit 1];
		Connect.Role_On_Relationship__c = 'Lawyer';
		update Connect;

	 	Test.StartTest();
	 	AssignIntermediaryRelationshipBatch relBatch = new AssignIntermediaryRelationshipBatch();
	 	database.executeBatch(relBatch);
	 	Test.stopTest();
	 }

}