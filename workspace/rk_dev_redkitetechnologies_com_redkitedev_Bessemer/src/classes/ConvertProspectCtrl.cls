public with sharing class ConvertProspectCtrl 
{
	public boolean createNew {get;set;}
	public Contact ContactObj {get;set;}
	private string redirectID;
	
	public ConvertProspectCtrl (ApexPages.Standardcontroller controller)
	{
		redirectID = controller.getId() ;
		ContactObj = (Contact) controller.getRecord();
	}
	
	public PageReference Convert ()
	{
		string recordTypeId = [select id from RecordType where sobjecttype = 'Contact' and Name = 'Prospect'].id;
		ContactObj.RecordTypeId = recordTypeId;
		update ContactObj ;
		PageReference pgRef = new PageReference('/' + redirectID);
		pgRef.setRedirect(true);
		return pgRef;
	}
}