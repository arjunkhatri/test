global class RecursiveFlagHandler 
{
	private static set<string> runningObjList ;
	private static boolean runningOrNot;
	public static  set<string> orphanedIds {get;set;}
	public static Integer initialCount {get;set;}
	
	public static boolean alreadyRunning (string objId)
	{
		boolean retVal = false;
		if (runningOrNot == true && runningObjList.Contains(objId) )
			retVal = true;
		System.debug('RunningList****: ' + runningObjList);
		return retval;
	}
	public static void setRunning(list<string> objIds)
	{
		if (runningObjList == null)
		{
			runningObjList = new set<string>();
			runningObjList.addAll(objIds) ;
		}
		else
			runningObjList.addAll(objIds) ;
		
		runningOrNot = true;
		


	} 
	
	public static void setStopped()
	{
		runningOrNot = false;
		runningObjList.clear();
		runningObjList = null;
	}
}