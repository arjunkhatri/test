public without sharing class RequestAccessHelper 
{
	//Custom Settings Constants
	private static final string AccountAccess = 'Account Access Request' ;
	private static final string AccountOwnerAccess = 'Account Owner Request' ;
	private static final string ContactAccess = 'Contact Access Request' ;
	private static final string ContactOwnerAccess = 'Contact Owner Access Request' ;
	private static final string LeadAccess = 'Lead Access Request' ;
	private static final string LeadOwnerAccess = 'Lead Owner Request' ;
	
	
	public static string SendRequestEmail  (string ObjectId , string UserId ,  boolean RequestOwnership, string RequestMessage)
	{
		Messaging.SingleEmailMessage email ;
		
		if (ObjectId.substring(0,3) == '001')
		{
			list<Account> accountList = [select ownerId , owner.Email, name from Account where id = :ObjectId] ;
			
			if (accountList.size() > 0)
			{
				email = new Messaging.SingleEmailMessage();
				
				List<string> theToAddess = new List<string>();
				theToAddess.add(accountList[0].Owner.Email);
				//theToAddess.add('david.brandenburg@redkitetechnologies.com');
				email.setToAddresses(theToAddess);
				
				string Msg;
				
				
				if (RequestOwnership)
				{
					if (RequestAccessEmailTemplates__c.getvalues(AccountOwnerAccess) == null)
						return 'Error accessing email template.  Contact system administrator to fix. ' ;
					//Check for Email Template
					List<EmailTemplate> emailTemplate = [SELECT Id FROM EmailTemplate where DeveloperName = :RequestAccessEmailTemplates__c.getvalues(AccountOwnerAccess).TemplateDeveloperName__c] ;
					if (emailTemplate.size () == 0)
						return 'Error accessing email template.  Contact system administrator to fix. ' ;
						
					Msg = RequestAccessHelper.MergeEmailTemplate(ObjectId, 'Account' , UserId ,emailTemplate[0].id  , RequestMessage);
					email.setSubject(UserInfo.getName() + ' has requested ownership for ' + accountList[0].Name );
				}
				else
				{
					if (RequestAccessEmailTemplates__c.getvalues(AccountAccess) == null)
						return 'Error accessing email template.  Contact system administrator to fix ' ;
						
					//Check for Email Template
					List<EmailTemplate> emailTemplate = [SELECT Id FROM EmailTemplate where DeveloperName = :RequestAccessEmailTemplates__c.getvalues(AccountAccess).TemplateDeveloperName__c ] ;
					if (emailTemplate.size () == 0)
						return 'Error accessing email template.  Contact system administrator to fix ' ;
						
					Msg = RequestAccessHelper.MergeEmailTemplate(ObjectId, 'Account' , UserId ,emailTemplate[0].id  , RequestMessage);
					email.setSubject(UserInfo.getName() + ' has requested access to view  ' + accountList[0].Name );
				}
				
				email.setHtmlBody(Msg);
				Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  
				string retVal = '';
				for (Messaging.SendEmailResult result : results)
				{
					if (result.isSuccess())
						retVal = 'success' ;
					else
					{
						for (Messaging.Sendemailerror err : result.getErrors() )
						{
							retVal += err + '\n';
						}
					}
				}
				return retVal;
			}
			else
				return 'Account record not found';
			
		}
		if (ObjectId.substring(0,3) == '003')
		{
			list<Contact> contactList = [select ownerId , owner.Email, name from Contact where id = :ObjectId] ;
			
			if (contactList.size() > 0)
			{
				email = new Messaging.SingleEmailMessage();
				//email.setSubject('Record Access Request');
				List<string> theToAddess = new List<string>();
				theToAddess.add(contactList[0].Owner.Email);
				//theToAddess.add('david.brandenburg@redkitetechnologies.com');
				email.setToAddresses(theToAddess);
				
				string Msg = '';
				if (RequestOwnership)
				{
					if (RequestAccessEmailTemplates__c.getvalues(ContactOwnerAccess) == null)
						return 'Error accessing email template.  Contact system administrator to fix. ' ;
					//Check for Email Template
					List<EmailTemplate> emailTemplate = [SELECT Id FROM EmailTemplate where DeveloperName = :RequestAccessEmailTemplates__c.getvalues(ContactOwnerAccess).TemplateDeveloperName__c] ;
					if (emailTemplate.size () == 0)
						return 'Error accessing email template.  Contact system administrator to fix. ' ;
						
					Msg = RequestAccessHelper.MergeEmailTemplate(ObjectId, 'Contact' , UserId ,emailTemplate[0].id  , RequestMessage);
					email.setSubject(UserInfo.getName() + ' has requested ownership for ' + contactList[0].Name );
				}
				else
				{
					if (RequestAccessEmailTemplates__c.getvalues(contactAccess) == null)
						return 'Error accessing email template.  Contact system administrator to fix ' ;
						
					//Check for Email Template
					List<EmailTemplate> emailTemplate = [SELECT Id FROM EmailTemplate where DeveloperName = :RequestAccessEmailTemplates__c.getvalues(contactAccess).TemplateDeveloperName__c ] ;
					if (emailTemplate.size () == 0)
						return 'Error accessing email template.  Contact system administrator to fix ' ;
						
					Msg = RequestAccessHelper.MergeEmailTemplate(ObjectId, 'Contact' , UserId ,emailTemplate[0].id  , RequestMessage);
					email.setSubject(UserInfo.getName() + ' has requested access to view  ' + contactList[0].Name );
				}
				
				email.setHtmlBody(Msg);
				Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  
				string retVal = '';
				for (Messaging.SendEmailResult result : results)
				{
					if (result.isSuccess())
						retVal = 'success' ;
					else
					{
						for (Messaging.Sendemailerror err : result.getErrors() )
						{
							retVal += err + '\n';
						}
					}
				}
				return retVal;
			}
			else
				return 'Contact record not found';
		}
		
		if (ObjectId.substring(0,3) == '00Q')
		{
			list<Lead> leadList = [select ownerId , owner.Email, name from Lead where id = :ObjectId] ;
			
			if (leadList.size() > 0)
			{
				email = new Messaging.SingleEmailMessage();
				//email.setSubject('Record Access Request');
				List<string> theToAddess = new List<string>();
				theToAddess.add(leadList[0].Owner.Email);
				//theToAddess.add('david.brandenburg@redkitetechnologies.com');
				email.setToAddresses(theToAddess);
				
				string Msg = '';
				if (RequestOwnership)
				{
					if (RequestAccessEmailTemplates__c.getvalues(leadOwnerAccess) == null)
						return 'Error accessing email template.  lead system administrator to fix. ' ;
					//Check for Email Template
					List<EmailTemplate> emailTemplate = [SELECT Id FROM EmailTemplate where DeveloperName = :RequestAccessEmailTemplates__c.getvalues(leadOwnerAccess).TemplateDeveloperName__c] ;
					if (emailTemplate.size () == 0)
						return 'Error accessing email template.  lead system administrator to fix. ' ;
						
					Msg = RequestAccessHelper.MergeEmailTemplate(ObjectId, 'lead' , UserId ,emailTemplate[0].id  , RequestMessage);
					email.setSubject(UserInfo.getName() + ' has requested ownership for ' + leadList[0].Name );
				}
				else
				{
					if (RequestAccessEmailTemplates__c.getvalues(leadAccess) == null)
						return 'Error accessing email template.  lead system administrator to fix ' ;
						
					//Check for Email Template
					List<EmailTemplate> emailTemplate = [SELECT Id FROM EmailTemplate where DeveloperName = :RequestAccessEmailTemplates__c.getvalues(leadAccess).TemplateDeveloperName__c ] ;
					if (emailTemplate.size () == 0)
						return 'Error accessing email template.  lead system administrator to fix ' ;
						
					Msg = RequestAccessHelper.MergeEmailTemplate(ObjectId, 'lead' , UserId ,emailTemplate[0].id  , RequestMessage);
					email.setSubject(UserInfo.getName() + ' has requested access to view  ' + leadList[0].Name );
				}
				
				email.setHtmlBody(Msg);
				Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  
				string retVal = '';
				for (Messaging.SendEmailResult result : results)
				{
					if (result.isSuccess())
						retVal = 'success' ;
					else
					{
						for (Messaging.Sendemailerror err : result.getErrors() )
						{
							retVal += err + '\n';
						}
					}
				}
				return retVal;
			}
			else
				return 'Lead record not found';
		}
		
		return 'Object is not supported';
	}
		
	  public static string MergeEmailTemplate (string ObjectID , string ObjectName, string UserId , string TemplateId , string AccessReason  )
	  {
	  		
	  		list<EmailTemplate> template = [SELECT Body, Id FROM EmailTemplate where Id =  :TemplateId ];
	  		List<sObject> objectRecord  ;
	  		
	  		System.debug(GetFieldList(ObjectName ));
	  		objectRecord = database.query('select ' + GetFieldList(ObjectName ) + ' from ' + ObjectName + ' where id = \'' + ObjectId + '\'');
	  		
	  		List<User> userRecord = database.query('select ' + GetFieldList('User' ) + ' from User where id = \'' + UserId + '\'');
	  		
	  		string msg = template[0].Body ;
	  		msg = msg.ReplaceAll('\n', '<br/>');
	  		msg = msg.ReplaceAll('\\{!AccessReason\\}', AccessReason );
	  		
	  		Pattern MyPattern =Pattern.compile('\\{!([^}]*)\\}');
			Matcher MyMatcher = MyPattern.matcher(msg);
		
			while (MyMatcher.find()) 
			{
				string replaceString = '\\{!' + MyMatcher.group(1) + '\\}';
				string fullString = MyMatcher.group(1);
				string[] splitString = fullString.split('\\.'); 
				
				
				if (splitString.size() == 2 && splitString[0] == 'User')	
				{
					if (userRecord[0].get(splitString[1])  == null)
						msg = msg.ReplaceAll(replaceString, '' );
					else
						msg = msg.ReplaceAll(replaceString, String.ValueOf(userRecord[0].get(splitString[1])) );
					
				}
				else if (splitString.size() == 2 && splitString[1] == 'Link' && ObjectName == 'Contact' && splitString[0] ==  'Account' )	
				{
					if ( objectRecord[0].getSobject('Account')== null)
						msg = msg.ReplaceAll(replaceString, '<a href="https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ) + '/' +objectRecord[0].get('id') + '">' + objectRecord[0].get('Name') + '</a>' );
					else
						msg = msg.ReplaceAll(replaceString, '<a href="https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ) + '/' + objectRecord[0].getSobject('Account').get('Id') + '">' + objectRecord[0].getSobject('Account').get('Name') + '</a>' );
				}
					
				else if (splitString.size() == 2 && splitString[0] == ObjectName)	
				{
					try {
						if (splitString[1] == 'Link')
							msg = msg.ReplaceAll(replaceString, '<a href="https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ) + '/' +objectRecord[0].get('id') + '">' + objectRecord[0].get('Name') + '</a>' );
						else
						{
							if (objectRecord[0].get(splitString[1]) == null)
								msg = msg.ReplaceAll(replaceString, '');
							else
								msg = msg.ReplaceAll(replaceString, String.ValueOf(objectRecord[0].get(splitString[1])) );
						}
					}
					catch (Exception ex)
					{
						msg = msg.ReplaceAll(replaceString, '');
						//Need to add error message
					}
					
				}
				else
					msg = msg.ReplaceAll(replaceString, ''); 
				
					
					
					
				
			}
			
			return msg;
	  		
	  		
	  }

	private static string GetFieldList (string ObjectName)
  	{
        string retval = '';
        SObjectType objToken = Schema.getGlobalDescribe().get(ObjectName);
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap(); 
       
        Set<String> fieldSet = fields.keySet();
        for(String s:fieldSet)
        {
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            retval += selectedField.getName() + ',' ;
        }
        if (ObjectName == 'Contact')
        	retval +=  'Account.Id,Account.Name,' ;
        	
        return retval.substring(0, retval.length()-1);
    	}
}