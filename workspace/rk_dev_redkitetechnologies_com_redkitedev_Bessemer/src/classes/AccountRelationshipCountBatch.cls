global class AccountRelationshipCountBatch implements Database.Batchable<sObject>{
	public static final String RCTYPE = 'R:R';
	public static final String CLIENTR = 'Client Relationship';
	public static final String PROSPECTIVER = 'Prospective Relationship';
	
/*
    global Iterable<Account> start(Database.BatchableContext BC){ 
    	List<Account> lstAccount = [SELECT Id,
    								Num_of_Client_Relationships__c,
    								Num_of_Prospective_Relationships__c FROM Account];
    	return lstAccount;
    }
*/
  	
  	static final String sQuery = 'SELECT Id, Num_of_Client_Relationships__c, Num_of_Prospective_Relationships__c FROM Account';
  	
    global Database.QueryLocator start(Database.BatchableContext BC){ 
    	return Database.getQueryLocator(sQuery);
    }
    
	global void execute(Database.BatchableContext BC, List<Account> scope){
    	map<Id,set<Id>> mpAccToAccIdsClient = new map<Id,set<Id>>();
    	map<Id,set<Id>> mpAccToAccIdsProsp = new map<Id,set<Id>>();		
    	List<Account> lstAccToUpdate = new List<Account>();
    	Id clientRecTypId;
    	Id prospectRecTypId;
    	for(RecordType RecType: [SELECT Name, Id FROM RecordType WHERE 
    									  Name =:CLIENTR OR Name = :PROSPECTIVER])
    	{
    		if(RecType.Name == CLIENTR)
    			clientRecTypId = RecType.Id;
    		if(RecType.Name == PROSPECTIVER)
    			prospectRecTypId = RecType.Id;								  
    	}		
    	
    	for(Connections__c RelRec:[SELECT Relationship_R1__r.Id,
    	 							      Relationship_R1__c, 
    	 							      RecordType.Name,
    	 							      Relationship2__c,
    	 							      Relationship2__r.Id,
    	 							      Relationship2__r.RecordTypeId
    	 							      FROM Connections__c WHERE 
    	 							      RecordType.Name = :RCTYPE AND
    	 							      Relationship2__c IN :scope] )
    	{
    	 	if(RelRec.Relationship_R1__r.Id != null && RelRec.Relationship2__r.Id != null){
	    	 	if(clientRecTypId != null && RelRec.Relationship2__r.RecordTypeId == clientRecTypId ){
	    	 		//fill map of Account  vs client relationship ids
	    	 		if(!mpAccToAccIdsClient.containsKey(RelRec.Relationship_R1__r.Id)){
	    	 			mpAccToAccIdsClient.put(RelRec.Relationship_R1__r.Id,new Set<Id>{RelRec.Relationship2__r.Id});
	    	 		}
	    	 		else if (mpAccToAccIdsClient.containsKey(RelRec.Relationship_R1__r.Id)){
	    	 			mpAccToAccIdsClient.get(RelRec.Relationship_R1__r.Id).add(RelRec.Relationship2__r.Id);
	    	 		}
	    	 	
	    	 	}
	    	 	else if(prospectRecTypId != null && RelRec.Relationship2__r.RecordTypeId == prospectRecTypId){
	    	 		//fill map of Account is vs prospect relationship ids
	    	 		if(!mpAccToAccIdsProsp.containsKey(RelRec.Relationship_R1__r.Id)){
	    	 			mpAccToAccIdsProsp.put(RelRec.Relationship_R1__r.Id,new Set<Id>{RelRec.Relationship2__r.Id});
	    	 		}
	    	 		else if (mpAccToAccIdsProsp.containsKey(RelRec.Relationship_R1__r.Id)){
	    	 			mpAccToAccIdsProsp.get(RelRec.Relationship_R1__r.Id).add(RelRec.Relationship2__r.Id);
	    	 		}	    	 		
	    	 	}
    	 	}
    	}
    	//iterate Account and get count of relationship
    	for(Account accRec:scope){ 
    	 	//get client relationship count
    	 	if(!mpAccToAccIdsClient.isEmpty() && mpAccToAccIdsClient.containsKey(accRec.id)){
    	 		accRec.Num_of_Client_Relationships__c = mpAccToAccIdsClient.get(accRec.id).size();
    	 	}
    	 	else{
    	 		accRec.Num_of_Client_Relationships__c = 0;
    	 	}
    	 	
    	 	//get prospect relationship count
     	 	if(!mpAccToAccIdsProsp.isEmpty() && mpAccToAccIdsProsp.containsKey(accRec.id)){
    	 		accRec.Num_of_Prospective_Relationships__c = mpAccToAccIdsProsp.get(accRec.id).size();
    	 	}
    	 	else{
    	 		accRec.Num_of_Prospective_Relationships__c = 0;
    	 	}   	 	
    	 	lstAccToUpdate.add(accRec);
    	}    
    	
 		if(lstAccToUpdate != null && lstAccToUpdate.size() > 0){
			Database.SaveResult[] srListAcc = Database.update(lstAccToUpdate, false);
			for (Database.SaveResult sr : srListAcc) {
			    if (!sr.isSuccess()) {
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage());                    
			        }
			    }
			}			
		}     	
	}    

	global void finish(Database.BatchableContext BC){
	   	
	}	
}