global class TaskEmailReminderBatch implements Database.Batchable<Task>{
	public List<Task> listTask;
	public Map<String,set<String>> RelToTeamMem;
	public Map<String,set<String>> RelToSelTeamMem;
	public set<String> setClientRel;
	public Map<String,String> TaskToRef;
	public List<String> lstRel;
	public String temId;
	public List<String> noRelTaskOwner;
	List<Messaging.SendEmailResult> listEmailResult = null; 
	public static final String TEMPLATE = 'Email_Reminder'; 
	public static final String CLIENT = 'Client Relationship';
	
	public TaskEmailReminderBatch(){
		listTask = [Select WhatId,
					Who.Name,
					What.Name, 
					ActivityDate,  
					Subject, 
					OwnerId,
					WhoId,
					Description,
					Id, 
					Email_Reminder_Date__c, 
					AccountId 
				    From Task 
				    where Email_Reminder_Date__c <= today 
				    and Email_Reminder_Sent__c = false
				    and Email_Reminder_Date__c != null
				    and ActivityDate >= today
				    and ActivityDate != null
				    ];
	}
	
    global Iterable<Task> start(Database.BatchableContext BC){
    	return listTask;
    }
    
	global void execute(Database.BatchableContext BC, List<Task> scope){
		RelToTeamMem = new Map<String,set<String>>();
		RelToSelTeamMem = new Map<String,set<String>>();
		setClientRel = new set<String>();
		TaskToRef = new Map<String,String>();
		lstRel = new List<String>();
		noRelTaskOwner = new List<String>();
		Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
		Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();		
		set<String> setRoles = new set<String>();
			setRoles.add('Senior Client Advisor');
			setRoles.add('Client Advisor');
			setRoles.add('Associate Client Advisor');
			
	 	for(Task cTask:scope){
	 		//check for whatid
	 		if(cTask.WhatId != null && String.valueOf(cTask.WhatId).startswith('001')){
	 			lstRel.add(cTask.WhatId);
	 			TaskToRef.put(cTask.id,cTask.WhatId);
	 		}
	 		//check for account id
	 		else if(cTask.AccountId != null){
	 			lstRel.add(cTask.AccountId);
	 			TaskToRef.put(cTask.id,cTask.AccountId);
	 		}
	 		//No any relationship to task
	 		else{
	 			noRelTaskOwner.add(cTask.OwnerId);
	 			TaskToRef.put(cTask.id,cTask.OwnerId);
	 		}
	 		cTask.Email_Reminder_Sent__c = true;
	 	}
	 	
	 	if(lstRel != null && lstRel.size() > 0){
	 		for(AccountTeamMember teamMem :[Select UserId,
	 										AccountId,
	 										TeamMemberRole,
	 										Account.RecordTypeId 
	 										From AccountTeamMember where 
	 										AccountId = :lstRel])
	 		{
	 			//get all team members related to relationship
				if(!RelToTeamMem.containsKey(teamMem.AccountId)){
					RelToTeamMem.put(teamMem.AccountId ,new set<String>{teamMem.UserId} );	
				}else{
					RelToTeamMem.get(teamMem.AccountId).add(teamMem.UserId);
				}	 	
				//get selected team members related to relationship
				if(!RelToSelTeamMem.containsKey(teamMem.AccountId) && setRoles.contains(teamMem.TeamMemberRole)){
					RelToSelTeamMem.put(teamMem.AccountId ,new set<String>{teamMem.UserId} );	
				}else if (setRoles.contains(teamMem.TeamMemberRole)){
					RelToSelTeamMem.get(teamMem.AccountId).add(teamMem.UserId);
				}
				//if relationship is client relationship then add it to set
				if(rtMapById.get(teamMem.Account.RecordTypeId).getName() == CLIENT)
					setClientRel.add(teamMem.AccountId);
	 		}
	 	}
	 	sendSingleMail(scope);
	 	if(scope != null && scope.size() > 0){
	 		update scope;
	 	} 
	}
	global void finish(Database.BatchableContext BC){
	   	
	}
	
	public  void sendSingleMail(List<Task> listTsk){
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		String sfUrl = 'https://'+URL.getSalesforceBaseUrl().getHost();
		EmailTemplate emailTemplate = [Select 
									  Id,
									  Subject, 
									  HtmlValue, 
									  Body From EmailTemplate where DeveloperName =: TEMPLATE];

	    // grab the Task fields we need.
		for(Task tsk:listTsk){
			set<String> lstUserIds = new set<String>();
		
			String relatedID = TaskToRef.get(tsk.id);
			//if task is not related to any relationship 
			if(relatedID != null && relatedID.startsWith('005')){
				lstUserIds.add(relatedID);
			}
			else if(relatedID != null){
				//if task related to any relationship get members
				if(setClientRel.contains(relatedID)){
					lstUserIds = RelToSelTeamMem.get(relatedID);
				}
				else{
					lstUserIds = RelToTeamMem.get(relatedID);
				}
			}
 			//add the owner (assigned to) in the user list
 			if(lstUserIds == null)
 				lstUserIds = new set<String>();
				lstUserIds.add(tsk.OwnerId);	

			if(lstUserIds != null && lstUserIds.size() > 0){
				for(String strMembers:lstUserIds){
				    // process the merge fields
				    String subject = emailTemplate.Subject;
					    if(tsk.ActivityDate != null){
					    	subject = subject.replace('{!Task.ActivityDate}', string.valueOf(tsk.ActivityDate));
					    }else{
					    	subject = subject.replace('{!Task.ActivityDate}', '');
					    }
				    String plainBody = emailTemplate.Body;
						if(tsk.ActivityDate != null){    
				    		plainBody = plainBody.replace('{!Task.ActivityDate}', string.valueOf(tsk.ActivityDate));
						}else{
							plainBody = plainBody.replace('{!Task.ActivityDate}', '');
						}
				    	plainBody = plainBody.replace('{!Task.Subject}', string.valueOf(tsk.Subject));
				    	plainBody = plainBody.replace('{!Task.Link}', string.valueOf(sfUrl+'/'+tsk.id));
				    	if(tsk.Who.Name != null){
				    		plainBody = plainBody.replace('{!Task.Who}', tsk.Who.Name);
				    	}
				    	else{
				    		plainBody = plainBody.replace('{!Task.Who}', '');
				    	}
				    	if(tsk.What.Name != null){
				    		plainBody = plainBody.replace('{!Task.What}', tsk.What.Name);
				    	}
				    	else{
				    		plainBody = plainBody.replace('{!Task.What}', '');
				    	}
				    	if(tsk.Description != null){
				    		plainBody = plainBody.replace('{!Task.Description}', tsk.Description);
				    	}
				    	else{
				    		plainBody = plainBody.replace('{!Task.Description}', '');
				    	}	
				    //build the email message
				    Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
					    email.setTargetObjectId(strMembers);
					    email.setSaveAsActivity(false);
					    email.setSubject(subject);
					    email.setPlainTextBody(plainBody);
					    mails.add(email);
				}
			}
		}
		try{
		    if(mails != null && mails.size() > 0){
		        listEmailResult = Messaging.sendEmail(mails);	
		    }		
		}catch(Exception e){
		
		}
	}		
}