/*
    
     Authors :  Don Koppel
     Created Date: 2014-07-20
     Last Modified: 2014-07-20
*/

public with sharing class AccountTrigHand {
    // template methods
    public void onAfterInsert(list<Accounts__c> newList) {
    	updateDataSyncLog(newList, 'I');
    }
    public void onAfterUpdate(list<Accounts__c> newList, map<id,Accounts__c> oldMap) {
    	updateDataSyncLog(newList, 'U');
    }
    public void onAfterDelete(list<Accounts__c> oldList){
    	updateDataSyncLog(oldList, 'D');
    }

// business logic

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs Account EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Accounts__c> recordList, String operation) {
    	
    	//records should only be added to the data sync log if the current user is the Bessemer Integration user
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
    	
	    	list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
	 		for (Accounts__c account : recordList){
	 			if(account.EDB_Entity_Id__c <> null){
		 			Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
		 			syncRecord.EDB_Entity_Id__c = account.EDB_Entity_Id__c;
		 			syncRecord.Entity__c = 'LGL_ENTITY';
		 			syncRecord.Action__c = Operation;
		 			syncList.Add(syncRecord);
	 			}
	 		}

			if(synclist.size() > 0)
				insert syncList;
    	}
    }       
}