@isTest
public with sharing class TestRelationshipTeamListCtrl {
	static testMethod void RelationshipTeamtest(){
		Profile pf = [Select Id From Profile where Name= 'Bessemer Wealth Advisor'];
		Profile pf1 = [Select Id From Profile where Name= 'Bessemer Client Advisor'];
		User u = TestDataFactory.createTestUser(pf.id);
			insert u;
			system.assert(u.id != null,'Created user record');
		User u1 = TestDataFactory.createTestUser(pf1.id);
			u1.Username = 'test1@domain.com.dev';
			insert u1;
			system.assert(u1.id != null,'Created user record');			
		GlobalSettings__c gs = new GlobalSettings__c();
			gs.Relationship_Number_Seq__c = 123;
			insert gs;			
		Account acc = TestDataFactory.createTestAccount();
		 	insert acc;
		 	system.assert(acc.id != null,'Created relationship record');
		Relationship_Team_Member__c accTM = TestDataFactory.createRelTeamMem(acc.id,u.id,'Cash Manager');
			insert accTM;
			system.assert(accTM.id != null,'Created account team member record');
		Relationship_Team_Member__c accTM1 = TestDataFactory.createRelTeamMem(acc.id,u1.id,'Senior Client Advisor');
			insert accTM1;
			system.assert(accTM1.id != null,'Created account team member record');
		Task tsk = TestDataFactory.createTask(acc.id);
			tsk.Relationship_Team_List__c = '';
			insert tsk; 			
 		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(acc);
 		RelationshipTeamListCtrl rel = new RelationshipTeamListCtrl(sc);		
			rel.recAdd();
			rel.editId = accTM.id;
			rel.recEdit();
			rel.delId = accTM.id;
			rel.recDelete();   
			rel.nextPage();
			rel.previousPage();
			rel.lastPage();
			rel.closePopup();
		Account accnt = [Select Senior_Client_Advisor__c from Account where id=:acc.id];
			system.assertEquals(accnt.Senior_Client_Advisor__c, u1.LastName);
			rel.delId = accTM1.id; 
			rel.recDelete();
		Account accnt1 = [Select Senior_Client_Advisor__c from Account where id=:acc.id];
			system.assertEquals(accnt1.Senior_Client_Advisor__c, null);	

	}
}