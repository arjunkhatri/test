/*
	
     Authors :  David Brandenburg
     Created Date: 2014-11-10
     Last Modified: 2014-11-11
     
     Purpose: Test Class for LeadNewEdit page. 
*/
@isTest
private class TestLeadNewExt
{
	static testMethod void LeadNewExt_Test1() 
	{
		createTestData ();
		Lead ld = [select id from Lead Limit 1] ;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(ld);
		PageReference pageRef = Page.LeadNew;
      	Test.setCurrentPage(pageRef);
      	ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
      	ApexPages.currentPage().getParameters().put('Id', ld.Id);
		Test.startTest() ;
		LeadNewExt myCon = new LeadNewExt(sc);
		DupeBlockerListCtrl dupBlock = new DupeBlockerListCtrl ();
		myCon.setComponentController (dupBlock)  ;
		myCon.init();
		myCon.LeadRec.Company = 'ABC TEST CORP' ;

		myCon.SaveRecord();
		
		Lead ldTest = [Select id, Company from Lead where Id = :ld.id];
		System.assertEquals('ABC TEST CORP' , ldTest.Company);

		Test.StopTest();

	}
	static testMethod void LeadNewExt_Test2() 
	{
		createTestData ();
		Lead ld = [select id from Lead Limit 1] ;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(ld);
		PageReference pageRef = Page.LeadNew;
      	Test.setCurrentPage(pageRef);
      	ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
      	ApexPages.currentPage().getParameters().put('Id', ld.Id);
		Test.startTest() ;
		LeadNewExt myCon = new LeadNewExt(sc);
		DupeBlockerListCtrl dupBlock = new DupeBlockerListCtrl ();
		myCon.setComponentController (dupBlock)  ;
		myCon.init();
		myCon.LeadRec.Company = 'ABC TEST CORP' ;

		myCon.SaveRecordOverRide();
		
		Lead ldTest = [Select id, Company from Lead where Id = :ld.id];
		System.assertEquals('ABC TEST CORP' , ldTest.Company);

		Test.StopTest();

	}
	private static void createTestData ()
	{
		/********** Create test data *****************/
    	GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        CRMfusionDBR101__Scenario__c scenario = new CRMfusionDBR101__Scenario__c ();
       	scenario.Name = 'Lead Name'; 
       	scenario.CRMfusionDBR101__Scenario_Type__c = 'Lead' ;
       	scenario.CRMfusionDBR101__Allow_Block_Bypass__c = true ;
       	scenario.CRMfusionDBR101__Deployed__c = true;
       	insert scenario ;
       	CRMfusionDBR101__Scenario_Rule__c rule = new CRMfusionDBR101__Scenario_Rule__c ();
       	rule.CRMfusionDBR101__Field_Name__c  = 'Email' ; 
       	rule.CRMfusionDBR101__Mapping_Type__c = 'Exact' ;
       	rule.CRMfusionDBR101__Scenario__c = scenario.id;
       	rule.CRMfusionDBR101__Scenario_Type__c = 'Lead' ; 
       	insert rule;



        Lead ld =  (Lead)TestDataCreator_Utils.createSObject('Lead' , true , 1);
        insert ld;
    }
}