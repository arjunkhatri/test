/**
*	TestDataFactory - This is a test utility class that can create test records for test classes
*/
@isTest
public class TestDataFactory {
	public static list<Inverse_Relationship__c> createTestInverseRelationships(){
		list<Inverse_Relationship__c> retList = new list<Inverse_Relationship__c>();
		
		retList.add(new Inverse_Relationship__c(Name='Referred', Relationship__c='Referred', Relationship_Inverse__c='Referred By'));
		retList.add(new Inverse_Relationship__c(Name='Referred By', Relationship__c='Referred By', Relationship_Inverse__c='Referred'));

		return retList;
	}
	
	public static Account createTestAccount(){
		return new Account(Name='Test Account');
	}

	public static Contact createTestContact(Account testAccount){
		Contact con = new Contact();
			con.FirstName='Test';
			con.LastName='Contact';
			con.AccountId = testAccount.Id;
		    con.MailingStreet = 'test street';
	    	con.MailingCity = 'New York';
	    	con.MailingCountry = 'us';
	    	con.Contact_S_Number__c = 'c1234';
		return con;
	}
	public static Connections__c createConnection(String relId,String conId){
		return new Connections__c(Relationship_R1__c = relId,Contact_C1__c = conId);
	}
	public static AccountTeamMember createATM(Id accId,Id uId, String strRole){
		AccountTeamMember atm = new AccountTeamMember();
			atm.AccountId = accId;
			atm.UserId = uId;
			atm.TeamMemberRole = strRole;
		return atm;
	} 
	public static AccountShare createAccShare(Id accId,Id uId,String aLevel){ 
		AccountShare ash = new AccountShare();
			ash.AccountId = accId;
			ash.UserOrGroupId = uId;
			ash.AccountAccessLevel = aLevel;
			ash.OpportunityAccessLevel = aLevel;
			ash.CaseAccessLevel = aLevel;
		return ash;
	}	 
	public static User createTestUser(String myProfileid){
		User u = new User(LastName = 'Test', email = 'test@domain.com', Alias = 'myAlias',
			EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', 
			TimeZoneSidKey = 'America/Los_Angeles', UserName = 'test@domain.com.dev', profileId = myProfileid);
		return u;
	}
	public static FeedItem createFeedItem(String parentId){
		FeedItem fi = new FeedItem();
			fi.ParentId = parentId;
			fi.Body = 'test comment';
	 		fi.ContentFileName = 'test.txt';
	 		fi.ContentData = blob.valueOf('testdata');		
		return fi;
	}
	public static GlobalSettings__c createGlobalSetting(){
		GlobalSettings__c gs = new GlobalSettings__c();
			gs.Compliance_Email_Address__c = 'test@user.com';
			gs.Contact_S_Number_Seq__c = 1701;
		return gs;
	}		
	public static Chatter_Compliance_Field_Mapping__c createComplianceFieldMapping(){
	 	Chatter_Compliance_Field_Mapping__c ccfm = new Chatter_Compliance_Field_Mapping__c();
	 		ccfm.Name = 'Relationship';
	 		ccfm.Object_API_Name__c = 'Account';
	 		ccfm.Object_Id_Prefix__c = '001';
	 		ccfm.EDB_Id_Field__c = 'Relationship_Number__c';
		return ccfm;
	}
	public static FeedComment createFeedComment(String FeedItemId){
		FeedComment fc = new FeedComment();
			fc.CommentBody = 'testdata';
			fc.FeedItemId = FeedItemId;
		return fc;
	}
	public static Task createTask(String AccountId){
	 	Task tsk = new Task();
	 	tsk.WhatId = AccountId;
	 	tsk.ActivityDate = date.today();
	 	tsk.Type = 'Email - Outbound';
	 	tsk.Subject = 'call';
	 	tsk.Status = 'Not Started';
	 	return tsk;
	}
	public static EmailTemplate createTemplate(String folderId,String DevName,String Name, String TemType){
		EmailTemplate et = new EmailTemplate();
		et.FolderId = folderId;
		et.Subject = 'You have an upcoming Activity for {!Task.ActivityDate}';
		et.Body = 'test {!Task.ActivityDate} test {!Task.Subject} {!Task.Link}';
		et.IsActive = true;
		et.DeveloperName = DevName;
		et.Name = name;
		et.TemplateType = TemType;
		return et;
	}
	public static Accounts__c customAccount(String relId){
		return new Accounts__c(Relationship__c = relId);
	}
	public static Relationship_Team_Member__c createRelTeamMem(Id relId,Id uId,String role){
		Relationship_Team_Member__c rtm = new Relationship_Team_Member__c();
		rtm.Relationship__c = relId; 
		rtm.User__c = uId;
		rtm.Role__c = role;
		return rtm;
	}	
	public static Activity_Staging__c createActivityStaging(){
		Activity_Staging__c actStg = new Activity_Staging__c(
		Type__c = 'To Do',Subject__c = 'Call',Status__c = 'In Progress',
		Priority__c = 'Normal',Due_Date__c = date.today().addDays(5),
		Description__c = 'test description',Activity_Id__c = '1234',
		Processed__c = false,Employee_Number__c = '1500023453',
		Relationship_Number__c = 'Rel001',
		Customer_S_Number__c = 'CS001',
		Account_Number__c = 'AC001',
		SubAccount_Number__c = 'SA001');		
		return actStg;
	}
	public static SubAccounts__c createSubAccount(String uNumber,Id relId){
	
		return new SubAccounts__c(SubAccount_Number_Unique__c = uNumber,Relationship__c = relId);
	}
	

}