@isTest
private class TestManageMailing_ExportController {
	
	@isTest static void testMailingExport() {
		GlobalSettings__c setting = TestDataFactory.createGlobalSetting();
		setting.Relationship_Number_Seq__c = 1201;
		setting.Integration_User_Name__c = userinfo.getUserName();
		insert setting;

		Account acc = TestDataFactory.createTestAccount();
        insert acc;
        
        Contact con = TestDataFactory.createTestContact( acc );
        con.Contact_Preference__c = 'Mail;Do Not Mail';
        insert con;
        
        Addresses__c testAdd = new Addresses__c();
        testAdd.Contact__c = con.Id;
        insert testAdd;

        Mailing__c testMailing = new Mailing__c();
        testMailing.Contact__c = con.Id;
        testMailing.Mailings__c = 'Mail';
        testMailing.Type__c = 'type';
        testMailing.Address__c = testAdd.Id;
        insert testMailing;

        Mailing_Link__c testMailLink = new Mailing_Link__c();
        testMailLink.MailingId__c = testMailing.Id;
        insert testMailLink;

        Test.startTest();
        	PageReference testPage = Page.ManageMailing_Export;
			testPage.getParameters().put( ManageMailing_EditController.PARAM_MAILINGS, 'Mail' );
			testPage.getParameters().put( ManageMailing_EditController.PARAM_TYPE, 'type' );
			Test.setCurrentPage( testPage );

            ApexPages.StandardController std = new ApexPages.StandardController( testMailing );
        	ManageMailing_ExportController ctrl = new ManageMailing_ExportController( std );

        	ctrl.getMailingRecs();
        	ctrl.filterText = 'type';
        	ctrl.getFilteredMailingRecs();
        	ctrl.GenerateCSV();
        Test.stopTest();

       
	}
}