@isTest
public with sharing class TestAccountRelationshipCountBatch {

	static testMethod void AccountRelationshipCountBatchTest(){
		GlobalSettings__c gs = testdatafactory.createGlobalSetting();
			gs.Address_Number_Seq__c = 10002000;
			insert gs;
		Account acc = testdatafactory.createTestAccount();
			acc.Relationship_Number__c = 'R1234A';
			acc.RecordTypeId = [Select Id From RecordType where Name ='Prospective Relationship'].id;
			insert acc;
		Contact con = testdatafactory.createTestContact(acc);
			con.Num_of_Client_Relationships__c = 0;
			con.Num_of_Prospective_Relationships__c = 0;
			insert con;
		Account acc1 = testdatafactory.createTestAccount();
			acc1.Relationship_Number__c = 'R1234B';
			acc1.RecordTypeId = [Select Id From RecordType where Name ='Client Relationship'].id;
			insert acc1;	
		Connections__c conctn1 = testdatafactory.createConnection(acc.id, con.id);
			conctn1.Relationship2__c = acc1.id;
			conctn1.RecordTypeId = [Select Id From RecordType where Name ='R:R'].id;
			insert 	conctn1;
		Connections__c conctn2 = testdatafactory.createConnection(acc1.id, con.id);
			conctn1.RecordTypeId = [Select Id From RecordType where Name ='R:R'].id;
			conctn2.Relationship2__c = acc.Id;
			insert 	conctn2;	
	 	Test.StartTest();
	 	AccountRelationshipCountBatch countBatch = new AccountRelationshipCountBatch();
	 		database.executeBatch(countBatch);
	 	Test.stopTest(); 	
	 	Account accnt = [select Num_of_Client_Relationships__c, Num_of_Prospective_Relationships__c from Account where id =:acc.id];
	 		system.assertEquals(accnt.Num_of_Client_Relationships__c, 1);
	 		system.assertEquals(accnt.Num_of_Prospective_Relationships__c, 0);		
	}
}