@isTest
private class Test_TrigAccountContactSync {
    
    static testMethod void Account_Contact_Sync_Test1 ()
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        gs.Integration_User_Name__c = UserInfo.getUserName() ;
        insert gs;

        
        map<String, set<String>> includeFld = new map<String, set<String>> () ;
        set<String> tmpFld = new set<String> ();
        tmpFLd.add('Relationship__c') ;
        includeFld.put('Accounts__c' , tmpFld);
        TestDataCreator_Utils.IncludedFields = includeFld ;
        List<Accounts__c> acctList = TestDataCreator_Utils.createSObjectList('Accounts__c' , true , 3); 
        insert acctList;
        TestDataCreator_Utils.Clear();
        List<Contact> contactList = TestDataCreator_Utils.createSObjectList('Contact' , true , 3); 
        insert contactList;
        TestDataCreator_Utils.Clear();
        TestDataCreator_Utils.FillAllFields = true;
        List<Account_Contact__c> acctContactList = TestDataCreator_Utils.createSObjectList('Account_Contact__c' , false , 3); 
        For (integer i = 0 ; i< 3 ; i++)
        {
            acctContactList[i].Account__c = acctList[i].id;
            acctContactList[i].Contact__c = contactList[i].id;
	        acctContactList[i].Role__c = 'Third Party' ;
            //acctContactList[i].
        }
        insert acctContactList;
        
        TestDataCreator_Utils.Clear();
        List<Account_Contact_Sync__c> acctContactSyncList = TestDataCreator_Utils.createSObjectList('Account_Contact_Sync__c' , false , 3); 
        for (integer c = 0 ; c <3 ;c++ )
        {
            acctContactSyncList[c].Account__c = acctList[0].id ;
            acctContactSyncList[c].Account_Contact__c = acctContactList[0].id ;
            acctContactSyncList[c].Contact__c = contactList[0].id ;
	        acctContactSyncList[c].Role__c = 'Third Party' ;
        }

        insert acctContactSyncList ;

        acctContactSyncList[0].Role__c = 'Third Party' ;
        Update acctContactSyncList[0] ;

        delete acctContactSyncList[0];




    }
    
    
    
}