@isTest(seeAllData = true)
public with sharing class TestDeleteIntegrationUserDataBatch {
	 static testMethod void DeleteIntegrationUserDataBatchTest(){
	 	Profile prf = [Select Id From Profile where Name = 'Bessemer Integration'];
	 	User usr = [Select id from User where id = '005f0000001KQvX'];
	 	if(DeleteIntUserDataConfig__c.getInstance('Account') == null){
		 	DeleteIntUserDataConfig__c dUserData = new DeleteIntUserDataConfig__c(Name = 'Account');
		 	insert dUserData;
	 	}
	 	system.runAs(usr){
		 	Account acc = TestDataFactory.createTestAccount();
		 		acc.Relationship_Number__c = '12345';
		 		acc.EDB_Entity_Id__c = '123';
		 		insert acc; 	
		 	Test.StartTest();
		 	DeleteIntegrationUserDataBatch delBatch = new DeleteIntegrationUserDataBatch();
		 	database.executeBatch(delBatch);
		 	Test.stopTest(); 	
	 	}
	 }
}