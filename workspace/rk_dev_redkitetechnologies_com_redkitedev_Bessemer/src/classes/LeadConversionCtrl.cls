/*
	
     Authors :  David Brandenburg
     Created Date: 2014-06-17
     Last Modified: 2014-06-23
     
     Purpose: Controller for LeadConversion page.  Is used as a custom lead conversion page
*/
public with sharing class LeadConversionCtrl 
{
	public Task TaskObj {get;set;}
	public Contact AccountObj {get;set;}
	public boolean createNew {get;set;}
	public boolean createTask {get;set;}
	public Contact DateComponent {get;set;}
	public string TimeComponent {get;set;}
	public string CompanyName {get;set;}
	
	private string LeadId ;
	private String LeadCompanyName ;
	
	public  LeadConversionCtrl ()
	{
		TaskObj = new Task();
		AccountObj = new Contact();
		LeadId = ApexPages.currentPage().getParameters().get('id');
		Lead ld = [select id, Company from Lead where id =:LeadId];
		LeadCompanyName = ld.Company;
		createTask = false;
		createNew = false;
		DateComponent = new Contact();
	}
	
	public list<selectOption> getTimeFrames ()
	{
		list<selectOption> retval = new list<selectOption> ();
		Time tm = time.newInstance(0, 0, 0, 0);
		DateTime dt = DateTime.newInstance(System.Now().date(), tm) ;
		for (integer c = 1 ; c<=48;c++)
		{
			string str = dt.format('hh:mm a');
			retval.add(new selectOption(str, str) );
			dt = dt.addMinutes(30);
		}
		return retval;
	}
	
	public PageReference Cancel ()
	{
		PageReference pgRef = new PageReference('/' + LeadId );
		pgRef.setRedirect(true);
		return pgRef;
	}
	
	public PageReference convert  ()
	{
		if (createTask)
		{
			if (TaskObj.Subject == null || TaskObj.Subject.length() ==0)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'The Task subject is required');
				ApexPages.addMessage(myMsg);
				return null;
			}
			
		}
		//Validate the reminder data is set
		if (TaskObj.isReminderSet)
		{
			if (DateComponent.Birthdate == null)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'The Reminder date is required');
				ApexPages.addMessage(myMsg);
				return null;
			}
			
		}
		
		list<string> contactId = new list<string> () ;
		string redirectID = ConvertLeadNewRecord (LeadId, '' , AccountObj.AccountId , false, '' , contactId );	
		//List<Apexpages.Message> msgs = ApexPages.getMessages();
		
		if (redirectID == 'PageError')
			return null;
			
		if (createTask)
		{
			TaskObj.WhatId = redirectID;
			if (contactId.size() > 0 )
				TaskObj.WhoId = contactId[0];
			if (TaskObj.isReminderSet)
			{
				
				DateTime dt ;
				if (TimeComponent.contains('PM') )
				{
					string [] timeSplit = TimeComponent.substring(0,5).split(':') ;
					time tm = time.newInstance(Integer.ValueOf(timeSplit[0]),Integer.ValueOf(timeSplit[1]), 0, 0) ;
					tm.addHours(12);
					dt = DateTime.newInstance(DateComponent.Birthdate,tm );
				}
				else
				{
					string [] timeSplit = TimeComponent.substring(0,5).split(':') ;
					time tm = time.newInstance(Integer.ValueOf(timeSplit[0]),Integer.ValueOf(timeSplit[1]), 0, 0) ;
					dt = DateTime.newInstance(DateComponent.Birthdate ,tm );
				}
				taskObj.ReminderDateTime = dt;
			}
			insert  TaskObj ;
		}
		PageReference pgRef = new PageReference('/' + redirectID);
		pgRef.setRedirect(true);
		return pgRef;
		
	}
	private string ConvertLeadNewRecord (string LeadID, string AccountOwner, string AccountID , boolean DoCreateOpportunity , string OpportunityName , list<string> newContactId)
    {
    	Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(LeadId);
		//Need to set the converted status
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);

		//lc.setOwnerId (AccountOwner);
		if (createNew == false && AccountId != null)
			lc.setAccountId( AccountID) ;
			
		if(DoCreateOpportunity )
			lc.setOpportunityName(OpportunityName);
		else
			lc.setDoNotCreateOpportunity(true);
		
		Lead Leadrec = [select Street,City, State, PostalCode , SSN_TIN__c from Lead where id = :LeadID] ;
		
		Database.LeadConvertResult lcr ;
		SavePoint sp = Database.setSavePoint();
		
		try 
		{
			List<string> ldList = new List<string>();
			ldList.add(LeadId);
				
			lcr = Database.convertLead(lc);
			RecursiveFlagHandler.setRunning(ldList); 	
			//Change the Account Name if Needed
			if (createNew && CompanyName != LeadCompanyName)
			{
				Account Acct = [Select id, Name from Account where Id = :lcr.getAccountId()] ;
				Acct.Name = CompanyName ;
				Update Acct;
			}
		
			newContactId.add(lcr.getContactId());
			string accountRT = [select id from RecordType where sObjectType = 'Account' and DeveloperName = 'Prospective_Relationship'  ].id;
			Account acctRec =  [select id, RecordTypeId from Account where id = :lcr.getAccountId()] ;
			acctRec.RecordTypeId = accountRT ;
			update acctRec;
			string contactRT = [select id from RecordType where sObjectType = 'Contact' and DeveloperName = 'Prospect' ].id;
			Contact contactRec =  [select id, RecordTypeId from Contact where id = :lcr.getContactId()] ;
			contactRec.RecordTypeId = contactRT ;
			contactRec.SSN_TIN__c = Leadrec.SSN_TIN__c ;

			update contactRec;
		
			//add the Connection Object
			
			string RecTypeId = [select id from RecordType where sObjectType = 'Connections__c' and DeveloperName = 'R_C'  ].id;
			Connections__c connection = [select Role_on_Relationship__c from Connections__c where Relationship_R1__c =  :lcr.getAccountId() and Contact_C1__c = :lcr.getContactId() and RecordTypeId = :RecTypeId ];
			
			//Check to see if a PDM already exists.
			List<Connections__c> existingConnections = [select id from Connections__c where Relationship_R1__c =  :lcr.getAccountId() and Role_on_Relationship__c = 'Primary Decision Maker' and RecordTypeId = :RecTypeId];
			
			
				if (existingConnections.size() == 0 && connection != null)
				{
					connection.Role_on_Relationship__c = 'Primary Decision Maker';
					update connection ;
				}

			//Delete the extra Address Record
			List<Addresses__c> add =  [select id, Primary_Address__c from Addresses__c where Contact__c = :contactRec.id and  Street_Address__c = :LeadRec.Street and City__c = :Leadrec.City and State__c = :LeadRec.State and Postal_code__c = :Leadrec.PostalCode];
			if (!add.isEmpty())
			{	for (Addresses__c a :add)
				{
					if (a.Primary_Address__c == false )
						a.Primary_Address__c = true;
					else
						a.Primary_Address__c = false;
				}
				update add;
			}

			add =  [select id from Addresses__c where Contact__c = :contactRec.id and  Primary_Address__c = false and Street_Address__c = :LeadRec.Street and City__c = :Leadrec.City and State__c = :LeadRec.State and Postal_code__c = :Leadrec.PostalCode];
			if (!add.isEmpty())
				delete add;
			
		}
		catch (Exception ex )
		{
			// Use Rollback 
           	Database.rollback(sp);
			string errorMsg = 'The lead has NOT been converted but an error occurred post-processing.  Contact your System Administrator' ;
        	ApexPages.Message myMsg = new ApexPages.Message (ApexPages.Severity.Info , errorMsg);
        	ApexPages.addMessage(myMsg);
        	myMsg = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage());
    		ApexPages.addMessage(myMsg);
    		return 'PageError';
		}
		
		
		
		
		return  lcr.getAccountId();
			
		
		
    }
}