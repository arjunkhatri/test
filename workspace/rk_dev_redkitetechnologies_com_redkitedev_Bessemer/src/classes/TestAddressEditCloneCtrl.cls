@isTest
private class TestAddressEditCloneCtrl {

	 static testMethod void AddresseditCloneCtrl_Test1() 
  {
        createTestDataContacts () ;
        List<Addresses__c> ad = [select id, Contact__c from Addresses__c  ];
      System.assertEquals(ad.size() , 5) ;
          //List<Contact>  ct = [select Id, accountId from Contact where id = :ad[0].Contact__c] ;
          
                
          ApexPages.StandardController sc = new ApexPages.StandardController(ad[0]);
        
          PageReference pageRef = Page.AddressEditClone;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('retUrl', ad[0].Contact__c);
      ApexPages.currentPage().getParameters().put('ContactId', ad[0].Contact__c);
      ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
          Test.startTest() ;
          AddresseditCloneCtrl  myCon = new AddresseditCloneCtrl (sc);
          
       myCon.init();
        
        Addresses__c add = myCon.addressObj;
        add.City__c = 'Test';
      add.State__c = 'TX';
      add.Address_Type__c = 'Other 2';


      
      System.assertEquals( 4 , myCon.relatedAddresses.KeySet().size()  ) ;
      List<string> addressKeys = new List<String>();
      for (string str : myCon.relatedAddresses.KeySet() )
        addressKeys.add(str);
     
      myCon.relatedAddresses.get(addressKeys[0]).addressList[0].copyAddress = true;
      myCon.relatedAddresses.get(addressKeys[1]).newAddress = true;
      System.assertEquals(true , myCon.relatedAddresses.get(addressKeys[0]).addressList[0].copyAddress );
        myCon.saveAndMore();
      //myCon.SaveAndClose();
      List<Apexpages.Message> msgs = ApexPages.getMessages();
      System.assertEquals('Addresses updated successfully' , msgs[0].getDetail() ) ;

      List<Addresses__c> addList = [select id from Addresses__c where Contact__c = :addressKeys[1]] ;
      System.assertEquals(2 , addList.size() ) ;

        Test.stopTest();
  }
  
  static testMethod void AddresseditCloneCtrl_Test2() 
  {
      createTestDataLeads () ;
      List<Addresses__c> ad = [select id, Lead__c from Addresses__c  ];
      System.assertEquals(ad.size() , 5) ;
      //List<Contact>  ct = [select Id, accountId from Contact where id = :ad[0].Contact__c] ;
      
          
      ApexPages.StandardController sc = new ApexPages.StandardController(ad[0]);
    
      PageReference pageRef = Page.AddressEditClone;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('retUrl', '/' + ad[0].Lead__c);
      ApexPages.currentPage().getParameters().put('LeadId', ad[0].Lead__c);
      ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
      Test.startTest() ;
      AddresseditCloneCtrl  myCon = new AddresseditCloneCtrl (sc);
      
      myCon.init();
      
      Addresses__c add = myCon.addressObj;
      add.City__c = 'Test';
      add.State__c = 'CA';
      add.Address_Type__c = 'Other 2';
      myCon.saveAndMore();
      
      List<Apexpages.Message> msgs = ApexPages.getMessages();
      System.assertEquals('Addresses updated successfully' , msgs[0].getDetail() ) ;

      List<Addresses__c> addList = [select id , State__c  from Addresses__c where Id = :ad[0].id] ;
      System.assertEquals('CA' , addList[0].State__c ) ;
      add = myCon.addressObj;
      add.State__c = 'CA';
      myCon.SaveAndClose();
      Test.stopTest();
  }
  static testMethod void AddresseditCloneCtrl_Test3() 
  {
      //Purpose Test Creating A New Address
      createTestDataContacts () ;
      List<Contact>  ct = [select Id, accountId from Contact limit 1 ] ;
      ApexPages.StandardController sc = new ApexPages.StandardController(new Addresses__c ());
    
      PageReference pageRef = Page.AddressEditClone;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('retUrl', ct[0].id );
      ApexPages.currentPage().getParameters().put('ContactId', ct[0].id);
      ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
      Test.startTest() ;
      AddresseditCloneCtrl  myCon = new AddresseditCloneCtrl (sc);
      
       myCon.init();
      
      Addresses__c add = myCon.addressObj;
      add.City__c = 'Test';
      add.State__c = 'AK';
      add.Address_Type__c = 'Other 2';

      myCon.saveAndMore();
      myCon.Cancel();

      List<Addresses__c> addList = [select id , State__c from Addresses__c where Contact__c = :ct[0].id and State__c = 'AK'] ;
      System.assertEquals(1 , addList.size() ) ;

      Test.stopTest();
  }
  static testMethod void AddresseditCloneCtrl_Test4() 
  {
      //Purpose Test Creating A New Address
      createTestDataLeads () ;
      List<Lead>  ld = [select Id from Lead limit 1 ] ;
      ApexPages.StandardController sc = new ApexPages.StandardController(new Addresses__c ());
    
      PageReference pageRef = Page.AddressEditClone;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('retUrl', ld[0].id );
      ApexPages.currentPage().getParameters().put('LeadId', ld[0].id);
      ApexPages.currentPage().getHeaders().put('USER-AGENT' , 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
      Test.startTest() ;
      AddresseditCloneCtrl  myCon = new AddresseditCloneCtrl (sc);
      
       myCon.init();
      
      Addresses__c add = myCon.addressObj;
      add.City__c = 'Test';
      add.State__c = 'AK';
      add.Address_Type__c = 'Other 2';
      myCon.SaveAndClose();


      List<Addresses__c> addList = [select id , State__c from Addresses__c where Lead__c = :ld[0].id and State__c = 'AK'] ;
      System.assertEquals(1 , addList.size() ) ;

      Test.stopTest();
  }


    
    private static void createTestDataContacts ()
  {
    
      
      /********** Create test data *****************/
      GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;
        
      //The Data Creator will Create both the Address and Contact Objects.
      map<String, set<String>> ExcludeMap = new map<String, set<String>> ();
        ExcludeMap = TestDataCreator_Utils.ExcludedFields ;
        set<string> excludeFld = new set<string> ();
        //excludeFld.add('Primary_Decision_Maker__c') ;
        excludeFld.add('geopointe__Geocode__c') ;
        
        //There are values already in the map so need rebuild
        if (TestDataCreator_Utils.ExcludedFields.containskey('Account') )
        {
          set<string> tmpSet = TestDataCreator_Utils.ExcludedFields.get('Account') ;
          for (string key : tmpSet)
            excludeFld.add( key) ;
        }
        ExcludeMap.put('Account', exCludeFld);
        
        
        TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;
        List<Account> accountList = TestDataCreator_Utils.createSObjectList('Account' , false , 1); 
        insert accountList;

        excludeFld.add('Lead__c') ;
        ExcludeMap.put('Addresses__c', exCludeFld);

        
        TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;
        List<Addresses__c> addressList = TestDataCreator_Utils.createSObjectList('Addresses__c' , true , 5); 
        insert addressList;
        

        List<Contact> ctList = [select Id, AccountId from Contact where CreatedDate = Today  ];
        for (Contact ct : ctList)
          ct.AccountId = accountList [0].id ;
        Update ctList ; 
    
     
 
  }
  private static void createTestDataLeads ()
  {
    
      
      /********** Create test data *****************/
      GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;
     

        map<String, set<String>> ExcludeMap = new map<String, set<String>> ();
        ExcludeMap = TestDataCreator_Utils.ExcludedFields ;
        set<string> excludeFld = new set<string> ();
        excludeFld.add('Contact__c') ;
        ExcludeMap.put('Addresses__c', exCludeFld);

        TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;
        List<Addresses__c> addressList = TestDataCreator_Utils.createSObjectList('Addresses__c' , true , 5); 
        insert addressList;
        

        
  }
	
	
}