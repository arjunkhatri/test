/**
* Utility_Common - class intended to contain shared reusable code
*/
public with sharing class Utility_Common {
	public static map<String, String> inverseRelationshipMap {
		get{
			if (inverseRelationshipMap==null){
				inverseRelationshipMap = new map<String,String>();
				list<Inverse_Relationship__c> invRelList = Inverse_Relationship__c.getAll().values();
				for (Inverse_Relationship__c currInvRel : invRelList){
					inverseRelationshipMap.put(currInvRel.Relationship__c, currInvRel.Relationship_Inverse__c);
				}
			}
			return inverseRelationshipMap;
		}
		set;
	}
	
	public static string GetFieldList (string ObjectName)
    {
    	string retval = '';
    	SObjectType objToken = Schema.getGlobalDescribe().get(ObjectName);
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap(); 
       
        Set<String> fieldSet = fields.keySet();
        list<string> tmpList = new list<string>() ;
        for(String s:fieldSet)
        {
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            if (selectedField.isAccessible())
            	tmpList.add(selectedField.getName()) ;
        }
        
        tmpList.sort();
        
        for (string s : tmpList)
        	retval += s + ',' ;
        
        return retval.substring(0, retval.length()-1);
    }
    
    //Get a list of picklist values
    /* 
    public static List<Schema.PicklistEntry> GetObjectsPickListValues (string ObjName, string objField)
    {
	    Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Map <String, Schema.SObjectField> field_Map = schemaMap.get(objName).getDescribe().fields.getMap();
		List<Schema.PicklistEntry> ple = field_map.get(objField).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
   		return ple;
    }
    */
    
    //Get a list of picklist values 
    public static set<string> GetObjectsPickListValues (string ObjName, string objField)
    {
	    Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Map <String, Schema.SObjectField> field_Map = schemaMap.get(objName).getDescribe().fields.getMap();
		List<Schema.PicklistEntry> ple = field_map.get(objField).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
   		set<string> returnValues =  new set<string> ();
   		for (Schema.PicklistEntry p : ple)
   			returnValues.add(String.ValueOf(p.getValue() ) );
   			
   		return returnValues;
    }
	
	

}