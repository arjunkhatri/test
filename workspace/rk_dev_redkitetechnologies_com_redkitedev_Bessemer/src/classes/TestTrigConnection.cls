/*
    
     Authors :  David Brandenburg
     Created Date: 2014-04-30
     Last Modified: 2014-04-30
     
     Purpose:  Test Class for the Connection Object Trigger

*/



@isTest
private class TestTrigConnection {

    //Tests the Connecton Relation To RelationShip trigger
    static testMethod void Connection_Relationship_Test() 
    {
        CreateTestData ();
        List<Account> accountList = [select id ,Name from Account];
        list<RecordType> rt = [select id, Name from RecordType where SobjectType = 'Connections__c' ];
        string RtId = ''; 
        for (RecordType r :rt )
        {
            if (r.Name == 'R:R')
            {
                RtId = r.id;
                break;
            }
        } 
       
        Test.startTest();
        
        Connections__c connection = new Connections__c();
        connection.RecordTypeId = RtId;
        connection.Relationship_R1__c = accountList[0].id;
        connection.Relationship2__c  = accountList[1].id ;
        connection.Role_on_Relationship__c = 'Primary Member';
        insert connection;
        
        List<Connections__c> connectionCheck = [select id, Role_on_Relationship__c from Connections__c where Relationship_R1__c = :accountList[1].id];
        System.assertEquals(2, connectionCheck.size()) ;
        System.assertEquals('Other', connectionCheck[0].Role_on_Relationship__c) ;
        
        
        //Update the Connection to a differnet role
        connection = [select id, RecordTypeId , Role_on_Relationship__c from Connections__c where id = :connection.id];
        ConnectionTrigHandHelper.setAlreadyCreatedInverseStopped();
        connection.Role_on_Relationship__c = 'Decision Maker';
        update connection;
        
        //Check to see if the inverse record changed.
        connectionCheck = [select Role_on_Relationship__c from Connections__c where Relationship_R1__c = :accountList[1].id];
        System.assertEquals(2, connectionCheck.size()) ;
        System.assertEquals('Other', connectionCheck[0].Role_on_Relationship__c) ;
        
        ConnectionTrigHandHelper.setAlreadyCreatedInverseStopped();
        delete connection;
        connectionCheck = [select Role_on_Relationship__c from Connections__c where Relationship_R1__c = :accountList[1].id];
        System.assertEquals(1, connectionCheck.size()) ;
        
        Test.stopTest();
        
        
        
       
    }
    
    //Tests the Connecton Relation To RelationShip trigger
    static testMethod void Connection_Contact_Test() 
    {
        CreateTestData ();
        List<Contact> contactList = [select id ,Name from Contact];
        list<RecordType> rt = [select id, Name from RecordType where SobjectType = 'Connections__c' ];
        string RtId = ''; 
        for (RecordType r :rt )
        {
            if (r.Name == 'C:C')
            {
                RtId = r.id;
                break;
            }
        } 
       
        Test.startTest();
        
        Connections__c connection = new Connections__c();
        connection.RecordTypeId = RtId;
        connection.Contact_C1__c = contactList[0].id;
        connection.Contact_C2__c  = contactList[1].id ;
        connection.Role_on_Relationship__c = 'Parent';
        insert connection;
        
        List<Connections__c> connectionCheck = [select id, Role_on_Relationship__c from Connections__c where Contact_C1__c = :contactList[1].id];
        System.assertEquals(2, connectionCheck.size()) ;
        System.assertEquals('Other', connectionCheck[0].Role_on_Relationship__c) ;
        
        
        //Update the Connection to a differnet role
        connection = [select id, RecordTypeId , Role_on_Relationship__c from Connections__c where id = :connection.id];
        ConnectionTrigHandHelper.setAlreadyCreatedInverseStopped();
        connection.Role_on_Relationship__c = 'Brother';
        update connection;
        
        //Check to see if the inverse record changed.
        connectionCheck = [select Role_on_Relationship__c from Connections__c where Contact_C1__c = :contactList[1].id];
        System.assertEquals(2, connectionCheck.size()) ;
        System.assertEquals('Other', connectionCheck[0].Role_on_Relationship__c) ;
        
        ConnectionTrigHandHelper.setAlreadyCreatedInverseStopped();
        delete connection;
        connectionCheck = [select Role_on_Relationship__c from Connections__c where Contact_C1__c = :contactList[1].id];
        System.assertEquals(1, connectionCheck.size()) ;
        
        Test.stopTest();
        
        
        
       
    }
   
    
    private static void CreateTestData ()
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        Account acct = new Account ();
        acct.Name = 'Test Company 1' ;
        insert acct;
        
        Contact ct = new Contact ();
        ct.AccountId = acct.id;
        ct.FirstName = 'Test';
        ct.LastName = 'User';
        ct.MailingStreet = '1 test ave';
        insert ct;
        
        acct = new Account ();
        acct.Name = 'Test Company 2' ;
        insert acct;
        
        ct = new Contact ();
        ct.AccountId = acct.id;
        ct.FirstName = 'Test';
        ct.LastName = 'User';
        ct.MailingStreet = '1 test ave';
        insert ct;
        
        //Build the Inverse Mapping Object
        Inverse_Connection_Role__c mapping = new Inverse_Connection_Role__c();
        mapping.Name = 'R:R Primary Member';
        mapping.Connection_Record_Type__c = 'R:R';
        mapping.Role__c = 'Primary Member';
        mapping.Inverse_Role__c = 'Primary';
        insert mapping;
        
        mapping = new Inverse_Connection_Role__c();
        mapping.Name = 'R:R Decision Maker';
        mapping.Connection_Record_Type__c = 'R:R';
        mapping.Role__c = 'Decision Maker';
        mapping.Inverse_Role__c = 'Maker';
        insert mapping;
        
        mapping = new Inverse_Connection_Role__c();
        mapping.Name = 'C:C Parent';
        mapping.Connection_Record_Type__c = 'C:C';
        mapping.Role__c = 'Parent';
        mapping.Inverse_Role__c = 'Child';
        insert mapping;
        
        mapping = new Inverse_Connection_Role__c();
        mapping.Name = 'C:C Brother';
        mapping.Connection_Record_Type__c = 'C:C';
        mapping.Role__c = 'Brother';
        mapping.Inverse_Role__c = 'Sister';
        insert mapping;
        
        
        
    }
}