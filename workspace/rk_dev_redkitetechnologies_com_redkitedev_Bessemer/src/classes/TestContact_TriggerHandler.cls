@isTest
public with sharing class TestContact_TriggerHandler {
   static testMethod void testContacttriggerHandler() 
    {
    	List<Account> lstAccount = new List<Account>();
    	List<Contact> lstContact = new List<Contact>();
    	GlobalSettings__c gs = TestDataFactory.createGlobalSetting();
    		gs.Address_Number_Seq__c = 12200122;
    		insert gs;
		RecordType clientRT = [Select Id From RecordType where name='Client Contact'];    
				
        Account acc = TestDataFactory.createTestAccount();
        	acc.Relationship_Number__c = '12345';
        	lstAccount.add(acc);
        Account acc2 = TestDataFactory.createTestAccount();
        	acc2.Relationship_Number__c = '12233';  
        	lstAccount.add(acc2);  
        Account acc3 = TestDataFactory.createTestAccount();
        	acc3.Relationship_Number__c = '15433';  
        	lstAccount.add(acc3);        	    	
        	insert lstAccount;
        	system.assert(lstAccount.size() == 3,'Account records created successfully');
        	
        Contact con = TestDataFactory.createTestContact(acc);
        	lstContact.add(con);
        Contact con2 = TestDataFactory.createTestContact(lstAccount[1]);
        	con2.Contact_S_Number__c = 'con121212';
        	lstContact.add(con2); 		
        Contact con3 = TestDataFactory.createTestContact(lstAccount[2]);
        	con3.Contact_S_Number__c = 'con121213';
        	con3.RecordTypeId = clientRT.id;        	
        	lstContact.add(con3);
        	insert 	lstContact;
        	system.assert(lstContact.size() == 3,'Contact records created successfully');
        	
 		Connections__c con1 = TestDataFactory.createConnection(lstAccount[0].id,lstContact[0].id);
 			con1.Role_on_Relationship__c = 'Primary Decision Maker';
 			insert con1;
        List<Contact> cont = [Select id,AccountId from Contact where id=:lstContact[0].id limit 1];
        	cont[0].AccountId = null;
        	update cont;  

 		Connections__c conn = TestDataFactory.createConnection(lstAccount[1].id,lstContact[1].id);
 			conn.Role_on_Relationship__c = 'Decision Maker';
 			insert conn;
        List<Contact> cont2 = [Select id,AccountId from Contact where id=:lstContact[1].id limit 1];
        	cont2[0].AccountId = null;
        	update cont2; 
 
 		Connections__c conn3 = TestDataFactory.createConnection(lstAccount[2].id,lstContact[2].id);
 			conn.Role_on_Relationship__c = 'Other';
 			insert conn3;
 			
 		Accounts__c acnt = new Accounts__c();
 				acnt.Relationship__c = lstAccount[2].id;
 				insert acnt;
 		Account_Contact__c acccon = new Account_Contact__c();
 			acccon.Account__c = acnt.id;
 			acccon.Contact__c = lstContact[2].id;
 			acccon.Role__c = 'Beneficiary';
 			insert acccon;
 			 			
        List<Contact> cont3 = [Select id,AccountId from Contact where id=:lstContact[2].id limit 1];
        	cont3[0].AccountId = null;
        	update cont3;        	
        	Contact fCon2 = [Select id,AccountId from Contact where id=:cont3[0].id limit 1];
        	system.assertEquals(fCon2.AccountId, acc3.id);     
    }
}