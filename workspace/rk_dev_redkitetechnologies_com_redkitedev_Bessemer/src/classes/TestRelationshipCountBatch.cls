@isTest
public with sharing class TestRelationshipCountBatch {

	static testMethod void RelationshipCountBatchTest(){
		GlobalSettings__c gs = testdatafactory.createGlobalSetting();
			gs.Address_Number_Seq__c = 10002000;
			insert gs;
		Account acc = testdatafactory.createTestAccount();
			acc.Relationship_Number__c = 'R1234A';
			acc.RecordTypeId = [Select Id From RecordType where Name ='Prospective Relationship'].id;
			insert acc;
		Contact con = testdatafactory.createTestContact(acc);
			con.Num_of_Client_Relationships__c = 0;
			con.Num_of_Prospective_Relationships__c = 0;
			insert con;
		Account acc1 = testdatafactory.createTestAccount();
			acc1.Relationship_Number__c = 'R1234B';
			acc1.RecordTypeId = [Select Id From RecordType where Name ='Client Relationship'].id;
			insert acc1;	
		Connections__c conctn1 = testdatafactory.createConnection(acc.id, con.id);
			conctn1.RecordTypeId = [Select Id From RecordType where Name ='R:C'].id;
			insert 	conctn1;
		Connections__c conctn2 = testdatafactory.createConnection(acc1.id, con.id);
			conctn1.RecordTypeId = [Select Id From RecordType where Name ='R:C'].id;
			insert 	conctn2;	
	 	Test.StartTest();
	 	RelationshipCountBatch countBatch = new RelationshipCountBatch();
	 		database.executeBatch(countBatch);
	 	Test.stopTest(); 	
	 	Contact cont = [select Num_of_Client_Relationships__c, Num_of_Prospective_Relationships__c from Contact where id =:con.id];
	 		system.assertEquals(cont.Num_of_Client_Relationships__c, 0);
	 		system.assertEquals(cont.Num_of_Prospective_Relationships__c, 1);	 					
	}

}