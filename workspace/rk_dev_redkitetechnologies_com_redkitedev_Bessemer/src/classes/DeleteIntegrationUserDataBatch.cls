global class DeleteIntegrationUserDataBatch implements Database.Batchable<SObject>{
	public List<SObject> listSobject;
	public static final String userId ='005f0000001KQvX';
	public DeleteIntegrationUserDataBatch(){
		listSobject = new List<SObject>();
		List<DeleteIntUserDataConfig__c> listObj = DeleteIntUserDataConfig__c.getall().values();
		if(listObj != null && listObj.size() > 0){
			for(DeleteIntUserDataConfig__c Obj:listObj){
				String query = 'SELECT ID FROM ';
					query = query + Obj.Name;
					query = query + ' WHERE CreatedById '
							 + ' = \'' 
							 + userId
							 + '\' LIMIT 10000'; 
				List<SObject> objRec = Database.query(query);
				if(objRec != null && objRec.size() > 0){
					listSobject.addAll(objRec);
				}
			}
		}
	}

    global Iterable<SObject> start(Database.BatchableContext BC){
    	return listSobject;
    }
    
	global void execute(Database.BatchableContext BC, List<SObject> scope){
	 	if(scope != null && scope.size() > 0){
	 		Database.DeleteResult[] resDels = Database.delete(scope,false);
	 	}
	} 
	global void finish(Database.BatchableContext BC){
	   	
	}  	   
}