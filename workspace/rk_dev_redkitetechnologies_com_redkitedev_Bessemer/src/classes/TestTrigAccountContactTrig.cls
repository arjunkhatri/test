@isTest
private class TestTrigAccountContactTrig {
	
	static testMethod void  AccountContactTrig_Test1() 
    {
    	GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;

        
        map<String, set<String>> includeFld = new map<String, set<String>> () ;
        set<String> tmpFld = new set<String> ();
        tmpFLd.add('Relationship__c') ;
        includeFld.put('Accounts__c' , tmpFld);
        TestDataCreator_Utils.IncludedFields = includeFld ;
        List<Accounts__c> acctList = TestDataCreator_Utils.createSObjectList('Accounts__c' , true , 3); 
        insert acctList;
        TestDataCreator_Utils.Clear();
        List<Contact> contactList = TestDataCreator_Utils.createSObjectList('Contact' , true , 3); 
        insert contactList;
        TestDataCreator_Utils.Clear();
        TestDataCreator_Utils.FillAllFields = true;
    	List<Account_Contact__c> acctContactList = TestDataCreator_Utils.createSObjectList('Account_Contact__c' , false , 3); 
    	For (integer i = 0 ; i< 3 ; i++)
    	{
    		acctContactList[i].Account__c = acctList[i].id;
    		acctContactList[i].Contact__c = contactList[i].id;
    		//acctContactList[i].
    	}
    	Test.startTest() ;
    	insert acctContactList ;

    	acctContactList[1].Role__c = 'Other' ;
    	Update acctContactList[1] ;

    	acctContactList[1].Role__c = 'Third Party' ;
    	Update acctContactList[1] ;

    	delete acctContactList[2];

    	Test.stopTest();


    }
	 
}