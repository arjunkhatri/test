public with sharing class ManageMailing_MassAddController {

	public static final String PARAM_MAILINGS = 'mailings';
	public static final String PARAM_TYPE = 'type';
	public static final String PARAM_DEBUG = 'debug';

	List<Mailing__c> tobeAdded;
	public List<Contact> contacts;
	public String searchText{get;set;}
	public List<innerContactClass> contactList {get;set;}
	public Mailing__c mailRec{get;set;}
	public String cityFilter {get;set;}
	public String stateFilter {get;set;}
	public String filterText {get;set;}
	public String targetUrl {get;set;}
	private Boolean selectAll {get;set;}
	public String type{get;set;}



	public ManageMailing_MassAddController(ApexPages.StandardController controller)
	{

		tobeAdded = new List<Mailing__c>();
		contactList = new List<innerContactClass>();
		searchText = '';
		mailRec = new Mailing__c();
		type = '';

		map<String, String> params = ApexPages.currentPage().getParameters();

		if (params.containsKey(PARAM_MAILINGS)) mailRec.Mailings__c = params.get(PARAM_MAILINGS);
		if (params.containsKey(PARAM_TYPE)) {
			mailRec.Type__c = params.get(PARAM_TYPE);
			type = ' - ' + params.get(PARAM_TYPE);
		}
		targetUrl = (params.containsKey('saveURL'))  ? params.get('saveURL') : '/';	
	}

	public PageReference searchContacts()
	{
		Id add;
		List<Contact> conList;
		contactList = new List<innerContactClass>();

		String limitString = ' Order By LastName limit 1000';
		String whereString = '';

		String soql = 'select Id, FirstName, LastName, Entity_Type__c, MailingStreet, '+
						  '(select Id from Addresses__r where Primary_Address__c = true), '+	
						  'MailingCity, MailingState, MailingPostalCode, MailingCountry from Contact ';

		list<String> whereStrings = new list<String>();

		if(String.isEmpty(cityFilter)==false) whereStrings.add('MailingCity = :cityFilter');
		if(String.isEmpty(stateFilter)==false) whereStrings.add('MailingState = :stateFilter');
		if (String.isEmpty(filterText)==false) whereStrings.add('(Entity_Type__c like  \'%'+filterText+'%\' OR MailingCity like \'%'+filterText+'%\' '+
		   'OR MailingState like  \'%'+filterText+'%\' or MailingCountry like \'%'+filterText+'%\' or LastName like \'%'+filterText+'%\' or  FirstName like \'%'+filterText+'%\' '+
		   'OR MailingStreet like \'%'+filterText+'%\' )'); 


		if(whereStrings.size()>0) whereString = ' where '+ String.join(whereStrings, ' and ');

		soql += whereString + limitString;	

		System.Debug('SOQL:'+soql);
		conList = Database.query(soql);	


		for(Contact cont: conList)
		{

			for(Addresses__c addr: cont.Addresses__r)
			{
				add = addr.Id;
				break;
			}
			contactList.add(new innerContactClass(cont, add));
		}
		return null;

	}

	public PageReference selectAll(){

		if(selectAll!=null && selectAll) 
			selectAll = false;
		else
			selectAll = true;

		for(innerContactClass contObj : contactList)
		{
			
			contObj.selected  = (selectAll) ? true : false;
		}

		return null;
	}


	public PageReference addContacts(){

		Mailing__c tempRec;

		//loop through all selected contacts form the table
		for(innerContactClass contObj : contactList)
		{
			if(contObj.selected)
			{
				tempRec = new Mailing__c();
				tempRec.Mailings__c = mailRec.Mailings__c;
				tempRec.Type__c = mailRec.Type__c;
				tempRec.contact__c = contObj.con.Id;
				tempRec.Delivery_Method__c = 'Mail';
				if(contObj.add!=null)
					tempRec.Address__c = contObj.add;

				tobeAdded.add(tempRec);
			}
		}

		if(tobeAdded.size()!=0)
		{
			try
			{
				upsert tobeAdded;
				return new PageReference(targetUrl);
			}
			catch (Exception e)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  e.getMessage() ); 
				ApexPages.addMessage(myMsg);
			}
		}

		return null;
	}

	//wrapper class to display list of contacts
	public class innerContactClass{
        public Contact con {get;set;}
        public Id add {get;set;}
        public boolean selected {get;set;}
        //Constructor
        public innerContactClass(Contact c, Id addId){
            con = c;
            selected = false;
            if(addId!=null)
            	add= addId;
        }
    }

}