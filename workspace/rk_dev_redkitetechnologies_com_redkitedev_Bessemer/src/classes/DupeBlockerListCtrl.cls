public with sharing class DupeBlockerListCtrl extends ComponentControllerBase 
{
    public List<GlobalContactSearch.ContactResultSet> ContactDupList {get;set;}
    public List<GlobalContactSearch.LeadResultSet> LeadDupList {get;set;}
    public boolean OverRideDup { get;set;}
    transient boolean isOverride ;
    public boolean hasDuplicates {get;set;}
    public string ParentRerender {get;set;}
    //private Contact currentContact ;
    
    
    public DupeBlockerListCtrl  ()
    {
        ContactDupList = new List<GlobalContactSearch.ContactResultSet> ();
        LeadDupList =  new List<GlobalContactSearch.LeadResultSet> ();
        OverRideDup  = false;
        hasDuplicates = false;
    }
    
    public void OverRideDupeBlocker ()
    {
        CRMfusionDBR101.DB_Api.preventBlocking();
        hasDuplicates = false ;
        
        /*
        //Mark the Records as Bypassed
        if (ContactDupList != null && ContactDupList.size() > 0)
        {
            List<Contact> ctList = new List<Contact> ();
            for(GlobalContactSearch.ContactResultSet rs : ContactDupList )
            {
                Contact tmpCt = rs.Contact;
                tmpCt.Dupe_Blocker_Bypass__c = true;
                ctList.add(tmpCt);
            }
            update ctList ; 
        }
        
         CRMfusionDBR101.DB_Api.preventBlocking();
        */
    }
    
    public boolean CheckLeadDups (Lead leadObj)
    {
        if (!RecursiveFlagHandler.alreadyRunning('Lead') && !RecursiveFlagHandler.alreadyRunning('Contact'))
            hasDuplicates = false;
            
        if (RecursiveFlagHandler.alreadyRunning('Contact') )
            return hasDuplicates;
            
        if (!Schema.sObjectType.Lead.isAccessible())
            return false;
            
        GlobalContactSearch searchObj = new GlobalContactSearch ();
        List<string> returnList = new List<string>() ;
        List<boolean> returnHardDupsList = new List<boolean>() ;
        LeadDupList  = searchObj.CheckForLeadDuplicates(leadObj , returnList ,returnHardDupsList);
        
        System.Debug('*** ' + returnHardDupsList );
        
        if (leadObj.Dupe_Blocker_Bypass__c && returnHardDupsList[0] == false )
        {
            LeadDupList.Clear();
            hasDuplicates = false;
            CRMfusionDBR101.DB_Api.preventBlocking();
        }
        
        else if (LeadDupList.size() > 0)
        {
            if (!hasDuplicates)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'A duplicate contact(s) exists in the system.'));
                hasDuplicates = true;
            }
            if ( OverRideDup == false)
                OverRideDup = returnHardDupsList[0];
           
        }
        
        
        
        
        Contact ct = new Contact ();
        ct.FirstName = leadObj.FirstName ;
        ct.LastName = leadObj.LastName ;
        ct.Email = leadObj.Email ;
        ct.MailingStreet = leadObj.Street;
        ct.MailingCity = leadObj.City;
        ct.MailingState = leadObj.State;
        list<string> tmpList = new list<string>();
        tmpList.add('Contact');
        RecursiveFlagHandler.setRunning(tmpList);
        CheckContactDups (ct);

         
        return hasDuplicates;
    }
    
    public boolean CheckContactDups (Contact ContactObj)
    {
        if (!RecursiveFlagHandler.alreadyRunning('Lead') && !RecursiveFlagHandler.alreadyRunning('Contact'))
            hasDuplicates = false;
            
        if (RecursiveFlagHandler.alreadyRunning('Lead') )
            return hasDuplicates;
        
        if (!Schema.sObjectType.Contact.isAccessible())
            return false;
            
        
            
        
        GlobalContactSearch searchObj = new GlobalContactSearch ();
        List<string> returnList = new List<string>() ;
        List<boolean> returnHardDupsList = new List<boolean>() ;
        ContactDupList = searchObj.CheckForDuplicates(ContactObj , returnList ,returnHardDupsList);
        
        if (ContactObj.Dupe_Blocker_Bypass__c && returnHardDupsList[0] == false )
        {
            ContactDupList.Clear();
            hasDuplicates = false;
            CRMfusionDBR101.DB_Api.preventBlocking();
        }
        
        else if (ContactDupList.size() > 0)
        {
            if ( OverRideDup == false  )
                OverRideDup = returnHardDupsList[0];
            if (!hasDuplicates)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'A duplicate contact(s) exists in the system.'));
                hasDuplicates = true;
            }
            
        }
        
        //Now Check the Lead Object for duplicates
        
        {
            Lead ld = new Lead ();
            ld.FirstName = ContactObj.FirstName ;
            ld.LAstName = ContactObj.LastName ;
            ld.Email = ContactObj.Email ;
            list<string> tmpList = new list<string>();
            tmpList.add('Lead');
            RecursiveFlagHandler.setRunning(tmpList);
            CheckLeadDups (ld);
        }
       
        return hasDuplicates;
    }
}