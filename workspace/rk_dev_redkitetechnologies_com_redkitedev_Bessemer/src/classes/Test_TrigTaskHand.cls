@isTest
public with sharing class Test_TrigTaskHand {
    static testMethod void taskTrigHandtest(){
        Profile pf = [Select Id From Profile where Name= 'Bessemer Wealth Advisor'];
        Profile pf1 = [Select Id From Profile where Name= 'Bessemer Client Advisor'];
        
        User u = TestDataFactory.createTestUser(pf.id);
            insert u;
            system.assert(u.id != null,'Created user record');
        User u1 = TestDataFactory.createTestUser(pf1.id);
            u1.Username = 'test1@domain.com.dev';
            insert u1;
            system.assert(u1.id != null,'Created user record');                         
        Account acc = TestDataFactory.createTestAccount();
            acc.Relationship_Number__c = '1234';
            insert acc;
            system.assert(acc.id != null,'Created relationship record');
        Account acc1 = TestDataFactory.createTestAccount();
            acc1.Relationship_Number__c = '12345';
            insert acc1;
            system.assert(acc1.id != null,'Created relationship record');           
        AccountTeamMember accTM = TestDataFactory.createATM(acc.id,u.id,'Cash Manager');
            insert accTM;
            system.assert(accTM.id != null,'Created account team member record');
        AccountTeamMember accTM1 = TestDataFactory.createATM(acc1.id,u1.id,'Cash Manager');
            insert accTM1;
            system.assert(accTM.id != null,'Created account team member record');                           
        Task tsk = TestDataFactory.createTask(acc.id);
            tsk.Relationship_Team_List__c = '';
            insert tsk;
        Task newTask = [Select Whatid,Relationship_Team_List__c from Task where id=:tsk.id];
            system.assertEquals(newTask.Relationship_Team_List__c, '#'+String.valueOf(u.id).substring(0,15)+'#');
            newTask.WhatId = acc1.id;
            update newTask;
        Task taskUpdated = [Select Whatid,Relationship_Team_List__c from Task where id=:newTask.id];
            system.assertEquals(taskUpdated.Relationship_Team_List__c, '#'+String.valueOf(u1.id).substring(0,15)+'#');          
        Accounts__c accCust = TestDataFactory.customAccount(acc1.id);
            insert accCust;
            taskUpdated.WhatId = accCust.id;
            update taskUpdated; 
    }
    
}