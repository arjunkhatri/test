/*
	
     Authors :  David Brandenburg
     Created Date: 2014-06-23
     Last Modified: 2014-06-23
     
     Purpose: Test Class for Family Group Roolup page. 
*/
@isTest
private class testMethod_FamilyGroupRollupCtrl 
{
	private static void createTestData ()
	{
		/********** Create test data *****************/
    	GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;
        
        set<string> singleGrp = new set<string>() ;
        singleGrp.add('Family_Group__c');
        map<String, set<String>> ExcludeMap = new map<String, set<String>> ();
        ExcludeMap = TestDataCreator_Utils.ExcludedFields ;
        set<string> excludeFld = new set<string> ();
        //excludeFld.add('Primary_Decision_Maker__c') ;
        excludeFld.add('geopointe__Geocode__c') ;
        
        //There are values already in the map so need rebuild
        if (TestDataCreator_Utils.ExcludedFields.containskey('Account') )
        {
        	set<string> tmpSet = TestDataCreator_Utils.ExcludedFields.get('Account') ;
        	for (string key : tmpSet)
        		excludeFld.add( key) ;
        }
        ExcludeMap.put('Account', exCludeFld);
        
       	
        TestDataCreator_Utils.ExcludedFields  = ExcludeMap ;

        TestDataCreator_Utils.SingleRelatedRecord = singleGrp ;
		List<Account> accountList = TestDataCreator_Utils.createSObjectList('Account' , true , 26); 
		insert accountList;
		
		 
		//********************************/
	}
	static testMethod void FamilyGroupRollupCtrl_Test1() 
    {
       
		createTestData () ;
		
		Family_Group__c fg = [select id from Family_Group__c Limit 1 ];
		ApexPages.StandardController sc = new ApexPages.StandardController(fg);
		Test.startTest() ;
		FamilyGroupRollupCtrl myCon = new FamilyGroupRollupCtrl(sc);
		
		myCon.init() ;
		System.assertEquals(10, myCon.AccountSearchResults.size() );
		System.assertEquals(true, myCon.getHasNext() );
		myCon.doLast() ;
		System.assertEquals(true, myCon.getHasPrevious() );
		mycon.doFirst();
		System.assertEquals(26, myCon.getTotalRecords() );
		System.assertEquals(3, myCon.getTotalPages() );
		System.assertEquals(1, myCon.getPageNumber() );
		
		myCon.RequestAccessId = myCon.AccountSearchResults[2].Account.id;
		myCon.RequestOwnership = false ;
		myCon.RequestMessage = 'Ownership reason';
		//myCon.SendRequestEmail();
		
		Test.stopTest();
		
    }
    /*
    static testMethod void FamilyGroupRollupCtrl_Test2() 
    {
       	createTestData ();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(fg);
		Test.startTest() ;
		FamilyGroupRollupCtrl myCon = new FamilyGroupRollupCtrl(sc);
		myCon.init() ;
		System.assertEquals(10, myCon.AccountSearchResults.size() );
		
		myCon.RequestAccessId = myCon.AccountSearchResults[2].Account.id;
		myCon.RequestOwnership = true ;
		myCon.RequestMessage = 'Ownership reason';
		myCon.SendRequestEmail();
		
		Test.stopTest();
		
    }
    */
    
}