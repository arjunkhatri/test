public with sharing class ConnectionsRedirectCtrl 
{
	private Connections__c connectionsObj ;
	
	public ConnectionsRedirectCtrl (ApexPages.Standardcontroller sc)
	{
		connectionsObj  = (Connections__c)sc.getRecord() ;
	}
	
	public PageReference init()
	{
		PageReference pgref ;
		if (connectionsObj.RecordType.Name == 'C:C')
		{
			pgref = new PageReference('/' + connectionsObj.Contact_C2__c  + '/e?retURL=' + connectionsObj.Contact_C2__c) ;
			pgRef.setRedirect(true);
		}
		else if (connectionsObj.RecordType.Name == 'R:C')
		{
			pgref = new PageReference('/' + connectionsObj.Relationship_R1__c + + '/e?retURL=' + connectionsObj.Relationship_R1__c) ;
			pgRef.setRedirect(true);
		}
		else 
		{
			pgref = new PageReference('/' + connectionsObj.Relationship2__c + + '/e?retURL=' + connectionsObj.Relationship2__c) ;
			pgRef.setRedirect(true);
		}
		return pgRef;
	}
}