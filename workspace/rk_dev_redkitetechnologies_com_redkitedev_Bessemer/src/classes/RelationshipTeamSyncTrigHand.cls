/*
    
     Authors :  Don Koppel
     Created Date: 2014-09-09
     Last Modified: 2014-09-09
*/

public with sharing class RelationshipTeamSyncTrigHand {
    // template methods
    public void onBeforeInsert(list<Relationship_Team_Sync__c> newList) {
		upsertRelationshipTeamMember(newList);
    }
    public void onAfterInsert(list<Relationship_Team_Sync__c> newList) {
    	updateDataSyncLog(newList, 'I');
    }
    public void onBeforeUpdate(list<Relationship_Team_Sync__c> newList, map<id,Relationship_Team_Sync__c> oldMap) {
    }
    public void onAfterUpdate(list<Relationship_Team_Sync__c> newList, map<id,Relationship_Team_Sync__c> oldMap) {
    }
    public void onAfterDelete(list<Relationship_Team_Sync__c> oldList){
    }

// business logic

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs Account_Contact_Sync EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Relationship_Team_Sync__c> recordList, String operation) {
    	
    	//records should only be added to the data sync log if the current user is the Bessemer Integration user
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
    	
	    	list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
	 		for (Relationship_Team_Sync__c rts : recordList){
	 			Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
	 			syncRecord.EDB_Entity_Id__c = rts.EDB_Entity_Id__c;
	 			syncRecord.Entity__c = 'EMP_RELTNSHP_OPTN';
	 			syncRecord.Action__c = rts.Action__c;
	 			syncList.Add(syncRecord);
	 		}

			insert syncList;
    	}
    }

	public void upsertRelationshipTeamMember(list<Relationship_Team_Sync__c> rtsList){

		//This method determines whether Relationship Team data synced from EDB should be inserted/updated/delete
		//and performs the necessary operations.  This method also maintains the Senior CA field on the Relationship
		//object as per defined business rules.
		
		//TODO: add logic to maintain Senior CA flag
		Map<String,set<String>> relToDelAtm = new Map<String,set<String>>();
		Map<String,set<String>> relToNewAtm = new Map<String,set<String>>();
		List<String> rtsRelationshipIds = new List<String>();
		for (Relationship_Team_Sync__c rts : rtsList){
			rtsRelationshipIds.add(rts.AccountId__c);
		}
		
		List<AccountTeamMember> atmList = [SELECT Id, AccountId, UserId, TeamMemberRole
											FROM  AccountTeamMember
											WHERE AccountId IN :rtsRelationshipIds];
			
		Boolean bFound;	
		List<AccountTeamMember> atmInsert = new List<AccountTeamMember>();								
		List<AccountTeamMember> atmUpdate = new List<AccountTeamMember>();
		List<AccountTeamMember> atmDelete = new List<AccountTeamMember>();
										
		for (Relationship_Team_Sync__c rts : rtsList){
			bFound = false;
			for (AccountTeamMember atm : atmList){
				String teamMemUser = atm.UserId;
				String accId = atm.AccountId;
				if (teamMemUser.contains(String.valueof(rts.UserId__c)) && 
							accId.contains(String.valueof(rts.AccountId__c))){
					if (rts.Action__c == 'U' || rts.Action__c == 'I'){
						atm.TeamMemberRole = rts.Role__c;
						atmUpdate.add(atm);
						
					}
					else if (rts.Action__c == 'D'){
						atmDelete.add(atm);
						//fill map of Relationship vs deleted team member user
						if(teamMemUser.length() == 18)
							teamMemUser = teamMemUser.substring(0, 15);
						if(!relToDelAtm.containsKey(atm.AccountId)){
							relToDelAtm.put(atm.AccountId ,new set<String>{teamMemUser});	
						}else{
							relToDelAtm.get(atm.AccountId).add(teamMemUser);
						}						
					}

					bFound = true;
				}
			}
			if (!bFound){
				atmInsert.add(new AccountTeamMember(AccountId = rts.AccountId__c, UserId = rts.UserId__c, TeamMemberRole = rts.Role__c));
				String rtsUser = String.valueOf(rts.UserId__c);
				if(rtsUser.length() == 18)
					rtsUser = rtsUser.substring(0,15);
				//fill map of Relationship vs deleted team member user
				if(!relToNewAtm.containsKey(rts.AccountId__c)){
					relToNewAtm.put(rts.AccountId__c ,new set<String>{rtsUser});	
				}else{
					relToNewAtm.get(rts.AccountId__c).add(rtsUser);
				}				
			}
		}
		
		if (atmInsert.size() > 0){
			insert atmInsert;
			if(!relToNewAtm.isEmpty()){
				updateTaskOnInsertMember(relToNewAtm);
			}			
		}
		
		if (atmUpdate.size() > 0){
			update atmUpdate;
		}
		
		if (atmDelete.size() > 0){
			delete atmDelete;
			if(!relToDelAtm.isEmpty()){
				updateTaskOnDeleteMember(relToDelAtm);
			}
		}
		return;
	}       

	public void deleteRelationshipTeamMember(list<Relationship_Team_Sync__c> rtsList){
		List<AccountTeamMember> atmList = new List<AccountTeamMember>(); 		
		for(Relationship_Team_Sync__c rts :rtsList){
			AccountTeamMember atm = new AccountTeamMember(AccountId = rts.AccountId__c, UserId = rts.UserId__c);
			atmList.add(atm);
		}
		if (atmList.size() > 0){
			delete atmList;
		}
		return;
	}
	//function to update task Relationship_Team_List__c when team member is deleted
	public void updateTaskOnDeleteMember(Map<String,set<String>> mpRelToAtm){ 
		List<Task> listTask = new List<Task>();
		for(Task tsk :[Select WhatId, 
							Relationship_Team_List__c 
							From Task where WhatId =: mpRelToAtm.keyset()])
		{
			if(tsk.WhatId != null){
				String tskWhatid = string.valueOf(tsk.WhatId);
				set<String> listDelAtm = mpRelToAtm.get(string.valueof(tskWhatid));
				if(listDelAtm != null && listDelAtm.size() > 0){
					for(String atmUser:listDelAtm){
						if(atmUser.length() == 18)
							atmUser = atmUser.substring(0, 15);
						if(tsk.Relationship_Team_List__c != null && 
							tsk.Relationship_Team_List__c != ''   &&
						   	tsk.Relationship_Team_List__c.contains(atmUser)){
						   	tsk.Relationship_Team_List__c = tsk.Relationship_Team_List__c.replace('#'+atmUser+'#','');
						}
					}
				}
				listTask.add(tsk);
			}
		}
		if(listTask != null && listTask.size() > 0){
			update listTask;
		}
	}
	
	//function to update task Relationship_Team_List__c when team member is insert
	public void updateTaskOnInsertMember(Map<String,set<String>> mpRelToNewAtm){
		List<Task> listTaskUpdate = new List<Task>();
		for(Task tsk :[Select WhatId, 
							Relationship_Team_List__c 
							From Task where WhatId =: mpRelToNewAtm.keyset()])
		{
			if(tsk.WhatId != null){
				String tskWhtId = string.valueOf(tsk.WhatId);
				if(!Test.isRunningTest()){
					if(tskWhtId.length() == 18)
						tskWhtId = tskWhtId.substring(0, 15);	
				}
				set<String> listNewAtm = mpRelToNewAtm.get(tskWhtId);
				if(listNewAtm != null && listNewAtm.size() > 0){
					for(String atmUser:listNewAtm){
						if(atmUser.length() == 18)
							atmUser = atmUser.substring(0, 15);
						if(tsk.Relationship_Team_List__c != null && 
						   tsk.Relationship_Team_List__c != '' ){
						   	if(!tsk.Relationship_Team_List__c.contains(atmUser)){
						    	tsk.Relationship_Team_List__c = tsk.Relationship_Team_List__c + '#'+atmUser+'#';
						   	}
						}
						else{
							tsk.Relationship_Team_List__c = '#'+atmUser+'#';
						}
					}
				}
				listTaskUpdate.add(tsk);
			}
		}
		if(listTaskUpdate != null && listTaskUpdate.size() > 0){
			update listTaskUpdate;
		}	
	}
}