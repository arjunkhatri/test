public with sharing class AddresseditCloneCtrl 
{
	public String addressId { get; set; }
	public Addresses__c addressObj { get; set; }
	public String selectedId { get; set; }
	public Integer relatedAddressesCount { get; set; }
	public String saveAndCloseBtn { get; set; }
	public String pageType { get; set; }
	public Map<String, AddressWrapper> relatedAddresses { get; set; }
	public List<SortClass> relatedAddressList { get; set; }
	public String columnsToDisplay { get; set; }

	private String accountId;
	private String selectedRecordId;
	private Boolean hasErrors = false;
	private String platForm;

	public AddresseditCloneCtrl( ApexPages.Standardcontroller sc )
	{
		addressId = sc.getId();
		GlobalContactSearch searchObj = new GlobalContactSearch();
		platform = searchObj.checkPlatform();
		if( platForm == 'Phone' )
			columnsToDisplay = '1';
		else
			columnsToDisplay = '2';
	}

	public AddresseditCloneCtrl()
	{
		selectedId = ApexPages.currentPage().getParameters().get('id');
	}

	public PageReference init()
	{
		relatedAddressesCount = 0;
		string retUrl = ApexPages.currentPage().getParameters().get('retURL');
		System.Debug('***Param: ' + ApexPages.currentPage().getParameters() );
		string urlPrefix = '' ;
		if (ApexPages.currentPage().getParameters().get('LeadId') != null) 
			urlPrefix = '00Q';
		else
		{
			if ( ApexPages.currentPage().getParameters().get('ContactId') != null )
			{
				urlPrefix = ApexPages.currentPage().getParameters().get('ContactId').substring(0, 3) ;
			}
			else
			{
				urlPrefix = '';
				try {
					if (retURL.IndexOf('.com')> 0 && retURL.split('.com').size() == 2)
						urlPrefix = retURL.split('.com')[1].substring( 1, 4 ) ;
					else
						urlPrefix = retURL.substring(1, 4 ) ;
				}
				catch (Exception ex) 
				{
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'Error accessing page. Please contact your System Administrator' ) );
					return null;
				}
			}
		}

		if( urlPrefix == '00Q' )
			pageType = 'Lead';
		else
			pageType = 'Contact';

/*
		if( retUrl != null )
		{
			String recordPrefix = '';
			if( platform == 'Phone' )
				recordPrefix = retURL.split('.com')[1];
			else
				recordPrefix = retURL;

			if( recordPrefix.subString( 0, 4 ) == '/00Q' )
				pageType = 'Lead';
			else
				pageType = 'Contact';
		}
*/

		system.debug( 'pagetype : ' + pageType );
		if( addressId != null )
		{
			addressObj = [ SELECT Address_Type__c, Custom_Addressee__c, Lead__c, Contact__c, Contact__r.Name, Street_Address__c, 
			Country__c,   City__c, State__c, Postal_Code__c, Primary_Address__c, Duration_Start__c, Duration_End__c, Address_Label_Block__c, Additional_Address_Line__c , 
								  Legal_Address__c
						   FROM Addresses__c
						   WHERE Id = :addressId ];

			if( pageType == 'Contact' )
			{
				Contact contactObj = [ SELECT Id, AccountId FROM Contact WHERE Id IN ( SELECT Contact__c FROM Addresses__c WHERE Id = :addressId ) ];
				accountId = contactObj.AccountId;
				selectedRecordId = contactObj.Id;
				getRelatedAddresses();
			}
			else if( pageType  == 'Lead' )
			{
				Lead leadObj = [ SELECT Id FROM Lead WHERE Id IN ( SELECT Lead__c FROM Addresses__c WHERE Id = :addressId ) ];
				selectedRecordId = leadObj.Id;
			}
			saveAndCloseBtn = 'Save & More';
		}
		else
		{ 
			newAddressRecord();
		}

		return null;
	}

	public void getRelatedAddresses()
	{
		if( accountId == null )
			return;

		Map<String, AddressWrapper> returnMap = new Map<String, AddressWrapper>();

		List<Addresses__c> addressList = [ SELECT Address_Type__c, Contact__c ,Contact__r.Name, Street_Address__c, 

		Country__c,			City__c, State__c, Postal_Code__c, Primary_Address__c, Legal_Address__c, Custom_Addressee__c
								   FROM Addresses__c
										   WHERE Contact__c != :selectedRecordId
												AND Contact__c IN ( SELECT Id FROM Contact WHERE AccountId = :accountId )
										   ORDER BY Contact__c ];

		Set<String> addressSet = new Set<String>();
		for( Addresses__c a : addressList )
			addressSet.add( a.Contact__c );

		List<AddressObject> tmp = new List<AddressObject>();
		
		String currentId;
		String currentName;
		if( !addressList.isEmpty() )
		{
			currentId = addressList[0].Contact__c;
			currentName =  addressList[0].Contact__r.Name;
		}
		String rtId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND Name = 'R:C' ].Id;
		String sqlWhere = ' RecordTypeId = \'' + rtId + '\' and Relationship_R1__c = \'' + accountId + '\'' ;
		List<Connections__c> connectionsList = Database.query( 'SELECT Relationship_R1__c, Contact_C1__c, Role_On_Relationship__c FROM Connections__c WHERE ' + sqlWhere );

		Map<String, String> connectionMap = new Map<String, String>();
		for( Connections__c c : connectionsList )
			connectionMap.put( c.Contact_C1__c , c.Role_On_Relationship__c );

		for( Addresses__c a : addressList )
		{
			if( currentId != a.Contact__c )
			{
				AddressWrapper w = new AddressWrapper( currentName, connectionMap.get( currentId ), currentId, tmp );
				returnMap.put( currentId, w );
				tmp = new List<AddressObject>();
				currentId = a.Contact__c;
				currentName = a.Contact__r.Name;
			}
			tmp.add( new AddressObject( false, a ) );
		}

		if( !tmp.isEmpty() )
		{
			AddressWrapper w = new AddressWrapper( currentName, connectionMap.get( currentId ), currentId, tmp );
			returnMap.put( currentId, w );
		}

		//Add Contacts without addresses
		Map<String, Contact> contactMap = new Map<String,Contact>( [ SELECT Id, Name FROM Contact WHERE Id != :selectedRecordId AND AccountId = :accountId ] );
		for( String key : contactMap.keyset() )
		{
			List<AddressObject> tmpAddressList = new List<AddressObject> ();
			if( !addressSet.contains( key ) )
			{
				Addresses__c newAddress = new Addresses__c();
				newAddress.Contact__c = key; 
				tmpAddressList.add( new AddressObject( false, newAddress ) );
				AddressWrapper w = new AddressWrapper( contactMap.get( key ).name, connectionMap.get( key ), key, tmpAddressList );
				returnMap.put(key, w);
			}
		}

		//Sort the Map
		List<SortClass> scList = new  List<SortClass>();
		for( String key : returnMap.keyset() )
		{
			scList.add( new SortClass( returnMap.get( key ).contactId, returnMap.get( key ).contactName ) );
		}
		scList.sort();

		Map<String,AddressWrapper> returnMapFinal = new Map<String,AddressWrapper>();
		relatedAddressList = scList;

		relatedAddressesCount = returnMap.size();
		relatedAddresses =  returnMap;
	}

	public PageReference cancel()
	{
		PageReference pgRef = new PageReference('/' + this.selectedRecordId);
		pgRef.setRedirect(true);
		return pgRef;
	}

	/*
	public PageReference QuickSave ()
	{
		List<Addresses__c> addressList = new List<Addresses__c>();
		for (String key : RelatedAddresses.keyset() )
		{
			List<AddressObject> address = RelatedAddresses.get(key).addressList;
			for (AddressObject obj : address)
				addressList.add(obj.address);	
		}
		
		//Check to make sure only 1 primary Address is assigned 
		set<String> contactsetCount = new set<String>();
		for (Addresses__c address: addressList)
			contactsetCount.add(address.Contact__c);
			
		set<String> contactSet = new set<String>();
		
		for (Addresses__c address: addressList)
		{
			if (address.Primary_Address__c && contactSet.contains(address.Contact__c) )
			{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,'Addresses NOT updated.  Only 1 primary address can be SELECTed.')) ;
				return null;
			}
			else
			{
				if (address.Primary_Address__c)
					contactset.add(address.Contact__c);
			}
		}
		
		contactset =  new set<String>();
		for (Addresses__c address: addressList)
		{
			if (address.Primary_Address__c && !contactset.contains(address.Contact__c) )
				contactset.add(address.Contact__c); 
		}
		if (contactsetCount.size() != contactSet.size())
		{
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,'Addresses NOT updated.  A primary address must be SELECTed for each contact.')) ;
			return null;
		}
		
		Database.SaveResult[] srList =  Database.update(addressList);
		boolean hasErrors = false;
		for (Database.SaveResult sr : srList) 
		{
			if (!sr.isSuccess() )
			{	
				hasErrors = true;
				for (Database.Error err : sr.getErrors() )
				{
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info, String.ValueOf(err.getMessage()) )) ;
				}
			}
		}
		if (hasErrors == false)
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,'Addresses updated successfully')) ;
		
		return null;
	}
	*/
	public PageReference saveAndMore()
	{
		if( !saveRecords() )
			return null;

		ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Info, 'Addresses updated successfully' ) );
		if( saveAndCloseBtn == 'Save & New' )
			newAddressRecord();

		return null;
	}

	public PageReference saveAndClose()
	{
		saveRecords();
		List<Apexpages.Message> msgs = ApexPages.getMessages();

		if( hasErrors == true || !msgs.isEmpty() )
			return null;
		else
		{
			PageReference pgRef = new PageReference( '/' + this.selectedRecordId );
			pgRef.setRedirect( true );
			return pgRef;
		}
	}

	private void newAddressRecord()
	{
		addressObj = new Addresses__c();
		if( pageType == 'Contact' )
		{
			String contactId = ApexPages.currentPage().getParameters().get( 'contactId' );
			addressObj.Contact__c = contactId;
			selectedRecordId  = contactId;
			accountId = [ SELECT AccountId FROM Contact WHERE Id = :contactId ].AccountId;
			getRelatedAddresses();
		}
		else if( pageType == 'Lead' )
		{
			String leadId = '';
			if (ApexPages.currentPage().getParameters().get('LeadId') != null) 
				leadId = ApexPages.currentPage().getParameters().get('LeadId') ;
			else
				leadId = ApexPages.currentPage().getParameters().get('retURL').split('.com')[1].subString( 1, 16 );
			addressObj.Lead__c = leadId;
			selectedRecordId  = leadId;
		}

		saveAndCloseBtn = 'Save & New';
	}

	private Boolean saveRecords()
	{
		//Turn Off DupeBlocker Check
		CRMfusionDBR101.DB_Api.preventBlocking();
		hasErrors = false;

		List<Addresses__c> addressList ;
		if( pageType == 'Lead' )
			addressList = [ SELECT Id FROM Addresses__c WHERE Lead__c = :selectedRecordId AND Address_Type__c = :addressObj.Address_Type__c ];
		else
			addressList = [ SELECT Id FROM Addresses__c WHERE Contact__c = :selectedRecordId AND Address_Type__c = :addressObj.Address_Type__c ];

		if( !addressList.isEmpty() )
		{
			for( Addresses__c ad : addressList )
			{
				if( ad.Id != addressObj.Id )
				{
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'Address was NOT updated/created successfully.  The ' + pageType + ' already has a address with this address type' ) );
					hasErrors = true;
				}
			}
		}

		if( relatedAddresses != null && pageType == 'Contact' && validAddresses() )
			hasErrors = true;

		if( hasErrors == true )
			return false;
		try
		{
			upsert addressObj;
		}
		catch( Exception ex ) { return false;}

		this.addressId = addressObj.Id;

		if( pageType == 'Contact' && this.addressId != null && relatedAddresses != null )
			copyAddresses();

		return true;
	}
	public Boolean validAddresses()
	{
		Boolean retVal = false;

		Set<String> addressSet = new Set<String>();
		for( String key : relatedAddresses.keySet() )
		{
			addressSet.add( relatedAddresses.get( key ).contactId );
		}

		List<Addresses__c> addressList = [ SELECT Id, Contact__c FROM Addresses__c WHERE Contact__c IN :addressSet AND Address_Type__c = :addressObj.Address_Type__c ];
		if( !addressList.isEmpty() )
		{
			for( Addresses__c add : addressList )
			{
				AddressWrapper addressObj = relatedAddresses.get( add.Contact__c );
				if( addressObj.newAddress )
				{
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'Addresses were NOT updated/created successfully.  The Contact (' + relatedAddresses.get( add.Contact__c ).contactName  + ') already has an address with this address type' ) );
					retVal = true;
				}

				for( AddressObject addObj : addressObj.addressList )
				{
					if( addObj.copyAddress )
					{
						if( addObj.address.Id == add.Id )
						{
							ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'Addresses were NOT updated/created successfully.  The Contact (' + relatedAddresses.get( add.Contact__c ).contactName  + ') already has an address with this address type' ) );
							retVal = true;
						}
					}
				}
			}
		}

		return retVal;
	}

	public void copyAddresses()
	{
		List<Addresses__c> updateList = new List<Addresses__c>();
		List<Addresses__c> newList = new List<Addresses__c>();
		for( String key : relatedAddresses.keySet() )
		{
			AddressWrapper addressObj = relatedAddresses.get( key );
			for( AddressObject add :addressObj.addressList )
			{
				if( add.copyAddress )
				{
					Addresses__c updateAddress = add.address;
      
					updateAddress.Street_Address__c = this.addressObj.Street_Address__c;

					//updateAddress.Street_Address_Line_2__c = this.addressObj.Street_Address_Line_2__c;
					updateAddress.City__c = this.addressObj.City__c;
					updateAddress.State__c = this.addressObj.State__c;
					updateAddress.Postal_Code__c = this.addressObj.Postal_Code__c;
					//updateAddress.Address_Type__c = this.addressObj.Address_Type__c;
					updateAddress.Country__c = this.addressObj.Country__c;
					if( addressObj.addressList.size() == 1 )
						updateAddress.Primary_Address__c = true;

					updateAddress.Custom_Addressee__c = this.addressObj.Custom_Addressee__c  ;
					updateList.add( updateAddress );
				}
			}
			if( addressObj.newAddress )
			{
				Addresses__c newAddress = new Addresses__c() ;
   
			  	newAddress.Street_Address__c = this.addressObj.Street_Address__c;

				//newAddress.Street_Address_Line_2__c = this.addressObj.Street_Address_Line_2__c;
				newAddress.City__c = this.addressObj.City__c;
				newAddress.State__c = this.addressObj.State__c;
				newAddress.Postal_Code__c = this.addressObj.Postal_Code__c;
				newAddress.Address_Type__c = this.addressObj.Address_Type__c;
				newAddress.Country__c = this.addressObj.Country__c;
				newAddress.Custom_Addressee__c = this.addressObj.Custom_Addressee__c;
				//newAddress.Primary_Address__c = this.addressObj.Primary_Address__c;
				newAddress.Contact__c = key;
				newList.add( newAddress );
			}
		}
		if( !updateList.isEmpty() )
			update updateList;
		if( !newList.isEmpty() )
			insert newList;

		getRelatedAddresses();
	}

	public class SortClass implements Comparable
	{
		public String contactId { get; set; }
		public String contactName { get; set; }
		public Boolean disableNew { get; set; }
		
		public SortClass( String cId, String cName )
		{
			contactId = cId;
			contactName = cName;
		}

		public Integer compareTo( Object compareTo ) 
		{
			SortClass sc = (SortClass) compareTo;
			if( contactName == sc.contactName ) return 0;
			if( contactName > sc.contactName ) return 1;
			return -1;
		}
	}

	public class AddressWrapper 
	{
		public Decimal rank { get; set; }
		public String contactName { get; set; }
		public String contactId { get; set; }
		public Boolean newAddress { get; set; }  
		public Boolean disableNew { get; set; }
		public String contactRole { get; set; }
		public List<AddressObject> addressList { get; set; }
		
		public AddressWrapper( String ctName, String role, String ctId, List<AddressObject> addresses )
		{
			rank = 1;
			contactName = ctName;
			contactRole = role;
			contactId = ctId;
			addressList = addresses;
			newAddress = false;
			
			if( addresses.size() >= Integer.valueOf( AddressCustomzationConfig__c.getOrgDefaults().TotalAddressRecords__c ) )
				disableNew = true;
			else
				disableNew = false;
		}
	}

	public class AddressObject
	{
		public Boolean copyAddress { get; set; }  
		public Addresses__c address { get; set; }

		public AddressObject( Boolean copy, Addresses__c add )
		{
			copyAddress = copy;
			address = add;
		}
	}
}