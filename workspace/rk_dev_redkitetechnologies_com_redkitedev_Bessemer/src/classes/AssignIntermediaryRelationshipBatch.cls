global class AssignIntermediaryRelationshipBatch implements Database.Batchable<Contact>{
	public static final String INTERMEDIARY = 'Professional/Intermediary';
	public static final String RELRECTYPE = 'Professional/Intermediary';
	public static final String CONRECTYPE = 'R:C';
	public static final String STATUS = 'Open';
	public List<Connections__c> lstConnections; 
	public List<Contact> lstContacts;
	public String relRec; 
	public String conRec;

	public AssignIntermediaryRelationshipBatch(){
		lstContacts = [Select Id,Name,AccountId,Company_Name__c 
						From Contact
						where RecordType.Name = :INTERMEDIARY 
						and AccountId = null
						LIMIT 50000];	
	}
	
    global Iterable<Contact> start(Database.BatchableContext BC){
    	return lstContacts;
    }	
    
    global void execute(Database.BatchableContext BC, List<Contact> scope){	
		lstConnections = new List<Connections__c>();
		List<Contact> updateContacts = new List<Contact>();
		Map<String,Account> mpConToNewAcc = new Map<String,Account>();
		Map<String,List<Account>> bNameToAcc = new Map<String,List<Account>>();
		Map<String,String> connectionsExisting = new Map<String, String>();

		//Populate existing connections that have a professional role
		for(Connections__c con : [SELECT Contact_C1__c, Role_on_Relationship__c
									FROM Connections__c
									WHERE Contact_C1__c in :scope
									AND Role_on_Relationship__c IN ('Accountant', 'Lawyer', 'Insurer', 'Financial Planner')])
		{
			if(!connectionsExisting.containsKey(con.Contact_C1__c))
				connectionsExisting.put(con.Contact_C1__c, con.Role_on_Relationship__c);	
		}


		//get required record type ids
		for(RecordType rt:[Select Name, Id , SobjectType From RecordType where 
							(Name = :RELRECTYPE and SobjectType = 'Account') or
							(Name = :CONRECTYPE and SobjectType = 'Connections__c')])
		{
			if(rt.Name == RELRECTYPE && rt.SobjectType == 'Account'){	
				relRec = String.valueOf(rt.id);
			}
			if(rt.Name == CONRECTYPE && rt.SobjectType == 'Connections__c'){
				conRec  = String.valueOf(rt.id);
			}
		}
		for(Account objAcc:[Select id,Name from Account where RecordTypeId=:relRec order by CreatedDate DESC])
		{
			if(!bNameToAcc.containsKey(objAcc.Name.toLowerCase())){
				bNameToAcc.put(objAcc.Name.toLowerCase() ,new List<Account>{objAcc} );	
			}
			else{
				bNameToAcc.get(objAcc.Name.toLowerCase()).add(objAcc);
			}
		}
		for(Contact con:scope){
			String role;
			if(connectionsExisting.containsKey(con.Id))
				role = connectionsExisting.get(con.Id);
			else 
				role = 'Other';	
			 
			if(String.isNotEmpty(con.Company_Name__c)){
				List<Account> lstAcc = bNameToAcc.get(con.Company_Name__c.toLowerCase());
				if(lstAcc != null && lstAcc.size() > 0){
					con.AccountId = lstAcc[0].id;
					updateContacts.add(con);
					//create connection
					createConnection(con.id,lstAcc[0].id, role);
				}
				else{ 
					//create new account for Contact
					Account acc = new Account();
					acc.Name = con.Company_Name__c;
					acc.Active_Inactive__c = true;
					acc.Status__c = STATUS;
					if (acc.Name.length() > 50)
						acc.Name = acc.Name.substring(0,50);
					acc.RecordTypeId = relRec;
					mpConToNewAcc.put(con.Company_Name__c.toLowerCase(),acc);
				}
			}
			else{ 
				//create new account for Contact
				Account acc = new Account();
				acc.Name = con.Name;
				acc.Active_Inactive__c = true;
				acc.Status__c = STATUS;
				if (acc.Name.length() > 50)
					acc.Name = acc.Name.substring(0,50);
				acc.RecordTypeId = relRec;
				mpConToNewAcc.put(con.Name.toLowerCase(),acc);
			}
		}
		//insert new accounts
		if(!mpConToNewAcc.isEmpty()){
			insert mpConToNewAcc.values();
		}
		//fill map with business name vs new created accounts
		for(Contact con:scope){
			String role;
			if(connectionsExisting.containsKey(con.Id))
				role = connectionsExisting.get(con.Id);
			else 
				role = 'Other';	

			if(String.isNotEmpty(con.Company_Name__c) && mpConToNewAcc.containsKey(con.Company_Name__c.toLowerCase())){
				con.AccountId = mpConToNewAcc.get(con.Company_Name__c.toLowerCase()).id;
				updateContacts.add(con);
				createConnection(con.id,con.AccountId, role);
			}		
			else if (mpConToNewAcc.containsKey(con.Name.toLowerCase())){
				con.AccountId = mpConToNewAcc.get(con.Name.toLowerCase()).id;
				updateContacts.add(con);
				createConnection(con.id,con.AccountId, role);
			}		
		}
		//insert new connections
		if(lstConnections != null && lstConnections.size() > 0){
			insert lstConnections;
		}
		//update contacts
		if(updateContacts != null && updateContacts.size() > 0){
			update updateContacts;
		}
	}
	global void finish(Database.BatchableContext BC){
	   	
	} 	
	public void createConnection(String conId,String accId, String role){
		Connections__c connection = new Connections__c();
		connection.Relationship_R1__c = accId;
		connection.Contact_C1__c = conId;
		connection.RecordTypeId = conRec;
		connection.Role_on_Relationship__c = role;

		lstConnections.add(connection);	
	}
}