public without sharing class ContactPageExt extends PageControllerBase
{
	public Contact contactRec { get; set; }
	public String columnsToDisplay { get; set; }
	private Contact originalRecord;
	public Map<String, List<LayoutFields>> contactFields { get; set; }
	public List<String> contactSection { get; set; }
	public Boolean showRecord { get;set; }
	public Boolean canEditRecordType {get;set;}
	public string recordTypeOptions {get;set;} 
	public Addresses__c AddressObj {get;set;}

	
	//** Dupeblocker set up ************/
	public DupeBlockerListCtrl myComponentController { get; set; }
	public override void setComponentController( ComponentControllerBase compController )
	{
		myComponentController = (DupeBlockerListCtrl)compController;
	}
	public override ComponentControllerBase getMyComponentController()
	{
		return myComponentController;
	}
	transient Boolean isOverride;
	String contactId;

	/**************  End DupBlocker Setup **************/
	public ContactPageExt( ApexPages.StandardController stdController )
	{
		contactId = stdController.getId();

	}

	public void init()
	{
		showRecord  = true;
		UserRecordAccess ura = [ SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :contactId ]; 
		if( !ura.HasEditAccess )
		{
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'The current user does not have edit access to this record.  Please contact the record owner.' ) );
			showRecord  = false;
			return;
		}
 		
 		//Get the available recordtypes that can be changed.
 		List<ContactEditLayouts__c> cel = ContactEditLayouts__c.getall().values(); 
 		for (ContactEditLayouts__c c : cel)
 		{
 			if (c.CanChange__c)
 			{
 				if (recordTypeOptions == null)
 					recordTypeOptions = c.Name;
 				else
 					recordTypeOptions += ',' + c.Name;
 			}
 		}

 		//recordTypeOptions = 'Other,Prospect,Professional/Intermediary' ;
 		
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();
		
		if( platForm == 'Phone' )
			columnsToDisplay = '1';
		else
			columnsToDisplay = '2';

		AddressObj = new Addresses__c();
		if( ApexPages.currentPage().getParameters().get('id') ==  null )
		{
			this.contactRec = new Contact();
			string rType = ApexPages.currentPage().getParameters().get( 'RecordType' );
			this.contactRec.RecordTypeId = rType;
			
		}
		else
		{
			this.contactRec = Database.query( 'SELECT ' + Utility_Common.getFieldList( 'Contact' ) + ' FROM Contact WHERE Id = \'' + contactId + '\'' );
			AddressObj.State__c = this.contactRec.MailingState;
			if (this.contactRec.MailingCountry == null)
				AddressObj.Country__c = '';
			else
				AddressObj.Country__c = this.contactRec.MailingCountry;

			/*==========  Get the Address Object Primary Address  ==========*/
			List<Addresses__c> addList  = [SELECT Contact__c, Address_Type__c , Additional_Address_Line__c FROM Addresses__c where Primary_Address__c = true and Contact__c = :contactRec.id];
			if (!addList.isEmpty() )
			{
				AddressObj.Address_Type__c = addList[0].Address_Type__c ;
				AddressObj.Additional_Address_Line__c = addList[0].Additional_Address_Line__c ;
			}

			canEditRecordType = hasRecordTypeAccess (contactRec.RecordTypeId);
			if (canEditRecordType)
			{
				string rType = ApexPages.currentPage().getParameters().get( 'RecordType' );
				
				if( rType != null && contactRec.RecordTypeId != rType)
				{
					map<string, recordType> recTypeMap = new map<string, recordType> () ;
					List<RecordType> rtList =  [select id, Name from Recordtype where sObjectType = 'Contact'] ;
					for (RecordType r: rtList)
						recTypeMap.put(String.ValueOf(r.id).substring(0,15) , r) ;

					set<string> rtOptions = new set<string>() ;
					for (string str : recordTypeOptions.split(',') )
						rtOptions.add(str);

				
					if (rtOptions.Contains(recTypeMap.get(rType).Name ) && rtOptions.Contains(recTypeMap.get(String.ValueOf(contactRec.RecordTypeId).substring(0,15)).Name  ) )
						contactRec.RecordTypeId = rType;
					else
					{
						ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'The Contact type can not be changed from ' + recTypeMap.get(String.ValueOf(contactRec.RecordTypeId).substring(0,15)).Name  + ' to ' + recTypeMap.get(rType).Name + '.'  ));
						showRecord = false;
						return;
					}
				}
			}
		}
		originalRecord = contactRec.clone();
		processContactSection();
		processContactFields();
	}



	public PageReference saveRecord()
	{
		contactRec.MailingState = AddressObj.State__c ;
		contactRec.MailingCountry = AddressObj.Country__c;

		//Reset the Dupblocker Bypass Field if the field values have changed.
		if( contactRec.Id != null )
		{
			//Check make sure the Recordtype did not change.

			List<CRMfusionDBR101__Scenario__c> dupblockerScenario = [ SELECT Name, CRMfusionDBR101__Scenario_Type__c, CRMfusionDBR101__Allow_Block_Bypass__c, CRMfusionDBR101__Deployed__c,
																		( SELECT CRMfusionDBR101__Field_Name__c FROM CRMfusionDBR101__Scenario_Rules__r )
																	  FROM CRMfusionDBR101__Scenario__c
																	  WHERE CRMfusionDBR101__Allow_Block_Bypass__c = true
																			AND CRMfusionDBR101__Deployed__c = true
																			AND CRMfusionDBR101__Scenario_Type__c = 'Contact' ];
			Set<String> fieldSet = new Set<String>();
			for( CRMfusionDBR101__Scenario__c scenario : dupblockerScenario )
			{
				List<CRMfusionDBR101__Scenario_Rule__c> fieldList = scenario.getSObjects( 'CRMfusionDBR101__Scenario_Rules__r' );
				for( CRMfusionDBR101__Scenario_Rule__c field : fieldList )
				{
					if( !fieldSet.contains( field.CRMfusionDBR101__Field_Name__c ) )
						fieldSet.add( field.CRMfusionDBR101__Field_Name__c );
				}
			}

			for( String key : fieldSet )
			{
				if( String.valueOf( originalRecord.get( key ) ) != String.valueOf( contactRec.get( key ) ) )
				{
					contactRec.Dupe_Blocker_Bypass__c = false;
					break;
				}
			}
		}

		Boolean hasDuplicates = false;
		myComponentController.hasDuplicates = false;
		hasDuplicates = MyComponentController.checkContactDups( contactRec );

		if( hasDuplicates )
			return null;

		MyComponentController.overRideDupeBlocker() ;
		System.Debug ('***REC: ' + contactRec) ;
		List<Contact> contactList = new List<Contact>();
		contactList.add( contactRec );
		//Add the Address Record
		if (contactRec.Id != null)
		{
			List<Addresses__c> addList  = [SELECT Contact__c, Contact__r.Name, Street_Address__c, 
					Country__c,   City__c, State__c, Postal_Code__c, Primary_Address__c FROM Addresses__c where Primary_Address__c = true and Contact__c = :contactRec.id];
			if (! addList.isEmpty() )
			{
				Addresses__c add = addList[0];
						
				//add.Primary_Address__c = true ;
				add.Address_Type__c = AddressObj.Address_Type__c ;
				add.Street_Address__c = contactRec.MailingStreet;
				add.City__c = contactRec.MailingCity;
				add.State__c = ContactRec.MailingState;
				add.Postal_Code__c = ContactRec.MailingPostalCode;
				add.Country__c = ContactRec.MailingCountry;
				add.Address_Type__c = AddressObj.Address_Type__c ;
				add.Additional_Address_Line__c = AddressObj.Additional_Address_Line__c ;
				update add;
			}
			else
			{
				Addresses__c add = new Addresses__c ();
				add.Address_Type__c = AddressObj.Address_Type__c ;
				add.Street_Address__c = contactRec.MailingStreet;
				add.City__c = contactRec.MailingCity;
				add.State__c = ContactRec.MailingState;
				add.Postal_Code__c = ContactRec.MailingPostalCode;
				add.Country__c = ContactRec.MailingCountry;
				add.Address_Type__c = AddressObj.Address_Type__c ;
				add.Additional_Address_Line__c = AddressObj.Additional_Address_Line__c ;
				insert add;
	}
		}
		
				
		Database.UpsertResult[] srList = Database.upsert( contactList, false );
		// Iterate through each returned result
		for( Database.UpsertResult sr : srList )
		{
			if( sr.isSuccess() )
			{
				
				//Operation was successful, so get the ID of the record that was processed
				PageReference returnURL;
				returnURL = new PageReference( '/' + contactRec.Id );
				return returnURL;
			}
			else
			{
				// Operation failed, so get all errors  
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'There were error(s) on the page' ) );
					//System.debug('The following error has occurred.');                                  
				for( Database.Error err : sr.getErrors() )
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR,'Error: ' + err.getMessage() ) );
			}
		}

		return null;
	}
	public PageReference saveRecordOverride()
	{
		//if (isOverride )
		//{
		myComponentController.overRideDupeBlocker();
		ContactRec.Dupe_Blocker_Bypass__c = true;
		upsert contactRec;
		//}

		PageReference returnURL;
		returnURL = new PageReference( '/' + contactRec.Id );
		return returnURL;
	}
	
	public void processContactSection()
	{
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();

		List<String> contactDupFields = new List<String>(); 
		//List<AggregateResult> results = [ SELECT SectionName__c, SectionOrder__c FROM ContactFieldList__c WHERE Platform__c = :platform Group BY SectionName__c, SectionOrder__c Order By sum()   ]  ;

		String rtName = [ SELECT Name FROM RecordType WHERE Id = :contactRec.RecordTypeId and sObjectType='Contact' ].Name;
		String pageName = '';
		pageName = ContactEditLayouts__c.getInstance(rtName).PageName__c ;

		/*
		if( rtName == 'Other' || rtName == 'Client Contact' ||  rtName == 'Former Client' )
			pageName = 'Client Contact';
		else if( rtName == 'Prospect' )
			pageName = 'Contact Prospect';
		else if( rtName == 'Professional/Intermediary' )
			pageName = 'Contact Non Client';
		else if( rtName == 'Entity Contact' )
			pageName = 'Entity Contact';
		*/

		for( AggregateResult ar : [ SELECT SectionName__c FROM ContactFieldList__c WHERE PageName__c = :pageName AND Platform__c = :platform GROUP BY SectionName__c ORDER BY MAX( SectionOrder__c ) ] )
		{
			contactDupFields.add( String.valueOf( ar.get( 'SectionName__c' ) ) );
		}

		contactSection = contactDupFields;
	}
	
	public void  processContactFields()
	{
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();

		List<String> contactDupFields = new List<String>();
		Map<String, List<LayoutFields>> returnMap = new Map<String, List<LayoutFields>> ();

		String rtName = [ SELECT Name FROM RecordType WHERE Id = :contactRec.RecordTypeId ].Name;
		String pageName = ContactEditLayouts__c.getValues(rtName).PageName__c ;
		
		SObjectType objToken = Schema.getGlobalDescribe().get( 'Contact' );
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap(); 
		
/*
		Set<String> fieldSet = fields.keySet();
		List<String> tmpList = new List<String>();
		for( String s : fieldSet )
		{
			SObjectField fieldToken = fields.get( s );
			DescribeFieldResult selectedField = fieldToken.getDescribe();
			if( selectedField.isUpdateable()  )
		*/
		for( ContactFieldList__c cldf : [ SELECT SectionName__c, Field__c, Required__c, StyleClass__c FROM ContactFieldList__c WHERE Platform__c = :platform AND PageName__c = :pageName ORDER BY SortOrder__c ] )
		{
			if( cldf.Field__c != 'BlankSectionItem' && cldf.Field__c != 'AddressType' && cldf.Field__c != 'AddressAdditionalAddressLine')
			{
				SObjectField fieldToken = fields.get( cldf.Field__c );
				DescribeFieldResult selectedField = fieldToken.getDescribe();
				if( selectedField.isUpdateable() )
				{
					if( returnMap.containsKey( cldf.SectionName__c ) )
					{
						List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c ) ;
						tmpstringList.add( new LayoutFields( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpstringList );
					}
					else
					{
						List<LayoutFields> tmpStringList = new List<LayoutFields>();
						tmpstringList.add(new LayoutFields ( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpstringList );
					}
				}
				else
				{
					if( returnMap.containsKey( cldf.SectionName__c ) )
					{
						List<LayoutFields> tmpStringList = returnMap.get(cldf.SectionName__c ) ;
						tmpstringList.add( new LayoutFields( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpstringList );
					}
					else
					{
						List<LayoutFields> tmpStringList = new List<LayoutFields>();
						tmpstringList.add( new LayoutFields ( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpstringList );
					}
				}
			}
			else if (cldf.Field__c == 'AddressType' || cldf.Field__c == 'AddressAdditionalAddressLine')
			{
				if( returnMap.containsKey( cldf.SectionName__c ) )
				{
					List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c ) ;
					tmpstringList.add(new LayoutFields ( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c )  );
					returnMap.put( cldf.SectionName__c, tmpstringList );
				}
				else
				{
					List<LayoutFields> tmpStringList = new List<LayoutFields>();
					tmpstringList.add( new LayoutFields( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c )  );
					returnMap.put( cldf.SectionName__c, tmpstringList );
				}
			}
			else 
			{
				if( returnMap.containsKey( cldf.SectionName__c ) )
				{
					List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c ) ;
					tmpstringList.add(new LayoutFields ( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c )  );
					returnMap.put( cldf.SectionName__c, tmpstringList );
				}
				else
				{
					List<LayoutFields> tmpStringList = new List<LayoutFields>();
					tmpstringList.add( new LayoutFields( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c )  );
					returnMap.put( cldf.SectionName__c, tmpstringList );
				}
			}
		}

		contactFields = returnMap;
	}
	
	private boolean hasRecordTypeAccess (string recordTypeId)
	{
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

		List<RecordTypeInfo> rtInfos = gd.get('Contact').getDescribe().getRecordTypeInfos()  ;
		for ( RecordTypeInfo rt : rtInfos)
		{
			if (rt.getRecordTypeId() == recordTypeId)
			{
				return rt.isAvailable();	
			}
		}
		return false;
		
		
	}

	class LayoutFields 
	{
		public String fieldName { get; set; }
		public Boolean isEditable { get; set; }
		public Boolean required { get; set; }
		public String styleCls { get; set; }
		public LayoutFields( String fld, Boolean edit, Boolean req, String sc )
		{
			fieldName = fld;
			isEditable = edit;
			required = req;
			styleCls = sc;
		}
	}
}