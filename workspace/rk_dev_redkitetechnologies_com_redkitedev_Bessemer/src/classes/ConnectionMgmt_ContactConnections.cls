public with sharing class ConnectionMgmt_ContactConnections 
{
	public String contactId { get; set; }
	public Connections__c connectionObj { get; set; }
	public List<Connections__c> contactList { get; set; }
	public Connections__c connectionObjRel { get; set; }
	public Contact contactObj { get; set; }
	public String hasError1 { get; set; }
	public String hasError2 { get; set; }
	public String filterString { get; set; }
	public String selectedId { get; set; }
	public String contactTitle { get; set; }
	public List<FieldSet> fieldList { get; set; }
	private String recordTypeId;
	public String actionType { get; set; }

	private Map<String, SObjectField> accountFields;
	private Map<String, SObjectField> contactFields;
	private Map<String, String> labelCache;
	public ConnectionMgmt_ContactConnections()
	{
		if( contactId == null )
		{
			contactId = ApexPages.currentPage().getParameters().get('Id');
			connectionObj = new Connections__c();
			recordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND Name = 'C:C' ].Id;
			connectionObj.RecordTypeId = recordTypeId;
			connectionObjRel = new Connections__c();
			connectionObjRel.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND Name = 'R:C' ].Id;
		}
		hasError1 = '0';
		hasError2 = '0';
		filterString = 'All';
		actionType = 'Relationship';
		labelCache = new Map<String, String> ();
	}
	public void init()
	{
		contactTitle = [ SELECT Name FROM Contact WHERE Id = :contactId ].Name;
		createContactList();
	}

	public String getFieldLabel( String objType, String fieldName )
	{
		SObjectField fieldToken;
		if( accountFields == null )
		{
			SObjectType objToken = Schema.getGlobalDescribe().get('Account');
			DescribeSObjectResult objDef = objToken.getDescribe();
			accountFields = objDef.fields.getMap();
		}
		if( contactFields == null )
		{
			SObjectType objTokenContacts = Schema.getGlobalDescribe().get('Contact');
			DescribeSObjectResult objDefContacts = objTokenContacts.getDescribe();
			contactFields= objDefContacts.fields.getMap();
		}
		if( objType == 'Account' )
			fieldToken = accountFields.get( fieldName );

		if( objType == 'Contact' )
			fieldToken = contactFields.get( fieldName );
		
		DescribeFieldResult selectedField = fieldToken.getDescribe();
		return selectedField.getLabel();
	}

	public String getPanelVisibility()
	{
		if( actionType == 'Contact' )
			return 'ContactPanel';
		if( actionType == 'Relationship' )
			return 'RelationshipPanel';

		//Default Value
		return 'ContactPanel';
	}

	public void resetConnectionObj()
	{
		hasError1 = '0';
		hasError2 = '0';
		connectionObj = new Connections__c();
		connectionObjRel= new Connections__c();
	}

	private void createContactList()
	{
		List<Connections__c> tmpConnectionList;
		if( filterString == 'All' )
		{
			String sqlFields =  buildFieldList ( 'ContactConnection' );
			tmpConnectionList = Database.query('SELECT Id, RecordType.Name, Contact_C1__c, Contact_C2__r.Name, Relationship_R1__r.Name, Role_on_Relationship__c ' + sqlFields
			+ ' FROM Connections__c WHERE Contact_C1__c = \'' + contactId + '\' ' );
		}
		if( filterString == 'ClientContact' )
		{
			String sqlFields =  buildFieldList( 'ContactConnection' );
			tmpConnectionList = Database.query('SELECT Id, RecordType.Name, Contact_C1__c, Contact_C2__r.Name, Relationship_R1__r.Name, Role_on_Relationship__c ' + sqlFields
			+ ' FROM Connections__c WHERE Contact_C1__c = \'' + contactId + '\' AND ( Contact_C2__r.RecordType.Name = \'Client Contact\' OR Contact_C2__r.RecordType.Name = \'Prospect\' )');
			
		}
		if( filterString == 'NonClientContact' )
		{
			String sqlFields =  buildFieldList( 'ContactConnection' );
			tmpConnectionList = Database.query('SELECT Id, RecordType.Name, Contact_C1__c, Contact_C2__r.Name, Relationship_R1__r.Name, Role_on_Relationship__c ' + sqlFields
			+ ' FROM Connections__c WHERE Contact_C1__c = \'' + contactId + '\' AND Contact_C2__r.RecordType.Name = \'Non-Client Contact\'');
			
		}
		if( filterString == 'ClientRelationships' )
		{
			String sqlFields =  buildFieldList( 'AllRelationships' );
			tmpConnectionList = Database.query('SELECT Id, RecordType.Name, Contact_C1__c, Contact_C2__r.Name, Relationship_R1__r.Name, Role_on_Relationship__c ' +  sqlFields
			+ ' FROM Connections__c WHERE Contact_C1__c = \'' + contactId + '\' AND ( Relationship_R1__r.RecordType.Name = \'Client Relationship\' OR Relationship_R1__r.RecordType.Name = \'Prospective Relationship\' )');
		}
		if( filterString == 'NonClientRelationships' )
		{
			String sqlFields =  buildFieldList( 'AllRelationships' );
			tmpConnectionList = Database.query('SELECT Id, RecordType.Name, Contact_C1__c, Contact_C2__r.Name, Relationship_R1__r.Name, Role_on_Relationship__c ' +  sqlFields
			+ ' FROM Connections__c WHERE Contact_C1__c = \'' + contactId + '\' AND Relationship_R1__r.Recordtype.Name = \'Business\'') ;
		}
		if( filterString == 'Entity' )
		{
			String sqlFields =  buildFieldList( 'ContactConnection' );
			tmpConnectionList = Database.query('SELECT Id, RecordType.Name, Contact_C1__c, Contact_C2__r.Name, Relationship_R1__r.Name, Role_on_Relationship__c ' + sqlFields
			+ ' FROM Connections__c WHERE Contact_C1__c = \'' + contactId + '\' AND Contact_C2__r.RecordType.Name = \'Entity\'');
		}

		//Check to make sure the user has read access 
		List<Id> searchedIds = new List<Id>();
		for( Connections__c connection : tmpConnectionList )
		{
			if( connection.RecordType.Name == 'R:C' && connection.Relationship_R1__c != null )
				searchedIds.add(connection.Relationship_R1__c ) ;
			if( connection.RecordType.Name == 'C:C' && connection.Contact_C1__c != null )
				searchedIds.add(connection.Contact_C1__c);
		}

		List<UserRecordAccess> userRecordAccesses = [ SELECT RecordId FROM UserRecordAccess WHERE RecordId IN :searchedIds AND UserId = :UserInfo.getUserId() AND HasReadAccess = true ];
		Set<String> accessSet = new Set<String>();
		for(UserRecordAccess ur : userRecordAccesses)
			accessSet.add( ur.RecordId );

		contactList = new List<Connections__c>();
		for( Connections__c connection : tmpConnectionList )
		{
			if( connection.RecordType.Name == 'R:C' && accessSet.contains( connection.Relationship_R1__c ) && connection.Contact_C1__c != null )
				contactList.add(connection) ;
			if( connection.RecordType.Name == 'C:C' && accessSet.contains( connection.Contact_C1__c ) && connection.Contact_C2__c != null )
				contactList.add(connection);
		}
	}

	private String buildFieldList( String filterType )
	{
		List<AddressConnectionMap__c> addressMap = AddressConnectionMap__c.getAll().values();
		String sqlFields = '';
		List<FieldSet> fldSetList = new List<FieldSet>();
		String fieldLabel = '';

		for( AddressConnectionMap__c fieldMap : addressMap )
		{
			if( fieldMap.Type__c == filterType )
			{
				if( !labelCache.containskey( fieldMap.FieldName__c ) )
				{
					String [] fieldSplit = fieldMap.FieldName__c.split( '\\.' );
					if( fieldSplit.size() > 0 )
					{
						if( fieldSplit[0].contains( 'Relationship' ) )
							fieldLabel = getFieldLabel( 'Account', fieldSplit[1] );
						if( fieldSplit[0].contains( 'Contact' ) )
							fieldLabel = getFieldLabel( 'Contact', fieldSplit[1] );

						labelCache.put( fieldMap.FieldName__c, fieldLabel );
					}
				}
				else
					fieldLabel = labelCache.get( fieldMap.FieldName__c );

				fldSetList.add( new FieldSet( Integer.valueOf( fieldMap.Sort_Order__c ), fieldMap.FieldName__c, fieldLabel ) );
				if( sqlFields.length() == 0 )
					sqlFields = fieldMap.FieldName__c;
				else
					sqlFields += ',' + fieldMap.FieldName__c;
			}
		}

		if( fldSetList.size() > 0 )
		{
			fldSetList.sort();
			for( FieldSet f : fldSetList )
				fieldList  = fldSetList;
		}

		if( sqlFields.length() > 0 )
			sqlFields = ',' + sqlFields;
		return sqlFields;
	}

	public void saveRoleChange()
	{
		contactObj = new Contact();
		for( Connections__c c : contactList )
		{
			if( c.Id == selectedId )
			{
				try
				{
					update c;
				}
				catch( Exception ex )
				{
					//Error message will bubble up from Trigger
				}
				break;
			}
		}
		createContactList();
	}

	public void filterList()
	{
		createContactList();
	}

	public PageReference saveAndNew()
	{
		saveNewConnection();
		return null;
	}
	
	public PageReference saveAndClose()
	{
		saveNewConnection();
		if( hasError1 == '1' || hasError2 == '1' )
			return null;
		else
			return returnUrl();
	}

	public PageReference cancel()
	{
		return returnUrl();
	}

	private Pagereference returnUrl()
	{
		PageReference pageRef = new PageReference('/' + contactId);
		pageRef.setRedirect( true );
		return pageRef;
	}

	private void saveNewConnection()
	{
		hasError1 = '0';
		hasError2 = '0';
		Connections__c connection = new Connections__c();

		if( actionType == 'Contact' )
		{
			if( connectionObj.Contact_C2__c == null )
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact Name is a required field'));
				hasError1 = '1';
			}
			if( connectionObj.Role_on_Relationship__c == null )
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The Contact Role is a required field'));
				hasError2 = '1';
			}
			if( hasError1 == '1' || hasError2 == '1' )
				return;

			connection.Contact_C1__c = contactId;
			connection.Contact_C2__c = connectionObj.Contact_C2__c;
			connection.Role_on_Relationship__c = connectionObj.Role_on_Relationship__c;
			connection.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND DeveloperName = 'C_C' ].Id;
			insert connection;
		}
		
		if( actionType =='Relationship' )
		{
			if( connectionObjRel.Relationship_R1__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Relationship Name is a required field' ) );
				hasError1 = '1';
			}
			if( connectionObjRel.Role_on_Relationship__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Reltaionship Role is a required field' ) );
				hasError2 = '1';
			}
			if( hasError1 == '1' || hasError2 == '1' )
				return;

			connection.Relationship_R1__c = connectionObjRel.Relationship_R1__c;
			connection.Contact_C1__c = contactId;
			connection.Role_on_Relationship__c = connectionObjRel.Role_on_Relationship__c;
			connection.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND DeveloperName = 'R_C' ].Id;
			insert connection;
		}

		connectionObj = new Connections__c();
		connectionObjRel = new Connections__c();
		resetConnectionObj();
		createContactList();
	}

	public class FieldSet implements Comparable
	{
		public Integer sortNumber { get; set; }
		public String field { get; set; }
		public String fieldLabel { get; set; }
		
		public FieldSet ( Integer s, String f, String fldLabel )
		{
			sortNumber = s;
			field = f;
			fieldLabel = fldLabel;
		}

		public Integer compareTo( Object compareTo )
		{
			FieldSet fs = (FieldSet)compareTo;
			if( sortNumber > fs.sortNumber ) return 1;
			if( sortNumber == fs.sortNumber ) return 0;
			return -1;
		}
	}
}