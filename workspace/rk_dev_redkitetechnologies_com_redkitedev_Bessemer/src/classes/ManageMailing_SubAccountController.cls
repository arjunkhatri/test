public with sharing class  ManageMailing_SubAccountController {
    SubAccounts__c subAccount;

    public List<MailingTableEntry> tableEntries {get;set;}
    private Set<Id> mailingIdSet; // list of base Mailing records

    public ManageMailing_SubAccountController (ApexPages.StandardController controller)
    {
        subAccount = (SubAccounts__c)controller.getRecord();
        mailingIdSet = new Set<Id>();
        tableEntries = new List<MailingTableEntry>();
    }
    
    public void getMailingRecs()
    {
        List<Mailing_Link__c> directMailingLinks = [select Id, MailingId__c from Mailing_Link__c where SubAccountId__c=:subAccount.Id];

        mailingIdSet.clear();
        for(Mailing_Link__c currMailingLink : directMailingLinks){
            mailingIdSet.add(currMailingLink.MailingId__c);
        }
        
        tableEntries.clear();
        Map<Id, MailingTableEntry> mailingMap = new Map<Id, MailingTableEntry>();

        List<Mailing__c> tmpMailingList = [Select Id, Name, Mailing_Number__c, Mailings__c, Salutation__c, Type__c, Delivery_Method__c, Details__c, 
                                        Contact__c, Contact__r.Name, 
                                        Address__c, Address__r.Address_Type__c, 
   
                                        Address__r.Street_Address__c, 

                                        Address__r.City__c, Address__r.State__c, Address__r.Postal_Code__c , Lead__c
                                        from Mailing__c where Id IN: mailingIdSet or SubAccountId__c = :subAccount.Id];

         List<Mailing__c> mailingList = CheckRecordVisibility (tmpMailingList ) ;

        for(Mailing__c mail: mailingList){  
            System.debug('Mailing ' + mail.Name);
            MailingTableEntry newTableEntry = new MailingTableEntry(mail);

            tableEntries.add(newTableEntry);
            mailingMap.put(mail.Id, newTableEntry);
        }

        for(Mailing_Link__c ml: [Select Id, Name, MailingId__c, 
                                Appraisal_Freq__c, Basis_Override__c, Alt_Mailer__c, Cash_Advice__c, Cash_Statement_Freq__c, Cotrustee_Letter__c,Fee_Type__c,
                                Income_Distribution__c, Proxy_Type__c, Security_Advice__c, Statement_Type__c, Tax_Report__c, 
                                Investment_Group_Name__c, Investment_Group_Number__c,
                                SubAccountId__c, SubAccountId__r.Name, SubAccountId__r.SubAccount_Title__c,
                                Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Contact_S_Number__c,
                                Family_Group__c, Family_Group__r.Name, Family_Group__r.Family_Group__c,
                                RecordTypeId, RecordType.Name 
                                from Mailing_Link__c where MailingId__c IN: mailingMap.keySet()]){

            MailingTableEntry mailTableEntry = mailingMap.get(ml.MailingId__c);
            if (mailTableEntry!=null) mailTableEntry.addLink(ml);
        }
    }
    private List<Mailing__c> CheckRecordVisibility ( List<Mailing__c> MailList)
    {
        List<Mailing__c> returnedMailList = new List<Mailing__c> ();

        set<string> contactSet = new set<string>() ;
        set<string> leadSet = new set<string>() ;

        for (Mailing__c mailing : MailList)
        {
            if (mailing.Contact__c != null)
                contactSet.add(mailing.Contact__c) ;
            if (mailing.lead__c != null && mailing.Contact__c == null)
                leadSet.add(mailing.lead__c) ;
        }
        
        map<Id, Contact> contactMap = new map<id, Contact> ([select id , UserRecordAccess.HasEditAccess from Contact where id in :contactSet]) ;
        map<Id, Lead> leadMap = new map<id, Lead> ([select id, UserRecordAccess.HasEditAccess from Lead where id in :leadSet]) ;
        for (Mailing__c mailing : MailList)
        {
            if (contactMap.containsKey(mailing.Contact__c) || leadMap.containsKey(mailing.Lead__c) )
                returnedMailList.add(mailing)  ;
        }

        return returnedMailList ;
    }

    /*public class TableEntry
    {
        public Mailing__c mailingRec{get;set;}
        public List<Mailing_Link__c> familyGroups{get;set;}
        public List<Mailing_Link__c> otherContacts{get;set;}
        public List<Mailing_Link__c> otherEntities{get;set;}
        public Boolean isSelected{get;set;}
        
        public TableEntry(Mailing__c rec){
            this.mailingRec = rec;
            familyGroups = new List<Mailing_Link__c>();
            otherContacts = new List<Mailing_Link__c>();
            otherEntities = new List<Mailing_Link__c>();
        }
        public void addLink(Mailing_Link__c inLink){
            if(inLink.RecordType.Name=='Family Group') familyGroups.add(inLink);
            else if(inLink.RecordType.Name=='Other Contact') otherContacts.add(inLink);
            else otherEntities.add(inLink);
        }

    }*/

}