/*
    
     Authors :  Don Koppel
     Created Date: 2014-08-20
     Last Modified: 2014-08-20
*/

public with sharing class AccountContactTrigHand {  
	public static final String OWNER = 'Owner';  
	//public static final String RECIPIENT = '1099R Recipient';
	public static final String THIRDPARTY = 'Third Party';
	public Boolean isBessemerIntegration;
	public AccountContactTrigHand(){ 
		isBessemerIntegration = false; 
		List<Profile> biProfile = [Select Id From Profile Where Name = 'Bessemer Integration' limit 1];
		if(biProfile != null && biProfile.size() > 0){
			if(biProfile[0].id == userinfo.getProfileId())
				isBessemerIntegration = true;
		}
	}
    // template methods
    public void onAfterInsert(list<Account_Contact__c> newList) {
    	updateSyncTable(newList);
    }
    public void onAfterUpdate(list<Account_Contact__c> newList, map<id,Account_Contact__c> oldMap) {
    	updateSyncTable(newList);
    	deleteAccountContact(newList);
    }
    public void onBeforeUpdate(list<Account_Contact__c> newList, map<id,Account_Contact__c> oldMap) {
    	//if user trying to remove Owner role or the 1099 Recipient Role from the list of roles selected for an Account Contact record
		//throw validation error
		if(!isBessemerIntegration){ 
			validateAccContact(newList,oldMap);
		}
    }    
    public void onBeforeDelete(list<Account_Contact__c> oldList){
    	deleteSyncTable(oldList);
    } 
    public void onBeforeInsert(list<Account_Contact__c> newList){
		if(!isBessemerIntegration){ 
			for(Account_Contact__c acCon:newList){ 
				if(acCon.Role__c != THIRDPARTY && !String.isEmpty(acCon.Role__c)){ 
					acCon.addError(System.Label.AccountContact_Assign_Third_Party_Role);
				}
			}
		}
    } 
	// business logic
	
	public void validateAccContact(list<Account_Contact__c> newList,map<id,Account_Contact__c> oldMap){
		for(Account_Contact__c acRec:newList){
			// If a contact is not connected to an account then a user can connect the contact to the account using only 
			//the Third Party role.  Users are not allowed to assign any other roles.
			if(acRec.Contact__c != null && oldMap.get(acRec.id).Account__c == null && acRec.Account__c != null){
				if(acRec.Role__c != THIRDPARTY) 
					acRec.addError(System.Label.AccountContact_Assign_Third_Party_Role);
			}
			// If a contact is already connected to an account then a user can only 
			//add the Third Party role to the existing connection.
			if(acRec.Contact__c != null && acRec.Account__c != null && 
				!isSelected(String.valueof(oldMap.get(acRec.id).Role__c),THIRDPARTY)){
				if(checkAddThirdPartyOnly(String.valueof(oldMap.get(acRec.id).Role__c),acRec.Role__c)){ 
					acRec.addError(System.Label.AccountContact_Add_Third_Party_Role); 
				}
				else if(String.valueof(oldMap.get(acRec.id).Role__c) !=  acRec.Role__c && !isSelected(acRec.Role__c,THIRDPARTY)){
					acRec.addError(System.Label.AccountContact_Add_Third_Party_Role);
				}	
			}	
			// If a contact is already connected to an account and has the Third Party role assigned 
			//then a user can remove the Third Party role but no other role.
			
			if(acRec.Contact__c != null && acRec.Account__c != null && oldMap.get(acRec.id).Role__c != null &&
				isSelected(String.valueof(oldMap.get(acRec.id).Role__c),THIRDPARTY)){
				if(checkRemoveThirdPartyOnly(String.valueof(oldMap.get(acRec.id).Role__c),acRec.Role__c))
					acRec.addError(System.Label.AccountContact_Remove_Third_Party_Role);
			}
		}
	}

	//function to check user adding role other than third party role
	public boolean checkAddThirdPartyOnly(String oldVal,String newVal){
		Boolean isTPOnly = false;
		List<String> lstOldRoles = new List<String>();
		List<String> lstNewRoles = new List<String>();
		if(!isNullOrEmpty(oldVal) && !isNullOrEmpty(newVal)){
			if(oldVal.contains(';'))
				lstOldRoles = oldVal.split(';');
			else
				lstOldRoles.add(oldVal);	
			if(newVal.contains(';'))	
				lstNewRoles = newVal.split(';');
			else
				lstNewRoles.add(newVal);	
		}
		else if(isNullOrEmpty(oldVal) && newVal != THIRDPARTY){
			isTPOnly = true;
			return isTPOnly;
		}
		for(String strNew:lstNewRoles){
			Boolean isStrContain = false;
			for(String srtOld:lstOldRoles){
				if(strNew == srtOld){
					isStrContain = true;
				}
			}
			if(!isStrContain){
				if(strNew == THIRDPARTY){
					isTPOnly = false;
				}
				else{
					isTPOnly = true;
					break;
				}
			}			
		}
		return isTPOnly;
	}	
	//function to check user removing role other than third party role
	public boolean checkRemoveThirdPartyOnly(String oldVal,String newVal){
		Boolean isTPRemoveOnly = false;
		List<String> lstOldRoles = new List<String>();
		List<String> lstNewRoles = new List<String>();
		if(!isNullOrEmpty(oldVal) && !isNullOrEmpty(newVal)){
			if(oldVal.contains(';'))
				lstOldRoles = oldVal.split(';');
			else
				lstOldRoles.add(oldVal);
			if(newVal.contains(';'))		
				lstNewRoles = newVal.split(';');
			else
				lstNewRoles.add(newVal);	
		}
		else if(isNullOrEmpty(newVal) && oldVal != THIRDPARTY){
			isTPRemoveOnly = true;
			return isTPRemoveOnly;
		}
		for(String strOld:lstOldRoles){
			Boolean isStrContain = false;
			for(String strNew:lstNewRoles){
				if(strOld == strNew){
					isStrContain = true; 
				}
			}
			if(!isStrContain){
				if(strOld == THIRDPARTY){
					isTPRemoveOnly = false;
				}
				else{
					isTPRemoveOnly = true;
					break;
				}
			}			
		}
		return isTPRemoveOnly;
	}

	//function to find role in roles
	public Boolean isSelected(String strRoles,String role){ 
		Boolean isSel = false;
		if(strRoles != null && strRoles != ''){
			List<String> lstRole = new  List<String>();
			if(strRoles.contains(';'))
				lstRole = strRoles.split(';');
			else
				lstRole.add(strRoles);	
			for(String strR:lstRole){
				if(strR == role)
					isSel = true;
			}
		}
		return isSel;
	}
	public Boolean isNullOrEmpty(String strVal){
		if(strVal != null && strVal != '')
			return false;
		return true;	
	}
	public void deleteSyncTable(list<Account_Contact__c> account_contacts){
    	if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
			//delete all Account_Contact_Sync__c records that were linked to the the delete Account_Contact__c records
			List<Account_Contact_Sync__c> acsList = [SELECT Id, Account_Contact__c, Role__c FROM Account_Contact_Sync__c WHERE Account_Contact__c in :account_contacts];
			delete acsList;
    	}
    	
    	if(!isBessemerIntegration){ 
    		for(Account_Contact__c ac:account_contacts){
    			if(ac.Role__c != THIRDPARTY && !String.isEmpty(ac.Role__c)){
    				ac.addError(System.Label.AccountContact_Del_Third_Party_Role);
    			}
    		}
    	}
	}

	public void deleteAccountContact(list<Account_Contact__c> account_contacts){
		List<Account_Contact__c> acDeleteList = new List<Account_Contact__c>();
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
			//If the last role on the Account Contact record has been removed via sync from EDB, delete the Account Contact record
			List<Account_Contact__c> acList = [SELECT Id, Role__c FROM Account_Contact__c WHERE Id in :account_contacts];
			for(Account_Contact__c ac : acList){
				if( ac.Role__c == '' || ac.Role__c == null )
					acDeleteList.add(ac); 
			}
			if (acDeleteList.size() > 0)
				delete acDeleteList;
    	}
	}
	
	public void updateSyncTable(list<Account_Contact__c> account_contacts){

    	if (UserInfo.getUserName() != GlobalSettings__c.getInstance().Integration_User_Name__c){
			Boolean bFound;
			
			//These lists will contain the list of Account_Contact_Sync records that will need to be added and deleted
			List<Account_Contact_Sync__c> acsAddList = new List<Account_Contact_Sync__c>();
			List<Account_Contact_Sync__c> acsDelList = new List<Account_Contact_Sync__c>();
				
			//Create a map of Account Contact Sync records that exist for each Account_Contact
			Map<Id, Set<Account_Contact_Sync__c>> acsMap = new Map<Id, Set<Account_Contact_Sync__c>>();
			List<Account_Contact_Sync__c> acsList = [SELECT Id, Account_Contact__c, Role__c FROM Account_Contact_Sync__c WHERE Account_Contact__c in :account_contacts];
	
			for (Account_Contact_Sync__c acs : acsList){
				if(!acsMap.containsKey(acs.Account_Contact__c)){
					Set<Account_Contact_Sync__c> acsMapSet = new Set<Account_Contact_Sync__c>();
					acsMapSet.add(acs); 
					acsMap.put(acs.Account_Contact__c, acsMapSet);
				}	
				else{
					Set<Account_Contact_Sync__c> acsMapSet = acsMap.get(acs.Account_Contact__c);
					acsMapSet.add(acs);
				}
			}	
				
			//For each Account_Contact record, parse the roles and determine which already exist, which should be added, and which should be deleted	
			for (Account_Contact__c ac : account_contacts){
				List<String> roleList = new List<String>();
				if(!isNullOrEmpty(ac.Role__c) && ac.Role__c.contains(';'))
					roleList = ac.Role__c.split(';');
				else if(!isNullOrEmpty(ac.Role__c))
					roleList.add(ac.Role__c);
				Set<Account_Contact_Sync__c> acsMapSet = acsMap.get(ac.Id);
				
				if (acsMapSet != null){
					for(Account_Contact_Sync__c acs : acsMapSet){
						//Use acsMapList as the basis for the delete list
						//If we find a role, remove it from the delete list and remove the Role from the list 
						bFound = false;
						String sRole;
						if(roleList != null && roleList.size() > 0)
						{
							for (Integer i = 0; i < roleList.size(); i++){
								sRole = roleList[i];
								if (sRole == acs.Role__c){
									roleList.remove(i);
									acsMapSet.remove(acs);
								}				
							}
						}
					}
					for(Account_Contact_Sync__c acs : acsMapSet){
						acsDelList.add(acs);
					}
				}
	
				//All remaining roles should be added as Account_Contact_Sync__c records
				for (String sRole : roleList){
					Account_Contact_Sync__c acs = new Account_Contact_Sync__c();
					acs.Account_Contact__c = ac.Id;
					acs.Role__c = sRole;
					acs.Account__c = ac.Account__c;
					acs.Contact__c = ac.Contact__c;
					acsAddList.add(acs);
				}
			}
			
			if (acsAddList != null)
				insert acsAddList;
			if (acsDelList != null)
				delete acsDelList;
		}
	}
	
}