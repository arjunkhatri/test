public with sharing class FeedCommentTrigHand { 
	public static final String THREADID = 'Thread Id: ';
	public static final String POSTTEXT = 'Chatter Post Text: ';
	public static final String PARENTID = 'Parent Record Id: ';
	public static final String EDBID = 'EDB Id of the associated record: ';
	public static final String RECNAME = 'Name of the associated record: ';
	public static final String SUBJECT = ' Main Chatter Post: ';
	Map<String,Set<String>> mpPreToIds;
	Map<String,String> mpFeedToEDB; 
	Map<String,String> mpFeedToName;
	Map<String,String> mpPreToObjApi; 
	Map<String,String> mpPreToFldApi; 
	public Map<String,ContentVersion> mpFcomToFile;
	public Set<String> versionIds;
	public FeedCommentTrigHand(){
	
	}
    public void beforeDelete(List<FeedComment> lstPost){ 
    	for(FeedComment fCom:lstPost){
    		fCom.addError(System.Label.Chatter_Post_Not_Del);
    	}
    }	
	public void mailComment(List<FeedComment> lstComments){
		mpPreToIds = new Map<String,Set<String>>();
		mpFeedToEDB = new Map<String,String>(); 
		mpFeedToName = new Map<String,String>();
		mpPreToObjApi = new Map<String,String>(); 
		mpPreToFldApi = new Map<String,String>(); 		
		mpFcomToFile = new Map<String,ContentVersion>();
		versionIds = new Set<String>(); 
		String fromAddress;
		String toAddress;
		//read current user email id
		User cUser = [Select Email from User where id=:userinfo.getUserId()];
		if(cUser != null){
			fromAddress = cUser.Email;
		}
        //read to address from custom setting
        GlobalSettings__c gSetting = GlobalSettings__c.getOrgDefaults();
        if(gSetting != null){
        	toAddress = gSetting.Compliance_Email_Address__c;
        }
         		
		//get custom setting data in map
		List<Chatter_Compliance_Field_Mapping__c> cFields = Chatter_Compliance_Field_Mapping__c.getall().values();
		if(cFields != null && cFields.size() > 0){
			for(Chatter_Compliance_Field_Mapping__c rec:cFields){
				mpPreToObjApi.put(rec.Object_Id_Prefix__c,rec.Object_API_Name__c);
				mpPreToFldApi.put(rec.Object_Id_Prefix__c,rec.EDB_Id_Field__c);
			}
		}
		
		for(FeedComment fComment:lstComments){
			String prefix = String.valueOf(fComment.ParentId).substring(0,3);
			if(mpPreToIds.containsKey(prefix)){ 
				mpPreToIds.get(prefix).add(fComment.ParentId);
			}else{
				mpPreToIds.put(prefix,new Set<String>{fComment.ParentId});
			}
			versionIds.add(fComment.RelatedRecordId);
		}
		
		if(versionIds != null && !versionIds.isEmpty()){ 
			//get feed comment attachment
			List<ContentVersion> lstVesrion = [Select VersionData, Title, Id From 
											ContentVersion where id=:versionIds];
			for(ContentVersion cv:lstVesrion){
				mpFcomToFile.put(cv.id,cv);
			}										
		}
		
		for(String mp:mpPreToIds.keyset()){ 
			//get edb id and name 
			getEdbId(mp,mpPreToIds.get(mp));
		}
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
  		for(FeedComment feedCom:lstComments){  
  			String prefix = String.valueOf(feedCom.ParentId).substring(0,3);
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
			mail.setSubject(feedCom.FeedItemId + SUBJECT); 
			String body   = '<html><body> '+ THREADID + feedCom.FeedItemId+ '<br>'; 
					if(feedCom.CommentBody != null){ 
	      				body += POSTTEXT + feedCom.CommentBody+ '<br>';
					}else{
						body += POSTTEXT + '<br>';
					}
	      			body += PARENTID + feedCom.ParentId+ '<br>';
	      			body += EDBID + mpFeedToEDB.get(prefix)+ '<br>';
	      			body += RECNAME + mpFeedToName.get(prefix)+ '<br></body></html>';
	      		
	      	mail.setHtmlBody(body);	
			List<String> sendTo = new List<String>();
      		sendTo.add(toAddress);	  
			mail.setToAddresses(sendTo); 
	 		mail.setReplyTo(fromAddress);
	 		
			// Create the email attachment
			if(feedCom.RelatedRecordId != null){
				ContentVersion cVersion = mpFcomToFile.get(feedCom.RelatedRecordId);
			    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
			    efa.setFileName(cVersion.Title);
			    efa.setBody(cVersion.VersionData); 	 		
	 			mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
			}
	 		mails.add(mail);
  		}
  		if(mails.size() > 0)
  			Messaging.sendEmail(mails);	
	}
	
	public void getEdbId(String prefix, Set<String> setParents){  
		String objApiName = mpPreToObjApi.get(prefix);
		String fieldApiName = mpPreToFldApi.get(prefix);
		List<Sobject> obj;
		//confirm all parameters collected
		if(objApiName != null || objApiName != ''){
			String query = 'SELECT Name ';
				if(fieldApiName != null && fieldApiName != ''){
					query = query +','+fieldApiName;
				}
				query = query + ' FROM ' + objApiName 
				  + ' WHERE Id = :setParents';
			obj = Database.query(query);
		}	
		if(obj != null && obj.size() > 0){ 
			for(Sobject sObj:obj){
				if(fieldApiName != null && fieldApiName != ''){
					if(sObj.get(fieldApiName) != null && sObj.get(fieldApiName) != ''){
						String strEDB = String.valueOf(sObj.get(fieldApiName));
						if(strEDB != null)  
							mpFeedToEDB.put(prefix, strEDB);
					}else{
						mpFeedToEDB.put(prefix, 'N/A');
					}
				}else{
					mpFeedToEDB.put(prefix, 'N/A');
				}
				if(sObj.get('Name') != null && sObj.get('Name') != ''){	
					mpFeedToName.put(prefix,String.valueOf(sObj.get('Name')));
				}
			}
		}
	}	
}