global class FamilyGroupSharingBatch implements Database.Batchable<Family_Group__c>{
	public List<Family_Group__c> lstFamilyGrp;
    public List<Family_Group__Share> lstInsert;
    public List<Family_Group__Share> lstDelete;
    public List<Family_Group__Share> lstUpdate;
    public static final String ALL = 'All';	
    public static final String READ = 'read';	
    public static final String EDIT = 'edit';	
    public static final String OWNER = 'Owner';
	public FamilyGroupSharingBatch(){ 
		lstFamilyGrp = [Select Id From Family_Group__c];//where Name like '%Family G Test%'
	}
	
    global Iterable<Family_Group__c> start(Database.BatchableContext BC){
    	return lstFamilyGrp;
    }

    global void execute(Database.BatchableContext BC, List<Family_Group__c> scope){
		map<String,String> mpOldFgAccess = new map<String,String>();
		map<String,String> mpNewFgAccess = new map<String,String>(); 
    	lstInsert = new List<Family_Group__Share>();
    	lstDelete = new List<Family_Group__Share>();
    	lstUpdate = new List<Family_Group__Share>();	
		List<String> lstFgids = new List<String>();

		for(Family_Group__c lfg:scope){
			lstFgids.add(lfg.id);
		}
		if(lstFgids != null && lstFgids.size() > 0){
			//build old map		   	
			List<Family_Group__Share> lstFgshr = [Select UserOrGroupId,ParentId,Id,AccessLevel,RowCause
			 								From Family_Group__Share where ParentId=:lstFgids];
			for(Family_Group__Share fgs:lstFgshr){
				mpOldFgAccess.put(fgs.ParentId+'-'+fgs.UserOrGroupId,fgs.AccessLevel);
			}
			//build new updated map
			List<Account> lstAccount = [Select OwnerId,Id,Family_Group__c,Family_Group__r.OwnerId,
									   (Select AccountId,UserOrGroupId, AccountAccessLevel From 
									   Shares) From Account where Family_Group__c=:lstFgids];
			for(Account ac:lstAccount){
				for(AccountShare acs:ac.Shares){
					String strUser = String.valueOf(acs.UserOrGroupId);
					if(acs.AccountAccessLevel == ALL){
						mpNewFgAccess.put(ac.Family_Group__c+'-'+acs.UserOrGroupId,ALL);
					}
					else if(acs.AccountAccessLevel == EDIT || acs.AccountAccessLevel==READ){
							if(mpNewFgAccess.containsKey(ac.Family_Group__c+'-'+acs.UserOrGroupId) ){
								if(mpNewFgAccess.get(ac.Family_Group__c+'-'+acs.UserOrGroupId) == ALL){
									//break;
								}
								else if (mpNewFgAccess.get(ac.Family_Group__c+'-'+acs.UserOrGroupId) == READ 
										&& acs.AccountAccessLevel == EDIT){
									mpNewFgAccess.put(ac.Family_Group__c+'-'+acs.UserOrGroupId , EDIT);
								}
							}
							else{
								mpNewFgAccess.put(ac.Family_Group__c+'-'+acs.UserOrGroupId,acs.AccountAccessLevel);
							}		
					}
					else{
						mpNewFgAccess.put(ac.Family_Group__c+'-'+acs.UserOrGroupId,acs.AccountAccessLevel);
					}
				}
			}	
			//compaire old and new maps
			for(Family_Group__Share rec:lstFgshr){
				String strKey = rec.ParentId+'-'+rec.UserOrGroupId;
				if(mpOldFgAccess.get(strKey) != null && mpNewFgAccess.get(strKey) != null){
					if(mpOldFgAccess.get(strKey) != mpNewFgAccess.get(strKey)){
						//update new access
						if(mpNewFgAccess.get(strKey) != ALL){
							rec.AccessLevel = mpNewFgAccess.get(strKey);
						}else{
							rec.AccessLevel = EDIT;
						}
						if(rec.RowCause != OWNER){
							lstUpdate.add(rec);
						}
					}
				}
				//check record exist in old map but not in new map delete such record
				if(mpNewFgAccess.get(strKey) == null && rec.RowCause != OWNER){
					lstDelete.add(rec);
				}
			}
			for(String mpK:mpNewFgAccess.Keyset()){
				//check record exist in new map but not in old map then insert such record
				if(mpOldFgAccess.get(mpK) == null && mpNewFgAccess.containsKey(mpK)){
					Family_Group__Share fgs = new Family_Group__Share();
					if(mpNewFgAccess.get(mpK) != ALL){
						fgs.AccessLevel = mpNewFgAccess.get(mpK);
					}else{
						fgs.AccessLevel = EDIT;
					}
					if(mpK.contains('-')){ 
						fgs.ParentId = mpK.split('-')[0];
						fgs.UserOrGroupId = mpK.split('-')[1];
						lstInsert.add(fgs);
					}
				}
			}
			performDml();
		}
   	}
   
	global void finish(Database.BatchableContext BC){
	   	
	}       
	public void performDml(){
		//update Records
		List<Database.SaveResult> lsr;
		if(lstUpdate != null && lstUpdate.size()>0){
			lsr = Database.update(lstUpdate,false);
	        for(Database.SaveResult sr : lsr){
	            if(!sr.isSuccess()){
	                Database.Error err = sr.getErrors()[0];
	                system.debug('error>>>>'+err);
	            }
	        } 
		}  
        //delete records
		List<Database.DeleteResult> lsrDel;
		if(lstDelete != null && lstDelete.size()>0){
			lsrDel = Database.delete(lstDelete,false);
	        for(Database.DeleteResult dl : lsrDel){
	            if(!dl.isSuccess()){
	                Database.Error errDel = dl.getErrors()[0];
	                system.debug('error>>>>'+errDel);
	            }
	        }
		}         
		//insert records
		List<Database.SaveResult> lsrIns;
		if(lstInsert != null && lstInsert.size()>0){
			lsrIns = Database.insert(lstInsert,false);
	        for(Database.SaveResult il : lsrIns){
	            if(!il.isSuccess()){
	                Database.Error errIns = il.getErrors()[0];
	                system.debug('error>>>>'+errIns);

	            }
	        }
		}         	
	}
}