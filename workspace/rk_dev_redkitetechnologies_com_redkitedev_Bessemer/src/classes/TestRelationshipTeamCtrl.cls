@isTest
public with sharing class TestRelationshipTeamCtrl { 
    static testMethod void Relationshiptest(){
        Profile pf = [Select Id From Profile where Name= 'Bessemer Wealth Advisor'];
        Profile pf1 = [Select Id From Profile where Name= 'Bessemer Client Advisor'];
        User u = TestDataFactory.createTestUser(pf.id);
            insert u;
            system.assert(u.id != null,'Created user record');
        User u1 = TestDataFactory.createTestUser(pf1.id);
            u1.Username = 'test1@domain.com.dev';
            insert u1;
            system.assert(u1.id != null,'Created user record');
        GlobalSettings__c gs = new GlobalSettings__c();
            gs.Relationship_Number_Seq__c = 123;
            insert gs;
        Account acc = TestDataFactory.createTestAccount();
            insert acc;
            system.assert(acc.id != null,'Created relationship record');
        Task tsk = TestDataFactory.createTask(acc.id);
             tsk.Relationship_Team_List__c = '';
             insert tsk;    
        Relationship_Team_Member__c accTM = TestDataFactory.createRelTeamMem(acc.id,u.id,'Cash Manager');
            insert accTM;
            system.assert(accTM.id != null,'Created account team member record');
        AccountShare aShare = TestDataFactory.createAccShare(acc.id,u.id,'Read');
            insert aShare;
            system.assert(aShare.id != null,'Created account share record');
        Account acc1 = TestDataFactory.createTestAccount();
            insert acc1;
            system.assert(acc1.id != null,'Created relationship record');
        Relationship_Team_Member__c accTM1 = TestDataFactory.createRelTeamMem(acc.id,u1.id,'Senior Client Advisor');
            insert accTM1;
            system.assert(accTM1.id != null,'Created account team member record');
        Relationship_Team_Member__c accTM2 = TestDataFactory.createRelTeamMem(acc.id,u.id,'Wealth Advisor');
            insert accTM2;
            system.assert(accTM2.id != null,'Created account team member record');
        AccountShare aShare1 = TestDataFactory.createAccShare(acc.id,u1.id,'Read'); 
            insert aShare1;  
            system.assert(aShare1.id != null,'Created account share record');
        ValidateRelationshipTeamRoles__c vRoles = new ValidateRelationshipTeamRoles__c(Name = 'Senior Client Advisor'); 
            insert vRoles;
        Bessemer_Role_To_Team__c bRole = new Bessemer_Role_To_Team__c(Name = 'Washington DC Client Advisor' ,   Client_Advisor_Team__c = 'Washington DC Client Advisor',
                                                                            Role_Name__c = '1.02.04.01.02 Washington DC Client Advisor',Wealth_Advisor_Team__c='Washington DC Wealth Advisor');
            insert bRole;

            PageReference pageRef = Page.RelationshipTeam;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('eid', accTM.id);
            RelationshipTeamCtrl rtc = new RelationshipTeamCtrl();
                //try to save edited with null role
                rtc.lstMem[0].Role__c = '';
                rtc.eSave();
                rtc.lstMem[0].Role__c = 'Senior Client Advisor';
                rtc.eSave();
                rtc.lstMem[0].Role__c = 'Wealth Advisor';
                rtc.eSave();
                rtc.newSaveOk();
            Account accnt = [Select Senior_Client_Advisor__c from Account where id=:acc.id];
                system.assertEquals(accnt.Senior_Client_Advisor__c, u.LastName);

                rtc.lstMem[0].Role__c = 'Other Admin'; 
                rtc.eSave();
                //Add new Team Members mode
                ApexPages.currentPage().getParameters().put('eid','');
                ApexPages.currentPage().getParameters().put('aid',acc.id);
                //add new with blank role
                rtc.initNew();  
                rtc.lstWrap[0].accTm.User__c = u.id;
                rtc.newSave();
                //add new with more than one Senior Client Advisor
                rtc.initNew();  
                rtc.lstWrap[0].accTm.User__c = u.id;
                rtc.lstWrap[0].accTm.Role__c ='Senior Client Advisor';
                rtc.lstWrap[1].accTm.User__c = u1.id;
                rtc.lstWrap[1].accTm.Role__c ='Senior Client Advisor';
                rtc.newSave();
                //add new with diffrent role
                rtc.lstWrap[0].accTm.User__c = u.id;
                rtc.lstWrap[0].accTm.Role__c ='Other Admin';
                rtc.lstWrap[1].accTm.User__c = u1.id;
                rtc.lstWrap[1].accTm.Role__c ='Other Admin'; 
                rtc.newSave();
            List<Relationship_Team_Member__c>    lstTmMem = [Select id from Relationship_Team_Member__c where Relationship__c=:acc.id];
                system.assertEquals(lstTmMem.size(), 3);
                rtc.closePopup();
                rtc.Cancel();
    }

}