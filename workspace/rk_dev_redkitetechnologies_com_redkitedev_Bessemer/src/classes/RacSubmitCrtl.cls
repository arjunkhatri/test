/*
    
     Authors :  Jina Chieta, Don Koppel
     Created Date: 2014-07-20
     Last Modified: 2014-07-20
*/

public with sharing class RacSubmitCrtl {
	public string relId {get;set;}
	public String finalRelvalStr = '';
	public String finalConValStr = '';
	public String strVal = '';
	public String relNumber = '';
	public String racUrl {get;set;}
	public static final String MESSAGE = ' field(s) required.<br>';	
	public static final String PDMAKER = 'Primary Decision Maker';
	public static final String DMAKER = 'Decision Maker';


	public RacSubmitCrtl(ApexPages.Standardcontroller sc){
		relId = sc.getId();
	}

	public pagereference init(){ 
		validateRelationships();
		validateContacts();
		validateEntities();
		racUrl = '';
		if(finalRelvalStr + finalConValStr == ''){
			// validation for relationship and contact pass successfully
			GlobalSettings__c gs = GlobalSettings__c.getInstance();
			if(gs != null && gs.RAC_Tool_URL__c != null && gs.RAC_Tool_URL__c != ''){
				racUrl = 'http://'+gs.RAC_Tool_URL__c + '?Relationship='+relNumber;
			}
		}else{
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,finalRelvalStr+finalConValStr));
			return null;			
		}	
		PageReference pgRef = new PageReference ('/' + relId);
		pgRef.setRedirect(true);
    	return pgRef;
		return null;
	}

	public void validateRelationships(){ 
		String strRel;
		if(relId != null && relId != ''){
			Sobject relObj;
			String strFields = '';
			List<RAC_Relationship_Required_Fields__c> rcs = RAC_Relationship_Required_Fields__c.getall().values();
			if(rcs != null && rcs.size() > 0){
			 	for(RAC_Relationship_Required_Fields__c rr:rcs){
			 		strFields = strFields +rr.FieldApi__c+',';
			 	}
			}
			String query = 'SELECT Name,id,Relationship_Number__c';
			if(strFields !=null && strFields !=''){
			 	strFields = strFields.substring(0,strFields.length() - 1);
			 	query = query +','+ strFields;
			}
	        query += ' FROM Account WHERE id=\''+relId+'\'';
	        relObj =  Database.query(query);
			if(relObj != null){
				relNumber  = string.valueof(relObj.get('Relationship_Number__c'));
				strRel = '<a href="/'+relObj.get('Id')+'">'+relObj.get('Name')+': </a>';
				finalRelvalStr = strRel;	         	
			 	for(RAC_Relationship_Required_Fields__c rf:rcs){
			 		if(relObj.get(rf.FieldApi__c) == null || relObj.get(rf.FieldApi__c) ==''){
			 			finalRelvalStr = finalRelvalStr + ' '+rf.Name +',';
			 		}
			 	}
			}		
			if(finalRelvalStr != strRel){
				finalRelvalStr = finalRelvalStr.substring(0, finalRelvalStr.length() - 1);
				finalRelvalStr = finalRelvalStr + MESSAGE;
			}else{
				finalRelvalStr = '';
			}
		}
	}
	
	public void validateContacts(){
		List<String> lstConIds = new List<String>();
		List<String> strConVal = new List<String>();
		List<Sobject> conObj = new List<Sobject>();
		String cFields = '';
		String innerFields = '';
		RecordType rType = [Select Name, Id From RecordType where name='R:C' limit 1]; 
		if(rType != null){
			//collect all contact ids related to relationship
			for(Connections__c con: [Select Role_on_Relationship__c, Relationship_R1__c, RecordTypeId,
		 							Contact_C1__c From Connections__c where RecordTypeId=:rType.id and 
		 							(Role_on_Relationship__c =: PDMAKER or 
		 							Role_on_Relationship__c =: DMAKER) and 
		 							Relationship_R1__c =:relId]){
		 				lstConIds.add(con.Contact_C1__c);	
		 	}
		}
		if(lstConIds != null && lstConIds.size() > 0){
			List<RAC_Contact_Required_Fields__c> crf = RAC_Contact_Required_Fields__c.getall().values();
			if(crf != null && crf .size() > 0){
				for(RAC_Contact_Required_Fields__c cf:crf){
					if(!cf.FieldApi__c.contains('Connection__r.')){
						cFields = cFields + cf.FieldApi__c+',';
					}else
					{// Fields related to connections
						innerFields = innerFields + cf.FieldApi__c.replace('Connection__r.','')+',';
					}
				}
			}
			
			String cQuery = 'SELECT id,Full_Name__c,';
			if(innerFields != ''){
				innerFields = innerFields.substring(0,innerFields.length() - 1);
				String innerQuery = '(Select '+ innerFields +' From Connections_C1__r)';
				cQuery = cQuery + innerQuery;	
			}
			
			if(cFields != null && cFields !=''){
			 	cFields = cFields.substring(0,cFields.length() - 1);
			 	cQuery = cQuery +','+ cFields;
			}
	        cQuery += ' FROM Contact WHERE id in: lstConIds';
	        conObj =  Database.query(cQuery);
	        if(conObj != null && conObj.size() > 0){
		        for(Sobject cObj:conObj){
					strVal = '';					
					String strCon = '<a href="/'+cObj.get('Id')+'">'+cObj.get('Full_Name__c')+': </a>';
					strVal = strCon;		         
		     		for(RAC_Contact_Required_Fields__c conf:crf){
		     			if(!conf.FieldApi__c.contains('Connection__r.')){
			     			if(cObj.get(conf.FieldApi__c) == null || cObj.get(conf.FieldApi__c) ==''){
			     				strVal = strVal +' '+conf.Name+',';
			     			}
		     			}else
		     			{
		     				String fld = conf.FieldApi__c.replace('Connection__r.','');
				     		for(SObject  conn:cObj.getSObjects('Connections_C1__r')){
				     			if(conn.get(fld) == null || conn.get(fld) ==''){
				     				if(!strVal.contains(conf.Name)){
				     					strVal = strVal +' '+conf.Name+',';
				     				}
				     			}
				     		}		     			
		     			}
		     		}
					if(strVal != strCon){
						strVal = strVal.substring(0, strVal.length() - 1);
						strVal = strVal + MESSAGE;
						strConVal.add(strVal);
					}		     		
		         }
	         }	
		}
		if(strConVal != null && strConVal.size() > 0){
			for(String str:strConVal){
				finalConValStr = finalConValStr + str;
			}
		}
	}
	
	public void validateEntities(){
		List<String> lstConIds = new List<String>();
		List<String> strConVal = new List<String>();
		List<Sobject> conObj = new List<Sobject>();
		String cFields = '';
		String innerFields = '';
		RecordType rTypeConnection = [Select Name, Id From RecordType where name='R:C' limit 1]; 
		RecordType rTypeEntityContact = [Select Name, Id From RecordType where DeveloperName ='Entity_Contact' limit 1]; 
		if(rTypeConnection != null && rTypeEntityContact != null ){
			//collect all contact ids for entities related to relationship
			for(Connections__c con: [SELECT	Role_on_Relationship__c, Relationship_R1__c, RecordTypeId,
		 										Contact_C1__c 
		 							FROM 	Connections__c 
		 							WHERE	RecordTypeId=:rTypeConnection.id
		 							AND		Contact_C1__r.RecordTypeId = :rTypeEntityContact.Id 
		 							AND		Relationship_R1__c =:relId]){
		 				lstConIds.add(con.Contact_C1__c);	
		 	}
		}
		if(lstConIds != null && lstConIds.size() > 0){
			List<RAC_Entity_Required_Fields__c> crf = RAC_Entity_Required_Fields__c.getall().values();
			if(crf != null && crf .size() > 0){
				for(RAC_Entity_Required_Fields__c cf:crf){
					if(!cf.FieldApi__c.contains('Connection__r.')){
						cFields = cFields + cf.FieldApi__c+',';
					}else
					{// Fields related to connections
						innerFields = innerFields + cf.FieldApi__c.replace('Connection__r.','')+',';
					}
				}
			}
			
			String cQuery = 'SELECT id,Full_Name__c,';
			
			if(cFields != null && cFields !=''){
			 	cFields = cFields.substring(0,cFields.length() - 1);
			 	cQuery = cQuery + cFields;
			}
	        cQuery += ' FROM Contact WHERE id in: lstConIds';
	        
	        system.debug(cQuery);
	        
	        conObj =  Database.query(cQuery);
	        if(conObj != null && conObj.size() > 0){
		        for(Sobject cObj:conObj){
					strVal = '';					
					String strCon = '<a href="/'+cObj.get('Id')+'">'+cObj.get('Full_Name__c')+': </a>';
					strVal = strCon;		         
		     		for(RAC_Entity_Required_Fields__c conf:crf){
		     			if(!conf.FieldApi__c.contains('Connection__r.')){
			     			if(cObj.get(conf.FieldApi__c) == null || cObj.get(conf.FieldApi__c) ==''){
			     				strVal = strVal +' '+conf.Name+',';
			     			}
		     			}else
		     			{
		     				String fld = conf.FieldApi__c.replace('Connection__r.','');
				     		for(SObject  conn:cObj.getSObjects('Connections_C1__r')){
				     			if(conn.get(fld) == null || conn.get(fld) ==''){
				     				if(!strVal.contains(conf.Name)){
				     					strVal = strVal +' '+conf.Name+',';
				     				}
				     			}
				     		}		     			
		     			}
		     		}
					if(strVal != strCon){
						strVal = strVal.substring(0, strVal.length() - 1);
						strVal = strVal + MESSAGE;
						strConVal.add(strVal);
					}		     		
		         }
	         }	
		}
		if(strConVal != null && strConVal.size() > 0){
			for(String str:strConVal){
				finalConValStr = finalConValStr + str;
			}
		}
	}
}