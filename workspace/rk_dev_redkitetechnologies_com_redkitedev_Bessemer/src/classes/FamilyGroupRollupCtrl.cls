global without sharing class FamilyGroupRollupCtrl 
{
	public List< AccountResultSet> accountSearchResults { get; set; }

	public String familyGroupID { get; set; }
	public Family_Group__c familyGroupObj { get; set; }
	public String accountSortField { get; set; }
	public String accountSortOrder { get; set; }  
	public static String acctSortField { get; set; }
	public static String acctSortOrder { get; set; }
	public  Boolean showMore { get; set; }
	public String platForm { get; set; }
	private ApexPages.StandardSetController setAccountList;
	private Integer pageSize = 10;

	public FamilyGroupRollupCtrl( ApexPages.StandardController controller )
	{
		familyGroupID = controller.getId();
		familyGroupObj = ( Family_Group__c )controller.getRecord();
		if ( ApexPages.currentPage().getParameters().get( 'showMore' ) == 'true' )
		{
			showMore = true;
			pageSize = 50;
		}
		else
			showMore = false;

		GlobalContactSearch gcs = new GlobalContactSearch();
		platForm = gcs.checkPlatform();
	}

	public void init()
	{
		accountSortField = 'Name';
		accountSortOrder = 'asc';
		setAccountList = new ApexPages.StandardSetController ( [ SELECT Id FROM Account WHERE Family_Group__c = :familyGroupID ORDER BY Name ] );
		setAccountList.setpageNumber(1);
		setAccountList.setPageSize(pageSize);
		getData();
	}

	private void getData()
	{
		Set<Id> accountSet = new Set<Id>();
		List<Account> acctList = setAccountList.getRecords();
		for ( Account a : acctList )
			accountSet.add( a.Id );

		List<Account> accountList = [ SELECT Id,  Name, Relationship_Number__c, Primary_Decision_Maker__r.Name, Primary_Decision_Maker_Name__c, Senior_Client_Advisor__c, Estimated_Annual_Fees__c, Market_Value__c, Owner.Name
									  FROM Account
									  WHERE Family_Group__c = :familyGroupID AND Id IN :accountSet ORDER BY Name ];

		Set<Id> accountIds = new Set<Id>();
		for( Account a : accountList )
			accountIds.add( a.Id );

		Set<Id> accountVisibleSet = GlobalContactSearchSharing.accountOwnersRecords( accountIds );
		List< AccountResultSet> results = new List< AccountResultSet>();
		for( Account a : accountList )
		{
			if( accountVisibleSet.contains( a.Id ) )
				results.add(new AccountResultSet( true, a, 'A_' + a.Name ) ) ;
			else
				results.add(new AccountResultSet( false, a, 'B_' + a.Name ) ) ;
		}
		acctSortField = accountSortField;
		acctSortOrder = accountSortOrder;

		results.sort();
		accountSearchResults = results;
	}

	public List<String> getAccountFields()
	{
		List<String> returnList = new List<String>();
		GlobalContactSearch gcs = new GlobalContactSearch();

		List<FGContactRollupFields__c> fieldList = [ SELECT Field__c FROM FGContactRollupFields__c WHERE Platform__c = :gcs.checkPlatform() AND Object__c = 'Account' ORDER BY SortOrder__c ];
		for( FGContactRollupFields__c fld : fieldList )
			returnList.add( fld.Field__c );

		return returnList;
	} 

	/**** Paging Methods ***/
	//return wheteher previous page exists
	public Boolean getHasPrevious()
	{
		return this.setAccountList.getHasPrevious();
	}

	//   return whether next page exists
	public Boolean getHasNext()
	{
		return this.setAccountList.getHasNext();
	}

	//   advance to next page
	public void doNext()
	{
		if( this.setAccountList.getHasNext() )
			this.setAccountList.next();

		getData();
	}

	public void doLast()
	{
		if( this.setAccountList.getHasNext() )
			this.setAccountList.last();

		getData();
	}

	public void doFirst()
	{
		if( this.setAccountList.getHasPrevious() )
			this.setAccountList.first();

		getData();
	}
	//advance to previous page
	public void doPrevious()
	{
		if( this.setAccountList.getHasPrevious() )
			this.setAccountList.previous();

		getData();
	}

	//return page number
	public Integer getPageNumber()
	{
		return this.setAccountList.getPageNumber(); 
	}

	//return total pages
	public Integer getTotalPages()
	{
		Decimal totalSize = this.setAccountList.getResultSize();
		Decimal pageSize = this.setAccountList.getPageSize();
		Decimal pages = totalSize / pageSize;
		return (Integer)pages.round( System.RoundingMode.CEILING );
	}

	//return page number
	public Integer getTotalRecords()
	{
		return this.setAccountList.getResultSize();
	}
	
	@RemoteAction
	global static String sendRequestEmail( String requestAccessId, Boolean requestOwnership, String requestMessage )
	{
		return RequestAccessHelper.SendRequestEmail( requestAccessId, UserInfo.getUserId(), requestOwnership, requestMessage );
	}

	public class AccountResultSet implements Comparable 
	{
		public boolean accountVisibility { get; set; }
		public Account account { get; set; }
		public String SortField { get; set; }

		public AccountResultSet( Boolean acctVis, Account a, String srtField )
		{
			accountVisibility = acctVis;
			account = a;
			SortField = srtField;
		}

		public Integer compareTo( Object compareTo )
		{
			AccountResultSet dataSet = (AccountResultSet)compareTo;
			if( Sortfield > DataSet.Sortfield )  
				return acctSortOrder.equals('asc') ? 1 : 0;
			else
				return acctSortOrder.equals('asc') ? 0 : 1;
		}
	}
}