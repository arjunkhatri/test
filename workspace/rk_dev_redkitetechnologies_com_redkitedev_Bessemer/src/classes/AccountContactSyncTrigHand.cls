/*
    
     Authors :  Don Koppel
     Created Date: 2014-08-20
     Last Modified: 2014-08-20
*/

public with sharing class AccountContactSyncTrigHand {
    // template methods
    public void onBeforeInsert(list<Account_Contact_Sync__c> newList) {
		AddNewAccountContacts(newList);
    }
    public void onAfterInsert(list<Account_Contact_Sync__c> newList) {
    	updateDataSyncLog(newList, 'I');
		updateAccountContactRole(newList);
    }
    public void onBeforeUpdate(list<Account_Contact_Sync__c> newList, map<id,Account_Contact_Sync__c> oldMap) {
		AddNewAccountContacts(newList);
    }
    public void onAfterUpdate(list<Account_Contact_Sync__c> newList, map<id,Account_Contact_Sync__c> oldMap) {
    	updateDataSyncLog(newList, 'U');
		updateAccountContactRole(newList);
    }
    public void onAfterDelete(list<Account_Contact_Sync__c> oldList){
    	updateDataSyncLog(oldList, 'D');
		updateAccountContactRole(oldList);
    }

// business logics

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs Account_Contact_Sync EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Account_Contact_Sync__c> recordList, String operation) {
    	
    	//records should only be added to the data sync log if the current user is the Bessemer Integration user
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
    	
	    	list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
	 		for (Account_Contact_Sync__c acs : recordList){
	 			Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
	 			syncRecord.EDB_Entity_Id__c = acs.EDB_Entity_Id__c;
	 			syncRecord.Entity__c = 'CUST_LGL_ENTITY_OPTN';
	 			syncRecord.Action__c = Operation;
	 			syncList.Add(syncRecord);
	 		}

			insert syncList;
    	}
    }       
    	
	public void AddNewAccountContacts(list<Account_Contact_Sync__c> acsList){

    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
				
			Map<Id, Set<Account_Contact_Sync__c>> acsMap = new Map<Id, Set<Account_Contact_Sync__c>>(); //map of Account Contact Sync records that were inserted or updated
			Map<String, Id> acMap =  new Map<String, Id>(); //map of parent Account Contact record keys and Ids
			Set<String> acSet = new Set<String>(); //Set of ex
			List<Account_Contact__c> acNewList = new List<Account_Contact__c>(); //list of new Account_Contact__c records to create
			List<Account_Contact__c> acExistingList = new List<Account_Contact__c>(); //list of existing Account_Contact__c records to update (match on Account # + Contact S #)
			List<Account_Contact_Sync__c> acsNewList = new List<Account_Contact_Sync__c>(); //list of Account_Contact_Sync__c records where Acount_Contact__c is null
			List<Id> accountList = new List<Id>(); //list of Account__c.Id values of the Account Contact Sync records that were inserted or updated; used to identify parent records

//			String acKey;  //will be set to Account__c + Contact__c to create a unique key for each Account_Contact__c

			for (Account_Contact_Sync__c acs : acsList){
//				if(!accountList.contains(acs.Account__c)){
					accountList.add(acs.Account__c);
//				}
			}			

			acExistingList = [SELECT Id, SFDC_Account_Contact_Key__c FROM Account_Contact__c WHERE Account__c in :accountList];

			//Loop through existing Account_Contact__c records; find matches with new/updated Account_Contact_Sync records using Account # + Contact S #
			for (Account_Contact_Sync__c acs : acsList){

				Boolean bFound = false;

				for(Account_Contact__c ac : acExistingList){
					
					system.debug('acs SFDC Key = ' + acs.SFDC_Account_Contact_Key__c + ' ; ac SFDC key = ' + ac.SFDC_Account_Contact_Key__c);
					if (ac.SFDC_Account_Contact_Key__c == acs.SFDC_Account_Contact_Key__c){

						bFound = true;
						acs.Account_Contact__c = ac.Id;
						
						//Check whether this Account_Contact__c has already been added to avoid dupes
						if(!acSet.contains(acs.SFDC_Account_Contact_Key__c)){
							acSet.add(acs.SFDC_Account_Contact_Key__c);
						}	
					}
				}

				//If not found, add it to the list to be inserted
				if (!bFound && !acSet.contains(acs.SFDC_Account_Contact_Key__c)) {
					acNewList.add(new Account_Contact__c(Account__c = acs.Account__c, Contact__c = acs.Contact__c, SFDC_Account_Contact_Key__c = acs.SFDC_Account_Contact_Key__c) );					
					acSet.add(acs.SFDC_Account_Contact_Key__c);
				}
			}	
/*
			//Loop through Account_Contact_Sync__c records; build a map of parent Account_Contact records
			//Any Account_Contact__c records that do not exist will need to be created	
			for (Account_Contact_Sync__c acs : acsList){
				//If acs.Account_Contact__c is populated, add to the acs map
				if(acs.Account_Contact__c != null){
					if(!acMap.containsKey(acs.Account_Contact__c)){
						acKey = '' + acs.Account__c + acs.Contact__c;
						acMap.put(acKey, acs.Account_Contact__c);
					}	
				}
				//If acs.Account_Contact__c is not populated, add to the list of ac records to be created 
				else{
					//Check whether this Account_Contact__c has already been added to avoid dupes
					Boolean bFound = false;
					for(Account_Contact__c ac : acNewList){
						if (ac.Account__c == acs.Account__c && ac.Contact__c == acs.Contact__c){
							bFound = true;
							
						}
					}

					//If not found, add it to the list to be inserted
					if (!bFound) {
						acNewList.add(new Account_Contact__c(Account__c = acs.Account__c, Contact__c = acs.Contact__c) );
					}
				} 
			}	
*/			
			//If any Account_Contact records need to be created, insert them and add to the map
			if (acNewList.size() > 0) {
system.debug(acNewList);
				Database.SaveResult[] srList = Database.insert(acNewList);

				for(Integer i = 0; i < acNewList.size(); i++){
//					acKey = '' + acNewList[i].Account__c + acNewList[i].Contact__c;
//					acMap.put(acNewList[i].SFDC_Account_Contact_Key__c, srList[i].getId());
					
					//Populate Account_Contact__c on Account_Contact_Sync__c records with matching keys
					for(Account_Contact_Sync__c acs : acsList){
						if (acs.SFDC_Account_Contact_Key__c == acNewList[i].SFDC_Account_Contact_Key__c){
							acs.Account_Contact__c = srList[i].getId();
						}
					}
				}				
			}
		}
	}
    	
	public void updateAccountContactRole(list<Account_Contact_Sync__c> acsList){

    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
				
			Map<Id, Set<Account_Contact_Sync__c>> acsMap = new Map<Id, Set<Account_Contact_Sync__c>>(); //map of Account Contact Sync records that were inserted or updated
			Map<String, Id> acMap =  new Map<String, Id>(); //map of parent Account Contact record keys and Ids
			List<Account_Contact__c> acNewList = new List<Account_Contact__c>(); //list of new Account_Contact__c records to create
			List<Account_Contact_Sync__c> acsNewList = new List<Account_Contact_Sync__c>(); //list of Account_Contact_Sync__c records where Acount_Contact__c is nul

			String acKey;  //will be set to Account__c + Contact__c to create a unique key for each Account_Contact__c

			//Loop through Account_Contact_Sync__c records; build a map of parent Account_Contact records
			//Any Account_Contact__c records that do not exist will need to be created	
			for (Account_Contact_Sync__c acs : acsList){
				//If acs.Account_Contact__c is populated, add to the acs map
				if(acs.Account_Contact__c != null){
					if(!acMap.containsKey(acs.Account_Contact__c)){
						acKey = '' + acs.Account__c + acs.Contact__c;
						acMap.put(acKey, acs.Account_Contact__c);
					}	
				}
			}	

			//We now have a list of Account_Contact__c records where Role__c needs to be populated
			List<Account_Contact_Sync__c> acsListWithRole = [SELECT Role__c, Account_Contact__c 
														FROM  Account_Contact_Sync__c
														WHERE Account_Contact__c in :acMap.values() ];		
			List<Account_Contact__c> acListForUpdate = [SELECT Id, Role__c
														FROM   Account_Contact__c
														WHERE  Id IN :acMap.values()];			

			Map<Id, String> acRoleMap = new Map<Id, String>();  //This map will be used to construct the new role list for each Account_Contact__c
			String acRole; //Temp variable used to concatenate the list of Roles for an Account Contact
			
			for (Account_Contact_Sync__c acs : acsListWithRole){
				//If Account_Contact__c is null, find the newly created parent; update Account_Contact__c and add to the map
				if(!acRoleMap.containsKey(acs.Account_Contact__c)){
					acRoleMap.put(acs.Account_Contact__c, acs.Role__c);
				}	
				else{
					acRole = acRoleMap.get(acs.Account_Contact__c);
					acRoleMap.put(acs.Account_Contact__c, acs.Role__c + ';' + acRole);
				}
			}
			
			//Loop through the Account_Contact list for update, and udpdate Role__c values
			for (Account_Contact__c ac : acListForUpdate) {
				if (acRoleMap.containsKey(ac.Id)){
					ac.Role__c = acRoleMap.get(ac.Id);
				}
				else{
					ac.Role__c = '';
				}
			}
			update acListForUpdate;
				
		}
	}
}