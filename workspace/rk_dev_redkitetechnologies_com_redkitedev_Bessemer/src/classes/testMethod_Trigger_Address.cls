/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testMethod_Trigger_Address {

    static testMethod void testMethod_Trigger_Address_InsertStatement() 
    {
		AddressCustomzationConfig__c custSettings = AddressCustomzationConfig__c.getOrgDefaults();
		custSettings.TotalAddressRecords__c = 2;
		insert custSettings ;
		
		Contact ct = new Contact ();
		ct.FirstName = 'Test';
		ct.LastName = 'User';
		ct.MailingStreet = '1 test ave';
		insert ct;
		
		Test.startTest();
		Addresses__c address = new Addresses__c ();
		address.Contact__c = ct.id;
		address.Street_Address__c = '2 test ave';
		insert address;
		
		//Attempot to insert a second addresss.  Should result in a DML exeception
		string errorMsg ='';
		try 
		{
			address = new Addresses__c ();
			address.Contact__c = ct.id;
			address.Street_Address__c = '3 test ave';
			insert address;
		}
		catch (DmlException ex)
		{
			errorMsg = ex.getMessage();
		}
		List<Addresses__c> addressList = [select id from Addresses__c where Contact__c = :ct.id];
		System.assertEquals(2, addressList.size() );
		
		address = addressList[1];
		address.Primary_Address__c = true;
		update address;
		
		Test.stopTest();
		
		
    }
}