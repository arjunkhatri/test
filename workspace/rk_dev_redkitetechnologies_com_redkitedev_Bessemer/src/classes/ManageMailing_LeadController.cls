public with sharing class ManageMailing_LeadController {
    public Lead ld;

    public List<MailingTableEntry> tableEntries {get;set;}
    public List<AddrInfo> addrEntries { get; set; }

    public Boolean massEditAddr {get; set; }
    public Boolean mainMode{get;set;}
    public String selectedAddrValue {get;set;}

   
    public ManageMailing_LeadController(ApexPages.StandardController controller)
    {
        ld = (Lead)controller.getRecord();
        massEditAddr=false;
        mainMode = true;
        tableEntries = new List<MailingTableEntry>();
    }

    
    public void getMailingRecs(){
        tableEntries.clear();

        //obtain mailing records related to this contact

        List<Mailing__c> tmpMailingList = [Select Id, Name, Mailing_Number__c, Mailings__c, Salutation__c, Type__c, Delivery_Method__c, Details__c, Mailing_Name__c,
                                        Contact__c, Contact__r.Name, Lead__c,
                                        SubAccountId__c, SubAccountId__r.Name,
                                        Address__c, Address__r.Address_Type__c, Address__r.Street_Address__c, Address__r.City__c, Address__r.State__c, Address__r.Postal_Code__c
                                        from Mailing__c where lead__c=:ld.Id order by createddate desc];
        
        List<Mailing__c> mailingList = CheckRecordVisibility (tmpMailingList ) ;
        if(!mailingList.isEmpty())
        {
            Map<Id, MailingTableEntry> mailingMap = new Map<Id, MailingTableEntry>();

            for(Mailing__c mail: mailingList){  
                System.debug('Mailing ' + mail.Name);
                MailingTableEntry newTableEntry = new MailingTableEntry(mail);

                tableEntries.add(newTableEntry);
                mailingMap.put(mail.Id, newTableEntry);
            }

            for(Mailing_Link__c ml: [Select Id, Name, MailingId__c, 
                                    Appraisal_Freq__c, Basis_Override__c, Alt_Mailer__c, Cash_Advice__c, Cash_Statement_Freq__c, Cotrustee_Letter__c,Fee_Type__c,
                                    Income_Distribution__c, Proxy_Type__c, Security_Advice__c, Statement_Type__c, Tax_Report__c, 
                                    Investment_Group_Name__c, Investment_Group_Number__c,
                                    SubAccountId__c, SubAccountId__r.Name, SubAccountId__r.SubAccount_Title__c,
                                    Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Contact_S_Number__c,
                                    Family_Group__c, Family_Group__r.Name, Family_Group__r.Family_Group__c,
                                    RecordTypeId, RecordType.Name
                                    from Mailing_Link__c where MailingId__c IN: mailingMap.keySet()]){

                MailingTableEntry mailTableEntry = mailingMap.get(ml.MailingId__c);
                if (mailTableEntry!=null) mailTableEntry.addLink(ml);
            }
        }
    }
    
    private List<Mailing__c> CheckRecordVisibility ( List<Mailing__c> MailList)
    {
        List<Mailing__c> returnedMailList = new List<Mailing__c> ();

        set<string> contactSet = new set<string>() ;
        set<string> leadSet = new set<string>() ;

        for (Mailing__c mailing : MailList)
        {
            if (mailing.Contact__c != null)
                contactSet.add(mailing.Contact__c) ;
            if (mailing.lead__c != null && mailing.Contact__c == null)
                leadSet.add(mailing.lead__c) ;
        }
        
        map<Id, Contact> contactMap = new map<id, Contact> ([select id , UserRecordAccess.HasEditAccess from Contact where id in :contactSet]) ;
        map<Id, Lead> leadMap = new map<id, Lead> ([select id, UserRecordAccess.HasEditAccess from Lead where id in :leadSet]) ;
        for (Mailing__c mailing : MailList)
        {
            if (contactMap.containsKey(mailing.Contact__c) || leadMap.containsKey(mailing.Lead__c) )
                returnedMailList.add(mailing)  ;
        }

        return returnedMailList ;
    }

    public void setAddresses(){
        
       
        //based on selected contact
        List<Addresses__c> listAddresses = new List<Addresses__c>();
        if(ld!=null)
        {
            
            listAddresses = [Select Id, Name, Address_Type__c, Street_Address__c, City__c, State__c, Postal_Code__c, Country__c, Address_Label_Block__c 
                             from Addresses__c where lead__c =: ld.Id];
              
        }
        
        //add a new AddrInfo of radio button
        addrEntries =  new List<AddrInfo>();
        for(Addresses__c addr: listAddresses)
        {
           
            addrEntries.add(new AddrInfo(addr.Id, addr.Address_Label_Block__c, addr));
            System.debug('Adding ' + addr.Id);
        }
        massEditAddr = true;
    }
    
    public PageReference massEdit()
    {
        setAddresses();
        massEditAddr = true;
        mainMode = false;
        return null;
    }
    public PageReference massEditSave(){
        list<Mailing__c> updateMailings = new list<Mailing__c>();
        for(MailingTableEntry currTableEntry : tableEntries){
            if(currTableEntry.isSelected!=null)
                if (currTableEntry.isSelected){
                    updateMailings.add(currTableEntry.mailingRec);
                    currTableEntry.mailingRec.address__c = selectedAddrValue;
                }
        }
        if (updateMailings.size()>0) update updateMailings;
        getMailingRecs();
        return massEditCancel();
    }
    public PageReference massEditCancel()
    {
        massEditAddr = false;
        mainMode = true;
        return null;
    }
    
    /*public class TableEntry
    {
        public Mailing__c mailingRec{get;set;}
        public List<Mailing_Link__c> familyGroups{get;set;}
        public List<Mailing_Link__c> otherContacts{get;set;}
        public List<Mailing_Link__c> otherEntities{get;set;}
        public Boolean isSelected{get;set;}
        
        public TableEntry(Mailing__c rec){
            this.mailingRec = rec;
            familyGroups = new List<Mailing_Link__c>();
            otherContacts = new List<Mailing_Link__c>();
            otherEntities = new List<Mailing_Link__c>();
        }
        public void addLink(Mailing_Link__c inLink){
            if(inLink.RecordType.Name=='Family Group') familyGroups.add(inLink);
            else if(inLink.RecordType.Name=='Other Contact') otherContacts.add(inLink);
            else otherEntities.add(inLink);
        }

    }*/
    
     public class AddrInfo {
        public String Value { get; set; }
        public String Label { get; set; }
        public Addresses__c addr {get;set;}
        public Boolean isChecked { get; set; }
        
        public AddrInfo(String Value, String Label, Addresses__c address) {
            this.Value = Value;
            this.Label = Label;
            this.addr = address;
        } 
    }
}