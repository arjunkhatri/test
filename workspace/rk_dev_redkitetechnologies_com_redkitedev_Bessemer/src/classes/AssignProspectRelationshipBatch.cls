global class AssignProspectRelationshipBatch implements Database.Batchable<Contact> {
	public static final String PROSPECT = 'Prospect';
	public static final String RELRECTYPE = 'Prospective Relationship';
	public static final String CONRECTYPE = 'R:C';
	public static final String STATUS = 'Open';
	public List<Connections__c> lstConnections; 
	public List<Contact> lstContacts;
	public String relRec; 
	public String conRec; 

	public AssignProspectRelationshipBatch(){
		lstContacts = [select Id, Name, AccountId, LastName, OwnerId 
						from Contact
						where RecordType.Name = :PROSPECT 
						and AccountId = null
						LIMIT 50000];	
	}
	
    global Iterable<Contact> start(Database.BatchableContext BC){
    	return lstContacts;
    }	
    
    global void execute(Database.BatchableContext BC, List<Contact> scope){	
		lstConnections = new List<Connections__c>();
		List<Contact> updateContacts = new List<Contact>();
		Map<String,Account> mpConToNewAcc = new Map<String,Account>();
		Map<String,List<Account>> bNameToAcc = new Map<String,List<Account>>();
		//get required record type ids
		for(RecordType rt:[Select Name, Id , SobjectType From RecordType where 
							(Name = :RELRECTYPE and SobjectType = 'Account') or
							(Name = :CONRECTYPE and SobjectType = 'Connections__c')])
		{
			if(rt.Name == RELRECTYPE && rt.SobjectType == 'Account'){	
				relRec = String.valueOf(rt.id);
			}
			if(rt.Name == CONRECTYPE && rt.SobjectType == 'Connections__c'){
				conRec  = String.valueOf(rt.id);
			}
		}
//		for(Account objAcc:[Select id,Name from Account where RecordTypeId=:relRec order by CreatedDate DESC])
//		{
//			if(!bNameToAcc.containsKey(objAcc.Name)){
//				bNameToAcc.put(objAcc.Name ,new List<Account>{objAcc} );	
//			}else{
//				bNameToAcc.get(objAcc.Name).add(objAcc);
//			}
//		}
		for(Contact con:scope){ 
//			if(con.Business_Name__c != null && con.Business_Name__c != ''){
//				List<Account> lstAcc = bNameToAcc.get(con.Business_Name__c);
//				if(lstAcc != null && lstAcc.size() > 0){
//					con.AccountId = lstAcc[0].id;
//					updateContacts.add(con);
//					//create connection
//					createConnection(con.id,lstAcc[0].id);
//				}else{ 
					//create new account for Contact
					Account acc = new Account();
					acc.Name = 'The ' + con.LastName + ' Relationship';
					acc.RecordTypeId = relRec;
					acc.OwnerId = con.OwnerId;
					acc.Active_Inactive__c = true;
					acc.Status__c = STATUS;					
					mpConToNewAcc.put(con.Id,acc);
//				}
//			}
		}
		//insert new accounts
		if(!mpConToNewAcc.isEmpty()){
			insert mpConToNewAcc.values();
		}
		//fill map with business name vs new created accounts
		for(Contact con:scope){
			if(mpConToNewAcc.containsKey(con.Id)){
				con.AccountId = mpConToNewAcc.get(con.Id).id;
				updateContacts.add(con);
				createConnection(con.id,con.AccountId);
			}		
		}
		//insert new connections
		if(lstConnections != null && lstConnections.size() > 0){
			insert lstConnections;
		}
		//update contacts
		if(updateContacts != null && updateContacts.size() > 0){
			update updateContacts;
		}
	}
	global void finish(Database.BatchableContext BC){
	   	
	} 	
	public void createConnection(String conId,String accId){
		Connections__c connection = new Connections__c();
		connection.Relationship_R1__c = accId;
		connection.Contact_C1__c = conId;
		connection.Role_on_Relationship__c = 'Primary Decision Maker';
		connection.RecordTypeId = conRec;
		lstConnections.add(connection);	
	}

}