@isTest
private class TestManageMailing_SubAccountController {
	
	@isTest static void testMailingSubAccount() {
		GlobalSettings__c setting = TestDataFactory.createGlobalSetting();
		setting.Relationship_Number_Seq__c = 1201;
		setting.Integration_User_Name__c = userinfo.getUserName();
		insert setting;

		Account accnt = testdatafactory.createTestAccount();
			accnt.Relationship_Number__c = 'Rel002';
		insert accnt;
		
		Accounts__c acc = testdatafactory.customAccount(accnt.id);
			insert acc;
			
		SubAccounts__c subAcc = testdatafactory.createSubAccount('SA001', accnt.id);
		subAcc.Account__c = acc.id;
		insert subAcc;

        Contact con = TestDataFactory.createTestContact( accnt );
        con.Contact_Preference__c = 'Mail;Do Not Mail';
        insert con;
        
        Addresses__c testAdd = new Addresses__c();
        testAdd.Contact__c = con.Id;
        insert testAdd;

        Mailing__c testMailing = new Mailing__c();
        testMailing.Contact__c = con.Id;
        testMailing.Mailings__c = 'Mail';
        testMailing.Type__c = 'type';
        testMailing.Address__c = testAdd.Id;
        insert testMailing;

        Mailing_Link__c testMailLink = new Mailing_Link__c();
        testMailLink.MailingId__c = testMailing.Id;
        testMailLink.SubAccountId__c = subAcc.Id;
        insert testMailLink;

		Test.startTest();
			ApexPages.StandardController std = new ApexPages.StandardController( subAcc );
			ManageMailing_SubAccountController ctrl = new ManageMailing_SubAccountController( std );
			ctrl.getMailingRecs();
		Test.stopTest();
	}
}