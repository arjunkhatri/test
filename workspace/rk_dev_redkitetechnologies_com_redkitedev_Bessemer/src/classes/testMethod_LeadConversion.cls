/*
	
     Authors :  David Brandenburg
     Created Date: 2014-06-23
     Last Modified: 2014-06-23
     
     Purpose: Test Class for LeadConversion page. 
*/
@isTest
private class testMethod_LeadConversion 
{
	static testMethod void LeadConversion_Test_NoTask() 
    {
    	/********** Create test data *****************/
    	Lead ld =  (Lead)TestDataCreator_Utils.createSObject('Lead' , true , 1);
    	insert ld;
		//********************************/
		
    	PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
      	ApexPages.currentPage().getParameters().put('Id', ld.Id);
    	Test.startTest();
    	LeadConversionCtrl myCon = new LeadConversionCtrl ();
    	myCon.createTask = false;
    	myCon.createNew = true;
    	myCon.convert() ;
    	list<Account> acctCheck = [select id from Account where Name = :ld.Company];
    	System.assertEquals(1, acctCheck.size());
    	Test.stopTest();
    }
    static testMethod void LeadConversion_Test_WithTask() 
    {
    	/********** Create test data *****************/
    	Lead ld =  (Lead)TestDataCreator_Utils.createSObject('Lead' , true , 1);
    	insert ld;
    	Account acct =  (Account)TestDataCreator_Utils.createSObject('Account' , true , 1 );
    	insert acct;
		//********************************/
    	PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
      	ApexPages.currentPage().getParameters().put('Id', ld.Id);
    	Test.startTest();
    	LeadConversionCtrl myCon = new LeadConversionCtrl ();
    	myCon.createTask =true;
    	myCon.TaskObj.Subject = 'Other';
    	myCon.TaskObj.Priority = 'Normal';
    	myCon.TaskObj.Status = 'In Progress';
    	myCon.createNew = false;
    	myCon.AccountObj.AccountId = acct.id;
    	myCon.convert() ;
    	list<Task> tsk = [select id from Task where Whatid = :acct.id ];
    	System.assertEquals(1, tsk.size());
    	list<Contact> ct = [select id from Contact where AccountId = :acct.id and LastName = :ld.LastName];
    	System.assertEquals(1, ct.size());
    	Test.stopTest();
    }
    static testMethod void LeadConversion_Test_Cancel() 
    {
    	/********** Create test data *****************/
    	Lead ld =  (Lead)TestDataCreator_Utils.createSObject('Lead' , true , 1);
    	insert ld;
    	
		//********************************/
		
    	PageReference pageRef = Page.LeadConversion;
        Test.setCurrentPage(pageRef);
      	ApexPages.currentPage().getParameters().put('Id', ld.Id);
    	Test.startTest();
    	LeadConversionCtrl myCon = new LeadConversionCtrl ();
    	PageReference pgRef = myCon.Cancel() ;
    	System.assertEquals('/' + ld.id, pgRef.getUrl());
    	Test.stopTest();
    }
}