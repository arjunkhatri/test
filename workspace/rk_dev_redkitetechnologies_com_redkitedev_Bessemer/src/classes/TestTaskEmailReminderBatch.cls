@isTest
public with sharing class TestTaskEmailReminderBatch {
    static testMethod void TaskEmailReminderBatchTest(){
        system.runAs(new User(Id = UserInfo.getUserId())){
	        Account acc = TestDataFactory.createTestAccount();
	        	acc.Relationship_Number__c = 'Rel111';
	            insert acc;
	        AccountTeamMember atm = TestDataFactory.createATM(acc.id, userinfo.getUserId(), 'Associate CA');
	            insert atm;
	        Task tsk =  TestDataFactory.createTask(acc.id);
	            tsk.Email_Reminder__c = date.today();
	            tsk.Email_Reminder_Sent__c = false ;
	            tsk.ActivityDate = date.today();
	            //tsk.Email_Reminder_Days_before_Due_Date__c = 12;
	            insert tsk;
	        Task tsk1 = TestDataFactory.createTask(null);
	            insert tsk1;
	            test.startTest();
	        TaskEmailReminderBatch tBatch = new TaskEmailReminderBatch();
	            database.executeBatch(tBatch);
	            test.stopTest();
       }
    }
    
   	static testMethod void TaskEmailReminderBatchTest1(){
	    system.runAs(new User(Id = UserInfo.getUserId())){
	        Account acc = TestDataFactory.createTestAccount();
	        	acc.Relationship_Number__c = 'Rel112';
	            insert acc;
	        AccountTeamMember atm = TestDataFactory.createATM(acc.id, userinfo.getUserId(), 'Associate CA');
	            insert atm;
	        Task tsk =  TestDataFactory.createTask(acc.id);
	            tsk.whatId = null;
	            tsk.Email_Reminder__c = date.today();
	            tsk.Email_Reminder_Sent__c = false ;
	            tsk.ActivityDate = date.today();
	            //tsk.Email_Reminder_Days_before_Due_Date__c = 12;
	            insert tsk;
	        Task tsk1 = TestDataFactory.createTask(null);
	            insert tsk1;
	            test.startTest();
	        TaskEmailReminderBatch tBatch = new TaskEmailReminderBatch();
	            database.executeBatch(tBatch);
	            test.stopTest();
	   }
   }
}