/*
	
	 Authors :  David Brandenburg
	 Created Date: 2014-04-28
	 Last Modified: 2014-04-28
	 
	 Purpose: Controller for ConnectionMgmt_RelationshipConnections page
*/
public with sharing class ConnectionMgmt_RelationshipConnections extends PageControllerBase
{
	public Account accountObj { get; set; }
	public Connections__c connectionObjNewRC { get; set; }
	public Connections__c connectionObj { get; set; }
	public Connections__c connectionObjRel { get; set; }
	public List<Connections__c> contactList { get; set; }
	public List<FieldSet> fieldList { get; set; }
	public Contact contactObj { get; set; }
	public String selectedId { get; set; }
	public String actionType { get; set; }
	public String accountId { get; set; }
	public String contactRT { get; set; }
	public String hasError1 { get; set; }
	public String hasError2 { get; set; }
	public Boolean hasDuplicates { get; set; }
	public List<GlobalContactSearch.ContactResultSet> dupList { get; set; }
	public String accountTitle { get; set; }
	transient Boolean isOverride;
	public String filterString { get; set; }
	public Boolean isEntityContact { get; set; }
	public Map<String,List<LayoutFields> > contactFieldList { get; set; }
	public List<String> contactSection { get; set; }
	public Contact contactRTSelect { get; set; }
	public String columnsToDisplay { get; set; }
	public Addresses__c AddressObj {get;set;}

	private Map<String, SObjectField> accountFields;
	private Map<String, SObjectField> contactFields;
	private Map<String, String> labelCache;
	private Boolean hasPageErrors = false;
	private static final String PRIMARY_DECISION_MAKER = 'Primary Decision Maker';

	public ConnectionMgmt_RelationshipConnections( ApexPages.Standardcontroller controller )
	{
		if( controller.getId() == null )
			accountId = ApexPages.currentPage().getParameters().get( 'accountid' );
		else
		{
			Connections__c connection = [ SELECT Id, RecordType.Name, Relationship_R1__c, Contact_C1__c FROM Connections__c WHERE Id = :controller.getId() ];
			if( connection.RecordType.Name == 'R:R' )
				accountId = connection.Relationship_R1__c;
			if( connection.RecordType.Name == 'R:C' )
				accountId = connection.Relationship_R1__c;
			if( connection.RecordType.Name == 'C:C' )
				accountId = connection.Contact_C1__c;
		}

		contactObj = new Contact();
		AddressObj = new Addresses__c ();
		Account acctRT = [ SELECT RecordType.Name FROM Account WHERE Id = :accountId ];
		//if(String.Valueof(acctRT.RecordType.Name).contains('Business') )
		//	contactRT = [ SELECT id, Name FROM RecordType WHERE sObjectType= 'Contact' and Name = 'Professional/Intermediary'].Id;
		//if(String.Valueof(acctRT.RecordType.Name).contains('Client') )
		//	contactRT = [ SELECT id, Name FROM RecordType WHERE sObjectType= 'Contact' and Name = 'Client Contact'].Id;
		//if(String.Valueof(acctRT.RecordType.Name).contains('Prospect') )

		contactRT = getRecordTypeId( 'Contact', 'Prospect' );
		contactObj.RecordTypeid = contactRT;
		contactObj.AccountId = accountId;
		contactObj.OwnerId = UserInfo.getUserId();
		isEntityContact = false;

		processContactSection();
		processContactFields();
		contactRTSelect = new Contact();
		contactRTSelect.RecordTypeid = contactRT;
	}

	public ConnectionMgmt_RelationshipConnections()
	{
		accountId = ApexPages.currentPage().getParameters().get( 'id' );
	}

	public List<ConnectionNewContactFields__c> getConnectionNewContactFields()
	{
		return [ SELECT Name, Column1__c, Column2__c, Order__c FROM ConnectionNewContactFields__c WHERE EntityType__c = :isEntityContact ORDER BY Order__c ];
	}

	public DupeBlockerListCtrl myComponentController { get; set; }
	public override void setComponentController( ComponentControllerBase compController )
	{
		myComponentController = ( DupeBlockerListCtrl )compController;
	}
	public override ComponentControllerBase getMyComponentController()
	{
		return myComponentController;
	}

	public void contactTypeChange()
	{
		contactObj = new Contact();
		Account acctRT = [ SELECT RecordType.Name FROM Account WHERE Id = :accountId ];

		contactObj.AccountId = accountId;
		contactObj.OwnerId = UserInfo.getUserId();
		contactObj.RecordTypeId = contactRTSelect.RecordTypeId;
		actionType = 'NewContact';
		processContactSection();
		processContactFields();
	}

	public String getPanelVisibility()
	{
		if( actionType == 'Contact' )
			return 'ContactPanel';
		if( actionType == 'Relationship' )
			return 'RelationshipPanel';
		if( actionType == 'NewContact' )
			return 'NewContactPanel';

		//Default Value
		return 'ContactPanel';
	}

	public PageReference init()
	{
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();
		if( platForm == 'Phone' )
			columnsToDisplay = '1';
		else
			columnsToDisplay = '2';

		if( accountId.subString(0,3) == '003' )
		{
			PageReference pageRef = Page.ConnectionMgmt_ContactConnections;
			pageRef.getParameters().put( 'id', accountId );
			pageRef.setRedirect(true);
			return pageRef;
		}

		actionType = 'NewContact';
		connectionObj = new Connections__c();
		connectionObj.RecordTypeId = [ SELECT id FROM RecordType WHERE SobjectType = 'Connections__c' and Name = 'R:C' ].Id;
		connectionObjNewRC = new Connections__c();
		connectionObjNewRC.RecordTypeId = [ SELECT id FROM RecordType WHERE SobjectType = 'Connections__c' and Name = 'R:C' ].Id;
		connectionObjRel = new Connections__c();
		connectionObjRel.RecordTypeId = [ SELECT id FROM RecordType WHERE SobjectType = 'Connections__c' and Name = 'R:R' ].Id;

		hasError1 = '0';
		hasError2 = '0';
		filterString = 'All';
		labelCache = new Map<String, String> ();

		accountObj = [ SELECT Name, Owner.Name, Senior_Client_Advisor__c, Market_Value__c, Family_Group__c, Bessemer_Office__c, Type, Estimated_Annual_Fees__c FROM Account WHERE ID = :accountId ];
		accountTitle = [ SELECT Name FROM Account WHERE Id = :accountId ].Name;
		createContactList();
		isOverride = false;
		return null;
	}

	private Pagereference returnUrl()
	{
		PageReference pageRef = new PageReference( '/' + accountId );
		pageRef.setRedirect(true);
		return pageRef;
	}

	public void filterList()
	{
		createContactList();
	}

	private void createContactList()
	{
		fieldList = new List<FieldSet>();
		List<Connections__c> tmpConnectionList;
		if( filterString == 'All' )
		{
			String sqlFields =  buildFieldList( 'Default' );
			tmpConnectionList = Database.query( 'SELECT Id, RecordType.Name, Contact_C1__r.AccountId, Contact_C1__r.Name, Relationship_R1__c, Relationship_R1__r.Name, Relationship2__r.Name, Role_on_Relationship__c ' + sqlFields + ' FROM Connections__c WHERE Relationship_R1__c = \'' + accountId + '\' ORDER BY Relationship2__r.Name, Contact_C1__r.LastName, Contact_C1__r.FirstName' );
			//ContactList = [ SELECT id, Contact_C1__r.Name,Relationship2__r.Name, Relationship2__r.BillingCity, Contact_C1__r.MailingCity, Contact_C1__r.Email, Contact_C1__r.MobilePhone , Role_on_Relationship__c
			//  FROM Connections__c WHERE Relationship_R1__c = :AccountId ];
		}
		if( filterString == 'ClientContact' )
		{
			String sqlFields =  buildFieldList( 'AllContacts' );
			tmpConnectionList = Database.query( 'SELECT Id, RecordType.Name, Contact_C1__r.AccountId, Contact_C1__r.Name, Relationship2__r.Name, Role_on_Relationship__c, Contact_C2__c ' + sqlFields + ' FROM Connections__c WHERE Relationship_R1__c = \'' + accountId + '\' AND (Contact_C1__r.RecordType.Name = \'Client Contact\' OR Contact_C1__r.RecordType.Name = \'Prospect\') ORDER BY Relationship2__r.Name, Contact_C1__r.LastName, Contact_C1__r.FirstName' );
			
		}
		if( filterString == 'NonClientContact' )
		{
			String sqlFields =  buildFieldList( 'NonClientContact' );
			tmpConnectionList = Database.query( 'SELECT Id, RecordType.Name, Contact_C1__r.AccountId, Contact_C1__r.Name, Relationship2__r.Name, Role_on_Relationship__c, Contact_C2__c ' + sqlFields + ' FROM Connections__c WHERE Relationship_R1__c = \'' + accountId + '\' AND Contact_C1__r.RecordType.Name = \'Non-Client Contact\' ORDER BY Relationship2__r.Name, Contact_C1__r.LastName, Contact_C1__r.FirstName' );
			
		}
		if( filterString == 'ClientRelationships' )
		{
			String sqlFields =  buildFieldList( 'AllRelationships' );
			tmpConnectionList = Database.query( 'SELECT Id, RecordType.Name, Contact_C1__r.Name, Relationship2__r.Name, Role_on_Relationship__c ' + sqlFields + ' FROM Connections__c WHERE Relationship_R1__c = \'' + accountId + '\' AND (Relationship2__r.RecordType.Name = \'Client Relationship\' OR Relationship2__r.Name = \'Prospective Relationship\') ORDER BY Relationship2__r.Name, Contact_C1__r.LastName , Contact_C1__r.FirstName' );
		}
		 if( filterString == 'NonClientRelationships' )
		{
			String sqlFields =  buildFieldList( 'AllRelationships' );
			tmpConnectionList = Database.query( 'SELECT Id, RecordType.Name, Contact_C1__r.Name, Relationship2__r.Name, Role_on_Relationship__c ' + sqlFields + ' FROM Connections__c WHERE Relationship_R1__c = \'' + accountId + '\' AND Relationship2__r.Recordtype.Name = \'Business\' ORDER BY Relationship2__r.Name, Contact_C1__r.LastName, Contact_C1__r.FirstName' );
		}
		if( filterString == 'Entity' )
		{
			String sqlFields =  buildFieldList( 'AllContacts' );
			tmpConnectionList = Database.query( 'SELECT Id, RecordType.Name, Contact_C1__r.Name, Relationship2__r.Name, Role_on_Relationship__c, Contact_C2__c  ' + sqlFields + ' FROM Connections__c WHERE Relationship_R1__c = \'' + accountId + '\' AND Contact_C1__r.RecordType.Name = \'Entity\' ORDER BY Relationship2__r.Name, Contact_C1__r.LastName, Contact_C1__r.FirstName' );
		}

		//Check to make sure the user has read access 
		List<Id> searchedIds = new List<Id>();
		for( Connections__c connection : tmpConnectionList )
		{
			if( connection.RecordType.Name == 'R:R' && connection.Relationship2__c != null )
				searchedIds.add( connection.Relationship2__c );
			if( connection.RecordType.Name == 'R:C' && connection.Contact_C1__c != null )
				searchedIds.add( connection.Contact_C1__c );
			if( connection.RecordType.Name == 'C:C' && connection.Contact_C2__c != null )
				searchedIds.add( connection.Contact_C2__c );
		}

		List<UserRecordAccess> userRecordAccesses = [ SELECT RecordId FROM UserRecordAccess WHERE RecordId IN :searchedIds AND UserId =:UserInfo.getUserId() AND HasReadAccess = true ];
		Set<String> accessSet = new Set<String>();
		for( UserRecordAccess ur : userRecordAccesses )
			accessSet.add( ur.RecordId );

		contactList = new List<Connections__c>();
		for( Connections__c connection : tmpConnectionList )
		{
			if( connection.RecordType.Name == 'R:R' && accessSet.contains( connection.Relationship2__c ) &&  connection.Relationship2__c != null  )
				contactList.add( connection );
			if( connection.RecordType.Name == 'R:C' && accessSet.contains( connection.Contact_C1__c ) && connection.Contact_C1__c != null )
				contactList.add( connection );
			if( connection.RecordType.Name == 'C:C' && accessSet.contains( connection.Contact_C2__c ) &&  connection.Contact_C2__c != null )
				contactList.add( connection );
		}
	}

	private String getRecordTypeId( String objName , String typeName )
	{
		List<RecordType> rt = Database.query( 'SELECT Id FROM RecordType WHERE SObjectType = \'' + objName + '\' and Name = \'' + typeName + '\'' );
		String retVal = '';
		if( !rt.isEmpty() )
			retVal = rt[0].Id;

		return retVal;
	}

	private String buildFieldList( String filterType )
	{
		List<AddressConnectionMap__c> addressMap = AddressConnectionMap__c.getAll().values();
		String sqlFields = '';
		List<FieldSet> fldSetList = new List<FieldSet>();
		String fieldLabel  ='';

		for( AddressConnectionMap__c fieldMap : addressMap )
		{
			if( fieldMap.Type__c ==  filterType )
			{
				if( !labelCache.containskey( fieldMap.FieldName__c ) )
				{
					String [] fieldSplit = fieldMap.FieldName__c.split('\\.');
					if( fieldSplit.size() > 0 )
					{
						if( fieldSplit[0].contains( 'Relationship' ) )
							fieldLabel = getFieldLabel( 'Account', fieldSplit[1] );
						if( fieldSplit[0].contains( 'Contact' ) )
							fieldLabel = getFieldLabel( 'Contact', fieldSplit[1] );

						labelCache.put( fieldMap.FieldName__c, fieldLabel );
					}
				}
				else
					fieldLabel = labelCache.get( fieldMap.FieldName__c );

				fldSetList.add( new FieldSet( Integer.valueOf( fieldMap.Sort_Order__c ),fieldMap.FieldName__c, fieldLabel ) );
				if( sqlFields.length() == 0 )
					sqlFields = fieldMap.FieldName__c;
				else
					sqlFields += ',' + fieldMap.FieldName__c;
			}
		}

		if( fldSetList.size() > 0 )
		{
			fldSetList.sort();
			for( FieldSet f : fldSetList )
				fieldList  = fldSetList;
		}

		if( sqlFields.length() > 0 )
			sqlFields = ',' + sqlFields;

		return sqlFields;
	}

	public String getFieldLabel( String objType , String fieldName )
	{
		SObjectField fieldToken; 
		
		if( accountFields == null )
		{
			SObjectType objToken = Schema.getGlobalDescribe().get( 'Account' );
			DescribeSObjectResult objDef = objToken.getDescribe();
			accountFields = objDef.fields.getMap();
		}
		if( contactFields == null )
		{
			SObjectType objTokenContacts = Schema.getGlobalDescribe().get( 'Contact' );
			DescribeSObjectResult objDefContacts = objTokenContacts.getDescribe();
			contactFields= objDefContacts.fields.getMap();
		}

		if( objType == 'Account' )
			fieldToken = accountFields.get( fieldName );
		if( objType == 'Contact' )
			fieldToken = contactFields.get( fieldName );

		DescribeFieldResult selectedField = fieldToken.getDescribe();
		return selectedField.getLabel();
	}

	public void saveRoleChange()
	{
		contactObj = new Contact();
		List<Connections__c> updateContactsList = new List<Connections__c>();
		Boolean isPrimary = false;
		for( Connections__c c : contactList )
		{
			if( c.Id == selectedId )
			{
				updateContactsList.add( c );
				if( c.Role_on_Relationship__c == 'Primary Decision Maker' )
				{
					isPrimary = true;
					break;
				}
			}
		}
		if( isPrimary )
		{
			for( Connections__c c : contactList )
			{
				if( c.Id != selectedId && c.Role_on_Relationship__c == 'Primary Decision Maker' )
				{
					c.Override__c = true;
					c.Role_on_Relationship__c = 'Decision Maker';
					updateContactsList.add( c );
				}
			}
		}
		try
		{
			update updateContactsList;
		}
		catch(Exception ex)
		{
			//Messages will bubble up FROM the trigger logic
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error,ex.getMessage() ) );
			createContactList();
		}
	}

	public void resetConnectionObj()
	{
		hasError1 = '0';
		hasError2 = '0';
		if( actionType == 'NewContact' )
		{
			contactObj = new Contact();
			AddressObj = new Addresses__c ();
			contactObj.RecordTypeId = contactRT;
		}
		/*
		else
		{
			connectionObj = new Connections__c();
			connectionObjNewRc = new Connections__c();
			connectionObjRel= new Connections__c();
		}
		*/
		connectionObj = new Connections__c();
		connectionObj.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND Name = 'R:C' ].Id;
		connectionObjNewRC = new Connections__c();
		connectionObjNewRC.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND Name = 'R:C' ].Id;
		connectionObjRel = new Connections__c();
		connectionObjRel.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND Name = 'R:R' ].Id;
	}

	public PageReference saveAndOverride()
	{
		myComponentController.OverRideDupeBlocker();
		isOverride = true;
		saveNewConnection();
		isOverride = false;
		return null;
	}

	public PageReference saveAndNew()
	{
		checkOverride();
		saveNewConnection();
		isOverride = false;
		return null;
	}

	public PageReference saveAndClose()
	{
		checkOverride();
		saveNewConnection();
		isOverride = false;
		if( hasError1 == '1' || hasError2 == '1' || hasDuplicates || hasPageErrors )
			return null;
		else
			return returnUrl();
	}

	public PageReference cancel()
	{
		return returnUrl();
	}

	//***************   private methods  ***********************************
	private void checkOverride()
	{
		//Reset the Duplicates Param
		hasDuplicates = false; 
		//Check to see if the postback originated FROM the ByPass Override Button
		String overRideParam =  ApexPages.currentPage().getParameters().get( 'OverRideBtn' );
		if( overRideParam == null )
			isOverRide = false;
		else
			isOverRide = Boolean.valueOf( overRideParam );
	}

	private void saveNewConnection()
	{
		hasError1 = '0';
		hasError2 = '0';
		hasPageErrors = false;
		Connections__c connection= new Connections__c();

		if( actionType == 'Contact' )
		{
			if( connectionObj.Contact_C1__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Contact Name is a required field' ) );
				hasError1 = '1';
			}
			if( connectionObj.Role_on_Relationship__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Contact Role is a required field' ) );
				hasError2 = '1';
			}
			if( hasError1 == '1' || hasError2 == '1' )
				return;

			connection.Relationship_R1__c = accountId;
			connection.Contact_C1__c = connectionObj.Contact_C1__c;
			connection.Role_on_Relationship__c = connectionObj.Role_on_Relationship__c;
			connection.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND DeveloperName = 'R_C' ].Id;

			try
			{
				insert connection;
			}
			catch( Exception ex )
			{
				//Error bubbles up FROM the trigger handler
				//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() ));
			}
		}
		if( actionType =='Relationship' )
		{
			if( connectionObjRel.Relationship2__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Relationship Name is arequired field' ) );
				hasError1 = '1';
				return;
			}
			if( connectionObjRel.Role_on_Relationship__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Relationship Role is a required field' ) );
				hasError2 = '1';
			}
			if( hasError1 == '1' || hasError2 == '1' )
				return;

			connection.Relationship_R1__c = accountId;
			connection.Relationship2__c = connectionObjRel.Relationship2__c;
			connection.Role_on_Relationship__c = connectionObjRel.Role_on_Relationship__c;
			connection.RecordTypeId = [ SELECT Id FROM RecordType WHERE SObjectType = 'Connections__c' AND DeveloperName = 'R_R' ].Id;
			try
			{
				insert connection;
			}
			catch( Exception ex )
			{
				//Error bubbles up FROM the trigger handler
				//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() ));
			}
		}
		if( actionType =='NewContact' )
		{
			/**************  Add the State and Country  from the Address Object ******/
			contactObj.MailingState	 = AddressObj.State__c ;
			contactObj.MailingCountry = AddressObj.Country__c;

			//Check for permissions on the Relationship record
			if ( connectionObjNewRc.Role_on_Relationship__c == PRIMARY_DECISION_MAKER )
			{
				if (!Schema.sObjectType.Account.fields.Primary_Decision_Maker__c.isUpdateable() )
				{
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The current user does not have permissions to update the relationship.' ) );
					hasError2 = '1';
				}
			}
			
			if( [ SELECT Name FROM RecordType WHERE SObjectType = 'Contact' AND Id = :contactRTSelect.RecordTypeId ].Name == 'Entity Contact' ){
				if(contactObj.Entity_Name__c <> null && contactObj.Entity_Name__c.length() > 50)
					contactObj.LastName = contactObj.Entity_Name__c.substring(0,50);
				else
					contactObj.LastName = contactObj.Entity_Name__c;				
			}

			//Check for required fields.  Need to handle in class because of the multiple panels on the VF page
			if( connectionObjNewRc.Role_on_Relationship__c == null )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Role is a required field' ) );
				hasError2 = '1';
			}
			if( contactObj.LastName == null || contactObj.LastName.length() == 0 )
			{
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Contact LastName is a required field' ) );
				hasError1 = '1';
			}

			contactObj.AccountId = accountId;
			if( connectionObjNewRc.Role_on_Relationship__c == PRIMARY_DECISION_MAKER )
			{
				List<Account>  accountList = [ SELECT Id, Primary_Decision_Maker__c FROM Account WHERE Id = :accountId AND Primary_Decision_Maker__C != null ];
				if( accountList.size() > 0 )
				{
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'The Role of the Primary Decision Maker alreay exists for this Relationship' ) );
					hasPageErrors = true;
					return;
				}
			}

			if( hasError1 == '1' || hasError2 == '1' || hasPageErrors )
				return;

			//Check For Duplicates
			hasDuplicates = false;
			if( !isOverRide )
				hasDuplicates = MyComponentController.CheckContactDups( contactObj );
			else
			{
				MyComponentController.OverRideDupeBlocker();
				contactObj.Dupe_Blocker_Bypass__c = true;
			}
			if(hasDuplicates)
				return;

			insert contactObj;
			//Add the Address Record
			List<Addresses__c> addList  = [SELECT Contact__c, Contact__r.Name, Street_Address__c, 
				Country__c,   City__c, State__c, Postal_Code__c, Primary_Address__c FROM Addresses__c where Primary_Address__c = true and Contact__c = :contactObj.id];
			if (addList.isEmpty() )
			{
				if (contactObj.MailingStreet != null || contactObj.MailingCity != null || contactObj.MailingState != null || contactObj.MailingPostalCode != null)
				{
					Addresses__c add = new Addresses__c ();
					add.Contact__c = contactObj.id;
					add.Primary_Address__c = true ;
					add.Address_Type__c = 'Home';
					add.Street_Address__c = contactObj.MailingStreet;
					add.City__c = contactObj.MailingCity;
					add.State__c = contactObj.MailingState;
					add.Postal_Code__c = contactObj.MailingPostalCode;
					insert add;
				}
			}
			else
			{
					Addresses__c add = addList[0];
					
					//add.Primary_Address__c = true ;
					//add.Address_Type__c = 'Home';
					add.Street_Address__c = contactObj.MailingStreet;
					add.City__c = contactObj.MailingCity;
					add.State__c = contactObj.MailingState;
					add.Postal_Code__c = contactObj.MailingPostalCode;
					update add;
					
			}
			//Get the Connection created the by Contact Trigger and Update the Role.
			List<Connections__c> existingConnection = [ SELECT Role_on_Relationship__c FROM Connections__c WHERE Relationship_R1__c = :accountId AND Contact_C1__c = :contactObj.Id ];
			for( Connections__c c :existingConnection )
				c.Role_on_Relationship__c = connectionObjNewRc.Role_on_Relationship__c;

			update existingConnection;
			/*
			connection.Relationship_R1__c = accountId;
			connection.Contact_C1__c = contactObj.Id;
			connection.Role_on_Relationship__c = connectionObjNewRc.Role_on_Relationship__c;
			connection.RecordTypeId = [ SELECT id FROM RecordType WHERE sObjectType = 'Connections__c' and DeveloperName = 'R_C'  ].Id;
			insert connection;
			*/
			
		}
		resetConnectionObj();
		createContactList();
	}

	public void processContactSection()
	{
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();

		List<String> contactDupFields = new List<String>();
		//List<AggregateResult> results = [ SELECT SectionName__c, SectionOrder__c FROM ContactFieldList__c WHERE Platform__c = :platform Group BY SectionName__c, SectionOrder__c ORDER BY sum()   ] ;
		//List<>
		String rtName = [ SELECT Name FROM RecordType WHERE Id = :contactObj.RecordTypeId ].Name;
		String pageName = ContactEditLayouts__c.getValues(rtName).PageName__c;

		for( AggregateResult ar : [ SELECT SectionName__c FROM ContactFieldList__c WHERE PageName__c = :pageName AND Platform__c = :platform GROUP BY SectionName__c ORDER BY MAX( SectionOrder__c ) ] )
		{
			contactDupFields.add( String.valueOf( ar.get( 'SectionName__c' ) ) );
		}

		contactSection = contactDupFields;
	}

	public void processContactFields()
	{
		GlobalContactSearch searchObj = new GlobalContactSearch();
		String platform = searchObj.checkPlatform();

		List<String> contactDupFields = new List<String>();
		Map<String, List<LayoutFields>> returnMap = new Map<String, List<LayoutFields>>();

		String rtName = [ SELECT Name FROM RecordType WHERE Id = :contactObj.RecordTypeId ].Name;
		String pageName = ContactEditLayouts__c.getValues( rtName ).PageName__c;

		SObjectType objToken = Schema.getGlobalDescribe().get( 'Contact' );
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap(); 
		/*
		Set<String> FieldSet = fields.keySet();
		List<String> tmpList = new List<String>();
		for( String s : FieldSet )
		{
			SObjectField fieldToken = fields.get( s );
			DescribeFieldResult selectedField = fieldToken.getDescribe();
			if( selectedField.isUpdateable()  )
		*/
		for( ContactFieldList__c cldf : [ SELECT SectionName__c, Field__c, Required__c, StyleClass__c FROM ContactFieldList__c WHERE Platform__c = :platform AND PageName__c = :pageName ORDER BY SortOrder__c ] )
		{
			if( cldf.Field__c != 'BlankSectionItem' && cldf.Field__c != 'AddressType' && cldf.Field__c != 'AddressAdditionalAddressLine' )
			{
				SObjectField fieldToken = fields.get( cldf.Field__c );
				DescribeFieldResult selectedField = fieldToken.getDescribe();
				if( selectedField.isUpdateable() )
				{
					if( returnMap.ContainsKey( cldf.SectionName__c ) )
					{
						List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c );
						tmpStringList.add( new LayoutFields(cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpStringList );
					}
					else
					{
						List<LayoutFields> tmpStringList = new List<LayoutFields>();
						tmpStringList.add( new LayoutFields( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpStringList );
					}
				}
				else
				{
					if( returnMap.containsKey( cldf.SectionName__c ) )
					{
						List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c );
						tmpStringList.add( new LayoutFields( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put(cldf.SectionName__c , tmpStringList);
					}
					else
					{
						List<LayoutFields> tmpStringList = new List<LayoutFields>();
						tmpStringList.add( new LayoutFields( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c ) );
						returnMap.put( cldf.SectionName__c, tmpStringList );
					}
				}
			}
			else if (cldf.Field__c == 'AddressType' || cldf.Field__c == 'AddressAdditionalAddressLine')
			{
				if( returnMap.containsKey( cldf.SectionName__c ) )
				{
					List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c ) ;
					tmpstringList.add(new LayoutFields ( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c )  );
					returnMap.put( cldf.SectionName__c, tmpstringList );
				}
				else
				{
					List<LayoutFields> tmpStringList = new List<LayoutFields>();
					tmpstringList.add( new LayoutFields( cldf.Field__c, true, cldf.Required__c, cldf.StyleClass__c )  );
					returnMap.put( cldf.SectionName__c, tmpstringList );
				}
			}
			else
			{
				if( returnMap.containsKey( cldf.SectionName__c ) )
				{
					List<LayoutFields> tmpStringList = returnMap.get( cldf.SectionName__c );
					tmpStringList.add( new LayoutFields( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c ) );
					returnMap.put( cldf.SectionName__c , tmpStringList );
				}
				else
				{
					List<LayoutFields> tmpStringList = new List<LayoutFields>();
					tmpStringList.add( new LayoutFields( cldf.Field__c, false, cldf.Required__c, cldf.StyleClass__c ) );
					returnMap.put( cldf.SectionName__c, tmpStringList );
				}
			}
		}

		contactFieldList = returnMap;
	}

	class LayoutFields
	{
		public String fieldName { get; set; }
		public Boolean isEditable { get; set; }
		public Boolean required { get; set; }
		public String styleCls { get; set; }

		public LayoutFields( String fld, Boolean edit, Boolean req, String sc )
		{
			fieldName = fld;
			isEditable = edit;
			required = req;
			styleCls = sc;
		}
	}

	public class FieldSet implements Comparable 
	{
		public Integer sortNumber { get; set; }
		public String field { get; set; }
		public String fieldLabel { get; set; }

		public FieldSet( Integer s, String f, String fldLabel )
		{
			sortNumber = s;
			field = f;
			fieldLabel = fldLabel;
		}

		public Integer compareTo( Object compareTo )
		{
			FieldSet fs = (FieldSet)compareTo;
			if( sortNumber > fs.sortNumber ) return 1;
			if( sortNumber == fs.sortNumber ) return 0;
			return -1;
		}
	}
}