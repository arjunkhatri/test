global class RelationshipCountBatch implements Database.Batchable<sObject>{
	public static final String RCTYPE = 'R:C';
	public static final String CLIENTR = 'Client Relationship';
	public static final String PROSPECTIVER = 'Prospective Relationship';

/*	
    global Iterable<Contact> start(Database.BatchableContext BC){ 
    	List<Contact> lstContact = [SELECT Id,
    								Num_of_Client_Relationships__c,
    								Num_of_Prospective_Relationships__c FROM Contact];
    	return lstContact;
    }
*/
  	
  	static final String sQuery = 'SELECT Id,	Num_of_Client_Relationships__c,	Num_of_Prospective_Relationships__c FROM Contact';
  	
    global Database.QueryLocator start(Database.BatchableContext BC){ 
    	return Database.getQueryLocator(sQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope){
    	map<Id,set<Id>> mpConToAccIdsClient = new map<Id,set<Id>>();
    	map<Id,set<Id>> mpConToAccIdsProsp = new map<Id,set<Id>>();
    	List<Contact> lstContToUpdate = new List<Contact>();
    	Id clientRecTypId;
    	Id prospectRecTypId;
    	for(RecordType RecType: [SELECT Name, Id FROM RecordType WHERE 
    									  Name =:CLIENTR OR Name = :PROSPECTIVER])
    	{
    		if(RecType.Name == CLIENTR)
    			clientRecTypId = RecType.Id;
    		if(RecType.Name == PROSPECTIVER)
    			prospectRecTypId = RecType.Id;
    	}
    	
    	for(Connections__c conRec:[SELECT Relationship_R1__r.RecordTypeId,
    	 						          Relationship_R1__r.Id,
    	 							      Relationship_R1__c, 
    	 							      RecordType.Name,
    	 							      Contact_C1__c,
    	 							      Contact_C1__r.Id
    	 							      FROM Connections__c WHERE 
    	 							      RecordType.Name = :RCTYPE AND
    	 							      Contact_C1__c IN :scope] )
    	{
    	 	if(conRec.Relationship_R1__r.Id != null && conRec.Contact_C1__r.Id != null){
	    	 	if(clientRecTypId != null && conRec.Relationship_R1__r.RecordTypeId == clientRecTypId ){
	    	 		//fill map of contact is vs client relationship ids
	    	 		if(!mpConToAccIdsClient.containsKey(conRec.Contact_C1__r.Id)){
	    	 			mpConToAccIdsClient.put(conRec.Contact_C1__r.Id,new Set<Id>{conRec.Relationship_R1__r.Id});
	    	 		}
	    	 		else if (mpConToAccIdsClient.containsKey(conRec.Contact_C1__r.Id)){
	    	 			mpConToAccIdsClient.get(conRec.Contact_C1__r.Id).add(conRec.Relationship_R1__r.Id);
	    	 		}
	    	 	
	    	 	}
	    	 	else if(prospectRecTypId != null && conRec.Relationship_R1__r.RecordTypeId == prospectRecTypId){
	    	 		//fill map of contact is vs prospect relationship ids
	    	 		if(!mpConToAccIdsProsp.containsKey(conRec.Contact_C1__r.Id)){
	    	 			mpConToAccIdsProsp.put(conRec.Contact_C1__r.Id,new Set<Id>{conRec.Relationship_R1__r.Id});
	    	 		}
	    	 		else if (mpConToAccIdsProsp.containsKey(conRec.Contact_C1__r.Id)){
	    	 			mpConToAccIdsProsp.get(conRec.Contact_C1__r.Id).add(conRec.Relationship_R1__r.Id);
	    	 		}	    	 		
	    	 	
	    	 	}
    	 	}
    	}
    	//iterate contacts and get count of relationship
    	for(Contact conRec:scope){
    	 	//get client relationship count
    	 	if(!mpConToAccIdsClient.isEmpty() && mpConToAccIdsClient.containsKey(conRec.id)){
    	 		conRec.Num_of_Client_Relationships__c = mpConToAccIdsClient.get(conRec.id).size();
    	 	}
    	 	else{
    	 		conRec.Num_of_Client_Relationships__c = 0;
    	 	}
    	 	
    	 	//get prospect relationship count
     	 	if(!mpConToAccIdsProsp.isEmpty() && mpConToAccIdsProsp.containsKey(conRec.id)){
    	 		conRec.Num_of_Prospective_Relationships__c = mpConToAccIdsProsp.get(conRec.id).size();
    	 	}
    	 	else{
    	 		conRec.Num_of_Prospective_Relationships__c = 0;
    	 	}   	 	
    	 	lstContToUpdate.add(conRec);
system.debug('Contact ' + conRec.Id + ' Prospective: ' + conRec.Num_of_Prospective_Relationships__c + ' Client: ' + conRec.Num_of_Client_Relationships__c);
    	}
 		if(lstContToUpdate != null && lstContToUpdate.size() > 0){
			Database.SaveResult[] srListCon = Database.update(lstContToUpdate, false);
			for (Database.SaveResult sr : srListCon) {
			    if (!sr.isSuccess()) {
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage());
			        }
			    }
			}
		}
    }
    
	global void finish(Database.BatchableContext BC){
	}
}