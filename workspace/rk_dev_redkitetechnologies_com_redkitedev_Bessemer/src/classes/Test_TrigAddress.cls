@isTest
private class Test_TrigAddress {
    
     static testMethod void TestTrigAddress_Test1 ()
     {
        
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        insert gs;
        
        
        List<Addresses__c> testData =  TestDataCreator_Utils.createSObjectList('Addresses__c' , true , 1); 
        testData[0].Primary_Address__c = true;
        insert testData ;
     }
    
}