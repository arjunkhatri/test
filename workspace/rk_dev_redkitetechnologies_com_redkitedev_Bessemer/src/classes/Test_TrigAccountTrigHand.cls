@isTest
private class Test_TrigAccountTrigHand {
    
    static testMethod void  AccountContactTrig_Test1() 
    {
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        gs.Integration_User_Name__c = UserInfo.getUserName();
        insert gs;
    
        map<String, set<String>> includeFld = new map<String, set<String>> () ;
        set<String> tmpFld = new set<String> ();
        tmpFLd.add('Relationship__c') ;
        includeFld.put('Accounts__c' , tmpFld);
        TestDataCreator_Utils.IncludedFields = includeFld ;
        List<Accounts__c> acctList = TestDataCreator_Utils.createSObjectList('Accounts__c' , true , 25); 
        for (integer i = 0 ; i < 10 ; i++)
            acctList[i].EDB_Entity_Id__c = 'EDB' + String.ValueOf(i);
        insert acctList;

        List<Data_Sync_Log__c> logList = [select id from Data_Sync_Log__c];
        System.assertEquals(10 , logList.size() );

        acctList [5]. Market_Value__c = 10000;
        update acctList [5] ;
        logList = [select id from Data_Sync_Log__c];
        System.assertEquals(11 , logList.size() );

        delete acctList [3];
        logList = [select id from Data_Sync_Log__c];
        System.assertEquals(13 , logList.size() );

    }




}