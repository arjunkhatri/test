global class UploadStagedTasksBatch implements Database.Batchable<sObject>{
	public Set<String> setEmpNumber;
	public Set<String> setRelNumber;
	public Set<String> setCustSNumber;
	public Set<String> setAccNumber;
	public Set<String> setSubAccNumber;	
	public map<String,Id> mpEmpNoToUser;
	public map<String,Id> mpRelNoToRelOwner;
	public map<String,Id> mpCustNoToConId;
	public map<String,Id> mpCustNoToLeadId;
	public map<String,Id> mpSubAccNoToAccId;
	public map<String,Id> mpAccNoToAccId;
	public map<String,Id> mpRelNoToRelId;
	public map<String,String> mpSubAccNoSubAccId;
	public map<String,Id> mpCustNoToConAccId;
	public map<Id,Task> mpActIdtoTsk;
	public static final String TICKLER = 'Tickler';
	public static final String NOSUBJ = 'No Subject';
	public static final String OTHER = 'Other';
	public static final String NOSTARTED = 'Not Started';
	public static final String NORMAL = 'Normal';	
	public Boolean isLeadWhoId;
	public Id bessemerIntUser;
 	public UploadStagedTasksBatch(){
 
 	}
 	
	static final String sQuery = 'Select Type__c,Subject__c,SubAccount_Number__c,Status__c,Relationship_Number__c,Priority__c,Employee_Number__c,Due_Date__c,Description__c,Customer_S_Number__c,Activity_Id__c,Account_Number__c, EDB_Created_Date__c From Activity_Staging__c Where Processed__c = false';
 	
 	
    global Database.QueryLocator start(Database.BatchableContext BC){ 
    	return Database.getQueryLocator(sQuery);
    }
/* 	
    global Iterable<Activity_Staging__c> start(Database.BatchableContext BC){ 
    	return getunStagedTasks();
    }
*/    
    //Get all UnStaged Task to process
    public List<Activity_Staging__c> getunStagedTasks(){
    	List<Activity_Staging__c> lstStagedTask = [Select Type__c,
    	 												  Subject__c,
    	 												  SubAccount_Number__c,
    	 												  Status__c,
    	 												  Relationship_Number__c,
    	 												  Priority__c,
    	 												  Employee_Number__c,
    	 												  Due_Date__c,
    	 												  Description__c,
    	 												  Customer_S_Number__c,
    	 												  Activity_Id__c,
    	 												  Account_Number__c,
    	 												  EDB_Created_Date__c	 
    	 												  From Activity_Staging__c 
    	 												  Where Processed__c = false];
    	return lstStagedTask; 												  
    }

	global void execute(Database.BatchableContext BC, List<Activity_Staging__c> scope){
		List<Activity_Staging__c> listProcessedActStg = new List<Activity_Staging__c>();
		setEmpNumber = new Set<String>();
		setRelNumber = new Set<String>();
		setCustSNumber = new Set<String>();
		setAccNumber = new Set<String>();
		setSubAccNumber = new Set<String>();
		mpEmpNoToUser = new map<String,Id>();
		mpRelNoToRelOwner = new map<String,Id>();
		mpCustNoToConId = new map<String,Id>();
		mpCustNoToLeadId = new map<String,Id>();
		mpSubAccNoToAccId = new map<String,Id>();
		mpAccNoToAccId = new map<String,Id>();
		mpRelNoToRelId = new map<String,Id>();
		mpSubAccNoSubAccId = new map<String,String>();
		mpCustNoToConAccId = new map<String,Id>();
		mpActIdtoTsk = new  map<Id,Task>();
		for(Activity_Staging__c scopRec:scope){
			if(!isNullOrEmpty(scopRec.Employee_Number__c))
				setEmpNumber.add(scopRec.Employee_Number__c.trim());
			if(!isNullOrEmpty(scopRec.Relationship_Number__c))	
				setRelNumber.add(scopRec.Relationship_Number__c.trim());
			if(!isNullOrEmpty(scopRec.Customer_S_Number__c))	
				setCustSNumber.add(scopRec.Customer_S_Number__c.trim());
			if(!isNullOrEmpty(scopRec.Account_Number__c))	
				setAccNumber.add(scopRec.Account_Number__c.trim());
			if(!isNullOrEmpty(scopRec.SubAccount_Number__c))	
				setSubAccNumber.add(scopRec.SubAccount_Number__c.trim());
		} 
		
		getAllMapData();
		
		for(Activity_Staging__c actStage:scope){
			isLeadWhoId = false;
			Task tsk = new Task();
			if(!isNullOrEmpty(actStage.Activity_Id__c))
				tsk.Activity_Id__c = actStage.Activity_Id__c;
			if(!isNullOrEmpty(actStage.Type__c))
				tsk.Type = actStage.Type__c;
			else
				tsk.Type = OTHER;		
			if(!isNullOrEmpty(actStage.Description__c))
				tsk.Description = actStage.Description__c;
			if(!isNullOrEmpty(actStage.Status__c))
				tsk.Status = actStage.Status__c;
			else
				tsk.Status = NOSTARTED;
			if(!isNullOrEmpty(actStage.Subject__c))
				tsk.Subject = actStage.Subject__c;
			else
				tsk.Subject = NOSUBJ;	
			if(!isNullOrEmpty(actStage.Priority__c))
				tsk.Priority = actStage.Priority__c;
			else
				tsk.Priority = NORMAL;	
			if(actStage.Due_Date__c != null)
				tsk.ActivityDate = actStage.Due_Date__c;
			//if(actStage.EDB_Created_Date__c != null)
			//	tsk.CreatedDate = actStage.EDB_Created_Date__c;
			//assign task owner id
			tsk.OwnerId = assignTaskOwnerId(actStage);	
			//assign task who id
			tsk.WhoId = assignTaskWhoId(actStage.Customer_S_Number__c);	
			//assign task whatid
			if(!isLeadWhoId)
				tsk.WhatId = assignTaskWhatId(actStage);			
			//assign sub account number
			if(!isNullOrEmpty(actStage.SubAccount_Number__c) &&!mpSubAccNoSubAccId.isEmpty() &&
			 						mpSubAccNoSubAccId.containsKey(actStage.SubAccount_Number__c.trim()))
				tsk.Sub_Account__c = String.valueOf(mpSubAccNoSubAccId.get(actStage.SubAccount_Number__c.trim()));

				mpActIdtoTsk.put(actStage.Id,tsk);
		}
		if(!mpActIdtoTsk.isEmpty() && mpActIdtoTsk.Values().size() > 0){ 
			Database.UpsertResult[] srList = Database.upsert(mpActIdtoTsk.Values(),Schema.Task.Activity_Id__c, false);
			for (Database.UpsertResult sr : srList) {
			    if (sr.isSuccess()) {
			        System.debug('Successfully upserted Task. Task ID: ' + sr.getId());
			    }
			    else {
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage());                    
			        }
			    }
			}	
		}
		
		//get activity staging records whose corrosponding task is successfully created
		for(Activity_Staging__c actStgRec:scope){
			if(!mpActIdtoTsk.isEmpty() && mpActIdtoTsk.containsKey(actStgRec.id) && 
					mpActIdtoTsk.get(actStgRec.id).id != null){
				actStgRec.Processed__c = true;						  	
				listProcessedActStg.add(actStgRec);				
			}
		}
					
		if(listProcessedActStg != null && listProcessedActStg.size() > 0){
			Database.SaveResult[] srListAct = Database.update(listProcessedActStg, false);
			for (Database.SaveResult sr : srListAct) {
			    if (!sr.isSuccess()) {
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage());                    
			        }
			    }
			}			
		}
		
	}
	
	//fill all required maps here  
	public void getAllMapData(){
		//get bessemer integration user id
		List<User> userB = [Select Id From User where Profile.Name = 'Bessemer Integration' limit 1];
		if(userB != null && userB.size() > 0)
			bessemerIntUser = userB[0].id;
		
		//fill map of emp number vs user id
		for(User userRec: [Select Id, EmployeeNumber 
						  From User where EmployeeNumber IN: setEmpNumber])
		{
			mpEmpNoToUser.put(userRec.EmployeeNumber.trim(),userRec.Id);
		}
		
		//fill map of Relationship Number vs Relationship Id
		for(Account relRec:[Select Relationship_Number__c, OwnerId 
							From Account where Relationship_Number__c IN:setRelNumber])
		{
			mpRelNoToRelOwner.put(relRec.Relationship_Number__c.trim(),relRec.OwnerId);	
			mpRelNoToRelId.put(relRec.Relationship_Number__c.trim(),relRec.id);			
		}
		
		//fill map of Customer S Number to Contact id 
		for(Contact conRec:[Select Id, Contact_S_Number__c,AccountId 
							From Contact Where Contact_S_Number__c IN:setCustSNumber])
		{
			mpCustNoToConId.put(conRec.Contact_S_Number__c.trim(),conRec.Id);
			if(conRec.AccountId != null)
				mpCustNoToConAccId.put(conRec.Contact_S_Number__c.trim(),conRec.AccountId);
		}
		
		//fill map of Customer S Number to Lead id	
		for(Lead leadRec:[Select Id, Contact_S_Number__c 
						 From Lead where Contact_S_Number__c IN:setCustSNumber])
		{
			mpCustNoToLeadId.put(leadRec.Contact_S_Number__c.trim(),leadRec.Id);			 
		}	
		
		//fill map of SubAccount Number vs SubAccount Account Id
		for(SubAccounts__c subAccRec:[Select SubAccount_Number_Unique__c, Id,Name,Account__r.Id
									 From SubAccounts__c where SubAccount_Number_Unique__c IN:setSubAccNumber])
		{   
			if(subAccRec.Account__r.Id != null)
				mpSubAccNoToAccId.put(subAccRec.SubAccount_Number_Unique__c.trim(),subAccRec.Account__r.Id);
			mpSubAccNoSubAccId.put(subAccRec.SubAccount_Number_Unique__c.trim(),subAccRec.Name);
		}
		
		//fill map of  Account Number vs Account__c
		for(Accounts__c accRec:[Select Id, Account_Number_Unique__c 
								From Accounts__c where Account_Number_Unique__c IN:setAccNumber])							 
		{
			mpAccNoToAccId.put(accRec.Account_Number_Unique__c.trim(),accRec.Id);
		}
			
	}    
	
	public Id assignTaskOwnerId(Activity_Staging__c actStage){
		Id tskOwner;
		//check user employee number
		if(!isNullOrEmpty(actStage.Employee_Number__c) && !mpEmpNoToUser.isEmpty() &&
			mpEmpNoToUser.containsKey(actStage.Employee_Number__c.trim())){
			tskOwner = Id.valueOf(mpEmpNoToUser.get(actStage.Employee_Number__c.trim()));
		}
		else if(!isNullOrEmpty(actStage.Relationship_Number__c) && !mpRelNoToRelOwner.isEmpty() && 
			mpRelNoToRelOwner.containsKey(actStage.Relationship_Number__c.trim())){
			tskOwner = Id.valueOf(mpRelNoToRelOwner.get(actStage.Relationship_Number__c.trim()));
		}
		else if(!isNullOrEmpty(actStage.Type__c) && actStage.Type__c.trim() == TICKLER &&
		 	actStage.Due_Date__c != null && actStage.Due_Date__c >= Date.today()){
			if(GlobalSettings__c.getInstance().Bessemer_Help_Desk_User__c != null)
				tskOwner = GlobalSettings__c.getInstance().Bessemer_Help_Desk_User__c;
		}
		else{ //defaults to
			if(bessemerIntUser != null)
				tskOwner = bessemerIntUser;
		}
		return tskOwner;
	}
	//function to assign task who id
	public Id assignTaskWhoId(String custSNumber){
		Id whoId;
		if(!isNullOrEmpty(custSNumber) && !mpCustNoToConId.isEmpty() &&
			mpCustNoToConId.ContainsKey(custSNumber.trim())){
			whoId = Id.valueOf(mpCustNoToConId.get(custSNumber.trim()));
		}
		else if(!isNullOrEmpty(custSNumber) && !mpCustNoToLeadId.isEmpty() &&
		 	mpCustNoToLeadId.ContainsKey(custSNumber.trim())){
			whoId = Id.valueOf(mpCustNoToLeadId.get(custSNumber.trim()));
			isLeadWhoId = true;
		}
		else{
			whoId = null;
		}
		return whoId;
	}
	//function to assign task what id
	public Id assignTaskWhatId(Activity_Staging__c actStg){ 
		Id whatId;
		if(!isNullOrEmpty(actStg.SubAccount_Number__c) && !mpSubAccNoToAccId.isEmpty() && 
			mpSubAccNoToAccId.ContainsKey(actStg.SubAccount_Number__c.trim())){
			whatId =  Id.valueOf(mpSubAccNoToAccId.get(actStg.SubAccount_Number__c.trim()));
		}
		else if(!isNullOrEmpty(actStg.Account_Number__c) && !mpAccNoToAccId.isEmpty() && 
			mpAccNoToAccId.containsKey(actStg.Account_Number__c.trim())){
			whatId =  Id.valueOf(mpAccNoToAccId.get(actStg.Account_Number__c.trim()));
		} 
		else if(!isNullOrEmpty(actStg.Relationship_Number__c) && !mpRelNoToRelId.isEmpty() && 
			mpRelNoToRelId.containsKey(actStg.Relationship_Number__c.trim())){
			whatId =  Id.valueOf(mpRelNoToRelId.get(actStg.Relationship_Number__c.trim()));
		}
		else if(!isNullOrEmpty(actStg.Customer_S_Number__c) && !mpCustNoToConAccId.isEmpty() && 
			mpCustNoToConAccId.containsKey(actStg.Customer_S_Number__c.trim())){
			whatId =  Id.valueOf(mpCustNoToConAccId.get(actStg.Customer_S_Number__c.trim()));
		}
		return whatId;
	}
	//function checking null or empty string
	public Boolean isNullOrEmpty(String strVal){  
		return (strVal != null && strVal != '' ? false:true);
	}
	
	global void finish(Database.BatchableContext BC){
	   	
	} 
}