@isTest
public with sharing class TestFamilyGroupSharingBatch {
	 static testMethod void testFamilyGroupSharing(){
	 	Profile pf = [Select Name, Id From Profile p where name='Bessemer Client Advisor'];
	 	User usr1 = TestDataFactory.createTestUser(pf.id);
	 		insert usr1;
	 		system.assert(usr1.id != null,'User Created successfully');
	 		
	 	//create records for update Family_Group__share
	 	Family_Group__c fg = new Family_Group__c(Name ='test Fg');
	 		insert fg;
	 	Account acc = new Account(Name='test Account',Family_Group__c = fg.id);
	 		acc.Relationship_Number__c = 'Rel1234';
	 		insert acc;
	 	AccountShare ash = new AccountShare(AccountId = acc.id,UserOrGroupId = usr1.id,AccountAccessLevel = 'Read',
	 						OpportunityAccessLevel = 'Read',CaseAccessLevel = 'Read');
	 		insert ash;	
	 	Family_Group__Share fgShr = new Family_Group__Share(ParentId = fg.id,UserOrGroupId = usr1.id,AccessLevel = 'Edit');
	 		insert fgShr;
	 	system.assertEquals(acc.Family_Group__c, fg.id);
	 	system.assertEquals(fgShr.AccessLevel,'Edit');
	 	
	 	//create records for delete Family_Group__share
	 	Family_Group__c fg1 = new Family_Group__c(Name ='test Fg1');
	 		insert fg1;
	 	system.assert(fg1.id != null,'Family Group Created successfully');
	 	Family_Group__Share fgShr1 = new Family_Group__Share(ParentId = fg1.id,UserOrGroupId = usr1.id,AccessLevel = 'Edit');
	 		insert fgShr1; 	
		system.assertEquals(fgShr1.UserOrGroupId,  usr1.id);
		
	 	//create record for insert Family_Group__share
	 	Family_Group__c fg2 = new Family_Group__c(Name ='test Fg2');
	 		insert fg2; 
	 	system.assert(fg2.id != null,'Family Group Created successfully');	
	 	Account acc2 = new Account(Name='test Account2',Family_Group__c = fg2.id);
	 		acc2.Relationship_Number__c = 'Rel1235';
	 		insert acc2;
	 	AccountShare ash2 = new AccountShare(AccountId = acc2.id,UserOrGroupId = usr1.id,AccountAccessLevel = 'Read',
	 						OpportunityAccessLevel = 'Read',CaseAccessLevel = 'Read');
	 		insert ash2;	  		
	 	system.assertEquals(acc2.Family_Group__c,  fg2.id);
	 	system.assertEquals(ash2.AccountId,  acc2.id); 		
	 		Test.StartTest();
	 		FamilyGroupSharingBatch familyGrp = new FamilyGroupSharingBatch();
	 		database.executeBatch(familyGrp);
	 		Test.stopTest();
	 } 
}