/*
    
     Authors :  Don Koppel
     Created Date: 2014-07-02
     Last Modified: 2014-07-02
*/

public with sharing class FamilyGroupTrigHand {
    // template methods
    public void onAfterInsert(list<Family_Group__c> newList) {
    	updateDataSyncLog(newList, 'I');
    }
    public void onAfterUpdate(list<Family_Group__c> newList, map<id,Family_Group__c> oldMap) {
    	updateDataSyncLog(newList, 'U');
    }
    public void onAfterDelete(list<Family_Group__c> oldList){
    	updateDataSyncLog(oldList, 'D');
    }

// business logic

	//This method add records to the Data_Sync_Log__c object to track the inserts / updates / deletes synced from EDB that were
	//completed successfully.  The Informatica job that syncs Famility Group EDB-->SFDC will use this data as a source to update
	//status within EDB and will then delete these records.
    private void updateDataSyncLog (list<Family_Group__c> recordList, String operation) {
    	
    	//records should only be added to the data sync log if the current user is the Bessemer Integration user
    	if (UserInfo.getUserName() == GlobalSettings__c.getInstance().Integration_User_Name__c){
    	
	    	list<Data_Sync_Log__c> syncList = new list<Data_Sync_Log__c>();
	 		for (Family_Group__c familyGroup : recordList){
	 			if(familyGroup.EDB_Entity_Id__c <> null){
		 			Data_Sync_Log__c syncRecord = new Data_Sync_Log__c();
		 			syncRecord.EDB_Entity_Id__c = familyGroup.EDB_Entity_Id__c;
		 			syncRecord.Entity__c = 'FML_GRP';
		 			syncRecord.Action__c = Operation;
		 			syncList.Add(syncRecord);
	 			}
	 		}

			if(synclist.size() > 0)
				insert syncList;
    	}
    }       
}