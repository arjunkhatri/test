@isTest(SeeAllData=true)
public with sharing class TestRelationshipTrigHandler {
   static testMethod void testRelationshipTrigHandler() 
    { 
		Profile myProf = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
		system.assert(myProf.id != null,'Profile id fetched successfully');
		
		User u = TestDataFactory.createTestUser(myProf.id); 
		insert u;    	
		system.assert(u.id != null,'user record created successfully');
		    	
        Account acc = TestDataFactory.createTestAccount();
        insert acc;
        system.assert(acc.id != null,'Account record created successfully');
        
        Contact con = TestDataFactory.createTestContact(acc);
        insert con;
 		system.assert(con.id != null,'Contact record created successfully');   
    	List<Account> acnt = [Select Ownerid from Account where id=:acc.id limit 1];
    	system.debug('<<<>>>'+acnt);
    	acnt[0].Ownerid = u.id;
    	update acnt;
    }
}