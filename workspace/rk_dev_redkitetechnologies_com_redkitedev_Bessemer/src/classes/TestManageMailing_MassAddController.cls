@isTest
private class TestManageMailing_MassAddController {
	
	@isTest static void testAdd() {
		GlobalSettings__c setting = TestDataFactory.createGlobalSetting();
		setting.Relationship_Number_Seq__c = 1201;
		setting.Integration_User_Name__c = userinfo.getUserName();
		insert setting;

		Account acc = TestDataFactory.createTestAccount();
        insert acc;
        
        Contact con = TestDataFactory.createTestContact( acc );
        con.Contact_Preference__c = 'Mail;Do Not Mail';
        con.Entity_Type__c = 'searchtext';
        insert con;
        
        Addresses__c testAdd = new Addresses__c();
        testAdd.Contact__c = con.Id;
        testAdd.City__c = 'new york';
        testAdd.State__c = 'ny';
        testAdd.Primary_Address__c = true;
        insert testAdd;

        Test.startTest();
        	PageReference testPage = Page.ManageMailing_Edit;
			testPage.getParameters().put( ManageMailing_MassAddController.PARAM_MAILINGS, 'mailings' );
			testPage.getParameters().put( ManageMailing_MassAddController.PARAM_TYPE, 'type' );
			Test.setCurrentPage( testPage );

        	ApexPages.StandardController std = new ApexPages.StandardController( new Mailing__c() );
        	ManageMailing_MassAddController ctrl = new ManageMailing_MassAddController( std );

        	ctrl.cityFilter = 'new york';
        	ctrl.stateFilter = 'ny';
        	ctrl.filterText = 'search';
        	System.debug ( '--->' + [SELECT MailingCity, MailingState,Entity_Type__c FROM Contact]);
        	ctrl.searchContacts();
        	//check all
        	ctrl.selectAll();
        	//uncheck all
        	ctrl.selectAll();

        	//check all for saving
        	ctrl.selectAll();
        	ctrl.addContacts();

        Test.stopTest();

	}
}