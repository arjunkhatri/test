@isTest(SeeAllData=false)

public with sharing class Test_TrigRelationshipTrigHandler {
   
   static testMethod void testRelationshipTrigHandler_Test1() 
   { 
   		id prospId;
   		id clientId;
   		List<RecordType> myProfl = [SELECT Id,Name FROM RecordType WHERE Name = 'Prospective Relationship' or 
   																		 Name ='Client Relationship'];
   		for(RecordType rTyp:myProfl){
   			if(rTyp.Name == 'Prospective Relationship')
   				prospId = rTyp.id;
   			if(rTyp.Name == 'Client Relationship')
   				clientId = rTyp.id;   				
   		}
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1111;
        gs.Relationship_Number_Seq__c  =1111;
        gs.Address_Number_Seq__c = 2222;
        gs.Compliance_Email_Address__c = 'test@test.com' ;
        insert gs;

        Profile myProf = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        system.assert(myProf.id != null,'Profile id fetched successfully');
        
        Profile myProf1 = [SELECT Id FROM Profile WHERE Name = 'Salesforce Developer'];
        
        User u = TestDataFactory.createTestUser(myProf.id); 
        insert u;
        system.assert(u.id != null,'user record created successfully');
        User u1 = TestDataFactory.createTestUser(myProf1.id); 
        u1.Username = 'test1@domain.com.dev';
        insert u1;
        Account acc = TestDataFactory.createTestAccount();
        acc.RecordTypeId = prospId;
        acc.OwnerId = u1.id;
        insert acc;
        system.assert(acc.id != null,'Account record created successfully');

        Relationship_Team_Member__c accTM = TestDataFactory.createRelTeamMem(acc.id,u.id,'Senior Client Advisor');
        insert accTM;
        system.assert(accTM.id != null,'Created account team member record');

        Bessemer_Role_To_Team__c bRole = new Bessemer_Role_To_Team__c(Name = 'Washington DC Client Advisor' ,
                                                                      Client_Advisor_Team__c = 'Washington DC Client Advisor',
                                                                      Role_Name__c = '1.02.04.01.02 Washington DC Client Advisor',
                                                                      Wealth_Advisor_Team__c='Washington DC Wealth Advisor');
        insert bRole;
        system.assert(bRole.id != null,'Created Bessemer Role To Team record');

        Contact con = TestDataFactory.createTestContact(acc);
        insert con;
        system.assert(con.id != null,'Contact record created successfully');   
        List<Account> acnt = [Select Ownerid from Account where id=:acc.id limit 1];
        acnt[0].Ownerid = u.id;
        acnt[0].RecordTypeId = clientId;
        acnt[0].Cash_Short_Term_Market_Value__c  = 70;
        acnt[0].Market_Value__c = 2;
        update acnt;

       


    }
    static testMethod void testRelationshipTrigHandler_Test2() 
    { 
        GlobalSettings__c gs = new GlobalSettings__c();
        gs.Contact_S_Number_Seq__c = 1234;
        gs.Relationship_Number_Seq__c  =5678;
        gs.Address_Number_Seq__c = 9876;
        gs.Compliance_Email_Address__c = 'test1@test.com' ;
        gs.Integration_User_Name__c = UserInfo.getUserName() ;
        insert gs;

        Profile myProf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        system.assert(myProf.id != null,'Profile id fetched successfully');
        
        User u = TestDataFactory.createTestUser(myProf.id); 
        insert u;
        system.assert(u.id != null,'user record created successfully');

        Account acc = TestDataFactory.createTestAccount();
        acc.EDB_Entity_Id__c  = 'ABCD';
        insert acc;
        system.assert(acc.id != null,'Account record created successfully');

        Relationship_Team_Member__c accTM = TestDataFactory.createRelTeamMem(acc.id,u.id,'Senior Client Advisor');
        insert accTM;
        system.assert(accTM.id != null,'Created account team member record');

        Bessemer_Role_To_Team__c bRole = new Bessemer_Role_To_Team__c(Name = 'Washington DC Client Advisor' ,
                                                                      Client_Advisor_Team__c = 'Washington DC Client Advisor',
                                                                      Role_Name__c = '1.02.04.01.02 Washington DC Client Advisor',
                                                                      Wealth_Advisor_Team__c='Washington DC Wealth Advisor');
        insert bRole;
        system.assert(bRole.id != null,'Created Bessemer Role To Team record');

        Contact con = TestDataFactory.createTestContact(acc);
        insert con;
        system.assert(con.id != null,'Contact record created successfully');   
        List<Account> acnt = [Select Ownerid from Account where id=:acc.id limit 1];
        acnt[0].Ownerid = u.id;
        update acnt;
       delete acnt;
		
    }
}