@isTest
public with sharing class testMethod_Trigger_Contact {
    static testMethod void testMethod_Trigger_Contact_test() 
    {
        Account acc = TestDataFactory.createTestAccount();
        insert acc;
        system.assert(acc.id != null,'Account record created successfully');
        
        Account acc2 = TestDataFactory.createTestAccount();
        insert acc2;            
        system.assert(acc2.id != null,'Account record created successfully');
        
        Contact con = TestDataFactory.createTestContact(acc);
        insert con;
 		system.assert(con.id != null,'Contact record created successfully');   
 		     
        List<Contact> cont = [Select id,AccountId from Contact where id=:con.id limit 1];
        cont[0].AccountId = acc2.id;
        update cont;     
    }
}