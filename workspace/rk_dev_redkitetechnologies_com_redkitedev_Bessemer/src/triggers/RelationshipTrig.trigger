/*
    
     Authors :  Don Koppel
     Created Date: 2014-07-20
     Last Modified: 2014-10-28
*/

trigger RelationshipTrig on Account (after delete, after insert, after undelete, 
after update, before delete, before insert, before update)  
{
    RelationshipTrigHandler handler  = new RelationshipTrigHandler();
    //Handle Update Actions
    if (Trigger.isInsert) {
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.New);
    }
    if(Trigger.isUpdate){     
        if (Trigger.isAfter){
        	if(RelationshipTrigHandler.runOnce()){
            	handler.onAfterUpdate(Trigger.New,Trigger.oldMap); 
        	}
        }
        if (Trigger.isBefore){
            handler.onBeforeUpdate(Trigger.New,Trigger.oldMap); 
        }        
    }   
    if (Trigger.isDelete)
    {
        if (Trigger.isBefore)
             handler.onBeforeDelete(Trigger.Old);
        if (Trigger.isAfter)
             handler.onAfterDelete(Trigger.Old);
    
    }
       
}