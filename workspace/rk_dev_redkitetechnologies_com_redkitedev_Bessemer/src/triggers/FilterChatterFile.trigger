trigger FilterChatterFile on ContentVersion (before insert) {
	if(trigger.isBefore && trigger.isInsert){
	    for (ContentVersion cv : Trigger.new) {
	        // Origin is 'H' for Chatter files
	        if (cv.Origin == 'H') {
	            cv.addError(System.Label.File_Upload_Not_Allowed);
	        }
	    }
	}
}