/**
* ReferralTrigger : Trigger code for Referral object
*/
trigger ReferralTrigger on Referral__c (before insert, after insert, before update, before delete, after undelete) {
	ReferralTriggerHandler handler = new ReferralTriggerHandler(); 
	if (trigger.isBefore){
		if(Trigger.isInsert)
			handler.onBeforeInsert(Trigger.new);
		else if(trigger.isUpdate)
			handler.onBeforeUpdate(Trigger.oldMap, Trigger.new);
		else if(trigger.isDelete)
			handler.onBeforeDelete(Trigger.oldMap);
	}
	else{
		if(trigger.isInsert){
			handler.onAfterInsert(Trigger.new);
		}
		else if(trigger.isUnDelete)
			handler.onAfterUndelete(Trigger.new);
	}
}