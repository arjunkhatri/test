trigger FeedItemTrig on FeedItem (after insert, after update, before insert ,before delete) {
	FeedItemTrigHand handler = new FeedItemTrigHand();
	if(trigger.isAfter){
		if(trigger.isInsert){
			handler.mailPost(trigger.new); 
		}
		if(trigger.isUpdate){
			handler.mailPost(trigger.new);
		}
	}
	if(trigger.isBefore){
        if(trigger.isInsert){
            handler.ensureFeedItem(trigger.new); 
        }
		if(trigger.isDelete){
			handler.beforeDelete(trigger.old);
		}
	}
}