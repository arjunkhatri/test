/*
    
     Authors :  Don Koppel
     Created Date: 2014-08-20
     Last Modified: 2014-08-20
*/

trigger AccountContactSyncTrig on Account_Contact_Sync__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    AccountContactSyncTrigHand handler = new AccountContactSyncTrigHand ();
    
    if (Trigger.isInsert)
    {
        if (Trigger.isBefore)
            handler.onBeforeInsert(Trigger.New);
            
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.New);
    }
    if (Trigger.isUpdate)
    {
        if (Trigger.isBefore)
            handler.onBeforeUpdate(Trigger.New, Trigger.oldMap);
        if (Trigger.isAfter)
            handler.onAfterUpdate(Trigger.New , Trigger.newMap);
    }
    
	if (Trigger.isDelete){
		//if (Trigger.isBefore)
		//	handler.onBeforeDelete(Trigger.Old);
		if (Trigger.isAfter)
        	handler.onAfterDelete(Trigger.Old);
	}

}