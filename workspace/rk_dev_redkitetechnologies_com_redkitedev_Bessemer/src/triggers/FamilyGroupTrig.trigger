/*
    
     Authors :  Don Koppel
     Created Date: 2014-07-02
     Last Modified: 2014-07-02
*/

trigger FamilyGroupTrig on Family_Group__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    FamilyGroupTrigHand handler = new FamilyGroupTrigHand ();
    
    if (Trigger.isInsert) {
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.New);
    }
    if (Trigger.isUpdate) {
        if (Trigger.isAfter)
            handler.onAfterUpdate(Trigger.New , Trigger.newMap);
    }
    
    if (Trigger.isDelete)
        handler.onAfterDelete(Trigger.Old);
}