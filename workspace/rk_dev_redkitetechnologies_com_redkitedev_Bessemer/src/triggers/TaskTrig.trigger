trigger TaskTrig on Task (before delete, before insert, before update) {
    TaskTrigHand handler = new TaskTrigHand ();
    if(Trigger.isBefore){
	    if (Trigger.isInsert) {
	    	handler.onBeforeInsert(Trigger.New);
	    }
	    if (Trigger.isUpdate) {
	    	handler.onBeforeUpdate(Trigger.New,Trigger.oldMap);
	    }
	    if (Trigger.isDelete){
	        handler.onBeforeDelete(Trigger.Old);
	    }
    }
}