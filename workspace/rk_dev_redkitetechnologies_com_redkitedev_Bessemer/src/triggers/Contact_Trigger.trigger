/*
    
     Authors :  David Brandenburg, Don Koppel
     Created Date: 2014-04-09
     Last Modified: 2014-07-17
*/


trigger Contact_Trigger on Contact (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
    Contact_TriggerHandler handler = new Contact_TriggerHandler (); 
    
    //Handle Insert Actions
    if (Trigger.isInsert)
    {    
        if (Trigger.isAfter)
            handler.onAfterInsert(Trigger.New);
        if (Trigger.isBefore)
            handler.onBeforeInsert(Trigger.New);    
    }
    //Handle Update Actions
    if(Trigger.isUpdate){     
        if (Trigger.isBefore)
            handler.onBeforeUpdate(Trigger.New,Trigger.oldMap);  
        if (Trigger.isAfter){
        	if(Contact_TriggerHandler.runOnce())
            	handler.onAfterUpdate(Trigger.New , Trigger.newMap ,Trigger.oldMap );
        }
    }

    if (Trigger.isDelete)
    {
        if (Trigger.isBefore)
            handler.onBeforeDelete(Trigger.Old);
        if (Trigger.isAfter)
            handler.onAfterDelete(Trigger.Old);
    }
    
}