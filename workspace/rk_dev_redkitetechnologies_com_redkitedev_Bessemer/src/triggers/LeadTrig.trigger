/*
    
     Authors :  Don Koppel
     Created Date: 2014-08-08
     Last Modified: 2014-08-08
*/

trigger LeadTrig on Lead (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    LeadTrigHand handler = new LeadTrigHand ();
    
    if (Trigger.isInsert) 
    {
    	if (Trigger.isBefore)
    		handler.onBeforeInsert(Trigger.New);
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.New);
    }
    if (Trigger.isUpdate) 
    {
    	if (Trigger.isBefore)
            handler.onBeforeUpdate(Trigger.New , Trigger.newMap);	
        if (Trigger.isAfter)
            handler.onAfterUpdate(Trigger.New , Trigger.newMap);
    }
    
    if (Trigger.isDelete)
        handler.onAfterDelete(Trigger.Old);

}