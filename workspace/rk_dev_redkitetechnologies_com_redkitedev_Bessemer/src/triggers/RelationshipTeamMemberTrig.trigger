/*
    
     Authors :  Don Koppel
     Created Date: 2014-10-08
     Last Modified: 2014-10-08
*/

trigger RelationshipTeamMemberTrig on Relationship_Team_Member__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    RelationshipTeamMemberTrigHand handler = new RelationshipTeamMemberTrigHand ();
    
    if (Trigger.isInsert)
    {
        if (Trigger.isBefore)
            handler.onBeforeInsert(Trigger.New);
            
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.NewMap);
    }
    if (Trigger.isUpdate)
    {
        if (Trigger.isBefore)
            handler.onBeforeUpdate(Trigger.New, Trigger.oldMap);
        if (Trigger.isAfter)
            handler.onAfterUpdate(Trigger.NewMap , Trigger.OldMap);
    }
    
	if (Trigger.isDelete){
		//if (Trigger.isBefore)
		//	handler.onBeforeDelete(Trigger.Old);
		if (Trigger.isAfter)
        	handler.onAfterDelete(Trigger.OldMap);
	}
}