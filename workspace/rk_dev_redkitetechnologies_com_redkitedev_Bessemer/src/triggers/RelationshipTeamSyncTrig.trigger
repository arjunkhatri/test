/*
    
     Authors :  Don Koppel
     Created Date: 2014-09-09
     Last Modified: 2014-09-09
*/

trigger RelationshipTeamSyncTrig on Relationship_Team_Sync__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    RelationshipTeamSyncTrigHand handler = new RelationshipTeamSyncTrigHand ();
    
    if (Trigger.isInsert)
    {
        if (Trigger.isBefore)
            handler.onBeforeInsert(Trigger.New);
            
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.New);
    }
    if (Trigger.isUpdate)
    {
        if (Trigger.isBefore)
            handler.onBeforeUpdate(Trigger.New, Trigger.oldMap);
        if (Trigger.isAfter)
            handler.onAfterUpdate(Trigger.New , Trigger.newMap);
    }
    
	if (Trigger.isDelete){
		//if (Trigger.isBefore)
		//	handler.onBeforeDelete(Trigger.Old);
		if (Trigger.isAfter)
        	handler.onAfterDelete(Trigger.Old);
	}

}