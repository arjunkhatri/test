/*
    
     Authors :  Don Koppel
     Created Date: 2014-07-20
     Last Modified: 2014-07-20
*/

trigger AccountTrig on Accounts__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    AccountTrigHand handler = new AccountTrigHand ();
    
    if (Trigger.isInsert) {
        if (Trigger.isAfter) 
            handler.onAfterInsert(Trigger.New);
    }
    if (Trigger.isUpdate) {
        if (Trigger.isAfter)
            handler.onAfterUpdate(Trigger.New , Trigger.newMap);
    }
    
    if (Trigger.isDelete)
        handler.onAfterDelete(Trigger.Old);
}