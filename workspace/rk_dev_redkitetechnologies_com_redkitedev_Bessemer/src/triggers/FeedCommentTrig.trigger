trigger FeedCommentTrig on FeedComment (after insert, after update, before delete) {
	FeedCommentTrigHand handler = new FeedCommentTrigHand();
	if(trigger.isAfter){
		if(trigger.isInsert){ 
			handler.mailComment(trigger.new); 
		}
		if(trigger.isUpdate){
			handler.mailComment(trigger.new);
		}
	}
	if(trigger.isBefore){ 
		if(trigger.isDelete){ 
			handler.beforeDelete(trigger.old);
		}
	}	
}