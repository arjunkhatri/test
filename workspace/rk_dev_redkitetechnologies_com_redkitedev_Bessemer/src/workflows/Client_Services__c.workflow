<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Client Service Util Compiler</fullName>
        <field>Service_Utilization_Actual__c</field>
        <formula>Service_Utilization_Score__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Client Service Util Compiler</fullName>
        <actions>
            <name>Client Service Util Compiler</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Services__c.Service_Utilization_Score__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This WFR is needed to assign a rating to the &quot;Service Utilization Score&quot;  on the Client Services object. We could not originally reference the score because the formula field exceeded the character limit and therefore had to be compiled using a WFR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
