<apex:page standardcontroller="Contact">
<apex:messages />
	<apex:sectionheader title="{!$ObjectType.Contact.label} Edit" subtitle="{!IF(ISNULL(Contact.Name), 'New Contact',Contact.Name)}"/>
	<apex:form >
		<apex:pageblock mode="edit" title="{!$ObjectType.Contact.label} Edit">
			<apex:pageblockbuttons >
				<apex:commandbutton value="Save" action="{!Save}"/>
				<apex:commandbutton value="Cancel" action="{!Cancel}"/>
			</apex:pageblockbuttons>

			<!-- **********   [Record Type : Entity Contact ]   **********  -->
			<apex:outputpanel rendered="{!CASESAFEID(Contact.RecordTypeId)='012f0000000D3apAAC'}">
				<apex:pageblocksection title="Contact Information" showheader="true" columns="2">
					<apex:pageblocksectionitem >
						<apex:outputlabel value="First Name"/>
						<apex:outputpanel >
							<apex:inputfield value="{!Contact.Salutation}" required="false"/>
							<apex:inputfield value="{!Contact.FirstName}" required="false"/>
						</apex:outputpanel>
					</apex:pageblocksectionitem>
					<apex:inputfield value="{!Contact.Contact_S_Number__c}" required="false"/>
					<apex:inputfield value="{!Contact.LastName}" required="true"/>
					<apex:outputfield value="{!Contact.RecordTypeId}"/>
					<apex:inputfield value="{!Contact.Nickname__c}" required="false"/>
					<apex:inputfield value="{!Contact.occupation_type__c}" required="false"/>
					<apex:inputfield value="{!Contact.Middle_Initial__c}" required="false"/>
					<apex:inputfield value="{!Contact.Specific_Occupation__c}" required="false"/>
					<apex:inputfield value="{!Contact.AccountId}" required="true"/>
					<apex:inputfield value="{!Contact.AssistantName}" required="false"/>
					<apex:outputfield value="{!Contact.OwnerId}"/>
					<apex:inputfield value="{!Contact.AssistantPhone}" required="false"/>
					<apex:inputfield value="{!Contact.Formal_Salutation__c}" required="false"/>
					<apex:inputfield value="{!Contact.Assistant_Email__c}" required="false"/>
				</apex:pageblocksection>
				<apex:pageblocksection title="New Section" showheader="false" columns="1">
					<apex:inputfield value="{!Contact.Notes__c}" required="false"/>
				</apex:pageblocksection>
				<apex:pageblocksection title="Address Information" showheader="true" columns="2">
					<apex:inputfield value="{!Contact.MailingStreet}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.MailingCity}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.MailingState}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.MailingPostalCode}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.MailingCountry}" required="false"/>
					<apex:pageblocksectionitem />
				</apex:pageblocksection>
				<apex:pageblocksection title="Contact Details" showheader="true" columns="2">
					<apex:inputfield value="{!Contact.MobilePhone}" required="false"/>
					<apex:inputfield value="{!Contact.Email}" required="false"/>
					<apex:inputfield value="{!Contact.HomePhone}" required="false"/>
					<apex:inputfield value="{!Contact.Email_Address_2__c}" required="false"/>
					<apex:inputfield value="{!Contact.Home_Phone_2__c}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.Office_Phone__c}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.Office_Phone_2__c}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.OtherPhone}" required="false"/>
					<apex:pageblocksectionitem />
				</apex:pageblocksection>
				<apex:pageblocksection title="Other Information" showheader="true" columns="2">
					<apex:inputfield value="{!Contact.Contact_Preference__c}" required="false"/>
					<apex:inputfield value="{!Contact.Marital_Status__c}" required="false"/>
					<apex:inputfield value="{!Contact.Cease_Communications__c}" required="false"/>
					<apex:inputfield value="{!Contact.Spouse__c}" required="false"/>
					<apex:inputfield value="{!Contact.Communications_Comment__c}" required="false"/>
					<apex:inputfield value="{!Contact.Children__c}" required="false"/>
					<apex:inputfield value="{!Contact.Board_Affiliaations__c}" required="false"/>
					<apex:inputfield value="{!Contact.Charitable_Causes__c}" required="false"/>
					<apex:inputfield value="{!Contact.School_Affiliations__c}" required="false"/>
					<apex:inputfield value="{!Contact.Hobbies_Interests__c}" required="false"/>
				</apex:pageblocksection>
				<apex:pageblocksection title="Know your Client" showheader="true" columns="2">
					<apex:inputfield value="{!Contact.SSN_TIN__c}" required="false"/>
					<apex:inputfield value="{!Contact.ID_1_Include_Type_CountryState__c}" required="false"/>
					<apex:inputfield value="{!Contact.Birthdate}" required="false"/>
					<apex:inputfield value="{!Contact.ID_2_Include_Type_CountryState__c}" required="false"/>
					<apex:inputfield value="{!Contact.Gender__c}" required="false"/>
					<apex:inputfield value="{!Contact.Background_Check__c}" required="false"/>
					<apex:inputfield value="{!Contact.Country_of_Birth__c}" required="false"/>
					<apex:inputfield value="{!Contact.Other_Search_es_Optional__c}" required="false"/>
					<apex:inputfield value="{!Contact.Country_of_Residence__c}" required="false"/>
					<apex:inputfield value="{!Contact.US_Political_Figure__c}" required="false"/>
					<apex:inputfield value="{!Contact.Citizenship_Type__c}" required="false"/>
					<apex:inputfield value="{!Contact.Is_Foreign_Political_Figure__c}" required="false"/>
					<apex:inputfield value="{!Contact.Country_of_Citizenship__c}" required="false"/>
					<apex:inputfield value="{!Contact.Sr_Foreign_Pol_Figure__c}" required="false"/>
					<apex:inputfield value="{!Contact.Foreign_Tax_ID_Number_TIN__c}" required="false"/>
					<apex:inputfield value="{!Contact.Section_16_Insider__c}" required="false"/>
					<apex:inputfield value="{!Contact.Account_Activity__c}" required="false"/>
					<apex:inputfield value="{!Contact.Risk_Rating__c}" required="false"/>
					<apex:inputfield value="{!Contact.Affil_w_Broker_Dealer__c}" required="false"/>
					<apex:pageblocksectionitem />
				</apex:pageblocksection>
				<apex:pageblocksection title="Know Your Client" showheader="false" columns="2">
					<apex:inputfield value="{!Contact.Ifno_ID_Obtained_Explain__c}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.Source_of_Wealth__c}" required="false"/>
					<apex:pageblocksectionitem />
					<apex:inputfield value="{!Contact.KYC_QC_Comments__c}" required="false"/>
					<apex:pageblocksectionitem />
				</apex:pageblocksection>
				<apex:pageblocksection title="Business Information" showheader="true" columns="2">
					<apex:inputfield value="{!Contact.Business_Name__c}" required="false"/>
					<apex:inputfield value="{!Contact.Business_Owner__c}" required="false"/>
					<apex:inputfield value="{!Contact.Percentage_Owned__c}" required="false"/>
					<apex:inputfield value="{!Contact.Title}" required="false"/>
					<apex:inputfield value="{!Contact.Est_Value_of_Business__c}" required="false"/>
					<apex:inputfield value="{!Contact.Type_of_Business__c}" required="false"/>
				</apex:pageblocksection>
			</apex:outputpanel>
		</apex:pageblock>
	</apex:form>
	<center><br/>
		<i style="font-size:10px;color:#aaa;">Generated By LAYOUT PAGE <a href="http://www.clerisoft.com" target="_blank">[http://www.clerisoft.com]</a> (2008-2014) : Wed Aug 27 2014 14:58:40 GMT-0600 (Mountain Daylight Time)</i>
	</center><br/>
</apex:page>