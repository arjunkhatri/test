trigger LoanSponsorTrigger on Loan_Sponsor__c (before insert, before update,after insert,after update,after delete) {
    
    LoanSponsorTriggerHandler objLSHandler = new LoanSponsorTriggerHandler();
    
   if(Trigger.isBefore){
        if(Trigger.isInsert){
            objLSHandler.OnBeforeInsert(Trigger.new);
        }
        else{
            objLSHandler.OnBeforeUpdate(Trigger.new);
        }
    }
     else if(Trigger.isAfter){
        if(Trigger.isInsert){
            objLSHandler.OnAfterInsert(Trigger.newMap);
        }
        else if(Trigger.isDelete){
            objLSHandler.OnAfterDelete(Trigger.old);
        }
        else if(Trigger.isUpdate){
            objLSHandler.OnAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }
     } 
}