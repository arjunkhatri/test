trigger LoanRequiredFieldValidationTrigger on Loan__c (before insert, before update,after insert, after update) {
	
	LoanRequiredFieldValidationHandler objLoanReqdField = new LoanRequiredFieldValidationHandler();
	
	if(Trigger.isInsert && Trigger.isAfter)
		objLoanReqdField.OnAfterInsert(Trigger.new);
	else if(Trigger.isAfter && Trigger.isUpdate){
		if (!LoanRequiredFieldValidationHandler.hasFired()) {		
			objLoanReqdField.OnAfterUpdate(Trigger.new,Trigger.old);
		}
	}
	else if(Trigger.isBefore && Trigger.isInsert)
		objLoanReqdField.OnBeforeInsert(Trigger.new);
	else if(Trigger.isBefore && Trigger.isUpdate){
		if (!LoanRequiredFieldValidationHandler.beforeUpdateFired()) 
			objLoanReqdField.OnBeforeUpdate(Trigger.new,Trigger.old);
	}
}