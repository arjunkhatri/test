trigger CBSATrigger on CBSA__c (before delete) {
	
	CBSAHandler objHandler = new CBSAHandler();
	
	if(trigger.isBefore){
		if(trigger.isDelete){
			objHandler.OnBeforeDelete(trigger.oldMap);
		}
	}
}