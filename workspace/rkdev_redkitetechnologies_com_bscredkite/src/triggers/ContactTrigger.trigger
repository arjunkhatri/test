trigger ContactTrigger on Contact (before update,before delete) {
	
	ContactTriggerHandler objHandler = new ContactTriggerHandler();
	if(trigger.isBefore){ 
		if(trigger.isDelete){
			objHandler.onBeforeDelete(trigger.old);
		}else if(trigger.isUpdate){ 
			objHandler.onBeforeUpdate(trigger.new,trigger.oldMap);
		}
	}
}