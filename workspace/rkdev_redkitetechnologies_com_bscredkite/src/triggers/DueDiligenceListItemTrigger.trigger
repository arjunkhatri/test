trigger DueDiligenceListItemTrigger on DDL_Item__c (before delete, before insert, before update) {
    
    DueDiligenceListItemHandler objHandler = new DueDiligenceListItemHandler();
    
    if(trigger.isBefore){
        if(trigger.isInsert){
            objHandler.OnBeforeInsert(Trigger.new);
        }else if(trigger.isUpdate){
            if(!DueDiligenceListItemHandler.hasUpdateFired())
                objHandler.OnBeforeUpdate(Trigger.oldMap ,Trigger.newMap);
        }
        else{
            objHandler.OnBeforeDelete(Trigger.old);
        }
    }
}