trigger AccountTrigger on Account (before update,before delete,after delete) {
	AccountHandler objHandler = new AccountHandler();
	if(trigger.isBefore){ 
		if(trigger.isUpdate){
			objHandler.onBeforeUpdate(trigger.oldMap,trigger.newMap);
		}else if(trigger.isDelete){  
			objHandler.onBeforeDelete(trigger.old);
		}   
	}
	if(trigger.isAfter){   
		if(trigger.isDelete){
			objHandler.onAfterDelete(trigger.old,AccountHandler.mpAccToLSpon);
		}
	}
}