trigger ZipCodeTrigger on Zip_Code__c (after insert,after update,after delete) {
	
	ZipCodeHandler objHandler = new ZipCodeHandler();
	if(trigger.isAfter){
		
		if(trigger.isInsert){
			objHandler.OnAfterInsert(trigger.new);
		}
		else if(trigger.isUpdate){
			objHandler.OnAfterUpdate(trigger.oldMap, trigger.new);
		}
		else if(trigger.isDelete){
			objHandler.OnAfterDelete(trigger.old);
		}
	}
}