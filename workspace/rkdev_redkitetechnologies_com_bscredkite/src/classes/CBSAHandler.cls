/*
 *	Description - Handler for CBSATrigger
 *  
 *    Version          Date          Description
 *     1.0           08/10/2013     -On Delete of CBSA update the Zip Code record
 */
public with sharing class CBSAHandler {
	
	public void OnBeforeDelete(map<Id,CBSA__c> oldCBSAMap){
		
		list<Zip_Code__c> updatedZipCodeList = new list<Zip_Code__c>();
		
		//query all the zip code where the CBSA lookup is to the deleted records
		for(Zip_Code__c zipCode : [Select CBSA__c from Zip_Code__c 
												  where CBSA__c IN: oldCBSAMap.keySet()]){
			zipCode.CBSA__c = null;
			updatedZipCodeList.add(zipCode);
		} 
		
		if(updatedZipCodeList.size() > 0)
			update updatedZipCodeList;
	}

	//Test Method
	static testMethod void testUpdateZipCOde(){
		//Create MSA
		CBSA__c cbsa = new CBSA__c(Name = 'Test CBSA');
		insert cbsa;
		
		CBSA__c unknownCBSA = new CBSA__c(Name = 'UNKNOWN');
		insert unknownCBSA;
		
		// Create Zip Code
		Zip_Code__c zip = new Zip_Code__c(Name = 'Test Zip', CBSA__c = cbsa.Id ,Zip_Code_County__c = 'Zip County');
		insert zip;
		
		// Create Property
		Property__c objProperty = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
	 											  City__c = 'Test City',State__c='LA',Postal_Code__c= 'Test Zip',
	 											  County__c = 'County',Unit_Count__c = 1.2);
	 	insert objProperty;
	 	
	 	test.startTest();
	 		//Delete the CBSA record
	 		delete cbsa;
	 		
	 		zip = [Select CBSA__c from Zip_Code__c where Id =: zip.Id];
			
			//After delete of the CBSA the CBSA of Zip becomes null 
			system.assert(zip.CBSA__c == null);
			
			//If the CBSA of Zip is null assign the CBSA as UNKNOWN
			objProperty = [Select CBSA__c from Property__c where Id=: objProperty.Id ];
			system.assert(objProperty.CBSA__c == unknownCBSA.Id); 		
	 		
	 	test.stopTest();
	 	
	}	
}