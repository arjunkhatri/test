/*
 *	Description - Handler for ZipCodeTrigger
 *  
 *    Version          Date          Description
 *     1.0           08/10/2013     -On update of ZipCode MSA/County update the Property record
 */

public with sharing class ZipCodeHandler {
	
	public void OnAfterInsert(list<Zip_Code__c> newZipCodeList){
		updateProperty(null,newZipCodeList);
	}
	
	public void OnAfterUpdate(map<Id,Zip_Code__c> oldZipCodeMap,list<Zip_Code__c> newZipCodeList){
		updateProperty(oldZipCodeMap,newZipCodeList);
	}
	
	public void OnAfterDelete(list<Zip_Code__c> oldZipCodeList){
		
		set<String> setDelZipCode = new set<String>();
		list<Property__c> updatedPropertyList = new list<Property__c>();
		
		for(Zip_Code__c zipCode :oldZipCodeList){
			setDelZipCode.add(zipCode.Name);
		}
		
		//Query CBSA where NAme is Unknown
		list<CBSA__c> lstCBSA = [Select Id,
										Name
									  from CBSA__c
									  where Name =: 'UNKNOWN' LIMIT 1];
		
		if(lstCBSA.size() > 0)
		for(Property__c objProperty : [Select CBSA__c,
  	        								  County__c,
										      Postal_Code__c
									  from Property__c where Postal_Code__c IN: setDelZipCode]){
		
			
			objProperty.CBSA__c = lstCBSA[0].Id	;	
			objProperty.County__c = null;
			updatedPropertyList.add(objProperty);						  	
		}
		
		if(updatedPropertyList.size() > 0)
			update updatedPropertyList;
	}
	
	public void updateProperty(map<Id,Zip_Code__c> oldZipCodeMap,list<Zip_Code__c> newZipCodeList){
		
		map<String ,Zip_Code__c > zipCodeMap = new map<String,Zip_Code__c> ();
		list<Property__c> updatedPropertyList = new list<Property__c>();
		
		for(Zip_Code__c zipCode : newZipCodeList){
			
			//If the CBSA or the County is changed  
			if(oldZipCodeMap == null || ( oldZipCodeMap.size() > 0 && 
											(zipCode.CBSA__c	!= oldZipCodeMap.get(zipCode.Id).CBSA__c 
			                               	|| zipCode.Zip_Code_County__c != oldZipCodeMap.get(zipCode.Id).Zip_Code_County__c)
			                            )
			   ){
				
				zipCodeMap.put(zipCode.Name ,zipCode);					
			}			
		}
		
		//Query CBSA where NAme is Unknown
		list<CBSA__c> lstCBSA = [Select Id,
										Name
									  from CBSA__c
									  where Name =: 'UNKNOWN' LIMIT 1];
		
		//Query the Property records where the postal Code is same as that of the updated Zip Code
		for(Property__c objProperty : [Select CBSA__c,
  	        								  County__c,
										      Postal_Code__c
									  from Property__c where Postal_Code__c IN: zipCodeMap.keySet()]){

			if(zipCodeMap.size() > 0 && zipCodeMap.containsKey(objProperty.Postal_Code__c)){
				
				//If CBSA is Changed
				if(zipCodeMap.get(objProperty.Postal_Code__c).CBSA__c == null &&  lstCBSA.size() > 0)
					objProperty.CBSA__c = lstCBSA[0].Id;
				else if(oldZipCodeMap == null || ( oldZipCodeMap.size() > 0 &&
													oldZipCodeMap.containsKey(zipCodeMap.get(objProperty.Postal_Code__c).Id) &&
													zipCodeMap.get(objProperty.Postal_Code__c).CBSA__c != oldZipCodeMap.get(zipCodeMap.get(objProperty.Postal_Code__c).Id).CBSA__c )) 
					objProperty.CBSA__c = zipCodeMap.get(objProperty.Postal_Code__c).CBSA__c;
				//if County is Changed
				if(oldZipCodeMap == null || ( oldZipCodeMap.size() > 0 &&
											  oldZipCodeMap.containsKey(zipCodeMap.get(objProperty.Postal_Code__c).Id) &&
											   zipCodeMap.get(objProperty.Postal_Code__c).Zip_Code_County__c != oldZipCodeMap.get(zipCodeMap.get(objProperty.Postal_Code__c).Id).Zip_Code_County__c ))	
					objProperty.County__c = zipCodeMap.get(objProperty.Postal_Code__c).Zip_Code_County__c;					
				updatedPropertyList.add(objProperty);
			}
		}
		
		if(updatedPropertyList.size() > 0 )
			update updatedPropertyList;		
	}
	
	
	//Test Method
	static testMethod void testUpdateProperty(){
		//Create MSA
		CBSA__c cbsa = new CBSA__c(Name = 'Test CBSA');
		insert cbsa;
		
		CBSA__c cbsa1 = new CBSA__c(Name = 'Test Other CBSA');
		insert cbsa1;
		
		CBSA__c unknownCBSA = new CBSA__c(Name = 'UNKNOWN');
		insert unknownCBSA;
		
		// Create Zip Code
		Zip_Code__c zip = new Zip_Code__c(Name = 'Test Zip', CBSA__c = cbsa.Id ,Zip_Code_County__c = 'Zip County');
		insert zip;
		
		Zip_Code__c zip1 = new Zip_Code__c(Name = 'Other Zip', CBSA__c = cbsa1.Id ,Zip_Code_County__c = 'Zip1 County');
		insert zip1;
		
		// Create Property
		Property__c objProperty = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
	 											  City__c = 'Test City',State__c='LA',Postal_Code__c= 'Test Zip',
	 											  County__c = 'County',Unit_Count__c = 1.2);
	 	insert objProperty;
	 	
	 	Property__c objProperty1 = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
	 											  City__c = 'Test City',State__c='LA',Postal_Code__c= 'Other Zip',
	 											  County__c = 'County',Unit_Count__c = 1.2);
	 	insert objProperty1;
	 	
	 	Property__c objProperty2 = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
	 											  City__c = 'Test City',State__c='LA',Postal_Code__c= 'Other Zip',
	 											  County__c = 'County',Unit_Count__c = 1.2);
	 	insert objProperty2;
	 	
	 	Property__c objProperty3 = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
	 											  City__c = 'Test City',State__c='LA',Postal_Code__c= 'New Zip',
	 											  County__c = 'County',Unit_Count__c = 1.2);
	 	insert objProperty3;
	 	
	 	list<Zip_Code__c> zipCodeList = new list<Zip_Code__c>();
	 	
	 	test.startTest();
		 	//Change the County and MSA for ZIp Code
		 	zip.CBSA__c = cbsa1.Id;
		 	zip.Zip_Code_County__c = 'Other County';
		 	
		 	zip1.CBSA__c = cbsa.Id;
		 	zip1.Zip_Code_County__c = 'Other  Zip1 County';
		 	
		 	zipCodeList.add(zip);
		 	zipCodeList.add(zip1);
		 	update zipCodeList;
		 	
		 	//Check if the Property record is updated
		 	objProperty = [Select County__c,CBSA__c from Property__c where Id =: objProperty.Id];
		 	objProperty1 = [Select County__c,CBSA__c from Property__c where Id =: objProperty1.Id];
		 	objProperty2 = [Select County__c,CBSA__c from Property__c where Id =: objProperty2.Id];
		 	
		 	system.assert(objProperty.CBSA__c == cbsa1.Id && objProperty.County__c == zip.Zip_Code_County__c);
		 	system.assert(objProperty1.CBSA__c == cbsa.Id && objProperty1.County__c == zip1.Zip_Code_County__c);
		 	system.assert(objProperty2.CBSA__c == cbsa.Id && objProperty2.County__c == zip1.Zip_Code_County__c);
		
			//delete zip
			delete zip1;
			
			//Check if the CBSA lookup is assigned to Unknown CBSA record
			objProperty1 = [Select County__c,CBSA__c from Property__c where Id =: objProperty1.Id];
		 	objProperty2 = [Select County__c,CBSA__c from Property__c where Id =: objProperty2.Id];
		 	
		 	system.assert(objProperty1.CBSA__c == unknownCBSA.Id);
		 	system.assert(objProperty2.CBSA__c == unknownCBSA.Id);
		 	
		 	Zip_Code__c newZip = new Zip_Code__c(Name = 'New Zip', CBSA__c = cbsa1.Id ,Zip_Code_County__c = 'Zip1 County');
			insert newZip;
			
			objProperty3 = [Select County__c,CBSA__c from Property__c where Id =: objProperty3.Id];
			system.assert(objProperty3.CBSA__c == newZip.CBSA__c);
			system.assert(objProperty3.County__c == newZip.Zip_Code_County__c);
			
		test.stopTest();
	}
	
}