global with sharing class DueDiligenceList_Generate_New    
{
    WebService static String generateDueDiligence(String DDLId)
    {
        //fetching the loan data for the selected loan.
        DDL__c objDDL = [Select Id,Loan__r.Loan_Status__c,Loan__r.Investor_Type__c,Loan__r.Early_Rate_Lock__c,Due_Diligence_List_Created__c,Type__c From DDL__c where id=:DDLId];               
        
        //checking whether investor type is null.
        if(objDDL.Loan__r.Investor_Type__c != null)
        {
            //check whether Due_Diligence_List_Created__c is true if yes display error else go for further processing.
            if(objDDL.Due_Diligence_List_Created__c == true)
            {
                return System.Label.DueDiligenceGen_AlreadyGenerated;
            }
            else
            {
                //select all the Due_Diligence_List_Item__c whose loan type includes the selected loan type.
                String strQuery = 'SELECT Item_Desc__c,Id,Sort_Order__c FROM Due_Diligence_List_Item__c WHERE Loan_Program__c includes (\''+objDDL.Loan__r.Investor_Type__c+'\') AND ERL__c ='+objDDL.Loan__r.Early_Rate_Lock__c+' AND Section__c =\''+objDDL.Type__c+'\'';
                List<DDL_Item__c> lstDueDiligenceList = new List<DDL_Item__c>();
                //as we are creating Due diligence list so we are making "Due_Diligence_List_Created__c" field in loan as true. 
                objDDL.Due_Diligence_List_Created__c = true;
                //here we create due diligence list.
                for(Due_Diligence_List_Item__c objDDLL : database.query(strQuery))
                {
                    DDL_Item__c objDDLI = new DDL_Item__c();
                    objDDLI.Due_Diligence_List_Library__c = objDDLL.Id;
                    objDDLI.Due_Diligence_List__c = objDDL.Id;
                    objDDLI.Item_Desc__c = objDDLL.Item_Desc__c;
                    objDDLI.Sort_Order__c = objDDLL.Sort_Order__c;
                    lstDueDiligenceList.add(objDDLI);
                }
                try
                {
                    if(lstDueDiligenceList.size() > 0)
                    {
                        insert lstDueDiligenceList;
                        update objDDL;
                    }
                    //if insert and update is successful return null.
                    return null;                                                
                }
                catch(DMLException e)
                {
                    if(e.getDmlType(0) == System.StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY)
                        return System.Label.DueDiligenceGen_Error;
                    else
                        return e.getMessage()+'|'+e.getStackTraceString();
                }
            }
        }
        //if loan type is null display error.
        else
        {
            return System.Label.DueDiligenceGen_NoLoanTypeError;
        }
    }
    
    public static testMethod void testForDueDiligence()
    {
        Account objAcc = new Account();
        objAcc.Name = 'test';
        insert objAcc;
        
        RecordType objRecId = [SELECT Name,Id FROM RecordType WHERE Name='Fannie' Limit 1];
        
        Loan__c objLoan = new Loan__c();
        objLoan.RecordTypeId = objRecId.Id;
        objLoan.AccountId__c = objAcc.Id;
        objLoan.Name = 'testLoan';
        objLoan.Investor_Type__c = 'Fannie';
        objLoan.Loan_Status__c = 'Prospecting';
        objLoan.Due_Diligence_List_Created__c = false;
        objLoan.Early_Rate_Lock__c = true;
        insert objLoan;
        
        system.assert(objLoan.Id != null, 'There is problem in inserting loan');
        
        // insert a test DDI list item
        Due_Diligence_List_Item__c objDDLL = new Due_Diligence_List_Item__c();
        objDDLL.Item_Desc__c = 'testList';
        objDDLL.Loan_Program__c = 'Fannie';
        objDDLL.ERL__c = true;
        objDDLL.Section__c = 'test';
        insert objDDLL;
        
        system.assert(objDDLL.Id != null, 'There is problem in inserting Due Diligence List Item');
        
        // insert a test DDL
        DDL__c objDDL = new DDL__c();
        objDDL.Due_Diligence_List_Created__c = false;
        objDDL.Type__c = 'Key Principal';
        objDDL.Loan__c = objLoan.Id;
        objDDL.Type__c = 'test';
        insert objDDL; 
        
        system.assert(objDDL.Id != null, 'There is problem in inserting Due Diligence List');

        // Modified : as DDL item are now created on insert of DDL so set it to false
        objDDL.Due_Diligence_List_Created__c = false;
        update objDDL;

        generateDueDiligence(objDDL.Id);
        
        objDDL.Due_Diligence_List_Created__c = true;
        update objDDL;
        
        system.assert(objDDL.Due_Diligence_List_Created__c, 'The Due Diligence List Created is not set to true');
        
        generateDueDiligence(objDDL.Id);
        
    }
}