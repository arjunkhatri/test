/*
	Batch class sending email to deal team members of loans whose required fields are missing to move to next status
*/
global class EmailRequiredFieldsBatch implements Database.Batchable<Sobject>,Schedulable{   
	public List<Sobject> listLoans;
	public List<Sobject> filteredLoans;
	public List<String> listLoanIds; 
	public Map<String,list<String>> mpChildsReqFields;
	public Map<String, Sobject> loanMap;
	public Map<String,String> mpSobjtoLoan;//for meny to meny relationship object
	public Map<String,List<String>> mpLoantoChildReq;
	public Map<String,List<String>> mpLoanToDealTeam;
	public Map<String,List<String>> mpLoanToDealTeamCC;
	public Map<ID,List<ID>> mpLoanToParents;
	public Map<String,String> mpLoanToNextStatus;
	public MailDetailConfig__c mdc; 
	public static final String oneToMany = 'One-to-Many';
	public static final String manyToMany = 'Many-to-Many';
	public static final String self = 'Self';
	public static final String Subchild = 'Subchild';
	public static final String loanTerm = 'Loan_Term__c';
	public static final String strLoan = 'Loan__c';		
	public map<String, Schema.SObjectType> schemaMap;
	public Set<String> setDealTeamRole;
	public Set<String> setDealTeamRoleCC;
	public set<String> priorToFields;
	public List<PriorDaysConfig__c> pDays;
	public EmailRequiredFieldsBatch(){
		setDealTeamRole = new Set<String>();
		setDealTeamRoleCC = new Set<String>();	
		//get mail detail config to addresses
		mdc = MailDetailConfig__c.getInstance('Addresses');
		if(mdc != null){
			if(mdc.ToAddress__c != null && mdc.ToAddress__c != ''){ 
				if(mdc.ToAddress__c.contains(',')){
					for(String strTo : mdc.ToAddress__c.split(',')){
						if(strTo != null && strTo != '')
							setDealTeamRole.add(strTo.trim());
					}
				}
				else{ 
					setDealTeamRole.add(mdc.ToAddress__c.trim());
				}
			}
			if(mdc.CCAddress__c != null && mdc.CCAddress__c != ''){ 
				if(mdc.CCAddress__c.contains(',')){ 
					for(String strCC:mdc.CCAddress__c.split(',')){
						if(strCC != null && strCC != '')
						setDealTeamRoleCC.add(strCC.trim());
					}
				} 
				else{
					setDealTeamRoleCC.add(mdc.CCAddress__c.trim());
				}
			}
		}
		priorToFields = new set<String>();  
		pDays = PriorDaysConfig__c.getall().values();
		if(pDays != null && pDays.size() > 0){
			for(PriorDaysConfig__c pdc: pDays){
				if(pdc.Object__c == strLoan)
					priorToFields.add(pdc.PriorToDateField__c);
			}
		}					
	}
	public void getFilteredLoans(){	
		filteredLoans = new List<Sobject>();
		List<String> strFields = new List<String>();
		strFields = getObjFields('Loan__c');
		if(strFields != null && strFields.size() > 0){
			String strQuery = 'SELECT ';
			for(String strFld:strFields){ 
				strQuery = strQuery + strFld + ',';
			} 
			strQuery = strQuery +' (Select Role__c, UserId__c,Email__c From Deal_Team_Member__r) From Loan__c'; 
			listLoans = Database.query(strQuery);
			if(priorToFields != null && priorToFields.size() > 0){
				for(Sobject lObj:listLoans){
					Boolean skipLoan = false;
					//check prior to field should not null
					for(String strField:priorToFields){
						if(lObj.get(strField) == null){ 
							skipLoan = true;
						}
					}
					if(!skipLoan){ 
						//filter for Underwriter
						Boolean isReqDealTeam = false;
						for(Sobject dtMem:lObj.getSObjects('Deal_Team_Member__r')){
							String dtRole = String.valueof(dtMem.get('Role__c'));
							if(setDealTeamRole != null && (setDealTeamRole.contains(dtRole) || setDealTeamRoleCC.contains(dtRole))){
								isReqDealTeam = true;
							}
						}
						if(isReqDealTeam){  
							//filter for Prior days
							Boolean isInPriorDays = false;
							for(PriorDaysConfig__c pdc:pDays){
								if(pdc.Object__c == strLoan){
									Integer noOfDays = Date.today().daysBetween(Date.valueof(lObj.get(pdc.PriorToDateField__c)));
									if(pdc.Days__c == noOfDays){
										isInPriorDays = true;
									}
								}
							}
							if(isInPriorDays){
								filteredLoans.add(lObj);
							}
						}
					}
				}
			}	
		}
	}
	
	public void initData(List<Sobject> scope){
		listLoanIds = new List<String>();
		loanMap = new Map<String, Sobject>();
		mpLoanToDealTeam = new  Map<String,List<String>>();
		mpLoanToDealTeamCC = new  Map<String,List<String>>(); 
		mpLoanToParents = new  Map<ID,List<ID>>();
		mpSobjtoLoan = new Map<String, String>();
		for(Sobject lObj:scope){ 
			for(Sobject dtMem:lObj.getSObjects('Deal_Team_Member__r')){
				if(setDealTeamRole != null && setDealTeamRole.contains(String.valueof(dtMem.get('Role__c')))){
					if(!mpLoanToDealTeam.containsKey(String.valueof(lObj.get('Id')))){
						mpLoanToDealTeam.put(String.valueof(lObj.get('Id')),new List<String>{String.valueof(dtMem.get('Email__c'))});
					}else if (mpLoanToDealTeam.containsKey(String.valueof(lObj.get('Id')))){
						mpLoanToDealTeam.get(String.valueof(lObj.get('Id'))).add(String.valueof(dtMem.get('Email__c')));
					}
				}
				//fill map of loan vs deal team cc addresses
				if(setDealTeamRoleCC != null && setDealTeamRoleCC.contains(String.valueof(dtMem.get('Role__c')))){
					if(!mpLoanToDealTeamCC.containsKey(String.valueof(lObj.get('Id')))){
						mpLoanToDealTeamCC.put(String.valueof(lObj.get('Id')),new List<String>{String.valueof(dtMem.get('Email__c'))});
					}else if (mpLoanToDealTeamCC.containsKey(String.valueof(lObj.get('Id')))){
						mpLoanToDealTeamCC.get(String.valueof(lObj.get('Id'))).add(String.valueof(dtMem.get('Email__c')));
					}
				}							
				loanMap.put(lObj.id,lObj);
				listLoanIds.add(lObj.id);
			}
		}	
	}
    global Iterable<Sobject> start(Database.BatchableContext BC){ 
    	getFilteredLoans();
    	return filteredLoans;
    }
	
    global void execute(SchedulableContext sc) {
      database.executebatch(new EmailRequiredFieldsBatch());
    }
	global void execute(Database.BatchableContext BC, List<Sobject> scope){ 
		initData(scope);
		getLoanNextStatus(scope);
		schemaMap = Schema.getGlobalDescribe();
		mpLoantoChildReq = new  Map<String,List<String>>();
		mpChildsReqFields  = new Map<String,list<String>>();
	    List<Loan_Relationship_Config__c>	relationshipConfig =  Loan_Relationship_Config__c.getall().values();
	    if(relationshipConfig != null && relationshipConfig.size() > 0){
			    for(Loan_Relationship_Config__c relObj:relationshipConfig){
		    		List<Sobject> listChildObject = getLoanChildObjectRecods(relObj.name);
		    		if(listChildObject != null && listChildObject.size() > 0){
		    			getMissingChildObjectFields(listChildObject,relObj.name);
		    		}
			    }
	    }
	    if(!mpChildsReqFields.isEmpty()){
	    	getEmailBodyData();
	    }
	    if(!mpLoantoChildReq.isEmpty()){
	    	sendMidssingFieldsEmail();
	    }
	}
	global void finish(Database.BatchableContext BC){
	   	
	}
	//function to get all fields of object given to it
	public List<String> getObjFields(String objName){
		List<String> listFields = new List<String>();
		Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
		if(globalDescription.containskey(objName)){
			Schema.sObjectType objType = globalDescription.get(objName); 
			Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
			Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
			for(Schema.SObjectField field : mapFieldList.values())  
			{  
			    Schema.DescribeFieldResult fieldResult = field.getDescribe();  
			    if(fieldResult.isAccessible())  
			    {  
			        listFields.add(fieldResult.getName());
			    }  
			}
		}
		return 	listFields;
	}

	public List<Sobject> getLoanChildObjectRecods(String childObjName){
		Set<Id> setRecordId = new Set<Id>();
		Set<Id> setChild = new Set<Id>();
		List<Sobject> listSobject = new List<Sobject>();
		String Query;
		String loanField;
			Query = 'SELECT ';
			for(String strField:getObjFields(childObjName)){
				if(Query == 'SELECT '){
					Query += strField;
				}else{
					Query += ','+ strField;
				}
			}
			if(Query != 'SELECT '){
				Query += ' FROM '+ childObjName + ' WHERE '; 
				Loan_Relationship_Config__c	relationshipConfig =  Loan_Relationship_Config__c.getInstance(childObjName);		
				if(relationshipConfig != NULL && relationshipConfig.Type__c != NULL &&
					(relationshipConfig.Type__c.equalsIgnoreCase(oneToMany) 
			    	|| relationshipConfig.Type__c.equalsIgnoreCase(self)))		
				{
					loanField = relationshipConfig.Loan_Field__c;
					Query += ' '+relationshipConfig.Loan_Field__c+'=: listLoanIds';
			    }
			    else if(relationshipConfig != NULL && relationshipConfig.Type__c != NULL && 
		    		relationshipConfig.Type__c.equalsIgnoreCase(ManyToMany))
			    {
			        List<String> lstRelFields = relationshipConfig.Relationship_Field__c.split(',');
			        List<String> lstLoanFields = relationshipConfig.Loan_Field__c.split(',');
			        integer idx = 0; 
			    	for(String obj : relationshipConfig.Related_Objects__c.split(','))
			    	{	
			    		if(lstRelFields[idx] != NULL && lstLoanFields[idx] != NULL)
			    		{
					    	String relQuery = 'Select '+lstRelFields[idx]+','+lstLoanFields[idx]+' from '+obj+' where '+lstLoanFields[idx]+' =: listLoanIds';//TODO 
							List<sObject> lstObj = Database.query(relQuery);
							if(lstObj != null && lstObj.size() > 0)
							{
								for(sobject obj1 : lstObj){
									setRecordId.add(String.valueOf(obj1.get(lstRelFields[idx])));
									mpSobjtoLoan.put(String.valueOf(obj1.get(lstRelFields[idx])),String.valueOf(obj1.get(lstLoanFields[idx])));
								}
							}
							idx++;
			    		}
			    	}
			    	Query += ' Id IN :setRecordId';		    
			    }
			    else{
			    	Query = Query.substringBefore(' FROM '+ childObjName + ' WHERE ');
			    	Query += ',( select id From Loans__r )' + ' FROM '+ childObjName + ' WHERE Id IN ';
			    	Query += '(Select ' + relationshipConfig.Relationship_Field__c + ' from Loan__c where Id=: listLoanIds)';
			    		for(Sobject sObj: Database.query(Query)){
							listSobject.add(sObj);
							for(Sobject lObj:sObj.getSObjects('Loans__r')){ 
								if(!mpLoanToParents.containsKey(String.valueOf(lObj.get('id')))){
									mpLoanToParents.put(String.valueOf(lObj.get('id')),new List<Id>{String.valueOf(sObj.get('Id'))});
								}
								else if(mpLoanToParents.containsKey(String.valueOf(lObj.get('id')))){ 
									mpLoanToParents.get(String.valueOf(lObj.get('id'))).add(String.valueOf(sObj.get('Id')));
								}
							}
			    		}
			    	return listSobject;    	
			    }
			    if(Query != null){
				    //if no loan term is final then check in all loan term related to loan
				    if(childObjName.equalsIgnoreCase(loanTerm)){
				    	listSobject = getLoanTermNotFinal(Query,loanField);
				    }else{		    
				    	listSobject = Database.query(Query);
				    }
			    }
			}
		return listSobject;
	}
	
	public List<Sobject> getLoanTermNotFinal(String termQuery,String loanField){
		List<Sobject> listTermObj = new List<Sobject>();
		map<String,List<Sobject>> mpLoantoListTerm = new map<String,List<Sobject>>();//contains all loan term
		for(Sobject termObj:Database.query(termQuery)){
			String loanId  = String.valueOf(termObj.get(loanField));
			//fill map of loan vs all loan term
			if(!mpLoantoListTerm.containsKey(loanId)){
				mpLoantoListTerm.put(loanId,new List<Sobject>{termObj});
			}
			else if(mpLoantoListTerm.containsKey(loanId)){
				mpLoantoListTerm.get(loanId).add(termObj);
			}
		}
		for(String strObj:mpLoantoListTerm.keySet()){
			Boolean isFinal = false;
			for(Sobject lstTerm : mpLoantoListTerm.get(strObj)){
				if(Boolean.valueOf(lstTerm.get('Final__c')) == true ){
					listTermObj.add(lstTerm);
					isFinal = true;
				}
			}
			if(!isFinal){
				listTermObj.addAll(mpLoantoListTerm.get(strObj));
			}
		}
		return listTermObj;
	}
	
	public void getMissingChildObjectFields(List<Sobject> listChildObj , String childObjName){
		Loan_Relationship_Config__c	relationshipConfig =  Loan_Relationship_Config__c.getInstance(childObjName);
		// Fetch all the records from custom setting in a map
 		map<String,LoanValidationConfig__c> loanValidationConfig  = LoanValidationConfig__c.getAll();	
 		map<String, Integer> StatusToIndex = new map<String, Integer>();
		Schema.SObjectType schemaObjType = schemaMap.get(childObjName);
		Map<String, Schema.SObjectField> fieldMap = schemaObjType.getDescribe().fields.getMap();
	    for(LoanValidationConfig__c obj : loanValidationConfig.values()){
	    	if(obj.Object__c == childObjName){ 
		        Set<String> setExecutionType = new Set<String>();
		        	if(obj.Execution_Type__c != NULL){
	                    for(String execType : obj.Execution_Type__c.split(',')){
	                        setExecutionType.add(execType.trim().toLowerCase());
	                    }
		            }
	                for(Sobject childObj:listChildObj){
	                	if(!StatusToIndex.isEmpty())
	                		StatusToIndex.clear();
		            	List<String> listStatus = new List<String>();
		            	String loanProgram;
		            	String strExecutionType;
		            	String loanId;
		            	String loanStatus;
		            	if(relationshipConfig != null){
			            	if(relationshipConfig.Type__c.equalsIgnoreCase(oneToMany) || 
			            	   relationshipConfig.Type__c.equalsIgnoreCase(self)){
			            		loanId = String.valueof(childObj.get(relationshipConfig.Loan_Field__c));
			            	}else if(relationshipConfig.Type__c.equalsIgnoreCase(manyToMany)){
			            		loanId = String.valueof(mpSobjtoLoan.get(String.valueof(childObj.get('Id'))));
			            	}
			            	else{
			            		for(Id lId:mpLoanToParents.keySet()){
			            			for(Id pId:mpLoanToParents.get(lId)){
			            				if(String.valueOf(childObj.get('Id')) == String.valueOf(pId)){
			            					String strLid = String.valueOf(lId);
			            					if(loanMap.containsKey(strLid)){
			            						loanId = strLid;
			            						break;
			            					}
			            				}
			            			} 	
			            		}
			            	}
		            	}
		            	if(loanId != null && loanId != '' && loanMap.containsKey(loanId)){ 
		            		loanProgram = String.valueof(loanMap.get(loanId).get('Loan_Program__c'));
		                	//get loan field values
							strExecutionType = String.valueof(loanMap.get(loanId).get('Investor_Type__c'));	            	
		                	loanStatus = String.valueof(loanMap.get(loanId).get('Loan_Status__c'));		            		
		            	}
		            	if(loanProgram != null && loanProgram != ''){ 
				    		Loan_Status_Config__c loanStatusCS = Loan_Status_Config__c.getInstance(loanProgram);
				    		if(loanStatusCS != null && loanStatusCS.Status__c.contains(',')){
				    			listStatus = loanStatusCS.Status__c.split(',');
				    		}else if(loanStatusCS != null){
				    			listStatus.add(loanStatusCS.Status__c);
				    		} 		
				        	Integer index = 0;
				        	for(String lnStatus : listStatus){
				        		StatusToIndex.put(lnStatus.trim().toLowerCase(),index);
				        		index++;
				        	} 
		            	}	                	

		                //Find match based on loan status , Execution Type and existing status
		                if(obj.Loan_Program__c != NULL && obj.Existing_Status__c != NULL && obj.New_Status__c != NULL &&
		                    obj.Object__c != NULL && obj.Required_Fields__c != NULL && obj.Execution_Type__c != NULL &&
		                    obj.Loan_Program__c.equalsIgnoreCase(loanProgram) && 
		                    setExecutionType.contains(strExecutionType.toLowerCase()) &&
		                    StatusToIndex.containsKey(obj.Existing_Status__c.trim().toLowerCase()) &&
		                    StatusToIndex.containsKey(loanStatus.trim().toLowerCase()) &&
		                    StatusToIndex.get(obj.Existing_Status__c.trim().toLowerCase()) <= StatusToIndex.get(loanStatus.trim().toLowerCase()) && 
		                    obj.Object__c == childObjName)
		                {	
		                	String objectType;
							objectType = GetObjectTypeHelper.getKeyPrefix((String)childObj.get('Id'));
							String childName = String.valueof(childObj.get('Name'));
		                	List<String> lstReqFields = new List<String>();
		                	if(obj.Required_Fields__c.contains(',')){
		                		lstReqFields = obj.Required_Fields__c.split(',');
		                	}
		                	else{
		                		lstReqFields.add(obj.Required_Fields__c);
		                	}
							for(String strReq:lstReqFields){ 
								if(childObj.get(strReq.trim()) == null){
				                    if(!mpChildsReqFields.containsKey(String.valueof(objectType+'#'+childName+'#'+loanId+'#'+childObj.get('Id')))){
				                        mpChildsReqFields.put(String.valueof(objectType+'#'+childName+'#'+loanId+'#'+childObj.get('Id')),new list<String>{fieldMap.get(strReq.trim()).getDescribe().getLabel()});
				                    }
				                    else if(mpChildsReqFields.containsKey(String.valueof(objectType+'#'+childName+'#'+loanId+'#'+childObj.get('Id')))){
				                        mpChildsReqFields.get(String.valueof(objectType+'#'+childName+'#'+loanId+'#'+childObj.get('Id'))).add(fieldMap.get(strReq.trim()).getDescribe().getLabel());
				                    }								
								}
							}
		                }
	                }
		    	}
			}
	}
	
	public void getEmailBodyData(){
		for(String mpKey:mpChildsReqFields.keySet()){
			if(mpKey.contains('#')){
				List<String> lstData = mpKey.split('#');
				if(lstData != null && lstData.size() == 4){
					String strReqFields = ': ';
					for(String strFld:mpChildsReqFields.get(mpKey)){
						if(strReqFields == ': '){
							strReqFields += strFld;
						}else{
							strReqFields += ','+strFld;
						} 
					}
					if(!mpLoantoChildReq.containsKey(lstData[2])){
						mpLoantoChildReq.put(lstData[2],new List<String>{lstData[0]+'_'+lstData[1]+'_'+lstData[3]+'_'+strReqFields});
					}
					else if (mpLoantoChildReq.containsKey(lstData[2])){
						mpLoantoChildReq.get(lstData[2]).add(lstData[0]+'_'+lstData[1]+'_'+lstData[3]+'_'+strReqFields);
					}
				}
			}
		}
	}
	//function to collect loan vs next status map	
	public void getLoanNextStatus(List<Loan__c> scopeLoans){
		mpLoanToNextStatus = new map<String,String>();
		map<String,String> mpLoanStatus = new map<String,String>();
		for(Loan_Status_Config__c lsc : Loan_Status_Config__c.getall().values()){
			mpLoanStatus.put(lsc.Name,lsc.Status__c);
		}
		
		for(Loan__c ln:scopeLoans){
			if(mpLoanStatus.containsKey(ln.Loan_Program__c)){
				String strStatus = String.valueof(mpLoanStatus.get(ln.Loan_Program__c));
				if(strStatus != null && strStatus != ''){
					List<String> lstSts = strStatus.split(',',-1);
					for(Integer i = 0;i <= lstSts.size() - 1; i++){
						if(lstSts[i] == ln.Loan_Status__c && i+1 < lstSts.size())
							mpLoanToNextStatus.put(ln.id,lstSts[i+1]);
					}
				}
			}
		}
	}
	//sending mail to Deal team members
	public void sendMidssingFieldsEmail(){ 
		List<Messaging.SendEmailResult> listEmailResult = null; 
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		String sfUrl ='https://'+URL.getSalesforceBaseUrl().getHost();
		List<OrgWideEmailAddress> fromAdd = [Select Id From OrgWideEmailAddress limit 1];
		for(String mpKey:mpLoantoChildReq.keySet()){
			String Subject = '';
			String mBody = '';
				Subject =  mdc.Subject__c;
				mBody = mdc.Mail_Text__c;
			if(mdc.Mail_Text1__c != null && mdc.Mail_Text__c != ''){ 
				mBody += ' '+ mdc.Mail_Text1__c;
			}	
			if(Subject.contains('{!Loan__c.Name}')){
				Subject = Subject.replace('{!Loan__c.Name}',String.valueOf(loanMap.get(mpKey).get('Name'))); 
			}
			String htmlBody = '<HTML>';
			for(String lstChild:mpLoantoChildReq.get(mpKey)){ 
				if(lstChild.contains('_')){
					List<String> strFld = lstChild.split('_');
					if(strFld != null && strFld.size() > 0){
						htmlBody += '<br/><b>'+strFld[0] +'  -  '+
						'<a href="'+sfUrl+'/'+strFld[2]+'">'+strFld[1]+'</a></b>';
						htmlBody += '  '+strFld[3] +'<br/> </HTML>';
						strFld.clear();
					}
				}
			}
			if(mBody.contains('{!Loan__c.Status}')){
				String statusMessage = 'You need to populate the following missing fields in order to move from ';
				statusMessage +=  String.valueOf(loanMap.get(mpKey).get('Loan_Status__c'));
				statusMessage += ' to '+ String.valueOf(mpLoanToNextStatus.get(mpKey));
				mBody = mBody.replace('{!Loan__c.Status}', statusMessage);
			} 
			if(mBody.contains('{!Loan__c.Estimated_Closing_Date__c}')){  
				String strDate = String.valueOf(loanMap.get(mpKey).get('Estimated_Closing_Date__c')).split(' ')[0];
				String strFormDate;
				if(strDate.contains('-')){
					List<String> lstFormat = strDate.split('-');
					strFormDate = lstFormat[1]+'/'+lstFormat[2]+'/'+lstFormat[0];
				}
				mBody = mBody.replace('{!Loan__c.Estimated_Closing_Date__c}', strFormDate); 
			}
			if(mBody.contains('{!Loan__c.Link}')){  
				mBody = mBody.replace('{!Loan__c.Link}', sfUrl+'/'+mpKey);
			}	
			if(mBody.contains('{!Loan__c.Name}')){
				mBody = mBody.replace('{!Loan__c.Name}', String.valueOf(loanMap.get(mpKey).get('Name')));
			}			
			if(mBody.contains('{!Loan__c.RequiredFields}')){ 
				mBody = mBody.replace('{!Loan__c.RequiredFields}', htmlBody);
			}					
		    Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
			    if(!mpLoanToDealTeam.isEmpty() && mpLoanToDealTeam.containsKey(mpKey)){ 
				   email.setToAddresses(mpLoanToDealTeam.get(mpKey));
			    }
			    if(fromAdd != null && fromAdd.size() > 0){  
			    	email.setOrgWideEmailAddressId(fromAdd[0].id);
			    }
			    email.setSaveAsActivity(false);
			    email.setSubject(Subject);
			    email.setHtmlBody(mBody); 
			    if(!mpLoanToDealTeamCC.isEmpty() && mpLoanToDealTeamCC.containsKey(mpKey)){
			    	email.setCcAddresses(mpLoanToDealTeamCC.get(mpKey));
			    }
			    mails.add(email);	 
		}
		try{
		    if(mails != null && mails.size() > 0){
		        listEmailResult = Messaging.sendEmail(mails);	
		    }		
		}catch(Exception e){
		}		
	}
}