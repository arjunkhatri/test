/**
 * This Test class is for the batch class ReportCommentsDueAlertBatch 
 */
@isTest
private class ReportCommentsDueAlertBatchTest {

    static testMethod void testReportCommentsDueAlertBatchTest() {
        Profile objProfile = [Select Id, Name From Profile Where Name='Loan Originator' limit 1];
        UserRole objUserRole = [Select Name, Id From UserRole where Name='Underwriter' limit 1];
        RecordType rec = [Select SobjectType, Name, Id From RecordType  where SobjectType = 'Third_Party_Report__c' limit 1];
        User objTestUser = new User(ProfileId = objProfile.Id , IsActive = true ,FirstName = 'testName',Username = 'testName@yahoo.com' ,LastName = 'TestLast',
                                    Email = 'testName@gmail.com', Alias = 'test' , CommunityNickname = 'testNickName123@' , TimeZoneSidKey = 'America/New_York',
                                    EmailEncodingKey = 'ISO-8859-1' , LanguageLocaleKey = 'en_US' , LocaleSidKey = 'en_US' ,UserRoleId = objUserRole.Id);
        insert objTestUser;
        system.assert(objTestUser.id != null,'Created user record');

            System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            PriorDaysConfig__c pdConf = new PriorDaysConfig__c(Name = '5_Report_Comments_Due__c',
                                    PriorToDateField__c = 'Report_Comments_Due__c',Days__c = 5,Object__c = 'Third_Party_Report__c');
            insert pdConf;
            system.assert(pdConf.id != null,'Created PriorDaysConfig__c record');

            MailDetailConfig__c mdc = new MailDetailConfig__c(Name = 'ReportCommentsDue',
            ToAddress__c = 'Underwriter,Loan Processer,testTo@gmail.com', Subject__c = 'Report Comments Due on {!Third_Party_Report__c.Report_Comments_Due__c} for {!Third_Party_Report__c.Loan__c}',
            Mail_Text__c = 'Fannie Loan: {!Third_Party_Report__c.Loan__c}',
            Mail_Text1__c = '{!Third_Party_Report__c.Report_Comments_Due__c}.Loan Estimated Closing Date: {!Loan__c.Estimated_Closing_Date__c} Link : {!Third_Party_Report__c.Link}',
            CCAddress__c='Loan Processer,testCC@gmail.com');
            insert mdc;
            system.assert(mdc.id != null,'Created MailDetailConfig__c record');

            Account objAcc = new Account(Name='testAcc');
            insert objAcc;

            Loan__c testLoan = new Loan__c(Name='test',Loan_Program__c ='test pgm', Loan_Status__c = 'Prospecting',
                                            Investor_Type__c = 'Fannie',Estimated_Closing_Date__c = date.today().addDays(5));
            insert testLoan;
            system.assert(testLoan.id != null,'Created Loan__c record');

            Deal_Team__c  objDealTeam = new Deal_Team__c (Role__c = 'Underwriter',LoanId__c = testLoan.id,UserId__c =objTestUser.id);
            insert objDealTeam;
            system.assert(objDealTeam.id != null,'Created Deal_Team__c record');

            Property__c objProperty = new Property__c(Name = 'test' ,  
                                                      Property_Type__c = 'Cooperative Housing' , 
                                                      Address__c = 'Test Address' , 
                                                      City__c = 'test City' ,
                                                      State__c = 'NY' ,
                                                      Postal_Code__c = 'Test Postal');
            insert objProperty;
            system.assert(objProperty.id != null,'Created Property__c record');

            Loan_Property__c objLaonProp = new Loan_Property__c( LoanId__c = testLoan.id , PropertyId__c = objProperty.id);
            insert objLaonProp;
            system.assert(objLaonProp.id != null,'Created Loan_Property__c record');

            Third_Party_Report__c objThirdParty = new Third_Party_Report__c(Report_Comments_Due__c= date.today().addDays(5) ,
                                                                            RecordTypeId = rec.id ,
                                                                            Appraiser__c = objAcc.id ,
                                                                            Draft_Due_Date__c = date.today() ,
                                                                            Engagement_Date__c = date.today() ,
                                                                            Loan_Property__c = objLaonProp.id ,
                                                                            Property__c = objProperty.id);
            insert objThirdParty;
            system.assert(objThirdParty.id != null,'Created Third_Party_Report__c record');

            ReportCommentsDueAlertBatch objReportCommentAlertBatch = new ReportCommentsDueAlertBatch();
            database.executeBatch(objReportCommentAlertBatch);
            }
    }
}