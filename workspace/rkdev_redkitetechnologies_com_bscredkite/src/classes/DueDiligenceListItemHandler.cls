/*  
 *  Descprition :Trigger Handler for DueDiligenceListItemTrigger .It is for the sort order Field
 *               of the list item.When someone deletes a list item,it should re-order automatically. 
 *               Similarly, if someone inserts/updates the order number, it should also reorder.
 *
 *  Revision History:
 *
 *   Version          Date            Description
 *   1.0            12/02/2013      Initial Draft
 *  
 */

public with sharing class DueDiligenceListItemHandler {
    
      public static boolean updateFired = false;
    
    // Function to check if the trigger for update is fired
    public static boolean hasUpdateFired() {
        return updateFired;
    }
    
    public void OnBeforeInsert(List<DDL_Item__c> lstDDLItem)
    {
        updateFired = true;
        reorder(lstDDLItem,'Insert');
    }
    
    public void OnBeforeUpdate(map<Id,DDL_Item__c> mapOldDDLItem , map<Id,DDL_Item__c> mapNewDDLItem)
    {
        updateFired = true;
        swapSortOrder(mapOldDDLItem , mapNewDDLItem);
    }
    
    public void OnBeforeDelete(List<DDL_Item__c> lstDDLItem){
        updateFired = true;
        reorder(lstDDLItem,'Delete');
    }
    
    public void reorder(List<DDL_Item__c> lstDDLItem ,String frmOp){
        
        // List contains updated DDL Items
        list<DDL_Item__c> updatedDDLItem = new List<DDL_Item__c>();
        
        //set of DDL_SortOrder of deleted DDLItems
        set<String> setDeletedDDLItem = new set<String>();
        
        //Variable contains Least sort order amongst inserted/deleted records
        Decimal leastSortOrder ;
        
        //set DDL_SortOrder of inserted records
        set<String> setSortedDDLItem = new set<String>();
        
        // Set contains Id of the records that are deleted 
        set<id> setDeletedDDLIId = new set<id>();
        //Set of DueDiligenceList ids
        set<id> setParentId = new set<id>();
        
        for(DDL_Item__c DDLItem : lstDDLItem)
        {
            if(DDLItem.Sort_Order__c != NULL)
            {
                if(frmOp.equalsIgnoreCase('Delete') && 
                             !setDeletedDDLItem.contains(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c))
                {
                    if(leastSortOrder == NULL || DDLItem.Sort_Order__c < leastSortOrder)
                    	leastSortOrder = DDLItem.Sort_Order__c;
                    	
                    //Add Id of deleted records
                    setDeletedDDLIId.add(DDLItem.id);
                    
                    //Add the Sort order of deleted records along with its Parent
                    setDeletedDDLItem.add(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c);
                    setParentId.add(DDLItem.Due_Diligence_List__c);
                }
                else if(frmOp.equalsIgnoreCase('Insert') && 
                             !setSortedDDLItem.contains(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c))
                {
                    if(leastSortOrder == NULL || DDLItem.Sort_Order__c < leastSortOrder)
                    	leastSortOrder = DDLItem.Sort_Order__c;
                    
                    // Add the recently added records to the sorted set along with its Parent
                    setSortedDDLItem.add(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c);
                    setParentId.add(DDLItem.Due_Diligence_List__c);
                }
                else{
                    if(frmOp.equalsIgnoreCase('Insert'))
                        DDLItem.addError('Two items cannot have same sort order for a Due Diligence List Items');   
                }
            }
        }
        
        if((frmOp.equalsIgnoreCase('Insert') && !setSortedDDLItem.isEmpty()) ||
           (frmOp.equalsIgnoreCase('Delete') && !setDeletedDDLItem.isEmpty()) )
        {
            String strQuery = 'Select Id,Name,Sort_Order__c,Due_Diligence_List__c from DDL_Item__c ' ;
            
            //Form Query of the records with higher sort Order
            if(frmOp.equalsIgnoreCase('Insert'))
                strQuery += 'where Sort_Order__c >=: leastSortOrder AND Due_Diligence_List__c IN: setParentId order by Sort_Order__c ASC';
            else if(frmOp.equalsIgnoreCase('Delete'))
                strQuery += 'where Sort_Order__c >: leastSortOrder AND Due_Diligence_List__c IN: setParentId AND id NOT IN :setDeletedDDLIId order by Sort_Order__c ASC';
            
            for(DDL_Item__c DDLItem : database.query(strQuery))
            {
                Decimal prevSortOrder = DDLItem.Sort_Order__c;
                Boolean isModified = false;
                
               if(frmOp.equalsIgnoreCase('Delete'))
                    DDLItem.Sort_Order__c--;
                        
                // update the sort order of DB record
                if(frmOp.equalsIgnoreCase('Insert') && !setSortedDDLItem.isEmpty() && 
                             setSortedDDLItem.contains(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c))
                {
                	isModified = true;
                    //Increment till there is no record with same sort order in sorted set
                    while(setSortedDDLItem.contains(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c)){
                        DDLItem.Sort_Order__c++;
                    }
                    setSortedDDLItem.add(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c);
                    
                }else if(frmOp.equalsIgnoreCase('Delete') && !setDeletedDDLIId.isEmpty() && 
                    		setDeletedDDLItem.contains(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c))
                {
                	isModified = true;
                    while(setDeletedDDLItem.contains(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c)){
                        DDLItem.Sort_Order__c--;    
                    }
                    
                    // remove the item that is ordered
                    setDeletedDDLItem.remove(DDLItem.Due_Diligence_List__c+'_'+(++DDLItem.Sort_Order__c));
                    
                    // Add the previous sort order of recently modified record to deleted list
                    setDeletedDDLItem.add(DDLItem.Due_Diligence_List__c+'_'+prevSortOrder);
                }
                // Populate a List of updated records
                if(isModified)
                	updatedDDLItem.add(DDLItem);
            }
            try
            {
                if(!updatedDDLItem.isEmpty())
                    update updatedDDLItem;
            }catch(DMLException dmlException)
            {
                system.debug('------DML Exception:'+dmlException.getMessage());
            }
        }
    }
    
    public void swapSortOrder(map<Id,DDL_Item__c> mapOldDDLItem , map<Id,DDL_Item__c> mapNewDDLItem){
        
        set<Decimal> setNewSortOrder = new set<Decimal>();
        
        //Set of recently modified record ids
        set<Id> setDDLIId = new set<Id>();
        
        // List contains updated DDL Items
        list<DDL_Item__c> updatedDDLItem = new List<DDL_Item__c>();
        
        //map of ParentId_Newsort order as key and its old Sort order
        map<String,Decimal> mapRecordsSortOrders = new map<String,Decimal>();
        
        //Set of parent Ids
        set<ID> setDDLId = new set<ID>();
        
        for(DDL_Item__c DDLItem : mapNewDDLItem.Values())
        {
            //If the Sort order of the record is changed add it to map
            if(DDLItem.Sort_Order__c !=NULL && 
               (DDLItem.Sort_Order__c != mapOldDDLItem.get(DDLItem.Id).Sort_Order__c))
            {
            	// Populate new Sort 
                setNewSortOrder.add(DDLItem.Sort_Order__c);
                setDDLIId.add(DDLItem.id);
                setDDLId.add(DDLItem.Due_Diligence_List__c);
                mapRecordsSortOrders.put(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c , mapOldDDLItem.get(DDLItem.Id).Sort_Order__c);
            }
        }
        if(!setNewSortOrder.isEmpty()){
            
            /* Query the records with sort order same as new sort order of same parent DDL  
             *  do not fetch if the record is being modified 
             */
            for(DDL_Item__c DDLItem : [Select Id,
                                              Name,
                                              Sort_Order__c,
                                              Due_Diligence_List__c
                                       from DDL_Item__c 
                                       where Sort_Order__c in: setNewSortOrder
                                       AND Due_Diligence_List__c in: setDDLId
                                       AND Id Not IN : setDDLIId])
            {
                Boolean isModified = false;
                //Now check for the same parent and swap sort order
                if(!mapRecordsSortOrders.isEmpty() && 
                	mapRecordsSortOrders.containsKey(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c)){
                	
                	isModified = true;
                	/* While to handle special case 
                	 * for eg if 3-->4 and 4-->5 then 5-->3
                	 */ 
                	while(mapRecordsSortOrders.containsKey(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c))
                		DDLItem.Sort_Order__c = mapRecordsSortOrders.get(DDLItem.Due_Diligence_List__c+'_'+DDLItem.Sort_Order__c);
                }       
                if(isModified)
                	updatedDDLItem.add(DDLItem);                           
            }                               
        }   
        try
        {
            if(!updatedDDLItem.isEmpty()){
                update updatedDDLItem;
            }   
        }catch(DMLException dmlException)
        {
            system.debug('------DML Exception:'+dmlException.getMessage());
        }
    }
    
    //---------------- Test Methods----------------------------//
    
    static testMethod void DDLITriggerTest()
    {
        Loan__c objLoan = new Loan__c(Name = 'testLoan',Loan_Status__c = 'Prospect',Investor_Type__c = 'Test');
        insert objLoan;
        
        Test.startTest();
        
        // Add DDLI with SO check insert delete and update case
        DDL__c objDDL = new DDL__c(Loan__c = objLoan.Id);
        insert objDDL;
        
        DDL_Item__c objDDLI1 = new DDL_Item__c(Due_Diligence_List__c = objDDL.Id , Sort_Order__c =1);
        insert objDDLI1;
        
        DDL_Item__c objDDLI2 = new DDL_Item__c(Due_Diligence_List__c = objDDL.Id , Sort_Order__c =2);
        insert objDDLI2;
        
        DDL_Item__c objDDLI3 = new DDL_Item__c(Due_Diligence_List__c = objDDL.Id , Sort_Order__c =3);
        insert objDDLI3;
        
        DDL_Item__c objDDLI4 = new DDL_Item__c(Due_Diligence_List__c = objDDL.Id , Sort_Order__c =2);
        insert objDDLI4;
        
        objDDLI2 = [Select Id,Sort_Order__c from DDL_Item__c where id = : objDDLI2.Id];
        objDDLI3 = [Select Id,Sort_Order__c from DDL_Item__c where id = : objDDLI3.Id];
        system.assertEquals(objDDLI2.Sort_Order__c ,3);
        system.assertEquals(objDDLI3.Sort_Order__c ,4);
        
        
        objDDLI2.Sort_Order__c =  4;
        updateFired = false;
        update objDDLI2;
        
        objDDLI3 = [Select Id,Sort_Order__c from DDL_Item__c where id = : objDDLI3.Id];
        // Check if the Sort order is swapped
        system.assertEquals(objDDLI2.Sort_Order__c ,4);
        system.assertEquals(objDDLI3.Sort_Order__c ,3);
        
        // Delete the record with Sort order as 1
        delete objDDLI1;
        
        //Check if Sort order of  objDDLI4 is changed from 2-->1
        objDDLI4 = [Select Id,Sort_Order__c from DDL_Item__c where id = : objDDLI4.Id];
        system.assertEquals(objDDLI4.Sort_Order__c ,1);
        
        DDL_Item__c objDDLI5 = new DDL_Item__c(Due_Diligence_List__c = objDDL.Id , Sort_Order__c =5);
        DDL_Item__c objDDLI6 = new DDL_Item__c(Due_Diligence_List__c = objDDL.Id , Sort_Order__c =5);
        
        List<DDL_Item__c> lstDDLI = new List<DDL_Item__c> {objDDLI5,objDDLI6};
        try{
            insert lstDDLI;
        }catch(Exception e){
            system.debug('------Record with same Sort order---------');
        }
        Test.stopTest();
    }
}