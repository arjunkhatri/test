/*
 * Descprition :Trigger Handler to validate if there is one Final Loan term for a loan record and 
 *               copy fields of Final Loan term to Loan object 
 *  
 *  Revision History:
 *
 *   Version          Date          Description
 *   1.0            28/09/2012      Initial Draft
 *
 *   1.1            30/10/2012      Copy all the Loan Term Section  fields from Loan term to Loan
 *									if Record type is TBD and the fields are not null
 *
 *   1.2            19/04/13        When selecting Final Loan term validate for missing fields from previous
 *                                  Loan Status.
 */
public with sharing class LoanTermTriggerHandler {
	
    
  public void OnBeforeInsert(List<Loan_Term__c> lstLoanTerm){
    this.validateAndCopy(lstLoanTerm);
  }
  
  public void OnBeforeUpdate(List<Loan_Term__c> lstLoanTerm){
    this.validateAndCopy(lstLoanTerm);
  }
  
  public void validateAndCopy(List<Loan_Term__c> pLstLoanTerm){
    // Set of Loan Id 
    Set<Id> setLoanId = new Set<Id>();
    // List of Final Loan Terms
    List<Loan_Term__c> lstFinalLT = new List<Loan_Term__c>();
    
    //Map of Loan ids and Loan Term
    Map<Id,Loan_Term__c> mapLoanId = new Map<Id,Loan_Term__c>();
    Map<Id,Loan_Term__c> mapDL = new Map<Id,Loan_Term__c>();
    map<Id,Loan__c> mapLoan ;
    List<Loan__c> lstUpdatedLoan = new List<Loan__c>();
    map<String,LoanValidationConfig__c> mapLoanValidationConfig;
    
     Set<ID> setDeselectedLT = new Set<ID>();
    
    for(Loan_Term__c objLT : pLstLoanTerm){
        if(objLT.Final__c){
            setLoanId.add(objLT.LoanId__c);
            lstFinalLT.add(objLT);
        }
        else if(!objLT.Final__c){
            if(objLT.id != NULL)
                setDeselectedLT.add(objLT.id);
        }
    }
    
    if(setLoanId.size() > 0){
    	
    	//10/04/13 : execute the check on selection of final checkbox
    	// Query the Loan to check the status 
    	mapLoan = new map<Id,Loan__c>([Select Id,Loan_Status__c,Loan_Program__c,Investor_Type__c
    	                            from Loan__c where Id IN: setLoanId]);
    	
        for(Loan_Term__c objLT : [Select Id,
                                         Name,
                                         LoanId__c 
                                  from Loan_Term__c 
                                  where LoanId__c in: setLoanId and Final__c =: true]){
            if(setDeselectedLT != NULL && !setDeselectedLT.contains(objLT.Id))
                mapLoanId.put(objLT.LoanId__c,objLT);
        }
    }
    if(!lstFinalLT.isEmpty())
    	mapLoanValidationConfig = LoanValidationConfig__c.getAll();
    
    Boolean isSkipValidation  = checkUserProfile();
    Id recTypeId = [Select Id, Name from RecordType where Name = 'TBD' limit 1].Id;
    for(Loan_Term__c objLT : lstFinalLT)
    {
    	if(isSkipValidation){
	    	//Changes :19/04/13 :check missing fields for previous Loan Status
	    	LoanRequiredFieldValidationHandler objHandler = new LoanRequiredFieldValidationHandler();
	    	map<String,Integer> mapLoanStatus = objHandler.populateLoanStatus(mapLoan.get(objLT.LoanId__c));
	    	set<String> setStatus = new Set<String>();
	    	RequiredFieldsHelper ReqdFieldsHelper = new RequiredFieldsHelper();
	    	ValidateFieldsHelper fieldsHelper = new ValidateFieldsHelper();
	    	
	    	for(String objStatus : mapLoanStatus.keySet()){
	    		if(mapLoanStatus.get(objStatus.toLowerCase()) < mapLoanStatus.get(mapLoan.get(objLT.LoanId__c).Loan_Status__c.toLowerCase())){
	    			setStatus.add(objStatus);
	    		}
	    	}
	    	for(LoanValidationConfig__c obj : mapLoanValidationConfig.values()){
	    		//Code Modified : Execution Type Field added to Loan Validation Config Custom setting
				Set<String> setExecutionType = new Set<String>();
				
				if(obj.Execution_Type__c != NULL){
					for(String execType : obj.Execution_Type__c.split(',')){
						setExecutionType.add(execType.trim().toLowerCase());
					}
				}
				List<sObject> lstSObject = new List<sObject>(); 
				// If Loan Program and existing status and execution type matches validate the records 
				if(obj.Loan_Program__c != NULL && obj.Existing_Status__c != NULL &&
				    obj.New_Status__c != NULL && obj.Object__c != NULL && obj.Object__c == 'Loan_Term__c'
				     && obj.Execution_Type__c != NULL &&
				    obj.Required_Fields__c != NULL && 
				     obj.Loan_Program__c.equalsIgnoreCase(mapLoan.get(objLT.LoanId__c).Loan_Program__c) && 
				    setStatus.contains(obj.Existing_Status__c.toLowerCase()) && 
				    setExecutionType.contains(mapLoan.get(objLT.LoanId__c).Investor_Type__c.toLowerCase()))
				{
					//Check if next status of Custom setting matches with next status on Loan
					Integer nextStatus = mapLoanStatus.get(obj.Existing_Status__c.toLowerCase());
					
					if(mapLoanStatus.get(obj.New_Status__c.toLowerCase()) == (++nextStatus))
					{
						ReqdFieldsHelper = fieldsHelper.validateFields(mapLoan.get(objLT.LoanId__c), obj.Object__c.trim(),
						                    obj.Required_Fields__c.trim(),mapLoan.get(objLT.LoanId__c).Id, 'trigger', new list<sObject>{objLT});
					}
				}
	    	}
	    	if(ReqdFieldsHelper != NULL && !ReqdFieldsHelper.mapObjectName.isEmpty()){
			 	String reqdFields;
			 	for(list<String> missingFields :ReqdFieldsHelper.mapObjectName.values() ){
			 		for(String field :  missingFields)
			 			reqdFields = reqdFields == NULL ?field : reqdFields+','+field;
			 	}
			 	objLT.addError(string.format(System.Label.LoanTermHandler_FinalMissingFieldError, new string[] {reqdFields}));
	    	}
	    	//END
    	}
    	
        Loan_Term__c objLoanTerm = mapLoanId.get(objLT.LoanId__c);
        if(mapLoanId != NULL && mapLoanId.containsKey(objLT.LoanId__c) && (objLT.Id != objLoanTerm.Id)){ 
                objLT.addError(string.format(System.Label.LoanTermHandler_ErrorMsg, 
                                  new string[] {objLoanTerm.Id,objLoanTerm.Name}));
        }
        else{
            if(mapDL != NULL && mapDL.containsKey(objLT.LoanId__c))
            {
                objLT.addError(string.format(System.Label.LoanTermHandler_ErrorMsg, 
                                             new string[] {null,null}));
            }
            else{
                Loan__c objLoan = new Loan__c(Id=objLT.LoanId__c,
                                              Borrower_Requested_Loan_Amount__c = objLT.Requested_Loan_Amount__c,
                                              Loan_Amount__c = objLT.Loan_Amount__c,
                                              Final_Loan_Amount__c = objLT.Final_Loan_Amount__c,
                                              Term__c = objLT.Term__c,
                                              Prepayment_Term__c = objLT.Prepayment_Term__c,
                                              Interest_Only_Period__c = objLT.Interest_Only_Period__c,                                          
                                              Amortization__c = objLT.Amortization__c,
                                              Interest_Rate_Accrual__c = objLT.Interest_Rate_Accrual__c,
                                              Rate_Option__c = objLT.Rate_Option__c,
                                              Estimated_Note_Rate__c = objLT.Estimated_Note_Rate__c,
                                              UW_Tier__c = objLT.UW_Tier__c,
                                              Floor_Rate__c = objLT.Floor_Rate__c,
                                              DSCR__c = objLT.DSCR__c,
                                              UWDSCR__c = objLT.UW_DSCR__c,
                                              Actual_DSCR__c = objLT.Actual_DSCR__c,
                                              LTV__c = objLT.LTV__c,
                                              LTC__c = objLT.LTC__c,
                                              Index_Type__c = objLT.Index_Type__c,
                                              Date_of_Index__c = objLT.Date_of_Index__c,
                                              Index__c = objLT.Index__c,
                                              Investor_Spread__c = objLT.Investor_Spread__c,
                                              Guarantee_Fee__c = objLT.Guarantee_Fee__c,
                                              Servicing_fee__c = objLT.Servicing_Fee__c,
                                              Outside_Origination_Fee__c = objLT.Outside_Origination_Fee__c,
                                              Inside_Origination_Fee__c = objLT.Inside_Origination_Fee__c,
                                              Pricing_Waiver__c = objLT.Pricing_Waiver__c,
                                              Pool_CUSIP__c = objLT.Pool_CUSIP__c,
                                              Risk_Sharing__c = objLT.Risk_Sharing__c,
                                              CUSIP__c = objLT.CUSIP__c,
                                              Price_Percent__c = objLT.Price_Percent__c,
                                              Obligor_Rating__c = objLT.Obligor_Rating__c,
                                              Obligor_Rating_ID__c = objLT.Obligor_Rating_ID__c,
                                              Facility_Rating__c = objLT.Facility_Rating__c,
                                              Facility_Rating_ID__c = objLT.Facility_Rating_ID__c,
                                              Interest_Only_DSCR_at_Actual_Rate__c = objLT.Interest_Only_DSCR_at_Actual_Rate__c,
                                              Final_Note_Rate__c = objLT.Final_Note_Rate__c,
                                              Final_I_O_DSCR_at_Actual_Rate__c = objLT.Final_I_O_DSCR_at_Actual_Rate__c,
                                              Final_Amort_DSCR_at_Actual_Rate__c = objLT.Final_Amort_DSCR_at_Actual_Rate__c,
                                              Final_Amort_DSCR_at_UW_Rate__c = objLT.Final_Amort_DSCR_at_UW_Rate__c,
                                              Final_LTV__c = objLT.Final_LTV__c); 
                                              
                if(objLT.RecordTypeId == recTypeId){
                    
                    if(objLT.Investor_Type__c != NULL)
                        objLoan.Investor_type__c = objLT.Investor_Type__c;
                    if(objLT.Investor__c != NULL)
                        objLoan.Investor__c = objLT.Investor__c;
                    if(objLT.Loan_Program__c != NULL)   
                        objLoan.Loan_Program__c = objLT.Loan_Program__c ;
                }
                lstUpdatedLoan.add(objLoan);
                mapDL.put(objLT.LoanId__c,objLT);
            }
        }
    }
    try{
        update lstUpdatedLoan;
    }
    catch(Exception e){
        String strError = String.valueOf(e);
        List<String> error = strError.split(',',2);
        if(e.getDmlStatusCode(0) == 'FIELD_CUSTOM_VALIDATION_EXCEPTION')
            pLstLoanTerm[0].addError(error[1]);
        else
            pLstLoanTerm[0].addError(e.getDmlStatusCode(0));
    }
  }
  
	// Function to check if the custom setting contains the profile then bypass validation logic
	public Boolean checkUserProfile(){
	    LoanValidationBypassConfig__c objLVC= 
	                                LoanValidationBypassConfig__c.getInstance([Select Profile.Name 
	                                                                           from User 
	                                                                           where id = :Userinfo.getUserId()].Profile.Name);
	    return objLVC != NULL ? false : true;
	}//End  
    
  static testMethod void loanTermTriggerTest(){
    Loan__c objLoan = new Loan__c(Name='Test',Loan_Amount__c = 200 , Term__c= 2);
    insert objLoan;
    
    Loan_Term__c objLoanTerm = new Loan_Term__c(LoanId__c=objLoan.Id,Requested_Loan_Amount__c = 2000,
                                                Guarantee_Fee__c = 100,Outside_Origination_Fee__c = 10,LTV__c =2,
                                                Inside_Origination_Fee__c = 10, Investor_Spread__c =20,
                                                Price_Percent__c = 200,CUSIP__c = 'Test',Pool_CUSIP__c = 'Test',Pricing_Waiver__c ='test data',
                                                Risk_Sharing__c = '0%',DSCR__c= 12,UW_DSCR__c = 10,Actual_DSCR__c = 9,
                                                Loan_Amount__c = 1000,Term__c= 3 ,Amortization__c = 3,Floor_Rate__c= 12,
                                                Servicing_fee__c = 200 , Final__c = true, Final_Loan_Amount__c = 2000);                                           
                                            
    insert  objLoanTerm;
    
    Loan__c objDBLoan = [select Borrower_Requested_Loan_Amount__c,Loan_Amount__c,Term__c,Prepayment_Term__c,
                                Interest_Only_Period__c,Amortization__c,Interest_Rate_Accrual__c,Rate_Option__c,
                                Estimated_Note_Rate__c,Floor_Rate__c,DSCR__c,UWDSCR__c,Actual_DSCR__c,LTV__c,LTC__c,
                                Index_Type__c,Date_of_Index__c,Index__c,Investor_Spread__c,Guarantee_Fee__c,
                                Servicing_fee__c,Outside_Origination_Fee__c,Inside_Origination_Fee__c,
                                Pricing_Waiver__c,Risk_Sharing__c,Price_Percent__c,CUSIP__c,Final_Loan_Amount__c 
                         from Loan__c
                         where id = :objLoan.Id];                                                   
    
    // Check if Loan Term fields are copied to fields on Loan object
    system.assertEquals(objLoanTerm.Requested_Loan_Amount__c, objDBLoan.Borrower_Requested_Loan_Amount__c);
    system.assertEquals(objLoanTerm.DSCR__c, objDBLoan.DSCR__c); 
    system.assertEquals(objLoanTerm.Loan_Amount__c, objDBLoan.Loan_Amount__c); 
    system.assertEquals(objLoanTerm.Amortization__c, objDBLoan.Amortization__c);  
    system.assertEquals(objLoanTerm.Floor_Rate__c, objDBLoan.Floor_Rate__c); 
    system.assertEquals(objLoanTerm.Servicing_fee__c, objDBLoan.Servicing_fee__c);  
    system.assertEquals(objLoanTerm.Term__c, objDBLoan.Term__c);
    system.assertEquals(objLoanTerm.Final_Loan_Amount__c, objDBLoan.Final_Loan_Amount__c); 
    
        
    Loan_Term__c objLoanTerm1 = new Loan_Term__c(LoanId__c=objLoan.Id ,Requested_Loan_Amount__c = 1000,Amortization__c = 2,
                                                 Guarantee_Fee__c = 100,Outside_Origination_Fee__c = 10,LTV__c =2,
                                                 Inside_Origination_Fee__c = 10, Investor_Spread__c =20,
                                                 Price_Percent__c = 200,CUSIP__c = 'Test',Pool_CUSIP__c = 'Test',Pricing_Waiver__c ='test data',
                                                 Risk_Sharing__c = '0%',DSCR__c= 12,UW_DSCR__c = 10,Actual_DSCR__c = 9,
                                                 Loan_Amount__c = 1000,Term__c= 3 ,Floor_Rate__c= 12,
                                                 Servicing_fee__c = 100, Final__c = true, Final_Loan_Amount__c = 1000);
    
    // Throw an error if another final Loan term is created
    try{
        insert  objLoanTerm1;
    }
    catch (Exception e){
        system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',e.getDmlStatusCode(0));
    }
    
    objLoanTerm.Final__c = false;
    update objLoanTerm;
    
    insert objLoanTerm1;
    // Check if Loan Term fields are copied to fields on Loan object
    system.assertEquals(objLoanTerm.Requested_Loan_Amount__c, objDBLoan.Borrower_Requested_Loan_Amount__c);
    system.assertEquals(objLoanTerm.DSCR__c, objDBLoan.DSCR__c); 
    system.assertEquals(objLoanTerm.Loan_Amount__c, objDBLoan.Loan_Amount__c); 
    system.assertEquals(objLoanTerm.Amortization__c, objDBLoan.Amortization__c); 
    system.assertEquals(objLoanTerm.Floor_Rate__c, objDBLoan.Floor_Rate__c); 
    system.assertEquals(objLoanTerm.Servicing_fee__c, objDBLoan.Servicing_fee__c);  
    system.assertEquals(objLoanTerm.Term__c, objDBLoan.Term__c);
    system.assertEquals(objLoanTerm.Final_Loan_Amount__c, objDBLoan.Final_Loan_Amount__c); 
  }
  static testMethod void loanTermTrigger_TBDTest(){
    Loan__c objLoan = new Loan__c(Name='Test',Loan_Amount__c = 200 , Term__c= 2 ,Loan_Program__c = 'Conventional',
                                    Investor__c = 'Test investor',Investor_type__c = 'Test type');
    insert objLoan; 
    
    Id recTypeId = [Select Id, Name from RecordType where Name = 'TBD' limit 1].Id;
    
    Loan_Term__c objLoanTerm = new Loan_Term__c(LoanId__c=objLoan.Id,recordTypeId = recTypeId,
                                                Loan_Program__c = 'Students', Investor__c = 'LoanTerm Investor',
                                                Investor_type__c = 'LoanTerm Investor type',
                                                Requested_Loan_Amount__c = 2000,
                                                Guarantee_Fee__c = 100,Outside_Origination_Fee__c = 10,LTV__c =2,
                                                Inside_Origination_Fee__c = 10, Investor_Spread__c =20,
                                                Price_Percent__c = 200,CUSIP__c = 'Test',Pool_CUSIP__c = 'Test',Pricing_Waiver__c ='test data',
                                                Risk_Sharing__c = '0%',DSCR__c= 12,UW_DSCR__c = 10,Actual_DSCR__c = 9,
                                                Loan_Amount__c = 1000,Term__c= 3 ,Amortization__c = 3,Floor_Rate__c= 12,
                                                Servicing_fee__c = 200 , Final__c = true , Final_I_O_DSCR_at_Actual_Rate__c = 20,
                                                Final_LTV__c = 40);

    insert  objLoanTerm;
    
    Loan__c objDBLoan = [select Loan_Program__c ,Investor__c,Investor_type__c , Final_I_O_DSCR_at_Actual_Rate__c ,
    					 Final_LTV__c from Loan__c where id = :objLoan.Id];
          
    system.assertEquals(objLoanTerm.Loan_Program__c, objDBLoan.Loan_Program__c);  
    system.assertEquals(objLoanTerm.Investor__c, objDBLoan.Investor__c);
    system.assertEquals(objLoanTerm.Investor_type__c, objDBLoan.Investor_type__c);
    system.assertEquals(objLoanTerm.Final_I_O_DSCR_at_Actual_Rate__c, objDBLoan.Final_I_O_DSCR_at_Actual_Rate__c);
    system.assertEquals(objLoanTerm.Final_LTV__c, objDBLoan.Final_LTV__c);
    
  }
  static testMethod void loanTermTrigger_NegativeTest(){
    Loan__c objLoan = new Loan__c(Name='Test',Loan_Amount__c = 200 , Term__c= 2 ,Loan_Program__c = 'TBD',
                                    Loan_Status__c = 'Pre-Screen (Underwriting)',
                                    Investor__c = 'Test investor',Investor_type__c = 'Test type');
    insert objLoan; 
    
    Id recTypeId = [Select Id, Name from RecordType where Name = 'TBD' limit 1].Id;
    
    Loan_Term__c objLoanTerm = new Loan_Term__c(LoanId__c=objLoan.Id,recordTypeId = recTypeId,
                                                Loan_Program__c = 'Students', Investor__c = 'LoanTerm Investor',
                                                Investor_type__c = 'LoanTerm Investor type',
                                                Requested_Loan_Amount__c = 2000,
                                                Guarantee_Fee__c = 100,Outside_Origination_Fee__c = 10,LTV__c =2,
                                                Inside_Origination_Fee__c = 10, Investor_Spread__c =20,
                                                Price_Percent__c = 200,CUSIP__c = 'Test', Pool_CUSIP__c = 'Test',Pricing_Waiver__c ='test data',
                                                Risk_Sharing__c = '0%',DSCR__c= 12,UW_DSCR__c = 10,Actual_DSCR__c = 9,
                                                Loan_Amount__c = 1000,Term__c= 3 ,Amortization__c = 3,Floor_Rate__c= 12,
                                                Servicing_fee__c = 200 , Final__c = true);
                                                
    try{                                            
        insert  objLoanTerm;
    }catch(Exception e){
        system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',e.getDmlStatusCode(0));
    }
  }
}