/*
 *   Description - Trigger handler for COntactTrigger
 *    Version          Date          Description
 *     1.0           24/04/2013     On delete of Contact update delete Loan Sponsor records to fire
 * 									Loan Sponsor trigger 
 *
 */

public with sharing class ContactTriggerHandler {

	public static final String frmDelete = 'Delete';
	public static final String frmUpdate = 'Update';
	
	public void onBeforeDelete(list<Contact> lstContact){
		deleteUpdateLoanSponsor(lstContact,frmDelete,null);
	}
	
	public void onBeforeUpdate(list<Contact> lstContact,map<Id,Contact> oldConMap){
		deleteUpdateLoanSponsor(lstContact,frmUpdate,oldConMap);		
	}

	public void deleteUpdateLoanSponsor(list<Contact> lstContact,String frmOp,map<Id,Contact> oldCMap){
		set<Id> setContactId = new set<Id>(); 
		
		list<Loan_Sponsor__c> lstLSToBeUpdated = new list<Loan_Sponsor__c>();
		
		for(Contact objContact : lstContact){ 
			if(oldCMap != null && oldCMap.containsKey(objContact.Id)){ 
				if((objContact.LastName != String.valueOf(oldCMap.get(objContact.Id).LastName)) || 
					(objContact.FirstName != String.valueOf(oldCMap.get(objContact.Id).FirstName))){
					setContactId.add(objContact.Id);
				}
			}
			else{
				setContactId.add(objContact.Id);
			}
		}
		
		//query the Loan Sponsor records
		for(Loan_Sponsor__c objSponsor : [Select Id,Contact__c from Loan_Sponsor__c where
											Contact__c IN : setContactId]){
			
			if(frmOp.equalsIgnoreCase(frmDelete)){
				lstLSToBeUpdated.add(objSponsor);
			}
			else if(frmOp.equalsIgnoreCase(frmUpdate)){
				//objSponsor.Contact__c = objSponsor.Contact__c;
				lstLSToBeUpdated.add(objSponsor);		
			}								
		}
		
		if(frmOp.equalsIgnoreCase(frmDelete) && !lstLSToBeUpdated.isEmpty()){ 
			delete lstLSToBeUpdated;
		}
		else if(frmOp.equalsIgnoreCase(frmUpdate) && !lstLSToBeUpdated.isEmpty()){
			Database.SaveResult[] srList = Database.update(lstLSToBeUpdated,false);
			// Iterate through each returned result
			for (Database.SaveResult sr : srList) {  
			    if (!sr.isSuccess()){
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('Error' + ': ' + err.getMessage());
			        }
			    }
			}	
		}
	}
	
	static testMethod void testDeleteLoanSponsor(){
		
		Loan__c objLoan = new Loan__c(Name = 'Test Loan',Report_Key_Principal__c = '',Report_Principal__c = '',
										Loan_Program__c = 'Test',Loan_Status__c = 'status3');
	 	insert objLoan;
	 	
	 	Contact objContact = new Contact(Description = 'Test Contact',LastName ='Name');
	 	insert objContact;
	 	
	 	Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'Principal',Contact__c =objContact.Id,Sponsor_is_the_Individual__c = true );
	 	insert objLS;
	 	
	 	Loan_Sponsor__c objLS1 = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'Key Principal',Contact__c =objContact.Id,Sponsor_is_the_Individual__c = true );
	 	insert objLS1;
		
		test.startTest();
			objLoan = [Select Report_Key_Principal__c,Report_Principal__c from Loan__c where Id = : objLoan.Id];
			system.assertEquals(objLoan.Report_Key_Principal__c,objContact.Name);
			system.assertEquals(objLoan.Report_Principal__c,objContact.Name);
			
			delete objContact;
			objLoan = [Select Report_Key_Principal__c,Report_Principal__c from Loan__c where Id = : objLoan.Id];
			
			system.assertEquals(objLoan.Report_Key_Principal__c,NULL);
			system.assertEquals(objLoan.Report_Principal__c,NULL);
		test.stopTest();
		
	}
	
	static testMethod void testUpdateLoanSponsor(){
		
		//Create Custom setting record for test role
		Report_LoanSponsor_Field__c reportCS = new Report_LoanSponsor_Field__c(Name ='TestRole', 
																			   Loan_Field__c='Report_Key_Principal__c');
		insert reportCS;
		
		Loan__c objLoan = new Loan__c(Name = 'Test Loan',Report_Key_Principal__c = '',Report_Principal__c = '',
										Loan_Program__c = 'Test',Loan_Status__c = 'status3');
	 	insert objLoan;
	 	
	 	Contact objContact = new Contact(Description = 'Test Contact',LastName ='Name'); 
	 	insert objContact;
	 	
	 	Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'TestRole',Contact__c = objContact.Id ,Sponsor_is_the_Individual__c = true);
	 	insert objLS;
	 	
		test.startTest();
			objLoan = [Select Report_Key_Principal__c from Loan__c where Id = : objLoan.Id];
			
			system.assertEquals(objLoan.Report_Key_Principal__c,'Name');
			
			objContact.LastName = 'Test Con1';
			update objContact;
			objLoan = [Select Report_Key_Principal__c,Report_Principal__c from Loan__c where Id = : objLoan.Id];
			
			system.assertEquals(objLoan.Report_Key_Principal__c,'Test Con1'); 
		test.stopTest();
	} 
}