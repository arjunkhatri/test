public with sharing class LoanContactHandler
{
	public void OnBeforeInsertUpdate(List<Loan_Contact__c> lstLoanContact)
	{
		//try
		//{
			Boolean isError  = false;
			Map<String,List<String>> mapContactIdToListProperty = new Map<String,List<String>>(); 
			Map<String,set<String>> mapLoanIdToContactId = new Map<String,set<String>>(); 
			set<String> setLoanIdForCheckingDuplicate = new set<String>();
			set<String> setLoanId = new set<String>();
			set<String> setContactId = new set<String>();
			//Here we are checking for the role of the loan contact creating map between loan id and set of conatact(in set we get all the contact related to a particular loan) 
			for(Loan_Contact__c objLC : lstLoanContact)
			{				
				if(objLC.Role__c == System.Label.PropertyContact_LoanContactRole)
				{
					//Initially we add loan id to the set. 
					if(setLoanIdForCheckingDuplicate.size() == 0)
						setLoanIdForCheckingDuplicate.add(objLC.LoanId__c);
					//if the loan contact records loan id is in the set and is not in map means previously there were no loan contact record with this loan id 
					//so we can directly add this id to the contact set and than add contact set to the map.
					if(setLoanIdForCheckingDuplicate.contains(objLC.LoanId__c) && !mapLoanIdToContactId.containsKey(objLC.LoanId__c))
					{
						setContactId.clear();
						setContactId.add(objLC.ContactId__c);
						mapLoanIdToContactId.put(objLC.LoanId__c,setContactId);
					}
					//if the loan contact records loan id is in the set and in map means previously there were loan contact record with this loan id
					//so we need to fetch the set first 
					//add the contact id to the set 
					//than to the map.
					//by doing this we are assure that for a particular loan we get all the contact id 
					else if(setLoanIdForCheckingDuplicate.contains(objLC.LoanId__c) && mapLoanIdToContactId.containsKey(objLC.LoanId__c))
					{
						set<String> setconId = mapLoanIdToContactId.get(objLC.LoanId__c);
						setconId.add(objLC.ContactId__c);
						mapLoanIdToContactId.put(objLC.LoanId__c,setconId);
					}
					//if set does not contain loan id than we add the loan id to the set ,create contact set and than create map.
					else
					{
						setContactId.clear();
						setLoanIdForCheckingDuplicate.add(objLC.LoanId__c);
						setContactId.add(objLC.ContactId__c);
						mapLoanIdToContactId.put(objLC.LoanId__c,setContactId);
					}
					setLoanId.add(objLC.LoanId__c);
				}
			}
			
			if(setLoanId.size() > 0)
			{
				Boolean isProperty = false;
				List<String> lstPropertyId = new List<String>(); 
				Map<String,List<String>> mapLoanIdToProperty = new Map<String,List<String>>();
				//querying all the records related to the selected loan ids.
				String strQuery = 'Select LoanId__c,Id,PropertyId__c From Loan_Property__c where LoanId__c IN :setLoanId';
				setLoanIdForCheckingDuplicate.clear();
				//Again same way as done above we need to create a map but between loan id and list of property.
				//logic remains same but here we are creating map of string,list<string> instead of map string,set<string>
				for(Loan_Property__c objLP : database.query(strQuery))
				{
					if(setLoanIdForCheckingDuplicate.size() == 0)
					{
						setLoanIdForCheckingDuplicate.add(objLP.LoanId__c);
						isProperty = true;
					}
				
					if(setLoanIdForCheckingDuplicate.contains(objLP.LoanId__c) && !mapLoanIdToProperty.containsKey(objLP.LoanId__c))
					{
						lstPropertyId.clear(); 
						lstPropertyId.add(objLP.PropertyId__c);
						mapLoanIdToProperty.put(objLP.LoanId__c,lstPropertyId);
					}
					else if(setLoanIdForCheckingDuplicate.contains(objLP.LoanId__c) && mapLoanIdToProperty.containsKey(objLP.LoanId__c))
					{
						List<String> lstId = mapLoanIdToProperty.get(objLP.LoanId__c);
						lstId.add(objLP.PropertyId__c);
						mapLoanIdToProperty.put(objLP.LoanId__c,lstId);
					}
					else
					{
						lstPropertyId.clear();
						setLoanIdForCheckingDuplicate.add(objLP.LoanId__c);
						lstPropertyId.add(objLP.PropertyId__c);
						mapLoanIdToProperty.put(objLP.LoanId__c,lstPropertyId);
					}
				}
				//here we create map with contactid as key and list<property> as value. 
				for(String strLoanId : mapLoanIdToContactId.keySet())
				{
					set<String> setContactIdToInsert = mapLoanIdToContactId.get(strLoanId);
					for(String strConId : setContactIdToInsert)
					{
						mapContactIdToListProperty.put(strConId,mapLoanIdToProperty.get(strLoanId));
					}
				}
				
				//There exists any property related to the selected loan than call the createpropertycontact. 
				if(mapContactIdToListProperty.size() > 0 && isProperty)
				{
					PropertyContact_Create objPropertyContact = new PropertyContact_Create();
					isError  = objPropertyContact.createPropertyContactFromLoanContact(mapContactIdToListProperty);
				}
			}
			
			if(isError)
			{
				for(Loan_Contact__c objLC : lstLoanContact)
				{
					objLC.addError(System.Label.PropertyContact_InsertError);
				}
			}
		//}
		//catch(exception e)
		//{
		//	System.debug(LoggingLevel.ERROR, e.getMessage());
		//	System.debug(LoggingLevel.ERROR, e.getStackTraceString());
		//}
	}
	
	// Test Methods
    static testMethod void LoanContactPropertyTriggerTest()
    {
		List<Property__c> lstProp = new List<Property__c>();
		List<Loan__c> lstLoan  = new List<Loan__c>(); 
		List<Contact> lstContact  = new List<Contact>();
		List<Loan_Property__c> lstLoanProp = new List<Loan_Property__c>(); 
		List<Loan_Contact__c> lstLoanContact = new List<Loan_Contact__c>(); 
		
		Test.startTest();
		
    	Account objAcc = new Account();
    	objAcc.Name = 'testAcc';
    	insert objAcc;
	    
    	Loan__c objLoan = new Loan__c();
    	objLoan.AccountId__c = objAcc.Id;
    	objLoan.Name = 'testLoan';
    	objLoan.Loan_Status__c = 'Prospect';
    	lstLoan.add(objLoan);
    	
    	Loan__c objLoan2 = new Loan__c();
    	objLoan2.AccountId__c = objAcc.Id;
    	objLoan2.Name = 'testLoan';
    	objLoan2.Loan_Status__c = 'Prospect';
    	lstLoan.add(objLoan2);
    	
    	insert lstLoan;
    	
    	Contact objCon = new Contact();
        objCon.LastName = 'testCon';
        objCon.AccountId = objAcc.Id;
        objCon.Email = 'testCoverage@testCoverage.com';
        lstContact.add(objCon);
        
        Contact objCon2 = new Contact();
        objCon2.LastName = 'testCon2';
        objCon2.AccountId = objAcc.Id;
        objCon2.Email = 'testCoverage2@testCoverage2.com';
        lstContact.add(objCon2);
        
        Contact objCon3 = new Contact();
        objCon3.LastName = 'testCon3';
        objCon3.AccountId = objAcc.Id;
        objCon3.Email = 'testCoverage3@testCoverage3.com';
        lstContact.add(objCon3);
        
        insert lstContact;
        
        Property__c objProp = new Property__c();
        objProp.Name = 'test Prop';
        lstProp.add(objProp);
        
        Property__c objProp2 = new Property__c();
        objProp2.Name = 'test Prop2';
        lstProp.add(objProp2);
        
        Property__c objProp3 = new Property__c();
        objProp3.Name = 'test Prop2';
        lstProp.add(objProp3);
        
        insert lstProp;
        
        Loan_Property__c objLP = new Loan_Property__c();  
        objLP.LoanId__c = objLoan.Id;
        objLP.PropertyId__c = objProp.Id;
        lstLoanProp.add(objLP);
        
        Loan_Property__c objLP2 = new Loan_Property__c();  
        objLP2.LoanId__c = objLoan.Id;
        objLP2.PropertyId__c = objProp2.Id;
        lstLoanProp.add(objLP2);
        
        Loan_Property__c objLP3 = new Loan_Property__c();  
        objLP3.LoanId__c = objLoan2.Id;
        objLP3.PropertyId__c = objProp3.Id;
        lstLoanProp.add(objLP3);
        
        insert lstLoanProp;
		
		//Test Coverage for loanContactTrigger.
		
		Loan_Contact__c objLC4 = new Loan_Contact__c();
        objLC4.ContactId__c = objCon3.Id;
        objLC4.LoanId__c = objLoan.Id;
        objLC4.Role__c = System.Label.PropertyContact_LoanContactRole;
        lstLoanContact.add(objLC4);
        
        Loan_Contact__c objLC5 = new Loan_Contact__c();
        objLC5.ContactId__c = objCon3.Id;
        objLC5.LoanId__c = objLoan2.Id;
        objLC5.Role__c = System.Label.PropertyContact_LoanContactRole;
        lstLoanContact.add(objLC5);
		
		insert lstLoanContact;
		lstLoanContact.clear();
		
        Loan_Contact__c objLC = new Loan_Contact__c();
        objLC.ContactId__c = objCon.Id;
        objLC.LoanId__c = objLoan.Id;
        objLC.Role__c = System.Label.PropertyContact_LoanContactRole;
        lstLoanContact.add(objLC);
        
        Loan_Contact__c objLC2 = new Loan_Contact__c();
        objLC2.ContactId__c = objCon2.Id;
        objLC2.LoanId__c = objLoan.Id;
        objLC2.Role__c = System.Label.PropertyContact_LoanContactRole;
        lstLoanContact.add(objLC2);
        
        Loan_Contact__c objLC3 = new Loan_Contact__c();
        objLC3.ContactId__c = objCon2.Id;
        objLC3.LoanId__c = objLoan2.Id;
        objLC3.Role__c = System.Label.PropertyContact_LoanContactRole;
        lstLoanContact.add(objLC3);
        
        insert lstLoanContact;
        
        objLC3.Role__c = System.Label.PropertyContact_LoanContactRole;
        update objLC3;
        
        //Test coverage for loanPropertyTrigger.
        update lstLoanContact;
        lstLoanProp.clear();
        
        Loan_Property__c objLP6 = new Loan_Property__c();  
        objLP6.LoanId__c = objLoan2.Id;
        objLP6.PropertyId__c = objProp3.Id;
        lstLoanProp.add(objLP6);
        
        Loan_Property__c objLP7 = new Loan_Property__c();  
        objLP7.LoanId__c = objLoan.Id;
        objLP7.PropertyId__c = objProp3.Id;
        lstLoanProp.add(objLP7);
        
        insert lstLoanProp;
        
        lstLoanProp.clear();
        
        Loan_Property__c objLP4 = new Loan_Property__c();  
        objLP4.LoanId__c = objLoan2.Id;
        objLP4.PropertyId__c = objProp3.Id;
        lstLoanProp.add(objLP4);
        
        Loan_Property__c objLP5 = new Loan_Property__c();  
        objLP5.LoanId__c = objLoan.Id;
        objLP5.PropertyId__c = objProp.Id;
        lstLoanProp.add(objLP5);
        
        insert lstLoanProp;

        Test.stopTest();
    }
}