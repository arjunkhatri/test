/*  
 *  Descprition :Send Email Using Email Template
 *  
 *  Revision History:
 *
 *   Version          Date            Description
 *   1.0            01/01/2013      Initial Draft
 */
public with sharing class SendEmail {
	
	//private EmailMessageWrapper emailMessageWrapper;
	public static void sendEmail(List<EmailMessageWrapper> listEmailMessageWrapper, String emailTemplateName){//, List<Loan__c> loanslist) {
   		List<Messaging.SendEmailResult> listEmailResult = null;
        List<Messaging.Singleemailmessage> listSingleEmailMessages = new List<Messaging.Singleemailmessage>();
        EmailTemplate emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Name = :emailTemplateName limit 1];
        if(emailTemplate != null){
        for (EmailMessageWrapper emailMessageWrapper : listEmailMessageWrapper) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSenderDisplayName('Case Age Notification');
            if(emailMessageWrapper.FromAddress != null && emailMessageWrapper.FromAddress.length() > 0)
                mail.setReplyTo(emailMessageWrapper.FromAddress);
           //if(emailMessageWrapper.ToAddress != null && emailMessageWrapper.ToAddress.length() > 0){
             //  mail.setToAddresses(new String[] { emailMessageWrapper.ToAddress });
           //}
           if(emailMessageWrapper.ToAddress != null && emailMessageWrapper.ToAddress.size() > 0){
               mail.setToAddresses(emailMessageWrapper.ToAddress);
           }else
               mail.setTargetObjectId(emailMessageWrapper.ToAddressId);
           if(emailMessageWrapper.BccAddress != null && emailMessageWrapper.BccAddress.length() > 0)
               mail.setBccAddresses(new String[] {emailMessageWrapper.BccAddress });
           /*
           String subject = null;
           
           if(emailMessageWrapper.Subject != null && emailMessageWrapper.Subject.length() > 0) {
               mail.setSubject(emailMessageWrapper.Subject);
               subject = emailMessageWrapper.Subject;
           }
           else
               subject = emailTemplate.Subject;
  
           for(String key: emailMessageWrapper.ParameterSubjectMap.keySet())
               subject = subject.replace(key, (emailMessageWrapper.ParameterSubjectMap.get(key) == null ? '' : emailMessageWrapper.ParameterSubjectMap.get(key)));
  
           mail.setSubject(subject);
           String htmlBody = emailTemplate.HtmlValue;
           String plainBody = emailTemplate.Body;
           for (String key : emailMessageWrapper.ParameterBodyMap.keySet()) {
           	 if(htmlBody != null)
           	   htmlBody = htmlBody.replace(key, (emailMessageWrapper.ParameterBodyMap.get(key) == null) ? '' : emailMessageWrapper.ParameterBodyMap.get(key));
           	 if(plainBody != null)  
               plainBody = plainBody.replace(key, (emailMessageWrapper.ParameterBodyMap.get(key) == null) ? '' : emailMessageWrapper.ParameterBodyMap.get(key));
           }

           mail.setHtmlBody(htmlBody);
           mail.setSaveAsActivity(false);
           mail.setPlainTextBody(plainBody);
           */
          system.debug('***To send Mail**');
           mail.setTargetObjectId('003e000000Dpent');
           
           mail.setTemplateId(emailTemplate.Id);
           mail.setWhatId('a0xe0000000skIp');
           listSingleEmailMessages.add(mail);
           system.debug('***To send Mail1**');
       }
        }
       //if(!Test.isRunningTest())
       if(listSingleEmailMessages.size() > 0){
           listEmailResult = Messaging.sendEmail(listSingleEmailMessages);
           system.debug('***Mail Sent**');
       }
           
         
   }
}