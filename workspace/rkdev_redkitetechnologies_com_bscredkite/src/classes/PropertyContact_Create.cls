public with sharing class PropertyContact_Create
{
	public Boolean createPropertyContactFromLoanContact(Map<String,List<String>> mapContactIdToListProperty)
	{
		try
		{
			List<String> lstPropertyId = new List<String>(); 
			set<String> setContactId = new set<String>(); 
			Map<String,List<String>> mapInsertPropertyContact = new Map<String,List<String>>(); 
			
			//creating a list of property id and set of contact id so that it can be used to in query. 
			for(String strContactId : mapContactIdToListProperty.keySet())
			{
				lstPropertyId.addAll(mapContactIdToListProperty.get(strContactId));
				setContactId.add(strContactId);
			}
			
			set<String> setProjectIdToInserted = new set<String>(); 
			List<Property_Contact__c> lstPropertyContact = new List<Property_Contact__c>(); 
			//querying all the records related to particular property and contact to avoid duplication in database. 
			List<Property_Contact__c> lstPropCon = [Select PropertyId__c,ContactId__c From Property_Contact__c where PropertyId__c IN :lstPropertyId and ContactId__c IN : setContactId];
			//if list conatins value than we need to use duplication logic.
			if(lstPropCon.size() > 0)
			{
				Map<String,set<String>> mapContactToPropertyId = new Map<String,set<String>>(); 
				set<String> setContactIdForCheckingDuplicate = new set<String>();
				set<String> setPropertyId = new set<String>();
				system.debug('==============================lstPropCon='+lstPropCon);
				//Here we create map between property id as key and set of contact id as value  
				for(Property_Contact__c objPC : lstPropCon)
				{
					if(setContactIdForCheckingDuplicate.size() == 0)
						setContactIdForCheckingDuplicate.add(objPC.ContactId__c);
				
					if(setContactIdForCheckingDuplicate.contains(objPC.ContactId__c) && !mapContactToPropertyId.containsKey(objPC.ContactId__c))
					{
						setPropertyId.clear();
						setPropertyId.add(objPC.PropertyId__c);
						mapContactToPropertyId.put(objPC.ContactId__c,setPropertyId);
					}
					else if(setContactIdForCheckingDuplicate.contains(objPC.ContactId__c) && mapContactToPropertyId.containsKey(objPC.ContactId__c))
					{
						set<String> setconId = mapContactToPropertyId.get(objPC.ContactId__c);
						setconId.add(objPC.PropertyId__c);
						mapContactToPropertyId.put(objPC.ContactId__c,setconId);
					}
					else
					{
						setPropertyId.clear();
						setContactIdForCheckingDuplicate.add(objPC.ContactId__c);
						setPropertyId.add(objPC.PropertyId__c);
						mapContactToPropertyId.put(objPC.ContactId__c,setPropertyId);
					}
				}
				system.debug('==============================mapContactToPropertyId='+mapContactToPropertyId);
				system.debug('==============================mapContactIdToListProperty='+mapContactIdToListProperty);
				//Here for each contact id we compare the propety id from the trigger(list of property id) and from the data base(setProperty id) 
				//than create records for the non duplicate property contact.
				for(String strContactId : mapContactIdToListProperty.keySet())
				{
					set<String> setPropId = new set<String>(); 
					if(mapContactToPropertyId.get(strContactId) != null)
						setPropId.addAll(mapContactToPropertyId.get(strContactId));
					system.debug('==============================setPropId='+setPropId);
					List<String> lstpropId = new List<String>();
					if(mapContactIdToListProperty.get(strContactId) != null) 
						lstpropId.addAll(mapContactIdToListProperty.get(strContactId));
					system.debug('==============================lstpropId='+lstpropId);
					for(String strPropId : lstpropId)
					{
						if(!setPropId.contains(strPropId))
						{
							Property_Contact__c objPC = new Property_Contact__c();
							objPC.ContactId__c = strContactId;
							objPC.PropertyId__c = strPropId;
							lstPropertyContact.add(objPC);
						}
					}
				}
				system.debug('==============================lstPropertyContact='+lstPropertyContact);			
			}
			//esle if we dont get any duplicate records from database than directly create Property Contact records.
			//this is place where we get to know why we have used map
			//As we can see that each contact creates property contact records for its related property in the list
			//So here if there are two contacts from different loan and each loan has diff property list,
			//than the contact should properly match with its related property and create that many property contact records.      
			else
			{
				for(String strContactId : mapContactIdToListProperty.keySet())
				{
					List<String> lstPropertyToInsert = mapContactIdToListProperty.get(strContactId);
					for(String strPropId : lstPropertyToInsert)
					{
						Property_Contact__c objPC = new Property_Contact__c();
						objPC.ContactId__c = strContactId;
						objPC.PropertyId__c = strPropId;
						lstPropertyContact.add(objPC);
					}
				}
			}
			
			if(lstPropertyContact.size() > 0)
				insert lstPropertyContact;
			return false;
		}
		catch(exception e)
		{
			System.debug(LoggingLevel.ERROR, e.getMessage());
			System.debug(LoggingLevel.ERROR, e.getStackTraceString());
			return true; 
		}
	}
	//the logic for this function is same as previous function.
	public Boolean createPropertyContactFromLoanProject(Map<String,List<String>> mapPropertyIdToListContact)
	{
		try
		{
			List<String> lstContactId = new List<String>(); 
			set<String> setPropertyId = new set<String>(); 
			
			for(String strPropertyId : mapPropertyIdToListContact.keySet())
			{
				lstContactId.addAll(mapPropertyIdToListContact.get(strPropertyId));
				setPropertyId.add(strPropertyId);
			}
			
			List<Property_Contact__c> lstPropertyContact = new List<Property_Contact__c>(); 
			
			List<Property_Contact__c> lstPropCon = [Select PropertyId__c,ContactId__c From Property_Contact__c where PropertyId__c =:setPropertyId and ContactId__c  IN : lstContactId];
			
			if(lstPropCon.size() > 0)
			{
				Map<String,set<String>> mapPropertyToContactId = new Map<String,set<String>>(); 
				set<String> setPropertyIdForCheckingDuplicate = new set<String>();
				set<String> setContactId = new set<String>();
				for(Property_Contact__c objPC : lstPropCon)
				{
					if(setPropertyIdForCheckingDuplicate.size() == 0)
						setPropertyIdForCheckingDuplicate.add(objPC.PropertyId__c);
				
					if(setPropertyIdForCheckingDuplicate.contains(objPC.PropertyId__c) && !mapPropertyToContactId.containsKey(objPC.PropertyId__c))
					{
						setContactId.clear();
						setContactId.add(objPC.ContactId__c);
						mapPropertyToContactId.put(objPC.PropertyId__c,setContactId);
					}
					else if(setPropertyIdForCheckingDuplicate.contains(objPC.PropertyId__c) && mapPropertyToContactId.containsKey(objPC.PropertyId__c))
					{
						set<String> setconId = mapPropertyToContactId.get(objPC.PropertyId__c);
						setconId.add(objPC.ContactId__c);
						mapPropertyToContactId.put(objPC.PropertyId__c,setconId);
					}
					else
					{
						setContactId.clear();
						setPropertyIdForCheckingDuplicate.add(objPC.PropertyId__c);
						setContactId.add(objPC.ContactId__c);
						mapPropertyToContactId.put(objPC.PropertyId__c,setContactId);
					}
				}
				
				for(String strPropertyId : mapPropertyIdToListContact.keySet())
				{
					List<String> lstContactIdToInsert = new List<String>();
					set<String> setConId = new set<String>();
					if(mapPropertyToContactId.get(strPropertyId) != null)
						setConId.addAll(mapPropertyToContactId.get(strPropertyId));					
					List<String> lstContact = new List<String>();
					if(mapPropertyIdToListContact.get(strPropertyId) != null)
						lstContact.addAll(mapPropertyIdToListContact.get(strPropertyId));
					for(String strConId : lstContact)
					{
						if(!setConId.contains(strConId))
						{
							Property_Contact__c objPC = new Property_Contact__c();
							objPC.ContactId__c = strConId;
							objPC.PropertyId__c = strPropertyId;
							lstPropertyContact.add(objPC);
						}
					}
				}
			}
			else
			{
				for(String strPropertyId : mapPropertyIdToListContact.keySet())
				{
					List<String> lstContactToInsert = mapPropertyIdToListContact.get(strPropertyId);
					for(String strConId : lstContactToInsert)
					{
						Property_Contact__c objPC = new Property_Contact__c();
						objPC.ContactId__c = strConId;
						objPC.PropertyId__c = strPropertyId;
						lstPropertyContact.add(objPC);
					}
				}
			}
			
			if(lstPropertyContact.size() > 0)
				insert lstPropertyContact;
			return false;
		}
		catch(exception e)
		{
			System.debug(LoggingLevel.ERROR, e.getMessage());
			System.debug(LoggingLevel.ERROR, e.getStackTraceString());
			return true; 
		}
	}
}