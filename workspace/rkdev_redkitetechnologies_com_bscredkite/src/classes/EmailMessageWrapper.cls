public with sharing class EmailMessageWrapper {
	
	public String FromAddress;
	public String[] ToAddress;
	public String ToAddressId;
	public String BccAddress;
	public String Subject; 
	public Map<String, String> ParameterSubjectMap;
	public String Body; 
	public Map<String, String> ParameterBodyMap; 

	public EmailMessageWrapper(String[] toAddr, Map<String, String>  mapSubjectParams, Map<String, String> mapBodyParams ){
	 	this.ToAddress = toAddr;
	 	this.ParameterSubjectMap = mapSubjectParams;
	 	this.ParameterBodyMap = mapBodyParams;
	}
}