global class ReportScheduler implements System.Schedulable{
	public map<id,Schedule_Report_Details__c> mpIdtoObj = new map<id,Schedule_Report_Details__c>();
	global void execute(SchedulableContext sc) {
		set<Id> setAttachments = new set<Id>();    
		List<Schedule_Report_Details__c> lstReports = new List<Schedule_Report_Details__c>();
		List<Schedule_Report_Details__c> lstReportsUpdate = new List<Schedule_Report_Details__c>();
		List<String> listCron = new List<String>();
		Datetime dt = system.now();
		String sCron = '0 0';
				sCron += ' '+String.valueOf(dt.hour());
				sCron += ' '+String.valueOf(dt.day());
				sCron += ' '+String.valueOf(dt.month());
				sCron += ' ?';	
		for(Schedule_Report_Details__c sReport:[Select 	id,
														Execution_Date__c,
														Recipient__c,
														Report_Id__c,
														Is_Repeat_Weekly__c,
														Is_Alternate_Week__c,
														Cron_Expression__c,
														(Select Id From Attachments)
														from Schedule_Report_Details__c 
														where Cron_Expression__c =:sCron])
		{
			for(Attachment attRec:sReport.Attachments){ 
				setAttachments.add(attRec.id);
				mpIdtoObj.put(attRec.id,sReport);
			}
			if(sReport.Is_Repeat_Weekly__c || sReport.Is_Alternate_Week__c){
				Datetime dt1;
				if(sReport.Is_Repeat_Weekly__c){
					sReport.Execution_Date__c = sReport.Execution_Date__c.addDays(7);
					dt1 = system.now().addDays(7);
				}
				if(sReport.Is_Alternate_Week__c){
					sReport.Execution_Date__c = sReport.Execution_Date__c.addDays(14);
					dt1 = system.now().addDays(14);
				}
				String nextCron = '0 0'+' '+String.valueOf(dt1.hour())+' '+String.valueOf(dt1.day())+' '+String.valueOf(dt1.month())+' ?';
				listCron.add(nextCron);
				sReport.Cron_Expression__c = nextCron;
				sReport.Name = sReport.Report_Id__c +'#' + nextCron;
				lstReportsUpdate.add(sReport);
			}
			else
				lstReports.add(sReport);
				
		}
		
		if(setAttachments != null && setAttachments.size() > 0)
			sendemail(setAttachments);
			
		if(lstReportsUpdate != null && lstReportsUpdate.size() > 0)		
			update lstReportsUpdate;			
			
		if(lstReports != null && lstReports.size() > 0)		
			delete lstReports;		
		integer i = 0;	
		for(String strCron:listCron){	
			ReportScheduler schReport = new ReportScheduler();
			system.schedule('Schedule Report'+i+system.now(), strCron, schReport);
			i++;					
		}						  			
	}
	
	public void sendemail(set<Id> attachmentSet ){ 
		for(Attachment sReport :[Select Id,Name, Body From Attachment where id =: attachmentSet])									
		{
			List<String> lstRecipients = new List<String>();
			String strRecipients = String.valueOf(mpIdtoObj.get(sReport.id).Recipient__c);
			String strPlaintxtbody = 'Link to the report: ';
			strPlaintxtbody += URL.getSalesforceBaseUrl().toExternalForm()+'/'+String.valueOf(mpIdtoObj.get(sReport.id).Report_Id__c);
			if(strRecipients != '' && strRecipients != null){
				if(strRecipients.contains(',')){
					for(String strrec:strRecipients.split(',')){
						lstRecipients.add(strrec.trim());
					}
				}
				else{
					lstRecipients.add(strRecipients.trim());
				}
			}	
	        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
	        attachment.setFileName(sReport.Name);
	        attachment.setBody(sReport.Body); 
	        attachment.setContentType('text/csv');
	        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
	        message.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment } );
	        message.setSubject(sReport.Name);
	        message.setPlainTextBody(strPlaintxtbody);
	        message.setToAddresses(lstRecipients);
	        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );
		}
	}	
}