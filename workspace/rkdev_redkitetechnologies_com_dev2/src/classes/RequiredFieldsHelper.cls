/*
 *  Description : Bean Class to return populated map 
 *  Revision History :
 *
 *   Version          Date            Description
 *   1.0            9/04/2012        Initial Draft
 */
public with sharing class RequiredFieldsHelper
{
	public map<String,list<String>> mapObjectName {get; set;}
	public map<String,Id> mapRecordIds {get ; set;}
	public map<String,list<String>> mapReqdFields {get; set;}
	public map<String,String> mapRecordName {get; set;}
	public map<String,String> mapObject {get; set;}
	public String strException {get; set;}
	
	public RequiredFieldsHelper()
	{
		mapReqdFields = new map<String,list<String>>();
		mapRecordName = new map<String,String>();
		mapObject = new map<String,String>();
		mapObjectName = new map<String,list<String>>();
		mapRecordIds = new map<String,Id>();
		strException = '';
	}
}