/**
 * This class contains unit tests for validating the behavior of CloneLoanController Class
 *
 */
@isTest
private class CloneLoanControllerTest {

    static testMethod void loanUnitTest() {
    	
    	Profile objProfile = [Select Id, Name From Profile Where Name='Loan Originator' limit 1];
        UserRole objUserRole = [Select Name, Id From UserRole where Name='Underwriter' limit 1];
        
        User objTestUser1 = new User(ProfileId = objProfile.Id,IsActive = true, FirstName = 'testName1',
        					Username = 'testName1@yahoo.com', LastName = 'TestLast1',
        					Email = 'testName1@gmail.com',Alias = 'test1',CommunityNickname = 'test1',
        					TimeZoneSidKey = 'America/New_York', LocaleSidKey = 'en_US', 
        					EmailEncodingKey = 'ISO-8859-1',LanguageLocaleKey = 'en_US',
        					UserRoleId = objUserRole.Id);
        	insert objTestUser1;
       		system.assert(objTestUser1.id != null,'User Created');
        	system.runAs(new User(Id = UserInfo.getUserId())){
	        Test.startTest();
	        Loan__c objLoan = new Loan__c(Name='Test record',Loan_Program__c='test pgm',Loan_Status__c='Pre-screen (Underwriting)',
	                                          Investor_Type__c = 'Freddie');
	                                          
		        //Populate all the required fields and save the record
		        objLoan.LTC__c = 15;
		        objLoan.DSCR__c = 12;
		        objLoan.LTV__c = 10;
		        
		        //Populate the fields which are to be set to null
		        objLoan.Firm_App_Underwriting_Submitted_to_HUD__c = system.today();
		        objLoan.Reviewed__c = true;
		        objLoan.Processing_Fee__c = 4000;
		        insert objLoan;
		        system.assert(objLoan.id != null,'Loan Created');
	        Deal_Team__c objDealTeam = new Deal_Team__c( LoanId__c =objLoan.Id ,Role__c = 'Prescreen Analyst',
	                                                     UserId__c = objTestUser1.Id,
	                                                     Trigger_Email_Notification__c = 0 );
		        insert objDealTeam;
		        system.assert(objDealTeam.id != null,'Deal Team Created');
	        Loan_Term__c objLoanTerm = new Loan_Term__c();
		        objLoanTerm.LoanId__c = objLoan.Id;
		        objLoanTerm.Final__c = true;
		        //insert objLoanTerm;
		        //system.assert(objLoanTerm.id != null,'Loan Term Created');
	        Property__c objProperty = new Property__c();
		        objProperty.Property_Type__c = 'Other';
		        insert objProperty;
		        system.assert(objProperty.id != null,'Property Created');
	        Loan_Property__c objLoanProperty = new Loan_Property__c();
		        objLoanProperty.LoanId__c = objLoan.Id;
		        objLoanProperty.PropertyId__c = objProperty.Id;
		        insert objLoanProperty;
		        system.assert(objLoanProperty.id != null,'Loan Property Created');
	        Contact con = new Contact();
		        con.LastName = 'test cont';
		        insert con;
			Loan_Sponsor__c  objLoanSpons = new Loan_Sponsor__c();
				objLoanSpons.Role__c = 'Principal';
				objLoanSpons.Loan__c = objLoan.Id;
				objLoanSpons.Contact__c  = con.id; 
				objLoanSpons.Sponsor_is_the_Individual__c = true;
		        insert objLoanSpons;
		        system.assert(objLoanSpons.id != null,'Loan Sponsor Created');
	        Loan_Contact__c  objLoanCont = new Loan_Contact__c();
		        objLoanCont.Role__c = 'Appriser';
		        objLoanCont.LoanId__c = objLoan.Id;
		        objLoanCont.ContactId__c = con.id;
		        insert objLoanCont;
		        system.assert(objLoanCont.id != null,'Loan Contact Created');
		    Account acc = new Account(Name = 'test Account');
		    	insert acc;
		        system.assert(acc.id != null,'Account Created');
		    Third_Party_Report__c report = new Third_Party_Report__c();
		    	report.Appraiser__c = acc.id;
		    	report.Engagement_Date__c = date.today();
		    	report.Draft_Due_Date__c  = date.today();
		    	//report.Loan__c  = objLoan.Id;
		    	report.Loan_Property__c = objLoanProperty.id;
		    	insert report;
		    	system.assert(acc.id != null,'Third Party Report Created');
	        //insert custom setting object
	        List<LoanCloneDisplayFields__c> lstLcd = new  List<LoanCloneDisplayFields__c>();
		        LoanCloneDisplayFields__c lcd = new LoanCloneDisplayFields__c(name = 'Loan_Status__c'
		        								,value__c = 'Rate Locked',isEdit__c = false);
		        	lstLcd.add(lcd);		
		        LoanCloneDisplayFields__c lcd1 = new LoanCloneDisplayFields__c(name = 'AccountId__c'
		        								,value__c = 'Lookup',isEdit__c = true); 		        								
					lstLcd.add(lcd1);
		        insert lstLcd;
	        CloneLoanController objCloneLoanController = new CloneLoanController();
	       
		        PageReference pageRef = Page.CloneLoan;
		        Test.setCurrentPage(pageRef);
		        
		        ApexPages.currentPage().getParameters().put('id', objLoan.Id);
		        ApexPages.currentPage().getParameters().put('childobjecttypes', 'Deal_Team__c,Loan_Term__c,Loan_Property__c,Loan_Contact__c,Loan_Sponsor__c');
		       
		        objCloneLoanController.initialiseObjectsForCloning();
		        PageReference pgRef = objCloneLoanController.doClone();    
		        objCloneLoanController.objectChildren[0].objectRows[0].getName();
		        objCloneLoanController.Cancel();
		        
		        List<Loan__c> lstLoan = [Select Id, Name from Loan__c where createdDate = today];   
		        system.assertEquals(lstLoan.size(), 2);
		        //Check if the Assumption fields are set to null
		        objLoan = [Select Id, Name, Processing_Fee__c,Reviewed__c, Firm_App_Underwriting_Submitted_to_HUD__c
		        				 from Loan__c where Id =: pgRef.getUrl().substringAfter('/') ];
		        system.assertEquals(objLoan.Processing_Fee__c,null);
		        system.assertEquals(objLoan.Firm_App_Underwriting_Submitted_to_HUD__c,null);
		        system.assertEquals(objLoan.Reviewed__c,false);
	        Test.stopTest();
        }
    }
}