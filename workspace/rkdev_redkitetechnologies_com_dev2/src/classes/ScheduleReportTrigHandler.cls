public with sharing class ScheduleReportTrigHandler {
    public static String mockOutput;
    public static String format= 'csv';
    public static String encoding ='UTF-8';
    public static String requestUrl;
	public static void onBeforeUpdate(String strSession, List<Schedule_Report_Details__c> lstsReports){
		//To DO: add filter to records to whom we have to send email
		set<id> setRec = new set<id>();
		for(Schedule_Report_Details__c sRec:lstsReports){
			setRec.add(sRec.Id);
		}
		system.debug('>>>>>setRec'+setRec);
		sendemail(strSession,setRec);
	}
	
	@future(callout=true)
	public static void sendemail(String uSession , set<Id> setReports){ 
		/*
    	Datetime dt = system.now();
    	String sCron = '0 28';  
				sCron += ' '+String.valueOf(dt.hour());
				sCron += ' '+String.valueOf(dt.day());
				sCron += ' '+String.valueOf(dt.month());  
				sCron += ' ?'; 
		system.debug('>>>>>sCron'+sCron);
		*/	
		Attachment att = [Select Name, Id, Body From Attachment where id = '00Pe00000022DQX' limit 1];
		for(Schedule_Report_Details__c sReport:[Select id,Report_Id__c,Recipient__c,Report_Name__c 
											from Schedule_Report_Details__c 
											where id = :setReports])
		{
			
			List<String> lstRecipients = new List<String>();
			String strRecipients = sReport.Recipient__c;
			String strPlaintxtbody = 'Link to the report: ';
			strPlaintxtbody += URL.getSalesforceBaseUrl().toExternalForm()+'/'+sReport.Report_Id__c;
			if(strRecipients != '' && strRecipients != null){
				if(strRecipients.contains(',')){
					for(String strrec:strRecipients.split(',')){
						lstRecipients.add(strrec.trim());
					}
				}
				else{
					lstRecipients.add(strRecipients.trim());
				}
			}	
						 	
	        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
	        attachment.setFileName(sReport.Report_Name__c +'.csv');
	        //attachment.setBody(report.getContent());
	        //attachment.setBody(getOutput(uSession,sReport.Report_Id__c)); 
	        attachment.setBody(att.Body); 
	        attachment.setContentType('text/csv');
	        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
	        message.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment } );
	        message.setSubject(sReport.Report_Name__c);
	        message.setPlainTextBody(strPlaintxtbody);
	        message.setToAddresses(lstRecipients);
	        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );
		}
	}
	
    public static blob getOutput(String session,String reportId){
    	
        if(requestUrl == null) {
        	requestUrl = '/' + reportId + '?'+'export=1&xf=' + format + '&enc=' + encoding;
        	requestUrl = URL.getSalesforceBaseUrl().toExternalForm().replace('http:', 'https:') + requestUrl;
            system.debug('>>>>requestUrl'+requestUrl);
        }
    	Http h = new Http();
        HttpRequest req = new HttpRequest();
        	req.setEndpoint(requestUrl);
        	req.setMethod('GET');
        	req.setHeader('Cookie','sid=' + session);
     		HttpResponse res = h.send(req);
     	system.debug('>>>>res'+res);
     	system.debug('>>>>res.getStatusCode()'+res.getStatusCode());
     	system.debug('>>>>res.getBodyAsBlob()'+res.getBodyAsBlob());
     	if(res.getStatusCode() == 200)
        	return res.getBodyAsBlob();
        return null;	
    }	
	
}