public with sharing class LinkLoanSponsorController {
	//Static Final
	public static final String PARAM_SEARCHCONFIGNAME = 'scn';
	public static final String PARAM_LINKCONFIGNAME = 'lcn';
	public static final String PARAM_FROMID = 'fid';
	public static final String PARAM_FROMNAME = 'fn';
	public static final String PARAM_RETURL = 'retURL';
	//Property
	public List<selectoption> pagesOptions {get;set;}
	public Account account {get;set;} // new account to create
 	public boolean createAccount{get;private set;}
	public String  m2mLinkUrlBase{get;private set;} //link to redirect to new LoanSponsor pg
	public String newContactLink {get;private set;} //link to redirect to new COntact record
	public String searchTerm{get;set;}  // Contains the string to be searched
	public boolean searchPerformed{get;private set;}
	public list<searchResults> lstSearchResult{get;set;} // list of searched Account and Contact records
	public String fromName{get; private set;}
	public String AccountId {get;set;}
	public integer total_no_of_pages{get;set;}
	public Integer noOfRecordPerPage {get;set;}
    public Integer selectedPage {get;set;}
    public Map<Integer, List<searchResults >> mapResults{get;set;}
    public boolean keywordSearch;
    public map<Id,sObject> mapAccount;

    public List<SelectOption> getNoOfRecordsOnPg() {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	options.add(new SelectOption(10+'',10+''));
 	 	options.add(new SelectOption(15+'',15+''));
 	 	options.add(new SelectOption(20+'',20+''));
 	 	//options.add(new SelectOption(25+'',25+''));
 	 	//options.add(new SelectOption(50+'',50+''));
 	 	return options;
  	}

    private String searchConfigName; 
	public  String linkConfigName;
	public  String retUrl; 
	public  String fromId;
    //Constructor
	public LinkLoanSponsorController(){
		map<String, String> params = ApexPages.currentPage().getParameters(); 
		searchConfigName = params.get(PARAM_SEARCHCONFIGNAME);
		linkConfigName = params.get(PARAM_LINKCONFIGNAME);
		retUrl = params.get(PARAM_RETURL);
		fromId = params.get(PARAM_FROMID);
		fromName = params.get(PARAM_FROMNAME);
		
		m2mLinkUrlBase = generateM2MLinkUrl()+'&'+M2M_LinkController.PARAM_TOID+'=';
		newContactLink = createNewContact(NULL).getUrl()+'&accid=';
		searchTerm = '';	
		createAccount = false;
		lstSearchResult = new list<searchResults>(); 
		account = new Account();
		searchPerformed= false;
		keywordSearch = false;
		AccountId = NULL; 
		
		mapResults =new Map<Integer, List<searchResults >> ();
       	noOfRecordPerPage = Integer.valueOf(System.Label.LinkLoanSponsor_NoOfRecordsPerPage);
        selectedPage = 1;
	}

	//Function to display all searched  Contacts/Accounts meeting the search term 
	public PageReference goSearch(){
		searchPerformed = false;
		createAccount = false;
		lstSearchResult.clear();
		total_no_of_pages = 0;
		selectedPage= 1;
		if (searchTerm !=null && searchTerm.trim()!=''){
			if (searchTerm.trim().length()<2){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,
				                                           System.Label.M2M_SearchTooShort));
			}
			else{
				try
				{
					searchTerm = searchTerm.trim();
					if(searchTerm !=null && searchTerm.trim()!='' && searchTerm.contains('*'))
						searchTerm= searchTerm.remove('*');
					//set of relationship that are 
					list<Relationship__c> lstRelationship = new list<Relationship__c>();
					map<Id,list<Relationship__c>> mapConRelatedAcc = new map<Id,list<Relationship__c>>();
					
					// Query realtionship object and prepare a map of Contact Id and
					// its related Accounts
	   				for(Relationship__c objRelationship : [Select Id,Name,Account__c,
	   				                                        Account__r.Name,Contact__c,
	   				                                        Contact__r.Name,
	   														Contact__r.AccountId,
	   														Contact__r.Account.Name
	   													   from Relationship__c
	   													   where Contact__r.Name like: '%'+searchTerm+'%' or
	   													   		Contact__r.Account.Name like: '%'+searchTerm+'%' or
	   													   		Account__r.Name like: '%'+searchTerm+'%'
	   													   order by Account__r.Name,Contact__r.Name]){
	   					
	   					if(!mapConRelatedAcc.isEmpty() && mapConRelatedAcc.containsKey(objRelationship.Contact__c)){
	   					
	   						mapConRelatedAcc.get(objRelationship.Contact__c).add(objRelationship);
	   					}else{
	   						mapConRelatedAcc.put(objRelationship.Contact__c,new list<Relationship__c>{objRelationship});
	   					}
						
						//Add the relationship whose ACcount name matches search term to a list
						//and contact name doesnt match
						if((objRelationship.Account__c != NULL && objRelationship.Account__r.Name != NULL &&
						    objRelationship.Account__r.Name != '' && objRelationship.Account__r.Name.containsIgnoreCase(searchTerm)
						    ) 
						   &&
							( (objRelationship.Contact__c != NULL && objRelationship.Contact__r.AccountId != NULL &&
								!objRelationship.Contact__r.Account.Name.containsIgnoreCase(searchTerm)) 
							   ||(objRelationship.Contact__r.AccountId == NULL)
							)
						   )
						{
							lstRelationship.add(objRelationship);
						}
	   				}
					//SOSL to fetch the Contact and Account matching the search term
					list<list<sObject>> searchList =[FIND :searchTerm+'*' IN ALL FIELDS RETURNING 
												 	 Account (Name,Id where (Name like : '%'+searchTerm+'%') order by Name),
		  										 	 Contact(Name,Id,AccountId,Account.Name where Name like: '%'+searchTerm+'%' 
			  						 					  									order by Name)
	   											    ];
	   				//create a map of Account
	   				 mapAccount = new map<Id,sObject>();
	   				for(sObject objAccount : searchList[0]){
	   					mapAccount.put(objAccount.Id,objAccount);
	   				}
	   				Id AccountID = NULL;
	   				set<Id> setAddedAccount = new set<Id>();
	   				//Iterate through the sorted Contacts
	   				for(sObject objContact : searchList[1]){
						
						//now add Account its Account link to allow user to add 
						if(AccountID != NULL && objContact.get('AccountId') != NULL && 
												AccountID != objContact.get('AccountId') &&
											    !mapAccount.isEmpty() &&
												mapAccount.containsKey(AccountID)){
							lstSearchResult.add(new SearchResults(mapAccount.get(AccountID),true,false,false));
							setAddedAccount.add(AccountID);
						}
						//add the Contact to the list first
						if(objContact.get('AccountId') != NULL)
							lstSearchResult.add(new searchResults(objContact,false,true,false));
						
						//check if there are its related Accounts add  them	  
						if(!mapConRelatedAcc.isEmpty() && mapConRelatedAcc.containsKey(objContact.id)){
							for( Relationship__c objRelationship : mapConRelatedAcc.get(objContact.id)){
								lstSearchResult.add(new searchResults(objRelationship,false,false,true));
							}
						}
						if(objContact.get('AccountId') != NULL)
							AccountID = (Id)objContact.get('AccountId');		
	   				}
	   				
	   				if(AccountID != NULL && !mapAccount.isEmpty() && mapAccount.containsKey(AccountID)){
						lstSearchResult.add(new SearchResults(mapAccount.get(AccountID),true,false,false));
						setAddedAccount.add(AccountID);
	   				}
	   				//Query Contact to fetch the Contacts Where Account name matches search term as they are not retrived by SOSL
	   				map<Id,list<Contact>> mapAccountsContact = new map<Id,list<Contact>>();
	   				for(Contact objContact :[Select Id,Name,AccountId,Account.Name from Contact where 
	   											Account.Name like: '%'+searchTerm+'%' 
	   											//or Id IN: setContact
	   											and (NOT Name  like: '%'+searchTerm+'%')
	   											 order by Name]  ){
	   					
	   					if(!mapAccountsContact.isEmpty() && mapAccountsContact.containsKey(objContact.AccountId)){
	   						mapAccountsContact.get(objContact.AccountId).add(objContact);
	   					}
	   					else
	   						mapAccountsContact.put(objContact.AccountId,new list<Contact>{objContact});
	   				}
	   				//Iterate the Account map and check the Account that are not added 
	   				for(sObject objAccount : searchList[0]){
	   					//if that map is not added in set
	   					if(setAddedAccount.isEmpty() || (!setAddedAccount.isEmpty() && !setAddedAccount.contains(objAccount.Id)) ||
	   								!mapAccountsContact.isEmpty() && mapAccountsContact.containsKey(objAccount.Id))
	   					{
	   						//check if they have Contact if yes add them
	   						if(!mapAccountsContact.isEmpty() && mapAccountsContact.containsKey(objAccount.Id)){
	   							for(Contact objContact :mapAccountsContact.get(objAccount.Id)){
									//add the Contact to the list first
									lstSearchResult.add(new searchResults(objContact,false,true,false));	 
									
									//if it has relationship add it   
									//check if there are its related Accounts add  them	  
									if(!mapConRelatedAcc.isEmpty() && mapConRelatedAcc.containsKey(objContact.id)){
										for( Relationship__c objRelationship : mapConRelatedAcc.get(objContact.id)){
											
											lstSearchResult.add(new searchResults(objRelationship,false,false,true));
										}
									}								
	   							}
	   						}
	   						//Now add that Account to list
	   						if(setAddedAccount.isEmpty() || (!setAddedAccount.isEmpty() && !setAddedAccount.contains(objAccount.Id)))
	   							lstSearchResult.add(new SearchResults(objAccount,true,false,false));
	   					}
	   				}
	   				//Add the Relationship records whose Account matches SearchTerm
					for(Relationship__c objRelationship : lstRelationship){
						lstSearchResult.add(new searchResults(objRelationship,false,false,true));
					}  				
	   				//END 
					//Pagination logic
					if(lstSearchResult.size()>0)
					{
			            pagesOptions = new List<SelectOption>();
			            total_no_of_pages = lstSearchResult.size()/noOfRecordPerPage;
			            if(math.mod(lstSearchResult.size(),noOfRecordPerPage) > 0){
			               total_no_of_pages = total_no_of_pages +1;
			            }
			            integer pageStartValue = 0;
			            integer pageEndValue = noOfRecordPerPage;
						for(integer i = 0; i<total_no_of_pages ; i++)
						{
				        	integer counter = i+1;
				        	pagesOptions.add(new SelectOption(counter+'',counter +''));
				        	List<searchResults> searchResultList = new List<searchResults>();
				       		for(integer j = pageStartValue ; j< pageEndValue; j++){
					        	try{
					            	searchResultList.add(lstSearchResult[j]);
					            }
					        	catch(Exception e) {
					       		}
				        	}
			                pageStartValue = pageEndValue;
			                pageEndValue = noOfRecordPerPage*(i+2);
			           		mapResults.put(counter,searchResultList);
		           	     }//End : Pagination logic
		      		}
				}catch(Exception e){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
				}
				searchPerformed = true;
				keywordSearch = true;
				return null;
			}
		}else{
		    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_SearchTooShort));
		}
		return null;
	}

	public void cancel(){
		createAccount = false;	
	}
	public void nextPage(){}

	//Function to redirect to new Contact record for selected Account
	public PageReference createNewContact(Id AccountId){
		
		String newRecordUrl = '/003/e';
		PageReference pr = new PageReference(newRecordUrl);
		map<string, string> params = pr.getParameters();
		String targetSaveUrl = generateM2MLinkUrl();

		params.put('saveURL', targetSaveUrl);
		//Default to the correct record type when creating Contact, Get the recordTypeId from custom setting
		M2M_Config__c recTypeConfig;
		if (linkConfigName!=null && linkConfigName!='') 
			recTypeConfig = M2M_Config__c.getInstance(linkConfigName); 
		if(recTypeConfig !=NULL)
			params.put('RecordType', recTypeConfig.LinkToDefaultRecordTypeId__c); //to put RecordType in the URL
		
		params.put('cancelURL', retUrl);
		params.put('retURL', retUrl);
		if(AccountId != NULL)
			params.put('accid',AccountId);
		return pr;
	}
	public void next(){
		selectedPage++;
	}
	public void previous(){
		selectedPage--;
	}
	public Boolean hasNext{  
    	get{
       	 return (selectedPage == total_no_of_pages? false:true);
    	}set;  
    }  
    public Boolean hasPrevious {  
    	get{
       		return (selectedPage == 1? false:true);
    	}
    	set;
    }
	// Function called when New Account record is to be created
	public PageReference newAccount(){
		if(keywordSearch == false){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,
													   System.Label.LinkLoanSponsor_KeywordSearchError));
			return null;
		}
		else{
			searchPerformed = false;
			//Should allow user to create new Account 
			createAccount = true;
			return null;
		}
	}
	//Function to populate 
	public PageReference assignAccountID(){
		String accId = ApexPages.currentPage().getParameters().get('entity');
		String accName = ApexPages.currentPage().getParameters().get('entityName');
		String toId = ApexPages.currentPage().getParameters().get('toID');
		
		String isIndividual = ApexPages.currentPage().getParameters().get('individual');
		
		if(accId != NULL || accId != '' )
			m2mLinkUrlBase += toId+'&entity='+accId+'&entityName='+accName;
		else
			m2mLinkUrlBase += toId;
		
		if(isIndividual != null && isIndividual != '' && isIndividual != 'null'){
			m2mLinkUrlBase += '&in='+isIndividual;
		} 
		return new pagereference(m2mLinkUrlBase);
	}
	//Function to save the newly created Account and to redirect to New Contact with this account prepopulated
	public PageReference saveAccount(){
		try{
			insert account;
			//redirect to Contact with Account prepopulated
			return createNewContact(account.Id);
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return NULL;
		}
	}

	private String generateM2MLinkUrl(){
		return '/apex/m2m_link?'+
			M2M_LinkController.PARAM_LINKCONFIGNAME+'='+linkConfigName+'&'+
			M2M_LinkController.PARAM_RETURL+'='+retUrl+'&'+
			M2M_LinkController.PARAM_FROMID+'='+fromId;
	}

	//Wrapper Class - Contains sObject and a boolean variable to check if its Account or Contact
	public class searchResults{
		public sObject sObj {get;set;}
		public Boolean isAccount {get;set;}
		public Boolean isContact {get;set;}
		public Boolean isRelationship {get;set;}
		
		public searchResults(sObject pSObj,Boolean pIsAccount,Boolean pIsContact,Boolean pIsRelationship){
			sObj = pSObj;
			isAccount = pIsAccount;
			isContact = pIsContact;
			isRelationship = pIsRelationship;
		} 
	}

	//Test Methods
	public static testMethod void testAccountSearch(){
		
		M2M_Config__c testConfig1 = new M2M_Config__c(Name='testLoanContact',  
			LinkFromField__c='Loan__c', LinkFromFieldId__c='00Na0000009r4Id', LinkFromObjectName__c='Loan__c', LinkObjectHasOtherData__c=true,
			LinkObjectName__c='Loan_Sponsor__c',LinkObjectPrefix__c='a0u',LinkToFieldId__c='00Na0000009r4Id',LinkToField__c='Contact__c',
			LinkToObjectName__c='Contact',RedirectToRecordTypeSelection__c = false,LinkToDefaultRecordTypeId__c = '012a0000001NRYGAA4');
		insert testConfig1;

		Loan__c testLoan = new Loan__c(name='testLoan');
		insert testLoan;

		//Create Account record and associate contact with it
		Account objAccount = new Account(Name ='Test Account1',Phone='123');
		insert objAccount;

		//Create a Account without Contact
		Account objAccount1 = new Account(Name =' Account1 test',Phone='12345');
		insert objAccount1;

		Contact objCon = new Contact(LastName = 'Account1 Contact',AccountId = objAccount1.Id);
		insert objCon;

		Contact objContact = new Contact(LastName = 'Test Contact',AccountId = objAccount.Id);
		insert objContact;

		Contact objContact1 = new Contact(LastName = 'Other test Contact',AccountId = objAccount.Id);
		insert objContact1;

		//Create Related Account records for Contact and check if they are added to list
		Relationship__c objRelationship = new Relationship__c(Account__c = objAccount.Id,Contact__c = objContact.Id);
		insert objRelationship;

		Relationship__c objRelationship1 = new Relationship__c(Account__c = objAccount1.Id,Contact__c = objContact.Id);
		insert objRelationship1;

		Relationship__c objRelationship2 = new Relationship__c(Account__c = objAccount1.Id,Contact__c = objCon.Id);
		insert objRelationship2;

		PageReference testPageReference = Page.LinkLoanSponsor; 
		map<string, string> params = testPageReference.getParameters();
		params.put(LinkLoanSponsorController.PARAM_FROMID,testLoan.Id);
		params.put(LinkLoanSponsorController.PARAM_FROMNAME, testLoan.Name);

		params.put(M2M_LinkController.PARAM_LINKCONFIGNAME,'testLoanContact');
		params.put(M2M_LinkController.PARAM_RETURL,testLoan.Id);
		params.put(M2M_LinkController.PARAM_FROMID,testLoan.Id);

		Test.setCurrentPage(testPageReference);
		LinkLoanSponsorController linkController = new LinkLoanSponsorController();

		system.assertEquals('' ,linkController.searchTerm);
		system.assertEquals(0 ,linkController.lstSearchResult.size());
		system.assertEquals(false ,linkController.searchPerformed);
		system.assertEquals(testLoan.Name ,linkController.fromName);
		system.assertEquals(testLoan.Id ,linkController.retUrl);

		//when search term is null characters check if error is displayed
		linkController.searchTerm = '';
		PageReference errPgRef = linkController.goSearch();
		system.assert(errPgRef == NULL);

		//when search term is less than 2 characters check if error is displayed
		linkController.searchTerm = 't';
		PageReference errPgReference = linkController.goSearch();
		system.assert(errPgReference == NULL);

		Id [] fixedSearchResults= new Id[4];
        fixedSearchResults[0] = objAccount.Id;
        fixedSearchResults[1] = objAccount1.Id;
        fixedSearchResults[2] = objContact.Id;
        fixedSearchResults[3] = objContact1.Id;
        //Required so SOSL will fire in Unit Tests Properly
        Test.setFixedSearchResults(fixedSearchResults);

		linkController.searchTerm = 'test';
		linkController.goSearch();
		system.assert(linkController.lstSearchResult.size() > 0);

		//Check on cancel create Account is set to false
		linkController.newAccount();
		linkController.cancel();
		linkController.getNoOfRecordsOnPg();
		system.assert(linkController.createAccount == false);

		//Check for create new Account record
		linkController.newAccount();
		linkController.account.Name = 'Test new Account';
		PageReference newContactPg = linkController.saveAccount();

		String targetSaveUrl = '/apex/m2m_link?'+M2M_LinkController.PARAM_LINKCONFIGNAME+'='+linkController.linkConfigName+'&'+
								M2M_LinkController.PARAM_RETURL+'='+linkController.retUrl+'&'+
								M2M_LinkController.PARAM_FROMID+'='+linkController.fromId;
								
		PageReference testContactPg = new PageReference('/003/e');
		testContactPg.getParameters().put('accid',linkController.account.Id);
		testContactPg.getParameters().put('cancelURL',testLoan.Id);
		testContactPg.getParameters().put('RecordType',testConfig1.LinkToDefaultRecordTypeId__c);
		testContactPg.getParameters().put('retURL',testLoan.Id);
		testContactPg.getParameters().put('saveURL',targetSaveUrl);

		system.assert(linkController.account.Id != NULL);
		system.assert(newContactPg.getUrl() == testContactPg.getUrl());
		linkController.next();
		linkController.previous();
		linkController.assignAccountID();
	}
}