public with sharing class M2M_NewController {
    public static final String PARAM_SEARCHCONFIGNAME = 'scn';
    public static final String PARAM_LINKCONFIGNAME = 'lcn';
    public static final String PARAM_FROMID = 'fid';
    public static final String PARAM_FROMNAME = 'fn';
    public static final String PARAM_RETURL = 'retURL';
    public static final String linkLoanSponsor = 'LinkLoanSponsor';
    public static final String SRCHLOAN = 'SearchLoan';
    public static final String SRCHPROPERTY = 'SearchProperty';
    // Properties   
    public String fromName{get; private set;}
    public M2M_NewController controller{get;private set;}

    public String searchTerm{get;set;}
    public List<sObject> searchResults{get;private set;}
    public boolean searchPerformed{get;private set;}
    
    public String  m2mLinkUrlBase{get;private set;}
    public String  searchObjectPrefix{get;private set;}
    public boolean isSearchbefore{get; set;}
    // Controller private variables
    private String searchConfigName;
    public String linkConfigName;
    public String retUrl; 
    public String fromId;
    private M2M_Config__c currSearchConfig;
    public M2M_Config__c recTypeConfig;
    private Boolean isRedirectToRecordType; 
    private Integer maxSearchResults;
    
    public M2M_NewController(){
        isSearchbefore = false;
        map<String, String> params = ApexPages.currentPage().getParameters(); 
        searchConfigName = params.get(PARAM_SEARCHCONFIGNAME);
        linkConfigName = params.get(PARAM_LINKCONFIGNAME);
        retUrl = params.get(PARAM_RETURL);
        fromId = params.get(PARAM_FROMID);
        fromName = params.get(PARAM_FROMNAME);
        if (searchConfigName!=null && searchConfigName!='') currSearchConfig = M2M_Config__c.getInstance(searchConfigName);
        if(currSearchConfig != NULL && currSearchConfig.SearchResultLimit__c != NULL)
            maxSearchResults = currSearchConfig.SearchResultLimit__c.intValue();
        else
            maxSearchResults = 100;

        searchTerm = '';        
        searchResults = new List<sObject>();
        searchPerformed = false;
        
        m2mLinkUrlBase = generateM2MLinkUrl()+'&'+M2M_LinkController.PARAM_TOID+'=';
        if(currSearchConfig != NULL && currSearchConfig.SearchObjectPrefix__c != NULL)
            searchObjectPrefix = currSearchConfig.SearchObjectPrefix__c;
        
        controller = this;
    }

    public PageReference goSearch(){ 
        searchPerformed = false;
        isSearchbefore = true;
        if (searchTerm!=null && searchTerm.trim()!=''){
            if (searchTerm.trim().length()<2){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_SearchTooShort));
            }
            else if (currSearchConfig == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_PageRefresh));
            }
            else{
                Integer maxSize = maxSearchResults + 1;
                
                String searchStringFields = 'Id,FirstName,LastName,';
                if(searchObjectPrefix != null ){
                    if (searchObjectPrefix.equals(Contact.SObjectType.getDescribe().getKeyPrefix()) == false ){
                        searchStringFields = 'Id,Name,';
                    }
                }
                if(currSearchConfig != null){
                    String escapedSearchTerm = escapeSearchString(searchTerm);
                    String soslQuery = 'FIND {'+escapedSearchTerm+'*} RETURNING '+currSearchConfig.SearchObjectName__c+' ('+searchStringFields+
                        currSearchConfig.SearchFields__c+') LIMIT '+maxSize;  
                    
                    System.debug('sosqlQuery:'+soslQuery);
                    List<List<SObject>> searchList=search.query(soslQuery);
                    
                    searchResults = searchList[0];
                    if (searchResults.size()==maxSize){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 
                            String.format(System.Label.M2M_TooManyResults, new String[]{maxSearchResults.format()}) ));
                        searchResults.remove(maxSize-1);
                    }
                }
                searchPerformed = true;
            }
        }
        else{
            searchResults = new List<sObject>();
        }
        return null;
    }
    
    public PageReference newRecord(){ 
        if(!isSearchbefore && (searchConfigName == SRCHPROPERTY || searchConfigName == SRCHLOAN)){
            ApexPages.Message myMsg;
            if(searchConfigName == SRCHPROPERTY)
                myMsg= new ApexPages.Message(ApexPages.Severity.ERROR,
                                             string.format(System.Label.Search_Validation_Message, new string[] {'Properties'}));
            else
                myMsg= new ApexPages.Message(ApexPages.Severity.ERROR,
                                             string.format(System.Label.Search_Validation_Message, new string[] {'Loans'}));
            ApexPages.addMessage(myMsg);    
            return null;        
        }
        PageReference pr;
        //Check if new record should be redirected to record type page
        if (linkConfigName!=null && linkConfigName!='' && M2M_Config__c.getInstance(linkConfigName)!= NULL)
            isRedirectToRecordType = M2M_Config__c.getInstance(linkConfigName).RedirectToRecordTypeSelection__c; 
            
        // Code Modified: 4/11/13 : Check if we have to redirect to record type page if yes then 
        //                          set URL accordingly 
        if(isRedirectToRecordType != NULL && isRedirectToRecordType == false ){
            
            String newRecordUrl = '/'+currSearchConfig.SearchObjectPrefix__c+'/e';
            pr = new PageReference(newRecordUrl);
            map<string, string> params = pr.getParameters();
            String targetSaveUrl = generateM2MLinkUrl();
            targetSaveUrl = targetSaveUrl +'&'+ M2M_LinkController.PARAM_ISNEW+'=true';
            params.put('saveURL', targetSaveUrl);
            //Default to the correct record type when creating Contact, Get the recordTypeId from custom setting
            if (linkConfigName!=null && linkConfigName!='') recTypeConfig = M2M_Config__c.getInstance(linkConfigName); 
            if(recTypeConfig !=NULL)
                params.put('RecordType', recTypeConfig.LinkToDefaultRecordTypeId__c); //to put RecordType in the URL
            
            params.put('cancelURL', retUrl);
            params.put('retURL', retUrl);
            //params.put('save_new_url', targetSaveUrl);
        }
        else if(isRedirectToRecordType == true){
            String newRecordUrl = '/setup/ui/recordtypeselect.jsp';
            pr = new PageReference(newRecordUrl);
            map<string, string> params = pr.getParameters();
            
            //String targetSaveUrl = generateM2MLinkUrl();
            //Code Modified 09/04/13: to Prepopulate Account lookup when creating Contact from Account record
            if(retUrl != NULL && retUrl.substring(1,4) == '001'){ 
                if(retUrl.contains('/'))
                     params.put('accid', retUrl.substringAfter('/'));
                else
                    params.put('accid', retUrl);
            } 
            params.put('save_new_url', Contact.SObjectType.getDescribe().getKeyPrefix()+'/e?retURL='+retUrl);
            params.put('retURL', retUrl);
            params.put('saveURL', retUrl);
            if(linkConfigName!=null && linkConfigName!='' &&  M2M_Config__c.getInstance(linkConfigName) !=NULL &&
                    M2M_Config__c.getInstance(linkConfigName).LinkToObjectName__c != NULL)
                params.put('ent',M2M_Config__c.getInstance(linkConfigName).LinkToObjectName__c); 
        }
        return pr;
    }
    
    private String generateM2MLinkUrl(){
        return '/apex/m2m_link?'+
            M2M_LinkController.PARAM_LINKCONFIGNAME+'='+linkConfigName+'&'+
            M2M_LinkController.PARAM_RETURL+'='+retUrl+'&'+
            M2M_LinkController.PARAM_FROMID+'='+fromId;
    }
    private string escapeSearchString(string searchString){
        return searchString.replaceAll('\\?','\\\\?').replaceAll('&','\\\\&').replaceAll('\\|','\\\\|').replaceAll('!','\\\\!')
            .replaceAll('\\{','\\\\{').replaceAll('\\}','\\\\}').replaceAll('\\[','\\\\[').replaceAll('\\]','\\\\]')
            .replaceAll('\\(','\\\\(').replaceAll('\\)','\\\\)').replaceAll('\\^','\\\\^').replaceAll('~','\\\\~')
            .replaceAll(':','\\\\:').replaceAll('\\\\','\\\\').replaceAll('"','\\\\"').replaceAll('\'','\\\\\'')
            .replaceAll('\\+','\\\\+').replaceAll('-',' ');//.replaceAll('-','\\\\-');
    }
    
    // Test methods ----------------------------------------------------
    public static testMethod void testThisClass(){
        M2M_Config__c testConfig = new M2M_Config__c(Name='testSearchConfig',  
            SearchFields__c='Address__c', SearchObjectName__c='Property__c', SearchObjectPrefix__c='a0G', SearchResultLimit__c=1);
        insert testConfig;
        
        M2M_Config__c testLinkConfig = new M2M_Config__c(Name='testLinkConfig',  
            SearchFields__c='Address__c', SearchObjectName__c='Property__c', SearchObjectPrefix__c='a0G', SearchResultLimit__c=1,
            LinkToDefaultRecordTypeId__c = '012V00000000EaYIAU');
        insert testLinkConfig;
        
        Loan__c testLoan = new Loan__c(name='testLoan');
        insert testLoan;
        
        PageReference testPageReference = Page.M2M_Property_New; // org specific 
        map<string, string> params = testPageReference.getParameters();
        params.put(M2M_NewController.PARAM_SEARCHCONFIGNAME, 'testSearchConfig');
        params.put(M2M_NewController.PARAM_LINKCONFIGNAME, 'testLinkConfig');
        params.put(M2M_NewController.PARAM_FROMID, testLoan.Id);
        params.put(M2M_NewController.PARAM_FROMNAME, testLoan.Name);
        params.put(M2M_NewController.PARAM_RETURL, '/'+testLoan.Id);
        
        Test.setCurrentPage(testPageReference);
        M2M_NewController testController = new M2M_NewController();
        
        // check defaults
        System.assertEquals('' ,testController.searchTerm);
        System.assertEquals(0 ,testController.searchResults.size());
        System.assertEquals(false ,testController.searchPerformed);
        System.assertEquals(1 ,testController.maxSearchResults);
        System.assertEquals('testLoan' ,testController.fromName);
        
        String m2mLink = '/apex/m2m_link?'+
            M2M_LinkController.PARAM_LINKCONFIGNAME+'=testLinkConfig&'+
            M2M_LinkController.PARAM_RETURL+'=/'+testLoan.Id+'&'+
            M2M_LinkController.PARAM_FROMID+'='+testLoan.Id+'&'+
            M2M_LinkController.PARAM_TOID+'=';
        System.assertEquals(m2mLink ,testController.m2mLinkUrlBase);
        
        // setup a search
        Property__c testProperty1 = new Property__c(name='testProperty1');      
        Property__c testProperty2 = new Property__c(name='testProperty2');
        insert new Property__c[]{testProperty1, testProperty2};
        
        testController.searchTerm = 'testPro';
        Test.setFixedSearchResults(new Id[]{testProperty1.Id, testProperty2.Id});
        testController.goSearch();
        
        System.assertEquals(1 ,testController.searchResults.size());
        System.assertEquals(true ,testController.searchPerformed);
        
        
        // setup a bad search
        testController.searchTerm = '    ';
        testController.goSearch();
        
        System.assertEquals(0 ,testController.searchResults.size());
        System.assertEquals(false ,testController.searchPerformed);


        // setup a too short search
        testController.searchTerm = 'a';
        testController.goSearch();
        
        System.assertEquals(0 ,testController.searchResults.size());
        System.assertEquals(false ,testController.searchPerformed);

        // check the new link
        PageReference newRecordPR = testController.newRecord();
        map<string,string> newRecordParams = newRecordPR.getParameters();
        
        String newLink = '/apex/m2m_link?'+
            M2M_LinkController.PARAM_LINKCONFIGNAME+'=testLinkConfig&'+
            M2M_LinkController.PARAM_RETURL+'=/'+testLoan.Id+'&'+
            M2M_LinkController.PARAM_FROMID+'='+testLoan.Id+'&'+
            M2M_LinkController.PARAM_ISNEW+'=true';
            
        System.assert(newRecordPR.getUrl().startsWith('/a0G/e'));
        System.assertEquals(newLink ,newRecordParams.get('saveURL'));
        System.assertEquals('/'+testLoan.Id ,newRecordParams.get('cancelURL'));
        System.assertEquals('/'+testLoan.Id ,newRecordParams.get('retURL'));
                
    }
    //test for AccountCOntact
    public static testMethod void testForAccContacts(){
        Account objAccount = new Account(Name ='TestAcc');
        insert objAccount;
        
        M2M_Config__c testSearchConfig = new M2M_Config__c(Name='testSearchConfig',  
            SearchFields__c='AccountId,Title,Type__c,Email,Phone', SearchObjectName__c='Contact', SearchObjectPrefix__c='003', SearchResultLimit__c=1);
        insert testSearchConfig;
        
        M2M_Config__c testConfig1 = new M2M_Config__c(Name='testAccContact',  
            LinkFromField__c='Account__c', LinkFromFieldId__c='00Na0000009rEKV', LinkFromObjectName__c='Account', LinkObjectHasOtherData__c=true,
            LinkObjectName__c='Relationship__c',LinkObjectPrefix__c='a13',LinkToFieldId__c='00Na0000009rEKz',LinkToField__c='Contact__c',
            LinkToObjectName__c='Contact',RedirectToRecordTypeSelection__c = true);
        insert testConfig1;
        
        PageReference testPageReference = Page.M2M_Property_New; // org specific 
        map<string, string> params = testPageReference.getParameters();
        params.put(M2M_NewController.PARAM_SEARCHCONFIGNAME, 'testSearchConfig');
        params.put(M2M_NewController.PARAM_LINKCONFIGNAME, 'testAccContact');
        params.put(M2M_NewController.PARAM_FROMID, objAccount.Id);
        params.put(M2M_NewController.PARAM_FROMNAME, objAccount.Name);
        params.put(M2M_NewController.PARAM_RETURL, '/'+objAccount.Id);
        
        Test.setCurrentPage(testPageReference);
        M2M_NewController testController = new M2M_NewController();
        testController.goSearch();
        // check the new link
        PageReference newRecordPR = testController.newRecord();
        map<string,string> newRecordParams = newRecordPR.getParameters();
        
        String newLink = '/apex/m2m_link?'+
            M2M_LinkController.PARAM_LINKCONFIGNAME+'=testAccContact&'+
            M2M_LinkController.PARAM_RETURL+'=/'+objAccount.Id+'&'+
            M2M_LinkController.PARAM_FROMID+'='+objAccount.Id;
            
        System.assert(newRecordPR.getUrl().startsWith('/setup/ui/recordtypeselect.jsp'));
        System.assertEquals(objAccount.Id ,newRecordParams.get('accid'));
        System.assertEquals('/'+objAccount.Id ,newRecordParams.get('retURL'));
    }
}