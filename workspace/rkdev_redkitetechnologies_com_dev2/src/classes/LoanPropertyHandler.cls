/*
 *  Description : Trigger handler for LoanProperty Trigger
 *  Version          Date          Description
 *   1.1           23/04/13     Concatenated the property data into different report
 *                              fields on the loan object.
 *   1.2           27/05/15     Added Logic for handling Case Sensitivity while populating 
 *                              Loan's "Report Property" fields (Name, Street, and City )
 *                              from Property.
 */
public with sharing class LoanPropertyHandler
{
    public void OnBeforeInsert(List<Loan_Property__c> lstLoanProperty)
    {
        //try
        //{
            Boolean isError  = false;
            Map<String,List<String>> mapPropertyIdToListContact = new Map<String,List<String>>(); 
            Map<String,set<String>> mapLoanIdToPropertyId = new Map<String,set<String>>(); 
            set<String> setLoanIdForCheckingDuplicate = new set<String>();
            set<String> setLoanId = new set<String>();
            set<String> setPropertyId = new set<String>();
            
            //Here creating map between loan id and set of property(in set we get all the property related to a particular loan)    
            for(Loan_Property__c objLP : lstLoanProperty)
            {   
                //Initially we add loan id to the set.          
                if(setLoanIdForCheckingDuplicate.size() == 0)
                    setLoanIdForCheckingDuplicate.add(objLP.LoanId__c);
                
                //if the loan property records loan id is in the set and is not in map means previously there were no loan property record with this loan id 
                //so we can directly add this id to the property set and than add property set to the map.
                if(setLoanIdForCheckingDuplicate.contains(objLP.LoanId__c) && !mapLoanIdToPropertyId.containsKey(objLP.LoanId__c))
                {
                    setPropertyId.clear();
                    setPropertyId.add(objLP.PropertyId__c);
                    mapLoanIdToPropertyId.put(objLP.LoanId__c,setPropertyId);
                }
                //if the loan property records loan id is in the set and in map means previously there were loan property record with this loan id
                //so we need to fetch the set first 
                //add property id to the set 
                //than add property set to the map.
                //by doing this we are assure that for a particular loan we get all the property id
                else if(setLoanIdForCheckingDuplicate.contains(objLP.LoanId__c) && mapLoanIdToPropertyId.containsKey(objLP.LoanId__c))
                {
                    set<String> setPropId = mapLoanIdToPropertyId.get(objLP.LoanId__c);
                    setPropId.add(objLP.PropertyId__c);
                    mapLoanIdToPropertyId.put(objLP.LoanId__c,setPropId);
                }
                //if set does not contain loan id than we add the loan id to the set ,create property set and than create map.
                else
                {
                    setPropertyId.clear();
                    setLoanIdForCheckingDuplicate.add(objLP.LoanId__c);
                    setPropertyId.add(objLP.PropertyId__c);
                    mapLoanIdToPropertyId.put(objLP.LoanId__c,setPropertyId);
                }
                setLoanId.add(objLP.LoanId__c);
            }
            
            if(setLoanId.size() > 0)
            {
                Boolean isContact = false;
                List<String> lstContactId = new List<String>(); 
                Map<String,List<String>> mapLoanIdToContact = new Map<String,List<String>>();
                String role = System.Label.PropertyContact_LoanContactRole;
                //querying all the records related to the selected loan ids.
                String strQuery = 'Select Role__c,LoanId__c,ContactId__c From Loan_Contact__c where Role__c =:role and LoanId__c IN :setLoanId';
                setLoanIdForCheckingDuplicate.clear();
                //Again same way as done above we need to create a map but between loan id and list of contact.
                //logic remains same but here we are creating map of string,list<string> instead of map string,set<string>
                for(Loan_Contact__c objLC: database.query(strQuery))
                {
                    system.debug('---------------------------objLC='+objLC);
                    if(setLoanIdForCheckingDuplicate.size() == 0)
                    {
                        setLoanIdForCheckingDuplicate.add(objLC.LoanId__c);
                        isContact = true;
                    }
                
                    if(setLoanIdForCheckingDuplicate.contains(objLC.LoanId__c) && !mapLoanIdToContact.containsKey(objLC.LoanId__c))
                    {
                        lstContactId.clear(); 
                        lstContactId.add(objLC.ContactId__c);
                        mapLoanIdToContact.put(objLC.LoanId__c,lstContactId);
                    }
                    else if(setLoanIdForCheckingDuplicate.contains(objLC.LoanId__c) && mapLoanIdToContact.containsKey(objLC.LoanId__c))
                    {
                        List<String> lstId = mapLoanIdToContact.get(objLC.LoanId__c);
                        lstId.add(objLC.ContactId__c);
                        mapLoanIdToContact.put(objLC.LoanId__c,lstId);
                    }
                    else
                    {
                        lstContactId.clear();
                        setLoanIdForCheckingDuplicate.add(objLC.LoanId__c);
                        lstContactId.add(objLC.ContactId__c);
                        mapLoanIdToContact.put(objLC.LoanId__c,lstContactId);
                    }
                }
                system.debug('------------------------------mapLoanIdToContact='+mapLoanIdToContact);
                //here we create map with contactid as key and list<property> as value.
                for(String strLoanId : mapLoanIdToPropertyId.keySet())
                {
                    set<String> setContactIdToInsert = mapLoanIdToPropertyId.get(strLoanId);
                    for(String strConId : setContactIdToInsert)
                    {
                        mapPropertyIdToListContact.put(strConId,mapLoanIdToContact.get(strLoanId));
                    }
                }
                //There exists any contact related to the selected loan than call the createpropertycontact.
                if(mapPropertyIdToListContact.size() > 0 && isContact)
                {
                    PropertyContact_Create objPropertyContact = new PropertyContact_Create();
                    isError = objPropertyContact.createPropertyContactFromLoanProject(mapPropertyIdToListContact);
                }
            }
            
            if(isError)
            {
                for(Loan_Property__c objLP : lstLoanProperty)
                {
                    objLP.addError(System.Label.PropertyContact_ErrorInserting); 
                }
            }
        //}
        //catch(exception e)
        //{
        //  System.debug(LoggingLevel.ERROR, e.getMessage());
        //  System.debug(LoggingLevel.ERROR, e.getStackTraceString());
        //} 
    }
    
    // Function used to populate Loan report fields on insert of Loan Property 
    public void OnAfterInsert(List<Loan_Property__c> lstLoanProperty){
        set<Id> setLoanId = new set<Id>();
        set<Id> setPropertyId = new set<Id>();
        map<Id,Loan__c> mapUpdatedLoans = new map<Id,Loan__c>();
        
        for(Loan_Property__c objLP : lstLoanProperty){
            setLoanId.add(objLP.LoanId__c);
            setPropertyId.add(objLP.PropertyId__c);
        }
        if(!setLoanId.IsEmpty()){
            map<Id,Loan__c> mapLoan = populateLoanMap(setLoanId);                                                           
            
            //Query Property fields that we want to concatenate to Loan
             map<Id,Property__c> mapProperty = new map<Id,Property__c>([Select Name,
                                                                               City__c,
                                                                               County__c,
                                                                               State__c,
                                                                               Address__c,
                                                                               CBSA__r.Name, 
                                                                               Property_Type__c,
                                                                               Postal_Code__c,
                                                                               Unit_Count__c from Property__c
                                                                        where Id IN: setPropertyId]);
            for(Loan_Property__c objLP : lstLoanProperty)
            {
                if(objLP.LoanId__c != NULL){
                    Loan__c objLoan = mapLoan.get(objLP.LoanId__c);
                    
                    //Add city to Loan Report field ,dont add duplicates
                    objLoan.Report_Property_City__c = checkDuplicatesAndAdd(objLoan.Report_Property_City__c,
                                                                    mapProperty.get(objLP.PropertyId__c).City__c,',');         
                    objLoan.Report_Property_County__c = checkDuplicatesAndAdd(objLoan.Report_Property_County__c,
                                                                    mapProperty.get(objLP.PropertyId__c).County__c,',');                    
                    objLoan.Report_Property_Name__c = checkDuplicatesAndAdd(objLoan.Report_Property_Name__c,
                                                                    mapProperty.get(objLP.PropertyId__c).Name,',');
                
                    objLoan.Report_Property_State__c = checkDuplicatesAndAdd(objLoan.Report_Property_State__c,
                                                                    mapProperty.get(objLP.PropertyId__c).State__c,',');
                    
                    objLoan.Report_Property_Street__c = checkDuplicatesAndAdd(objLoan.Report_Property_Street__c,
                                                                    mapProperty.get(objLP.PropertyId__c).Address__c,';');
                    objLoan.Report_Property_Type__c = checkDuplicatesAndAdd(objLoan.Report_Property_Type__c,
                                                                    mapProperty.get(objLP.PropertyId__c).Property_Type__c,',');
                    objLoan.Report_Property_Unit_Count__c = checkDuplicatesAndAdd(objLoan.Report_Property_Unit_Count__c,
                                                                String.valueOf(mapProperty.get(objLP.PropertyId__c).Unit_Count__c),',');
                    objLoan.Report_Property_CBSA__c = checkDuplicatesAndAdd(objLoan.Report_Property_CBSA__c,
                                                                    mapProperty.get(objLP.PropertyId__c).CBSA__r.Name,';'); 
                    objLoan.Report_Property_Postal_Code__c = checkDuplicatesAndAdd(objLoan.Report_Property_Postal_Code__c,
                                                                    mapProperty.get(objLP.PropertyId__c).Postal_Code__c,',');                                                
                                                                            
                    mapUpdatedLoans.put(objLP.LoanId__c,objLoan);
                }
            }
            
            if(!mapUpdatedLoans.isEmpty())
                update mapUpdatedLoans.values();                        
        }  
    }
    
    //Function to remove values of Property Fields on delete of Loan Property object
    public void OnAfterDelete(List<Loan_Property__c> lstLoanProperty){
        
        set<Id> setPropertyId = new set<Id>();
        set<Id> setLoanId = new set<Id>();
        //map of property id and its loan id
        map<Id,set<Id>> mapOfPropertyLoan = new map<Id,set<Id>>();
        
        //set of loan Id and cities that are in db related to that Loan
        map<Id,set<String>> mapLoanAndCity = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndCounty = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndName = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndState = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndAddress = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndPropertyType = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndUnitCount = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndCBSA = new map<Id,set<String>>();
        map<Id,set<String>> mapLoanAndPostalCode = new map<Id,set<String>>();
        
        map<Id,Loan__c> mapUpdatedLoans = new map<Id,Loan__c>();
        
        for(Loan_Property__c objLP : lstLoanProperty){
            setLoanId.add(objLP.LoanId__c);
            setPropertyId.add(objLP.PropertyId__c);
            
            if(mapOfPropertyLoan.isEmpty() || (!mapOfPropertyLoan.isEmpty() && !mapOfPropertyLoan.containsKey(objLP.PropertyId__c)))
                mapOfPropertyLoan.put(objLP.PropertyId__c,new set<Id>{objLP.LoanId__c});
            else if(!mapOfPropertyLoan.isEmpty() && mapOfPropertyLoan.containsKey(objLP.PropertyId__c)){
                mapOfPropertyLoan.get(objLP.PropertyId__c).add(objLP.LoanId__c);
            }
        }
        
        //query Db for all the properties related to loan 
        for(Loan_Property__c objLP : [Select PropertyId__r.Unit_Count__c,
                                           PropertyId__r.Property_Type__c, 
                                           PropertyId__r.County__c, 
                                           PropertyId__r.City__c, 
                                           PropertyId__r.Address__c, 
                                           PropertyId__r.Name,
                                           PropertyId__r.CBSA__r.Name,
                                           PropertyId__r.State__c,
                                           PropertyId__r.Postal_Code__c,
                                           PropertyId__c,LoanId__c
                                     From Loan_Property__c 
                                           where LoanId__c IN:setLoanId ]){
            mapLoanAndCity = populateMap(mapLoanAndCity, objLP.PropertyId__r.City__c,objLP.LoanId__c);
            mapLoanAndCounty = populateMap(mapLoanAndCounty, objLP.PropertyId__r.County__c,objLP.LoanId__c);
            mapLoanAndName = populateMap(mapLoanAndName, objLP.PropertyId__r.Name,objLP.LoanId__c);
            mapLoanAndState = populateMap(mapLoanAndState, objLP.PropertyId__r.State__c,objLP.LoanId__c);
            mapLoanAndAddress = populateMap(mapLoanAndAddress, objLP.PropertyId__r.Address__c,objLP.LoanId__c);
            mapLoanAndPropertyType = populateMap(mapLoanAndPropertyType, objLP.PropertyId__r.Property_Type__c,objLP.LoanId__c);
            mapLoanAndUnitCount = populateMap(mapLoanAndUnitCount,String.ValueOf(objLP.PropertyId__r.Unit_Count__c),objLP.LoanId__c);                       
            mapLoanAndCBSA = populateMap(mapLoanAndCBSA,objLP.PropertyId__r.CBSA__r.Name,objLP.LoanId__c);
            mapLoanAndPostalCode = populateMap(mapLoanAndPostalCode,objLP.PropertyId__r.Postal_Code__c,objLP.LoanId__c);
        }
        
        //query loan
        map<Id,Loan__c> mapLoan = populateLoanMap(setLoanId);
        
        //query deleted reords' property and check if there are duplicate in db then dont delete
        for(Property__c objProperty :[Select Name,City__c,County__c, State__c, Address__c,Postal_Code__c,
                                             Id,Property_Type__c, Unit_Count__c ,CBSA__r.Name
                                      from Property__c
                                      where Id IN: setPropertyId]){
            // values are to be removed frm loan
            for(Id loanId: mapOfPropertyLoan.get(objProperty.Id))
            {
                Loan__c objLoan =mapLoan.get(loanId);
                
                objLoan.Report_Property_City__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndCity,
                                                                           objLoan.Report_Property_City__c,
                                                                           objProperty.City__c,',');
                objLoan.Report_Property_County__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndCounty,
                                                                           objLoan.Report_Property_County__c,
                                                                           objProperty.County__c,',');
                objLoan.Report_Property_Name__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndName,
                                                                           objLoan.Report_Property_Name__c,
                                                                           objProperty.Name,',');
                objLoan.Report_Property_State__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndState,
                                                                           objLoan.Report_Property_State__c,
                                                                           objProperty.State__c,',');
                objLoan.Report_Property_Street__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndAddress,
                                                                             objLoan.Report_Property_Street__c,
                                                                             objProperty.Address__c,';');    
                objLoan.Report_Property_Type__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndPropertyType,
                                                                             objLoan.Report_Property_Type__c,
                                                                             objProperty.Property_Type__c,','); 
                objLoan.Report_Property_Unit_Count__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndUnitCount,
                                                                             objLoan.Report_Property_Unit_Count__c,
                                                                             String.ValueOf(objProperty.Unit_Count__c),',');
                objLoan.Report_Property_CBSA__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndCBSA,
                                                                             objLoan.Report_Property_CBSA__c,
                                                                             objProperty.CBSA__r.Name,';');
                                                                             
                objLoan.Report_Property_Postal_Code__c = checkDuplicatesAndRemove(objLoan.Id,mapLoanAndPostalCode,
                                                                          objLoan.Report_Property_Postal_Code__c,
                                                                          objProperty.Postal_Code__c,',');                                                                                                                                                                                                                                                                                                                                                                          
                
                mapUpdatedLoans.put(objLoan.Id,objLoan);                                                                                                                               
            }
        }
        
        if(!mapUpdatedLoans.isEmpty())
            update mapUpdatedLoans.values();
    }
    
    //Function to populate a map of loan and all the report fields 
    public map<Id,Loan__c> populateLoanMap(set<Id> pSetLoanId){
        
        return new map<Id,Loan__c>([Select Report_Property_City__c,
                                           Report_Property_County__c,
                                           Report_Property_Name__c,
                                           Report_Property_State__c,
                                           Report_Property_Street__c,
                                           Report_Property_Type__c,
                                           Report_Property_Unit_Count__c,
                                           Report_Property_CBSA__c,
                                           Report_Property_Postal_Code__c
                                    from Loan__c where Id IN: pSetLoanId]);
    } 
    
    public map<Id,set<String>> populateMap(map<Id,set<String>> mapLoanAndReprtFlds,String pReportFld,Id pLoanId){
        if(pReportFld != NULL){
            if(mapLoanAndReprtFlds.isEmpty() || (!mapLoanAndReprtFlds.isEmpty() &&
                                                 !mapLoanAndReprtFlds.containsKey(pLoanId)))
                mapLoanAndReprtFlds.put(pLoanId,new set<String>{pReportFld.toLowerCase()});
            else if(!mapLoanAndReprtFlds.isEmpty() && mapLoanAndReprtFlds.containsKey(pLoanId))         
                mapLoanAndReprtFlds.get(pLoanId).add(pReportFld.toLowerCase());
        }   
        return mapLoanAndReprtFlds;
    }
    
    //Function used to add property fields to LoanReportFields ,dont add duplicate
    public String checkDuplicatesAndAdd(String pLoanReportField ,String pPropertField,String pSeparator){
        if(pPropertField != NULL){
            if((pLoanReportField == NULL && pLoanReportField == '')|| 
                !(pLoanReportField != NULL &&
                    (pLoanReportField.equalsIgnoreCase(pPropertField) ||
                     pLoanReportField.startsWithIgnoreCase(pPropertField+pSeparator) ||
                     pLoanReportField.containsIgnoreCase(pSeparator+pPropertField+pSeparator) ||
                     pLoanReportField.endsWithIgnoreCase(pSeparator+pPropertField)
                     )
                  )
                )
                pLoanReportField = (pLoanReportField == NULL || pLoanReportField == '' ) ? pPropertField : 
                                                                                          pLoanReportField+pSeparator+pPropertField;
        }
        if((pLoanReportField != NULL && pLoanReportField != '' ) && pLoanReportField.startsWith(pSeparator))
            pLoanReportField = pPropertField.removeStart(pSeparator);
        else if((pLoanReportField != NULL && pLoanReportField != '' ) && pLoanReportField.endsWith(pSeparator))
            pLoanReportField = pPropertField.removeEnd(pSeparator);
        return pLoanReportField;
    }
    
    //Function used to add property fields to LoanReportFields for City,Street and name field
    public String checkDuplicatesAndAddSelected(String pLoanReportField ,String pPropertField,String pSeparator){
        if(pPropertField != NULL){
            if((pLoanReportField == NULL && pLoanReportField == '')|| 
                !(pLoanReportField != NULL &&
                    (pLoanReportField.equals(pPropertField) ||
                     pLoanReportField.startsWith(pPropertField+pSeparator) ||
                     pLoanReportField.contains(pSeparator+pPropertField+pSeparator) ||
                     pLoanReportField.endsWith(pSeparator+pPropertField)
                     )
                  )
                )
                pLoanReportField = (pLoanReportField == NULL || pLoanReportField == '' ) ? pPropertField : 
                                                                                          pLoanReportField+pSeparator+pPropertField;
        }
        if((pLoanReportField != NULL && pLoanReportField != '' ) && pLoanReportField.startsWith(pSeparator))
            pLoanReportField = pPropertField.removeStart(pSeparator);
        else if((pLoanReportField != NULL && pLoanReportField != '' ) && pLoanReportField.endsWith(pSeparator))
            pLoanReportField = pPropertField.removeEnd(pSeparator);
        return pLoanReportField;
    }    
    
    //Function used to remove property fields from LoanReportFields if there is no duplicate record
    public String checkDuplicatesAndRemove(Id pLoanId,map<Id,set<String>> pMapLoanAndRprtFld,
                                            String pRprtFld,String pFldToBeRmvd,String pSeparator){
        if(pRprtFld != NULL && pFldToBeRmvd != NULL && (pMapLoanAndRprtFld.isEmpty() || 
                                 (!pMapLoanAndRprtFld.isEmpty() && !pMapLoanAndRprtFld.containsKey(pLoanId)) ||
                                 (!pMapLoanAndRprtFld.isEmpty() && pMapLoanAndRprtFld.containsKey(pLoanId) && 
                                    !pMapLoanAndRprtFld.get(pLoanId).contains(pFldToBeRmvd.toLowerCase())
                                 )
                                )){ 
                                                            
            //now remove there are 3 cases 
            //its the Only field
            if(pRprtFld.trim().equalsIgnoreCase(pFldToBeRmvd.trim()))
                pRprtFld = '';
            else if(pRprtFld.containsIgnoreCase(pSeparator+pFldToBeRmvd+pSeparator)){  // end and middle 
                
                //To remove by ignoring case
                Integer index = pRprtFld.toLowerCase().indexOf(pSeparator+pFldToBeRmvd.toLowerCase()+pSeparator);
                pRprtFld = pRprtFld.replace(pRprtFld.substring(index, index+pFldToBeRmvd.length()+2),pSeparator);
            }
            else if(pRprtFld.endsWithIgnoreCase(pSeparator+pFldToBeRmvd)){
                pRprtFld = pRprtFld.removeEndIgnoreCase(pSeparator+pFldToBeRmvd);   
            }
            else if(pRprtFld.startsWithIgnoreCase(pFldToBeRmvd+pSeparator)){ //starting
                pRprtFld = pRprtFld.removeStartIgnoreCase(pFldToBeRmvd+pSeparator); 
            }
        }  
        if((pRprtFld != NULL && pRprtFld != '' ) && pRprtFld.startsWith(pSeparator))
            pRprtFld = pRprtFld.removeStart(pSeparator);
        else if((pRprtFld != NULL && pRprtFld != '' ) && pRprtFld.endsWith(pSeparator))
            pRprtFld = pRprtFld.removeEnd(pSeparator);
            
        return pRprtFld;
    }

    //Function used to remove property fields from LoanReportFields if there is no duplicate record for City,Street and name field
    public String checkDuplicatesAndRemoveSelected(Id pLoanId,map<Id,set<String>> pMapLoanAndRprtFld,
                                            String pRprtFld,String pFldToBeRmvd,String pSeparator){
        if(pRprtFld != NULL && pFldToBeRmvd != NULL && (pMapLoanAndRprtFld.isEmpty() || 
                                 (!pMapLoanAndRprtFld.isEmpty() && !pMapLoanAndRprtFld.containsKey(pLoanId)) ||
                                 (!pMapLoanAndRprtFld.isEmpty() && pMapLoanAndRprtFld.containsKey(pLoanId) && 
                                    !pMapLoanAndRprtFld.get(pLoanId).contains(pFldToBeRmvd)
                                 )
                                )){ 
                                                            
            //now remove there are 3 cases 
            //its the Only field
            if(pRprtFld.trim().equals(pFldToBeRmvd.trim()))
                pRprtFld = '';
            else if(pRprtFld.contains(pSeparator+pFldToBeRmvd+pSeparator)){  // end and middle 
                //To remove by ignoring case
                Integer index = pRprtFld.toLowerCase().indexOf(pSeparator+pFldToBeRmvd.toLowerCase()+pSeparator);
                pRprtFld = pRprtFld.replace(pRprtFld.substring(index, index+pFldToBeRmvd.length()+2),pSeparator);
            }
            else if(pRprtFld.endsWith(pSeparator+pFldToBeRmvd)){
                pRprtFld = pRprtFld.removeEndIgnoreCase(pSeparator+pFldToBeRmvd);   
            }
            else if(pRprtFld.startsWith(pFldToBeRmvd+pSeparator)){ //starting
                pRprtFld = pRprtFld.removeStartIgnoreCase(pFldToBeRmvd+pSeparator); 
            }
        }  
        if((pRprtFld != NULL && pRprtFld != '' ) && pRprtFld.startsWith(pSeparator))
            pRprtFld = pRprtFld.removeStart(pSeparator);
        else if((pRprtFld != NULL && pRprtFld != '' ) && pRprtFld.endsWith(pSeparator))
            pRprtFld = pRprtFld.removeEnd(pSeparator);
            
        return pRprtFld;
    }
        
    static testMethod void testConcatenatePropToLoan(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Program__c = 'Test',Loan_Status__c = 'status3');
        insert objLoan;
        
        Property__c objProperty = new Property__c(Name ='Test 1',Property_Type__c = 'Other',Address__c ='Test Street',
                                                  City__c = 'Test City',State__c='LA',Postal_Code__c= '1234',
                                                  County__c = 'Test County',Unit_Count__c = 1.2);
        insert objProperty;
        
        Property__c objProperty1 = new Property__c(Name ='Test Prop',Property_Type__c = 'Office',Address__c ='Street',
                                                  City__c = 'Test City',State__c='LA',Postal_Code__c= '5678',
                                                  County__c = 'County',Unit_Count__c = 1.4);
        insert objProperty1;
        
        Loan_Property__c objLP = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProperty.Id );
        insert objLP;
        
        test.startTest();
            objProperty= [Select County__c,City__c,Name,State__c,Address__c,Property_Type__c,Postal_Code__c,
                                 Unit_Count__c from Property__c where Id =: objProperty.Id ];
                                 
            objLoan = [Select  Report_Property_City__c,Report_Property_County__c,
                                Report_Property_Name__c,Report_Property_State__c,
                                Report_Property_Street__c,Report_Property_Postal_Code__c,
                                Report_Property_Type__c,
                                Report_Property_Unit_Count__c
                       from Loan__c where Id = : objLoan.Id];
            
            system.assertEquals(objLoan.Report_Property_City__c,objProperty.City__c);
            system.assertEquals(objLoan.Report_Property_County__c,objProperty.County__c);
            system.assertEquals(objLoan.Report_Property_Name__c,objProperty.Name);
            system.assertEquals(objLoan.Report_Property_State__c,objProperty.State__c);
            system.assertEquals(objLoan.Report_Property_Street__c,objProperty.Address__c);
            system.assertEquals(objLoan.Report_Property_Type__c,objProperty.Property_Type__c);
            system.assertEquals(objLoan.Report_Property_Unit_Count__c,String.valueOf(objProperty.Unit_Count__c));
            system.assertEquals(objLoan.Report_Property_Postal_Code__c,objProperty.Postal_Code__c);
            
            Loan_Property__c objLP1 = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProperty1.Id );
            insert objLP1;
            
            delete objLP;
            
            objProperty1= [Select County__c,City__c,Name,State__c,Address__c,Property_Type__c,Postal_Code__c,
                                 Unit_Count__c from Property__c where Id =: objProperty1.Id ];
                                 
            objLoan = [Select  Report_Property_City__c,Report_Property_County__c,
                                Report_Property_Name__c,Report_Property_State__c,
                                Report_Property_Street__c,Report_Property_Postal_Code__c,
                                Report_Property_Type__c,
                                Report_Property_Unit_Count__c
                       from Loan__c where Id = : objLoan.Id];
            
            system.assertEquals(objLoan.Report_Property_City__c,objProperty1.City__c);
            system.assertEquals(objLoan.Report_Property_County__c,objProperty1.County__c);
            system.assertEquals(objLoan.Report_Property_Name__c,objProperty1.Name);
            system.assertEquals(objLoan.Report_Property_State__c,objProperty1.State__c);
            system.assertEquals(objLoan.Report_Property_Street__c,objProperty1.Address__c);
            system.assertEquals(objLoan.Report_Property_Type__c,objProperty1.Property_Type__c);
            system.assertEquals(objLoan.Report_Property_Unit_Count__c,String.valueOf(objProperty1.Unit_Count__c));
            system.assertEquals(objLoan.Report_Property_Postal_Code__c,objProperty1.Postal_Code__c);
            
    }
}