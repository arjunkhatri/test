public with sharing class DueDiligenceListNew_NewController {
	// Static variables
	public static final String PARAM_DDLID = 'lid'; 

	// properties
	public List<SelectOption> ddListOptions{
		get{
			if (ddListOptions==null) {
				ddListOptions = getddListOptions();
			}
			return ddListOptions;
		}
		private set;
	}
	public DDL_Item__c ddlRecord{get;set;}
	public string DDLName{get; private set;}

	// private class variables
	private DDL__c parentDDL;
	private map<id, Due_Diligence_List_Item__c> ddListMap;

	// Constructors
	public DueDiligenceListNew_NewController(ApexPages.StandardController controller){
		ID DDLId = ApexPages.currentPage().getParameters().get(PARAM_DDLID);
		parentDDL = [select id, Name, loan__r.id, loan__r.Investor_Type__c, loan__r.Name, loan__r.Early_Rate_Lock__c, Type__c from DDL__c where id =:DDLID];
		DDLName = parentDDL.Name;

		ddlRecord = new DDL_Item__c(Due_Diligence_List__c=DDLId);
	}
	
	// Methods
	public List<SelectOption> getddListOptions(){
		ddListMap = new Map<ID, Due_Diligence_List_Item__c>([select id, Item_Desc__c, Sort_Order__c 
			from Due_Diligence_List_Item__c 
			where Loan_Program__c includes(:parentDDL.Loan__r.Investor_Type__c) and 
			ERL__c = :parentDDL.Loan__r.Early_Rate_Lock__c and Section__c =:parentDDL.Type__c]);
		
		list<SelectOption> options = new list<SelectOption>();
		options.add(new SelectOption('','--None--'));
		
		for(Due_Diligence_List_Item__c currDDItem : ddListMap.values()){
			options.add(new SelectOption(currDDItem.id,currDDItem.Item_Desc__c));
		}
		
		return options;
	}
	
	public PageReference save(){
		if (ddlRecord.Due_Diligence_List_Library__c!=null) ddlRecord.Sort_Order__c = 
			((Due_Diligence_List_Item__c)ddListMap.get(ddlRecord.Due_Diligence_List_Library__c)).Sort_Order__c;
		insert ddlRecord;
		return getDDEditPageRef();
	}
	
	public PageReference cancel(){
		return getDDEditPageRef();
	}
	
	private PageReference getDDEditPageRef(){
		PageReference pr = Page.DueDiligenceListEdit_New;
		pr.getParameters().put(DueDiligenceListEdit_NewController.PARAM_DDLID,parentDDL.Id);
		return pr;		
	}
	
	// test methods
	public static testMethod void testThisClass(){
		
		Loan__c testLoan = new Loan__c(Name='Test Loan', Investor_Type__c='Test', Loan_Program__c='Test', Loan_status__c='Prospect',Early_Rate_Lock__c = true);
		insert testLoan;
		
		DDL__c testDDL = new DDL__c(Type__c='Borrower and Property', Loan__c=testLoan.Id);
		insert testDDL;
		
		Due_Diligence_List_Item__c testDDLibrary = new Due_Diligence_List_Item__c(ERL__c = true,Item_Desc__c='Test Desc', Loan_Program__c='Test', Section__c='Borrower and Property',Sort_Order__c=1);
		insert testDDLibrary;
		
		Test.startTest();
		
		PageReference testPr = Page.DueDiligenceListNew_New;
		testPr.getParameters().put(DueDiligenceListNew_NewController.PARAM_DDLID, testDDL.Id);
		
		Test.setCurrentPageReference(testPr);
		DueDiligenceListNew_NewController testController = new DueDiligenceListNew_NewController(new ApexPages.StandardController(new DDL_Item__c()));
		
		System.assertEquals(2, testController.ddListOptions.size()); // none and the one we created
		
		// test the use case where user selected a item
		testController.ddlRecord.Due_Diligence_List_Library__c = testDDLibrary.Id;
		PageReference testSaveRef = testController.save();
		System.assert(testSaveRef.getUrl().startsWith(Page.DueDiligenceListEdit_New.getUrl()));
		
		List<DDL_Item__c> ddlistList = [select id from DDL_Item__c 
			where Due_Diligence_List_Library__c=:testDDLibrary.Id and Due_Diligence_List__c=:testDDL.Id];	
		System.assertEquals(1, ddlistList.size());
		
		
		Test.setCurrentPageReference(testPr);
		// try cancel
		PageReference testCancelRef = testController.cancel();
		System.assert(testCancelRef.getUrl().startsWith(Page.DueDiligenceListEdit_New.getUrl()));		
		
		Test.StopTest();
	}
}