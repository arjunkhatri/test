public with sharing class ScheduleReportsCtrl { 
	
	public scheduleRepWrap sWrap{get;set;}
	public string selectedReport{get;set;} 
	public map<String,String> mpRepIdToName = new map<String,String>();
	String sMode = 'Day';
	public String getsMode() {
        return sMode;
    }
    public void setsMode(String sMode) { this.sMode = sMode; }
	public ScheduleReportsCtrl(){ 

	}
	
	public void init(){
		Schedule_Report_Details__c srDetais =  new Schedule_Report_Details__c();
		sWrap = new scheduleRepWrap(srDetais);
		sWrap.scheduleObj.Execution_Date__c = date.today();
		sWrap.scheduleObj.PreferredStartTime__c = null;
		sWrap.scheduleObj.Recipient__c = '';	
		sWrap.isSunday = false;
		sWrap.isMonday = false;
		sWrap.isTuesday = false;
		sWrap.isWednesday = false;
		sWrap.isThursday = false;
		sWrap.isFriday = false;
		sWrap.isSaturday = false;
	}
	
 	public List<SelectOption> getReports() {
            List<SelectOption> options = new List<SelectOption>();
            for(Report repoRec:[Select id,Name from Report order by Name]){
            	options.add(new SelectOption(repoRec.id,repoRec.Name));
            	mpRepIdToName.put(repoRec.id,repoRec.Name);
            }
            return options;
    }	
    
 	public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Day','Select Day')); 
        options.add(new SelectOption('Week','Repeat Weekly')); 
        options.add(new SelectOption('Alter','Repeat Alternate Week')); 
        return options; 
    }    
    public PageReference openPanel() {

        return null;
    }    
	public class scheduleRepWrap{
		public Schedule_Report_Details__c scheduleObj{get;set;}
		public boolean isSunday{get;set;}
		public boolean isMonday{get;set;}
		public boolean isTuesday{get;set;}
		public boolean isWednesday{get;set;}
		public boolean isThursday{get;set;}
		public boolean isFriday{get;set;}
		public boolean isSaturday{get;set;}
		public scheduleRepWrap(Schedule_Report_Details__c scheduleObj){
			if(scheduleObj != null)
				this.scheduleObj = scheduleObj;
		}
	}
	
	public pagereference SendNow(){
		Boolean isRepeatWeekly = false;
		Boolean isAlterWeek = false;
		String strError = '';
		List<Schedule_Report_Details__c> lstschReports = new List<Schedule_Report_Details__c>();
		List<Attachment> lstAttchments = new List<Attachment>();
		List<datetime> lstScheduleDt = new List<datetime>();
		List<String> lstRecipients = new List<String>();
		String strExeTime = sWrap.scheduleObj.PreferredStartTime__c;
		if(strExeTime == null || strExeTime == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Preferred Start Time Required'));
			return null;
		}
		String intTime;
		if(strExeTime.contains('AM'))
			intTime = strExeTime.split(':')[0];
		else if(strExeTime.contains('PM')) 	
			intTime = String.valueOf(12 + integer.valueOf(strExeTime.split(':')[0]));
		String strRecipients = sWrap.scheduleObj.Recipient__c;
		if(strRecipients == '' || strRecipients == null){	
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Recipients Required'));
			return null;
		}
		if(sMode == 'Day'){
			lstScheduleDt.add((datetime)sWrap.scheduleObj.Execution_Date__c);
		}
		else if(sMode == 'Week' || sMode == 'Alter'){
			if(getDate() == null || getDate().size() == 0){	
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Select Atleast One Day'));
				return null;
			}			
			if(sMode == 'Week')
				isRepeatWeekly = true;
			else if(sMode == 'Alter')
				isAlterWeek = true;
			lstScheduleDt.addAll(getDate());
		}	
		integer i = 0;
		for(datetime dt:lstScheduleDt){ 
			String sCron = '0 0';
				sCron += ' '+intTime+' '+String.valueOf(dt.day())+' '+String.valueOf(dt.month())+' ?';	
				Schedule_Report_Details__c srDetais = new Schedule_Report_Details__c();
					srDetais.Name = selectedReport +'#'+sCron;
					srDetais.Cron_Expression__c = sCron;
					srDetais.Report_Id__c = selectedReport;
					srDetais.Report_Name__c = mpRepIdToName.get(selectedReport);
					srDetais.Execution_Date__c = dt.date();	
					srDetais.Recipient__c = strRecipients;
					srDetais.Is_Repeat_Weekly__c = isRepeatWeekly;
					srDetais.PreferredStartTime__c = strExeTime;
					srDetais.Is_Alternate_Week__c = isAlterWeek;
					lstschReports.add(srDetais);	
					ReportScheduler schReport = new ReportScheduler();
					system.schedule('Schedule Report'+i+system.now(), sCron, schReport);
					i++;	
		}
		if(lstschReports != null && lstschReports.size() > 0)
			insert lstschReports;
		if(lstschReports != null && lstschReports.size() > 0)
		{
	        ApexPages.PageReference report = new ApexPages.PageReference('/'+selectedReport+'?csv=1');
			for(Schedule_Report_Details__c srd:lstschReports){
				Attachment attachment = new Attachment();
	        	attachment.ParentId = srd.id;
			    attachment.Name = mpRepIdToName.get(selectedReport)+'.csv';
			    attachment.Body = report.getContent();
			    attachment.ContentType = 'text/csv';
			    lstAttchments.add(attachment);
			}
		}
			
		if(lstAttchments != null && lstAttchments.size() > 0)
			insert lstAttchments;

		intTime = '';
		init();
        pagereference pg = new pagereference('/apex/ScheduleReports');
        return pg;
	}
	public List<datetime> getDate(){
		Datetime dt = system.now();
		String dayOfWeek = dt.format('E');
		List<datetime> schDate = new List<datetime>();
		for(integer i = 0;i< 7 ;i++){
			if(sWrap.isSunday && dt.addDays(i).format('E') == 'Sun')
			{
				schDate.add(dt.addDays(i));
			}else if(sWrap.isMonday && dt.addDays(i).format('E') == 'Mon'){
				schDate.add(dt.addDays(i));
			}else if(sWrap.isTuesday && dt.addDays(i).format('E') == 'Tue'){
				schDate.add(dt.addDays(i));
			}else if(sWrap.isWednesday && dt.addDays(i).format('E') == 'Wed'){
				schDate.add(dt.addDays(i));
			}else if(sWrap.isThursday && dt.addDays(i).format('E') == 'Thu'){
				schDate.add(dt.addDays(i));
			}else if(sWrap.isFriday && dt.addDays(i).format('E') == 'Fri'){
				schDate.add(dt.addDays(i));
			}else if(sWrap.isSaturday && dt.addDays(i).format('E') == 'Sat'){
				schDate.add(dt.addDays(i));
			}
		}	
		return schDate;
	}
	
}