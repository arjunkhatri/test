/*
 * Descprition : Controller to check if the User has access to update Loan and its related objects
 *               Check the User Profile and Loan Status ,if it matches with the Loan Read Access Config
 *                custom setting record display an error message else allow user to update 
 *  
 *  Revision History:
 *
 *   Version          Date            Description
 *   1.0            11/10/2012        Initial Draft
 *
 */

public with sharing class LoanRelObj_EditDeleteController{
    
    //Constants
    public static final String PARAM_RETURL = 'returl';
    public static final String PARAM_ID = 'id';
    public static final String PARAM_DELID = 'delId';
    
    private Loan__c objLoan;
    private String delId;
    
    // Property
    public String retUrl{get; set;}
    public String id{get; set;}
    
    //Controller
    public LoanRelObj_EditDeleteController(ApexPages.StandardController stdController){
    }
    
    public PageReference init(){
        
        PageReference pageRef ;
        map<String, String> params = ApexPages.currentPage().getParameters(); 
        retUrl = params.get(PARAM_RETURL);
        id = params.get(PARAM_ID);
        delId = params.get(PARAM_DELID);
        // query user profile 
        String userProfile =  [Select Profile.Name from User where id = :Userinfo.getUserId()].Profile.Name; 
        LoanReadAccessConfig__c loanReadAccessConfig = LoanReadAccessConfig__c.getInstance(userProfile);
        String objectType =getObject(id);
        
        if(loanReadAccessConfig != NULL && loanReadAccessConfig.Status__c != NULL)
        {
            String status = this.getLoanStatus(objectType);
            if(status != NULL)
                pageRef = this.checkStatus(status,objectType,loanReadAccessConfig.Status__c);
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,
                                      System.Label.LoanRelObj_EditDeleteController_Message));
                return NULL;
            }
            return pageRef;
        }
        // If the record is not present in custom setting allow to edit or delete
        pageRef = this.editDelete(objectType);
        return pageRef;
    }
    
    public PageReference checkStatus(String pStatus,String pObjectType,String readAccessStatus){
        if(pStatus.toLowerCase() == readAccessStatus.toLowerCase()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,
                                                       System.Label.LoanRelObjEditDeleteController_ErrorMsg));
            return NULL;
        }
        else{
            Map<String,Integer> mapLoanStatus = new Map<String,Integer>();
            String loanPgm = objLoan.Loan_Program__c;
            Integer index = 1;
            String loanStatus;
            
            if(Loan_Status_Config__c.getInstance(loanPgm) == NULL){
                loanStatus = System.Label.LoanReqdFields_LoanStatus ;
            }
            else{
                Loan_Status_Config__c loanStatusConfig =  Loan_Status_Config__c.getInstance(loanPgm);
                loanStatus = loanStatusConfig.Status__c;
            }
            
            if(loanStatus != NULL){
                for(String objLoanStatus : loanStatus.split(',')){
                    objLoanStatus = objLoanStatus.trim();
                    mapLoanStatus.put(objLoanStatus.toLowerCase(),index++);
                }
            }
            if((mapLoanStatus.get(readAccessStatus.toLowerCase()) < mapLoanStatus.get(pStatus.toLowerCase())) ||
                mapLoanStatus.get(pStatus.toLowerCase()) == NULL)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,
                                                           System.Label.LoanRelObjEditDeleteController_ErrorMsg));
                return NULL;
            }
            PageReference pageRef= this.editDelete(pObjectType);
            return pageRef;
        }
    }
    
    // Check if page is called from Edit mode or Delete mode
    public PageReference editDelete(String pObjectType){
        if(delId == NULL){
            //EDIT MODE
            PageReference pageRef = new PageReference('/'+id+'/e?retURL='+retUrl);
            pageRef.getParameters().put('nooverride','1');
            return pageRef;
        }
        else{
            //DELETE MODE
            String query = 'Select Name from '+pObjectType+' where id =: delId';
            List<sObject> sObj = Database.query(query);
            if(sObj.size() != 0){
                try{
                    system.debug('*********'+sObj);
                    delete sObj;
                }catch(DMLException e){
                    system.debug('****Exception****'+e);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
                }
            }
            PageReference pageRef = new PageReference('/'+retUrl);
            pageRef.getParameters().put('nooverride','1');
            return pageRef;
        }
    }
    
    public String getLoanStatus(String pObjectType){
        
        String loanField;
        String status;
        Id LoanId;
        // get Loan field from CS
        try{
            Loan_Relationship_Config__c loanRelationshipConfig = Loan_Relationship_Config__c.getInstance(pObjectType);
            if(loanRelationshipConfig != NULL)
                loanField = loanRelationshipConfig.Loan_Field__c;
            
            String query = 'Select '+loanField+' from '+pObjectType+' where id =: id';
            List<sObject> sObj = Database.query(query);
            if(sObj.size() != 0){
                LoanId = (Id)sObj[0].get(loanField);
                objLoan = [Select Loan_Status__c,Loan_Program__c from Loan__c where id =: LoanId];
                status = objLoan.Loan_Status__c;
            }
            return status;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
            return NULL;
        }
    }

    // Function to determine the type of Object 
    public String getObject(String pId){
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, String> keyPrefixMap = new Map<String, String>{}; 
        
         // Populate a map of object prefix and Name 
        for(String sObj : schemaMap.keySet())  
        {  
            Schema.DescribeSObjectResult r =  schemaMap.get(sObj).getDescribe();  
            keyPrefixMap.put(r.getKeyPrefix(), r.getName());  
        }  
        //get the object type now  
        String objectType = keyPrefixMap.get(pId.subString(0,3)); 
        return objectType;  
    } 
    
    //Test Methods
     static testMethod void loanRelObj_EditDeleteTest(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Status__c = 'status1');
        insert objLoan;
        
        Property__c objProp = new Property__c(Name = 'Test Property');
        insert objProp;
        
        Loan_Property__c objProperty = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProp.Id);
        insert objProperty;
        
        LoanReadAccessConfig__c lraC = new LoanReadAccessConfig__c(Name='System Administrator', Status__c = 'status2');
        insert lraC;
        
        objLoan.Loan_Status__c = 'status2';
        update objLoan;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objProperty);
        LoanRelObj_EditDeleteController controller = new LoanRelObj_EditDeleteController(sc);
        
        PageReference pageRef = Page.LoanProperty_EditDelete;
        pageRef.getParameters().put('id', String.valueOf(objProperty.Id));
        pageRef.getParameters().put('delId', String.valueOf(objProperty.Id));
        Test.setCurrentPage(pageRef);
        
        controller.init();
     }
     
     static testMethod void loanRelObj_EditDeleteTest1(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Status__c = 'status1');
        insert objLoan;
        
        Property__c objProp = new Property__c(Name = 'Test Property');
        insert objProp;
        
        Loan_Property__c objProperty = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProp.Id);
        insert objProperty;
        
        LoanReadAccessConfig__c lraC = new LoanReadAccessConfig__c(Name='System Administrator', Status__c = 'status2');
        insert lraC;
        
        objLoan.Loan_Status__c = 'status2';
        update objLoan;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objProperty);
        LoanRelObj_EditDeleteController controller = new LoanRelObj_EditDeleteController(sc);
        
        PageReference pageRef = Page.LoanProperty_EditDelete;
        pageRef.getParameters().put('id', String.valueOf(objProperty.Id));
        Test.setCurrentPage(pageRef);
        
        controller.init();
     }
     
     // Test case when status of related object comes after the CS status 
     static testMethod void loanRelObj_EditDeleteTest2(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Program__c = 'Test',Loan_Status__c = 'status3');
        insert objLoan;
        
        Property__c objProp = new Property__c(Name = 'Test Property');
        insert objProp;
        
        Loan_Property__c objProperty = new Loan_Property__c(LoanId__c = objLoan.Id, PropertyId__c = objProp.Id);
        insert objProperty;
        
        Loan_Status_Config__c lsC = new Loan_Status_Config__c(Name = 'Test', Status__c ='status1,status2,status3');
        insert lsC;
        
        LoanReadAccessConfig__c lraC = new LoanReadAccessConfig__c(Name='System Administrator', Status__c = 'status2');
        insert lraC;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objProperty);
        LoanRelObj_EditDeleteController controller = new LoanRelObj_EditDeleteController(sc);
        
        PageReference pageRef = Page.LoanProperty_EditDelete;
        pageRef.getParameters().put('id', String.valueOf(objProperty.Id));
        Test.setCurrentPage(pageRef);
        controller.init();
     }
     
     // If the record is not is CS allow to Edit 
     static testMethod void loanRelObj_EditDeleteTest3(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Program__c = 'Test',Loan_Status__c = 'status3',Report_Principal__c= '');
        insert objLoan;
        
        Contact objContact = new Contact(Description = 'Test Contact',LastName ='Name');
        insert objContact;
        
        Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'Principal',Contact__c =objContact.Id,Sponsor_is_the_Individual__c = true );
        insert objLS;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objLS);
        LoanRelObj_EditDeleteController controller = new LoanRelObj_EditDeleteController(sc);
        
        PageReference pageRef = Page.LoanSponsor_EditDelete;
        pageRef.getParameters().put('id', String.valueOf(objLS.Id));
        Test.setCurrentPage(pageRef);
        
        controller.init();
        
        objLS.Role__c = 'Test Role 2';
        update objLS;
        
     }
     
     // If the record is not is CS allow to Delete 
     static testMethod void loanRelObj_EditDeleteTest4(){
        Loan__c objLoan = new Loan__c(Name = 'Test Loan',Loan_Program__c = 'Test',Loan_Status__c = 'status3');
        insert objLoan;
        
        Contact objContact = new Contact(Description = 'Test Contact',LastName ='Name');
        insert objContact;
        
        Loan_Sponsor__c objLS = new Loan_Sponsor__c(Loan__c = objLoan.Id, Role__c = 'Principal',Contact__c =objContact.Id,Sponsor_is_the_Individual__c = true );
        insert objLS;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objLS);
        LoanRelObj_EditDeleteController controller = new LoanRelObj_EditDeleteController(sc);
        
        PageReference pageRef = Page.LoanSponsor_EditDelete;
        pageRef.getParameters().put('id', String.valueOf(objLS.Id));
         pageRef.getParameters().put('delId', String.valueOf(objLS.Id));
        Test.setCurrentPage(pageRef);
        
        controller.init();
     }  
}