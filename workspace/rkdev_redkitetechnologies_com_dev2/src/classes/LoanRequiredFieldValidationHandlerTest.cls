@isTest(SeeAllData=true)
public with sharing class LoanRequiredFieldValidationHandlerTest {
    
    static testMethod void LoanReqdFieldTriggerTest()
    {
        Profile objProfile = [Select Id, Name From Profile Where Name='Loan Originator' limit 1];
        UserRole objUserRole = [Select Name, Id From UserRole where Name='Underwriter' limit 1];
        
        User objTestUser = new User();
        objTestUser.ProfileId = objProfile.Id; 
        objTestUser.IsActive = true;
        objTestUser.FirstName = 'testName';
        objTestUser.Username = 'testName@yahoo.com';
        objTestUser.LastName = 'TestLast';
        objTestUser.Email = 'testName@gmail.com';
        objTestUser.Alias = 'test';
        objTestUser.CommunityNickname = 'test123';
        objTestUser.TimeZoneSidKey = 'America/New_York';
        objTestUser.LocaleSidKey = 'en_US';
        objTestUser.EmailEncodingKey = 'ISO-8859-1';   
        objTestUser.LanguageLocaleKey = 'en_US';
        objTestUser.UserRoleId = objUserRole.Id;
        insert objTestUser;
        
        User objTestUser1 = new User();
        objTestUser1.ProfileId = objProfile.Id; 
        objTestUser1.IsActive = true;
        objTestUser1.FirstName = 'testName1';
        objTestUser1.Username = 'testName1@yahoo.com';
        objTestUser1.LastName = 'TestLast1';
        objTestUser1.Email = 'testName1@gmail.com';
        objTestUser1.Alias = 'test1';
        objTestUser1.CommunityNickname = 'test1';
        objTestUser1.TimeZoneSidKey = 'America/New_York';
        objTestUser1.LocaleSidKey = 'en_US';
        objTestUser1.EmailEncodingKey = 'ISO-8859-1';   
        objTestUser1.LanguageLocaleKey = 'en_US';
        objTestUser1.UserRoleId = objUserRole.Id;
        insert objTestUser1;
        
        system.runAs(objTestUser)
        {
            Loan_Status_Config__c statusConfig = new Loan_Status_Config__c(Name='test pgm',
                                                                           Status__c=System.Label.LoanReqdFields_LoanStatus);
            insert statusConfig;
        
            LoanValidationConfig__c validationConfig = new LoanValidationConfig__c(Name='Test',
                                                                                  Existing_Status__c='Prospecting',
                                                                                  Loan_Program__c='test pgm',
                                                                                  New_Status__c='Pre-screen (Origination)',
                                                                                  Object__c='Loan__c',
                                                                                  Required_Fields__c ='LTC__c',
                                                                                  Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig;
            
            LoanValidationConfig__c validationConfig1 = new LoanValidationConfig__c(Name='Test1',
                                                                                    Existing_Status__c='Pre-screen (Origination)',
                                                                                    Loan_Program__c='test pgm',
                                                                                    New_Status__c='Pre-screen (Underwriting)',
                                                                                    Object__c='Loan__c',
                                                                                    Required_Fields__c ='DSCR__c,LTV__c',
                                                                                    Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig1;
            
            LoanValidationConfig__c validationConfig2 = new LoanValidationConfig__c(Name='Test2',
                                                                                    Existing_Status__c='Pre-screen (Underwriting)',
                                                                                    Loan_Program__c='test pgm',
                                                                                    New_Status__c='Application Issued',
                                                                                    Object__c='Loan__c',
                                                                                    Required_Fields__c ='LTC__c,DSCR__c',
                                                                                    Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig2;
            
            LoanValidationConfig__c validationConfig3 = new LoanValidationConfig__c(Name='Test3',
                                                                                    Existing_Status__c='Pre-screen (Origination)',
                                                                                    Loan_Program__c='test pgm',
                                                                                    New_Status__c='Pre-screen (Underwriting)',
                                                                                    Object__c='Property__c',
                                                                                    Required_Fields__c ='Name,City__c,County__c',
                                                                                    Execution_Type__c = 'Fannie,Freddie');
            insert validationConfig3;
            
            Loan__c objLoan = new Loan__c(Name='Test record',Loan_Program__c='test pgm',Loan_Status__c='Pre-screen (Underwriting)',
                                          Investor_Type__c = 'Freddie');
            
            test.startTest();
            
            //Displays error if required fields are missing
            try{
                insert objLoan;
            }
            catch(Exception e){
                system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',e.getDmlStatusCode(0));
            }
            
            //Populate all the required fields and save the record
            objLoan.LTC__c = 15;
            objLoan.DSCR__c = 12;
            objLoan.LTV__c = 10;
            insert objLoan;
            
            Property__c objProperty = new Property__c(Name = 'test Property');
            insert objProperty;
            
            Deal_Team__c objDealTeam = new Deal_Team__c( LoanId__c =objLoan.Id ,Role__c = 'Prescreen Analyst',
                                                         UserId__c = objTestUser1.Id,
                                                         Trigger_Email_Notification__c = 0 );
            insert objDealTeam;
            
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            objLoan.Loan_Status__c = 'Application Issued';
            
            update objLoan;
            
            // Moving Loan status backwrd by removing required field
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            objLoan.LTC__c = NULL;
            objLoan.Loan_Status__c = 'Pre-screen (Origination)' ;
            
            try{
                update objLoan;
            }
            catch(Exception e){
                system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',e.getDmlStatusCode(0));
            } 
            test.stopTest();
        }
    } 
    
    static testMethod void testAutoNumber(){
        
        if(LoanValidationBypassConfig__c.getInstance(userinfo.getuserid()) == NULL){
            LoanValidationBypassConfig__c bypassConfig = new LoanValidationBypassConfig__c(Name = userinfo.getuserid() );
            insert bypassConfig;
        }
        
        Loan__c objLoan = new Loan__c(Name='Test record',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Freddie',OriginatorId__c = userinfo.getuserid());
        insert objLoan;
        
        
        Loan__c objLoan1 = new Loan__c(Name='Test record1',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Freddie');
        insert objLoan1;
        
        Loan__c FHALoan = new Loan__c(Name='FHA record',Loan_Program__c='221D4',Loan_Status__c='Prospecting',
                                              Investor_Type__c = 'FHA');
        insert FHALoan;
        
        
        Loan__c FHALoan1 = new Loan__c(Name='FHA record1',Loan_Program__c='221D4',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'FHA');
        insert FHALoan1;
        
        Loan__c FannieLoan = new Loan__c(Name='Fannie record',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Fannie');
        insert FannieLoan;
        
        Loan__c FannieLoan1 = new Loan__c(Name='Fannie record1',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Fannie');
        insert FannieLoan1;
        
        test.startTest();
            objLoan = [Select Loan_Autonumber_Freddie__c from Loan__c where Id = : objLoan.Id];
            objLoan1 = [Select Loan_Autonumber_Freddie__c from Loan__c where Id = : objLoan1.Id];
            
            
            FHALoan = [Select Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c,RecordTypeId from Loan__c where Id = : FHALoan.Id];
            FHALoan1 = [Select Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : FHALoan1.Id];
           
            FannieLoan = [Select Loan_Autonumber_Fannie__c from Loan__c where Id = : FannieLoan.Id];
            FannieLoan1 = [Select Loan_Autonumber_Fannie__c from Loan__c where Id = : FannieLoan1.Id];
            
            if(objLoan.Loan_Autonumber_Freddie__c != null)
                system.assert( (objLoan.Loan_Autonumber_Freddie__c+1) == (objLoan1.Loan_Autonumber_Freddie__c));
                
            if(FHALoan.Loan_Autonumber_FHA__c != null)      
                system.assert( (FHALoan.Loan_Autonumber_FHA__c+1) == (FHALoan1.Loan_Autonumber_FHA__c));
            
            if( FannieLoan.Loan_Autonumber_Fannie__c != null)   
               system.assert( (FannieLoan.Loan_Autonumber_Fannie__c+1) == (FannieLoan1.Loan_Autonumber_Fannie__c));
            
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            FannieLoan1.Investor_Type__c = 'Freddie';
            update FannieLoan1;
            
            FannieLoan1 = [Select Investor_Type__c,Loan_Autonumber_Fannie__c,Loan_Autonumber_Freddie__c from Loan__c where Id = : FannieLoan1.Id];
            
            //system.assert(FannieLoan1.Loan_Autonumber_Fannie__c == NULL
              //              &&    ((objLoan1.Loan_Autonumber_Freddie__c+1) == (FannieLoan1.Loan_Autonumber_Freddie__c)));
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;           
            FannieLoan1.Loan_Status__c = 'Prospecting';
            update FannieLoan1;
            
            FannieLoan1 = [Select Loan_Autonumber_Fannie__c,Loan_Autonumber_Freddie__c from Loan__c where Id = : FannieLoan1.Id];
            
            //system.assert( FannieLoan1.Loan_Autonumber_Freddie__c == NULL  );
            
            //Update FHA Related Records:
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            FHALoan.Loan_Program__c = '241';    
            update FHALoan;
            
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            FHALoan1.Loan_Program__c = '241';
            FHALoan1.Loan_Program__c = 'Prospecting';   
            update FHALoan1;
            //system.assert( (FHALoan.Loan_Autonumber_FHA__c+1) == (FHALoan1.Loan_Autonumber_FHA__c));
            
            FHALoan = [Select Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : FHALoan.Id];
            
            //system.assert(FHALoan.Loan_Autonumber_FHA__c == null);
                      
        test.stopTest();
    }
    
    //Test Method to check if the Deal Team Record is created on insert of the Loan 
    static testMethod void testDealTeamCreation(){
        Loan__c objLoan = new Loan__c(Name='Test record',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Freddie',OriginatorId__c = userinfo.getuserid());
        insert objLoan;
        
        list<Deal_Team__c> lstDealTeam = [Select LoanId__c,Role__c,UserId__c from Deal_Team__c
                                            where UserId__c =: objLoan.OriginatorId__c and
                                                  LoanId__c =: objLoan.Id and
                                                  Role__c =: 'Originator' ];
            system.assert(lstDealTeam.size() > 0);
        
        Loan__c objLoan1 = new Loan__c(Name='Test record',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Freddie');
        insert objLoan1;
        
    }
    
    static testMethod void testFHALoanUpdate(){
        
        Loan__c FHALoan = new Loan__c(Name='FHA record',Loan_Program__c='241',Loan_Status__c='Prospecting',
                                              Investor_Type__c = 'FHA');
        insert FHALoan;
        Loan__c FHALoan1 = new Loan__c(Name='FHA record',Loan_Program__c='221D4',Loan_Status__c='Prospecting',
                                              Investor_Type__c = 'FHA');
        insert FHALoan1;
        Test.startTest();
        Loan__c tempFHALoan =  [Select Id,Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : FHALoan.Id];
        tempFHALoan.Loan_Program__c = '223F';   
        tempFHALoan.Loan_Status__c = 'Prospecting';
        update tempFHALoan;
        
        FHALoan1 =  [Select Id,Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : FHALoan1.Id];
        FHALoan1.Loan_Program__c = '232NC'; 
        update FHALoan1;
        
        //To check FHA loan auto number for other than 221D4
        FHALoan1 =  [Select Id,Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : FHALoan1.Id];
         FHALoan1.Loan_Program__c = '223F'; 
        FHALoan1.Loan_Status__c = 'Firm App Underwriting';
        update FHALoan1;
        
        Test.stopTest();
        Loan__c testFHALoan = [Select Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : tempFHALoan.Id];
        
        FHALoan1 = [Select Loan_Autonumber_FHA__c, Loan_Program__c, Loan_Status__c from Loan__c where Id = : FHALoan1.Id];
        system.debug('**Value='+ FHALoan1.Loan_Autonumber_FHA__c);
    }
    
    static testMethod void testFhaAutoNumber1() {
        
      //Test 1  : expect NULL
       Loan__c FHALoan = new Loan__c(Name='FHA record',Loan_Program__c='221D4',Loan_Status__c='Prospecting',
                                              Investor_Type__c = 'FHA');
       insert  FHALoan;
       
       FHALoan.Loan_Program__c = '232NC';
       FHALoan.Loan_Status__c='Prospecting';
       update FHALoan;
       
       FHALoan = [SELECT Loan_Autonumber_FHA__c FROM Loan__c WHERE Id =: FHALoan.Id];
       system.assertEquals(FHALoan.Loan_Autonumber_FHA__c, null);
    }
    static testMethod void testFhaAutoNumber2() {
       //Test 2: Expect value
       Loan__c FHALoan1 = new Loan__c(Name='FHA record',Loan_Program__c='221D4',Loan_Status__c='Prospecting',
                                              Investor_Type__c = 'FHA',Loan_Autonumber_FHA__c = 10);
       insert  FHALoan1;
       
       FHALoan1.Loan_Program__c = '232NC';
       FHALoan1.Loan_Status__c='Firm App Underwriting';
       update FHALoan1;
       FHALoan1 = [SELECT Loan_Autonumber_FHA__c FROM Loan__c WHERE Id =: FHALoan1.Id];
       system.assertNotEquals(FHALoan1.Loan_Autonumber_FHA__c, null);
    }
    static testMethod void testFhaAutoNumber3() {
       //Test 3: Expect value
       Loan__c FHALoan1 = new Loan__c(Name='FHA record',Loan_Program__c='241',Loan_Status__c='Prospecting',
                                              Investor_Type__c = 'FHA',Loan_Autonumber_FHA__c = 10);
       insert  FHALoan1;
       
       FHALoan1.Loan_Program__c = '223F';
       FHALoan1.Loan_Status__c='Firm App Underwriting';
       update FHALoan1;
       FHALoan1 = [SELECT Loan_Autonumber_FHA__c FROM Loan__c WHERE Id =: FHALoan1.Id];
       system.assertNotEquals(FHALoan1.Loan_Autonumber_FHA__c, null);
    }
    
    static testMethod void testFannieAutoNumber(){
        Loan__c FannieLoan = new Loan__c(Name='Fannie record',Loan_Program__c='Conventional',Loan_Status__c='Prospecting',
                                          Investor_Type__c = 'Fannie');
        insert FannieLoan;
        
        FannieLoan.Loan_Program__c = 'Small Loans';
        update FannieLoan;
        system.assertEquals(FannieLoan.Loan_Autonumber_Fannie__c, null);
    }
    
    static testMethod void testSendEmail(){
        /*
        List<User> pu = [select id from User WHERE Email='dev2@redkitetechnologies.com'];
        Id originatorid = null;
        if(pu.size() > 0){
            originatorid = pu.get(0).Id;
            Integer emailbefore = Limits.getEmailInvocations();
            Loan__c loanBroker = new Loan__c(Name='Fannie record',Loan_Program__c='Conventional',Loan_Status__c='Prospecting', OriginatorId__c = originatorid,
                                              Investor_Type__c = 'Brokered', Loan_Purpose__c='Refinance',Lien_Position__c='2', Broker__c='test12345', Borrower_Requested_Loan_Amount__c=1000);
            insert loanBroker;
            
            
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            loanBroker.Broker__c = 'test56785698989';
            update loanBroker;
            
            LoanRequiredFieldValidationHandler.isBeforeUpdateFired = false;
            loanBroker.Broker__c = 'testn123';
            update loanBroker;
            
            
            //Bulk Insert
            List<Loan__c> lstLoan = new List<Loan__c>();
            for(Integer i=0;i<=20;i++){
                Loan__c loanBroker1 = new Loan__c(Name='Fannie record',
                                                 Loan_Program__c='Conventional',
                                                 Loan_Status__c='Underwriting', 
                                                 OriginatorId__c = originatorid,
                                                 Investor_Type__c = 'Brokered', 
                                                 Loan_Purpose__c='Refinance',
                                                 Lien_Position__c='2', 
                                                 Broker__c='ntest'+ i, 
                                                 Borrower_Requested_Loan_Amount__c=1000);
                lstLoan.add( loanBroker1 );                                  
            }
            
            insert lstLoan;
            system.assertNotEquals(emailbefore,Limits.getEmailInvocations(),'should have decreased');
        }
        //system.assertEquals(FannieLoan.Loan_Autonumber_Fannie__c, null);*/
    }
    
}