/* 
 *  Descprition : Helper Class to get the Object from prefix of Object Id
 * 
 *  Revision History:
 *
 *   Version          Date            Description
 *   1.0            9/03/2012      Initial Draft  
 *
 */
public with sharing class GetObjectTypeHelper {
	
	//Properties 
    public static Map<String, String> keyPrefixMap {get; private set;}  // map to store objects and their prefixes  
    public static map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    public static Set<String> keyPrefixSet;  // to hold set of all sObject prefixes  
	
	//Returns Object type from the prefix of object Id
	public static String getKeyPrefix(String ObjId)  
    {  
        keyPrefixMap = new Map<String, String>{};   // to store objects and their prefixes   
          
        // Populate a map of object prefix and Name 
        for(String sObj : schemaMap.keySet())  
        {  
            Schema.DescribeSObjectResult r =  schemaMap.get(sObj).getDescribe();  
            keyPrefixMap.put(r.getKeyPrefix(), r.getLabel());  
        }  
        
        //get the object type now  
        String objectType = keyPrefixMap.get(ObjId.subString(0,3)); 
        return objectType;  
    }  
    
	// Test methods
	static testMethod void GetKeyPrefixTest(){
		Loan__c objLoan = new Loan__c(Name='Test');
		insert objLoan;
		
		//GetObjectTypeHelper getObject = new GetObjectTypeHelper();
		String obj = GetObjectTypeHelper.GetKeyPrefix(objLoan.Id);
		
		system.assertEquals(obj,'Loan');  // Check if the object is Loan
		
	}
}