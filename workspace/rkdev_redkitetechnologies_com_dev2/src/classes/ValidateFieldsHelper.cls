/* 
 * Description - Helper Class to validate if the required fields are populated before moving to next status 
 *  
 * Revision History:
 *
 *   Version          Date            Description
 *   1.0            9/03/2012      Initial Draft
 *
 */
public with sharing class ValidateFieldsHelper {  
    //Constants
    public static final String name = 'Name';
    public static final String oneToMany = 'One-to-Many';
    public static final String ManyToMany = 'Many-to-Many';
    public static final String self = 'Self';
    public static final String frmTrigger = 'trigger';
    public static final String frmClass = 'Class'; 
    public static final String loanTerm = 'Loan_Term__c';
    
    
    public map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    public list<String> lstFields{get; private set;} 
    RequiredFieldsHelper objReturnHlp = new RequiredFieldsHelper();
    
    public RequiredFieldsHelper formQuery(Loan__c pLoanNew, String pObject, String pReqdFields, Id pLoanId, String strType,Id loanSpon){
        
        String query;
        Set<Id> setRecordId = new Set<Id>();
        RequiredFieldsHelper objReturnForm = new RequiredFieldsHelper();
        
        //Dynamic Query to fetch the fields from resptive objects based on Loan Id
        if(pReqdFields != NULL && pReqdFields.contains(name)){
            if(pReqdFields.endsWith(','))
                pReqdFields = pReqdFields.substring(0,pReqdFields.length() - 1);
            query = 'Select '+pReqdFields+ ',Id from '+pObject+ ' where';
        }
        else if(pReqdFields != NULL && pReqdFields != ''){ 
            if(pReqdFields.endsWith(','))
                pReqdFields = pReqdFields.substring(0,pReqdFields.length() - 1);            
            query = 'Select '+pReqdFields+ ',Name,Id from '+pObject+ ' where';
        }
        
        Loan_Relationship_Config__c relationshipConfig =  Loan_Relationship_Config__c.getInstance(pObject);
        if(relationshipConfig != NULL && relationshipConfig.Type__c != NULL &&
            (relationshipConfig.Type__c.equalsIgnoreCase(oneToMany) 
            || relationshipConfig.Type__c.equalsIgnoreCase(self))){
            query += ' '+relationshipConfig.Loan_Field__c+'=: pLoanId';
             /*Code Modifed 15/04/13 : When the user has selected one of the loan terms as final, the
              *                        required field logic should only apply to the selected final loan term.
              */
             if(pObject.equalsIgnoreCase(loanTerm)){
                //if object is LT then add condition final = true
                query += ' and Final__c ='+true; 
            } //END
            
        }
        else if(relationshipConfig != NULL && relationshipConfig.Type__c != NULL && 
                relationshipConfig.Type__c.equalsIgnoreCase(ManyToMany))
        {
            List<String> lstRelFields = relationshipConfig.Relationship_Field__c.split(',');
            List<String> lstLoanFields = relationshipConfig.Loan_Field__c.split(',');
            
            integer idx = 0; 
            for(String obj : relationshipConfig.Related_Objects__c.split(','))
            {   
                if(lstRelFields[idx] != NULL && lstLoanFields[idx] != NULL)
                {
                    String relQuery = 'Select '+lstRelFields[idx]+' from '+obj+' where '+lstLoanFields[idx]+' =: pLoanId';
                    List<sObject> lstObj = Database.query(relQuery);
                
                    if(lstObj != null && lstObj.size() > 0)
                    {
                        for(sobject obj1 : lstObj)
                            setRecordId.add(String.valueOf(obj1.get(lstRelFields[idx])));
                    }
                    idx++;
                }
            }
            query += ' Id IN :setRecordId';
        }
        else{//if field absent on parent object account
            if(loanSpon != null){
                query += ' Id =\''+loanSpon+'\'';
            }
            else{
                query = null;
            }
        }
        try{
            if(query != null){
                List<sObject> lstSObject = Database.query(query);
                /*Code Modified 15/04/13 : Check if object is LT and lstSobject is Empty i.e 
                 *                         there is no Final LT then change query to include all LT 
                 */
                if(pObject.equalsIgnoreCase(loanTerm) && lstSObject.isEmpty())
                {
                    lstSObject = Database.query(query.substringBefore('and'));
                } //END
    
                objReturnForm = validateFields(pLoanNew, pObject, pReqdFields, pLoanId, strType, lstSObject);
            }
            return objReturnForm;       
        }
        catch(Exception e)
        { 
            if(strtype == frmClass)
            {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,String.valueOf(e)));
                    return null;
            }
            else
            {   
                objReturnForm = new RequiredFieldsHelper();
                objReturnForm.strException = e.getMessage();
                return objReturnForm;
            }
        } 
        
    }
    
    public RequiredFieldsHelper validateFields(Loan__c pLoanNew, String pObject,String pReqdFields, Id pLoanId,String strType,List<sObject> plstSObject){
        
        //Populate a map of fields of object
        Schema.SObjectType schemaObjType = schemaMap.get(pObject);
        Map<String, Schema.SObjectField> fieldMap = schemaObjType.getDescribe().fields.getMap();
        String objectType;
        try{
    //      if(!plstSObject.isEmpty()){
                for(sObject obj : plstSObject)
                {
                    lstFields = new list<String>();
                    Set<String> setDupFields = new Set<String>();
                    if(objectType == null){
                        objectType =GetObjectTypeHelper.getKeyPrefix((String)obj.Id);
                    }
                    for(String field :  pReqdFields.split(','))
                    {
                        // If fields are blank add them to list
                        if((obj.get(field.trim()) == NULL || obj.get(field.trim()) == ''))
                            lstFields.add(fieldMap.get(field.trim()).getDescribe().getLabel());
                    }
                    //Populate a map of Object and its missing fields
                    if(!lstFields.isEmpty())
                    { 
                        if(strType.equalsIgnoreCase(frmTrigger)){
                            if(objReturnHlp.mapObjectName != null && objReturnHlp.mapObjectName.containsKey(objectType+'-'+(String)obj.get('Name')))
                            {
                                List<String> prevFields = objReturnHlp.mapObjectName.get(objectType+'-'+(String)obj.get('Name'));
                                setDupFields.addAll(prevFields);
                                for(String fields : lstFields)
                                {
                                    
                                    if(setDupFields != null && setDupFields.size() > 0 && !setDupFields.contains(fields))
                                        prevFields.add(fields);
                                }
                                objReturnHlp.mapObjectName.put(objectType+'-'+(String)obj.get('Name'),prevFields);
                            }
                            else
                                objReturnHlp.mapObjectName.put(objectType+'-'+(String)obj.get('Name'),lstFields);
                            
                            
                            
                            objReturnHlp.mapRecordIds.put(objectType+'-'+(String)obj.get('Name'),obj.Id);
                            
                        }
                        else if(strType.equalsIgnoreCase(frmClass))
                        {
                            if(objReturnHlp.mapReqdFields != NULL && objReturnHlp.mapReqdFields.containsKey(obj.Id)){
                                List<String> allFields = objReturnHlp.mapReqdFields.get(obj.id);
                                for(String fields : lstFields){
                                //  if(setDupFields != null && setDupFields.size() > 0 && !setDupFields.contains(fields))
                                        allFields.add(fields);
                                }
                                objReturnHlp.mapReqdFields.put(obj.Id,allFields);
                            }
                            else
                                objReturnHlp.mapReqdFields.put(obj.Id,lstFields);
                            
                            objReturnHlp.mapRecordName.put(obj.Id,(String)obj.get('Name'));
                            objReturnHlp.mapObject.put(obj.Id,objectType);   
                        }
                    }
                }
    //      }else if(plstSObject.isEmpty()){
                /*
                lstFields = new list<String>();
                for(String field :  pReqdFields.split(','))
                {
                    lstFields.add(fieldMap.get(field.trim()).getDescribe().getLabel());
                }
                System.debug('******pObject********'+pObject);
                //objReturnHlp.mapObjectName.put(pObject,lstFields);
                */
    //      }
            return objReturnHlp; 
        }
        catch(Exception e){
            if(strtype == frmClass)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,String.valueOf(e)));
                return null;
            }
            else
            {   
                objReturnHlp = new RequiredFieldsHelper();
                objReturnHlp.strException = e.getMessage();
                return objReturnHlp;
            }
        }  
    }
}