public with sharing class DueDiligenceListHandler {
	
	public void onAfterInsert(Set<Id> setId ,map<Id,DDL__c> mapDDl){
		generateDDL(setId,mapDDl);
	}
	public void onAfterUpdate(List<DDL__c> lstDDL , map<Id,DDL__c> mapDDl){
		
		// Set of DDL whose DDL items are not created
		set<Id> setDDLtoCreate = new set<Id>();
		
		//check if "Due_Diligence_List_Created__c" is unchecked then only generate DDl items
		for(DDL__c objDDL : lstDDL){
			if(!objDDL.Due_Diligence_List_Created__c){
				setDDLtoCreate.add(objDDL.id);
			}
		}
		if(!setDDLtoCreate.isEmpty()){
			generateDDL(setDDLtoCreate,mapDDl);
		}
	}
	
	public void generateDDL(set<Id> setId ,map<Id,DDL__c> mapDDl){
		
		List<DDL__c> lstDDL = [select Id,Type__c,Loan__c, Due_Diligence_List_Created__c
		                       From DDL__c where Id in: setId];
		                       
		//map of DDL ids and its corresponding loan ids
		map<Id,Id> mapOfIds = new map<Id,Id>();
		//List of generated DDL Item
		List<DDL_Item__c> lstDueDiligenceList = new List<DDL_Item__c>();
		// List of updated DDL
		List<DDL__c> lstUpdatedDDL = new List<DDL__c>();
		
		set<String> setInvestorType = new set<String>();
		set<Boolean> setEarlyRateLock = new set<Boolean>();
		set<String> setType = new set<String>();
		 
		for(DDL__c objDDL : lstDDL){
			mapOfIds.put(objDDL.id , objDDL.Loan__c);
			setType.add(objDDL.Type__c);
		}
		map<Id,Loan__c> mapOfLoan = new map<Id,Loan__c>((List<Loan__c>)[Select Id,
		                                                                       Loan_Status__c,
		                                                                       Investor_Type__c, 
		                                                                       Early_Rate_Lock__c 
		                                                                from Loan__c
		                                                                where Id in : mapOfIds.values()]);
		for(Id objId : mapOfLoan.keySet()){
			Loan__c objLoan = mapOfLoan.get(objId);
			setInvestorType.add(objLoan.Investor_Type__c);
			setEarlyRateLock.add(objLoan.Early_Rate_Lock__c);
		}
		
		List<Due_Diligence_List_Item__c> lstDDLI = [SELECT Id,
														   ERL__c,
		                                                   Section__c,	
		                                                   Item_Desc__c,
		                                                   Sort_Order__c,
		                                                   Loan_Program__c
		                                            FROM Due_Diligence_List_Item__c
		                                            WHERE Loan_Program__c in: setInvestorType 
		                                                  AND ERL__c in: setEarlyRateLock
		                                                  AND Section__c in: setType ];
		
		for(DDL__c objDDL : lstDDL){

			Loan__c objLoan = mapOfLoan.get(mapOfIds.get(objDDL.id));
			
			//checking whether investor type is null.
	        if(objLoan.Investor_Type__c != null && objDDL.Due_Diligence_List_Created__c == false)
	        {
	        	//as we are creating Due diligence list so make "Due_Diligence_List_Created__c" field in loan as true. 
            	objDDL.Due_Diligence_List_Created__c = true;
               
            	//here we create due diligence list.
                for(Due_Diligence_List_Item__c objDDLL : lstDDLI)
                {
                	if(objLoan.Investor_Type__c == objDDLL.Loan_Program__c &&  
                	   objLoan.Early_Rate_Lock__c == objDDLL.ERL__c &&
                	   objDDL.Type__c ==  objDDLL.Section__c)
                	{
		                DDL_Item__c objDDLI = new DDL_Item__c();
		                objDDLI.Due_Diligence_List_Library__c = objDDLL.Id;
		                objDDLI.Due_Diligence_List__c = objDDL.Id;
		                objDDLI.Item_Desc__c = objDDLL.Item_Desc__c;
		                objDDLI.Sort_Order__c = objDDLL.Sort_Order__c;
		                lstDueDiligenceList.add(objDDLI);
                	}
                }
                lstUpdatedDDL.add(objDDL);
			}
			//if loan type is null display error.
	        else if(objLoan.Investor_Type__c == NULL){
	        	mapDDl.get(objDDL.id).addError(System.Label.DueDiligenceGen_NoLoanTypeError);
	        }  
		}
		try{
            if(lstDueDiligenceList.size() > 0){
                insert lstDueDiligenceList;
                update lstUpdatedDDL;
            }
        }
        catch(DMLException e){
        	for(DDL__c objDDL : lstDDL){
	            if(e.getDmlType(0) == System.StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY)
	                mapDDl.get(objDDL.id).addError(System.Label.DueDiligenceGen_Error);
	            else
	                 mapDDl.get(objDDL.id).addError(e.getMessage()+'|'+e.getStackTraceString());
        	}
        }
	}
	
	public static testMethod void testForGenDueDiligence()
    {
    	Account objAcc = new Account();
        objAcc.Name = 'test';
        insert objAcc;
        
        RecordType objRecId = [SELECT Name,Id FROM RecordType WHERE Name='Fannie' Limit 1];
        
        Loan__c objLoan = new Loan__c(RecordTypeId = objRecId.Id,AccountId__c = objAcc.Id,Name = 'testLoan',
                                      Investor_Type__c = 'Fannie',Loan_Status__c = 'Prospecting',
                                      Due_Diligence_List_Created__c = false,Early_Rate_Lock__c = true);
        insert objLoan;
        
        system.assert(objLoan.Id != null, 'There is problem in inserting loan');
        
        // insert a test DDI list item
        Due_Diligence_List_Item__c objDDLL = new Due_Diligence_List_Item__c();
        objDDLL.Item_Desc__c = 'testList';
        objDDLL.Loan_Program__c = 'Fannie';
        objDDLL.ERL__c = true;
        objDDLL.Section__c = 'test';
        insert objDDLL;
        
        system.assert(objDDLL.Id != null, 'There is problem in inserting Due Diligence List Item');
        
        // insert a test DDL
        DDL__c objDDL = new DDL__c();
        objDDL.Due_Diligence_List_Created__c = false;
        objDDL.Type__c = 'Key Principal';
        objDDL.Loan__c = objLoan.Id;
        objDDL.Type__c = 'test';
        insert objDDL; 
        
        DDL__c objDL = [Select Due_Diligence_List_Created__c from DDL__c where id =: objDDL.id];
        system.assert(objDDL.Id != null, 'There is problem in inserting Due Diligence List');
        system.assert(objDL.Due_Diligence_List_Created__c, 'The Due Diligence List Created is not set to true');
    }
}