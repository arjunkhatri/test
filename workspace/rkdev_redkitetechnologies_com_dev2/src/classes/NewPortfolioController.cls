/*
 * Description - Controller for NewPortfolio page called from New Portfolio button on loan object. 
 *
 *   Version          Date          Description
 *     1.0           26/04/2013     Create a Portfolio record and link it to Loan.	
 *		 			 
 */
public with sharing class NewPortfolioController {
	public static final String PARAM_LOANID = 'LoanId'; 
	
	//Properties
	public ID loanId{get;private set;}
	public String user{get;set;} 
	public Portfolio__c portfolio{get;set;}
	
	//Constructor
	public NewPortfolioController(){
		 // must assume this is always coming from the Load Button
		 loanId = ApexPages.currentPage().getParameters().get(PARAM_LOANID);
		 portfolio = new Portfolio__c();
		 if(loanId != NULL){
		 	user = UserInfo.getName();
		 	//portfolio = new Portfolio__c();
		 }else{
		 	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Loan'));
		 }
	}
	
	//Function to redirect to loan on click of cancel
	public PageReference Cancel(){
		PageReference pgRef = new PageReference('/'+loanId);
		pgRef.setRedirect(true);
		return pgRef;
	}
	
	//Function called on save button to create Portfolio and link it to Loan record
	public PageReference saveAndLink(){
		try{
			if(portfolio.Name  == NULL || portfolio.Name == ''){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.LinkLoanPortfolio_NameRequired));
				return NULL;
			}
			insert portfolio;
			if(portfolio.Id != NULL){
				Loan__c objLoan =new Loan__c (Id = loanId, Portfolio__c = portfolio.Id);
				update objLoan;
			}
			return new PageReference('/'+loanId);	
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return NULL;
		}
	}
	
	//Test method
    public static testMethod void testNewPortfolioController(){
  		Loan__c testLoan = new Loan__c(name='testLoan');
		insert testLoan;
  		
  		PageReference testPageReference = Page.NewPortfolio; 
		map<string, string> params = testPageReference.getParameters();
		params.put(LinkLoanPortfolioController.PARAM_LOANID,testLoan.Id );
		
		Test.setCurrentPage(testPageReference);
		NewPortfolioController testController = new NewPortfolioController();  
		
		testController.portfolio.Name ='Test';
		testController.saveAndLink();
		
		Portfolio__c objPortfolio = [Select Name from Portfolio__c where Name =:'Test'];
		system.assert(objPortfolio.Id != NULL);
		
		testLoan = [Select Portfolio__c from Loan__c where Id =: testLoan.Id];
		system.assert(testLoan.Portfolio__c == objPortfolio.Id);
		
		PageReference cancelUrl =testController.Cancel(); 
		PageReference testCancel = new PageReference('/'+testLoan.Id);
		system.assertEquals(cancelUrl.getUrl(),testCancel.getUrl());
		
		
		//set name to NULL
		testController.portfolio.Name ='';
		testController.saveAndLink();
		
    }
     public static testMethod void testNegativeCase(){
     	PageReference testPageReference = Page.NewPortfolio; 
		map<string, string> params = testPageReference.getParameters();
		params.put(LinkLoanPortfolioController.PARAM_LOANID,NULL);
		
		Test.setCurrentPage(testPageReference);
		NewPortfolioController testController = new NewPortfolioController();  
		
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		
		system.assert(msgs[0].getDetail() == 'Invalid Loan');
    	testController.saveAndLink();
	}
}