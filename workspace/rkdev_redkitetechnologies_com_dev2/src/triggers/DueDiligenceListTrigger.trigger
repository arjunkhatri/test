trigger DueDiligenceListTrigger on DDL__c (after insert,after update) {
	
	DueDiligenceListHandler objHandler = new DueDiligenceListHandler();
	
	if(trigger.isAfter){
		if(trigger.isInsert){
			objHandler.onAfterInsert(trigger.newMap.keySet(),trigger.newMap);
		}
		else{
			objHandler.onAfterUpdate(trigger.old, trigger.newMap);
		}
	}
}