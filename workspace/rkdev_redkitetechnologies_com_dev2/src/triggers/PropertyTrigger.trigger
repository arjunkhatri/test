trigger PropertyTrigger on Property__c (before insert,before update,before delete,after update) {

	PropertyHandler objPropertyHandler = new PropertyHandler();
	if(trigger.isAfter){
		if(trigger.isUpdate)
			objPropertyHandler.onAfterUpdate(trigger.newMap, trigger.oldMap);
	}
	else if(trigger.isBefore){
		if(trigger.isInsert){
			objPropertyHandler.onBeforeInsert(trigger.new);
		}else if(trigger.isUpdate){
			objPropertyHandler.onBeforeUpdate(trigger.oldMap,trigger.new);
		}
		else if(trigger.isDelete){
			objPropertyHandler.onBeforeDelete(trigger.old);
		}
	}
}