trigger LoanContactTrigger on Loan_Contact__c (before insert,before update)
{
	LoanContactHandler objLoanContact = new LoanContactHandler();
	objLoanContact.OnBeforeInsertUpdate(Trigger.new);
}