trigger ScheduleReportTrig on Schedule_Report_Details__c (before update) {
	if(trigger.isBefore && trigger.isUpdate){   
		system.debug('>>>>>>session in trig'+userinfo.getSessionId());
		system.debug('>>>>>>trigger.new'+trigger.new);
		system.debug('>>>>>>trigger.old'+trigger.old);
		ScheduleReportTrigHandler.onBeforeUpdate(userinfo.getSessionId(), trigger.new);
	}
}