public without sharing class CalenderController {
	public map<String,List<calenderWrap>> mpDay1 {get;set;}
	public map<String,List<calenderWrap>> mpDay2 {get;set;}
	public map<String,List<calenderWrap>> mpDay3 {get;set;}
	public map<String,List<calenderWrap>> mpDay4 {get;set;}
	public map<String,List<calenderWrap>> mpDay5 {get;set;}
	public map<String,Event> mpKeyToEvents{get;set;}
	public string userSelected{get;set;}
	public string userSelectedId{get;set;}
	public string userSelectedUrl{get;set;}
	public string userSelectedRole{get;set;}
	private String strDateSelected = '';
	public date dToday{get;set;}
	public date dLastDay{get;set;}
	public boolean isBackDisplay{get;set;}
 	public void setstrDateSelected(String n) {
        strDateSelected = n;
    }
    public String getstrDateSelected() {
        return strDateSelected;
    }	
	public CalenderController(){ 
		//get default users id from main page url
		userSelectedId = ApexPages.currentPage().getParameters().get('uId');
		getUsers();
		dToday = System.Today();
		dLastDay = System.Today();
		isBackDisplay = false;
	}
	
	//code to get users to display
 	public List<SelectOption> getUsers() {
 		String loginUserAcc = [Select AccountId from User where id=:userinfo.getUserId()].AccountId;
        List<SelectOption> options = new List<SelectOption>();
        if(loginUserAcc != null){
	        for(AccountTeamMember  uRec:[Select User.FullPhotoUrl, 
	        							UserId,User.Name, TeamMemberRole 
	        							From AccountTeamMember 
	        							where AccountId = :loginUserAcc and (TeamMemberRole = 'Relationship Manager' 
	        							or TeamMemberRole = 'Private Banker')])
	        {
	        	options.add(new SelectOption(String.valueOf(uRec.UserId),String.valueOf(uRec.User.Name)));
	        	if(String.valueOf(uRec.UserId).contains(userSelectedId)){
	        		userSelected = String.valueOf(uRec.User.Name);
	        		userSelectedUrl = uRec.User.FullPhotoUrl;
	        		userSelectedRole = uRec.TeamMemberRole;
	        	}
	       }
        }
       return options;
    }
    
    public pagereference setNewuser(){
    	getUsers();
    	init();
    	return null;
    }
     
    public pagereference calculateWeek(){   
    	dLastDay = dLastDay.addDays(7);
    	dToday = dLastDay;
    	init();
    	isBackDisplay = true;
    	return null;
    }    	
    public pagereference calculatePrevWeek(){   
    	dLastDay = dLastDay.addDays(-7);
    	if(System.Today().daysBetween(dLastDay) == 0)
    		isBackDisplay = false;
    	else
    		isBackDisplay = true;    	
    	dToday = dLastDay;
    	init();
    	return null;
    }    
    
	public void init(){
		mpKeyToEvents = new map<String,Event>();
		mpDay1 = new map<String,List<calenderWrap>>();
		mpDay2 = new map<String,List<calenderWrap>>();
		mpDay3 = new map<String,List<calenderWrap>>();
		mpDay4 = new map<String,List<calenderWrap>>();
		mpDay5 = new map<String,List<calenderWrap>>();
		set<String> intHours = new set<String>();
			intHours.add('9 AM');
			intHours.add('10 AM');
			intHours.add('11 AM');
			intHours.add('12 PM');
			intHours.add('1 PM');
			intHours.add('2 PM');
			intHours.add('3 PM');
			intHours.add('4 PM');
		List<Event> lstEvents = [Select WhoId, WhatId, StartDateTime, OwnerId, Id, EndDateTime 
									From Event where OwnerId=:userSelectedId];
		for(Event eveRec: lstEvents){
			datetime dtm = eveRec.StartDateTime;
			String strevntKey = dtm.format('E')+' '+dtm.month()+'/'+dtm.day()+'/'+dtm.year()+dtm.hour();
			mpKeyToEvents.put(strevntKey,eveRec);
		}		

		Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),0,0,0);		
		Integer mp = 1;
		for(integer j = 0;j <= 6;j++)
		{
			datetime dtm = dt.addDays(j);
			if(dtm.format('E') != 'Sat' && dtm.format('E') != 'Sun' ){	
				for(integer i = 0;i<= 23;i++){ // 4/17/2015 8:36 PM
					datetime dtime = dtm.addHours(i);
					if(dtm.day() == dtime.day()){
						String fDate = dtime.format();
						List<String> strDt = new List<String>();
							strDt = fDate.split(' ');
						String strHr = strDt[1].split(':')[0];	
						String strCheck = strHr+' '+strDt[2];
						if(intHours.contains(strCheck)){
							String strKey = dtime.format('E')+' '+strDt[0].split('/')[0]+'/'+strDt[0].split('/')[1]+'/'+strDt[0].split('/')[2];
							//String strDate = strDt[0].split('/')[2] + '-' + strDt[0].split('/')[0] + '-' + strDt[0].split('/')[1] + ' ' + strHr + ':00:00';	
							String strDate = strDt[0].split('/')[0] + '-' + strDt[0].split('/')[1] + '-' + strDt[0].split('/')[2] + ' ' + strHr + ':00:00';	
							datetime dtNow = System.now();
							integer intDiff =  dtime.hour() - dtNow.hour();
							//check if slot is passeed
							Boolean isSlotPasses;
							if(intDiff > 0){
								isSlotPasses = false;
							}
							else{ 
								isSlotPasses = true;
							}	
							
							Boolean isSlotBooked;
							if(mpKeyToEvents.containsKey(strKey+strHr)){
								isSlotBooked = true;
							}else{
								isSlotBooked = false;
							}	
							if(mp == 1){
								if(!mpDay1.containsKey(strKey)){
									
									mpDay1.put(strKey,new List<calenderWrap>{new calenderWrap(strHr+':00'+strDt[2],strDate,isSlotBooked,isSlotPasses)});
							
								}
								else if (mpDay1.containsKey(strKey)){
									
									mpDay1.get(strKey).add(new calenderWrap(strHr+':00'+strDt[2], strDate,isSlotBooked,isSlotPasses));
								}
							}
							else if(mp == 2){
								if(!mpDay2.containsKey(strKey)){
									
									mpDay2.put(strKey,new List<calenderWrap>{new calenderWrap(strHr+':00'+strDt[2],strDate,isSlotBooked,isSlotPasses)});
							
								}
								else if (mpDay2.containsKey(strKey)){
									
									mpDay2.get(strKey).add(new calenderWrap(strHr+':00'+strDt[2], strDate,isSlotBooked,isSlotPasses));
								}
							}							
							else if(mp == 3){
								if(!mpDay3.containsKey(strKey)){
									
									mpDay3.put(strKey,new List<calenderWrap>{new calenderWrap(strHr+':00'+strDt[2],strDate,isSlotBooked,isSlotPasses)});
							
								}
								else if (mpDay3.containsKey(strKey)){
									
									mpDay3.get(strKey).add(new calenderWrap(strHr+':00'+strDt[2], strDate,isSlotBooked,isSlotPasses));
								}
							}							
							else if(mp == 4){
								if(!mpDay4.containsKey(strKey)){
									
									mpDay4.put(strKey,new List<calenderWrap>{new calenderWrap(strHr+':00'+strDt[2],strDate,isSlotBooked,isSlotPasses)});
							
								}
								else if (mpDay4.containsKey(strKey)){
									
									mpDay4.get(strKey).add(new calenderWrap(strHr+':00'+strDt[2], strDate,isSlotBooked,isSlotPasses));
								}
							}							
							else if(mp == 5){
								
								if(!mpDay5.containsKey(strKey)){
									
									mpDay5.put(strKey,new List<calenderWrap>{new calenderWrap(strHr+':00'+strDt[2],strDate,isSlotBooked,isSlotPasses)});
							
								}
								else if (mpDay5.containsKey(strKey)){
									
									mpDay5.get(strKey).add(new calenderWrap(strHr+':00'+strDt[2], strDate,isSlotBooked,isSlotPasses));
								}
							}							
						}
					}
					
				}
	
				mp++;
			}
				
		}
	}
	
	public pagereference selectDate(){
		return new pagereference ('/BNYMWM/apex/ScheduleAppointment?uId='+userSelectedId+'&sDate='+strDateSelected);
	}
    public pagereference calGoBack(){

       return new pagereference('/BNYMWM/apex/Home');
    }
	public class calenderWrap{
		public string strTime{get;set;}
		public String startdateTime{get;set;}
		public boolean isBooked{get;set;}
		public boolean isPassed{get;set;}
		
		public calenderWrap(String strTime,String startdateTime, boolean isBooked,boolean isPassed){
			this.strTime = strTime;
			this.startdateTime = startdateTime;
			this.isBooked = isBooked;
			this.isPassed = isPassed;
		}
	}
}