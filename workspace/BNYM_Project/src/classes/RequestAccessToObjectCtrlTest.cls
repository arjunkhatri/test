@isTest
private class RequestAccessToObjectCtrlTest {
	static Account testAccount;
	static Contact testContact;
	static Lead testLead;

	static void setup(){

		User currUser  = new User( Id = UserInfo.getUserId(), Email = 'test@test.com');
		update currUser; 

		testAccount = new Account( Name = 'testAcc' );
		insert testAccount;

		testContact = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		insert testContact;

		testLead = new Lead( LastName = 'testLead', Company = 'test' );
		insert testLead;
	}

	@isTest static void testRequestEmail() {
		setup();
		Map<String, RequestAccessEmailTemplates__c> sett = RequestAccessEmailTemplates__c.getAll();
		delete sett.values();
		
		RequestAccessToObjectCtrl.SendRequestEmail( testAccount.Id, false, 'this is test' );
		RequestAccessToObjectCtrl.SendRequestEmail( testAccount.Id, true, 'this is test' );
		RequestAccessToObjectCtrl.SendRequestEmail( testContact.Id, false, 'this is test' );
		RequestAccessToObjectCtrl.SendRequestEmail( testContact.Id, true, 'this is test' );
		RequestAccessToObjectCtrl.SendRequestEmail( testLead.Id, false, 'this is test' );
		RequestAccessToObjectCtrl.SendRequestEmail( testLead.Id, true, 'this is test' );
	}
	
	@isTest static void testgetOwner() {
		setup();
		RequestAccessToObjectCtrl.getOwner( testContact.Id );
		RequestAccessToObjectCtrl.getOwner( testAccount.Id );
		RequestAccessToObjectCtrl.getOwner( testLead.Id );
	}
	
}