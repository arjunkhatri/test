public with sharing class TaskTriggerHandler 
{
  public void onAfterInsert(List<Task> newTaskList)
  {
    List<task> tasksToUpdate = new List<Task> ();
    
    for (Task tsk : newTaskList)
    {
      if (tsk.IsClosed == true )
        tasksToUpdate.add(tsk);
    }
    UpdateObjects(tasksToUpdate);
  }
  public void onAfterUpdate(List<Task> newTasks, Map<Id, Task> oldTaskMap) 
  {
    List<task> tasksToUpdate = new List<Task> ();
    
    for (Task tsk : newTasks)
    {
      if (tsk.IsClosed == true && oldTaskMap.get(tsk.id).isClosed == false)
        tasksToUpdate.add(tsk);
    }
    UpdateObjects(tasksToUpdate);
     }
     
     public void UpdateObjects  (List<Task> newTasks)
     {
       List<task_Updates__c> taskList = [select Task_Field__c, Task_Value__c ,Object_To_Update__c ,Field_To_Update__c ,Child_Object__c,Child_FIeld__c from Task_Updates__c where Active__c = true  ] ;
    
    
    set<string> sObjectType = new set<string>();
    map<string, string> sObjectList = BuildObjectMap ();
    set<string> UpdateAbleTasks = new set<string>();
    //List<csProperties> propList = new List<csProperties>();
    map<string , List<csProperties> > updateMap = new map<string, List<csProperties>>();
    map<string, string> lookupRelationship = new map<string, string> ();
    
    for (Task t : newTasks) 
    {
      string idString = '';
      if (t.whatid != null )
        idString = t.whatid;
      else
        idString = t.whoid;
      csProperties newProp; 
      List<csProperties> tmpPropList = new List<csProperties>();
      
      if (idString != null )
      {
        for (Task_Updates__c tl : taskList)
        {
          
          if (t.get(tl.Task_Field__c) == tl.Task_Value__c )
          {
            string prefix ;
            //if()
            newProp = new csProperties(idString, tl.Object_To_Update__c ,tl.Field_To_Update__c ,tl.Child_Object__c,tl.Child_FIeld__c);
            tmpPropList.add(newProp);
                    
            prefix = String.valueOf(idString).substring(0,3);
            sObjectType.add(sObjectList.get(prefix) );
              
            UpdateAbleTasks.add(t.id);
          }
          if (tmpPropList.size() >0)
            updateMap.put(idString, tmpPropList);
        }
      }
    }
    
      
    if (UpdateAbleTasks.size() == 0 )
      return;
    
      
    map<string, map<string,sObject> > objectList = new map<string, map<string, sObject> >();
    //Query the sObjects 
    set<string> keys = updateMap.keyset();
    for (string sObj : sObjectType )
    {
      List<sObject> recs = database.query('select ' + GetFieldList(sObj) + ' from ' + sObj + ' where id in  :keys');
      
      map<string, sObject> tmpMap = new map<string, sObject>();
      for (sObject o : recs)
        tmpMap.put(o.id, o);
        
      objectList.put(sObj , tmpMap);
      
    }
    
    //Look for Lookup Fields
    
    map<string , List<csProperties> > updateMapLookup = new map<string, List<csProperties>>();
    sObjectType.Clear();
    set<string> RemoveList = new set<string>();
    for (string s : updateMap.keyset() )
    {
      List<csProperties> csList = updateMap.get(s) ;
      map<string, sObject> currentSObjectList ;
      for (csProperties cs : csList)
      {
        
        if (cs.isLookup == true  && sObjectList.get(cs.objId.substring(0,3)) == cs.ChildObject)
        {
          
          currentSObjectList = objectList.get( sObjectList.get(cs.objId.substring(0,3)) ) ;
          //map<string, sObject> tmpMap = new map<string, sObject>();
          sObject sObj = currentSObjectList.get(cs.objId);
          
          
          string idString = String.ValueOf(sobj.get(cs.ChildLookup));
          csProperties newProp = new csProperties (idstring, cs.UpdateObject, cs.UpdateField);
          //newProp.objId = idString;
          
          lookupRelationship.put(cs.ObjId,idString ) ;
          
          if (updateMap.containskey (idstring))
          {
            List<csProperties> csOld = updateMap.get(idString);
            csOld.add(newProp);
            updateMap.put(idString, csOld);
            updateMapLookup.put(idString, csOld);    
          }
          else
          {
            List<csProperties> csNew  = new List<csProperties> ();
            csNew.add(newProp);
            updateMap.put(idString, csNew);
            updateMapLookup.put(idString, csNew);    
          }
                    
          string prefix = String.valueOf(idString).substring(0,3);
          sObjectType.add(sObjectList.get(prefix) );
          
          RemoveList.add(cs.objId) ;
          /*
          if (currentSObjectList.size() == 1 )
            objectList.Remove(cs.ChildObject);
          else
          {  
            currentSObjectList.Remove(cs.objId);
            objectList.put(cs.ChildObject ,currentSObjectList);
            
          }
          */
          sObjectType.add(sObjectList.get(prefix) );
          
          
        }
      }
      
      //for (string s : RemoveList)
      //  ObjectList.Remove(s);
      
    }
    
    for (string s : Removelist)
    {
      string ObjName = sObjectList.get(s.substring(0,3)) ;
      map<string, sObject> removeSObjectList = objectList.get( objName ) ;
      if (removeSObjectList.size() == 1 )
        objectList.Remove(ObjName);
      else
      {  
        removeSObjectList.Remove(s);
        objectList.put(objName ,removeSObjectList);
        
      }
    }
    
    System.Debug('****: ' + updateMapLookup);
    keys = updateMapLookup.keyset();
    for (string sObj : sObjectType )
    {
      List<sObject> recs = database.query('select ' + GetFieldList(sObj) + ' from ' + sObj + ' where id in  :keys');
      
      map<string, sObject> tmpMap = new map<string, sObject>();
      for (sObject o : recs)
        tmpMap.put(o.id, o);
        
      objectList.put(sObj , tmpMap);
      
    }
        
    List<sObject> updateList = new List<sObject>();
    set<string> updateSet = new set<string> ();
    for (Task t : newTasks) 
    {
      string idString = '';
      
      if (t.whatid != null )
        idString = t.whatid;
      else
        idString = t.whoid;
          
      if (UpdateAbleTasks.Contains(t.id) )
      {
        string prefix ='';
        
        List<csProperties> fieldToUpdateList;
        //What Object is the task
        if (lookupRelationship.ContainsKey(idString) )
          idString = lookupRelationship.get(idString);
        prefix = String.valueOf(idString).substring(0,3);
        fieldToUpdateList = updateMap.get(idString) ;
        map<string,sObject> tmpObjMap =  objectList.get(sObjectList.get(prefix)); 
        
        
        if (!updateSet.contains(idString) )
        {
          sObject sObj = tmpObjMap.get(idString);
          for (csProperties fieldToUpdate : fieldToUpdateList )
          {
            
            if (fieldToUpdate.UpdateObject ==  sObjectList.get(prefix))
            {
              
              sObj.put(fieldToUpdate.UpdateField , t.ActivityDate );
              
            }
          }
          updateSet.add(idString);
          updateList.add(sObj);
        }
      }
       } 
       
       if (updateList.size() > 0 )
         update updateList;
         
       
    
    /*
    
    List<account> acctList = database.query('select ' + GetFieldList('Account') + ' from account where id in  :keys');
    map<string,Account> acctMap = new map<string,Account> ( );
    for (Account a : acctList)
      acctMap.put(a.id, a);
    
    List<Account> updateAccts = new List<Account>();
    for (Task t : newTasks) 
    {
      if (acctMap.containskey(t.whatid))
      {
        Account a = acctMap.get(t.whatid);
        string[] fieldToUpdate = updateMap.get(t.whatid).split(':');
        a.put(fieldToUpdate[1] , t.ActivityDate);
        updateAccts.add(a);
      }
       } 
       
       update updateAccts;
       */
       
     }
     private string GetFieldList (string ObjectName)
    {
      string retval = '';
      SObjectType objToken = Schema.getGlobalDescribe().get(ObjectName);
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap(); 
       
        Set<String> fieldSet = fields.keySet();
        for(String s:fieldSet)
        {
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            retval += selectedField.getName() + ',' ;
        }
        
        return retval.substring(0, retval.length()-1);
    }
    
    private map<string, string>  BuildObjectMap ()
    {
      Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
      map<string, string> mapObjectList = new map<string, string> ();
    for(Schema.SObjectType stype : gd.values())
    {
        Schema.DescribeSObjectResult r = stype.getDescribe();
        String prefix = r.getKeyPrefix();
        mapObjectList.put(prefix , String.ValueOf(r.getSObjectType()) );
        
    }
    return mapObjectList ;
    }
    class csProperties 
     {
       public string objId {get;set;}
       public string UpdateObject {get;set;}
       public string UpdateField {get;set;}
       public boolean isLookup {get;set;}
       public string ChildObject {get;set;}
       public string ChildLookUp {get;set;}
       
       public csProperties (string id, string updObject, string updField, string childObj , string childField)
       {
         objId = id;
          UpdateObject = updObject;
          UpdateField = updField;
          if (childObj != null && childField != null)
          {
            isLookup = true;
            ChildObject = childObj;
            ChildLookUp = childField;
          }
          else
            isLookup = false;
       }
       public csProperties (string id, string updObject, string updField)
       {
         objId = id;
         UpdateObject = updObject;
         UpdateField = updField;
         isLookup = false;
       }
     }
}