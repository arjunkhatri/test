@isTest
private class ConnectionTrigHandTest {
	
	static Account testAccount;
	static Account testAccount2;
	static Contact testContact;
	static Contact testContact2;
	static List<Connections__c> connections;

	static void setup(){
		testAccount = new Account ( Name = 'testAccount');
		testAccount2 = new Account ( Name = 'testAccount');
		insert new List<Account> {testAccount, testAccount2};

		testContact = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		testContact2 = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		insert new List<Contact>{ testContact, testContact2 };

		Inverse_Connection_Role__c connRole  = new Inverse_Connection_Role__c();
		connRole.Name = 'ccRole1';
		connRole.Role__c = 'Parent';
		connRole.Inverse_Role__c = 'Child';
		connRole.Connection_Record_Type__c = 'C:C';
		insert connRole;

		Inverse_Connection_Role__c connRole2  = new Inverse_Connection_Role__c();
		connRole2.Name = 'ccRole2';
		connRole2.Role__c = 'Client';
		connRole2.Inverse_Role__c = 'Advisor';
		connRole2.Connection_Record_Type__c = 'R:R';
		insert connRole2; 

		Id rrRecId = [select id, Name from RecordType where SobjectType = 'Connections__c' AND Name = 'R:R' ].Id;
		Id ccRecId = [select id, Name from RecordType where SobjectType = 'Connections__c' AND Name = 'C:C' ].Id;

		connections = new List<Connections__c>();
		Connections__c conn = new Connections__c();
		conn.Role_in_Relationship__c = 'Client';
		conn.Relationship_R1__c = testAccount.Id;
		conn.Relationship_R2__c = testAccount2.Id;
		conn.RecordTypeId = rrRecId;
		connections.add( conn );

		conn = new Connections__c();
		conn.Role_in_Relationship__c = 'Parent';
		conn.Contact_C1__c = testContact.Id;
		conn.Contact_C2__c = testContact2.Id;
		conn.RecordTypeId = ccRecId;
		connections.add( conn );
	}

	@isTest static void testInsert() {
		setup();

		Test.startTest();
			insert connections;
		Test.stopTest();
	}

	@isTest static void testUpdate() {
		setup();
		insert connections;

		Test.startTest();
			ConnectionTrigHandHelper.setAlreadyCreatedInverseStopped();
			update connections;
		Test.stopTest();
	}

	@isTest static void testDelete() {
		setup();
		insert connections;

		Test.startTest();
			ConnectionTrigHandHelper.setAlreadyCreatedInverseStopped();
			delete connections;
		Test.stopTest();
	}
}