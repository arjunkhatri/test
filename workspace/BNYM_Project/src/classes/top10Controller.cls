public class top10Controller {

    private final Account account;
    String uid = UserInfo.getUserId();
    
    List<Account> getAccount;
    
    public List<Account> getAccount() { 
        if(getAccount ==null) {
            getAccount = [SELECT Name, AnnualRevenue FROM account 
            WHERE Account.Type = 'Client' 
            ORDER BY AnnualRevenue DESC];
        }
        return getAccount;
    }
}