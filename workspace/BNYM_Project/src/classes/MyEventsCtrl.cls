/*
 *   Descprition : Controller to fetch next 3 Campaigns that the Running User is a 
 *                 Campaign Member of.
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            27/04/15          Initial Draft
 */
public without sharing class MyEventsCtrl {
	public List < Campwrap > lstwrapCamp {get;set;}
	public String strUserCont { get; set; }
    //Constructor Start
    public MyEventsCtrl(){
        User uRec = [Select ContactId,AccountId,SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
        if(uRec != null && uRec.ContactId != null)
            strUserCont = uRec.ContactId;
       getlstcamp();
    }//Contructor end
    //Method to bring the list of Campaign 
    public void getlstcamp() {
        lstwrapCamp = new List < Campwrap > ();
            if(String.isNotBlank(strUserCont)){
                List <CampaignMember> lstCampaign  = [Select
                                                             Id, ContactId, Status, Campaign.Name,
                                                             Campaign.Id, Campaign.IsActive, 
                                                             Campaign.StartDate, CampaignId 
                                                      From
                                                             CampaignMember 
                                                      Where
                                                             ContactId = :strUserCont and
                                                             Campaign.IsActive = true
                                                      ORDER BY 
                                                             Campaign.StartDate LIMIT 3]; 
	             for (CampaignMember objCamp: lstCampaign) { 
		             Date sDate = objCamp.Campaign.StartDate;
		             String strDate = sDate.month()+'/'+sDate.day()+'/'+sDate.year();
		             lstwrapCamp.add(new Campwrap(objCamp.Campaign.id,objCamp.Campaign.Name,strDate,objCamp.Status));
	             }
             }
    }
    //Subclass : Wrapper Class for Campaign
    public class Campwrap {
    //Static Variables
    public string id {get; set;} 
    public string Name {get; set;}
    public String StartDate {get; set;}
    public String strStatus {get; set;}
        //Wrapper Class Controller
        Campwrap(String id,String Name,String sDate,String sStatus) {
            this.id = id;
            this.Name = Name;
            this.StartDate = sDate;
            this.strStatus = sStatus;
        }
    }
}