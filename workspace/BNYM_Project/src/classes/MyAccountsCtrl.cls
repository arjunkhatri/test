/*
 *   Descprition : Controller to calculate Investment and Banking product types Percentage with 
 *                 Total Current value of Financial_Account__c and Assets_and_Liabilities__c.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            29/04/15          Initial Draft
 */
public without sharing class MyAccountsCtrl { 
    public String strUserCont { get; set; }
    public String strUserAcct { get; set; }
    public Decimal percentInv {get;set;}
    public Decimal percentBanking {get;set;} 
    public Integer totalCurrentValue {get;set;}
    public Decimal totalInvsCurrValue {get;set;}
    public Decimal totalBankCurrValue {get;set;}
    public Integer totalassetCurrentValueSum {get;set;}
    public Integer netWorthFinancial {get;set;}
    public Integer netHouseholdWorth {get;set;}
    public Integer netWorthFinancialAcct {get;set;}
    public Integer netWorth {get;set;}
    public List<AggregateResult> lstaFinancial   = new List<AggregateResult>();
    public List<AggregateResult> lstAssetLiblity = new List<AggregateResult>();
    public String strChartUrl{get;set;}
    private static final String STR_INVESTMENT = 'Investment';
    private static final String STR_BANKING = 'Banking';
    private static final String STR_LOAN = 'Loan';
    private static final String STR_MORTAGE = 'Mortgage';
    private static final String STR_ASSET = 'Asset';
    private static final String STR_LIABILITY = 'Liability';
    private static final String STR_PRO_TYPE = 'Product_Type__c';
    private static final String STR_ACC_TYPE = 'Account_Type__c';
    private static final String STR_TYPE = 'Type__c';
    //Constructor Start
    public MyAccountsCtrl(){
        percentInv = 0.00;
        percentBanking = 0.00;
        totalCurrentValue = 0;
        totalInvsCurrValue = 0;
        totalBankCurrValue = 0;
        totalassetCurrentValueSum = 0;
        netWorthFinancial = 0;
        netHouseholdWorth = 0;
        netWorthFinancialAcct = 0;
        netWorth = 0;
        User uRec = [Select ContactId,AccountId,SmallPhotoUrl From User where id = :userinfo.getUserId() limit 1];
	        if(uRec != null && uRec.ContactId != null)
	            strUserCont = uRec.ContactId;
	        if(uRec != null && uRec.AccountId != null)
	            strUserAcct = uRec.AccountId;
        lstaFinancial = [Select
                               Product_Type__c, Account_Type__c, Name, Id , Sum(Current_Value__c)
                         From
                               Financial_Account__c
                         where
                               Relationship_Entity__c =: strUserAcct
                         GROUP BY 
                               Product_Type__c ,Id, Account_Type__c,Name];
       lstAssetLiblity = [Select
                                Id,Type__c ,Sum(Current_Value__c)
                          From
                                Assets_and_Liabilities__c
                          where
                                Account__c =: strUserAcct
                          GROUP BY
                                Id,Type__c ];
        getFinancialAccounts();
        getNetWorth();
    }//Contructor end
    //Method for calculate percentage of Investment and Banking product types for chart section
    public void getFinancialAccounts(){
	    Decimal investmentCount = 0;
	    Decimal bankingCount = 0;
	    Integer currentValueSum = 0;
	    Integer investSum = 0;
	    Integer bankSum = 0;
        if(String.isNotBlank(strUserAcct)){
            for(AggregateResult agrR : lstaFinancial){
                if(agrR != null ){
                    if(agrR.get('expr0') != null && agrR.get(STR_PRO_TYPE) !=null && agrR.get(STR_PRO_TYPE) != ''){
                        currentValueSum  = currentValueSum + Integer.valueOf(agrR.get('expr0'));
                        if(agrR.get(STR_PRO_TYPE).equals(STR_INVESTMENT)){
                            investmentCount++;
                            investSum = investSum + Integer.valueOf(agrR.get('expr0'));
                        }
                        else if(agrR.get(STR_PRO_TYPE).equals(STR_BANKING)){
                            bankingCount++;
                            bankSum = bankSum + Integer.valueOf(agrR.get('expr0'));
                        }
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size()>0){
                 totalCurrentValue = currentValueSum;
                 totalInvsCurrValue = investSum;
                 totalBankCurrValue = bankSum;
                 percentInv = ((totalInvsCurrValue / totalCurrentValue )*100).setScale(2);
                 percentBanking = ((totalBankCurrValue / totalCurrentValue)*100).setScale(2);
            }
        }
      String strInv = string.valueof(percentInv);
      String strBank = string.valueof(percentBanking);
      strChartUrl = '/BNYMWM/apex/MyAccountsChart?inv='+strInv+'&bank='+strBank;
   }
   //Method for calculate net worth for net worth section
   public void getNetWorth(){
        Integer loanSum = 0;
        Integer mortageSum = 0;
        Integer assetSum = 0;
        Integer liabilitySum = 0;
        Integer assetAccConSum = 0;
        Integer liabilityAccConSum = 0;
        Integer loanNetSum = 0;
        Integer mortageNetSum = 0;
        Integer assetCurrentValueSum = 0;
        if(String.isNotBlank(strUserAcct) && String.isNotBlank(strUserCont)){
            for(AggregateResult agrRFin : lstaFinancial){
                if(agrRFin != null ){
                    if(agrRFin.get('expr0') != null && agrRFin.get(STR_ACC_TYPE) !=null && agrRFin.get(STR_ACC_TYPE) != ''){
                        if(agrRFin.get(STR_ACC_TYPE).equals(STR_LOAN)){
                            loanSum = loanSum + Integer.valueOf(agrRFin.get('expr0'));
                        }
                        if(agrRFin.get(STR_ACC_TYPE).equals(STR_MORTAGE)){
                            mortageSum = mortageSum + Integer.valueOf(agrRFin.get('expr0'));
                        }
                    }
                }
            }
            for(AggregateResult assetAgrR : lstAssetLiblity){
                if(assetAgrR != null ){
                    if(assetAgrR.get(STR_TYPE) !=null && assetAgrR.get(STR_TYPE) != '' && assetAgrR.get('expr0') != null){
                        if(assetAgrR.get(STR_TYPE).equals(STR_ASSET)){
                            assetAccConSum = assetAccConSum + Integer.valueOf(assetAgrR.get('expr0'));
                        } 
                        if(assetAgrR.get(STR_TYPE).equals(STR_LIABILITY)){
                            liabilityAccConSum = liabilityAccConSum + Integer.valueOf(assetAgrR.get('expr0'));
                        }
                    }
                }
            }
            if(lstaFinancial != null && lstaFinancial.size()>0 && lstAssetLiblity != null && lstAssetLiblity.size() > 0){
                netWorthFinancial = totalCurrentValue - (loanSum + mortageSum);
                netHouseholdWorth = (netWorthFinancial + assetAccConSum ) - liabilityAccConSum;
            }
            List<AggregateResult> lstFinancialAcct = [Select SUM(Financial_Account__r.Current_Value__c), 
                                                        Financial_Account__r.Account_Type__c, 
                                                        Financial_Account__r.Id, 
                                                        Contact__c 
                                                    From 
                                                        Financial_Account_Connection__c 
                                                    where 
                                                        Contact__c =: strUserCont
                                                    GROUP BY 
                                                          Financial_Account__r.Account_Type__c, 
                                                          Financial_Account__r.Id,Contact__c];
            for(AggregateResult agrR1 : lstFinancialAcct){
	            if(agrR1 != null ){
	                if(agrR1.get('expr0') != null && agrR1.get(STR_ACC_TYPE) !=null && agrR1.get(STR_ACC_TYPE) != ''){
	                    assetCurrentValueSum  = assetCurrentValueSum + Integer.valueOf(agrR1.get('expr0'));
	                    if(agrR1.get(STR_ACC_TYPE).equals(STR_LOAN)){
	                        loanNetSum = loanNetSum + Integer.valueOf(agrR1.get('expr0'));
	                    }
	                    else if(agrR1.get(STR_ACC_TYPE).equals(STR_MORTAGE)){
	                        mortageNetSum = mortageNetSum + Integer.valueOf(agrR1.get('expr0'));
	                    }
	                }
	            }
            }
            List<AggregateResult> lstAssetAcctCont = [Select 
                                                        Assets_and_Liabilities__r.Account__c,
                                                        Contact__c, Assets_and_Liabilities__r.Type__c, 
                                                        SUM(Assets_and_Liabilities__r.Current_Value__c),
                                                        Assets_and_Liabilities__r.Id
                                                  From
                                                        Contact_Assets_and_Liabilities__c
                                                  where
                                                        Contact__c =: strUserCont
                                                  GROUP BY
                                                        Assets_and_Liabilities__r.Type__c,
                                                        Assets_and_Liabilities__r.Id,
                                                        Contact__c,
                                                        Assets_and_Liabilities__r.Account__c];
            for(AggregateResult objAssAcc : lstAssetAcctCont){
                if(objAssAcc != null ){
                    if(objAssAcc.get(STR_TYPE) !=null && objAssAcc.get(STR_TYPE) != '' && objAssAcc.get('expr0') != null){
                        if(objAssAcc.get(STR_TYPE).equals(STR_ASSET)){
                            assetSum = assetSum + Integer.valueOf(objAssAcc.get('expr0'));
                        }
                        else if(objAssAcc.get(STR_TYPE).equals(STR_LIABILITY)){
                            liabilitySum = liabilitySum + Integer.valueOf(objAssAcc.get('expr0'));
                        }
                    }
                }
            }
            if(lstFinancialAcct != null && lstFinancialAcct.size()>0 && lstAssetAcctCont != null && lstAssetAcctCont.size() > 0){
                totalassetCurrentValueSum = assetCurrentValueSum ;
                netWorthFinancialAcct = totalassetCurrentValueSum - (loanNetSum + mortageNetSum);
                netWorth = (netWorthFinancialAcct + assetSum ) - liabilitySum;
            }
        }
    }
}