/*
 *   Descprition : Controller to identify that on which enviornment vf page is working on
 *                 and redirect user accordingly.
 *
 *   Revision History:
 *
 *   Version          Date            Description
 *   1.0            13/04/15          Initial Draft
 */
public with sharing class RedirectController {
    private static final String STR_DESKTOP = 'desktop';
    private static final String STR_TABLET = 'Tablet';
    private static final String STR_PHONE = 'Phone';
	public pagereference init(){
		PageReference pageRef; 
			if(checkPlatform() == STR_DESKTOP){
				pageRef = new PageReference('/BNYMWM/apex/Community_Customer_Home');
				return pageRef;
			}
			else{
				pageRef = new PageReference('/BNYMWM/apex/Home');
				return pageRef;
			}
		return null;
	}
     //for checking on which platform page is working
    public String checkPlatform(){
        String retVal = '';
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_OPERAMINI_PHONE_UA');
        //& some devices use custom headers for the user-agent.
        if( String.isBlank( userAgent ) )
            userAgent = ApexPages.currentPage().getHeaders().get('HTTP_X_SKYFIRE_PHONE');
        //& replace with custom setting - using (?i) case insensitive mode.
        String deviceReg = '(?i)(iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce)';//iphone|ipod|ipad|blackberry|mobile|android|palm|windows\\s+ce
        String ipadReg = '(?i)(ipad)';
        String iphoneReg = '(?i)(iphone)';
        String mobileReg = '(?i)(mobile)';
        String androidReg = '(?i)(android)';
        String desktopReg = '(?i)(windows|linux|os\\s+[x9]|solaris|bsd)';
        String botReg = '(?i)(spider|crawl|slurp|bot)';
        Matcher ipadM = Pattern.compile( ipadReg ).matcher( userAgent );
        Matcher iphoneM = Pattern.compile( iphoneReg ).matcher( userAgent );
        Matcher mobileM = Pattern.compile( mobileReg ).matcher( userAgent );
        Matcher androidM = Pattern.compile( androidReg ).matcher( userAgent );
        Matcher m = Pattern.compile(deviceReg).matcher(userAgent);
        if( m.find(0) )
        {
            if( ipadM.find(0) || ( !mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = STR_TABLET;
                system.debug('This is for tablet');
            }
            else if( iphoneM.find(0) || ( mobileM.find(0) && androidM.find(0) ) )
            {
                retVal = STR_PHONE;
                system.debug('This is for Phone');
            }
        }
        else
        {
            m = Pattern.compile(desktopReg).matcher(userAgent);
            if( m.find(0) )
                retVal = STR_DESKTOP;
            m = Pattern.compile(botReg).matcher(userAgent);
            if( m.find(0) )
                retVal = STR_DESKTOP;
                system.debug('This is for desktop');
        }
        return retVal;
    }
}