@isTest
private class ConnectionMgmt_ContactConnectionsTest {
	
	static Account testAccount;
	static Contact testContact;
	static Contact testContact2;

	static void setup(){
		testAccount = new Account ( Name = 'testAccount');
		insert testAccount;

		testContact = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		testContact2 = new Contact( LastName = 'testContact', AccountId = testAccount.Id );
		insert new List<Contact>{ testContact, testContact2 };

		AddressConnectionMap__c conn  = new AddressConnectionMap__c();
		conn.Name = 'testConn';
		conn.Sort_Order__c = 1;
		conn.FieldName__c = 'Contact_C2__r.MailingCity';
		conn.Type__c = 'ContactConnection';
		insert conn; 

		PageReference pageRef = Page.ConnectionMgmt_ContactConnections;
		pageRef.getParameters().put( 'Id', testContact.Id );

		Test.setCurrentPageReference( pageRef );
	}

	@isTest static void testInit() {
		setup();
		ConnectionMgmt_ContactConnections ctrl = new ConnectionMgmt_ContactConnections();
		ctrl.Init();
		ctrl.ActionType = 'Contact';
		ctrl.getPanelVisibility();
		ctrl.ResetConnectionObj();
		ctrl.SaveRoleChange();
		ctrl.FilterList();
		ctrl.SaveAndNew();
		ctrl.ConnectionObj.Contact_C2__c = testContact2.Id;
		ctrl.ConnectionObj.Role_in_Relationship__c = 'testRole';
		ctrl.SaveAndNew();
		ctrl.SaveAndClose();
	}
}