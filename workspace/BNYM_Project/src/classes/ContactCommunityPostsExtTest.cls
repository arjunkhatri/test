@IsTest(SeeAllData=true) public class ContactCommunityPostsExtTest{
    public static Account testAccount;
    public static Contact testContact;
    public static Profile testProfile;
    public static User testUser;
    
    
    
    @IsTest(SeeAllData=true) static void TestPostStats() {
        // 1. Make sure at least one published community is defined
        List<Network> allCommunities = [SELECT Id,Name,Status FROM Network WHERE Status = 'Live'];
        System.assert(allCommunities != null, 'Failed to find one or more active, published communities in this org.');
        System.assert((allCommunities.size() > 0), 'Failed to find one or more active, published communities in this org.');
        
        Network community = allCommunities.get(0);
        System.assert(community != null, 'Failed to retrieve active, published community.');
        
        // find all members of the queried community
        List<NetworkMember> members = [SELECT Id,MemberId,NetworkId FROM NetworkMember WHERE NetworkId = :community.Id];
        System.assert(members != null, 'Failed to retrieve membership for the selected community: (' + community.Id + ').');
        System.assert(members.size() > 0, 'Failed to retrieve membership for the selected community: (' + community.Id + ').');
        
        // find all users associated with a contact record who aree members of the community.
        List<User> portalUsers = [SELECT ContactId,Id,IsActive,IsPortalEnabled,Name,Username,UserType FROM User WHERE ContactId != null and IsActive = true and Id IN(SELECT MemberId FROM NetworkMember WHERE NetworkId = :community.Id)];
        System.assert(portalUsers != null, 'Failed to find any portal-licensed or community-licensed members of community (' + community.Id + ').');
        System.assert(portalUsers.size() > 0, 'Failed to find any portal-licensed or community-licensed members of community (' + community.Id + ').');
        
        // grab the first active community member we can find
        User communityUser = portalUsers.get(0);
        System.assert(communityUser != null, 'Failed to retrieve a specific community user.');
        
        List<Contact> communityContacts = [SELECT Id, Name from Contact where Id = :communityUser.ContactId];
        System.assert((communityContacts != null && communityContacts.size() > 0), 'Failed to retrieve a contact record for the designated community user(' + communityUser.Id + ').');
        
        // find the member's contact record to drive the tests
        Contact communityContact = communityContacts.get(0);
        System.assert(communityContact != null, 'Failed to get test contact for the community user.');
        
        // create a group and add members
        CollaborationGroup testGroup = new CollaborationGroup();
        testGroup.NetworkId = community.Id;
        testGroup.Name = 'CCT Test Group';
        testGroup.CollaborationType = 'Public';
        insert testGroup;
        
        CollaborationGroupMember membership = new CollaborationGroupMember();
        membership.CollaborationGroupId = testGroup.Id;
        membership.MemberId = communityUser.Id;
        insert membership;
        
        // insert some community posts
        FeedItem testItem1 = new FeedItem();
        testItem1.NetworkScope = community.Id;
        testItem1.Body = 'Lorem ipsum';
        testItem1.ParentId = testGroup.Id;
        
        insert testItem1;
        
        // set up the controller extension
        // per http://salesforcesource.blogspot.com/2008/09/testing-your-controller-extentions.html
        PageReference pageRef = ApexPages.currentPage(); // Is this right?
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController sc = new ApexPages.standardController(communityContact);
        System.assert(sc != null, 'Failed to create a standard controller for the test contact record.');
        ContactCommunityPostsExt controller = new ContactCommunityPostsExt(sc);
        
        // test getCommunityUser(Id contactId)
        User commUserTest = controller.getCommunityUser(null);
        System.assert(commUserTest == null, 'Passing null user to getCommunityUser() did not return null.');
        
        commUserTest = controller.getCommunityUser(communityContact.Id);
        System.assert(commUserTest != null, 'Failed to retrieve user from getCommunityUser(' + communityContact.Id + ').');
        
        // test buildContact()
        Contact testContact1 = controller.buildContact(null);
        System.assert(testContact1 == null, 'Attempt to buildContact from null resulted in a contact.');
        
        testContact1 = controller.buildContact(communityContact);
        System.assert(testContact1 != null, 'Failed to build community contact from a contact id failed.');
        
        // test getAllActivity(User usr)
        List<ChatterActivity> testActivity = controller.getAllActivity(null);
        System.assert(testActivity == null, 'Attempt to create activity list from null resulted in a return value and not null.');
        
        // test getAllActivity()
        testActivity = controller.getAllActivity();
        System.assert(testActivity != null, 'Failed to retrieve any chatter activity list for the designated user.');
        System.assert(testActivity.size() > 0, 'Failed to retrieve any chatter activity for the designated user.');
        controller.getAllActivity();
        
        // test getRecentCommunityPosts(User usr)
        List<FeedItem> recentPosts = controller.getRecentCommunityPosts(null);
        System.assert(recentPosts == null, 'Unexpected value from attempt to retrieve community posts from null user.');
        
        recentPosts = controller.getRecentCommunityPosts(communityUser);
        System.assert(recentPosts != null, 'Null returned for recent posts!');
        System.assert(recentPosts.size() > 0, 'No recent post results returned.');
        
        // test getTotalPostCount(User usr)
        Integer postCount = controller.getTotalPostCount(null);
        System.assert(postCount == 0, 'Non-zero post count from null user.');
        
        postCount = controller.getTotalPostCount(communityUser);
        System.assert(postCount > 0, 'No posts returned from a real user...');
        
        // test getTotalCommentCount(User usr)
        Integer commentCount = controller.getTotalCommentCount(null);
        System.assert(commentCount == 0, 'Non-zero comment count from null user.');
        
        commentCount = controller.getTotalCommentCount(communityUser);
        System.assert(commentCount > -1, 'Negative comment count from null user.');
        
        // test getTopicUsage(User usr)
        List<ContactCommunityPostsExt.TopicStats> topicStats = controller.getTopicUsage(null);
        System.assert(topicStats == null, 'Unexpectedly returned non-null topic stats for null user.');
        
        // test getTopicStats()
        topicStats = controller.getTopicStats();
        System.assert(topicStats != null, 'Failed to retrieve topic stats for user (was null)');
        System.assert(topicStats.size() > -1, 'Unexpected size for TopicStats');
        controller.getTopicStats();
        
        // test getTotalLoginCount()
        Integer loginCount = controller.getTotalLoginCount();
        System.assert(loginCount > -1, 'Failed to retrieve a good login count...' + loginCount);
        
        // test getLoginHistory()
        List<LoginHistory> loginHistory = controller.getLoginHistory();
        System.assert(loginHistory != null);
        
        controller.getLoginHistory();
        
        // test getGroupMemberships(User usr)
        List<CollaborationGroupMember> groupMemberships = controller.getGroupMemberships(null);
        System.assert(groupMemberships == null, 'Unexpected non-null group memberships...');
        
        // test getGroups()
        groupMemberships = controller.getGroups();
        System.assert(groupMemberships != null, 'Failed to retrieve group memberships...');
        
        controller.getGroups();
        
        // test getPeopleFollowing(User usr)
        List<EntitySubscription> following = controller.getPeopleFollowing(null);
        System.assert(following == null, 'Returned unexpected following results for null...');
        
        // test getFollowingPeople()
        following = controller.getFollowingPeople();
        System.assert(following != null, 'Failed to retrieve follow list.');
        controller.getFollowingPeople();
        
        // test getIdeas()
        controller.getIdeas();
        
    }
}