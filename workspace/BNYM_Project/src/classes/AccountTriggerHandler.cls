public with sharing class AccountTriggerHandler 
{
  public void onBeforeDelete(List<Account> oldAccounts, Map<Id, Account> oldAccountMap) 
  {
    handleBeforeDelete (oldAccounts);
     } 
  private void handleBeforeDelete (List<Account> oldAccounts)
  {
    integer ErrorCount = 0;
    List<No_Delete_RecordTypes__c> rtTypes = [select Record_Type_Name__c , Exclude_Profiles__c from  No_Delete_RecordTypes__c where Object_Type__c = 'Account'];
    
    map<string, string > rtNameSet = new map<string, string >();
    
    for (No_Delete_RecordTypes__c rt : rtTypes)
      rtNameSet.put(rt.Record_Type_Name__c , rt.Exclude_Profiles__c);
      
    
    
    map<string, RecordType> rtMap = new map<string, RecordType>([select id , Name from RecordType where sObjectType = 'Account' and Name in : rtNameSet.keyset()]);
      
    for (Account a : oldAccounts) 
    {
      if (rtMap.ContainsKey(a.RecordTypeID) )
      {
        set<string> ProfileList = new set<string>();
      
        if (rtNameSet.get(rtMap.get(a.RecordTypeID).Name) != null )
          ProfileList = new set<string> (rtNameSet.get(rtMap.get(a.RecordTypeID).Name).Split(','));
        
        if ( !ProfileList.Contains([Select Name from Profile where ID = :UserInfo.getProfileId()].Name)  )
        {
          string errormsg = 'This type of Account can not be deleted. ' ;
          trigger.old[Errorcount].addError(errormsg);
          ErrorCount++;
        }
      }
    }  
  }
}