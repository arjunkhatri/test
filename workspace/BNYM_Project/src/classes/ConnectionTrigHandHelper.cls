global class ConnectionTrigHandHelper 
{
  // Static variables are local to the context of a Web request  
    // (or testMethod during a runTests call)  
    // Therefore, this variable will be initialized as false  
    // at the beginning of each Web request which accesses it.  

    private static boolean alreadyCreatedInverse = false;


    public static boolean hasAlreadyCreatedInverse() {
        return alreadyCreatedInverse;
    }

    // By setting the variable to true, it maintains this  
    // new value throughout the duration of the request  
    // (or testMethod)  
    
    public static void setAlreadyCreatedInverse() {
        alreadyCreatedInverse = true;
    }
    
     public static void setAlreadyCreatedInverseStopped() {
        alreadyCreatedInverse = false;
    }

}