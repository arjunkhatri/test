public with sharing class ContactCommunityPostsExt {
    public Contact myContact {get; set;}
    public User communityUser {get; set;}
    public boolean hasCommunityUser {get; private set;}
    
    // tab 1: Stats
    public List<ChatterActivity> allActivity;
    public DateTime lastLogin {get; set;}
    public Integer postCount {get; set;}
    public Integer commentCount {get; set;}
    
    // tab 2: posts
    public List<FeedItem> userPosts {get; set;}
    
    // tab 3: Topics
    public List<TopicStats> topicStats;

    // tab 4: groups
    public List<CollaborationGroupMember> groups;
    
    // tab 5: Following
    public List<EntitySubscription> followingPeople;
    
    // tab 6: Logins
    public Integer totalLoginCount;
    public List<LoginHistory> loginHistory;
 
    // tab 7: Ideas
    public List<Idea> ideas;
    
    public ContactCommunityPostsExt(ApexPages.StandardController controller) {
        this.myContact = buildContact((Contact)controller.getRecord());
        this.communityUser = getCommunityUser(myContact.Id);
        this.hasCommunityUser = false;
        this.userPosts = getRecentCommunityPosts(this.communityUser);
        if (this.communityUser != null) {
            this.hasCommunityUser = true;
            this.lastLogin = this.communityUser.LastLoginDate;
            this.postCount = getTotalPostCount(this.communityUser);
            this.commentCount = getTotalCommentCount(this.communityUser);
            // this.allActivity = getAllActivity(this.communityUser);
            // this.topicStats = getTopicUsage(this.communityUser);
            this.ideas = getIdeas(this.communityUser);
        }
    }
    
    public Contact buildContact(Contact contact) {
        if (contact == null) {
            return null;
        }
        
        Contact retVal = null;
        try {
            retVal = [
                SELECT 
                	Id,
                	AccountId,
                	Name 
                FROM 
                	Contact 
                WHERE 
                	Id = :contact.Id LIMIT 1];
        } catch (Exception e) {
            System.debug(e);
        }
        
        return retVal;
    }
    
    public User getCommunityUser(Id contactId) {
        if (contactId == null) {
            return null;
        }
        
        User commUser = null;
        try {
            commUser = [
                SELECT 
                	Id,
                	IsActive,
                    IsPortalEnabled,
                    UserType,
                    Username,
                    LastLoginDate 
                FROM 
                	User 
                WHERE 
                	ContactId = :contactId LIMIT 1];
        } catch (Exception e) {
            System.debug(e);
        }
        
        return commUser;
    }

    public List<ChatterActivity> getAllActivity() {
        if (this.allActivity != null) {
            return this.allActivity;
        }
        
        this.allActivity = getAllActivity(this.communityUser);
        return this.allActivity;
    }
    
    public List<ChatterActivity> getAllActivity(User usr) {
       	if (usr == null) {
            System.debug('No community user specified.');
            return null;
       	}
        
       	List<ChatterActivity> activity = null;
        try {
            activity = [
                SELECT 
                	c.Id,
                	c.NetworkId,
                	c.CommentCount,
                	c.CommentReceivedCount,
                	c.InfluenceRawRank,
                	c.LikeReceivedCount,
                	c.ParentId,
                	c.PostCount 
                FROM 
                	ChatterActivity c 
                WHERE 
                	c.ParentId = :usr.Id
                ];
        } catch (Exception e) {
            System.debug(e);
        }
        
        return activity;
    }
    
    public List<FeedItem> getRecentCommunityPosts(User usr) {
        if (usr == null) {
            System.debug('No community user specified.');
            return null;
        }
        
        List<FeedItem> posts = null;
        System.debug('Querying posts for community user [{' + usr.Id + '}]');
        try {
            posts = [SELECT 
                        Body,
                        CommentCount,
                        CreatedDate,
                        Id,
                        NetworkScope,
                     	LikeCount,
                        ParentId,
                        Title,
                        Type,
                        Visibility 
                    FROM FeedItem 
                    WHERE 
                        CreatedById = :usr.Id
                    ORDER BY 
                     	CreatedDate DESC NULLS FIRST 
                    LIMIT 20];
        } catch (Exception e) {
            system.debug(e);
        }
        
        return posts;
        
    }
    
    public Integer getTotalPostCount(User usr) {
       if (usr == null) {
            System.debug('No community user specified.');
            return 0;
       }
        
        Integer retVal = 0;
        System.debug('Querying post counts for community user [{' + usr.Id + '}]');
        try {
            retVal = [
            SELECT 
                count() 
            FROM 
                FeedItem f
            WHERE 
                f.CreatedById = :usr.Id];
 
        } catch (Exception e) {
            system.debug(e);
        }
        
        return retVal;
    }
    
    public Integer getTotalCommentCount(User usr) {
       if (usr == null) {
            System.debug('No community user specified.');
            return 0;
       }
        
        Integer retVal = 0;
        System.debug('Querying comment counts for community user [{' + usr.Id + '}]');
        try {
            retVal = [
            SELECT 
                count() 
            FROM 
                FeedComment f
            WHERE 
                f.CreatedById = :usr.Id];
 
        } catch (Exception e) {
            system.debug(e);
        }
        
        return retVal;
    }
    
    public List<TopicStats> getTopicStats() {
        if (this.topicStats != null) {
            System.debug('Topic stats already initialized with length ' + this.topicStats.size());
            return this.topicStats;
        }
        
        System.debug('Initializing topic stats from community user...');
        this.topicStats = getTopicUsage(this.communityUser);
        return this.topicStats;
    }
    
    public List<TopicStats> getTopicUsage(User usr) {
        if (usr == null) {
            System.debug('No community user specified.');
            return null;
        }
        
        System.debug('Creating stats list...');
        List<TopicStats> stats = new List<TopicStats>();
        List<AggregateResult> results = null;
        try {
            System.debug('Querying stat list');
            results = [
                SELECT 
                	TopicId
                FROM 
                	TopicAssignment 
                WHERE 
                	CreatedById = :usr.Id 
                GROUP BY 
                	TopicId];
            System.debug('Queried stat list. Aggregating results.');
            for (AggregateResult res : results) {
          		System.debug('Aggregating result...');
                TopicStats st = new TopicStats(res);
                stats.add(st);
                System.debug('Added ' + st.TopicName + ' to the list.');
            }
        } catch (Exception e) {
            System.debug('IT DONE WENT BOOM!.');
            System.debug(e);
        }
        
        System.debug('Returning topic stats.');
        return stats;
    }
    
	public class TopicStats {
		public String TopicId { get; private set; }
        public String TopicName { get; private set; }
        public Topic topic {get; private set; }
        
        public TopicStats(AggregateResult res) {
            if (res == null) {
                return;
            }

            this.TopicId = (String)res.get('TopicId');
            try {
                System.debug('Querying topic ' + this.topicId);
            	this.topic = [SELECT Id,Name,Description,NetworkId,TalkingAbout FROM Topic WHERE Id = :this.topicId LIMIT 1];
                this.TopicName = this.topic.Name;
            } catch (Exception e) {
                System.debug(e);
            }
        }
    }
    
    public Integer getTotalLoginCount() {
        this.totalLoginCount = [SELECT count() FROM LoginHistory WHERE UserId = :this.communityUser.Id];
        
        return this.totalLoginCount;
        // use this: http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_loginhistory.htm
    }
    
    public List<LoginHistory> getLoginHistory() {
        if (this.loginHistory != null) {
            return this.loginHistory;
        }
        
        this.loginHistory = [SELECT Id,LoginTime,ApiType,ApiVersion,LoginType,Status FROM LoginHistory WHERE UserId = :this.communityUser.Id];
        return this.loginHistory;
    }
    
    public List<CollaborationGroupMember> getGroups() {
        if (this.groups != null) {
            return this.groups;
        }
        
        this.groups = getGroupMemberships(this.communityUser);
        return this.groups;
    }
    
    public List<CollaborationGroupMember> getGroupMemberships(User usr) {
        if (usr == null) {
            System.debug('No community user specified.');
            return null;
        }
        
        List<CollaborationGroupMember> results = null;
        try {
            results = [
                SELECT 
                    Id,
                    CollaborationGroupId,
                    CollaborationRole,
                    CreatedDate,
                    MemberId,
                    NotificationFrequency 
                FROM 
                    CollaborationGroupMember 
                WHERE 
                    MemberId = :usr.Id 
                ORDER BY 
                    CreatedDate DESC NULLS FIRST 
                LIMIT 20];
        } catch (Exception e) {
            System.debug(e);
        }
        // 
		return results;
    }
    
    public List<EntitySubscription> getFollowingPeople() {
        if (this.followingPeople != null) {
            return this.followingPeople;
        }
        
        this.followingPeople = getPeopleFollowing(this.communityUser);
        return this.followingPeople;
    }
    
    public List<EntitySubscription> getPeopleFollowing(User usr) {
        if (usr == null) {
            System.debug('No community user specified.');
            return null;
        }
        
        List<EntitySubscription> results = null;
        try {
            results = [
            SELECT 
                Id,
                CreatedDate,
                NetworkId,
                ParentId,
                SubscriberId 
            FROM 
                EntitySubscription 
            WHERE
                SubscriberId = :usr.Id 
            ORDER BY CreatedDate DESC NULLS FIRST 
            LIMIT 20];
        } catch (Exception e) {
            System.debug(e);
        }
        
        return results;
    }
    
    public List<Idea> getIdeas() {
        if (this.ideas != null) {
            return this.ideas;
        }
        
        this.ideas = getIdeas(this.communityUser);
        return this.ideas;
    }
    
    public List<Idea> getIdeas(User usr) {
        if (usr == null) {
            System.debug('No community user specified.');
            return null;
        }
        
        List<Idea> results = null;
        try {
            results = [
                SELECT 
                	Id,
                	NumComments,
                	Status,
                	Title,
                	VoteScore,
                	VoteTotal,
                	CreatedDate
                FROM 
                	Idea 
                WHERE 
                	CreatedById = :usr.Id 
                ORDER BY CreatedDate DESC NULLS FIRST
                LIMIT 20];
        } catch (Exception e) {
            System.debug(e);
        }
 
		return results;
    }
}