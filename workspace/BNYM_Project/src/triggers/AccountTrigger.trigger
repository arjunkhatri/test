trigger AccountTrigger on Account (before delete) 
{
  AccountTriggerHandler handler = new AccountTriggerHandler();
  
  if(Trigger.isBefore && Trigger.isDelete) {
    handler.onBeforeDelete(Trigger.old, Trigger.oldmap);
  }
}