<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>ASI_NewFinancialAccountPage</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>ASI_NewFinancialAccountPage</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>ASI_RedirectToFinancialAccountsHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This object stores the financial account details for household</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ASI_Account_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field store the account group for a Financial account</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Account group for Person Account financial account</inlineHelpText>
        <label>Account Group</label>
        <referenceTo>ASI_Account_Group__c</referenceTo>
        <relationshipLabel>goalgamiPro Financial Accounts</relationshipLabel>
        <relationshipName>goalgamiPro_Financial_Accounts</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ASI_Cash_Balance__c</fullName>
        <defaultValue>0.00</defaultValue>
        <deprecated>false</deprecated>
        <description>This field specifies the cash balance of the financial account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Cash balance currently held in the account.</inlineHelpText>
        <label>Cash Balance ($)</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ASI_Comments__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the comments about the financial account</description>
        <externalId>false</externalId>
        <label>Comments</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ASI_External_Account_Number__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores an external account number. May be used for data integration purposes in the future.</description>
        <externalId>false</externalId>
        <label>Account Number</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Help_Site__c</fullName>
        <deprecated>false</deprecated>
        <description>This field contains the URL of the help site</description>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;http://support.advisorsoftware.com/forums/22208671-Financial-Accounts&apos;, &apos;Help for this page&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Help Site</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Household__c</fullName>
        <deprecated>false</deprecated>
        <description>This field specifies the client account that owns the financial account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Client account that owns the financial account.</inlineHelpText>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Financial_Accounts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ASI_Investment_Balance__c</fullName>
        <defaultValue>0.00</defaultValue>
        <deprecated>false</deprecated>
        <description>This field specifies the current value of the investment holdings in the account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Current value of investments (i.e. non-cash holdings) held in the account.</inlineHelpText>
        <label>Investment Holdings Value ($)</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ASI_Is_Out_Of_Sync__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This is the flag to  indicate that the Financial account is not in sync</description>
        <externalId>false</externalId>
        <label>Is Out Of Sync</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ASI_Long_Term_Gain_Loss__c</fullName>
        <defaultValue>0.00</defaultValue>
        <deprecated>false</deprecated>
        <description>This field stores the long term gain for the financial account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Current long-term gain or loss for the account. Long-term applies to those holdings held more than a year. Only used for Brokerage accounts.</inlineHelpText>
        <label>Long-Term Gain/Loss ($)</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ASI_Primary_Owner__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field specifies the primary owner of the financial account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contact that is the primary owner of the account.</inlineHelpText>
        <label>Primary Owner</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.ASI_Household__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>goalgamiPro Financial Accounts for Contacts</relationshipLabel>
        <relationshipName>Financial_Accounts</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ASI_Short_Term_Gain_Loss__c</fullName>
        <defaultValue>0.00</defaultValue>
        <deprecated>false</deprecated>
        <description>This field stores the short term gain for the financial account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Current short-term gain or loss for the account. Short-term applies to those holdings held less than a year. Only used for Brokerage accounts.</inlineHelpText>
        <label>Short-Term Gain/Loss ($)</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ASI_Subscriber_Financial_Account_ID__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the subscriber financial account id</description>
        <externalId>false</externalId>
        <label>Subscriber Financial Account ID</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Tax_Status__c</fullName>
        <deprecated>false</deprecated>
        <description>This field describes the tax status for an account</description>
        <externalId>false</externalId>
        <label>Tax Status</label>
        <picklist>
            <picklistValues>
                <fullName>Taxable</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tax-Deferred</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tax-Free</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ASI_Total_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This value is the sum of investment value and cash balance</description>
        <externalId>false</externalId>
        <formula>ASI_Investment_Balance__c  +  ASI_Cash_Balance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ASI_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>This field describes the type of financial account.</description>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>401(k)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>403(b)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>529 / Education</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Bank</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Brokerage</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Roth IRA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Roth 401(k)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Traditional IRA</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Related_Financial_Account__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/&apos;+ ASI_Subscriber_Financial_Account_ID__c , ASI_Subscriber_Financial_Account_ID__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Related Financial Account</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>goalgamiPro Financial Account</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>ASI_Household__c</columns>
        <columns>ASI_Primary_Owner__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>goalgamiPro Financial Accounts</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>ASI_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Tax_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Primary_Owner__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Cash_Balance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Investment_Balance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Total_Value__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Is_Out_Of_Sync__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>ASI_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ASI_Tax_Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ASI_Household__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ASI_External_Account_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ASI_Primary_Owner__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ASI_Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ASI_Tax_Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ASI_Household__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ASI_External_Account_Number__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ASI_Primary_Owner__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>ASI_Type__c</searchFilterFields>
        <searchFilterFields>ASI_Tax_Status__c</searchFilterFields>
        <searchFilterFields>ASI_Primary_Owner__c</searchFilterFields>
        <searchFilterFields>ASI_Cash_Balance__c</searchFilterFields>
        <searchFilterFields>ASI_Investment_Balance__c</searchFilterFields>
        <searchFilterFields>ASI_Total_Value__c</searchFilterFields>
        <searchFilterFields>ASI_Is_Out_Of_Sync__c</searchFilterFields>
        <searchResultsAdditionalFields>ASI_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Tax_Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Primary_Owner__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Cash_Balance__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Household__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Investment_Balance__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Total_Value__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ASI_Is_Out_Of_Sync__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ASI_Cash_Balance_Investment_Balance_Rule</fullName>
        <active>true</active>
        <description>Cash Balance and Investment Holdings Value must be greater than or equal to zero.</description>
        <errorConditionFormula>ASI_Cash_Balance__c  &lt; 0 ||  ASI_Investment_Balance__c  &lt; 0</errorConditionFormula>
        <errorMessage>Cash Balance and Investment Holdings Value must be greater than or equal to zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ASI_Short_Term_Long_Term_Rule</fullName>
        <active>true</active>
        <description>This rule validates if the Short term or long term rules are greater than Investment value</description>
        <errorConditionFormula>IF( (ASI_Long_Term_Gain_Loss__c  &gt; ASI_Investment_Balance__c) || 
 ( ASI_Short_Term_Gain_Loss__c  &gt;  ASI_Investment_Balance__c)  ||
(  ASI_Long_Term_Gain_Loss__c +  ASI_Short_Term_Gain_Loss__c  &gt;  ASI_Investment_Balance__c ),true,false)</errorConditionFormula>
        <errorMessage>Short-Term or Long-Term Gain/ Loss values or their sum cannot exceed the Investment Holdings Value of the account.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ASI_Validate_Birthdate_Of_Owner</fullName>
        <active>true</active>
        <description>This rule validates if the Owner of the Financial Account has Birth date set and if so if it is a future date.</description>
        <errorConditionFormula>IF(ISBLANK(ASI_Primary_Owner__c) , false , IF(ISNULL(ASI_Primary_Owner__r.Birthdate)|| ISBLANK(TEXT(ASI_Primary_Owner__r.ASI_Gender__c)),true,IF(ASI_Primary_Owner__r.Birthdate &gt;TODAY() || 
 YEAR(ASI_Primary_Owner__r.Birthdate) &lt; 1900 ,true,false)))</errorConditionFormula>
        <errorDisplayField>ASI_Primary_Owner__c</errorDisplayField>
        <errorMessage>Invalid Primary Owner selected. The Primary Owner assigned must have a birth date and gender set. You must update the Contact or Person Account record with this information before assigning as the Primary Owner.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ASI_Validate_FirstName_Of_Contact</fullName>
        <active>true</active>
        <description>This rule validates if the first name of the Primary Owner is set or not</description>
        <errorConditionFormula>IF(ISBLANK(ASI_Primary_Owner__c),false,ISBLANK( ASI_Primary_Owner__r.FirstName ))</errorConditionFormula>
        <errorDisplayField>ASI_Primary_Owner__c</errorDisplayField>
        <errorMessage>Invalid Primary Owner selected. The Primary Owner must have a first name entered. You must update the Contact record with this information before using the Contact as a Primary Owner.</errorMessage>
    </validationRules>
</CustomObject>
