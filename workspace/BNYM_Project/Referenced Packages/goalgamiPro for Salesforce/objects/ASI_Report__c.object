<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>ASI_CreateReport</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>ASI_CreateReport</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>ASI_RedirectToReportsHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This object stores the report configurations for a plan</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>ASI_BalanceSheet__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field links the balance sheet to the report</description>
        <externalId>false</externalId>
        <inlineHelpText>Balance Sheet that report is using.</inlineHelpText>
        <label>Balance Sheet</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Balance Sheet must belong to the plan selected</errorMessage>
            <filterItems>
                <field>ASI_Balance_Sheet__c.ASI_Plan__c</field>
                <operation>equals</operation>
                <valueField>$Source.ASI_Plan__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ASI_Balance_Sheet__c</referenceTo>
        <relationshipLabel>Reports</relationshipLabel>
        <relationshipName>Reports</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ASI_Body_Text_for_Notes_Section__c</fullName>
        <deprecated>false</deprecated>
        <description>This field captures the body text for the Notes section of the report.</description>
        <externalId>false</externalId>
        <inlineHelpText>Body text for the Notes section of the report.

Use &lt;p&gt; to indicate start of a new paragraph.</inlineHelpText>
        <label>Notes Body Text</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>20</visibleLines>
    </fields>
    <fields>
        <fullName>ASI_Can_I_Afford_My_Goals_Narrative__c</fullName>
        <deprecated>false</deprecated>
        <description>It  stores the section/text to include in report section Can I Afford My Goals?</description>
        <externalId>false</externalId>
        <inlineHelpText>Narrative text options for the &apos;Can I Afford My Goals?&apos; section of the report that will appear below the page title.</inlineHelpText>
        <label>&apos;Can I Afford My Goals?&apos; Text Options</label>
        <picklist>
            <picklistValues>
                <fullName>Resources are greater than the Aspirational Goal Level</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resources are greater than the Necessary and Target Goal Levels, but less than Aspirational Goal Level</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resources are greater than the Necessary Goal Level, but less than the Target and Aspirational Goal Level</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resources are less than the Necessary Goal Level</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Custom Text</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ASI_Custom_Text__c</fullName>
        <deprecated>false</deprecated>
        <description>Stores narrative text for the &apos;Can I Afford My Goals?&apos; section of the report.</description>
        <externalId>false</externalId>
        <inlineHelpText>Narrative text for the &apos;Can I Afford My Goals?&apos; section of the report that will appear below the page title. 180 characters maximum.</inlineHelpText>
        <label>&apos;Can I Afford My Goals?&apos; Text</label>
        <length>180</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Disclaimer_Text__c</fullName>
        <deprecated>false</deprecated>
        <description>This field captures the body text for the Notes section of the report.</description>
        <externalId>false</externalId>
        <inlineHelpText>Disclaimer text to include in the Notes section of the report. 

Use &lt;p&gt; to indicate start of a new paragraph.</inlineHelpText>
        <label>Disclaimer Text</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>ASI_Footer_Text__c</fullName>
        <deprecated>false</deprecated>
        <description>It contains the footer text for report</description>
        <externalId>false</externalId>
        <inlineHelpText>Text that appears in footer of report. 140 characters maximum.</inlineHelpText>
        <label>Footer Text</label>
        <length>140</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Help_Site__c</fullName>
        <deprecated>false</deprecated>
        <description>This field contains the URL of the help site</description>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;http://support.advisorsoftware.com/forums/22208811-Reports&apos;, &apos;Help for this page&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Help Site</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Household__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>It links the report to household</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of account associated with report.</inlineHelpText>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Reports</relationshipLabel>
        <relationshipName>Reports</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ASI_Plan__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>It links the plan to the report</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of plan associated with report.</inlineHelpText>
        <label>Plan Name</label>
        <referenceTo>ASI_Plan__c</referenceTo>
        <relationshipLabel>Reports</relationshipLabel>
        <relationshipName>Reports</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ASI_Report_Header__c</fullName>
        <deprecated>false</deprecated>
        <description>It stores the report header</description>
        <externalId>false</externalId>
        <inlineHelpText>Name that appears after the &apos;Prepared For&apos; text in the header area of the report. 30 characters maximum.</inlineHelpText>
        <label>&apos;Prepared For&apos; Name</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Report_Sections__c</fullName>
        <deprecated>false</deprecated>
        <description>This field store the sections that are included in the report</description>
        <externalId>false</externalId>
        <inlineHelpText>Sections included in report.</inlineHelpText>
        <label>Report Sections</label>
        <picklist>
            <picklistValues>
                <fullName>What Is My Plan?</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Can I Afford My Goals ? (1 chart)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Can I Afford My Goals ? (3 charts)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Notes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>All Sections</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>ASI_Report__c</fullName>
        <deprecated>false</deprecated>
        <description>It provides the hyperlink to view report</description>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/apex/goalgamiPro__ASI_ViewReport?id=&apos;+Id ,&apos;View&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Link to last report generated.</inlineHelpText>
        <label>View Report</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ASI_Request__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the Report request</description>
        <externalId>false</externalId>
        <inlineHelpText>Report request XML.</inlineHelpText>
        <label>Request</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>ASI_Title_for_Notes_Section__c</fullName>
        <deprecated>false</deprecated>
        <description>It stores theTitle for Notes Section</description>
        <externalId>false</externalId>
        <inlineHelpText>Title for Notes section of the report. 30 characters maximum.</inlineHelpText>
        <label>Notes Section Title</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Report</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>ASI_Household__c</columns>
        <columns>ASI_Plan__c</columns>
        <columns>ASI_BalanceSheet__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Report  Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Reports</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>ASI_Household__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_Plan__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ASI_BalanceSheet__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>ASI_Plan_Household_Validation_Rule</fullName>
        <active>true</active>
        <description>This rule validates that the Plan belongs to the Account selected.</description>
        <errorConditionFormula>ASI_Plan__r.ASI_Household__c  &lt;&gt;  ASI_Household__c</errorConditionFormula>
        <errorDisplayField>ASI_Plan__c</errorDisplayField>
        <errorMessage>Plan selected does not belong to the household selected.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>ASI_Run_Report</fullName>
        <availability>online</availability>
        <description>This button is used for Generating Report</description>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>Run Report</masterLabel>
        <openType>newWindow</openType>
        <page>ASI_RunReport</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
</CustomObject>
