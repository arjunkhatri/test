<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <customSettingsVisibility>Public</customSettingsVisibility>
    <description>This custom settings stores the mappings between account types for the goalgamiPro Financial Account object and an existing financial account object.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Default_Mapping__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to provide a goalgamiPro financial account type that all financial accounts will be mapped to if an explicit account type mapping doesn&apos;t exist.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the goalgamiPro financial account type that will be used by default if no explicit mapping exists for an account type. If you don&apos;t enter a value here, then no default account type mapping will be used.</inlineHelpText>
        <label>Default Mapping</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_401_k_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro 401(K) account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the 401(k) account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>401(k)</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_403_b_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro 403(b) account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the 403(b) account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>403(b)</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_529_Education_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro 529/Education account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the 529/Education account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>529 / Education</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_Bank_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro Bank account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the Bank account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>Bank</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_Brokerage_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro Brokerage account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the Brokerage account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>Brokerage</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_Roth_401_k_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the Roth 401(K) account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the Roth 401(k) account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>Roth 401(k)</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_Roth_IRA_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro Roth IRA account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the Roth IRA account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>Roth IRA</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type_Traditional_IRA_Value__c</fullName>
        <deprecated>false</deprecated>
        <description>This field stores the account types to map to the goalgamiPro Traditional IRA account type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the name of the account types available with your existing financial account object that should be mapped to the Traditional IRA account type in the goalgamiPro Financial Account object.</inlineHelpText>
        <label>Traditional IRA</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Financial Account Type Mapping</label>
</CustomObject>
