<!-- This VisualForce Page overrides the Standard New and Edit screen for Income -->
<apex:page standardController="goalgamiPro__ASI_Income__c" extensions="goalgamiPro.ASI_NewIncomePageController">
    <apex:sectionHeader title="{!$Label.goalgamipro__ASI_NewIncome_SectionHeader}" subtitle="{!sectionHeaderName}"/>
    <apex:form id="formId">
        <!--  Include Stylesheet -->
        <apex:styleSheet value="{!URLFOR($Resource.goalgamiPro__ASI_GGProResources,'/CSS/ReportConfiguration.CSS')}"/>
        <apex:pageMessages rendered="{!NOT(fromPlan)}"/>
        	<apex:pageBlock title="{!$Label.goalgamipro__ASI_NewIncome_SectionHeader}" mode="edit" id="pageBlock" rendered="{!fromPlan}">
	        
	        <script>
	            /** Function to get the Record type id selected and the call the action method to set the same **/
	            function changeRecordType(record){
	                var val = document.getElementById(record).value;     
	                setRecordType(val);
	            }
	            
	        </script>
	        
	        <!-- Action Function to set the Record type -->
	        <apex:actionFunction name="setRecordType" rerender="pageBlock" action="{!fetchRecordTypeName}" status="recordStatus">
	            <apex:param name="firstParam" value="" assignTo="{!recordTypeId}"/>
	        </apex:actionFunction>
	        <apex:actionFunction name="callRerender" rerender="pageBlock" immediate="true"/>
	         
	        <!--  Action Status -->
	        <center><b><apex:actionStatus startText="Saving....." id="saveStatus"/></b></center>
	        
	        <!--  Page Block Buttons for Save, Save&New and Cancel -->
            <apex:pageBlockButtons >
                <apex:commandButton value="{!$Label.goalgamipro__ASI_NewIncome_Save}" action="{!save}" rerender="messageId" status="saveStatus"/>
                <apex:commandButton value="{!$Label.goalgamipro__ASI_NewIncome_SaveAndNew}"  action="{!saveAndNew}" status="saveStatus"/>
                <apex:commandButton value="{!$Label.goalgamipro__ASI_NewIncome_Cancel}" action="{!cancel}" rerender="dummy" />
            </apex:pageBlockButtons>
            
            <!-- Apex Page Messages for validation error messages custom and standard -->
            <apex:pageMessages id="messageId"  showDetail="false"/>
            
            <!-- Page Block Section named Information -->
            <apex:pageBlockSection title="{!$Label.goalgamipro__ASI_NewIncome_InformationHeader}" columns="2">
                <apex:pageBlockSectionItem >  
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncomePage_Plan}" for="planField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Plan__c}" id="planField" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_CommentsField}" for="commentField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Comments__c}" id="commentField" />
                </apex:pageBlockSectionItem> 
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_NameField}" for="nameField"/>
                    <apex:outputPanel styleclass="requiredInput" layout="block">  
                       <div class="requiredBlock"/>
                       <apex:inputField value="{!income.name}" id="nameField" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem> 
                <apex:pageBlockSectionItem >    
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_RecordType_Field}" for="recordType"/>
                    <apex:actionRegion >
                    	<apex:inputField value="{!income.RecordTypeId}" id="recordType" onchange="changeRecordType('{!$Component.this}');" >                       
                    	</apex:inputField>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>   
                 <apex:pageBlockSectionItem > 
                 	<apex:actionStatus id="recordStatus" styleClass="searchStatus" >
                        <apex:facet name="start">  
                            <apex:image id="searchingImage" value="{!URLFOR($Resource.goalgamiPro__ASI_GGProResources,'/Images/indicator-big-2.gif')}" width="25" height="25"/>
                         </apex:facet>
                 	</apex:actionStatus>     
                </apex:pageBlockSectionItem>                    
                <apex:pageBlockSectionItem rendered="{!isAnnuityIncome}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_BenefitRecipientField}" for="benefitRecipient"/>
                     <apex:outputPanel styleclass="requiredInput" layout="block">  
                        <div class="requiredBlock"/>
                        <apex:selectList value="{!recepientSelected}" size="1" id="benefitRecipient" required="true" >
                        	<apex:selectOptions value="{!benefitRecipients}">
                        	</apex:selectOptions>
                    	</apex:selectList>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!isRetirementIncome}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_LinkedAccount_Field}" for="linkedAccount"/>
                    <apex:outputPanel styleclass="requiredInput" layout="block">  
                        <div class="requiredBlock"/>
                         <apex:selectList value="{!accountSelected}" size="1" id="linkedAccount" required="true" >
                        <apex:selectOptions value="{!linkedAccounts}">
                        </apex:selectOptions>
                    </apex:selectList> 
                    </apex:outputPanel>     
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >        
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Is_Included_in_Plan__c.inlineHelpText}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_IsIncludedField}" for="includedField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Is_Included_in_Plan__c}" id="includedField" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- Page Block Section named What is the timing of this Income/Savings? -->
            <apex:pageBlockSection title="{!$Label.goalgamipro__ASI_NewIncome_SectionHeaderTiming}" >
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Frequency__c.inlineHelpText}"> 
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_FrequencyField}" for="frequencyField"/>
                    <apex:outputPanel styleclass="requiredInput" layout="block">  
                        <div class="requiredBlock"/>
                        <apex:inputField value="{!income.goalgamiPro__ASI_Frequency__c}" id="frequencyField" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_When_will_this_Income_Savings_start__c.inlineHelpText}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_StartField}" for="startField"/>
                    <apex:outputPanel styleclass="requiredInput" layout="block">  
                        <div class="requiredBlock"/>
                        <apex:inputField value="{!income.goalgamiPro__ASI_When_will_this_Income_Savings_start__c}" id="startField" onchange="callRerender();"  />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_When_will_the_income_savings_end__c.inlineHelpText}" rendered="{!NOT(isAnnuityIncome)}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_EndField}" for="endField"/>
                    <apex:outputPanel styleclass="requiredInput" layout="block">  
                        <div class="requiredBlock"/>
                        <apex:inputField value="{!income.goalgamiPro__ASI_When_will_the_income_savings_end__c}" id="endField"  required="true"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Start_Date__c.inlineHelpText}"> 
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_Startdate_field}" for="startDateField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Start_Date__c}" id="startDateField" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Age__c.inlineHelpText}" rendered="{!isAnnuityIncome}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_AgeBenefitField}" for="ageField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Age__c}" id="ageField" required="{!IF(income.goalgamiPro__ASI_When_will_this_Income_Savings_start__c == 'Enter Age',true,false)}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_End_Date__c.inlineHelpText}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_Enddatefield}" for="endDateField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_End_Date__c}" id="endDateField" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
             <!-- Page Block Section named What is the income/savings per month (in today's dollars)? -->
            <apex:pageBlockSection title="{!$Label.goalgamipro__ASI_NewIncome_SectionHeader_Savings}" columns="1" >
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Contribution_Amount_per_period__c.inlineHelpText}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_AmtPerPeriodField}" for="amountField"/>
                    <apex:outputPanel styleclass="requiredInput" layout="block">  
                        <div class="requiredBlock"/>
                        <apex:inputField value="{!income.goalgamiPro__ASI_Contribution_Amount_per_period__c}" id="amountField"  />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Include_annual_COLA_of_2_5__c.inlineHelpText}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_AnnualCOLAField}" for="applyCOLAField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Include_annual_COLA_of_2_5__c}" id="applyCOLAField" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem HelpText="{!$ObjectType.goalgamiPro__ASI_Income__c.fields.goalgamiPro__ASI_Additional_annual_adjustments__c.inlineHelpText}">
                    <apex:outputLabel value="{!$Label.goalgamipro__ASI_NewIncome_AnnualAdjustmentField}" for="annualAdjField"/>
                    <apex:inputField value="{!income.goalgamiPro__ASI_Additional_annual_adjustments__c}" id="annualAdjField" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
        </apex:pageBlock>
    </apex:form>
 
</apex:page>