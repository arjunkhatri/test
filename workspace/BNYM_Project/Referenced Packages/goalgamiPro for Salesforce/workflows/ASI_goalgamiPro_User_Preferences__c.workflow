<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ASI_SetUserAssociated</fullName>
        <description>This updates the User Associated</description>
        <field>ASI_User_Associated__c</field>
        <formula>ASI_User__r.FirstName  + &apos; &apos; +  ASI_User__r.LastName</formula>
        <name>ASI_SetUserAssociated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ASI_SetAssociatedUser</fullName>
        <actions>
            <name>ASI_SetUserAssociated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sets the User Associated to the User Preference record</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
