<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ASI_Update_MemberId</fullName>
        <description>This is the field update for Member Id</description>
        <field>ASI_Unique_Member_Id__c</field>
        <formula>ASI_Account_Group__r.Id + &apos; &apos; +  ASI_Group_Member__r.Id</formula>
        <name>ASI_Update_MemberId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>ASI_Update_Account_Member_Id</fullName>
        <actions>
            <name>ASI_Update_MemberId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule updates the member id on creation/updation</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
