<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ASI_update_plan_financial_account_id</fullName>
        <description>It updates the plan financial account id</description>
        <field>ASI_Plan_Financial_Account_ID__c</field>
        <formula>ASI_Plan__r.Id  + &apos; &apos; +   ASI_Financial_Account__r.Id</formula>
        <name>ASI update plan financial account id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ASI Set the Plan Financial Account Id</fullName>
        <actions>
            <name>ASI_update_plan_financial_account_id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It updates the plan financial account id</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
