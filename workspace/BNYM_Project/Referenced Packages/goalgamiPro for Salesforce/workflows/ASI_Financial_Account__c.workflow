<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ASI_Set_Tax_Deferred_tax_Status</fullName>
        <description>It sets set the tax status as Tax Deferred ,If Account Type = &apos;401(k)&apos;, &apos;403(b)&apos;, &apos;529/Education&apos;, or &apos;Traditional IRA&apos;, set Tax Status = &apos;Tax-Deferred&apos;</description>
        <field>ASI_Tax_Status__c</field>
        <literalValue>Tax-Deferred</literalValue>
        <name>Set_Tax_Deferred_tax_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ASI_Set_Tax_FreeTax_Status</fullName>
        <description>It set the tax status as Tax Deferred If Account Type = &apos;Roth IRA&apos; or &apos;Roth 401(k)&apos;, set Tax Status = &apos;Tax Free&apos;</description>
        <field>ASI_Tax_Status__c</field>
        <literalValue>Tax-Free</literalValue>
        <name>ASI_Set Tax-FreeTax Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ASI_Set_Tax_Status_as_Taxable</fullName>
        <description>It  sets the tax status as Taxable for account type Bank or brokerage</description>
        <field>ASI_Tax_Status__c</field>
        <literalValue>Taxable</literalValue>
        <name>Set Tax Status as Taxable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ASI_Set Tax-Deferred Tax Status</fullName>
        <actions>
            <name>ASI_Set_Tax_Deferred_tax_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ASI_Financial_Account__c.ASI_Type__c</field>
            <operation>equals</operation>
            <value>403(b),401(k),529 / Education,Traditional IRA</value>
        </criteriaItems>
        <description>This workflow rule set the tax status as Tax Deferred ,If Account Type = &apos;401(k)&apos;, &apos;403(b)&apos;, &apos;529/Education&apos;, or &apos;Traditional IRA&apos;, set Tax Status = &apos;Tax-Deferred&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ASI_Set Tax-Free tax Status</fullName>
        <actions>
            <name>ASI_Set_Tax_FreeTax_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ASI_Financial_Account__c.ASI_Type__c</field>
            <operation>equals</operation>
            <value>Roth IRA,Roth 401(k)</value>
        </criteriaItems>
        <description>This workflow rule set the tax status as Tax Deferred If Account Type = &apos;Roth IRA&apos; or &apos;Roth 401(k)&apos;, set Tax Status = &apos;Tax Free&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ASI_Set_Taxable_tax_Status</fullName>
        <actions>
            <name>ASI_Set_Tax_Status_as_Taxable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ASI_Financial_Account__c.ASI_Type__c</field>
            <operation>equals</operation>
            <value>Brokerage,Bank</value>
        </criteriaItems>
        <description>This workflow rule set the tax status as Taxable for account type Bank or brokerage</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
